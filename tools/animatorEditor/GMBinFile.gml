#define Readme
/*
Read Each Script Before Calling Them

If you did not check the return of the OpenFile functions 
for 0 and attempt to proceed you will get an...

Unexpected Error Occured During The Game error

...rying to read a null file... I could check for the file pointer 
to be valid but that would only slow down the process

you will also get that error if you forget parameters to the scripts
or if you pass an enormous value into a smaller type like
-123456789 in WriteByte

Again, I could check for this but it would only slow things down

Make sure you use the right Write and Read for you data. The Write functions
don't care if the value is negative or not but the reads do.

The reads also sets the range of the value due to the nature of
the various types.

For example, a byte can be -127 to 128 or 0 to 255.
-127 to 128 is handled by the regular Read
0 to 255 is handled by the ReadU

A write can handle data beyond its normal range up to (not tested)
the limits of the next type. 

So a write byte should handle a range up to min+1/max-1 of the signed short (the next type)
Your data will not save properly (value will roll over, kindof like 1000 mod 256) but the system should not crash.

But anything beyond that range will crash the system.
The short hast the name limitations but using the min max of the int type.
The int has the same limitations but using the min max of the long type.
*/
#define GMBINInit
//Call this when the game starts to create the dll interface
//returns nothing... GM will stop if the dll could not be linked

//NOTE: You must have gmbinfile.dll in your game directory

//Example call
//When the game starts
//GMBINInit();
if !file_exists("gmbinfile.dll") show_message("File not found: gmbinfile.dll#In directory: " + working_directory);

var WTF;
WTF = false;
//export double GMBINOpenFileRead(LPCSTR fname)
global.dll_GMBINOpenFileRead=external_define("gmbinfile.dll","GMBINOpenFileRead",dll_stdcall,ty_real,1,ty_string);
if(WTF) show_message("Defined: GMBINOpenFileRead")

//export double GMBINOpenFileWrite(LPCSTR fname)
global.dll_GMBINOpenFileWrite=external_define("gmbinfile.dll","GMBINOpenFileWrite",dll_stdcall,ty_real,1,ty_string);
if(WTF) show_message("Defined: GMBINOpenFileWrite")

//export double GMBINOpenFileReadWrite(LPCSTR fname)
global.dll_GMBINOpenFileReadWrite=external_define("gmbinfile.dll","GMBINOpenFileReadWrite",dll_stdcall,ty_real,1,ty_string);
if(WTF) show_message("Defined: GMBINOpenFileReadWrite")


//export double GMBINOpenFileReadPW(LPCSTR fname, LPCSTR password)
global.dll_GMBINOpenFileReadPW=external_define("gmbinfile.dll","GMBINOpenFileReadPW",dll_stdcall,ty_real,2,ty_string,ty_string);
if(WTF) show_message("Defined: GMBINOpenFileReadPW")

//export double GMBINOpenFileWritePW(LPCSTR fname, LPCSTR password)
global.dll_GMBINOpenFileWritePW=external_define("gmbinfile.dll","GMBINOpenFileWritePW",dll_stdcall,ty_real,2,ty_string,ty_string);
if(WTF) show_message("Defined: GMBINOpenFileWritePW")

//export double GMBINOpenFileReadWritePW(LPCSTR fname, LPCSTR password)
global.dll_GMBINOpenFileReadWritePW=external_define("gmbinfile.dll","GMBINOpenFileReadWritePW",dll_stdcall,ty_real,2,ty_string,ty_string);
if(WTF) show_message("Defined: GMBINOpenFileReadWritePW")


//export double GMBINCloseFile(double hf)
global.dll_GMBINCloseFile=external_define("gmbinfile.dll","GMBINCloseFile",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINCloseFile")


//export double GMBINWriteByte (double hf, double byte)
global.dll_GMBINWriteByte =external_define("gmbinfile.dll","GMBINWriteByte",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteByte")

//export double GMBINReadByte(double hf)
global.dll_GMBINReadByte=external_define("gmbinfile.dll","GMBINReadByte",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadByte")

//export double GMBINReadUByte(double hf)
global.dll_GMBINReadUByte=external_define("gmbinfile.dll","GMBINReadUByte",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadUByte")


//export double GMBINWriteShort(double hf, double int)
global.dll_GMBINWriteShort=external_define("gmbinfile.dll","GMBINWriteShort",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteShort")

//export double GMBINWriteShortBE(double hf, double int)
global.dll_GMBINWriteShortBE=external_define("gmbinfile.dll","GMBINWriteShortBE",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteShortBE")

//export double GMBINReadShort(double hf)
global.dll_GMBINReadShort=external_define("gmbinfile.dll","GMBINReadShort",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadShort")

//export double GMBINReadShortBE(double hf)
global.dll_GMBINReadShortBE=external_define("gmbinfile.dll","GMBINReadShortBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadShortBE")

//export double GMBINReadUShort(double hf)
global.dll_GMBINReadUShort=external_define("gmbinfile.dll","GMBINReadUShort",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadUShort")

//export double GMBINReadUShortBE(double hf)
global.dll_GMBINReadUShortBE=external_define("gmbinfile.dll","GMBINReadUShortBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadUShortBE")


//export double GMBINWriteInt(double hf, double int)
global.dll_GMBINWriteInt=external_define("gmbinfile.dll","GMBINWriteInt",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteInt")

//export double GMBINWriteIntBE(double hf, double int)
global.dll_GMBINWriteIntBE=external_define("gmbinfile.dll","GMBINWriteIntBE",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteIntBE")


//export double GMBINReadInt(double hf)
global.dll_GMBINReadInt=external_define("gmbinfile.dll","GMBINReadInt",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadInt")

//export double GMBINReadIntBE(double hf)
global.dll_GMBINReadIntBE=external_define("gmbinfile.dll","GMBINReadIntBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadIntBE")

//export double GMBINReadUInt(double hf)
global.dll_GMBINReadUInt=external_define("gmbinfile.dll","GMBINReadUInt",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadUInt")

//export double GMBINReadUIntBE(double hf)
global.dll_GMBINReadUIntBE=external_define("gmbinfile.dll","GMBINReadUIntBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadUIntBE")


//export double GMBINWriteLong(double hf, double int)
global.dll_GMBINWriteLong=external_define("gmbinfile.dll","GMBINWriteLong",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteLong")

//export double GMBINWriteLongBE(double hf, double int)
global.dll_GMBINWriteLongBE=external_define("gmbinfile.dll","GMBINWriteLongBE",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteLongBE")

//export double GMBINReadLong(double hf)
global.dll_GMBINReadLong=external_define("gmbinfile.dll","GMBINReadLong",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadLong")

//export double GMBINReadLongBE(double hf)
global.dll_GMBINReadLongBE=external_define("gmbinfile.dll","GMBINReadLongBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadLongBE")

//export double GMBINReadULong(double hf)
global.dll_GMBINReadULong=external_define("gmbinfile.dll","GMBINReadULong",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadULong")

//export double GMBINReadULongBE(double hf)
global.dll_GMBINReadULongBE=external_define("gmbinfile.dll","GMBINReadULongBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadULongBE")

//export double GMBINWriteFloat(double hf, double int)
global.dll_GMBINWriteFloat=external_define("gmbinfile.dll","GMBINWriteFloat",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteFloat")

//export double GMBINWriteFloatBE(double hf, double int)
global.dll_GMBINWriteFloatBE=external_define("gmbinfile.dll","GMBINWriteFloatBE",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteFloatBE")

//export double GMBINReadFloat(double hf)
global.dll_GMBINReadFloat=external_define("gmbinfile.dll","GMBINReadFloat",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadFloat")

//export double GMBINReadFloatBE(double hf)
global.dll_GMBINReadFloatBE=external_define("gmbinfile.dll","GMBINReadFloatBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadFloatBE")


//export double GMBINWriteDouble(double hf, double real)
global.dll_GMBINWriteDouble=external_define("gmbinfile.dll","GMBINWriteDouble",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteDouble")

//export double GMBINWriteDoubleBE(double hf, double real)
global.dll_GMBINWriteDoubleBE=external_define("gmbinfile.dll","GMBINWriteDoubleBE",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINWriteDoubleBE")

//export double GMBINReadDouble(double hf)
global.dll_GMBINReadDouble=external_define("gmbinfile.dll","GMBINReadDouble",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadDouble")

//export double GMBINReadDoubleBE(double hf)
global.dll_GMBINReadDoubleBE=external_define("gmbinfile.dll","GMBINReadDoubleBE",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINReadDoubleBE")


//export double GMBINWriteBuffer(double hf, LPCSTR buff)
global.dll_GMBINWriteBuffer=external_define("gmbinfile.dll","GMBINWriteBuffer",dll_stdcall,ty_real,2,ty_real,ty_string);
if(WTF) show_message("Defined: GMBINWriteBuffer")

//export double GMBINReadBuffer(double hf, LPSTR buff)
global.dll_GMBINReadBuffer=external_define("gmbinfile.dll","GMBINReadBuffer",dll_stdcall,ty_real,2,ty_real,ty_string);
if(WTF) show_message("Defined: GMBINReadBuffer")

//export double GMBINWriteString(double hf, LPCSTR string)
global.dll_GMBINWriteString=external_define("gmbinfile.dll","GMBINWriteString",dll_stdcall,ty_real,2,ty_real,ty_string);
if(WTF) show_message("Defined: GMBINWriteString")


//export double GMBINIsEOF(double hf)
global.dll_GMBINIsEOF=external_define("gmbinfile.dll","GMBINIsEOF",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINIsEOF")

//export double GMBINSeekBOF(double hf)
global.dll_GMBINSeekBOF=external_define("gmbinfile.dll","GMBINSeekBOF",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINSeekBOF")

//export double GMBINSeekEOF(double hf)
global.dll_GMBINSeekEOF=external_define("gmbinfile.dll","GMBINSeekEOF",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINSeekEOF")

//export double GMBINSeekFromCurrent(double hf, double pos)
global.dll_GMBINSeekFromCurrent=external_define("gmbinfile.dll","GMBINSeekFromCurrent",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINSeekFromCurrent")

//export double GMBINSetPosition(double hf, double pos)
global.dll_GMBINSetPosition=external_define("gmbinfile.dll","GMBINSetPosition",dll_stdcall,ty_real,2,ty_real,ty_real);
if(WTF) show_message("Defined: GMBINSetPosition")

//export double GMBINGetPosition(double hf, double pos)
global.dll_GMBINGetPosition=external_define("gmbinfile.dll","GMBINGetPosition",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINGetPosition")

//export double GMBINIsError(double hf)
global.dll_GMBINIsError=external_define("gmbinfile.dll","GMBINIsError",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINIsError")

//export double GMBINGetActualSize(double hf)
global.dll_GMBINGetActualSize=external_define("gmbinfile.dll","GMBINGetActualSize",dll_stdcall,ty_real,1,ty_real);
if(WTF) show_message("Defined: GMBINGetActualSize")

//export double GMBINExtractFile(double hf, LPCSTR filename)
global.dll_GMBINExtractFile=external_define("gmbinfile.dll","GMBINExtractFile",dll_stdcall,ty_real,2,ty_real,ty_string);
if(WTF) show_message("Defined: GMBINExtractFile")


//export double GMBINInsertFile(double hf, LPCSTR filename)
global.dll_GMBINInsertFile=external_define("gmbinfile.dll","GMBINInsertFile",dll_stdcall,ty_real,2,ty_real,ty_string);
if(WTF) show_message("Defined: GMBINInsertFile")

#define GMBINUnload
//free the library
//Call this when the game end
external_free("gmbinfile.dll");
#define GMBINOpenFileRead
//export double GMBINOpenFileRead(LPCSTR fname)
//Open a binary file for reading, returning the file handle or 0 if failed to open

//the file must exist

return external_call(global.dll_GMBINOpenFileRead,argument0);




#define GMBINOpenFileReadPW
//export double GMBINOpenFileReadPW(LPCSTR fname, LPCSTR password)
//Open a binary file for reading, returning the file handle or 0 if failed to open

//the file must exist

//password specifies the password to used for decryption
//the password is verified against the file password
//the encryption is SHA1
//http://en.wikipedia.org/wiki/SHA1

//Seeks and positions will take into account the password hash in the file
//so seeks and setting/getting position and should use/return the same
//values as non encrypted files.
//If the file is not encrypted, the password is ignored
//YOU CANNOT open multiple files at the same time when a password is used
//the "use-password" flag is reset when the file is closed
return external_call(global.dll_GMBINOpenFileReadPW,argument0,argument1);




#define GMBINOpenFileWrite
//export double GMBINOpenFileWrite(LPCSTR fname)
//Open a binary file for writing, returning the file handle or 0 if failed to open

//if the file exists, it will be cleared

return external_call(global.dll_GMBINOpenFileWrite,argument0);




#define GMBINOpenFileWritePW
//export double GMBINOpenFileWritePW(LPCSTR fname, LPCSTR password)
//Open a binary file for writeing, returning the file handle or 0 if failed to open

//if the file exists, it will be cleared

//password specifies the password to used for encryption
//the password hash is saved to the top of the file
//the encryption is SHA1
//http://en.wikipedia.org/wiki/SHA1

//Seeks and positions will take into account the password hash in the file
//so seeks and setting/getting position and should use/return the same
//values as non encrypted files.
//If the file is not encrypted, the password is ignored
//YOU CANNOT open multiple files at the same time when a password is used
//the "use-password" flag is reset when the file is closed
return external_call(global.dll_GMBINOpenFileWritePW,argument0,argument1);




#define GMBINOpenFileReadWrite
//export double GMBINOpenFileReadWrite(LPCSTR fname)
//Open a binary file for reading and writing, retuning the file handle or 0 if failed to open
//this mode is for modifying file, by seeking a position or for appending, seeking to the end
//before writing

//the file must exist

return external_call(global.dll_GMBINOpenFileReadWrite,argument0);




#define GMBINOpenFileReadWritePW
//export double GMBINOpenFileReadWritePW(LPCSTR fname, LPCSTR password)
//Open a binary file for reading and writing, returning the file handle or 0 if failed to open
//this mode is for modifying file, by seeking a position or for appending, seeking to the end
//before writing

//the file must exist

//password specifies the password to used for decryption/encryption
//the password is verified against the file password
//the encryption is SHA1
//http://en.wikipedia.org/wiki/SHA1

//Seeks and positions will take into account the password hash in the file
//so seeks and setting/getting position and should use/return the same
//values as non encrypted files.
//If the file is not encrypted, the password is ignored
//YOU CANNOT open multiple files at the same time when a password is used
//the "use-password" flag is reset when the file is closed

return external_call(global.dll_GMBINOpenFileReadWritePW,argument0, argument1);




#define GMBINCloseFile
//export double GMBINCloseFile(double hf)
//Closes the binary file

return external_call(global.dll_GMBINCloseFile,argument0);




#define GMBINReadShortBE
//export double GMBINReadShortBE(double hf)
//Read a short from the file with big endian
//http://en.wikipedia.org/wiki/Endianness

//A short is 16 bits (2 bytes) integer
//values -32768 to 32767
return external_call(global.dll_GMBINReadShortBE,argument0);

#define GMBINReadUShortBE
//export double GMBINReadUShortBE(double hf)
//Read a unsigned short from the file with big endian
//http://en.wikipedia.org/wiki/Endianness

//A unsigned short is 16 bits (2 bytes) integer
//values 0 to 65535
return external_call(global.dll_GMBINReadUShortBE,argument0);

#define GMBINReadIntBE
//export double GMBINReadIntBE(double hf)
//Read a int from the file with big endian
//http://en.wikipedia.org/wiki/Endianness

//A int is 32 bits (4 bytes) integer
//values -2147483648 to 2147483647
return external_call(global.dll_GMBINReadIntBE,argument0);

#define GMBINReadUIntBE
//export double GMBINReadUIntBE(double hf)
//Read a unsigned int from the file with big endian
//http://en.wikipedia.org/wiki/Endianness

//A unsigned int is 32 bits (4 bytes) integer
//values 0 to 4294967295
return external_call(global.dll_GMBINReadUIntBE,argument0);

#define GMBINReadLongBE
//export double GMBINReadLongBE(double hf)
//Read a long from the file with big endian
//http://en.wikipedia.org/wiki/Endianness
show_message("Due to the nature of a GM real, this function GMBINReadLong is useless. Use GMBINReadDouble instead.")
//A long is 64 bits (8 bytes) integer
//values -9223372036854775808 to 9223372036854775807
return external_call(global.dll_GMBINReadLongBE,argument0);

#define GMBINReadULongBE
//export double GMBINReadULongBE(double hf)
//Read a unsigned long from the file with big endian
//http://en.wikipedia.org/wiki/Endianness
show_message("Due to the nature of a GM real, this function GMBINReadULong is useless. Use GMBINReadDouble instead.")

//A unsigned long is 64 bits (8 bytes) integer
//values 0 to 18446744073709551615
return external_call(global.dll_GMBINReadULongBE,argument0);

#define GMBINReadFloatBE
//export double GMBINReadFloat(double hf)
//Read a float from the file with big endian
//http://en.wikipedia.org/wiki/Endianness

//A float is 32 bits (4 bytes) floating point (real number)
//values http://en.wikipedia.org/wiki/IEEE_754-1985#Single-precision_32-bit
return external_call(global.dll_GMBINReadFloatBE,argument0);

#define GMBINReadDoubleBE
//export double GMBINReadDoubleBE(double hf)
//Read a double from the file with big endian
//http://en.wikipedia.org/wiki/Endianness

//A double is 64 bits (8 bytes) floating point (real number) (GM's real)
//values http://en.wikipedia.org/wiki/IEEE_754-1985#Double-precision_64_bit
return external_call(global.dll_GMBINReadDoubleBE,argument0);

#define GMBINReadByte
//export double GMBINReadByte(double hf)
//Read a byte from the file

//a byte is 8 bits integer
//value -128 to 128
return external_call(global.dll_GMBINReadByte,argument0);

#define GMBINReadUByte
//export double GMBINReadUByte(double hf)
//Reads a unsigned byte from the file

//a unsigned byte is 8 bits integer
//value 0 to 255
return external_call(global.dll_GMBINReadUByte,argument0);

#define GMBINReadByteChar
//GMBINReadByteChar(double hf)
//Read a byte from the file, returning a one character string instead of a real

//a byte is 8 bits in the file 
//value char(0) to chr(255)
return chr(GMBINReadUByte(argument0));

#define GMBINReadShort
//export double GMBINReadShort(double hf)
//Read a short from the file

//A short is 16 bits (2 bytes) integer
//values -32768 to 32767
return external_call(global.dll_GMBINReadShort,argument0);

#define GMBINReadUShort
//export double GMBINReadUShort(double hf)
//Read a unsigned short from the file

//A unsigned short is 16 bits (2 bytes) integer
//values 0 to 65535
return external_call(global.dll_GMBINReadUShort,argument0);

#define GMBINReadInt
//export double GMBINReadInt(double hf)
//Read a int from the file

//A int is 32 bits (4 bytes) integer
//values -2147483648 to 2147483647
return external_call(global.dll_GMBINReadInt,argument0);

#define GMBINReadUInt
//export double GMBINReadUInt(double hf)
//Read a unsigned int from the file

//A unsigned int is 32 bits (4 bytes) integer
//values 0 to 4294967295
return external_call(global.dll_GMBINReadUInt,argument0);

#define GMBINReadLong
//export double GMBINReadLong(double hf)
//Read a long from the file
show_message("Due to the nature of a GM real, this function GMBINReadLong is useless. Use GMBINReadDouble instead.")
//A long is 64 bits (8 bytes) integer
//values -9223372036854775808 to 9223372036854775807
return external_call(global.dll_GMBINReadLong,argument0);

#define GMBINReadULong
//export double GMBINReadULong(double hf)
//Read a unsigned long from the file
show_message("Due to the nature of a GM real, this function GMBINReadULong is useless. Use GMBINReadDouble instead.")

//A unsigned long is 64 bits (8 bytes) integer
//values 0 to 18446744073709551615
return external_call(global.dll_GMBINReadULong,argument0);

#define GMBINReadFloat
//export double GMBINReadFloat(double hf)
//Read a float from the file

//A float is 32 bits (4 bytes) floating point (real number)
//values http://en.wikipedia.org/wiki/IEEE_754-1985#Single-precision_32-bit
return external_call(global.dll_GMBINReadFloat,argument0);

#define GMBINReadDouble
//export double GMBINReadDouble(double hf)
//Read a double from the file

//A double is 64 bits (8 bytes) floating point (real number) (GM's real)
//values http://en.wikipedia.org/wiki/IEEE_754-1985#Double-precision_64_bit
return external_call(global.dll_GMBINReadDouble,argument0);

#define GMBINReadReal
//export double GMBINReadReal(double hf)
//Read a GM Real from the file

//a real is 8 bytes in the file
return GMBINReadDouble(argument0)

#define GMBINReadBuffer
//export double GMBINReadBuffer(double hf, size)
//Read a buffer from the file
//each character is 1 byte in the file
if(argument1 <= 0) return ""; //not passing a right size, silent ignore
//alocate the buffer
var buff; buff = string_repeat(chr(0), argument1)
//fill it
external_call(global.dll_GMBINReadBuffer,argument0, buff);
//return it
return buff;

#define GMBINReadString
//export double GMBINReadString(double hf)
//Read a string from the file
//As string has a little header, a int, specifying the string length
//<int><stringbuffer>
//read the size and ask GMBINReadBuffer to read
return GMBINReadBuffer(argument0,max(0,GMBINReadInt(argument0)));

#define GMBINExtractFile
//export double GMBINExtractFile(double hf, LPCSTR filename)
//Extracts a file added with GMBINInsertFile from the file 
//Filename is the target file. It will be overwriten if it exists
return external_call(global.dll_GMBINExtractFile,argument0, argument1);




#define GMBINWriteShortBE
//export double GMBINWriteShortBE(double hf, integer )
//Write a short or unsigned short to the file (the write does not care) with big endian
//http://en.wikipedia.org/wiki/Endianness

//a short is 16 bits integer
//see read eqivalent for range
//see GMBINWriteByte for signed/unsigned behaviour where the 
//flippage occurs at the maximum signed value of the data type
return external_call(global.dll_GMBINWriteShortBE,argument0,argument1);

#define GMBINWriteIntBE
//export double GMBINWriteIntBE(double hf, integer )
//Write a int or unsigned int to the file (the write does not care) with big endian
//http://en.wikipedia.org/wiki/Endianness

//a int is 32 bits integer
//see read eqivalent for range
//see GMBINWriteByte for signed/unsigned behaviour where the 
//flippage occurs at the maximum signed value of the data type

return external_call(global.dll_GMBINWriteIntBE,argument0,argument1);

#define GMBINWriteLongBE
//export double GMBINWriteLongBE(double hf, long integer )
//Write a long or unsigned long to the file (the write does not care) with big indian
show_message("Due to the nature of a GM real, this function GMBINWriteLong is useless. Use GMBINWrite Double instead.")

//a long is 64 bits integer
//see read eqivalent for range
//see GMBINWriteByte for signed/unsigned behaviour where the 
//flippage occurs at the maximum signed value of the data type

return external_call(global.dll_GMBINWriteLongBE,argument0,argument1);

#define GMBINWriteFloatBE
//export double GMBINWriteFloatBE(double hf, float )
//Write a float to the file (floats are always signed) with big endian
//http://en.wikipedia.org/wiki/Endianness

//a float is 32 bits floating point
//see read eqivalent for range
return external_call(global.dll_GMBINWriteFloatBE,argument0,argument1);

#define GMBINWriteDoubleBE
//export double GMBINWriteDoubleBE(double hf, double )
//Write a double to the file (doubles are always signed) with big endian
//http://en.wikipedia.org/wiki/Endianness

//a double is 32 bits floating point
//see read eqivalent for range

return external_call(global.dll_GMBINWriteDoubleBE,argument0,argument1);

#define GMBINWriteByte
//export double GMBINWriteByte(double hf, double byte)
//Write a byte or unsigned byte to the file (the write does not care)

//a byte is 8 bits integer
//value -128 to 127 when signed or 0-255 when unsigned, 
//the save does not care about the sign
//but if you write a value > 127 and read as unsigned (Normal Read), 
//the result will be a negative number
//if you write a negative and read as unsigned (The U version), 
//the result will be a positive number > 127
//Confusing but normal behaviour as the leftmost bit is used
//to either specify if the number is negative (if signed)
//or used as part of the actual numerical value (if unsigned)
//http://en.wikipedia.org/wiki/Sign-magnitude#Two.27s_complement
return external_call(global.dll_GMBINWriteByte,argument0,argument1);

#define GMBINWriteByteChar
//GMBINWriteByteChar(double hf, one character string byte)
//Writes a byte to the file, passing a one byte string instead of a real

//a byte is 1 byte in the file
return GMBINWriteByte(argument0,ord(argument1));

#define GMBINWriteShort
//export double GMBINWriteShort(double hf, integer )
//Write a short or unsigned short to the file (the write does not care)

//a short is 16 bits integer
//see read eqivalent for range
//see GMBINWriteByte for signed/unsigned behaviour where the 
//flippage occurs at the maximum signed value of the data type
return external_call(global.dll_GMBINWriteShort,argument0,argument1);

#define GMBINWriteInt
//export double GMBINWriteInt(double hf, integer )
//Write a int or unsigned int to the file (the write does not care)

//a int is 32 bits integer
//see read eqivalent for range
//see GMBINWriteByte for signed/unsigned behaviour where the 
//flippage occurs at the maximum signed value of the data type

return external_call(global.dll_GMBINWriteInt,argument0,argument1);

#define GMBINWriteLong
//export double GMBINWriteLong(double hf, long integer )
//Write a long or unsigned long to the file (the write does not care)
show_message("Due to the nature of a GM real, this function GMBINWriteLong is useless. Use GMBINWrite Double instead.")

//a long is 64 bits integer
//see read eqivalent for range
//see GMBINWriteByte for signed/unsigned behaviour where the 
//flippage occurs at the maximum signed value of the data type

return external_call(global.dll_GMBINWriteLong,argument0,argument1);

#define GMBINWriteFloat
//export double GMBINWriteFloat(double hf, float )
//Write a float to the file (floats are always signed)

//a float is 32 bits floating point
//see read eqivalent for range
return external_call(global.dll_GMBINWriteFloat,argument0,argument1);

#define GMBINWriteDouble
//export double GMBINWriteDouble(double hf, double )
//Write a double to the file (doubles are always signed)

//a double is 32 bits floating point
//see read eqivalent for range

return external_call(global.dll_GMBINWriteDouble,argument0,argument1);

#define GMBINWriteReal
//export double GMBINWriteReal(double hf, double real)
//Writes a GM real to the file

//a real is 8 bytes in the file
return GMBINWriteDouble(argument0,argument1);

#define GMBINWriteBuffer
//export double GMBINWriteBuffer(double hf, LPCSTR buff)
//Writes a buffer to the file

//each character is 1 byte in the file
return external_call(global.dll_GMBINWriteBuffer,argument0,argument1);

#define GMBINWriteString
//export double GMBINWriteString(double hf, LPCSTR string)
//Writes a buffer to the file, but this time the length of the string
//is written before the string as a int
//<int><buffer>
//4 bytes header + each character is 1 byte in the file
return external_call(global.dll_GMBINWriteString,argument0,argument1);

#define GMBINInsertFile
//export double GMBINInsertFile(double hf, LPCSTR filename)
//Inserts a file in your file, useful for packaging
//Filename is the source file to insert in the binary file
//TIP you should write the name of the file in the binary file if is important
//  to recreate the same file. Simply read the name and pass it on to the fuction
//  you are in charge of the path though...
//return 0 on error;
return external_call(global.dll_GMBINInsertFile,argument0, argument1);




#define GMBINIsEOF
//export double GMBINIsEOF(double hf)
//is end of file?

return external_call(global.dll_GMBINIsEOF,argument0);

#define GMBINIsError
//export double GMBINIsError(double hf)
//is there an error with the last action?
//a positive value is the error code (error = ferror(hf)) which I can't find
//documented anywhere
//0 is no error
return external_call(global.dll_GMBINIsError,argument0);

#define GMBINGetPosition
//export double GMBINGetPosition(double hf)
//get the position we are at in the file relative to the top
//or relative to the data section of the file (after the SHAW hash header)
return external_call(global.dll_GMBINGetPosition,argument0);

#define GMBINGetActualSize
//export double GMBINGetActualSize(double hf)
//get the size of the file, excluding the encryption header if encrypted

return external_call(global.dll_GMBINGetActualSize,argument0);

#define GMBINSeekBOF
//export double GMBINSeekBOF(double hf)
//Seek to the begining of the file. position 0, or the beginning of the data if encrypted

return external_call(global.dll_GMBINSeekBOF,argument0);

#define GMBINSeekEOF
//export double GMBINSeekEOF(double hf)
//Seek to the end of the file. Or the end of the data if encrypted

return external_call(global.dll_GMBINSeekEOF,argument0);

#define GMBINSeekFromCurrent
//export double GMBINSeekFromCurrent(double hf, double pos)
//Seek to pos relative to the current position. -pos to move back, +pos to move foreward
//WARNING YOU CAN SCREW UP THE ENCRYPTION HEADER using this function, by rewinding (-pos)
//from the top of the data and writing to the header...

return external_call(global.dll_GMBINSeekFromCurrent,argument0, argument1);

#define GMBINSetPosition
//export double GMBINSetPosition(double hf, double pos)
//Seek to pos relative to begining of the file or the top of the data section when encrypted
//WARNING YOU CAN SCREW UP THE ENCRYPTION HEADER using this function, by rewinding (-pos)
//from the top of the data and writing to the header...
//NOTE, I have not tested if you can actually use -pos for this function

return external_call(global.dll_GMBINSetPosition,argument0, argument1);

#define GMBINPackBits
//byte = GMBINPackBits(arg0 up to arg7)

//This will pack bits in a byte
//Packing up to 8 true/fasle or 1/0 values in a byte

//This can save a large amount of space in a file

//The values MUST be 1 or 0, I do not validate the arguments

var b;

b = argument7 << 7
b+= argument6 << 6
b+= argument5 << 5
b+= argument4 << 4
b+= argument3 << 3
b+= argument2 << 2
b+= argument1 << 1
b+= argument0 

#define GMBINUnpackBit
//true/false = GMBINUnpackBit(Byte, bit(0 to 7))

//This will unpack bits from a byte, on bit at a time

//This can save a large amount of space in a file
//You must have read the data using ReadUByte

//bit can be 0 to 7, matching the argument of the PackBits script

return (argument0 >> argument1) & 1


