#include "Util.h"

bool startsWith( const std::string & str, const std::string & prefix ) {
	return (str.length() >= prefix.length() &&
		str.substr( 0, prefix.length() ) == prefix);
}

bool endsWith( const std::string & str, const std::string & suffix ) {
	return (str.length() >= suffix.length() &&
		str.substr( str.length() - suffix.length(), suffix.length() ) == suffix);
}