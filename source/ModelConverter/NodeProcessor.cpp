#include "NodeProcessor.h"

#include "VertexExtractor.h"
#include "SkeletonExtractor.h"
#include "Mesh.h"
#include "FileWriter.h"

void getBounds( FbxNode * node, float * minBound, float * maxBound ) {
	for (int i = 0; i < node->GetChildCount(); ++i) {
		FbxNode * child = node->GetChild( i );
		if (child->GetName() == (std::string( node->GetName() ) + BOUNDS_SUFFIX)) {
			FbxGeometry * bound = child->GetGeometry();
			if (bound == NULL)
				continue;
			bound->ComputeBBox();
			for (int t = 0; t < 3; ++t) {
				minBound[t] = (float)bound->BBoxMin.Get()[t];
				maxBound[t] = (float)bound->BBoxMax.Get()[t];
			}
			return;
		}
	}

	FbxGeometry * bound = node->GetGeometry();
	if (bound == NULL)
		throw std::runtime_error( "Unable to get bounds for " + std::string( node->GetName() ) );
	bound->ComputeBBox();
	for (int t = 0; t < 3; ++t) {
		minBound[t] = (float)bound->BBoxMin.Get()[t];
		maxBound[t] = (float)bound->BBoxMax.Get()[t];
	}

	std::cout << "  WARNING: No bounds found for " << node->GetName() << "; using default bounds\n";
}

void processNode( FbxNode * node, const VertexDescriptions & vertexDescriptions ) {
	try {
		std::string name = node->GetName();

		if (endsWith( name, BOUNDS_SUFFIX ))
			// don't process bounds as meshes
			return;

		if (startsWith( name, STATIC_MESH_PREFIX )) {
			if (!vertexDescriptions.staticVertexDescriptionProvided)
				throw std::runtime_error( "No static vertex description provided" );

			std::cout << "  Converting static mesh " << name << '\n';
			FbxMesh * mesh = node->GetMesh();
			if (mesh == NULL)
				throw std::runtime_error( "Node " + name + " is not a static mesh" );

			std::vector <Vertex> vertices = extractVertices( mesh, std::map <FbxNode*, uint>(), vertexDescriptions, true );
			Mesh indexedMesh( vertices, vertexDescriptions, true );

			float minBound[3];
			float maxBound[3];
			getBounds( node, minBound, maxBound );

			writeMesh( name.substr( strlen( STATIC_MESH_PREFIX ) ) + MESH_EXTENSION, indexedMesh, vertexDescriptions, true, minBound, maxBound );

			std::cout << "  Converted static mesh " << name << '\n';
		} else if (startsWith( name, RIGGED_MESH_PREFIX )) {
			// don't actually do anything here, just make sure there is a skeleton attached (parent)
			FbxNode * parent = node->GetParent();
			if (parent == NULL || !startsWith( parent->GetName(), SKELETON_ROOT_PREFIX ))
				throw std::runtime_error( "Rigged mesh " + name + " is not the child of a skeleton" );
		} else if (startsWith( name, SKELETON_ROOT_PREFIX )) {
			// build the bone index map
			std::map <FbxNode*, uint> boneIndexMap = buildBoneIndexMap( node, SKELETON_BONE_PREFIX );

			std::vector <Bone> skeleton = extractSkeleton( node, boneIndexMap, SKELETON_BONE_PREFIX );

			writeSkeleton( name.substr( strlen( SKELETON_ROOT_PREFIX ) ) + SKELETON_EXTENSION, skeleton );

			std::cout << "  Converted skeleton " << name << '\n';

			// construct all the rigged meshes and animations
			for (int i = 0; i < node->GetChildCount(); ++i) {
				FbxNode * childNode = node->GetChild( i );
				std::string childName = childNode->GetName();
				try {
					if (startsWith( childName, RIGGED_MESH_PREFIX )) {
						if (!vertexDescriptions.riggedVertexDescriptionProvided)
							throw std::runtime_error( "No rigged vertex description provided" );

						std::cout << "    Converting rigged mesh " << childName << '\n';
						FbxMesh * mesh = childNode->GetMesh();
						if (mesh == NULL)
							throw std::runtime_error( "Node " + childName + " is not a rigged mesh" );

						std::vector <Vertex> vertices = extractVertices( mesh, boneIndexMap, vertexDescriptions, false );
						Mesh indexedMesh( vertices, vertexDescriptions, false );

						float minBound[3];
						float maxBound[3];
						getBounds( childNode, minBound, maxBound );

						writeMesh( childName.substr( strlen( RIGGED_MESH_PREFIX ) ) + MESH_EXTENSION, indexedMesh, vertexDescriptions, false, minBound, maxBound );

						std::cout << "    Converted rigged mesh " << childName << '\n';
					} else if (startsWith( childName, ANIMATION_PREFIX )) {
						Animation anim = extractAnimation( childNode, &boneIndexMap );

						writeAnimation( childName.substr( strlen( ANIMATION_PREFIX ) ) + ANIMATION_EXTENSION, anim );

						std::cout << "    Converted animation " << childName << '\n';
					}
				} catch (const std::exception & e) {
					std::cout << "    ERROR: " << e.what() << '\n';
					std::cout << "    Failed to process node " << childNode->GetName() << '\n';
				}
			}
		}
	} catch (const std::exception & e) {
		std::cout << "  ERROR: " << e.what() << '\n';
		std::cout << "  Failed to process node " << node->GetName() << '\n';
	}
}