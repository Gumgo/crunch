#ifndef FBX_DEFINED
#define FBX_DEFINED

#include "Fbx.h"
#include "VertexDescription.h"
#include <string>
#include <iostream>
#include <stack>

class Scene {
	FbxManager * manager;
	FbxScene * scene;

public:
	Scene( FbxManager * mgr, const std::string & fname );
	~Scene();

	typedef void (*TraverseFunction)( FbxNode *, const VertexDescriptions & );
	void traverse( TraverseFunction func, const VertexDescriptions & vertexDescriptions );
};

#endif