#ifndef FILEWRITER_DEFINED
#define FILEWRITER_DEFINED

#include <Crunch/Types.h>
#include "Mesh.h"
#include "SkeletonExtractor.h"
#include "VertexExtractor.h"
#include "VertexDescription.h"
#include <Crunch/KeyValueWriter.h>

static const char * MESH_EXTENSION = ".mesh";
static const char * SKELETON_EXTENSION = ".skel";
static const char * ANIMATION_EXTENSION = ".anim";

void writeMesh( const std::string & fname, const Mesh & mesh, const VertexDescriptions & desc, bool staticMesh,
	const float * minBound, const float * maxBound );

void writeSkeleton( const std::string & fname, const std::vector <Bone> & bones );

void writeAnimation( const std::string & fname, const Animation & anim );

#endif