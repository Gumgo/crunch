#include "Scene.h"
#include "NodeProcessor.h"
#include "VertexDescription.h"
#include "Util.h"

int main( int argc, char ** argv ) {
	// first find the vertex description
	VertexDescriptions vd;
	try {
		bool vertexDescriptionsFound = false;
		for (int i = 1; i < argc; ++i) {
			if (endsWith( argv[i], ".vd" )) {
				if (vertexDescriptionsFound)
					throw std::runtime_error( "Multiple vertex description files specified" );
				vertexDescriptionsFound = true;
				vd = getVertexDescriptions( argv[i] );
			}
		}
	} catch (const std::exception & e) {
		std::cout << e.what() << '\n';
		std::cout << "Failed to obtain vertex descriptions\n";
		return 0;
	}

	FbxManager * manager = FbxManager::Create();

	for (int i = 1; i < argc; ++i) {
		try {
			if (endsWith( argv[i], ".vd" ))
				continue;
			std::cout << "Processing scene " << argv[i] << '\n';
			Scene scene( manager, argv[i] );
			scene.traverse( &processNode, vd );
		} catch (const std::exception & e) {
			std::cout << e.what() << '\n';
			std::cout << "Failed to process scene " << argv[i] << '\n';
		}
	}

	manager->Destroy();
	return 0;
}