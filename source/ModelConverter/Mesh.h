#ifndef MESH_DEFINED
#define MESH_DEFINED

#include <Crunch/Types.h>
#include "VertexExtractor.h"
#include "VertexDescription.h"
#include <vector>
#include <map>

class Mesh {
public:
	struct Triangle {
		uint vertices[3];
	};

private:
	std::vector <Vertex> vertices;
	std::vector <Triangle> triangles;

	bool hasPositions;
	bool hasTexCoords;
	bool hasNormals;
	bool hasTangents;
	bool hasBitangents;
	bool hasBoneIndices;
	bool hasBoneWeights;
	uint boneCount;

public:
	Mesh( const std::vector <Vertex> & unindexedVertices, const VertexDescriptions & vd, bool staticMesh );

	const std::vector <Vertex> & getVertices() const;
	const std::vector <Triangle> & getTriangles() const;

	bool usesTexCoords() const;
	bool usesNormals() const;
	bool usesTangents() const;
	bool usesBitangents() const;
	bool usesBoneIndices() const;
	bool usesBoneWeights() const;
	uint getBoneCount() const;
};

#endif