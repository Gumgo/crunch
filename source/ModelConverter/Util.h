#ifndef UTIL_DEFINED
#define UTIL_DEFINED

#include <Crunch/Types.h>
#include <string>

bool startsWith( const std::string & str, const std::string & prefix );
bool endsWith( const std::string & str, const std::string & suffix );

#endif