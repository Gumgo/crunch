#include "SkeletonExtractor.h"

std::map <FbxNode*, uint> buildBoneIndexMap( FbxNode * root, const char * bonePrefix ) {
	std::stack <FbxNode*> nodeStack;
	std::map <FbxNode*, uint> boneIndexMap;

	// start with the root node (don't index it though - it's simply a marker, not a real bone)
	nodeStack.push( root );
	while (!nodeStack.empty()) {
		FbxNode * node = nodeStack.top();
		nodeStack.pop();

		// index all the children
		for (int i = 0; i < node->GetChildCount(); ++i) {
			FbxNode * child = node->GetChild( i );
			// if this isn't a bone, skip it
			if (!startsWith( child->GetName(), std::string( bonePrefix ) ))
				continue;
			boneIndexMap.insert( std::make_pair( child, (uint)boneIndexMap.size() ) );
			nodeStack.push( child );
		}
	}

	return boneIndexMap;
}

std::vector <Bone> extractSkeleton( FbxNode * root, const std::map <FbxNode*, uint> & boneIndexMap, const char * bonePrefix ) {
	std::vector <Bone> skeleton;
	skeleton.resize( boneIndexMap.size() );

	FbxScene * scene = root->GetScene();

	std::map <FbxNode*, uint>::const_iterator it;
	for (it = boneIndexMap.begin(); it != boneIndexMap.end(); ++it) {
		FbxNode * node = it->first;
		uint id = it->second;

		FbxNode * parent = node->GetParent();
		if (parent == root)
			// no parent - set ID to self to detect this case
			skeleton[id].parent = id;
		else {
			std::map <FbxNode*, uint>::const_iterator parentNode = boneIndexMap.find( parent );
			if (parentNode == boneIndexMap.end())
				throw std::runtime_error( "Invalid parent bone" );
			skeleton[id].parent = parentNode->second;
		}
		skeleton[id].name = std::string( node->GetName() ).substr( strlen( bonePrefix ) );

		// compute the inverse bind pose; get the bone's world space transformation and invert it
		FbxAMatrix bindPose = scene->GetEvaluator()->GetNodeGlobalTransform( node );
		FbxAMatrix invBindPose = bindPose.Inverse();

		for (int r = 0; r < 3; ++r) {
			for (int c = 0; c < 4; ++c)
				// transformations are composed in the reverse order, so we switch c and r
				skeleton[id].inverseBind[r*4+c] = (float)invBindPose.Get( c, r );
		}
	}

	return skeleton;
}

Animation extractAnimation( FbxNode * animNode, const std::map <FbxNode*, uint> * boneIndexMap ) {
	Animation a;

	// get the animation range in frames
	FbxProperty firstFrameProp = animNode->FindProperty( "firstFrame" );
	FbxProperty lastFrameProp = animNode->FindProperty( "lastFrame" );
	FbxProperty fadeInFramesProp = animNode->FindProperty( "fadeInFrames" );
	FbxProperty fadeOutFramesProp = animNode->FindProperty( "fadeOutFrames" );
	if (!firstFrameProp.IsValid() || !lastFrameProp.IsValid() || !fadeInFramesProp.IsValid() || !fadeOutFramesProp.IsValid())
		throw std::runtime_error( "First frame, last frame, or fade frames not found" );
	FbxDataType firstFrameType = firstFrameProp.GetPropertyDataType();
	FbxDataType lastFrameType = lastFrameProp.GetPropertyDataType();
	FbxDataType fadeInFramesType = fadeInFramesProp.GetPropertyDataType();
	FbxDataType fadeOutFramesType = fadeOutFramesProp.GetPropertyDataType();
	if (firstFrameType.GetType() != eFbxInt || lastFrameType.GetType() != eFbxInt ||
		fadeInFramesType.GetType() != eFbxInt || fadeOutFramesType.GetType() != eFbxInt)
		throw std::runtime_error( "First frame, last frame, or fade frames is not an integer" );
	FbxInt firstFrame = firstFrameProp.Get <FbxInt>();
	FbxInt lastFrame = lastFrameProp.Get <FbxInt>();
	FbxInt fadeInFrames = fadeInFramesProp.Get <FbxInt>();
	FbxInt fadeOutFrames = fadeOutFramesProp.Get <FbxInt>();
	if (firstFrame >= lastFrame)
		throw std::runtime_error( "Invalid animation range" );
	if (fadeInFrames < 0 || fadeOutFrames < 0)
		throw std::runtime_error( "Invalid animation fade frames" );

	// get the animation
	// make sure we have a stack
	if (animNode->GetScene()->GetSrcObjectCount( FBX_TYPE( FbxAnimStack ) ) == 0)
		throw std::runtime_error( "No animation stack" );
	FbxAnimStack * animStack = animNode->GetScene()->GetSrcObject( FBX_TYPE( FbxAnimStack ) );
	// make sure we have a layer
	if (animStack->GetMemberCount( FBX_TYPE( FbxAnimLayer ) ) == 0)
		throw std::runtime_error( "No animation layer" );
	// get layer 0
	FbxAnimLayer * animLayer = animStack->GetMember( FBX_TYPE( FbxAnimLayer ) );

	// convert to float time in seconds
	FbxTime startTime, endTime;
	FbxTime fadeInTime, fadeOutTime;
	startTime.SetFrame( firstFrame );
	endTime.SetFrame( lastFrame );
	fadeInTime.SetFrame( fadeInFrames );
	fadeOutTime.SetFrame( fadeOutFrames );
	float startTimeSeconds = (float)startTime.GetSecondDouble();
	float endTimeSeconds = (float)endTime.GetSecondDouble();

	a.length = endTimeSeconds - startTimeSeconds;
	a.fadeInTime = (float)fadeInTime.GetSecondDouble();
	a.fadeOutTime = (float)fadeOutTime.GetSecondDouble();

	// iterate over each bone
	for (std::map <FbxNode*, uint>::const_iterator it = boneIndexMap->begin(); it != boneIndexMap->end(); ++it) {
		FbxNode * bone = it->first;
		FbxAnimCurveNode * translations = bone->LclTranslation.GetCurveNode( animLayer );
		FbxAnimCurveNode * rotations = bone->LclRotation.GetCurveNode( animLayer );
		// set the rotation curve to use quaternion interpolation
		rotations->SetQuaternionInterpolation( eQuatInterpSlerp );
		// if no translations or rotations, skip
		if ((translations == NULL || !translations->IsAnimated()) && (rotations == NULL || !rotations->IsAnimated()))
			continue;

		// create the set of keyframes - only include frames in animation range, inclusive
		std::set <FbxTime> keyframeTimes;
		FbxAnimCurveNode * curveNodes[2] = { translations, rotations };
		for (size_t n = 0; n < 2; ++n) {
			if (curveNodes[n] == NULL || !curveNodes[n]->IsAnimated())
				continue;

			for (uint c = 0; c < curveNodes[n]->GetChannelsCount(); ++c) {
				FbxAnimCurve * curve = curveNodes[n]->GetCurve( c );
				// go through each keyframe
				int kfCount = curve->KeyGetCount();
				for (int kf = 0; kf < kfCount; ++kf) {
					FbxAnimCurveKey key = curve->KeyGet( kf );
					FbxTime keyTime = key.GetTime();
					float timeSeconds = (float)keyTime.GetSecondDouble();
					// check if it's in the animation range
					if (timeSeconds >= startTimeSeconds && timeSeconds <= endTimeSeconds)
						// set automatically eliminates duplicates
						keyframeTimes.insert( keyTime );
				}
			}
		}
		if (keyframeTimes.empty())
			// if no keyframes, don't include this bone in the animation
			continue;

		// add the bone and set its ID
		a.animatedBones.push_back( AnimatedBone() );
		AnimatedBone & ab = a.animatedBones.back();
		ab.boneId = it->second;

		// internally, the time value is stored as a longlong in increments of 1/46,186,158,000 second
		// occasionally, what appears to be the same keyframe differs a tiny bit
		// to avoid this, ignore keyframes which are too close (or equal) to the previous one

		// now we have a set of times to add keyframes at
		for (std::set <FbxTime>::iterator kfIt = keyframeTimes.begin(); kfIt != keyframeTimes.end(); ++kfIt) {
			Keyframe kf;

			// assign the keyframe's time relative to the start of the animation
			float kfTime = (float)kfIt->GetSecondDouble();
			kf.time = kfTime - startTimeSeconds;

			if (!ab.keyframes.empty()) {
				float timeDiff = kf.time - ab.keyframes.back().time;
				static const float DIFFERENCE_THRESHOLD = 0.000001f;
				if (timeDiff < DIFFERENCE_THRESHOLD)
					continue;
			}

			// get the local transformation of the bone
			FbxAMatrix localTransform = it->first->GetScene()->GetEvaluator()->GetNodeLocalTransform( it->first, *kfIt );
			FbxVector4 translation = localTransform.GetT();
			FbxQuaternion rotation = localTransform.GetQ();

			kf.translation[0] = (float)translation[0];
			kf.translation[1] = (float)translation[1];
			kf.translation[2] = (float)translation[2];

			// Crunch order is (r,i,j,k), FBX order is (x,y,z,w)
			kf.rotation[0] = (float)rotation[3];
			kf.rotation[1] = (float)rotation[0];
			kf.rotation[2] = (float)rotation[1];
			kf.rotation[3] = (float)rotation[2];

			ab.keyframes.push_back( kf );
		}
	}

	return a;
}