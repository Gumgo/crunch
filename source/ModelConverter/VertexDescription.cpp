#include "VertexDescription.h"

size_t VertexComponent::getSize() const {
	return VERTEX_COMPONENT_TYPE_SIZES[componentType] * VERTEX_DATA_TYPE_SIZES[dataType];
}

size_t VertexComponent::getAlignedSize() const {
	return (getSize() + 3) / 4;
}

VertexDescriptions getVertexDescriptions( const std::string & fname ) {
	VertexDescriptions descs;

	KeyValueReader in( fname );
	KeyValueReader::Iterator it = in.getIterator();

	if (!it.contains( "staticVertex" ))
		descs.staticVertexDescriptionProvided = false;
	else {
		KeyValueReader::Iterator staticVertexIt = it.getArray( "staticVertex" );
		descs.staticVertexDescriptionProvided = true;
		StaticVertexDescription & desc = descs.staticVertexDescription;

		desc.vertexSize = staticVertexIt.getUint( "vertexSize" );
		// warn if we're badly aligned
		if (desc.vertexSize % 4 != 0)
			std::cout << "WARNING: Vertex size is not a multiple of 4 bytes\n";

		KeyValueReader::Iterator componentsIt = staticVertexIt.getArray( "components" );
		for (size_t c = 0; c < componentsIt.getCount(); ++c) {
			// get each component
			VertexComponent vc;
			KeyValueReader::Iterator componentIt = componentsIt.getArray( c );

			vc.name = componentIt.getString( "name" );
			for (size_t i = 0; i < desc.vertexComponents.size(); ++i) {
				if (vc.name == desc.vertexComponents[i].name)
					throw std::runtime_error( "Duplicate vertex component name " + vc.name + " specified" );
			}

			vc.componentOffset = componentIt.getUint( "offset" );
			if (vc.componentOffset % 4 != 0)
				std::cout << "WARNING: Vertex component " + vc.name + " alignment is not a multiple of 4 bytes\n";

			std::string typeString = componentIt.getString( "type" );
			std::string dataTypeString = componentIt.getString( "dataType" );

			size_t t;
			for (t = 0; t < vertexComponentTypeCount; ++t) {
				if (typeString == VERTEX_COMPONENT_TYPE_NAMES[t]) {
					vc.componentType = (VertexComponentType)t;
					break;
				}
			}
			if (t == vertexComponentTypeCount)
				throw std::runtime_error( "Unknown vertex component type " + typeString );
			if (vc.componentType == VCT_BONE_INDICES || vc.componentType == VCT_BONE_WEIGHTS)
				throw std::runtime_error( "Bone indices and weights not supported for static vertices" );

			for (t = 0; t < vertexDataTypeCount; ++t) {
				if (dataTypeString == VERTEX_DATA_TYPE_NAMES[t]) {
					vc.dataType = (VertexDataType)t;
					break;
				}
			}
			if (t == vertexDataTypeCount)
				throw std::runtime_error( "Unknown vertex data type " + typeString );

			if (vc.dataType == VDT_FLOAT) {
				vc.normalized = false;
				vc.integer = false;
			} else {
				vc.normalized = componentIt.getBool( "normalized" );
				vc.integer = componentIt.getBool( "integer" );
			}
			if (vc.normalized && vc.integer)
				throw std::runtime_error( "Cannot have normalized integer vertex component" );

			// verify the component's size and offset
			if (vc.componentOffset + vc.getSize() > desc.vertexSize)
				throw std::runtime_error( "Vertex component " + vc.name + " does not fit in specified vertex structure" );
			// check to make sure it doesn't overlap any other vertices
			for (size_t v = 0; v < desc.vertexComponents.size(); ++v) {
				const VertexComponent & ovc = desc.vertexComponents[v];
				size_t isectMin = std::max( vc.componentOffset, ovc.componentOffset );
				size_t isectMax = std::min( vc.componentOffset + vc.getSize(), ovc.componentOffset + ovc.getSize() );
				if (isectMin < isectMax)
					throw std::runtime_error( "Vertex component " + vc.name + " overlaps with other vertex components" );
			}

			desc.vertexComponents.push_back( vc );
		}

		if (desc.vertexComponents.empty())
			throw std::runtime_error( "No vertex components specified" );
	}

	if (!it.contains( "riggedVertex" ))
		descs.riggedVertexDescriptionProvided = false;
	else {
		KeyValueReader::Iterator riggedVertexIt = it.getArray( "riggedVertex" );
		descs.riggedVertexDescriptionProvided = true;
		RiggedVertexDescription & desc = descs.riggedVertexDescription;

		desc.vertexSize = riggedVertexIt.getUint( "vertexSize" );
		desc.bonesPerVertex = riggedVertexIt.getUint( "boneCount" );
		if (desc.bonesPerVertex < 1 || desc.bonesPerVertex > 4)
			throw std::runtime_error( "Bone count must be between 1 and 4" );
		// warn if we're badly aligned
		if (desc.vertexSize % 4 != 0)
			std::cout << "WARNING: Vertex size is not a multiple of 4 bytes\n";

		KeyValueReader::Iterator componentsIt = riggedVertexIt.getArray( "components" );
		for (size_t c = 0; c < componentsIt.getCount(); ++c) {
			// get each component
			VertexComponent vc;
			KeyValueReader::Iterator componentIt = componentsIt.getArray( c );

			vc.name = componentIt.getString( "name" );
			for (size_t i = 0; i < desc.vertexComponents.size(); ++i) {
				if (vc.name == desc.vertexComponents[i].name)
					throw std::runtime_error( "Duplicate vertex component name " + vc.name + " specified" );
			}

			vc.componentOffset = componentIt.getUint( "offset" );
			if (vc.componentOffset % 4 != 0)
				std::cout << "WARNING: Vertex component " + vc.name + " alignment is not a multiple of 4 bytes\n";

			std::string typeString = componentIt.getString( "type" );
			std::string dataTypeString = componentIt.getString( "dataType" );

			size_t t;
			for (t = 0; t < vertexComponentTypeCount; ++t) {
				if (typeString == VERTEX_COMPONENT_TYPE_NAMES[t]) {
					vc.componentType = (VertexComponentType)t;
					break;
				}
			}
			if (t == vertexComponentTypeCount)
				throw std::runtime_error( "Unknown vertex component type " + typeString );

			for (t = 0; t < vertexDataTypeCount; ++t) {
				if (dataTypeString == VERTEX_DATA_TYPE_NAMES[t]) {
					vc.dataType = (VertexDataType)t;
					break;
				}
			}
			if (t == vertexDataTypeCount)
				throw std::runtime_error( "Unknown vertex data type " + typeString );

			if (vc.dataType == VDT_FLOAT)
				vc.normalized = false;
			else {
				vc.normalized = componentIt.getBool( "normalized" );
				if (vc.normalized && vc.componentType == VCT_BONE_INDICES)
					throw std::runtime_error( "Bone indices cannot be normalized" );
			}

			// verify the component's size and offset
			size_t size = vc.getSize();
			if (vc.componentType == VCT_BONE_INDICES || vc.componentType == VCT_BONE_WEIGHTS)
				size *= desc.bonesPerVertex;
			if (vc.componentOffset + size > desc.vertexSize)
				throw std::runtime_error( "Vertex component " + vc.name + " does not fit in specified vertex structure" );
			// check to make sure it doesn't overlap any other vertices
			for (size_t v = 0; v < desc.vertexComponents.size(); ++v) {
				const VertexComponent & ovc = desc.vertexComponents[v];
				size_t oSize = ovc.getSize();
				if (ovc.componentType == VCT_BONE_INDICES || ovc.componentType == VCT_BONE_WEIGHTS)
					oSize *= desc.bonesPerVertex;
				size_t isectMin = std::max( vc.componentOffset, ovc.componentOffset );
				size_t isectMax = std::min( vc.componentOffset + size, ovc.componentOffset + oSize );
				if (isectMin < isectMax)
					throw std::runtime_error( "Vertex component " + vc.name + " overlaps with other vertex components" );
			}

			desc.vertexComponents.push_back( vc );
		}

		if (desc.vertexComponents.empty())
			throw std::runtime_error( "No vertex components specified" );
	}

	return descs;
}