#include "VertexExtractor.h"

template <typename T> void extractElement( FbxMesh * mesh, FbxLayerElementTemplate <T> * layerElem, int triangleIndex, int vertexIndex, float * dest, int count, bool uv ) {
	// extract data based on the mapping mode
	int index;
	switch (layerElem->GetMappingMode()) {
	case FbxLayerElement::eByControlPoint:
		switch (layerElem->GetReferenceMode()) {
		case FbxLayerElement::eDirect:
			index = mesh->GetPolygonVertex( triangleIndex, vertexIndex );
			break;
		case FbxLayerElement::eIndexToDirect:
			index = layerElem->GetIndexArray().GetAt( mesh->GetPolygonVertex( triangleIndex, vertexIndex ) );
			break;
		default:
			throw std::runtime_error( "Invalid reference mode" );
		}
		break;
	case FbxLayerElement::eByPolygonVertex:
		switch (layerElem->GetReferenceMode()) {
		case FbxLayerElement::eDirect:
			index = uv ?
				mesh->GetTextureUVIndex( triangleIndex, vertexIndex ) :
				mesh->GetPolygonVertexIndex( triangleIndex ) + vertexIndex;
			break;
		case FbxLayerElement::eIndexToDirect:
			index = uv ?
				mesh->GetTextureUVIndex( triangleIndex, vertexIndex ) :
				layerElem->GetIndexArray().GetAt( mesh->GetPolygonVertexIndex( triangleIndex ) + vertexIndex );
			break;
		default:
			throw std::runtime_error( "Invalid reference mode" );
		}
		break;
	default:
		throw std::runtime_error( "Invalid mapping mode" );
	}

	T e = layerElem->GetDirectArray().GetAt( index );
	for (int p = 0; p < count; ++p)
		dest[p] = (float)e[p];
}

std::vector <Vertex> extractVertices( FbxMesh * mesh, const std::map <FbxNode*, uint> & boneIndexMap, const VertexDescriptions & vd, bool staticMesh ) {
	bool positions = false;
	bool normals = false;
	bool texCoords = false;
	bool tangents = false;
	bool bitangents = false;
	bool boneIndices = false;
	bool boneWeights = false;
	const std::vector <VertexComponent> & vc = staticMesh ? vd.staticVertexDescription.vertexComponents : vd.riggedVertexDescription.vertexComponents;
	for (size_t i = 0; i < vc.size(); ++i) {
		switch (vc[i].componentType) {
		case VCT_POSITION:
			positions = true;
			break;
		case VCT_NORMAL:
			normals = true;
			break;
		case VCT_TEXCOORDS:
			texCoords = true;
			break;
		case VCT_TANGENT:
			tangents = true;
			break;
		case VCT_BITANGENT:
			bitangents = true;
			break;
		case VCT_BONE_INDICES:
			boneIndices = true;
			break;
		case VCT_BONE_WEIGHTS:
			boneWeights = true;
			break;
		}
	}

	// triangulate the mesh
	bool triangulate = !mesh->IsTriangleMesh();
	if (triangulate) {
		FbxGeometryConverter converter( mesh->GetFbxManager() );
		mesh = converter.TriangulateMesh( mesh );
	}

	try {
		// reserve space for vertices
		int triangleCount = mesh->GetPolygonCount();
		std::vector <Vertex> vertices;
		vertices.reserve( (size_t)triangleCount * 3 );

		FbxVector4 * controlPoints = mesh->GetControlPoints();

		// fill in vertex positions
		if (positions) {
			for (int i = 0; i < triangleCount; ++i) {
				for (size_t t = 0; t < 3; ++t) {
					vertices.push_back( Vertex() );
					int positionIndex = mesh->GetPolygonVertex( i, t );
					for (int p = 0; p < 3; ++p)
						vertices.back().position[p] = (float)controlPoints[positionIndex][p];
				}
			}
		}

		if (boneIndices || boneWeights) {
			// check for skin
			FbxSkin * skin = NULL;
			for (int i = 0; i < mesh->GetDeformerCount(); ++i) {
				FbxDeformer * def = mesh->GetDeformer( i );
				if (def->GetDeformerType() == FbxDeformer::eSkin) {
					skin = FbxCast <FbxSkin>( def );
					break;
				}
			}

			if (skin == NULL)
				throw std::runtime_error( "No bone info found" );

			// construct the bone indices/weights based off of the bone name to index map passed in
			struct BoneInfluence {
				uint count;
				uint indices[4];
				float weights[4];
				BoneInfluence()
					: count( 0 ) {
					indices[0] = indices[1] = indices[2] = indices[3] = 0;
					weights[0] = weights[1] = weights[2] = weights[3] = 0.0f;
				}
			};
			std::vector <BoneInfluence> influences( (size_t)mesh->GetControlPointsCount() );
			int clusters = skin->GetClusterCount();
			// go through all the clusters and assign indices/weights to the vertices they influence
			for (int i = 0; i < clusters; ++i) {
				FbxCluster * cluster = skin->GetCluster( i );
				FbxNode * clusterBone = cluster->GetLink();
				std::map <FbxNode*, uint>::const_iterator it = boneIndexMap.find( clusterBone );
				if (it == boneIndexMap.end())
					throw std::runtime_error( "Mesh is influenced by a bone not in the skeleton" );

				uint boneIndex = it->second;
				int indexCount = cluster->GetControlPointIndicesCount();
				int * indices = cluster->GetControlPointIndices();
				double * weights = cluster->GetControlPointWeights();
				for (int j = 0; j < indexCount; ++j) {
					int index = indices[j];
					float weight = (float)weights[j];
					if (influences[index].count == vd.riggedVertexDescription.bonesPerVertex)
						throw std::runtime_error( "Too many bones influencing a single vertex" );
					influences[index].indices[influences[index].count] = boneIndex;
					influences[index].weights[influences[index].count] = weight;
					++influences[index].count;
				}
			}

			for (int i = 0; i < triangleCount; ++i) {
				for (size_t t = 0; t < 3; ++t) {
					Vertex & v = vertices[i*3+t];
					int positionIndex = mesh->GetPolygonVertex( i, t );
					memcpy( v.boneIndices, influences[positionIndex].indices, sizeof( v.boneIndices ) );
					memcpy( v.boneWeights, influences[positionIndex].weights, sizeof( v.boneWeights ) );
				}
			}
		}

		bool foundTexCoords = false;
		bool foundNormals = false;
		bool foundTangents = false;
		bool foundBitangents = false;

		// search through layers
		for (int l = 0; l < mesh->GetLayerCount(); ++l) {
			FbxLayer * layer = mesh->GetLayer( l );
			FbxLayerElementUV * texCoordLayer			= foundTexCoords	? NULL : layer->GetUVs();
			FbxLayerElementNormal * normalLayer			= foundNormals		? NULL : layer->GetNormals();
			FbxLayerElementTangent * tangentLayer		= foundTangents		? NULL : layer->GetTangents();
			FbxLayerElementBinormal * bitangentLayer	= foundBitangents	? NULL : layer->GetBinormals();

			if (texCoordLayer != NULL)
				foundTexCoords = true;
			if (normalLayer != NULL)
				foundNormals = true;
			if (tangentLayer != NULL)
				foundTangents = true;
			if (bitangentLayer != NULL)
				foundBitangents = true;

			// for each vertex, extract all elements in this layer that exist
			for (int i = 0; i < triangleCount; ++i) {
				for (int v = 0; v < 3; ++v) {
					Vertex & vtx = vertices[i*3+v];
					int positionIndex = mesh->GetPolygonVertexIndex( i ) + v;

					if (texCoords && texCoordLayer != NULL)
						extractElement( mesh, texCoordLayer, i, v, vtx.texCoord, 2, true );
					if (normals && normalLayer != NULL)
						extractElement( mesh, normalLayer, i, v, vtx.normal, 3, false );
					if (tangents && tangentLayer != NULL)
						extractElement( mesh, tangentLayer, i, v, vtx.tangent, 3, false );
					if (bitangents && bitangentLayer != NULL)
						extractElement( mesh, bitangentLayer, i, v, vtx.bitangent, 3, false );
				}
			}
		}

		// destroy the triangulated mesh
		if (triangulate)
			mesh->Destroy();

		// update statuses
		if (texCoords && !foundTexCoords)
			throw std::runtime_error( "No texture coordinates found" );
		if (normals && !foundNormals)
			throw std::runtime_error( "No normals found" );
		if (tangents && !foundTangents)
			throw std::runtime_error( "No tangents found" );
		if (bitangents && !foundBitangents)
			throw std::runtime_error( "No bitangents found" );

		return vertices;

	} catch (const std::exception &) {
		if (triangulate)
			mesh->Destroy();
		throw;
	}
}