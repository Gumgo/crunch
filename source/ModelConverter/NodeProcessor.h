#ifndef NODEPROCESSOR_DEFINED
#define NODEPROCESSOR_DEFINED

#include "Fbx.h"
#include <Crunch/Types.h>
#include "Util.h"
#include "VertexDescription.h"
#include <iostream>
#include <map>

static const char * STATIC_MESH_PREFIX = "sm_";
static const char * RIGGED_MESH_PREFIX = "rm_";
static const char * SKELETON_ROOT_PREFIX = "sk_";
static const char * SKELETON_BONE_PREFIX = "bn_";
static const char * BOUNDS_SUFFIX = "_bnd";
static const char * ANIMATION_PREFIX = "an_";

void processNode( FbxNode * node, const VertexDescriptions & vertexDescriptions );

#endif