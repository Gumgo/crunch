#ifndef VERTEXDESCRIPTION_DEFINED
#define VERTEXDESCRIPTION_DEFINED

#include <Crunch/Types.h>
#include <Crunch/KeyValueReader.h>
#include "Util.h"
#include <vector>
#include <iostream>

// TODO: eventually implement other types like 10_10_10_2

enum VertexDataType {
	VDT_BYTE,
	VDT_UNSIGNED_BYTE,

	VDT_SHORT,
	VDT_UNSIGNED_SHORT,

	VDT_INT,
	VDT_UNSIGNED_INT,

	VDT_FLOAT,

	vertexDataTypeCount
};

static const char * VERTEX_DATA_TYPE_NAMES[vertexDataTypeCount] = {
	"GL_BYTE",
	"GL_UNSIGNED_BYTE",

	"GL_SHORT",
	"GL_UNSIGNED_SHORT",

	"GL_INT",
	"GL_UNSIGNED_INT",

	"GL_FLOAT"
};

static const size_t VERTEX_DATA_TYPE_SIZES[vertexDataTypeCount] = {
	1,
	1,

	2,
	2,

	4,
	4,

	4
};

enum VertexComponentType {
	VCT_POSITION,
	VCT_NORMAL,
	VCT_TEXCOORDS,
	VCT_TANGENT,
	VCT_BITANGENT,
	VCT_BONE_INDICES,
	VCT_BONE_WEIGHTS,
	vertexComponentTypeCount
};

static const char * VERTEX_COMPONENT_TYPE_NAMES[vertexComponentTypeCount] = {
	"POSITION",
	"NORMAL",
	"TEXCOORDS",
	"TANGENT",
	"BITANGENT",
	"BONE_INDICES",
	"BONE_WEIGHTS"
};

static const size_t VERTEX_COMPONENT_TYPE_SIZES[vertexComponentTypeCount] = {
	3,
	3,
	2,
	3,
	3,
	1, // can vary
	1  // can vary
};

struct VertexComponent {
	std::string name;					// name of the component
	size_t componentOffset;				// offset of the component in the vertex data
	VertexComponentType componentType;	// type of component
	VertexDataType dataType;			// data type component should be represented as
	bool normalized;					// whether the data type is normalized
	bool integer;						// whether the data should be interpreted as an integer

	// returns the size in bytes of this component
	size_t getSize() const;
	// returns the padded size of this component
	size_t getAlignedSize() const;
};

struct StaticVertexDescription {
	size_t vertexSize; // vertex size in bytes
	std::vector <VertexComponent> vertexComponents;
};

struct RiggedVertexDescription {
	size_t vertexSize; // vertex size in bytes
	std::vector <VertexComponent> vertexComponents;
	uint bonesPerVertex;
};

struct VertexDescriptions {
	bool staticVertexDescriptionProvided;
	StaticVertexDescription staticVertexDescription;
	bool riggedVertexDescriptionProvided;
	RiggedVertexDescription riggedVertexDescription;
};

VertexDescriptions getVertexDescriptions( const std::string & fname );

#endif