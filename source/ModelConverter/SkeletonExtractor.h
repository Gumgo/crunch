#ifndef SKELETONEXTRACTOR_DEFINED
#define SKELETONEXTRACTOR_DEFINED

#include <Crunch/Types.h>
#include <Crunch/Maths.h>
#include "Util.h"
#include "Fbx.h"
#include <map>
#include <set>
#include <stack>
#include <vector>

std::map <FbxNode*, uint> buildBoneIndexMap( FbxNode * root, const char * bonePrefix );

struct Bone {
	std::string name;
	uint parent;
	float inverseBind[12];
};

struct Keyframe {
	float time; // time in seconds
	float translation[3];
	float rotation[4]; // quaternion
};

struct AnimatedBone {
	uint boneId;
	std::vector <Keyframe> keyframes;
};

struct Animation {
	float length; // length in seconds
	float fadeInTime; // fade in time in seconds
	float fadeOutTime; // fade out time in seconds
	std::vector <AnimatedBone> animatedBones;
};

std::vector <Bone> extractSkeleton( FbxNode * root, const std::map <FbxNode*, uint> & boneIndexMap, const char * bonePrefix );

Animation extractAnimation( FbxNode * animNode, const std::map <FbxNode*, uint> * boneIndexMap );

#endif