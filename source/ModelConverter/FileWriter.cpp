#include "FileWriter.h"

void writeMesh( const std::string & fname, const Mesh & mesh, const VertexDescriptions & desc, bool staticMesh,
	const float * minBound, const float * maxBound ) {
	KeyValueWriter out;

	KeyValueWriter::Iterator boundsIt = out.getIterator().setArray( "bounds" );
	{
		KeyValueWriter::Iterator minIt = boundsIt.setArray( "min" );
		{
			minIt.setFloat( "x", minBound[0] );
			minIt.setFloat( "y", minBound[1] );
			minIt.setFloat( "z", minBound[2] );
		}
		KeyValueWriter::Iterator maxIt = boundsIt.setArray( "max" );
		{
			maxIt.setFloat( "x", maxBound[0] );
			maxIt.setFloat( "y", maxBound[1] );
			maxIt.setFloat( "z", maxBound[2] );
		}
	}

	KeyValueWriter::Iterator buffersIt = out.getIterator().setArray( "buffers" );
	{
		KeyValueWriter::Iterator vertexBufferIt = buffersIt.setArray( "vertexBuffer" );
		{
			struct Attribute {
				std::string name;
				std::string type;
				bool normalized;
				bool integer;
				uint components;
				uint stride;
				uint offset;
				Attribute( const std::string & n, const std::string & t, bool nrm, bool i, uint c, uint s, uint o )
					: name( n )
					, type( t )
					, normalized( nrm )
					, integer( i )
					, components( c )
					, stride( s )
					, offset( o ) {
				}
			};

			size_t vertexSize = staticMesh ? desc.staticVertexDescription.vertexSize : desc.riggedVertexDescription.vertexSize;
			const std::vector <VertexComponent> & comps = staticMesh ?
				desc.staticVertexDescription.vertexComponents : desc.riggedVertexDescription.vertexComponents;

			size_t offset = 0;
			std::vector <Attribute> attribs;
			for (size_t i = 0; i < comps.size(); ++i) {
				const VertexComponent & c = comps[i];
				uint compSize;
				if (c.componentType == VCT_BONE_INDICES || c.componentType == VCT_BONE_WEIGHTS)
					// if it has these components, it must be a rigged mesh
					compSize = desc.riggedVertexDescription.bonesPerVertex;
				else
					compSize = VERTEX_COMPONENT_TYPE_SIZES[c.componentType];
				attribs.push_back( Attribute( c.name, VERTEX_DATA_TYPE_NAMES[c.dataType], c.normalized, c.integer, compSize, (uint)vertexSize, c.componentOffset ) );
			}

			KeyValueWriter::Iterator attributesIt = vertexBufferIt.setArray( "attributes" );
			{
				for (size_t i = 0; i < attribs.size(); ++i) {
					const Attribute & attrib = attribs[i];
					KeyValueWriter::Iterator attribIt = attributesIt.setArray();
					{
						attribIt.setString( "name", attrib.name );
						attribIt.setString( "type", attrib.type );
						if (attrib.type != "GL_FLOAT") {
							attribIt.setBool( "normalized", attrib.normalized );
							attribIt.setBool( "integer", attrib.integer );
						}
						attribIt.setUint( "components", attrib.components );
						attribIt.setUint( "stride", attrib.stride );
						attribIt.setUint( "offset", attrib.offset );
					}
				}
			}

			std::vector <byte> bufferData( vertexSize * mesh.getVertices().size(), 0 );
			for (size_t i = 0; i < mesh.getVertices().size(); ++i) {
				const Vertex & v = mesh.getVertices()[i];
				size_t offset = vertexSize * i;
				// write each vertex attribute
				for (size_t c = 0; c < comps.size(); ++c) {
					const VertexComponent & cmp = comps[c];
					const void * srcPtr;
					const void * dstPtr = &bufferData[offset + cmp.componentOffset];
					switch (cmp.componentType) {
					case VCT_POSITION:
						srcPtr = v.position;
						break;
					case VCT_TEXCOORDS:
						srcPtr = v.texCoord;
						break;
					case VCT_NORMAL:
						srcPtr = v.normal;
						break;
					case VCT_TANGENT:
						srcPtr = v.tangent;
						break;
					case VCT_BITANGENT:
						srcPtr = v.bitangent;
						break;
					case VCT_BONE_INDICES:
						srcPtr = v.boneIndices;
						break;
					case VCT_BONE_WEIGHTS:
						srcPtr = v.boneWeights;
						break;
					}
					size_t elements;
					if (cmp.componentType == VCT_BONE_INDICES || cmp.componentType == VCT_BONE_WEIGHTS)
						elements = desc.riggedVertexDescription.bonesPerVertex;
					else
						elements = VERTEX_COMPONENT_TYPE_SIZES[cmp.componentType];

					// convert to the appropriate type
					if (cmp.componentType != VCT_BONE_INDICES) {
						// the source component type is float
						for (size_t e = 0; e < elements; ++e) {
							float src = ((const float*)srcPtr)[e];
							// convert to the appropriate type
							switch (cmp.dataType) {
							case VDT_BYTE:
								if (cmp.normalized)
									((char*)dstPtr)[e] = (char)(src * 127.5f - 0.5f);
								else
									((char*)dstPtr)[e] = (char)src;
								break;
							case VDT_UNSIGNED_BYTE:
								if (cmp.normalized)
									((byte*)dstPtr)[e] = (byte)(src * 255.0f);
								else
									((byte*)dstPtr)[e] = (byte)src;
								break;
							case VDT_SHORT:
								if (cmp.normalized)
									((short*)dstPtr)[e] = (short)(src * 32767.5f - 0.5f);
								else
									((short*)dstPtr)[e] = (short)src;
								break;
							case VDT_UNSIGNED_SHORT:
								if (cmp.normalized)
									((ushort*)dstPtr)[e] = (ushort)(src * 65535.0f);
								else
									((ushort*)dstPtr)[e] = (ushort)src;
								break;
							case VDT_INT:
								if (cmp.normalized)
									((int*)dstPtr)[e] = (int)(src * 2147483647.5f - 0.5f);
								else
									((int*)dstPtr)[e] = (int)src;
								break;
							case VDT_UNSIGNED_INT:
								if (cmp.normalized)
									((uint*)dstPtr)[e] = (uint)(src * 4294967295.0f);
								else
									((uint*)dstPtr)[e] = (uint)src;
								break;
							case VDT_FLOAT:
								((float*)dstPtr)[e] = src;
								break;
							}
						}
					} else {
						// the source component type is uint
						// normalization doesn't apply in this case
						for (size_t e = 0; e < elements; ++e) {
							uint src = ((const uint*)srcPtr)[e];
							// convert to the appropriate type
							switch (cmp.dataType) {
							case VDT_BYTE:
								((char*)dstPtr)[e] = (char)src;
								break;
							case VDT_UNSIGNED_BYTE:
								((byte*)dstPtr)[e] = (byte)src;
								break;
							case VDT_SHORT:
								((short*)dstPtr)[e] = (short)src;
								break;
							case VDT_UNSIGNED_SHORT:
								((ushort*)dstPtr)[e] = (ushort)src;
								break;
							case VDT_INT:
								((int*)dstPtr)[e] = (int)src;
								break;
							case VDT_UNSIGNED_INT:
								((uint*)dstPtr)[e] = (uint)src;
								break;
							case VDT_FLOAT:
								((float*)dstPtr)[e] = (float)src;
								break;
							}
						}
					}

					// if we're using a normalized byte or ushort for weights, correct for quantization error
					// with uint it would be so small it doesn't matter
					if (cmp.componentType == VCT_BONE_WEIGHTS) {
						if (cmp.dataType == VDT_BYTE) {
							byte * wPtr = (byte*)dstPtr;
							uint total = 0;
							uint largest = 0;
							uint largestIdx = 0;
							for (uint e = 0; e < elements; ++e) {
								if (wPtr[e] > largest) {
									largest = wPtr[e];
									largestIdx = e;
								}
								total += wPtr[e];
							}
							// if total is 0, we can't really do much
							if (total > 0) {
								if (total > 255)
									wPtr[largestIdx] -= (total - 255);
								else
									wPtr[largestIdx] += (255 - total);
							}
						} else if (cmp.dataType == VDT_UNSIGNED_SHORT) {
							ushort * wPtr = (ushort*)dstPtr;
							uint total = 0;
							uint largest = 0;
							uint largestIdx = 0;
							for (uint e = 0; e < elements; ++e) {
								if (wPtr[e] > largest) {
									largest = wPtr[e];
									largestIdx = e;
								}
								total += wPtr[e];
							}
							// if total is 0, we can't really do much
							if (total > 0) {
								if (total > 65535)
									wPtr[largestIdx] -= (total - 65535);
								else
									wPtr[largestIdx] += (65535 - total);
							}
						}
					}
				}
			}

			vertexBufferIt.setBinaryData( "data", &bufferData[0], bufferData.size() );
		}

		KeyValueWriter::Iterator indexBufferIt = buffersIt.setArray( "indexBuffer" );
		{
			// use 16-bit indices if we can
			size_t indexSize = (mesh.getVertices().size() <= 65536) ? 2 : 4;

			indexBufferIt.setUint( "indexSize" , (uint)indexSize );

			std::vector <byte> bufferData( mesh.getTriangles().size() * 3 * indexSize );
			for (size_t i = 0; i < mesh.getTriangles().size(); ++i) {
				const Mesh::Triangle & t = mesh.getTriangles()[i];
				if (indexSize == 2) {
					ushort * buf = (ushort*)(&bufferData[0]);
					for (size_t v = 0; v < 3; ++v)
						buf[i*3+v] = (ushort)t.vertices[v];
				} else {
					uint * buf = (uint*)(&bufferData[0]);
					for (size_t v = 0; v < 3; ++v)
						buf[i*3+v] = t.vertices[v];
				}
			}
			indexBufferIt.setBinaryData( "data", &bufferData[0], bufferData.size() );
		}
	}

	out.writeToFile( fname );
}

void writeSkeleton( const std::string & fname, const std::vector <Bone> & bones ) {
	KeyValueWriter out;

	KeyValueWriter::Iterator it = out.getIterator();

	KeyValueWriter::Iterator bonesIt = it.setArray( "bones" );
	for (size_t b = 0; b < bones.size(); ++b) {
		KeyValueWriter::Iterator boneIt = bonesIt.setArray();
		boneIt.setString( "name", bones[b].name );
		// if bone has no parent, don't write the parent field
		// this is the case if the parent index is itself
		if (bones[b].parent != (uint)b)
			boneIt.setUint( "parent", bones[b].parent );

		// write the inverse bind in binary
		boneIt.setBinaryData( "inverseBind", (byte*)bones[b].inverseBind, sizeof( bones[b].inverseBind ) );
	}

	out.writeToFile( fname );
}

void writeAnimation( const std::string & fname, const Animation & anim ) {
	KeyValueWriter out;

	KeyValueWriter::Iterator it = out.getIterator();

	it.setFloat( "length", anim.length );
	it.setFloat( "fadeInTime", anim.fadeInTime );
	it.setFloat( "fadeOutTime", anim.fadeOutTime );

	KeyValueWriter::Iterator animatedBonesIt = it.setArray( "animatedBones" );
	for (size_t b = 0; b < anim.animatedBones.size(); ++b) {
		const AnimatedBone & ab = anim.animatedBones[b];
		KeyValueWriter::Iterator boneIt = animatedBonesIt.setArray();
		boneIt.setUint( "boneId", ab.boneId );

		KeyValueWriter::Iterator keyframesIt = boneIt.setArray( "keyframes" );
		for (size_t kf = 0; kf < ab.keyframes.size(); ++kf) {
			KeyValueWriter::Iterator keyframeIt = keyframesIt.setArray();
			keyframeIt.setFloat( "time", ab.keyframes[kf].time );
			keyframeIt.setBinaryData( "translation", (byte*)ab.keyframes[kf].translation, sizeof( ab.keyframes[kf].translation ) );
			keyframeIt.setBinaryData( "rotation", (byte*)ab.keyframes[kf].rotation, sizeof( ab.keyframes[kf].rotation ) );
		}
	}

	out.writeToFile( fname );
}