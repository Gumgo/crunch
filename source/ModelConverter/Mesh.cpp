#include "Mesh.h"

Mesh::Mesh( const std::vector <Vertex> & unindexedVertices, const VertexDescriptions & vd, bool staticMesh ) {
	hasPositions = false;
	hasNormals = false;
	hasTexCoords = false;
	hasTangents = false;
	hasBitangents = false;
	hasBoneIndices = false;
	hasBoneWeights = false;
	const std::vector <VertexComponent> & vc = staticMesh ? vd.staticVertexDescription.vertexComponents : vd.riggedVertexDescription.vertexComponents;
	for (size_t i = 0; i < vc.size(); ++i) {
		switch (vc[i].componentType) {
		case VCT_POSITION:
			hasPositions = true;
			break;
		case VCT_NORMAL:
			hasNormals = true;
			break;
		case VCT_TEXCOORDS:
			hasTexCoords = true;
			break;
		case VCT_TANGENT:
			hasTangents = true;
			break;
		case VCT_BITANGENT:
			hasBitangents = true;
			break;
		case VCT_BONE_INDICES:
			hasBoneIndices = true;
			break;
		case VCT_BONE_WEIGHTS:
			hasBoneWeights = true;
			break;
		}
	}

	if (!staticMesh)
		boneCount = vd.riggedVertexDescription.bonesPerVertex;

	struct Comparitor {
		Mesh * mesh;

		bool operator()( const Vertex & v0, const Vertex & v1 ) {
			if (mesh->hasPositions) {
				for (int i = 0; i < 3; ++i) {
					if (v0.position[i] < v1.position[i]) return true;
					if (v0.position[i] > v1.position[i]) return false;
				}
			}
			if (mesh->hasTexCoords) {
				for (int i = 0; i < 2; ++i) {
					if (v0.texCoord[i] < v1.texCoord[i]) return true;
					if (v0.texCoord[i] > v1.texCoord[i]) return false;
				}
			}
			if (mesh->hasNormals) {
				for (int i = 0; i < 3; ++i) {
					if (v0.normal[i] < v1.normal[i]) return true;
					if (v0.normal[i] > v1.normal[i]) return false;
				}
			}
			if (mesh->hasTangents) {
				for (int i = 0; i < 3; ++i) {
					if (v0.tangent[i] < v1.tangent[i]) return true;
					if (v0.tangent[i] > v1.tangent[i]) return false;
				}
			}
			if (mesh->hasBitangents) {
				for (int i = 0; i < 3; ++i) {
					if (v0.bitangent[i] < v1.bitangent[i]) return true;
					if (v0.bitangent[i] > v1.bitangent[i]) return false;
				}
			}
			if (mesh->hasBoneIndices) {
				for (uint i = 0; i < mesh->boneCount; ++i) {
					if (v0.boneIndices[i] < v1.boneIndices[i]) return true;
					if (v0.boneIndices[i] > v1.boneIndices[i]) return false;
				}
			}
			if (mesh->hasBoneWeights) {
				for (uint i = 0; i < mesh->boneCount; ++i) {
					if (v0.boneWeights[i] < v1.boneWeights[i]) return true;
					if (v0.boneWeights[i] > v1.boneWeights[i]) return false;
				}
			}
			return false;
		}
	};
	Comparitor c;
	c.mesh = this;

	std::map <Vertex, uint, Comparitor> vertexIndices( c );

	// go through all the vertices
	for (size_t i = 0; i < unindexedVertices.size() / 3; ++i) {
		Triangle tri;
		for (size_t v = 0; v < 3; ++v) {
			const Vertex & vtx = unindexedVertices[i*3+v];
			uint index;
			// try to find the vertex in the map
			std::map <Vertex, uint, Comparitor>::iterator it = vertexIndices.find( vtx );
			if (it == vertexIndices.end()) {
				// this vertex has not yet been indexed yet - add it
				index = vertices.size();
				vertices.push_back( vtx );
				vertexIndices.insert( std::make_pair( vtx, index ) );
			} else
				// this vertex has already been specified - just take that index
				index = it->second;
			tri.vertices[v] = index;
		}
		triangles.push_back( tri );
	}
}

const std::vector <Vertex> & Mesh::getVertices() const {
	return vertices;
}

const std::vector <Mesh::Triangle> & Mesh::getTriangles() const {
	return triangles;
}

bool Mesh::usesTexCoords() const {
	return hasTexCoords;
}

bool Mesh::usesNormals() const {
	return hasNormals;
}

bool Mesh::usesTangents() const {
	return hasTangents;
}

bool Mesh::usesBitangents() const {
	return hasBitangents;
}

bool Mesh::usesBoneIndices() const {
	return hasBoneIndices;
}

bool Mesh::usesBoneWeights() const {
	return hasBoneWeights;
}

uint Mesh::getBoneCount() const {
	return boneCount;
}