#ifndef VERTEXEXTRACTOR_DEFINED
#define VERTEXEXTRACTOR_DEFINED

#include <Crunch/Types.h>
#include "Fbx.h"
#include "VertexDescription.h"
#include <vector>
#include <map>
#include <string>

struct Vertex {
	float position[3];
	float texCoord[2];
	float normal[3];
	float tangent[3];
	float bitangent[3];

	uint boneIndices[4];
	float boneWeights[4];
};

std::vector <Vertex> extractVertices( FbxMesh * mesh, const std::map <FbxNode*, uint> & boneIndexMap, const VertexDescriptions & vd, bool staticMesh );

#endif