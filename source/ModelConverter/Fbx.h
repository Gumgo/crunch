#ifndef FBX_INCLUDE_DEFINED
#define FBX_INCLUDE_DEFINED

#define FBXSDK_NEW_API
#include <fbxsdk.h>
#ifdef _DEBUG
#pragma comment( lib, "fbxsdk-2013.1-mtd.lib" )
#else
#pragma comment( lib, "fbxsdk-2013.1-mt.lib" )
#endif
#pragma comment( lib, "wininet.lib" )

#endif