#include "Scene.h"

Scene::Scene( FbxManager * mgr, const std::string & fname ) {
	manager = mgr;
	scene = NULL;

	FbxImporter * importer = FbxImporter::Create( manager, "" );
	bool status = importer->Initialize( fname.c_str(), -1, manager->GetIOSettings() );
	if (!status) {
		importer->Destroy();
		throw std::runtime_error( "Failed to initialize importer for scene " + fname );
	}

	scene = FbxScene::Create( manager, "" );
	status = importer->Import( scene );
	importer->Destroy();
	if (!status) {
		scene->Destroy();
		throw std::runtime_error( "Failed to import scene " + fname );
	}
}

Scene::~Scene() {
	if (scene != NULL)
		scene->Destroy();
}

void Scene::traverse( Scene::TraverseFunction func, const VertexDescriptions & vertexDescriptions ) {
	std::stack <FbxNode*> nodeStack;

	nodeStack.push( scene->GetRootNode() );
	while (!nodeStack.empty()) {
		FbxNode * node = nodeStack.top();
		nodeStack.pop();
		(*func)( node, vertexDescriptions );
		for (int i = 0; i < node->GetChildCount(); ++i) {
			nodeStack.push( node->GetChild( i ) );
		}
	}
}