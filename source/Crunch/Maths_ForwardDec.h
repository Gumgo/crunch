/** @file Maths_ForwardDec.h
 *  @brief Contains forward declarations of the math classes.
 */

#ifndef MATHS_FORWARDDEC_DEFINED
#define MATHS_FORWARDDEC_DEFINED

template <typename T> class Vector2;
template <typename T> class Vector3;
template <typename T> class Vector4;
template <typename T> class Matrix22;
template <typename T> class Matrix33a;
template <typename T> class Matrix33;
template <typename T> class Matrix44a;
template <typename T> class Matrix44;
template <typename T> class AxisAngle;
template <typename T> class Quaternion;
template <typename T> class Line;
template <typename T> class Plane;
template <typename T> class Ray2;
template <typename T> class Ray3;

#endif