/** @file MeshResource.h
 *  @brief Contains the resource holding mesh data.
 */

#ifndef MESHRESOURCE_DEFINED
#define MESHRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "GpuBuffer.h"
#include <vector>

/** @brief Mesh resource.
 */
DEFINE_RESOURCE_CLASS( MeshResource, "mesh", "meshes" )
public:
	/** @brief The constructor.
	 */
	MeshResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief A vertex attribute description.
	 */
	struct AttributeData {
		std::string name;	/**< The attribute name.*/
		GLenum type;		/**< The attribute type.*/
		bool normalized;	/**< Whether the attribute should be normalized.*/
		bool integer;		/**< Whether the attribute should be interpreted as an integer.*/
		size_t components;	/**< The number of components.*/
		size_t stride;		/**< The number of bytes between two consecutive attributes.*/
		size_t offset;		/**< The offset into the data buffer of the first attribute.*/
	};

	const Vector3f & getMinBound() const;					/**< Returns the minimum bound of this mesh.*/
	const Vector3f & getMaxBound() const;					/**< Returns the maximum bound of this mesh.*/
	size_t getAttributeCount() const;						/**< Returns the number of vertex attributes.*/
	const AttributeData & getAttribute( size_t i ) const;	/**< Returns a vertex attribute.*/
	size_t getIndexSize() const;							/**< Returns the size in bytes of each index.*/
	GpuBufferReference getVertexBuffer();					/**< Returns the buffer containing the mesh's vertices on the GPU.*/
	GpuBufferConstReference getVertexBuffer() const;		/**< Returns the buffer containing the mesh's vertices on the GPU.*/
	GpuBufferReference getIndexBuffer();					/**< Returns the buffer containing the mesh's indices on the GPU.*/
	GpuBufferConstReference getIndexBuffer() const;			/**< Returns the buffer containing the mesh's indices on the GPU.*/
	size_t getVertexCount() const;							/**< Returns the highest index in the index buffer plus one.*/
	size_t getIndexCount() const;							/**< Returns the number of indices in the mesh.*/

private:
	Vector3f minBound;	// minimum bound of this mesh
	Vector3f maxBound;	// maximum bound of this mesh

	std::vector <AttributeData> attributes;	// list of vertex attributes
	std::vector <byte> vertexData;			// mesh vertex data
	size_t vertexCount;						// the number of vertices - taken to be the maximum index plus 1

	size_t indexSize;				// size of each index (1, 2, or 4 bytes)
	std::vector <byte> indexData;	// mesh index data
	size_t indexCount;				// the number of indices

	GpuBufferReference vertexBuffer;	// buffer containing the mesh's vertices on the GPU
	GpuBufferReference indexBuffer;		// buffer defining the mesh's indices on the GPU
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <MeshResource> MeshResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <MeshResource> MeshResourceConstReference;

#endif