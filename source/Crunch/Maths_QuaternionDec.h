/** @file Maths_QuaternionDec.h
 *  @brief Contains an implementation of a quaternion class.
 */

#ifndef MATHS_QUATERNIONDEC_DEFINED
#define MATHS_QUATERNIONDEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for quaternions.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Quaternion {
public:
	T r;	 /**< The (real) r component.*/
	T i;	 /**< The i component.*/
	T j;	 /**< The j component.*/
	T k;	 /**< The k component.*/

	/** @brief Constructs a quaternion with the default value.
	 */
	Quaternion();

	/** @brief Constructs a quaternion by copying the one provided.
	 */
	Quaternion( const Quaternion <T> & q );

	/** @brief Constructs a quaternion with the provided components.
	 */
	Quaternion( T pr, T pi, T pj, T pk );

	/** @brief Constructs a quaternion using an axis/angle.
	 */
	Quaternion( const Vector3 <T> & xyz, T a );

	/** @brief Constructs a quaternion using an axis/angle.
	 */
	Quaternion( const AxisAngle <T> & aa );

	/** @brief The destructor.
	 */
	~Quaternion();

	// operators

	/** @brief Returns the sum of the current value and the quaternion q.
	 */
	Quaternion <T> operator+( const Quaternion <T> & q ) const;

	/** @brief Returns the current value unmodified.
	 */
	Quaternion <T> operator+() const;

	/** @brief Returns the difference between the current value and the quaternion q.
	 */
	Quaternion <T> operator-( const Quaternion <T> & q ) const;

	/** @brief Returns the current value negated.
	 */
	Quaternion <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar s.
	 */
	Quaternion <T> operator*( T s ) const;

	/** @brief Returns the quotient of the current value and the scalar s.
	 */
	Quaternion <T> operator/( T s ) const;

	/** @brief Returns the product of the current value and the quaternion q.
	 */
	Quaternion <T> operator*( const Quaternion <T> & q ) const;

	/** @brief Returns the product of the current value and the 3D vector v.
	 */
	Vector3 <T> operator*( const Vector3 <T> & v ) const;

	/** @brief Sets the current value to the quaternion q.
	 */
	Quaternion <T> operator=( const Quaternion <T> & q );

	/** @brief Increments the current value by the quaternion q.
	 */
	Quaternion <T> operator+=( const Quaternion <T> & q );

	/** @brief Decrements the current value by the quaternion q.
	 */
	Quaternion <T> operator-=( const Quaternion <T> & q );

	/** @brief Multiplies the current value by the scalar s.
	 */
	Quaternion <T> operator*=( T s );

	/** @brief Divides the current value by the scalar s.
	 */
	Quaternion <T> operator/=( T s );

	/** @brief Multiplies the current value by the quaternion q.
	 */
	Quaternion <T> operator*=( const Quaternion <T> & q );

	/** @brief Returns whether the current value is equal to the quaternion q.
	 */
	bool operator==( const Quaternion <T> & q ) const;

	/** @brief Returns whether the current value is not equal to the quaternion q.
	 */
	bool operator!=( const Quaternion <T> & q ) const;

	/** @brief Accesses the <i>t</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U t );

	/** @brief Returns the <i>t</i>th component of the current value.
	 */
	template <typename U> T operator[]( U t ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Quaternion <U> () const;

	// mutable

	/** @brief Sets the current value's components to the quaternion q's components.
	 */
	void set( T pr, T pi, T pj, T pk);

	/** @brief Sets the current value using an axis/angle.
	 */
	void set( const Vector3 <T> & xyz, T a );

	/** @brief Sets the current value using an axis/angle.
	 */
	void set( const AxisAngle <T> & aa );

	/** @brief Normalizes the current value.
	 */
	void normalize();

	/** @brief Conjugates the current value.
	 */
	void conjugate();

	/** @brief Sets the magnitude of the current value value to the scalar m.
	 */
	void setMagnitude( T m );

	/** @brief Rotates the current value by the quaternion q.
	 */
	void rotate( const Quaternion <T> & q );

	// immutable

	/** @brief Returns the current value normalized.
	 */
	Quaternion <T> normalized() const;

	/** @brief Returns the conjugate of the current value.
	 */
	Quaternion <T> getConjugate() const;

	/** @brief Returns the magnitude of the current value.
	 */
	T magnitude() const;

	/** @brief Returns the squared magnitude of the current value.
	 */
	T magnitudeSquared() const;

	/** @brief Returns the dot product of the current value and the quaternion q.
	 */
	T dot( const Quaternion <T> & q ) const;

	/** @brief Returns the axis of the current value.
	 */
	Vector3 <T> getAxis() const;

	/** @brief Returns the angle of the current value.
	 */
	T getAngle() const;

	/** @brief Returns the current value rotated by the quaternion q.
	 */
	Quaternion <T> rotated( const Quaternion <T> & q ) const;

	/** @brief Rotates the 3D vector v by the current value.
	 */
	Vector3 <T> apply( const Vector3 <T> & v ) const;
};

/** @brief Returns the product of the scalar r and the quaternion q.
 */
template <typename T> Quaternion <T> operator*( T s, const Quaternion <T> & q );

/** @brief Interpolates between a and b by amount c using spherical linear interpolation.
 */
template <typename T, typename S> Quaternion <T> slerp( const Quaternion <T> & a, const Quaternion <T> & b, S c );

/** @brief Interpolates between a and by by amount c using normalized linear interpolation.
 */
template <typename T, typename S> Quaternion <T> nlerp( const Quaternion <T> & a, const Quaternion <T> & b, S c );

/** @brief Quaternion of type float.
 */
typedef Quaternion <float> Quaternionf;

/** @brief Quaternion of type double.
 */
typedef Quaternion <double> Quaterniond;

#endif