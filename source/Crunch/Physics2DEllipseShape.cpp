#include "Physics2DEllipseShape.h"
#include "Physics2DBody.h"

namespace Physics2D {

EllipseShape::EllipseShape( const EllipseShapeDefinition & d, Body * par )
	: Shape( d, Shape::T_ELLIPSE, par ) {
	radii = d.radii;

	setArea( computeArea() );
}

Real EllipseShape::computeArea() const {
	return boost::math::constants::pi <Real>() * radii.x * radii.y;
}

void EllipseShape::setRadii( const Vector2r & rad ) {
	radii = rad;
	setArea( computeArea() );
}

Vector2r EllipseShape::getRadii() const {
	return radii;
}

Aabb EllipseShape::getAabb( const Matrix33ar & transformation ) const {
	Matrix33ar tf = transformation * getTransform(); // world transform * shape transform

	// we want to find the points furthest along each axis after transformation
	// instead, we apply the inverse transformation of the vectors pointing along each axis
	// we only want the inverse of the rotation component, and the inverse of a rotation matrix is its transpose
	// elements [0,3] and [1,4] are the axis vectors, so the inverse (i.e. transpose) is [0,1] and [3,4]
	Vector2r xVec( tf[0], tf[1] );
	Vector2r yVec( tf[3], tf[4] );

	// the equation for an ellipse is (Wcos(theta), Hsin(theta))
	// we want to find the points most in the direction xVec and yVec
	// let v be our vector (either xVec or yVec)
	// then we want to maximize (or minimize):
	// dot( (Wcos(theta), Hsin(theta)), v ) = v.x * W * cos(theta) + v.y * H * sin(theta)
	// deriving, we get
	// -v.x*W*sin(theta) + v.y*H*cos(theta)
	// solving this for 0 to obtain the maximizing or minimizing theta we get
	// v.y*H*cos(theta) = v.x*W*sin(theta)
	// (v.y*H)/(v.x*W) = tan(theta)
	// theta = atan((v.y*H)/(v.x*W))

	// and so the maximized point (on the untransformed ellipse) is
	// p = ( v.x * cos( theta ), v.y * sin( theta ) )
	//   = ( v.x * cos( atan((v.y*H)/(v.x*W)) ), v.y * sin( atan((v.y*H)/(v.x*W)) ) )
	// let c = (v.y*H)/(v.x*W)
	// then
	// p = (v.x * cos( atan( c ) ), v.y * sin( atan( c ) ) )
	//   = (v.x / sqrt( c^2 + 1 ), v.y * c / sqrt( c^2 + 1 ) )

	// for each of these we need to check the case where c is +- infinity (due to the denominator being 0)
	// for the x component:
	//   atan(inf) = pi/2 ==> cos(pi/2) = 0
	//   atan(-inf) = -pi/2 ==> cos(-pi/2) = 0
	// for the y component:
	//   atan(inf) = pi/2 ==> sin(pi/2) = 1
	//   atan(-inf) = -pi/2 ==> sin(-pi/2) = -1

	Real cXDenom = xVec.x*radii.x;
	Vector2r pointX;
	if (cXDenom == 0.0f)
		// the case where this happens corresponds to theta being pi/2 or -pi/2. Since we account
		// for mirroring later, we just assume it's pi/2 (i.e. +infinity for the atan parameter)
		pointX.set( 0.0f, radii.y );
	else {
		Real cXNum = xVec.y*radii.y;
		Real cX = cXNum / cXDenom;
		Real iDenomX = (Real)1 / sqrt( cX*cX + 1 );
		pointX.set( radii.x * iDenomX, radii.y * cX * iDenomX );
	}

	Real cYDenom = yVec.x*radii.x;
	Vector2r pointY;
	if (cYDenom == 0.0f)
		// see above - same thing
		pointY.set( 0.0f, radii.y );
	else {
		Real cYNum = yVec.y*radii.y;
		Real cY = cYNum / cYDenom;
		Real iDenomY = (Real)1 / sqrt( cY*cY + 1 );
		pointY.set( radii.x * iDenomY, radii.y * cY * iDenomY );
	}

	// now transform the points
	Vector2r px0 = tf.transformPoint( pointX );
	Vector2r py0 = tf.transformPoint( pointY );
	// by symmetry, the other bounds are the negatives
	Vector2r px1 = tf.transformPoint( -pointX );
	Vector2r py1 = tf.transformPoint( -pointY );
	Vector2r minPt = vecMin( vecMin( px0, px1 ), vecMin( py0, py1 ) );
	Vector2r maxPt = vecMax( vecMax( px0, px1 ), vecMax( py0, py1 ) );
	return Aabb( minPt, maxPt );
}

void EllipseShape::computeMassData() {
	// to compute inertia we integrate: density * d(x)^2 dA over the disk of radius R
	// where d(x)^2 is the distance from the point x to the center of mass c, which is the origin
	// we use the change of variables: x = a*r*cos(theta), y = b*r*sin(theta) for theta in [0,2pi], r in [0,1]
	// det D_J = a*b*r, so applying the change of variables we get
	// a*b*density * int_0^2pi int_0^1 [a*r*cos(theta)^2 + b*r*sin(theta)^2] r dr dtheta
	// solving this integral results in
	// density*pi*a*b*(1/4)(a^2 + b^2)
	// which is m(a^2+b^2)/4
	// rewritten: m|radii|^2 / 4

	Real mass = getDensity() * getArea();
	Real inertia = mass*radii.magnitudeSquared()*(Real)0.25;
	Vector2r massCtr;
	if (isCenterOfMassOverridden()) {
		inertia += mass*getOverriddenCenterOfMass().magnitudeSquared();
		massCtr = getOverriddenCenterOfMass();
	}
	setMassData( MassData( getTransform().transformPoint( massCtr ), mass, inertia ) );
}

EllipseShapeDefinition::EllipseShapeDefinition() {
	radii.set( (Real)1, (Real)1 );
}

Shape * EllipseShapeDefinition::createShape( Body * parent ) const {
	return new EllipseShape( *this, parent );
}

}