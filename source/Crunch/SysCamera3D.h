#ifndef SYSCAMERA3D_DEFINED
#define SYSCAMERA3D_DEFINED

#include "Common.h"
#include "SysFrustum.h"

// TODO: make this more general to support ortho (or arbitrary projections)
class SysCamera3D {
public:
	void setProjection( float pFov, float pAspectRatio, float pNear, float pFar );
	void setCameraTarget( const Vector3f & from, const Vector3f & to, const Vector3f & up );
	void setCameraDirection( const Vector3f & from, const Vector3f & direction, const Vector3f & up );
	void setViewMatrix( const Matrix44af & matrix );
	void setCameraMatrix( const Matrix44af & matrix );

	float getFov() const;
	float getAspectRatio() const;
	float getNear() const;
	float getFar() const;

	const Matrix44f & getProjectionMatrix() const;
	const Matrix44af & getViewMatrix() const;
	const Matrix44af & getCameraMatrix() const;
	const Matrix44f & getViewProjectionMatrix() const;
	Matrix44af getModelViewMatrix( const Matrix33f & modelMatrix ) const;
	Matrix44af getModelViewMatrix( const Matrix44af & modelMatrix ) const;
	Matrix44f getModelViewProjectionMatrix( const Matrix33f & modelMatrix ) const;
	Matrix44f getModelViewProjectionMatrix( const Matrix44af & modelMatrix ) const;

	const SysFrustum & getFrustum() const;

	// converts a screen point to a world point
	// each component of screen ranges from 0 to 1 and represents position after projection
	// note: the resulting ray's direction is NOT normalized
	Ray3f screenToWorld( const Vector2f & screen ) const;

private:
	float projFov, projAspectRatio, projNear, projFar;
	Matrix44f projectionMatrix;
	Matrix44af viewMatrix;
	Matrix44af cameraMatrix; // inverse of viewMatrix
	Matrix44f viewProjectionMatrix;

	void computeViewProjectionMatrix();

	// the frustum
	SysFrustum frustum;
};

#endif