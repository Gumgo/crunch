#include "SysTerrain.h"
#include "Context.h"
#include "SysRenderer2D.h"
#include "Log.h"

SysTerrain::SysTerrain()
	: chunkMap( 80 ) {
	initialized = false;
	debugCamera = NULL;
}

SysTerrain::~SysTerrain() {
	// TODO: only flush the terrain tasks - this will require modifying flush
	Context::get().taskQueue.flushTasks();
}

SysTerrain::Chunk::Chunk() {
}

SysTerrain::Chunk::Chunk( size_t l, const Vector2i & c )
	: lod( l )
	, coords( c ) {
}

bool SysTerrain::Chunk::operator==( const Chunk & o ) const {
	return lod == o.lod && coords == o.coords;
}

SysTerrain::Position::Position() {
}

SysTerrain::Position::Position( const SysTerrain::Chunk & c, const Vector2f & o )
	: chunk( c )
	, offset( o ) {
}

SysTerrain::Position::Position( size_t l, const Vector2i & c, const Vector2f & o )
	: chunk( l, c )
	, offset( o ) {
}

void SysTerrain::ChunkStatus::addFlags( size_t f ) {
	flags |= f;
}

void SysTerrain::ChunkStatus::removeFlags( size_t f ) {
	flags &= ~f;
}

bool SysTerrain::ChunkStatus::hasAllFlags( size_t f ) const {
	return (flags & f) == f;
}

bool SysTerrain::ChunkStatus::hasSomeFlags( size_t f ) const {
	return (flags & f) != 0;
}

SysTerrain::ChunkStatus::ChunkStatus() {
	flags = 0;
	childrenDisplayed = 0;
	parentDisplayed = false;
}

bool SysTerrain::setupTerrain( const Settings & s ) {
	if (initialized) {
		Log::warning() << "Failed to initialize terrain because it has already been initialized";
		return false;
	}

	// check if the chunk size is a power of 2
	bool found1 = false;
	for (size_t i = 0; i < sizeof( s.chunkSize )*8; ++i) {
		if ((s.chunkSize & (1 << i)) != 0) {
			if (!found1)
				found1 = true;
			else {
				// not a power of 2
				Log::error() << "Terrain chunk size must be a power of 2";
				return false;
			}
		}
	}

	if (s.chunkSize < 8 || s.chunkSize > 128) {
		Log::error() << "Invalid terrain chunk size";
		return false;
	}
	if (s.maxViewDist <= 0.0f) {
		Log::error() << "Invalid terrain view distance";
		return false;
	}
	if (s.scale <= 0.0f) {
		Log::error() << "Invalid terrain scale";
		return false;
	}
	if (s.cameraFov >= 180.0f || s.cameraFov <= 0.0f) {
		Log::error() << "Invalid camera FOV for determining terrain LOD";
		return false;
	}
	if (s.imagePlanePolygonFraction <= 0.0f) {
		Log::error() << "Invalid image plane polygon size for terrain";
		return false;
	}
	if (s.morphingEnabled && (s.morphRatio <= 0.0f || s.morphRatio > 1.0f)) {
		Log::error() << "Invalid morph ratio";
		return false;
	}
	if (s.dataProvider.empty()) {
		Log::error() << "No data provider specified";
		return false;
	}

	settings = s;

	chunkSize = s.chunkSize;
	scale = s.scale;
	invScale = 1.0f / scale;
	maxLod = s.maxLod;
	maxViewDist = s.maxViewDist * invScale;
	maxViewDist2 = maxViewDist*maxViewDist;
	morphingEnabled = s.morphingEnabled;
	morphRatio = s.morphRatio;
	dataProvider = s.dataProvider;

	float imagePlaneSize = 2.0f * tan( degToRad( s.cameraFov * 0.5f ) );

	lods.clear();
	for (size_t i = 0; i <= maxLod; ++i) {
		lods.push_back( Lod() );
		lods.back().chunkSize = (float)(chunkSize << i);
		lods.back().invChunkSize = 1.0f / lods.back().chunkSize;
		float chunkPolySize = (float)(1 << i);
		// solve for dist: (chunkPolySize/dist) / imagePlaneSize = imagePlanePolygonFraction
		lods.back().minDist2 = chunkPolySize / (s.imagePlanePolygonFraction * imagePlaneSize);
	}
	if (morphingEnabled) {
		for (size_t i = 0; i < maxLod; ++i)
			lods[i].morphDistances.set( lerp( lods[i].minDist2, lods[i+1].minDist2, 1.0f - morphRatio ), lods[i+1].minDist2 );
		lods.back().morphDistances.set( maxViewDist2*10.0f, maxViewDist2*10.0f+1.0f );
	} else {
		// if morphing isn't disabled, just set really distances
		for (size_t i = 0; i <= maxLod; ++i)
			lods[i].morphDistances.set( maxViewDist2*10.0f, maxViewDist2*10.0f+1.0f );
	}
	for (size_t i = 0; i <= maxLod; ++i)
		lods[i].minDist2 *= lods[i].minDist2;

	// reserve space for chunk loading data
	chunkLoadingData.resize( MAX_CHUNKS_LOADING );
	// initially they are all free for use
	for (size_t i = 0; i < chunkLoadingData.size(); ++i) {
		// reserve buffers
		chunkLoadingData[i].chunkData.points.resize( (chunkSize+1)*(chunkSize+1) );
		chunkLoadingData[i].chunkData.patches.resize( chunkSize*chunkSize );
		chunkLoadingData[i].chunkBufferData.vertices.resize( (chunkSize+1)*(chunkSize+1) );
		freeChunkLoadingData.push( &chunkLoadingData[i] );
	}

	chunkSizeBytes = (chunkSize+1)*(chunkSize+1)*sizeof( ChunkVertex );
	chunksPerVertexBuffer = (VERTEX_BUFFER_SIZE + chunkSizeBytes - 1) / chunkSizeBytes;
	vertexBufferSize = chunksPerVertexBuffer * chunkSizeBytes;
	while (freeVertexBufferLocations.size() < INITIAL_VERTEX_BUFFER_CHUNKS)
		allocateVertexBuffer();

	generateIndexBuffer();

	initialized = true;
	return true;
}

SysTerrain::Position SysTerrain::convertLod( const Position & p, size_t toLod ) const {
	Position to;
	to.chunk.lod = toLod;
	if (p.chunk.lod < toLod) {
		size_t diff = toLod - p.chunk.lod;
		to.chunk.coords.set( p.chunk.coords.x >> diff, p.chunk.coords.y >> diff );
		Vector2i chunkOffset = p.chunk.coords - Vector2i( to.chunk.coords.x << diff, to.chunk.coords.y << diff );
		to.offset = p.offset + (Vector2f)(chunkOffset * (chunkSize << p.chunk.lod));
	} else {
		size_t diff = p.chunk.lod - toLod;
		to.chunk.coords.set( p.chunk.coords.x << diff, p.chunk.coords.y << diff );
		Vector2f chunkOffset( floor( p.offset.x * lods[toLod].invChunkSize ), floor( p.offset.y * lods[toLod].invChunkSize ) );
		to.chunk.coords += (Vector2i)chunkOffset;
		to.offset = p.offset - chunkOffset * lods[toLod].chunkSize;
	}
	return to;
}

Vector2i SysTerrain::convertLod( size_t lod, const Vector2i & chunk, size_t toLod ) const {
	if (lod < toLod) {
		size_t diff = toLod - lod;
		return Vector2i( chunk.x >> diff, chunk.y >> diff );
	} else {
		size_t diff = lod - toLod;
		return Vector2i( chunk.x << diff, chunk.y << diff );
	}
}

void SysTerrain::correctOffset( SysTerrain::Position & p ) const {
	Vector2f chunkOffset( floor( p.offset.x * lods[p.chunk.lod].invChunkSize ), floor( p.offset.y * lods[p.chunk.lod].invChunkSize ) );
	p.chunk.coords += (Vector2i)chunkOffset;
	p.offset -= lods[p.chunk.lod].chunkSize * chunkOffset;
}

float SysTerrain::distanceSquared( const Position & p, const Vector2i & chunk ) const {
	// assumes offsets are corrected
	if (p.chunk.coords == chunk)
		// inside chunk
		return 0.0f;
	else if (p.chunk.coords.x == chunk.x) {
		// x chunk is same - measure distance to y edge
		float dist = (float)(std::abs( p.chunk.coords.y - chunk.y ) - 1) * lods[p.chunk.lod].chunkSize;
		dist += (chunk.y < p.chunk.coords.y) ? p.offset.y : lods[p.chunk.lod].chunkSize - p.offset.y;
		return dist*dist;
	} else if (p.chunk.coords.y == chunk.y) {
		// y chunk is same - measure distance to x edge
		float dist = (float)(std::abs( p.chunk.coords.x - chunk.x ) - 1) * lods[p.chunk.lod].chunkSize;
		dist += (chunk.x < p.chunk.coords.x) ? p.offset.x : lods[p.chunk.lod].chunkSize - p.offset.x;
		return dist*dist;
	} else {
		// measure distance to corners
		Vector2i corner = chunk;
		// determine corner to test based on chunk position relative to p
		if (chunk.x < p.chunk.coords.x)
			++corner.x;
		if (chunk.y < p.chunk.coords.y)
			++corner.y;
		Vector2f xyDist = lods[p.chunk.lod].chunkSize * (Vector2f)(corner - p.chunk.coords) - p.offset;
		return xyDist.magnitudeSquared();
	}
}

void SysTerrain::update( const Vector2f & center ) {
	if (!initialized) {
		Log::warning() << "Terrain system not initialized";
		return;
	}

	// convert world coordinate to chunk/offset
	Position p( 0, Vector2i(), center*invScale );
	correctOffset( p );

	update( p.chunk.coords, p.offset );
}

void SysTerrain::update( const Vector2i & chunk, const Vector2f & offset ) {
	if (!initialized) {
		Log::warning() << "Terrain system not initialized";
		return;
	}

	currentCenter = Position( 0, chunk, offset );

	// figure out what chunks and LODs should be visible and at what LOD
	// do this by starting at the top and subdividing

	// first we start at the top level
	lods[maxLod].center = convertLod( Position( 0, chunk, offset ), maxLod );
	start = lods[maxLod].center;
	start.offset.x -= maxViewDist;
	start.offset.y -= maxViewDist;
	correctOffset( start );
	end = lods[maxLod].center;
	end.offset.x += maxViewDist;
	end.offset.y += maxViewDist;
	correctOffset( end );

	for (size_t i = 0; i < maxLod; ++i)
		lods[i].center = convertLod( lods[maxLod].center, i );

	// first clear the per-frame flags - these will be re-determined when we loop over chunks in range
	possiblyDisplayedChunks.clear();
	ChunkMap::Iterator it = chunkMap.getIterator();
	while (it.next()) {
		ChunkStatus & s = it.getValue();
		if (s.hasAllFlags( ChunkStatus::F_DISPLAYING )) {
			it.getValue().addFlags( ChunkStatus::F_DISPLAYED );
			possiblyDisplayedChunks.push_back( it.getKey() );
		} else
			it.getValue().removeFlags( ChunkStatus::F_DISPLAYED );
		s.removeFlags( ChunkStatus::F_SHOULD_BE_LOADED | ChunkStatus::F_SHOULD_DISPLAY | ChunkStatus::F_DISPLAYING );
		s.childrenDisplayed = 0;
		s.parentDisplayed = false;
	}

	subdivisionStack.clear();
	// examine each chunk - subdivide for LOD when needed
	for (int y = start.chunk.coords.y; y <= end.chunk.coords.y; ++y) {
		for (int x = start.chunk.coords.x; x <= end.chunk.coords.x; ++x) {
			subdivisionStack.push_back( Chunk( maxLod, Vector2i( x, y ) ) );
			// keep subdividing until low enough LOD
			while (!subdivisionStack.empty()) {
				Chunk chunk = subdivisionStack.back();
				subdivisionStack.pop_back();
				float dist2 = distanceSquared( lods[chunk.lod].center, chunk.coords );
				if (dist2 > maxViewDist2)
					// out of range - don't include this chunk
					continue;

				// update per-frame flags
				size_t chunkMapIndex;
				if (!chunkMap.getIndex( chunk, chunkMapIndex ))
					// if it's not in the chunk map, add it
					chunkMapIndex = chunkMap.put( chunk, ChunkStatus() );
				ChunkStatus & s = chunkMap.getByIndex( chunkMapIndex );

				// the chunk is in range, so it should be loaded
				s.addFlags( ChunkStatus::F_SHOULD_BE_LOADED );
				if (!s.hasAllFlags( ChunkStatus::F_DISPLAYED ))
					// if it has the DISPLAYED flag, it's already in the list
					possiblyDisplayedChunks.push_back( chunk );

				if (chunk.lod > 0 && dist2 < lods[chunk.lod].minDist2) {
					Vector2i subChunk = convertLod( chunk.lod, chunk.coords, chunk.lod-1 );
					subdivisionStack.push_back( Chunk( chunk.lod-1, subChunk ) );
					subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x+1, subChunk.y ) ) );
					subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x, subChunk.y+1 ) ) );
					subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x+1, subChunk.y+1 ) ) );
				} else
					// we've subdivided enough - this is the optimal chunk that should be displayed (if it's loaded and connected)
					s.addFlags( ChunkStatus::F_SHOULD_DISPLAY );
			}
		}
	}

	// we've determined what chunks should be loaded and optimally displayed
	// now we update the chunks
	it = chunkMap.getIterator();
	while (it.next()) {
		const Chunk & chunk = it.getKey();
		ChunkStatus & status = it.getValue();

		// skip if the skip flag is set - means this chunk is about to get removed
		if (status.hasAllFlags( ChunkStatus::F_SKIP ))
			continue;

		// if it's loading, check to see if it's done
		if (status.hasAllFlags( ChunkStatus::F_LOADING )) {
			// if the chunk should NOT be loading AND we have NOT tried to cancel it, then try to cancel it
			if (!status.hasSomeFlags( ChunkStatus::F_SHOULD_BE_LOADED | ChunkStatus::F_ATTEMPTED_CANCEL )) {
				// check if we successfully canceled the task
				if (Context::get().taskQueue.cancelTask( status.loadingTaskId )) {
					// success - free up the chunk loading data and remove this chunk
					freeChunkLoadingData.push( status.chunkLoadingData );
					it.remove();
					continue; // make sure not to try to access this chunk again!
				} else
					// we couldn't cancel it - set the ATTEMPTED_CANCEL flag so we don't needlessly keep trying
					status.addFlags( ChunkStatus::F_ATTEMPTED_CANCEL );
			}

			// check the task status (task is auto-released if done)
			if (Context::get().taskQueue.getTaskStatus( status.loadingTaskId ) == TaskQueue::TS_COMPLETE) {
				status.removeFlags( ChunkStatus::F_LOADING );
				status.addFlags( ChunkStatus::F_LOADED );
				// load data into a vertex buffer
				if (freeVertexBufferLocations.empty())
					allocateVertexBuffer();
				status.vertexBufferLocation = freeVertexBufferLocations.top();
				freeVertexBufferLocations.pop();
				status.vertexBufferLocation.vertexBuffer->write(
					status.vertexBufferLocation.byteOffset, chunkSizeBytes,
					&status.chunkLoadingData->chunkBufferData.vertices[0] );
				// set the metadata
				status.metadata = status.chunkLoadingData->chunkBufferData.metadata;
				// release the loading data
				freeChunkLoadingData.push( status.chunkLoadingData );
			}
		}

		// start the chunk loading if it should be loaded and isn't
		if (status.hasAllFlags( ChunkStatus::F_SHOULD_BE_LOADED )) {
			// only start loading if it isn't already loading or loaded
			if (!status.hasSomeFlags( ChunkStatus::F_LOADING | ChunkStatus::F_LOADED )) {
				if (startLoadingChunk( chunk, status ))
					status.addFlags( ChunkStatus::F_LOADING );
			}
			// if it's cached, remove it from the cache, since it's needed now
			if (status.hasAllFlags( ChunkStatus::F_CACHED )) {
				status.removeFlags( ChunkStatus::F_CACHED );
				chunkCache.uncache( status.cacheIndex );
			}
		} else {
			if (!status.hasSomeFlags( ChunkStatus::F_LOADING | ChunkStatus::F_LOADED )) {
				// not loaded; simply remove it
				it.remove();
				continue;
			} else if (status.hasAllFlags( ChunkStatus::F_LOADED ) && !status.hasSomeFlags( ChunkStatus::F_DISPLAYED | ChunkStatus::F_CACHED )) {
				// it's loaded but we don't need it - cache it
				// but don't cache if it was displayed the last update - we might still need to display it
				status.addFlags( ChunkStatus::F_CACHED );
				Chunk removedChunk;
				if (chunkCache.cache( chunk, status.cacheIndex, removedChunk )) {
					// a chunk got removed from the cache
					// if this chunk shouldn't be loaded, set its skip flag so its not recached if it hasn't been iterated over yet
					ChunkStatus & removedStatus = chunkMap.get( removedChunk );
					removedStatus.removeFlags( ChunkStatus::F_CACHED );
					if (!removedStatus.hasAllFlags( ChunkStatus::F_SHOULD_BE_LOADED )) {
						removedStatus.addFlags( ChunkStatus::F_SKIP );
						// we also free its data here
						freeVertexBufferLocations.push( removedStatus.vertexBufferLocation );
						chunksToRemove.push_back( removedChunk );
					}
				}
			}
		}
	}

	for (size_t i = 0; i < chunksToRemove.size(); ++i)
		chunkMap.remove( chunksToRemove[i] );
	chunksToRemove.clear();

	// used for determining AABB
	Vector2i chunkMin( std::numeric_limits <int>::max(), std::numeric_limits <int>::max() );
	Vector2i chunkMax( std::numeric_limits <int>::min(), std::numeric_limits <int>::min() );
	Vector2f zMinMax( std::numeric_limits <float>::max(), -std::numeric_limits <float>::max() );
	if (!possiblyDisplayedChunks.empty()) {
		struct LodSort {
			bool operator()( const Chunk & a, const Chunk & b ) const {
				return a.lod < b.lod;
			}
		};

		// we make two passes on the list of possibly displayed chunks
		// in the first pass, we determine if children of higher LODs are ready for display
		// this is so that we can disable display of the higher LOD if all children are ready
		// (i.e. any set of [grand]*children that cover all 4 direct children)
		// this is more convenient if the list is first ordered (because we only need to look 1 level up each time)
		// in the second pass, we disable the display of children whose [grand]*parents are being displayed

		std::sort( possiblyDisplayedChunks.begin(), possiblyDisplayedChunks.end(), LodSort() );
		// first pass
		for (size_t i = 0; i < possiblyDisplayedChunks.size(); ++i) {
			const Chunk & c = possiblyDisplayedChunks[i];
			ChunkStatus & s = chunkMap.get( c );
			if (c.lod > 0) {
				// if some children are out of range, we shouldn't count them against us; set their flags
				Vector2i baseCoords = convertLod( c.lod, c.coords, c.lod-1 );
				Vector2i add;
				for (add.y = 0; add.y <= 1; ++add.y) {
					for (add.x = 0; add.x <= 1; ++add.x) {
						int bitIndex = 2*add.y + add.x;
						if ((s.childrenDisplayed & (1 << bitIndex)) == 0) {
							// activate the bit if the child is out of range
							float dist2 = distanceSquared( lods[c.lod-1].center, baseCoords + add );
							if (dist2 > maxViewDist2)
								s.childrenDisplayed |= (1 << bitIndex);
						}
					}
				}
			}
			if (c.lod < maxLod && (s.childrenDisplayed == 0xF || s.hasAllFlags( ChunkStatus::F_DISPLAYED ) || s.hasAllFlags( ChunkStatus::F_LOADED | ChunkStatus::F_SHOULD_DISPLAY ))) {
				// if this chunk or all [grand]*children should display or have previously just displayed,
				// then alert the parent chunk: this chunk (or its [grand]*children) is ready for display OR can keep displaying if other chunks aren't ready
				Chunk parent( c.lod+1, convertLod( c.lod, c.coords, c.lod+1 ) );
				Vector2i baseCoords = convertLod( parent.lod, parent.coords, c.lod );
				int bitIndex = 2*(c.coords.y - baseCoords.y) + (c.coords.x - baseCoords.x);
				chunkMap.get( parent ).childrenDisplayed |= (1 << bitIndex);
			}
		}
		// second pass - we go in reverse this time
		for (size_t i = 0; i < possiblyDisplayedChunks.size(); ++i) {
			// if this chunk is loaded and should be displayed, then we should display - tell our children NOT to display
			// if this chunk is loaded, has just displayed, but now shouldn't be displayed:
			//   if our [grand]*children should and can all be displayed, stop displaying
			//   if a [grand]*parent told us to stop displaying, stop displaying
			//   otherwise, check if we're out of range; if so, stop displaying, and otherwise, keep displaying (and tell children not to display)
			// if we're displaying or a parent is displaying, pass the message down
			const Chunk & c = possiblyDisplayedChunks[possiblyDisplayedChunks.size()-i-1];
			ChunkStatus & s = chunkMap.get( c );

			bool shouldDisplay = false;
			if (!s.parentDisplayed) {
				// don't display if the parent is being displayed, because this means either this chunk or surrounding ones aren't loaded
				if (s.hasAllFlags( ChunkStatus::F_LOADED | ChunkStatus::F_SHOULD_DISPLAY ))
					// loaded and should display
					shouldDisplay = true;
				else if (s.hasAllFlags( ChunkStatus::F_DISPLAYED ) && !s.hasAllFlags( ChunkStatus::F_SHOULD_DISPLAY ) && s.childrenDisplayed != 0xF) {
					// we just displayed, but we should no longer display, and the children can't display
					// we keep displaying if we're in range
					float dist2 = distanceSquared( lods[c.lod].center, c.coords );
					// display if we're in range; otherwise, don't
					shouldDisplay = !(dist2 > maxViewDist2);
				}
			}
			if (shouldDisplay) {
				s.addFlags( ChunkStatus::F_DISPLAYING );
				Vector2i coordsMin = convertLod( c.lod, c.coords, 0 );
				Vector2i coordsMax = convertLod( c.lod, c.coords + Vector2i( 1, 1 ), 0 );
				chunkMin = vecMin( chunkMin, coordsMin );
				chunkMax = vecMax( chunkMax, coordsMax );
				zMinMax[0] = std::min( zMinMax[0], s.metadata.zBounds[0] );
				zMinMax[1] = std::max( zMinMax[1], s.metadata.zBounds[1] );
			}
			if (c.lod > 0 && (shouldDisplay || s.parentDisplayed)) {
				// either this chunk or a parent is displaying, so tell children not to
				Vector2i baseChildCoords = convertLod( c.lod, c.coords, c.lod-1 );
				Vector2i add;
				for (add.y = 0; add.y <= 1; ++add.y) {
					for (add.x = 0; add.x <= 1; ++add.x) {
						size_t childIndex;
						if (chunkMap.getIndex( Chunk( c.lod-1, baseChildCoords + add ), childIndex ))
							// if child exists, set it to not display
							chunkMap.getByIndex( childIndex ).parentDisplayed = true;
					}
				}
			}
		}

		// now that we've determined all the chunks to display, we need to patch them together by selecting the correct index buffers
		// to do this, we subdivide until we reach the chunk that is displayed
		// we then examine the chunks on each side, moving up the LODs until we find one without the display flag
		subdivisionStack.clear();
		for (int y = start.chunk.coords.y; y <= end.chunk.coords.y; ++y) {
			for (int x = start.chunk.coords.x; x <= end.chunk.coords.x; ++x) {
				Chunk c( maxLod, Vector2i( x, y ) );
				if (!chunkMap.contains( c ))
					continue;

				subdivisionStack.push_back( c );
				while (!subdivisionStack.empty()) {
					Chunk chunk = subdivisionStack.back();
					subdivisionStack.pop_back();

					// check if this chunk is being displayed
					size_t chunkIndex;
					if (!chunkMap.getIndex( chunk, chunkIndex ))
						continue;
					ChunkStatus & status = chunkMap.getByIndex( chunkIndex );
					if (chunk.lod > 0 && !status.hasAllFlags( ChunkStatus::F_DISPLAYING )) {
						Vector2i subChunk = convertLod( chunk.lod, chunk.coords, chunk.lod-1 );
						subdivisionStack.push_back( Chunk( chunk.lod-1, subChunk ) );
						subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x+1, subChunk.y ) ) );
						subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x, subChunk.y+1 ) ) );
						subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x+1, subChunk.y+1 ) ) );
					} else {
						// we've found the chunk to be displayed, so check the surrounding chunks up the LOD chain
						static const int addX[4] = { 0, 1, 0, -1 };
						static const int addY[4] = { -1, 0, 1, 0 };
						for (size_t s = 0; s < 4; ++s) {
							status.lodDifference[s] = 0; // initially assume no difference
							Chunk up = chunk;
							up.coords.x += addX[s];
							up.coords.y += addY[s];
							// loop up to max LOD
							while (up.lod <= maxLod) {
								size_t cIdx;
								if (chunkMap.getIndex( up, cIdx )) {
									const ChunkStatus & cs = chunkMap.getByIndex( cIdx );
									if (cs.hasAllFlags( ChunkStatus::F_DISPLAYING )) {
										// set the LOD difference and break - we've found the displayed chunk
										status.lodDifference[s] = up.lod - chunk.lod;
										break;
									}
								}
								up.coords = convertLod( up.lod, up.coords, up.lod+1 );
								++up.lod;
							}
						}
					}
				}
			}
		}
	}

	if (chunkMin.x > chunkMax.x)
		// empty AABB
		aabb = SysAabb::EMPTY;
	else
		aabb = SysAabb(
			Vector3f( (Vector2f)chunkMin * lods[0].chunkSize * settings.scale, zMinMax[0] ),
			Vector3f( (Vector2f)chunkMax * lods[0].chunkSize * settings.scale, zMinMax[1] ) );
}

SysTerrain::ChunkCache::ChunkCache() {
	entries.resize( CACHE_SIZE );
	for (size_t i = 0; i < CACHE_SIZE-1; ++i)
		entries[i].next = i+1;
	entries[CACHE_SIZE-1].next = NO_INDEX;
	firstFreeEntry = 0;
	mostRecentEntry = leastRecentEntry = NO_INDEX;
}

bool SysTerrain::ChunkCache::cache( const Chunk & chunk, size_t & idx, Chunk & removedChunk ) {
	bool ret = false;
	if (firstFreeEntry == NO_INDEX) {
		// remove oldest entry from cache
		ret = true;
		// store the chunk that is being removed
		removedChunk = entries[leastRecentEntry].chunk;
		// point the previous entry to nothing since it is now the last entry
		entries[entries[leastRecentEntry].prev].next = NO_INDEX;
		// leastRecentEntry now becomes the first free entry
		firstFreeEntry = leastRecentEntry;
		// "next" already points to NO_INDEX
		// point leastRecentEntry to the new least recent entry
		leastRecentEntry = entries[leastRecentEntry].prev;
	}
	// store the chunk
	size_t entry = firstFreeEntry;
	firstFreeEntry = entries[firstFreeEntry].next;
	idx = entry;
	entries[entry].chunk = chunk;
	entries[entry].prev = NO_INDEX;
	entries[entry].next = mostRecentEntry;
	if (mostRecentEntry != NO_INDEX)
		entries[mostRecentEntry].prev = entry;
	mostRecentEntry = entry;
	if (leastRecentEntry == NO_INDEX)
		leastRecentEntry = entry;

	return ret;
}

void SysTerrain::ChunkCache::uncache( size_t idx ) {
	if (entries[idx].prev == NO_INDEX)
		mostRecentEntry = entries[idx].next;
	else
		entries[entries[idx].prev].next = entries[idx].next;
	if (entries[idx].next == NO_INDEX)
		leastRecentEntry = entries[idx].prev;
	else
		entries[entries[idx].next].prev = entries[idx].prev;
	entries[idx].next = firstFreeEntry;
	firstFreeEntry = idx;
}

void SysTerrain::allocateVertexBuffer() {
	GpuBuffer::BufferDescription desc;
	desc.type = GL_ARRAY_BUFFER;
	desc.size = vertexBufferSize;
	desc.usage = GL_DYNAMIC_DRAW;
	vertexBuffers.push_back( Context::get().gpuState.createBuffer( desc ) );
	for (size_t i = 0; i < vertexBufferSize; i += chunkSizeBytes) {
		VertexBufferLocation l;
		l.vertexBuffer = vertexBuffers.back();
		l.byteOffset = i;
		freeVertexBufferLocations.push( l );
	}
}

bool SysTerrain::startLoadingChunk( const Chunk & chunk, ChunkStatus & status ) {
	// make sure there aren't too many chunks already loading
	if (freeChunkLoadingData.empty())
		// no free chunk loading data available
		return false;

	status.chunkLoadingData = freeChunkLoadingData.top();
	freeChunkLoadingData.pop();

	status.chunkLoadingData->chunk = chunk;

	// add the task and store its ID
	status.loadingTaskId = Context::get().taskQueue.addTask(
		fastdelegate::FastDelegate1 <void*, void>( this, &SysTerrain::loadChunkTask ), status.chunkLoadingData );
	return true;
}

void SysTerrain::loadChunkTask( void * chunkLoadingDataPtr ) const {
	ChunkLoadingData * loadingData = (ChunkLoadingData*)chunkLoadingDataPtr;
	dataProvider( this, loadingData->chunk, &loadingData->chunkData );
	generateVertices( loadingData );
}

void SysTerrain::generateVertices( ChunkLoadingData * cld ) const {
	ChunkMetadata & m = cld->chunkBufferData.metadata;
	// prepare to compute z bounds
	m.zBounds.set( std::numeric_limits <float>::max(), -std::numeric_limits <float>::max() );
	std::fill( m.materialsEnabled, m.materialsEnabled + 4*MAX_MATERIALS, false );
	// loop through each vertex and set materials
	// we use <= because we include the extra row of vertices
	for (size_t y = 0; y <= chunkSize; ++y) {
		for (size_t x = 0; x <= chunkSize; ++x) {
			size_t i = y*(chunkSize+1) + x;
			const ChunkData::PointData & p = cld->chunkData.points[i];
			ChunkVertex & v = cld->chunkBufferData.vertices[i];
			m.zBounds[0] = std::min( m.zBounds[0], p.z );
			m.zBounds[1] = std::max( m.zBounds[1], p.z );
			// determine whether this point is inside each of the 4 sections
			bool in[4];
			in[0] = (x >= y) && (x <= chunkSize - y);
			in[1] = (x >= y) && (x >= chunkSize - y);
			in[2] = (x <= y) && (x >= chunkSize - y);
			in[3] = (x <= y) && (x <= chunkSize - y);
			for (size_t s = 0; s < 4; ++s) {
				if (!in[s])
					continue;
				for (size_t t = 0; t < MAX_MATERIALS; ++t) {
					if (p.materials[t] > 0)
						m.materialsEnabled[s*MAX_MATERIALS + t] = true;
				}
			}
			v.xyPatchEnabled[0] = (byte)x;
			v.xyPatchEnabled[1] = (byte)y;
			v.z[0] = v.z[1] = v.z[2] = p.z;
			v.normalM0[0] = v.normalM1[0] = v.normalM2[0] = (short)clamp( floor( p.normal.x*32767.5f ), -32768.0f, 32767.0f );
			v.normalM0[1] = v.normalM1[1] = v.normalM2[1] = (short)clamp( floor( p.normal.y*32767.5f ), -32768.0f, 32767.0f );
			for (size_t i = 0; i < MAX_MATERIALS - 1; ++i) {
				v.materials[i] = p.materials[i];
				v.materials[i + MAX_MATERIALS - 1] = p.materials[i];
				v.materials[i + 2*MAX_MATERIALS - 2] = p.materials[i];
			}
			v.xyPatchEnabled[3] = 0;
		}
	}

	if (morphingEnabled) {
		// generate the LOD morphs
		// we generate up to 2 levels
		struct Morph {
			ChunkLoadingData * cld;
			static void fixMaterials( byte * materials ) {
				byte largest = 0;
				size_t largestIndex = 0;
				uint sum = 0;
				for (size_t i = 0; i < MAX_MATERIALS; ++i) {
					if (materials[i] > largest) {
						largest = materials[i];
						largestIndex = i;
					}
					sum += materials[i];
				}
				if (sum < 255)
					materials[largestIndex] += (255-sum);
				else if (sum > 255)
					materials[largestIndex] -= (sum-255);
			}
			static void blend( float & z, short * normal, byte * materials,
				const ChunkData::PointData & p0, const ChunkData::PointData & p1, float w0, float w1 ) {
				z = p0.z*w0 + p1.z*w1;
				Vector3f n = (p0.normal*w0 + p1.normal*w1).normalized();
				normal[0] = (short)clamp( floor( n.x*32767.5f ), -32768.0f, 32767.0f );
				normal[1] = (short)clamp( floor( n.y*32767.5f ), -32768.0f, 32767.0f );
				byte mtl[MAX_MATERIALS];
				for (size_t i = 0; i < MAX_MATERIALS; ++i)
					mtl[i] = (byte)floor( (float)p0.materials[i]*w0 + (float)p1.materials[i]*w1 + 0.5f );
				fixMaterials( mtl );
				for (size_t i = 0; i < MAX_MATERIALS - 1; ++i)
					materials[i] = mtl[i];
			}
			static void blend( float & z, short * normal, byte * materials,
				const ChunkData::PointData & p0, const ChunkData::PointData & p1, const ChunkData::PointData & p2, float w0, float w1, float w2 ) {
				z = p0.z*w0 + p1.z*w1 + p2.z*w2;
				Vector3f n = (p0.normal*w0 + p1.normal*w1 + p2.normal*w2).normalized();
				normal[0] = (short)clamp( floor( n.x*32767.5f ), -32768.0f, 32767.0f );
				normal[1] = (short)clamp( floor( n.y*32767.5f ), -32768.0f, 32767.0f );
				byte mtl[MAX_MATERIALS];
				for (size_t i = 0; i < MAX_MATERIALS; ++i)
					mtl[i] = (byte)floor( (float)p0.materials[i]*w0 + (float)p1.materials[i]*w1 + (float)p2.materials[i]*w2 + 0.5f );
				fixMaterials( mtl );
				for (size_t i = 0; i < MAX_MATERIALS - 1; ++i)
					materials[i] = mtl[i];
			}
			void morph1( size_t vi, size_t p0, size_t p1, float w0, float w1 ) {
				ChunkVertex & v = cld->chunkBufferData.vertices[vi];
				blend( v.z[1], v.normalM1, v.materials + MAX_MATERIALS - 1, cld->chunkData.points[p0], cld->chunkData.points[p1], w0, w1 );
			}
			void morph1( size_t vi, size_t p0, size_t p1, size_t p2, float w0, float w1, float w2 ) {
				ChunkVertex & v = cld->chunkBufferData.vertices[vi];
				blend( v.z[1], v.normalM1, v.materials + MAX_MATERIALS - 1, cld->chunkData.points[p0], cld->chunkData.points[p1], cld->chunkData.points[p2], w0, w1, w2 );
			}
			void morph2( size_t vi, size_t p0, size_t p1, float w0, float w1 ) {
				ChunkVertex & v = cld->chunkBufferData.vertices[vi];
				blend( v.z[2], v.normalM2, v.materials + 2*MAX_MATERIALS - 2, cld->chunkData.points[p0], cld->chunkData.points[p1], w0, w1 );
			}
			void morph2( size_t vi, size_t p0, size_t p1, size_t p2, float w0, float w1, float w2 ) {
				ChunkVertex & v = cld->chunkBufferData.vertices[vi];
				blend( v.z[2], v.normalM2, v.materials + 2*MAX_MATERIALS - 2, cld->chunkData.points[p0], cld->chunkData.points[p1], cld->chunkData.points[p2], w0, w1, w2 );
			}

			// c
			// | \
			// |   \
			// |     \
			// |   2   \
			// |         \
			// |   1   0   \
			// |             \
			// b---------------a
			// a, b, c = point indices
			// 0, 1, 2 = vertex indices
			void morphTri( size_t vi0, size_t vi1, size_t vi2, size_t pa, size_t pb, size_t pc ) {
				ChunkVertex & v0 = cld->chunkBufferData.vertices[vi0];
				ChunkVertex & v1 = cld->chunkBufferData.vertices[vi1];
				ChunkVertex & v2 = cld->chunkBufferData.vertices[vi2];
				blend( v0.z[2], v0.normalM2, v0.materials + 2*MAX_MATERIALS - 2, cld->chunkData.points[pa], cld->chunkData.points[pb], cld->chunkData.points[pc], 0.5f, 0.25f, 0.25f );
				blend( v1.z[2], v1.normalM2, v1.materials + 2*MAX_MATERIALS - 2, cld->chunkData.points[pa], cld->chunkData.points[pb], cld->chunkData.points[pc], 0.25f, 0.5f, 0.25f );
				blend( v2.z[2], v2.normalM2, v2.materials + 2*MAX_MATERIALS - 2, cld->chunkData.points[pa], cld->chunkData.points[pb], cld->chunkData.points[pc], 0.25f, 0.25f, 0.5f );
			}
		} m;
		m.cld = cld;

		// level 1
		// +---o---+---o---+
		// | \     |     / |
		// o   o   o   o   o
		// |     \ | /     |
		// +---o---+---o---+
		// |     / | \     |
		// o   o   o   o   o
		// | /     |     \ |
		// +---o---+---o---+

		// rows
		for (size_t y = 0; y <= chunkSize; y += 2) {
			for (size_t x = 1; x <= chunkSize; x += 2) {
				size_t i = y*(chunkSize+1) + x;
				m.morph1( i, i-1, i+1, 0.5f, 0.5f );
			}
		}
		// columns
		for (size_t y = 1; y <= chunkSize; y += 2) {
			for (size_t x = 0; x <= chunkSize; x += 2) {
				size_t i = y*(chunkSize+1) + x;
				m.morph1( i, i-chunkSize-1, i+chunkSize+1, 0.5f, 0.5f );
			}
		}
		// centers
		for (size_t y = 2; y <= chunkSize; y += 4) {
			for (size_t x = 2; x <= chunkSize; x += 4) {
				size_t i = y*(chunkSize+1) + x;
				// bl
				m.morph1( i-chunkSize-2,	i, i-2*chunkSize-4, 0.5f, 0.5f );
				// br
				m.morph1( i-chunkSize,		i, i-2*chunkSize, 0.5f, 0.5f );
				// tl
				m.morph1( i+chunkSize,		i, i+2*chunkSize, 0.5f, 0.5f );
				// tr
				m.morph1( i+chunkSize+2,	i, i+2*chunkSize+4, 0.5f, 0.5f );
			}
		}

		// level 2
		// +---o---o---o---+---o---o---o---+
		// | \             |             / |
		// o   o   o   o   o   o   o   o   o
		// |     \         |         /     |
		// o   o   o   o   o   o   o   o   o
		// |         \     |     /         |
		// o   o   o   o   o   o   o   o   o
		// |             \ | /             |
		// +---o---o---o---+---o---o---o---+
		// |             / | \             |
		// o   o   o   o   o   o   o   o   o
		// |         /     |     \         |
		// o   o   o   o   o   o   o   o   o
		// |     /         |         \     |
		// o   o   o   o   o   o   o   o   o
		// | /             |             \ |
		// +---o---o---o---+---o---o---o---+

		// rows
		for (size_t y = 0; y <= chunkSize; y += 4) {
			for (size_t x = 2; x <= chunkSize; x += 4) {
				size_t i = y*(chunkSize+1) + x;
				m.morph2( i-1,	i-2, i+2, 0.75f, 0.25f );
				m.morph2( i,	i-2, i+2, 0.5f, 0.5f );
				m.morph2( i+1,	i-2, i+2, 0.25f, 0.75f );
			}
		}

		// columns
		for (size_t y = 2; y <= chunkSize; y += 4) {
			for (size_t x = 0; x <= chunkSize; x += 4) {
				size_t i = y*(chunkSize+1) + x;
				m.morph2( i-chunkSize-1,	i-2*chunkSize-2, i+2*chunkSize+2, 0.75f, 0.25f );
				m.morph2( i,				i-2*chunkSize-2, i+2*chunkSize+2, 0.5f, 0.5f );
				m.morph2( i+chunkSize+1,	i-2*chunkSize-2, i+2*chunkSize+2, 0.25f, 0.75f );
			}
		}

		// diagonals and triangle faces
		for (size_t y = 4; y <= chunkSize; y += 8) {
			for (size_t x = 4; x <= chunkSize; x += 8) {
				size_t i = y*(chunkSize+1) + x;
				// diagonals
				// bl
				m.morph2( i-chunkSize-2,	i, i-4*chunkSize-8, 0.75f, 0.25f );
				m.morph2( i-2*chunkSize-4,	i, i-4*chunkSize-8, 0.5f, 0.5f );
				m.morph2( i-3*chunkSize-6,	i, i-4*chunkSize-8, 0.25f, 0.75f );
				// br
				m.morph2( i-chunkSize,		i, i-4*chunkSize, 0.75f, 0.25f );
				m.morph2( i-2*chunkSize,	i, i-4*chunkSize, 0.5f, 0.5f );
				m.morph2( i-3*chunkSize,	i, i-4*chunkSize, 0.25f, 0.75f );
				// tl
				m.morph2( i+chunkSize,		i, i+4*chunkSize, 0.75f, 0.25f );
				m.morph2( i+2*chunkSize,	i, i+4*chunkSize, 0.5f, 0.5f );
				m.morph2( i+3*chunkSize,	i, i+4*chunkSize, 0.25f, 0.75f );
				// tr
				m.morph2( i+chunkSize+2,	i, i+4*chunkSize+8, 0.75f, 0.25f );
				m.morph2( i+2*chunkSize+4,	i, i+4*chunkSize+8, 0.5f, 0.5f );
				m.morph2( i+3*chunkSize+6,	i, i+4*chunkSize+8, 0.25f, 0.75f );

				// triangle faces
				// bl
				m.morphTri( i-chunkSize-3, i-chunkSize-4, i-2*chunkSize-5,		i, i-4, i-4*chunkSize-8 );
				m.morphTri( i-2*chunkSize-3, i-3*chunkSize-4, i-3*chunkSize-5,	i, i-4*chunkSize-4, i-4*chunkSize-8 );
				// br
				m.morphTri( i-chunkSize+1, i-chunkSize+2, i-2*chunkSize+1,		i, i+4, i-4*chunkSize );
				m.morphTri( i-2*chunkSize-1, i-3*chunkSize-2, i-3*chunkSize-1,	i, i-4*chunkSize-4, i-4*chunkSize );
				// tl
				m.morphTri( i+chunkSize-1, i+chunkSize-2, i+2*chunkSize-1,		i, i-4, i+4*chunkSize );
				m.morphTri( i+2*chunkSize+1, i+3*chunkSize+2, i+3*chunkSize+1,	i, i+4*chunkSize+4, i+4*chunkSize );
				// tr
				m.morphTri( i+chunkSize+3, i+chunkSize+4, i+2*chunkSize+5,		i, i+4, i+4*chunkSize+8 );
				m.morphTri( i+2*chunkSize+3, i+3*chunkSize+4, i+3*chunkSize+5,	i, i+4*chunkSize+4, i+4*chunkSize+8 );
			}
		}
	}

	// set the patch enabled flags
	for (size_t y = 0; y < chunkSize; ++y) {
		for (size_t x = 0; x < chunkSize; ++x) {
			if (cld->chunkData.patches[y*chunkSize+x].enabled) {
				cld->chunkBufferData.vertices[(y  )*(chunkSize+1)+(x  )].xyPatchEnabled[2] |= 0x08;
				cld->chunkBufferData.vertices[(y  )*(chunkSize+1)+(x+1)].xyPatchEnabled[2] |= 0x04;
				cld->chunkBufferData.vertices[(y+1)*(chunkSize+1)+(x  )].xyPatchEnabled[2] |= 0x02;
				cld->chunkBufferData.vertices[(y+1)*(chunkSize+1)+(x+1)].xyPatchEnabled[2] |= 0x01;
			}
		}
	}
}

uint SysTerrain::xyIdx( size_t x, size_t y ) const {
	return y*(chunkSize+1) + x;
}

uint SysTerrain::rotateIdx( uint i ) const {
	uint y = i / (chunkSize+1);
	uint x = i - y*(chunkSize+1);

	// translate, rotate, untranslate
	// rotate: (x,y) --> (-y,x)
	uint newX = chunkSize - y;
	uint newY = x;

	return newY*(chunkSize+1) + newX;
}

void SysTerrain::generateIndexBuffer() {
	size_t indexSize;
	if ((chunkSize+1)*(chunkSize+1) > 65535) { // need 1 index for primitive restart
		indexType = GL_UNSIGNED_INT;
		indexSize = sizeof( uint );
	} else {
		indexType = GL_UNSIGNED_SHORT;
		indexSize = sizeof( ushort );
	}
	primitiveRestartIndex = (GLuint)((chunkSize+1)*(chunkSize+1));

	// reserve space for locations
	indexBufferLocations.reserve( (maxLod+1)*4 );

	// the actual data for the indices
	std::vector <char> indexBufferData;
	// buffer to build up the indices
	std::vector <uint> indices;
	// reserve approximate space
	size_t approxSize = 0;
	for (size_t i = 1; i <= chunkSize/2; ++i)
		approxSize += i*7; // 7 indices per fan
	indices.reserve( approxSize );
	indexBufferData.reserve( approxSize * indexSize * (maxLod+1) * 4 );

	// section consists of fans:
	// y
	// |
	// |           +           -- y = chunkSize/2
	// |          /|\
	// |         / | \
	// |        +--A--+
	// |       /|\ | /|\
	// |      / | \|/ | \
	// |     +--B--+--C--+     -- y = 2
	// |    /|\ | /|\ | /|\
	// |   / | \|/ | \|/ | \
	// |  +--D--+--E--+--F--+
	// | /|\ | /|\ | /|\ | /|\
	// |/ | \|/ | \|/ | \|/ | \
	// +--G--+--H--+--I--+--J--+---- x

	// we will generate only the bottom section, then rotate for the other sections
	for (size_t i = 0; i <= maxLod; ++i) {
		// the start row for the "upper indices" (fans A-C)
		size_t startRow = 2;
		// there are a few different cases for the bottom two rows
		if (i == 0) {
			// no LOD transition
			// we can adjust the start row down by 1 (capturing fans D-F)
			startRow = 1;
			// construct fans G-J
			for (size_t x = 1; x < chunkSize; x += 2) {
				indices.push_back( xyIdx( x, 0 ) );
				indices.push_back( xyIdx( x+1, 0 ) );
				indices.push_back( xyIdx( x, 1 ) );
				indices.push_back( xyIdx( x-1, 0 ) );
				indices.push_back( primitiveRestartIndex );
			}
		} else if (i == 1) {
			// 1 LOD transition
			// we can adjust the start row down by 1 (capturing fans D-F)
			startRow = 1;
			// construct fans G-J but remove the center vertex
			for (size_t x = 1; x < chunkSize; x += 2) {
				indices.push_back( xyIdx( x-1, 0 ) );
				indices.push_back( xyIdx( x+1, 0 ) );
				indices.push_back( xyIdx( x, 1 ) );
				indices.push_back( primitiveRestartIndex );
			}
		} else {
			// i LOD transitions
			// on the bottom row of vertices, only every (2^i)th vertex remains
			// for the fans that fall directly in between these vertices, we add another triangle to divide them
			// for other fans, we stretch the bottom vertex to the nearest non-skipped vertex
			size_t skipFactor = 1 << i;
			// we can't really skip more than chunkSize vertices, so just cut it off
			skipFactor = std::min( skipFactor, chunkSize );
			// this case would mean that a whole chunk in one LOD is smaller than a single patch in another LOD
			// the only way this would ever happen is if there are a ton of LODs or very small chunks
			for (size_t x = 2; x < chunkSize-1; x += 2) {
				// add the top 4 indices
				indices.push_back( xyIdx( x, 1 ) );
				indices.push_back( xyIdx( x+1, 1 ) );
				indices.push_back( xyIdx( x, 2 ) );
				indices.push_back( xyIdx( x-1, 1 ) );
				// determine whether this fan is a "center" fan
				size_t left = (x / skipFactor) * skipFactor;
				size_t right = left + skipFactor;
				size_t leftDiff = x - left;
				size_t rightDiff = right - x;
				if (leftDiff <= rightDiff)
					// closer to the left (or exactly between)
					// this case also covers being exactly on the left vertex
					indices.push_back( xyIdx( left, 0 ) );
				if (rightDiff <= leftDiff)
					// closer to the right (or exactly between)
					indices.push_back( xyIdx( right, 0 ) );
				// add the last index and the restart index
				indices.push_back( xyIdx( x+1, 1 ) );
				indices.push_back( primitiveRestartIndex );
			}
		}

		// generate the remaining fans (A-C in the chart) - these are always the same
		// x and y refer to the center of the fan
		// y starts at row 2, except for the first few LODs
		for (size_t y = startRow; y < chunkSize/2; ++y) {
			// x starts 1 in from y and jumps by 2
			for (size_t x = y+1; x < chunkSize-y; x += 2) {
				indices.push_back( xyIdx( x, y ) );
				indices.push_back( xyIdx( x+1, y ) );
				indices.push_back( xyIdx( x, y+1 ) );
				indices.push_back( xyIdx( x-1, y ) );
				indices.push_back( xyIdx( x, y-1 ) );
				indices.push_back( xyIdx( x+1, y ) );
				indices.push_back( primitiveRestartIndex );
			}
		}
		// the last index was the primitive restart indicator, but we don't need that
		indices.pop_back();

		// add to the buffer
		IndexBufferLocation l;
		l.byteOffset = indexBufferData.size();
		l.count = indices.size();
		indexBufferData.resize( indexBufferData.size() + indices.size()*indexSize );
		for (size_t i = 0; i < indices.size(); ++i) {
			size_t offset = l.byteOffset + i*indexSize;
			if (indexSize == sizeof( uint ))
				*((uint*)(&indexBufferData[offset])) = indices[i];
			else
				*((ushort*)(&indexBufferData[offset])) = (ushort)indices[i];
		}
		indexBufferLocations.push_back( l );
		indices.clear();
	}

	// we've constructed the first set of LODs; now we construct the other 3 by rotating
	for (size_t r = 0; r < 3; ++r) {
		for (size_t i = 0; i <= maxLod; ++i) {
			// add to the buffer
			IndexBufferLocation l;
			l.byteOffset = indexBufferData.size();
			// get the same LOD data except rotated back by 1
			IndexBufferLocation src = indexBufferLocations[r*(maxLod+1) + i];
			// same properties (just rotated)
			l.count = src.count;
			indexBufferData.resize( indexBufferData.size() + l.count*indexSize );
			for (size_t i = 0; i < l.count; ++i) {
				// rotate each index by 90 degrees
				size_t srcOffset = src.byteOffset + i*indexSize;
				size_t offset = l.byteOffset + i*indexSize;
				if (indexSize == sizeof( uint )) {
					uint srcIndex = *((uint*)(&indexBufferData[srcOffset]));
					if (srcIndex != primitiveRestartIndex)
						srcIndex = rotateIdx( srcIndex );
					*((uint*)(&indexBufferData[offset])) = srcIndex;
				} else {
					uint srcIndex = (uint)*((ushort*)(&indexBufferData[srcOffset]));
					if (srcIndex != primitiveRestartIndex)
						srcIndex = rotateIdx( srcIndex );
					*((ushort*)(&indexBufferData[offset])) = (ushort)srcIndex;
				}
			}
			indexBufferLocations.push_back( l );
		}
	}

	// compute min/max elements
	for (size_t l = 0; l < indexBufferLocations.size(); ++l) {
		IndexBufferLocation & ibl = indexBufferLocations[l];
		ibl.minElement = std::numeric_limits <size_t>::max();
		ibl.maxElement = 0;
		for (size_t i = 0; i < ibl.count; ++i) {
			size_t offset = ibl.byteOffset + i*indexSize;
			size_t index;
			if (indexSize == sizeof( uint ))
				index = (size_t)*((uint*)(&indexBufferData[offset]));
			else
				index = (size_t)*((ushort*)(&indexBufferData[offset]));
			ibl.minElement = std::min( ibl.minElement, index );
			ibl.maxElement = std::max( ibl.maxElement, index );
		}
	}

	// now we just upload
	GpuBuffer::BufferDescription desc;
	desc.type = GL_ELEMENT_ARRAY_BUFFER;
	desc.size = indexBufferData.size();
	desc.data = &indexBufferData[0];
	desc.usage = GL_STATIC_DRAW;
	indexBuffer = Context::get().gpuState.createBuffer( desc );
}

const SysTerrain::IndexBufferLocation & SysTerrain::getIndexBufferLocation( size_t section, size_t lodDifference ) const {
	return indexBufferLocations[section*(maxLod+1) + lodDifference];
}

void SysTerrain::getUsageInfo( UsageInfo & i ) const {
	i.chunksLoaded = 0;
	i.chunksActive = 0;
	i.chunksCached = 0;
	i.chunksInView = chunksToDraw.size();
	i.vertexBuffersAllocated = vertexBuffers.size();
	i.totalVertexBufferMemory = vertexBuffers.size() * vertexBufferSize;
	i.usedVertexBufferMemory = i.totalVertexBufferMemory - freeVertexBufferLocations.size() * chunkSizeBytes;
	i.indexBufferMemory = indexBuffer->getSize();

	// todo: active chunks
	ChunkMap::ConstIterator it = chunkMap.getConstIterator();
	while (it.next()) {
		const ChunkStatus & s = it.getValue();
		if (s.hasAllFlags( ChunkStatus::F_LOADED ))
			++i.chunksLoaded;
		if (s.hasAllFlags( ChunkStatus::F_CACHED ))
			++i.chunksCached;
	}
}

const SysTerrain::Settings & SysTerrain::getSettings() const {
	return settings;
}

const SysTerrain::Position & SysTerrain::getCenter( size_t lod ) const {
	return lods[lod].center;
}

Vector2f SysTerrain::getMorphDistances( size_t lod ) const {
	return lods[lod].morphDistances;
}

const SysAabb & SysTerrain::getAabb() const {
	return aabb;
}

void SysTerrain::prepareToDraw( const SysCamera3D * camera ) {
	if (!initialized || camera == NULL)
		return;

	const SysFrustum & frustum = camera->getFrustum();

	chunksToDraw.clear();
	subdivisionStack.clear();
	// quadtree-cull the chunks
	for (int y = start.chunk.coords.y; y <= end.chunk.coords.y; ++y) {
		for (int x = start.chunk.coords.x; x <= end.chunk.coords.x; ++x) {
			Chunk c( maxLod, Vector2i( x, y ) );
			if (!chunkMap.contains( c ))
				continue;

			subdivisionStack.push_back( c );
			while (!subdivisionStack.empty()) {
				Chunk chunk = subdivisionStack.back();
				subdivisionStack.pop_back();

				size_t chunkIndex;
				if (!chunkMap.getIndex( chunk, chunkIndex ))
					continue;
				const ChunkStatus & status = chunkMap.getByIndex( chunkIndex );

				// frustum cull
				// todo: eventually make this not suffer from floating point issues by first subtracting integer coordinates
				// of course, the camera will also need to be altered
				float sizeScale = lods[chunk.lod].chunkSize * scale;
				SysAabb aabb(
					Vector3f( sizeScale * (Vector2f)chunk.coords, status.metadata.zBounds[0] ),
					Vector3f( Vector2f( sizeScale, sizeScale ), status.metadata.zBounds[1] ) );
				aabb.maxBound.x += aabb.minBound.x;
				aabb.maxBound.y += aabb.minBound.y;
				if (!frustum.contains( aabb ))
					continue;

				// check if this chunk is optimal for display
				if (!status.hasAllFlags( ChunkStatus::F_DISPLAYING )) {
					if (chunk.lod > 0) {
						Vector2i subChunk = convertLod( chunk.lod, chunk.coords, chunk.lod-1 );
						subdivisionStack.push_back( Chunk( chunk.lod-1, subChunk ) );
						subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x+1, subChunk.y ) ) );
						subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x, subChunk.y+1 ) ) );
						subdivisionStack.push_back( Chunk( chunk.lod-1, Vector2i( subChunk.x+1, subChunk.y+1 ) ) );
					}
				} else
					// if so, add it to the list of chunks to draw
					chunksToDraw.push_back( chunkIndex );
			}
		}
	}

	if (chunksToDraw.empty())
		return;

	// now we have the culled set of chunks to optimally display
	// we next want to sort the chunks by distance to reduce overdraw
	// rather than actual distance, we just project them onto the forward vector of the camera
	// we take "center" as being our origin
	struct ZSort {
		SysTerrain * t;
		Vector3f fwdVec;
		bool operator()( size_t a, size_t b ) {
			const Chunk & cA = t->chunkMap.getKey( a );
			const Chunk & cB = t->chunkMap.getKey( b );
			Position pA( cA, Vector2f( t->lods[cA.lod].chunkSize*0.5f, t->lods[cA.lod].chunkSize*0.5f ) );
			Position pB( cB, Vector2f( t->lods[cB.lod].chunkSize*0.5f, t->lods[cB.lod].chunkSize*0.5f ) );
			// dA and dB are vectors pointing from the terrain center to the center of each chunk
			Vector2f dA = t->lods[cA.lod].chunkSize * (Vector2f)(pA.chunk.coords - t->lods[cA.lod].center.chunk.coords) +
				(pA.offset - t->lods[cA.lod].center.offset);
			Vector2f dB = t->lods[cB.lod].chunkSize * (Vector2f)(pB.chunk.coords - t->lods[cB.lod].center.chunk.coords) +
				(pB.offset - t->lods[cB.lod].center.offset);
			// project onto forward vector
			float zA = dA.dot( fwdVec.xy() );
			float zB = dB.dot( fwdVec.xy() );
			return zA < zB;
		}
	} zSort;
	zSort.t = this;
	const Matrix44af cameraMatrix = camera->getCameraMatrix();
	zSort.fwdVec.set( -cameraMatrix[2], -cameraMatrix[6], -cameraMatrix[10] );
	std::sort( chunksToDraw.begin(), chunksToDraw.end(), zSort );

	return;
}

void SysTerrain::draw( ProgramBinder b ) {
	if (!initialized)
		return;

	indexBuffer->bind();
	GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();

	for (size_t i = 0; i < chunksToDraw.size(); ++i) {
		const Chunk & chunk = chunkMap.getKey( chunksToDraw[i] );
		const ChunkStatus & status = chunkMap.getByIndex( chunksToDraw[i] );

		status.vertexBufferLocation.vertexBuffer->bind();

		// draw each section
		for (size_t s = 0; s < 4; ++s) {
			// offset into the materials enabled array based on s
			b( status.vertexBufferLocation.byteOffset, chunk, status.metadata.materialsEnabled + s*MAX_MATERIALS );

			const IndexBufferLocation & ibl = getIndexBufferLocation( s, status.lodDifference[s] );
			drawCall.setParameters( GL_TRIANGLE_FAN, ibl.minElement, ibl.maxElement, ibl.count, indexType,
				bufferOffset( ibl.byteOffset ), primitiveRestartIndex );
			drawCall.draw();
		}
	}
}

// debugging functions
void SysTerrain::setDebugCamera( const SysCamera2D * cam ) {
	debugCamera = cam;
}

void SysTerrain::drawChunks2D() {
	if (!initialized || debugCamera == NULL)
		return;

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_ALPHA );
	blendState.bind();

	float sc = 0.25f * (float)Context::get().gpuState.getCurrentViewportState().getViewportSize().y / maxViewDist;
	Matrix33af modelMatrix;
	modelMatrix.scale( sc, -sc ); // flip y
	modelMatrix.translate( 0.5f * (Vector2f)Context::get().gpuState.getCurrentViewportState().getViewportSize() );
	Matrix44f mvpMatrix = debugCamera->getModelViewProjectionMatrix( modelMatrix );

	size_t should = 0;
	size_t loaded = 0;
	size_t loading = 0;
	ChunkMap::ConstIterator it = chunkMap.getConstIterator();
	while (it.next()) {
		const Chunk & c = it.getKey();
		const ChunkStatus & s = it.getValue();
		if (!s.hasAllFlags( ChunkStatus::F_DISPLAYING ))
			continue;

		Vector2i chunk = convertLod( c.lod, c.coords, 0 );
		Vector2f start = lods[0].chunkSize * (Vector2f)(chunk - currentCenter.chunk.coords) - currentCenter.offset;
		Vector2f size( lods[c.lod].chunkSize, lods[c.lod].chunkSize );
		Vector4f color1( 1.0f, 0.0f, 0.0f, 0.5f );
		Vector4f color2( 0.0f, 0.0f, 1.0f, 0.5f );
		Vector4f colorBlend( 0.5f, 0.0f, 0.5f, 0.5f );

		SysRenderer2D::get().drawGradient( mvpMatrix, start, size, color1, colorBlend, colorBlend, color2 );
	}
	Vector4f green( 0.0f, 1.0f, 0.0f, 1.0f );
	float ctrSize = lods[0].chunkSize*0.125f;
	SysRenderer2D::get().drawRect( mvpMatrix, Vector2f( -ctrSize, -ctrSize ), 2.0f*Vector2f( ctrSize, ctrSize ), green );
}

void SysTerrain::drawOptimalChunks2D() {
	if (!initialized || debugCamera == NULL)
		return;

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_ALPHA );
	blendState.bind();

	float sc = 0.25f * (float)Context::get().gpuState.getCurrentViewportState().getViewportSize().y / maxViewDist;
	Matrix33af modelMatrix;
	modelMatrix.scale( sc, -sc ); // flip y
	modelMatrix.translate( 0.5f * (Vector2f)Context::get().gpuState.getCurrentViewportState().getViewportSize() );
	Matrix44f mvpMatrix = debugCamera->getModelViewProjectionMatrix( modelMatrix );

	size_t should = 0;
	size_t loaded = 0;
	size_t loading = 0;
	ChunkMap::ConstIterator it = chunkMap.getConstIterator();
	while (it.next()) {
		const Chunk & c = it.getKey();
		const ChunkStatus & s = it.getValue();
		if (!s.hasAllFlags( ChunkStatus::F_SHOULD_DISPLAY ))
			continue;

		Vector2i chunk = convertLod( c.lod, c.coords, 0 );
		Vector2f start = lods[0].chunkSize * (Vector2f)(chunk - currentCenter.chunk.coords) - currentCenter.offset;
		Vector2f size( lods[c.lod].chunkSize, lods[c.lod].chunkSize );
		Vector4f color1( 1.0f, 0.0f, 0.0f, 0.5f );
		Vector4f color2( 0.0f, 0.0f, 1.0f, 0.5f );
		Vector4f colorBlend( 0.5f, 0.0f, 0.5f, 0.5f );

		SysRenderer2D::get().drawGradient( mvpMatrix, start, size, color1, colorBlend, colorBlend, color2 );
	}
	Vector4f green( 0.0f, 1.0f, 0.0f, 1.0f );
	float ctrSize = lods[0].chunkSize*0.125f;
	SysRenderer2D::get().drawRect( mvpMatrix, Vector2f( -ctrSize, -ctrSize ), 2.0f*Vector2f( ctrSize, ctrSize ), green );
}