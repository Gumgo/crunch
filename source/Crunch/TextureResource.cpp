#include "TextureResource.h"
#include "Context.h"
#include "Log.h"

struct ImageData {
	std::vector <byte> data;	// image data
	uint width;					// image width in pixels
	uint height;				// image height in pixels
	uint bytesPerPixel;			// number of bytes per pixel
	uint rowWidthBytes;			// number of bytes per row

	ImageData()
		: width( 0 )
		, height( 0 )
		, bytesPerPixel( 0 ) {
	}

	ImageData( const ImageData & other ) {
		data = other.data;
		width = other.width;
		height = other.height;
		bytesPerPixel = other.bytesPerPixel;
		rowWidthBytes = other.rowWidthBytes;
	}

	ImageData( ImageData && other ) {
		data.swap( other.data );
		width = other.width;
		height = other.height;
		bytesPerPixel = other.bytesPerPixel;
		rowWidthBytes = other.rowWidthBytes;
	}
};

void getImageData( ImageData * data, const std::string & path ) {
	// get the type
	FREE_IMAGE_FORMAT format = FreeImage_GetFileType( path.c_str() );
	if (format == FIF_UNKNOWN)
		format = FreeImage_GetFIFFromFilename( path.c_str() );

	FIBITMAP * image = NULL;
	// try to load it
	if (format != FIF_UNKNOWN &&
		FreeImage_FIFSupportsReading( format ) &&
		(image = FreeImage_Load( format, path.c_str() )) != NULL) {

		// get image properties
		data->width = FreeImage_GetWidth( image );
		data->height = FreeImage_GetHeight( image );
		data->bytesPerPixel = FreeImage_GetBPP( image ) / 8;
		data->rowWidthBytes = FreeImage_GetPitch( image );

		// store the data
		uint size = data->rowWidthBytes*data->height;
		data->data.resize( size );
		memcpy( &data->data[0], FreeImage_GetBits( image ), size );

		// swap R and B
		for (uint y = 0; y < data->height; ++y) {
			for (uint x = 0; x < data->width; ++x)
				std::swap( data->data[y*data->rowWidthBytes + x*data->bytesPerPixel],
						   data->data[y*data->rowWidthBytes + x*data->bytesPerPixel + 2] );
		}

		FreeImage_Unload( image );
	} else {
		if (image != NULL)
			FreeImage_Unload( image );
		throw std::runtime_error( "Failed to get image data" );
	}
}

TextureResource::TextureResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m ) {
}

void TextureResource::loadImage() {
	// get the extension
	std::string::size_type dotIndex = getPath().rfind( "." );
	if (dotIndex == std::string::npos)
		throw std::runtime_error( "No extension found in " + getPath() );

	// load the appropriate format of data

	bool is2d = (getMetadata() & TYPE_MASK) == TYPE_2D;
	bool isCube = (getMetadata() & TYPE_MASK) == TYPE_CUBE;
	bool isArray = (getMetadata() & TYPE_MASK) == TYPE_ARRAY;
	bool is3d = (getMetadata() & TYPE_MASK) == TYPE_3D;

	if (is2d) {
		ImageData imageData;
		std::string ext = convertToUpper( getPath().substr( dotIndex ) );
		getImageData( &imageData, getPath() );
		data.swap( imageData.data );
		width = imageData.width;
		height = imageData.height;
		bytesPerPixel = imageData.bytesPerPixel;
		repeatX = (getMetadata() & REPEAT_X) != 0;
		repeatY = (getMetadata() & REPEAT_Y) != 0;
		repeatZ = (getMetadata() & REPEAT_Z) != 0;
	} else if (isCube || isArray || is3d) {
		std::string pathName = getPath().substr( 0, dotIndex );
		std::string pathExt = getPath().substr( dotIndex, getPath().length()-dotIndex );

		if (isCube) {
			const std::string faceNames[6] = { "xp", "xn", "yp", "yn", "zp", "zn" };

			ImageData imageData[6];

			uint dim, rwb, bytes;
			for (size_t i = 0; i < 6; ++i) {
				std::string faceName = pathName + '.' + faceNames[i] + pathExt;
				getImageData( &imageData[i], faceName );

				// if not square, error
				if (imageData[i].width != imageData[i].height)
					throw std::runtime_error( "Non-square cube map texture specified" );

				// if not same dimensions as previously loaded face textures, error
				if (i > 0 && imageData[i].width != dim)
					throw std::runtime_error( "Mismatched cube map texture dimensions" );

				if (i > 0 && imageData[i].bytesPerPixel != bytes)
					throw std::runtime_error( "Mismatched cube map texture bit depth" );

				dim = imageData[i].width;
				rwb = imageData[i].rowWidthBytes;
				bytes = imageData[i].bytesPerPixel;
			}

			// allocate data for the image, times 6 for each face of the cube map
			// images are stored contiguously
			size_t size = (size_t)(rwb * dim * 6);
			data.resize( size );

			// copy the data into one tall texture
			for (size_t i = 0; i < 6; ++i)
				memcpy( &data[0] + (rwb*dim*i), &imageData[i].data[0], rwb*dim );

			width = dim;
			height = dim;
			bytesPerPixel = bytes;
			rowWidthBytes = rwb;
			repeatX = false;
			repeatY = false;
			repeatZ = false;
		} else if (isArray || is3d) {
			std::string typeStr;
			if (isArray)
				typeStr = "array";
			else if (is3d)
				typeStr = "3D";
			size_t slices = 0;

			std::vector <ImageData> imageData;

			uint w, h, rwb, bytes;
			while (true) {
				std::string sliceName = pathName + '.' + toString( slices ) + pathExt;
				// for the first slice, don't check if the file exists so that we get an error if it doesn't
				std::ifstream file( sliceName );
				if (slices > 0 && !file.is_open())
					break;
				file.close();

				imageData.push_back( ImageData() );
				getImageData( &imageData.back(), sliceName );

				// if not same dimensions as previously loaded face textures, error
				if (slices > 0 && (imageData.back().width != w || imageData.back().height != h))
					throw std::runtime_error( "Mismatched " + typeStr + " texture dimensions" );

				if (slices > 0 && imageData.back().bytesPerPixel != bytes)
					throw std::runtime_error( "Mismatched " + typeStr + " texture bit depth" );

				w = imageData.back().width;
				h = imageData.back().height;
				rwb = imageData.back().rowWidthBytes;
				bytes = imageData.back().bytesPerPixel;
				++slices;
			}

			// allocate data for the image, once for each slice
			// images are stored contiguously
			size_t size = (size_t)(rwb * h * slices);
			data.resize( size );

			// copy the data into one tall texture
			for (size_t i = 0; i < slices; ++i)
				memcpy( &data[0] + (rwb*h*i), &imageData[i].data[0], rwb*h );

			width = w;
			height = h;
			bytesPerPixel = bytes;
			rowWidthBytes = rwb;
			repeatX = (getMetadata() & REPEAT_X) != 0;
			repeatY = (getMetadata() & REPEAT_Y) != 0;
			repeatZ = (getMetadata() & REPEAT_Z) != 0;
		} else
			assert( false );
	} else
		assert( false );

	mipmap = (getMetadata() & MIPMAP) != 0;
}

bool TextureResource::loadDataAsync() {
	try {
		std::string::size_type dotIndex = getPath().rfind( "." );
		if (dotIndex == std::string::npos)
			throw std::runtime_error( "No extension found in " + getPath() );

		std::string ext = convertToUpper( getPath().substr( dotIndex ) );
		if (ext == ".TGA" || ext == ".PNG" || ext == ".JPG")
			loadImage();
		else
			throw std::runtime_error( "Unknown extension of " + getPath() );
	} catch (const std::exception & e) {
		Log::error() << e.what();
		data.clear();
		data.shrink_to_fit();
		return false;
	}

	return true;
}

bool TextureResource::loadDataSync() {
	try {
		bool sRgb = (getMetadata() & SRGB) != 0;
		GLenum internalFormat = sRgb ?
			((bytesPerPixel == 3) ? GL_SRGB : GL_SRGB_ALPHA) :
			((bytesPerPixel == 3) ? GL_RGB : GL_RGBA);
		GLenum dataFormat = (bytesPerPixel == 3) ? GL_RGB : GL_RGBA;

		bool is2d = (getMetadata() & TYPE_MASK) == TYPE_2D;
		bool isCube = (getMetadata() & TYPE_MASK) == TYPE_CUBE;
		bool isArray = (getMetadata() & TYPE_MASK) == TYPE_ARRAY;
		bool is3d = (getMetadata() & TYPE_MASK) == TYPE_3D;

		if (is2d) {
			GpuTexture::Texture2DDescription tex2DDesc;
			tex2DDesc.imageDescription.internalFormat = internalFormat;
			tex2DDesc.imageDescription.minFilter = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
			tex2DDesc.imageDescription.magFilter = GL_LINEAR;
			tex2DDesc.mipmapDescription.generateMipmaps = mipmap;
			tex2DDesc.wrapS = repeatX ? GL_REPEAT : GL_CLAMP_TO_EDGE;
			tex2DDesc.wrapT = repeatY ? GL_REPEAT : GL_CLAMP_TO_EDGE;
			size_t faceSizeBytes = rowWidthBytes * height;

			GpuTexture::Data2DDescription face;
			tex2DDesc.mipmapLevels = &face;
			face.format = dataFormat;
			face.type = GL_UNSIGNED_BYTE;
			face.width = width;
			face.height = height;
			face.data = &data[0];
			texture = Context::get().gpuState.createTexture( tex2DDesc );
		} else if (isCube) {
			GpuTexture::TextureCubeMapDescription texCubeDesc;
			texCubeDesc.imageDescription.internalFormat = internalFormat;
			texCubeDesc.imageDescription.minFilter = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
			texCubeDesc.imageDescription.magFilter = GL_LINEAR;
			texCubeDesc.mipmapDescription.generateMipmaps = mipmap;
			texCubeDesc.wrapS = repeatX ? GL_REPEAT : GL_CLAMP_TO_EDGE;
			texCubeDesc.wrapT = repeatY ? GL_REPEAT : GL_CLAMP_TO_EDGE;

			size_t faceSizeBytes = rowWidthBytes * height;
			GpuTexture::Data2DDescription faces[6];
			for (size_t i = 0; i < 6; ++i) {
				texCubeDesc.mipmapLevels[i] = &faces[i];
				faces[i].format = dataFormat;
				faces[i].type = GL_UNSIGNED_BYTE;
				faces[i].width = width;
				faces[i].height = height;
				faces[i].data = &data[faceSizeBytes * i];
			}
			texture = Context::get().gpuState.createTexture( texCubeDesc );
		} else if (isArray) {
			GpuTexture::Texture2DArrayDescription tex2DArrayDesc;
			tex2DArrayDesc.imageDescription.internalFormat = internalFormat;
			tex2DArrayDesc.imageDescription.minFilter = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
			tex2DArrayDesc.imageDescription.magFilter = GL_LINEAR;
			tex2DArrayDesc.mipmapDescription.generateMipmaps = mipmap;
			tex2DArrayDesc.wrapS = repeatX ? GL_REPEAT : GL_CLAMP_TO_EDGE;
			tex2DArrayDesc.wrapT = repeatY ? GL_REPEAT : GL_CLAMP_TO_EDGE;

			size_t faceSizeBytes = rowWidthBytes * height;
			GpuTexture::Data3DDescription face;
			tex2DArrayDesc.mipmapLevels = &face;
			face.format = dataFormat;
			face.type = GL_UNSIGNED_BYTE;
			face.width = width;
			face.height = height;
			face.depth = data.size() / faceSizeBytes;
			face.data = &data[0];
			texture = Context::get().gpuState.createTexture( tex2DArrayDesc );
		} else if (is3d) {
			GpuTexture::Texture3DDescription tex3DDesc;
			tex3DDesc.imageDescription.internalFormat = internalFormat;
			tex3DDesc.imageDescription.minFilter = mipmap ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
			tex3DDesc.imageDescription.magFilter = GL_LINEAR;
			tex3DDesc.mipmapDescription.generateMipmaps = mipmap;
			tex3DDesc.wrapS = repeatX ? GL_REPEAT : GL_CLAMP_TO_EDGE;
			tex3DDesc.wrapT = repeatY ? GL_REPEAT : GL_CLAMP_TO_EDGE;
			tex3DDesc.wrapR = repeatZ ? GL_REPEAT : GL_CLAMP_TO_EDGE;

			size_t faceSizeBytes = rowWidthBytes * height;
			GpuTexture::Data3DDescription face;
			tex3DDesc.mipmapLevels = &face;
			face.format = dataFormat;
			face.type = GL_UNSIGNED_BYTE;
			face.width = width;
			face.height = height;
			face.depth = data.size() / faceSizeBytes;
			face.data = &data[0];
			texture = Context::get().gpuState.createTexture( tex3DDesc );
		} else
			assert( false );

		// clear the CPU texture memory
		data.clear();
		data.shrink_to_fit();
	} catch (const std::exception & e) {
		Log::error() << e.what();
		data.clear();
		data.shrink_to_fit();
		texture = GpuTextureReference();
		return false;
	}

	return true;
}

void TextureResource::freeData() {
}

bool TextureResource::readMetadata( uint & metadata, KeyValueReader::Iterator it ) {
	metadata = REPEAT_X | REPEAT_Y | REPEAT_Z;
	if (it.contains( "repeatX" ))
		metadata = (it.getBool( "repeatX" )) ? (metadata | REPEAT_X) : (metadata & ~REPEAT_X);
	if (it.contains( "repeatY" ))
		metadata = (it.getBool( "repeatY" )) ? (metadata | REPEAT_Y) : (metadata & ~REPEAT_Y);
	if (it.contains( "repeatZ" ))
		metadata = (it.getBool( "repeatZ" )) ? (metadata | REPEAT_Z) : (metadata & ~REPEAT_Z);
	if (it.contains( "mipmap" ))
		metadata = (it.getBool( "mipmap" )) ? (metadata | MIPMAP) : (metadata & ~MIPMAP);
	if (it.contains( "type" )) {
		std::string typeString = it.getString( "type" );
		if (typeString == "2D")
			metadata |= TYPE_2D;
		else if (typeString == "CUBE")
			metadata |= TYPE_CUBE;
		else if (typeString == "ARRAY")
			metadata |= TYPE_ARRAY;
		else if (typeString == "3D")
			metadata |= TYPE_3D;
		else
			return false;
	} else
		metadata |= TYPE_2D;
	if (it.contains( "sRGB" ))
		metadata = (it.getBool( "sRGB" )) ? (metadata | SRGB) : (metadata & ~SRGB);

	return true;
}

uint TextureResource::getDefaultMetadata() {
	return REPEAT_X | REPEAT_Y | REPEAT_Z | TYPE_2D;
}

GpuTextureReference TextureResource::getTexture() {
	return texture;
}

GpuTextureConstReference TextureResource::getTexture() const {
	return texture;
}