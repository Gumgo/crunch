#if 0

/** @file NetworkDataLayer.h
 *  @brief The lowest level networking layer.
 */

#ifndef NETWORKDATALAYER_DEFINED
#define NETWORKDATALAYER_DEFINED

#include "Common.h"
#include "Indexer.h"
#include "HashMap.h"
#include "HashFunctions.h"
#include "Network.h"
#include <boost/function.hpp>
#include <vector>

/** @brief The lowest level networking layer.
 *
 *  The data layer provides the functionality of sending
 *  and receiving data to and from arbitrary locations.
 */
class NetworkDataLayer : private boost::noncopyable {

	bool initialized;					/**< Whether the socket is initialized.*/
	SocketHandle socketId;				/**< The ID of the socket.*/
	std::vector <byte> receiveBuffer;	/**< The buffer for receiving data.*/
	static const size_t RECEIVE_BUFFER_SIZE = 32767;	/**< The size of the buffer for receiving data.*/
	// for actual limits, query the following:
	// on windows: SO_MAX_MSG_SIZE
	// on unix: SO_SNDBUF, SO_RCVBUF

	/** @brief Holds info about locations data is being send and received to and from.
	 */
	struct Connection {
		sockaddr_storage address;					/**< The address of the connection.*/
		static const size_t DATA_QUEUE_SIZE = 1024;	/**< The default size of the data queue.*/
		std::vector <byte> dataQueue;				/**< The queue of pending data.*/
		size_t currentQueueSize;					/**< The size of the most recently queued data.*/
		bool queueStarted;							/**< Whether a queue of data has been started.*/
	};

	/** @brief Holds a sockaddr_storage to be used as a key.
	 */
	struct AddressKey {
		sockaddr_storage address;							/**< The address.*/
		AddressKey( const sockaddr_storage & addr );		/**< The constructor.*/
		bool operator==( const AddressKey & other ) const;	/**< Equality comparison.*/
	};

	Indexer <Connection> connections;								/**< The list of current connections.*/
	HashMap <Hash <AddressKey>, AddressKey, size_t> connectionsMap;	/**< Maps connection addresses to corresponding connections.*/
	bool acceptNewConnections;									/**< Whether to add a connection when data from a new location is received.*/
	boost::function <void ( size_t )> onAcceptNewConnection;	/**< Called when a new connection is accepted if new connection acceptance is enabled.*/

	std::vector <size_t> sendPendingConnections;	/**< List of connections with data queued to be sent.*/

	/** @brief Called when data is received.
	 */
	boost::function <void ( size_t, const byte*, size_t )> onReceiveData;

	/** @brief Sends queued data on the given connection.
	 */
	void sendQueued( size_t i );

public:
	NetworkDataLayer();		/**< The constructor.*/
	~NetworkDataLayer();	/**< The destructor.*/

	bool init( ushort port );	/**< Initializes a socket on the given port.*/
	void term();				/**< Terminates the socket.*/

	/** @brief Enables acceptance of new connections.
	 *  @param onAcceptNewConnectionFn	Called when a new connection is accepted, passing the new connection's ID.
	 */
	void enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn );

	/** @brief Disables acceptance of new connections.
	 */
	void disableAcceptNewConnections();

	/** @brief Creates a new connection.
	 */
	bool createConnection( const std::string & address, ushort port, size_t & newConnectionId );

	/** @brief Destroys a connection.
	 */
	void destroyConnection( size_t connectionId );

	/** @brief Sets the function to be called when data is received.
	 *
	 *  The function is passed the connection ID, the data pointer, and the data length.
	 */
	void setOnReceiveData( const boost::function <void ( size_t, const byte*, size_t )> & onReceiveDataFn );

	/** @brief Receives and processes incoming data.
	 */
	void receiveAndProcess();

	/** @brief Starts a queue of data to be sent.
	 */
	bool startQueueData( size_t connectionId );

	/** @brief Queues data to be sent.
	 */
	bool queueData( size_t connectionId, const byte * data, size_t dataSize );

	/** @brief Sends all queued data.
	 */
	void sendQueued();
};

#endif

#endif