#include "SysTileLayer.h"
#include "Context.h"
#include "SystemState.h"
#include "SysRenderer2D.h"

SysTileLayer::TileCluster::TileCluster( const SysTileLayer * l, const TileClusterData & tileClusterData, std::vector <TileVertex> & vertices, std::vector <uint> & indices ) {
	layer = l;

	tileCount = tileClusterData.tiles.size();

	firstVertexIndex = vertices.size();
	firstTileIndex = indices.size();
	for (size_t i = 0; i < tileClusterData.tiles.size(); ++i) {
		const TileData & tile = tileClusterData.tiles[i];
		TileVertex vtx;
		vtx.animFrames = (int)tile.animationFrames;
		vtx.animFrameLength = (int)tile.animationFrameLength;
		vtx.animTimeOffset = (int)tile.animationTimeOffset;
		vtx.width = (float)tile.size[0];

		for (size_t t = 0; t < 4; ++t) {
			vtx.pos[0] = (float)tile.clusterPosition[0];
			vtx.pos[1] = (float)tile.clusterPosition[1];
			vtx.uv[0] = (float)tile.position[0];
			vtx.uv[1] = (float)tile.position[1];
			if (t == 1 || t == 2) {
				vtx.pos[0] += (float)tile.size[0];
				vtx.uv[0] += (float)tile.size[0];
			}
			if (t > 1) {
				vtx.pos[1] += (float)tile.size[1];
				vtx.uv[1] += (float)tile.size[1];
			}

			vertices.push_back( vtx );
		}

		indices[firstTileIndex+i*6  ] = (uint)(firstVertexIndex+i*4);
		indices[firstTileIndex+i*6+1] = (uint)(firstVertexIndex+i*4+1);
		indices[firstTileIndex+i*6+2] = (uint)(firstVertexIndex+i*4+3);
		indices[firstTileIndex+i*6+3] = (uint)(firstVertexIndex+i*4+1);
		indices[firstTileIndex+i*6+4] = (uint)(firstVertexIndex+i*4+3);
		indices[firstTileIndex+i*6+5] = (uint)(firstVertexIndex+i*4+2);
	}
}

void SysTileLayer::TileCluster::draw( GpuProgram * prog, const Vector2i & clusterCoordinates, const Vector2f & layerOffset ) {
	Matrix33af modelMatrix;
	modelMatrix.translate( (Vector2f)(clusterCoordinates*layer->clusterSize) + layerOffset );

	Matrix44f mvpMatrix = layer->camera->getModelViewProjectionMatrix( modelMatrix );
	prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

	GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
	drawCall.setParameters( GL_TRIANGLES, firstVertexIndex, firstVertexIndex + tileCount * 4 - 1, tileCount * 6,
		GL_UNSIGNED_INT, bufferOffset( firstTileIndex * sizeof( uint ) ) );
	drawCall.draw();
}

void SysTileLayer::onCreate() {
	tilesProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___tiles" );
	camera = NULL;
	drawCall.bind( this, &SysTileLayer::draw );
}

void SysTileLayer::onDraw() {
	if (camera != NULL && !tileset)
		Context::get().renderer.submit( drawCall, drawKey );
}

void SysTileLayer::draw() {
	if (tilesProgram->getStatus() != Resource::S_LOADED)
		return;

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_ALPHA );
	blendState.bind();

	static const int TEX_UNIT = 0;
	GpuTextureReference tex = tileset->getTexture();
	tex->bind( (size_t)TEX_UNIT );

	vertexBuffer->bind();
	indexBuffer->bind();

	GpuProgram * prog = tilesProgram->getProgram();
	prog->setAttribute( "pos", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( TileCluster::TileVertex ), bufferOffset( offsetof( TileCluster::TileVertex, pos ) ) );
	prog->setAttribute( "uv", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( TileCluster::TileVertex ), bufferOffset( offsetof( TileCluster::TileVertex, uv ) ) );
	prog->setAttribute( "width", vertexBuffer, 1, GL_FLOAT, false, false, sizeof( TileCluster::TileVertex ), bufferOffset( offsetof( TileCluster::TileVertex, width ) ) );
	prog->setAttribute( "animFrames", vertexBuffer, 1, GL_INT, false, true, sizeof( TileCluster::TileVertex ), bufferOffset( offsetof( TileCluster::TileVertex, animFrames ) ) );
	prog->setAttribute( "animFrameLength", vertexBuffer, 1, GL_INT, false, true, sizeof( TileCluster::TileVertex ), bufferOffset( offsetof( TileCluster::TileVertex, animFrameLength ) ) );
	prog->setAttribute( "animTimeOffset", vertexBuffer, 1, GL_INT, false, true, sizeof( TileCluster::TileVertex ), bufferOffset( offsetof( TileCluster::TileVertex, animTimeOffset ) ) );

	int time = (int)SystemState::get().getCounter();
	Vector2f texSize( (float)tex->getWidth(), (float)tex->getHeight() );
	prog->setUniform( "time", GL_INT, &time );
	prog->setUniform( "texSize", GL_FLOAT_VEC2, &texSize );
	prog->setUniform( "tex", GL_SAMPLER_2D, &TEX_UNIT );

	// find the corners of the view in world space
	float left = camera->getLeft();
	float right = camera->getRight();
	float bottom = camera->getBottom();
	float top = camera->getTop();
	Vector2f tl = camera->getCameraMatrix().transformPoint( Vector2f( left, top ) );
	Vector2f tr = camera->getCameraMatrix().transformPoint( Vector2f( right, top ) );
	Vector2f bl = camera->getCameraMatrix().transformPoint( Vector2f( left, bottom) );
	Vector2f br = camera->getCameraMatrix().transformPoint( Vector2f( right, bottom ) );
	// find a bounding rectangle
	Vector2f cornerMin = vecMin( vecMin( tl, tr ), vecMin( bl, br ) );
	Vector2f cornerMax = vecMax( vecMax( tl, tr ), vecMax( bl, br ) );
	// we want to draw all tile clusters which fall into the box defined by cornerMin and cornerMax

	// get the camera's offset from the translation component of the camera matrix
	Vector2f cameraOffset( camera->getCameraMatrix()[2], camera->getCameraMatrix()[5] );
	// we offset the tile layer by this amount due to parallax
	Vector2f layerOffset = cameraOffset * parallaxMultiplier;

	// now we adjust view bounding rectangle based on the parallax parameters
	cornerMin -= layerOffset;
	cornerMax -= layerOffset;
	Vector2f firstClusterOffset = (Vector2f)(clusterSize*firstClusterCoordinates);
	cornerMin -= firstClusterOffset;
	cornerMax -= firstClusterOffset;

	Vector2i start = (Vector2i)cornerMin / clusterSize;
	Vector2i end = (Vector2i( (int)std::ceil( cornerMax.x ), (int)std::ceil( cornerMax.y ) ) - Vector2i( 1, 1 )) / clusterSize;
	start = vecMax( start, Vector2i() );
	end = vecMin( end, Vector2i( (int)clustersPerRow, (int)clusterRows ) );

	for (int y = start.y; y <= end.y; ++y) {
		for (int x = start.x; x <= end.x; ++x) {
			size_t id = (size_t)x + clustersPerRow * (size_t)y;
			if (id < clusters.size() && clusters[id] != NULL)
				clusters[id]->draw( prog, Vector2i( x, y ) + firstClusterCoordinates, layerOffset );
		}
	}
}

void SysTileLayer::onDestroy() {
	for (size_t i = 0; i < clusters.size(); ++i)
		safeDelete( clusters[i] );
}

void SysTileLayer::init( const TileLayerData & tileLayerData ) {
	for (size_t i = 0; i < clusters.size(); ++i)
		safeDelete( clusters[i] );

	clusterSize = tileLayerData.clusterSize;
	firstClusterCoordinates = tileLayerData.firstClusterCoordinates;
	clustersPerRow = tileLayerData.clustersPerRow;
	clusterRows = tileLayerData.clusterRows;
	parallaxMultiplier = tileLayerData.parallaxMultiplier;
	drawKey = Renderer::makeKey( SysRenderer2D::PASS, tileLayerData.renderPriority );

	size_t totalTiles = 0;
	for (size_t i = 0; i < tileLayerData.clusters.size(); ++i)
		totalTiles += tileLayerData.clusters[i].tiles.size();
	std::vector <TileCluster::TileVertex> vertices;
	std::vector <uint> indices;
	vertices.reserve( totalTiles * 4 + 1 ); // +1 to make sure accessing [0] works
	indices.reserve( totalTiles * 6 + 1 );

	clusters.resize( tileLayerData.clustersEnabled.size(), NULL );
	size_t cluster = 0;
	for (size_t i = 0; i < clusters.size(); ++i) {
		if (tileLayerData.clustersEnabled[i]) {
			clusters[i] = new TileCluster( this, tileLayerData.clusters[cluster], vertices, indices );
			++cluster;
		}
	}

	GpuBuffer::BufferDescription vDesc;
	vDesc.type = GL_ARRAY_BUFFER;
	vDesc.size = sizeof( TileCluster::TileVertex ) * vertices.size();
	vDesc.data = vertices.empty() ? NULL : &vertices[0];
	vDesc.usage = GL_STATIC_DRAW;
	vertexBuffer = Context::get().gpuState.createBuffer( vDesc );

	GpuBuffer::BufferDescription iDesc;
	iDesc.type = GL_ELEMENT_ARRAY_BUFFER;
	iDesc.size = sizeof( uint ) * indices.size();
	iDesc.data = indices.empty() ? NULL : &indices[0];
	iDesc.usage = GL_STATIC_DRAW;
	indexBuffer = Context::get().gpuState.createBuffer( iDesc );
}

void SysTileLayer::setCamera( const SysCamera2D * cam ) {
	camera = cam;
}

void SysTileLayer::setTileset( TextureResourceReference & ts ) {
	tileset = ts;
}