#include "DataReader.h"

DataReader::DataReader( const std::string & fname ) {
	// initialize values
	dataSize = 0;
	dataPointer = 0;

	// attempt to open the file
	std::ifstream file( fname.c_str(), std::ios::binary | std::ios::ate );
	if (!file.is_open()) {
		st = ST_FILE_NOT_FOUND;
		return;
	}

	// get the file size
	dataSize = (size_t)file.tellg();
	data.resize( dataSize );

	// read in all the data
	file.seekg( 0, std::ios::beg );
	if (!data.empty())
		file.read( &data.front(), dataSize );
	if (file.fail()) {
		st = ST_FILE_READ_FAILURE;
		dataSize = 0;
		return;
	}

	// close the file
	file.close();
	st = ST_SUCCESS;
}

DataReader::Status DataReader::status() {
	return st;
}

size_t DataReader::size() {
	return dataSize;
}

size_t DataReader::bytesLeft() {
	return dataSize - dataPointer;
}

bool DataReader::end() {
	return (st == ST_END);
}

bool DataReader::fail() {
	return (st != ST_SUCCESS && st != ST_END);
}

bool DataReader::readData( void * dest, size_t amount ) {
	// if at the end of the file, cannot read more
	if (st == ST_END) {
		st = ST_NOT_ENOUGH_DATA;
		return false;
	}

	// if previous error, cannot read more
	if (st != ST_SUCCESS)
		return false;

	// if not enough to read, cannot read more
	if (bytesLeft() < amount) {
		st = ST_NOT_ENOUGH_DATA;
		return false;
	}

	// read the data and increment the data pointer
	memcpy( dest, &data[dataPointer], amount );
	dataPointer += amount;

	// if at the end, set the status
	if (bytesLeft() == 0)
		st = ST_END;
	return true;
}