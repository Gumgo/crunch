#ifndef MATHS_QUATERNIONDEF_DEFINED
#define MATHS_QUATERNIONDEF_DEFINED

template <typename T> Quaternion <T>::Quaternion()
	: r( 0 )
	, i( 0 )
	, j( 0 )
	, k( 1 ) {
}

template <typename T> Quaternion <T>::Quaternion( const Quaternion <T> & q )
	: r( q.r )
	, i( q.i )
	, j( q.j )
	, k( q.k ) {
}

template <typename T> Quaternion <T>::Quaternion( T pr, T pi, T pj, T pk )
	: r( pr )
	, i( pi )
	, j( pj )
	, k( pk ) {
}

template <typename T> Quaternion <T>::Quaternion( const Vector3 <T> & xyz, T a ) {
	set( xyz, a );
}

template <typename T> Quaternion <T>::Quaternion( const AxisAngle <T> & aa ) {
	set( aa.axis, aa.angle );
}

template <typename T> Quaternion <T>::~Quaternion() {
}

template <typename T> Quaternion <T> Quaternion <T>::operator+( const Quaternion <T> & q ) const {
	return Quaternion <T>( r + q.r, i + q.i, j + q.j, k + q.k );
}

template <typename T> Quaternion <T> Quaternion <T>::operator+() const {
	return *this;
}

template <typename T> Quaternion <T> Quaternion <T>::operator-( const Quaternion <T> & q ) const {
	return Quaternion <T>( r - q.r, i - q.i, j - q.j, k - q.k );
}

template <typename T> Quaternion <T> Quaternion <T>::operator-() const {
	return Quaternion <T>( -r, -i, -j, -k );
}

template <typename T> Quaternion <T> Quaternion <T>::operator*( T s ) const {
	return Quaternion <T>( r*s, i*s, j*s, k*s );
}

template <typename T> Quaternion <T> operator*( T s, const Quaternion <T> & q ) {
	return q*s;
}

template <typename T> Quaternion <T> Quaternion <T>::operator/( T s ) const {
	return Quaternion <T>( r/s, i/s, j/s, k/s );
}

template <typename T> Quaternion <T> Quaternion <T>::operator*( const Quaternion <T> & q ) const {
	return Quaternion <T>( r*q.i + i*q.r + j*q.k - k*q.j,
						   r*q.j + j*q.r + k*q.i - i*q.k,
						   r*q.k + k*q.r + i*q.j - j*q.i,
						   r*q.r - i*q.i - j*q.j - k*q.k );
}

template <typename T> Vector3 <T> Quaternion <T>::operator*( const Vector3 <T> & v ) const {
	Quaternion vq( 0, v.x, v.y, v.z );
	Quaternion conj = getConjugate();
	Quaternion ret = *this * vq * conj;
	return Vector3 <T>( ret.i, ret.j, ret.k );
}

template <typename T> Quaternion <T> Quaternion <T>::operator=( const Quaternion <T> & q ) {
	r = q.r;
	i = q.i;
	j = q.j;
	k = q.k;
	return *this;
}

template <typename T> Quaternion <T> Quaternion <T>::operator+=( const Quaternion <T> & q ) {
	r += q.r;
	i += q.i;
	j += q.j;
	k += q.k;
	return *this;
}

template <typename T> Quaternion <T> Quaternion <T>::operator-=( const Quaternion <T> & q ) {
	r -= q.r;
	i -= q.i;
	j -= q.j;
	k -= q.k;
	return *this;
}

template <typename T> Quaternion <T> Quaternion <T>::operator*=( T s ) {
	r *= s;
	i *= s;
	j *= s;
	k *= s;
	return *this;
}

template <typename T> Quaternion <T> Quaternion <T>::operator/=( T s ) {
	r /= s;
	i /= s;
	j /= s;
	k /= s;
	return *this;
}

template <typename T> Quaternion <T> Quaternion <T>::operator*=( const Quaternion <T> & q ) {
	T tr = r*q.i + i*q.r + j*q.k - k*q.j;
	T ti = r*q.j + j*q.r + k*q.i - i*q.k;
	T tj = r*q.k + k*q.r + i*q.j - j*q.i;
	T tk = r*q.r - i*q.i - j*q.j - k*q.k;

	r = tr;
	i = ti;
	j = tj;
	k = tk;

	return *this;
}

template <typename T> bool Quaternion <T>::operator==( const Quaternion <T> & q ) const {
	return (r == q.r && i == q.i && j == q.j && k == q.k);
}

template <typename T> bool Quaternion <T>::operator!=( const Quaternion <T> & q ) const {
	return (r != q.r || i != q.i || j != q.j || k != q.k);
}

template <typename T> template <typename U> T & Quaternion <T>::operator[]( U t ) {
	return *(&r+t);
}

template <typename T> template <typename U> T Quaternion <T>::operator[]( U t ) const {
	return *(&r+t);
}

template <typename T> template <typename U> Quaternion <T>::operator Quaternion <U> () const {
	return Quaternion <U>( (U)r, (U)i, (U)j, (U)k );
}

template <typename T> void Quaternion <T>::set( T pr, T pi, T pj, T pk ) {
	r = pr;
	i = pi;
	j = pj;
	k = pk;
}

template <typename T> void Quaternion <T>::set( const Vector3 <T> & xyz, T a ) {
	T half = a * (T)0.5;
	Vector3 <T> vec = xyz * sin( half );
	r = cos( half );
	i = vec.x;
	j = vec.y;
	k = vec.z;
}

template <typename T> void Quaternion <T>::set( const AxisAngle <T> & aa ) {
	set( aa.axis, aa.angle );
}

template <typename T> void Quaternion <T>::normalize() {
	setMagnitude( 1 );
}

template <typename T> void Quaternion <T>::conjugate() {
	i = -i;
	j = -j;
	k = -k;
}

template <typename T> void Quaternion <T>::setMagnitude( T m ) {
	T mag = magnitudeSquared();
	if (mag == 0)
		return;
	mag = m/sqrt( mag );
	r *= mag;
	i *= mag;
	j *= mag;
	k *= mag;
}

template <typename T> void Quaternion <T>::rotate( const Quaternion <T> & q ) {
	*this = q * *this;
}

template <typename T> Quaternion <T> Quaternion <T>::normalized() const {
	T mag = magnitudeSquared();
	if (mag == 0)
		return Quaternion <T>( 0, 0, 0, 0 );
	mag = 1/sqrt( mag );
	return Quaternion <T>( r*mag, i*mag, j*mag, k*mag );
}

template <typename T> Quaternion <T> Quaternion <T>::getConjugate() const {
	return Quaternion <T>( r, -i, -j, -k );
}

template <typename T> T Quaternion <T>::magnitude() const {
	return sqrt( r*r + i*i + j*j + k*k );
}

template <typename T> T Quaternion <T>::magnitudeSquared() const {
	return (r*r + i*i + j*j + k*k);
}

template <typename T> T Quaternion <T>::dot( const Quaternion <T> & q ) const {
	return r*q.r + i*q.i + j*q.j + k*q.k;
}

template <typename T> Vector3 <T> Quaternion <T>::getAxis() const {
	T scale = sqrt( i*i + j*j + k*k );
	if (scale == 0.0f)
		return Vector3 <T>( 0, 0, 1 );
	else {
		scale = 1 / scale;
		return Vector3 <T>( i, j, k )*scale;
	}
}

template <typename T> T Quaternion <T>::getAngle() const {
	return acos( r )*2;
}

template <typename T> Quaternion <T> Quaternion <T>::rotated( const Quaternion <T> & q ) const {
	return q * *this;
}

template <typename T> Vector3 <T> Quaternion <T>::apply( const Vector3 <T> & v ) const {
	return *this * v;
}

template <typename T, typename S> Quaternion <T> slerp( const Quaternion <T> & a, const Quaternion <T> & b, S c ) {
	Quaternion <T> q;
	T dot = a.dot( b );
	if (dot < 0) {
		dot = -dot;
		q = -b;
	} else
		q = b;

	T angle = acos( dot );
	T sinA = sin( angle );
	if (sinA == 0)
		return a;
	else
		return (a*sin( angle*(1-c) ) + q*sin( angle*c )) / sinA;
}

template <typename T, typename S> Quaternion <T> nlerp( const Quaternion <T> & a, const Quaternion <T> & b, S c ) {
	Quaternion <T> q;
	T dot = a.dot( b );
	if (dot < 0) {
		Quaternion <T> res = a*(1-c) - b*c;
		res.normalize();
		return res;
	} else {
		Quaternion <T> res = a*(1-c) + b*c;
		res.normalize();
		return res;
	}
}

#endif