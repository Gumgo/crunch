/** @file Physics2DConvexPolygonShape.h
 *  @brief Defines convex polygon shapes.
 */
#ifndef PHYSICS2DCONVEXPOLYGONSHAPE_DEFINED
#define PHYSICS2DCONVEXPOLYGONSHAPE_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DShape.h"

namespace Physics2D {

class Body;
struct ConvexPolygonShapeDefinition;

/** @brief A convex polygon shape.
 */
class ConvexPolygonShape : public Shape {
public:
	static const size_t MAX_VERTICES = 8;	/**< The maximum number of vertices allowed.*/

	ConvexPolygonShape( const ConvexPolygonShapeDefinition & d, Body * par );	/**< The constructor.*/

	/** @brief Returns the bounding box of the transformed shape.
	 */
	Aabb getAabb( const Matrix33ar & transformation ) const;

	/** @brief Computes the mass data of the shape.
	 */
	void computeMassData();

	void addVertex( const Vector2r & vertex );	/**< Pushes a vertex.*/
	void clearVertices();						/**< Clears the vertices.*/
	Vector2r getVertex( size_t index ) const;	/**< Returns a vertex.*/
	size_t getVertexCount() const;				/**< Returns the current number of vertices.*/

private:
	size_t vertexCount;					// current number of vertices
	Vector2r vertices[MAX_VERTICES];	// the vertices
};

/** @brief Defines convex polygon shapes.
 */
struct ConvexPolygonShapeDefinition : public ShapeDefinition {
	size_t vertexCount;										/**< The number of vertices.*/
	Vector2r vertices[ConvexPolygonShape::MAX_VERTICES];	/**< The vertices.*/

	ConvexPolygonShapeDefinition();				/**< Assigns default values.*/
	Shape * createShape( Body * parent ) const;	/**< Creates a convex polygon shape from the definition.*/
};

}

#endif