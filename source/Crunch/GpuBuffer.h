/** @file GpuBuffer.h
 *  @brief Encapsulates a buffer on the GPU.
 */

#ifndef GPUBUFFER_DEFINED
#define GPUBUFFER_DEFINED

#include "Common.h"
#include "GpuObject.h"

/** @brief Encapsulates a buffer on the GPU.
 */
class GpuBuffer : public ReferenceCountedGpuObject {
public:
	/** @brief Description of buffer settings.
	 */
	struct BufferDescription {
		GLenum type;		/**< The buffer type.*/
		size_t size;		/**< The size of the buffer.*/
		const void * data;	/**< The initial data to fill the buffer with.*/
		GLenum usage;		/**< The usage hint for the buffer.*/

		BufferDescription();	/**< Sets default values.*/
	};

	GLuint getId() const;		/**< Returns the ID of the underlying buffer.*/
	GLenum getType() const;		/**< Returns the type of the buffer.*/
	size_t getSize() const;		/**< Returns the size of the buffer in bytes.*/
	GLenum getUsage() const;	/**< Returns the usage hint for the buffer.*/

	void bind();					/**< Binds the buffer to the location corresponding to its originally specified type.*/
	void bind( GLenum location );	/**< Binds the buffer to the given location.*/

	/** @brief Writes data to the buffer, binding it to the location corresponding to its originally specified type.
	 */
	bool write( size_t bufferOffset, size_t dataSize, const void * data );

	/** @brief Writes data to the buffer, binding it to the given location.
	 */
	bool write( GLenum location, size_t bufferOffset, size_t dataSize, const void * data );

	/** @brief Orphans the buffer.
	 *
	 *  This is done by calling glBufferData() with a NULL pointer and
	 *  the same size and hints as when the buffer was originally allocated.
	 */
	void orphan();

	/** @brief Orphans the buffer, binding it to the given location.
	 */
	void orphan( GLenum location );

private:
	friend class GpuState;
	GpuBuffer( GpuState * gpuState, const BufferDescription & desc );

	~GpuBuffer();

	void onRelease() const;

	GLuint id;
	GLenum type;
	size_t size;
	GLenum usage;
};

/** @brief Returns a pointer offset from NULL by byteOffset.
 */
template <typename T> void * bufferOffset( T byteOffset ) {
	return (void*)((char*)NULL + byteOffset);
}

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <GpuBuffer> GpuBufferReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <GpuBuffer> GpuBufferConstReference;

#endif