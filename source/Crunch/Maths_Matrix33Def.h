#ifndef MATHS_MATRIX33DEF_DEFINED
#define MATHS_MATRIX33DEF_DEFINED

template <typename T> Matrix33 <T>::Matrix33() {
	set( 1, 0, 0,
		 0, 1, 0,
		 0, 0, 1 );
}

template <typename T> Matrix33 <T>::Matrix33( T m00, T m01, T m02,
											  T m10, T m11, T m12,
                                              T m20, T m21, T m22 ) {
	set( m00, m01, m02,
		 m10, m11, m12,
		 m20, m21, m22 );
}

template <typename T> Matrix33 <T>::Matrix33( const Matrix33 <T> & m ) {
	set( m[0], m[1], m[2],
		 m[3], m[4], m[5],
		 m[6], m[7], m[8] );
}

template <typename T> Matrix33 <T>::~Matrix33() {
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator+( const Matrix33 <T> & m ) const {
	return Matrix33( d[0] + m[0], d[1] + m[1], d[2] + m[2],
					 d[3] + m[3], d[4] + m[4], d[5] + m[5],
					 d[6] + m[6], d[7] + m[7], d[8] + m[8] );
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator+() const {
	return *this;
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator-( const Matrix33 <T> & m ) const {
	return Matrix33( d[0] - m[0], d[1] - m[1], d[2] - m[2],
					 d[3] - m[3], d[4] - m[4], d[5] - m[5],
					 d[6] - m[6], d[7] - m[7], d[8] - m[8] );
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator-() const {
	return Matrix33( -d[0], -d[1], -d[2],
					 -d[3], -d[4], -d[5],
					 -d[6], -d[7], -d[8] );
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator*( T r ) const {
	return Matrix33( d[0]*r, d[1]*r, d[2]*r,
					 d[3]*r, d[4]*r, d[5]*r,
					 d[6]*r, d[7]*r, d[8]*r );
}

template <typename T> Matrix33 <T> operator*( T r, const Matrix33 <T> & m ) {
	return m*r;
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator/( T r ) const {
	return Matrix33( d[0]/r, d[1]/r, d[2]/r,
					 d[3]/r, d[4]/r, d[5]/r,
					 d[6]/r, d[7]/r, d[8]/r );
}

template <typename T> Vector3 <T> Matrix33 <T>::operator*( const Vector3 <T> & v ) const {
	return Vector3 <T>( d[0]*v[0] + d[1]*v[1] + d[2]*v[2],
						d[3]*v[0] + d[4]*v[1] + d[5]*v[2],
						d[6]*v[0] + d[7]*v[1] + d[8]*v[2] );
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator*( const Matrix33 <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[3] + d[2]*m[6];
	T m01 = d[0]*m[1] + d[1]*m[4] + d[2]*m[7];
	T m02 = d[0]*m[2] + d[1]*m[5] + d[2]*m[8];

	T m10 = d[3]*m[0] + d[4]*m[3] + d[5]*m[6];
	T m11 = d[3]*m[1] + d[4]*m[4] + d[5]*m[7];
	T m12 = d[3]*m[2] + d[4]*m[5] + d[5]*m[8];

	T m20 = d[6]*m[0] + d[7]*m[3] + d[8]*m[6];
	T m21 = d[6]*m[1] + d[7]*m[4] + d[8]*m[7];
	T m22 = d[6]*m[2] + d[7]*m[5] + d[8]*m[8];

	return Matrix33 <T>( m00, m01, m02,
						 m10, m11, m12,
						 m20, m21, m22 );
}

template <typename T> Matrix33a <T> Matrix33 <T>::operator*( const Matrix33a <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[4] + d[2]*m[ 8];
	T m01 = d[0]*m[1] + d[1]*m[5] + d[2]*m[ 9];
	T m02 = d[0]*m[2] + d[1]*m[6] + d[2]*m[10];

	T m10 = d[3]*m[0] + d[4]*m[4] + d[5]*m[ 8];
	T m11 = d[3]*m[1] + d[4]*m[5] + d[5]*m[ 9];
	T m12 = d[3]*m[2] + d[4]*m[6] + d[5]*m[10];

	T m20 = d[6]*m[0] + d[7]*m[4] + d[8]*m[ 8];
	T m21 = d[6]*m[1] + d[7]*m[5] + d[8]*m[ 9];
	T m22 = d[6]*m[2] + d[7]*m[6] + d[8]*m[10];

	return Matrix33a <T>( m00, m01, m02, m[3],
						  m10, m11, m12, m[7],
						  m20, m21, m22, m[11] );
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator=( const Matrix33 <T> & m ) {
	set( m[0], m[1], m[2],
		 m[3], m[4], m[5],
		 m[6], m[7], m[8] );
	return *this;
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator+=( const Matrix33 <T> & m ) {
	d[0] += m[0]; d[1] += m[1]; d[2] += m[2];
	d[3] += m[3]; d[4] += m[4]; d[5] += m[5];
	d[6] += m[6]; d[7] += m[7]; d[8] += m[8];
	return *this;
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator-=( const Matrix33 <T> & m ) {
	d[0] -= m[0]; d[1] -= m[1]; d[2] -= m[2];
	d[3] -= m[3]; d[4] -= m[4]; d[5] -= m[5];
	d[6] -= m[6]; d[7] -= m[7]; d[8] -= m[8];
	return *this;
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator*=( T r ) {
	d[0] *= r; d[1] *= r; d[2] *= r;
	d[3] *= r; d[4] *= r; d[5] *= r;
	d[6] *= r; d[7] *= r; d[8] *= r;
	return *this;
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator/=( T r ) {
	d[0] /= r; d[1] /= r; d[2] /= r;
	d[3] /= r; d[4] /= r; d[5] /= r;
	d[6] /= r; d[7] /= r; d[8] /= r;
	return *this;
}

template <typename T> Matrix33 <T> Matrix33 <T>::operator*=( const Matrix33 <T> & m ) {
	T m00 = d[0]*m[0] + d[1]*m[3] + d[2]*m[6];
	T m01 = d[0]*m[1] + d[1]*m[4] + d[2]*m[7];
	T m02 = d[0]*m[2] + d[1]*m[5] + d[2]*m[8];

	T m10 = d[3]*m[0] + d[4]*m[3] + d[5]*m[6];
	T m11 = d[3]*m[1] + d[4]*m[4] + d[5]*m[7];
	T m12 = d[3]*m[2] + d[4]*m[5] + d[5]*m[8];

	T m20 = d[6]*m[0] + d[7]*m[3] + d[8]*m[6];
	T m21 = d[6]*m[1] + d[7]*m[4] + d[8]*m[7];
	T m22 = d[6]*m[2] + d[7]*m[5] + d[8]*m[8];

	set( m00, m01, m02,
		 m10, m11, m12,
		 m20, m21, m22 );

	return *this;
}

template <typename T> bool Matrix33 <T>::operator==( const Matrix33 <T> & m ) const {
	return (d[0] == m[0] && d[1] == m[1] && d[2] == m[2] &&
			d[3] == m[3] && d[4] == m[4] && d[5] == m[5] &&
			d[6] == m[6] && d[7] == m[7] && d[8] == m[8]);
}

template <typename T> bool Matrix33 <T>::operator!=( const Matrix33 <T> & m ) const {
	return (d[0] != m[0] || d[1] != m[1] || d[2] != m[2] ||
			d[3] != m[3] || d[4] != m[4] || d[5] != m[5] ||
			d[6] != m[6] || d[7] != m[7] || d[8] != m[8]);
}

template <typename T> template <typename U> T & Matrix33 <T>::operator[]( U i ) {
	return d[i];
}

template <typename T> template <typename U> T Matrix33 <T>::operator[]( U i ) const {
	return d[i];
}

template <typename T> template <typename U> T & Matrix33 <T>::operator()( U row, U col ) {
	return d[row*3+col];
}

template <typename T> template <typename U> T Matrix33 <T>::operator()( U row, U col ) const {
	return d[row*3+col];
}

template <typename T> template <typename U> Matrix33 <T>::operator Matrix33 <U> () const {
	return Matrix33 <U>( (U)d[0], (U)d[1], (U)d[2],
						 (U)d[3], (U)d[4], (U)d[5],
						 (U)d[6], (U)d[7], (U)d[8] );
}

template <typename T> void Matrix33 <T>::set( T m00, T m01, T m02,
											  T m10, T m11, T m12,
											  T m20, T m21, T m22 ) {
	d[0] = m00; d[1] = m01; d[2] = m02;
	d[3] = m10; d[4] = m11; d[5] = m12;
	d[6] = m20; d[7] = m21; d[8] = m22;
}

template <typename T> void Matrix33 <T>::transpose() {
	T temp;
	temp = d[3]; d[3] = d[1]; d[1] = temp;
	temp = d[6]; d[6] = d[2]; d[2] = temp;
	temp = d[7]; d[7] = d[5]; d[5] = temp;
}

template <typename T> bool Matrix33 <T>::invert() {
	T det = determinant();
	if (det == 0) {
		set( 0, 0, 0, 0, 0, 0, 0, 0, 0 );
		return false;
	}

	T invDet = 1/det;

	float m00 = invDet * (d[8]*d[4] - d[7]*d[5]);
	float m01 = invDet * (d[7]*d[2] - d[8]*d[1]);
	float m02 = invDet * (d[5]*d[1] - d[4]*d[2]);
	float m10 = invDet * (d[6]*d[5] - d[8]*d[3]);
	float m11 = invDet * (d[8]*d[0] - d[6]*d[2]);
	float m12 = invDet * (d[3]*d[2] - d[5]*d[0]);
	float m20 = invDet * (d[7]*d[3] - d[6]*d[4]);
	float m21 = invDet * (d[6]*d[1] - d[7]*d[0]);
	float m22 = invDet * (d[4]*d[0] - d[3]*d[1]);

	set( m00, m01, m02,
		 m10, m11, m12,
		 m20, m21, m22 );
	return true;
}

template <typename T> void Matrix33 <T>::rotateX( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	T r11 = cosA;
	T r12 = -sinA;
	T r21 = sinA;
	T r22 = cosA;

	T m10 = r11*d[3] + r12*d[6];
	T m11 = r11*d[4] + r12*d[7];
	T m12 = r11*d[5] + r12*d[8];

	T m20 = r21*d[3] + r22*d[6];
	T m21 = r21*d[4] + r22*d[7];
	T m22 = r21*d[5] + r22*d[8];

	d[3] = m10; d[4] = m11; d[5] = m12;
	d[6] = m20; d[7] = m21; d[8] = m22;
}

template <typename T> void Matrix33 <T>::rotateY( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r02 = sinA;
	T r20 = -sinA;
	T r22 = cosA;

	T m00 = r00*d[0] + r02*d[6];
	T m01 = r00*d[1] + r02*d[7];
	T m02 = r00*d[2] + r02*d[8];

	T m20 = r20*d[0] + r22*d[6];
	T m21 = r20*d[1] + r22*d[7];
	T m22 = r20*d[2] + r22*d[8];

	d[0] = m00; d[1] = m01; d[2] = m02;
	d[6] = m20; d[7] = m21; d[8] = m22;
}

template <typename T> void Matrix33 <T>::rotateZ( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r01 = -sinA;
	T r10 = sinA;
	T r11 = cosA;

	T m00 = r00*d[0] + r01*d[3];
	T m01 = r00*d[1] + r01*d[4];
	T m02 = r00*d[2] + r01*d[5];

	T m10 = r10*d[0] + r11*d[3];
	T m11 = r10*d[1] + r11*d[4];
	T m12 = r10*d[2] + r11*d[5];

	d[0] = m00; d[1] = m01; d[2] = m02;
	d[3] = m10; d[4] = m11; d[5] = m12;
}

template <typename T> void Matrix33 <T>::rotate( T x, T y, T z, T a ) {
	T sinA = sin( a );
	T cosA = cos( a );
	T xy = x*y;
	T xz = x*z;
	T yz = y*z;

	T r00 = cosA + x*x*(1 - cosA);
	T r01 = xy*(1 - cosA) - z*sinA;
	T r02 = xz*(1 - cosA) + y*sinA;
	T r10 = xy*(1 - cosA) + z*sinA;
	T r11 = cosA + y*y*(1 - cosA);
	T r12 = yz*(1 - cosA) - x*sinA;
	T r20 = xz*(1 - cosA) - y*sinA;
	T r21 = yz*(1 - cosA) - x*sinA;
	T r22 = cosA + z*z*(1 - cosA);

	T m00 = r00*d[0] + r01*d[3] + r02*d[6];
	T m01 = r00*d[1] + r01*d[4] + r02*d[7];
	T m02 = r00*d[2] + r01*d[5] + r02*d[8];

	T m10 = r10*d[0] + r11*d[3] + r12*d[6];
	T m11 = r10*d[1] + r11*d[4] + r12*d[7];
	T m12 = r10*d[2] + r11*d[5] + r12*d[8];

	T m20 = r20*d[0] + r22*d[6] + r22*d[6];
	T m21 = r20*d[1] + r22*d[7] + r22*d[7];
	T m22 = r20*d[2] + r22*d[8] + r22*d[8];

	d[0] = m00; d[1] = m01; d[2] = m02;
	d[3] = m10; d[4] = m11; d[5] = m12;
	d[6] = m20; d[7] = m21; d[8] = m22;
}

template <typename T> void Matrix33 <T>::rotate( const AxisAngle <T> & aa ) {
	rotate( aa.axis.x, aa.axis.y, aa.axis.z, aa.angle );
}


template <typename T> void Matrix33 <T>::rotate( const Quaternion <T> & q ) {
	T i2 = 2*q.i;
	T j2 = 2*q.j;
	T k2 = 2*q.k;

	T ii2 = i2*q.i;
	T ij2 = i2*q.j;
	T ik2 = i2*q.k;
	T ir2 = i2*q.r;
	T jj2 = j2*q.j;
	T jk2 = j2*q.k;
	T jr2 = j2*q.r;
	T kk2 = k2*q.k;
	T kr2 = k2*q.r;

	T r00 = 1 - jj2 - kk2;
	T r01 = ij2 - kr2;
	T r02 = ik2 + jr2;
	T r10 = ij2 + kr2;
	T r11 = 1 - ii2 - kk2;
	T r12 = jk2 - ir2;
	T r20 = ik2 - jr2;
	T r21 = jk2 + ir2;
	T r22 = 1 - ii2 - jj2;

	T m00 = r00*d[0] + r01*d[3] + r02*d[6];
	T m01 = r00*d[1] + r01*d[4] + r02*d[7];
	T m02 = r00*d[2] + r01*d[5] + r02*d[8];

	T m10 = r10*d[0] + r11*d[3] + r12*d[6];
	T m11 = r10*d[1] + r11*d[4] + r12*d[7];
	T m12 = r10*d[2] + r11*d[5] + r12*d[8];

	T m20 = r20*d[0] + r22*d[6] + r22*d[6];
	T m21 = r20*d[1] + r22*d[7] + r22*d[7];
	T m22 = r20*d[2] + r22*d[8] + r22*d[8];

	d[0] = m00; d[1] = m01; d[2] = m02;
	d[3] = m10; d[4] = m11; d[5] = m12;
	d[6] = m20; d[7] = m21; d[8] = m22;
}

template <typename T> void Matrix33 <T>::scale( T x, T y, T z ) {
	d[0] *= x;
	d[1] *= x;
	d[2] *= x;

	d[3] *= y;
	d[4] *= y;
	d[5] *= y;

	d[6] *= z;
	d[7] *= z;
	d[8] *= z;
}

template <typename T> void Matrix33 <T>::scale( const Vector3 <T> & s ) {
	scale( s.x, s.y, s.z );
}

template <typename T> void Matrix33 <T>::zero() {
	set( 0, 0, 0, 0, 0, 0, 0, 0, 0 );
}

template <typename T> void Matrix33 <T>::identity() {
	set( 1, 0, 0, 0, 1, 0, 0, 0, 1 );
}

template <typename T> Matrix33 <T> Matrix33 <T>::transposed() const {
	return Matrix33 <T>( d[0], d[3], d[6],
						 d[1], d[4], d[7],
						 d[2], d[5], d[8] );
}

template <typename T> Matrix33 <T> Matrix33 <T>::inverse() const {
	T det = determinant();
	if (det == 0)
		return Matrix33 <T>( 0, 0, 0, 0, 0, 0, 0, 0, 0 );

	T invDet = 1/det;

	float m00 = invDet * (d[8]*d[4] - d[7]*d[5]);
	float m01 = invDet * (d[7]*d[2] - d[8]*d[1]);
	float m02 = invDet * (d[5]*d[1] - d[4]*d[2]);
	float m10 = invDet * (d[6]*d[5] - d[8]*d[3]);
	float m11 = invDet * (d[8]*d[0] - d[6]*d[2]);
	float m12 = invDet * (d[3]*d[2] - d[5]*d[0]);
	float m20 = invDet * (d[7]*d[3] - d[6]*d[4]);
	float m21 = invDet * (d[6]*d[1] - d[7]*d[0]);
	float m22 = invDet * (d[4]*d[0] - d[3]*d[1]);

	return Matrix33 <T>( m00, m01, m02,
						 m10, m11, m12,
						 m20, m21, m22 );
}

template <typename T> Matrix33 <T> Matrix33 <T>::rotatedX( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	T r11 = cosA;
	T r12 = -sinA;
	T r21 = sinA;
	T r22 = cosA;

	T m10 = r11*d[3] + r12*d[6];
	T m11 = r11*d[4] + r12*d[7];
	T m12 = r11*d[5] + r12*d[8];

	T m20 = r21*d[3] + r22*d[6];
	T m21 = r21*d[4] + r22*d[7];
	T m22 = r21*d[5] + r22*d[8];

	return Matrix33 <T>( d[0], d[1], d[2],
						 m10, m11, m12,
						 m20, m21, m22 );
}

template <typename T> Matrix33 <T> Matrix33 <T>::rotatedY( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r02 = sinA;
	T r20 = -sinA;
	T r22 = cosA;

	T m00 = r00*d[0] + r02*d[6];
	T m01 = r00*d[1] + r02*d[7];
	T m02 = r00*d[2] + r02*d[8];

	T m20 = r20*d[0] + r22*d[6];
	T m21 = r20*d[1] + r22*d[7];
	T m22 = r20*d[2] + r22*d[8];

	return Matrix33 <T>( m00, m01, m02,
						 d[3], d[4], d[5],
						 m20, m21, m22 );
}

template <typename T> Matrix33 <T> Matrix33 <T>::rotatedZ( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r01 = -sinA;
	T r10 = sinA;
	T r11 = cosA;

	T m00 = r00*d[0] + r01*d[3];
	T m01 = r00*d[1] + r01*d[4];
	T m02 = r00*d[2] + r01*d[5];

	T m10 = r10*d[0] + r11*d[3];
	T m11 = r10*d[1] + r11*d[4];
	T m12 = r10*d[2] + r11*d[5];

	return Matrix33 <T>( m00, m01, m02,
						 m10, m11, m12,
						 d[6], d[7], d[8] );
}

template <typename T> Matrix33 <T> Matrix33 <T>::rotated( T x, T y, T z, T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );
	T xy = x*y;
	T xz = x*z;
	T yz = y*z;

	T r00 = cosA + x*x*(1 - cosA);
	T r01 = xy*(1 - cosA) - z*sinA;
	T r02 = xz*(1 - cosA) + y*sinA;
	T r10 = xy*(1 - cosA) + z*sinA;
	T r11 = cosA + y*y*(1 - cosA);
	T r12 = yz*(1 - cosA) - x*sinA;
	T r20 = xz*(1 - cosA) - y*sinA;
	T r21 = yz*(1 - cosA) - x*sinA;
	T r22 = cosA + z*z*(1 - cosA);

	T m00 = r00*d[0] + r01*d[3] + r02*d[6];
	T m01 = r00*d[1] + r01*d[4] + r02*d[7];
	T m02 = r00*d[2] + r01*d[5] + r02*d[8];

	T m10 = r10*d[0] + r11*d[3] + r12*d[6];
	T m11 = r10*d[1] + r11*d[4] + r12*d[7];
	T m12 = r10*d[2] + r11*d[5] + r12*d[8];

	T m20 = r20*d[0] + r22*d[6] + r22*d[6];
	T m21 = r20*d[1] + r22*d[7] + r22*d[7];
	T m22 = r20*d[2] + r22*d[8] + r22*d[8];

	return Matrix33 <T>( m00, m01, m02,
						 m10, m11, m12,
						 m20, m21, m22 );
}

template <typename T> Matrix33 <T> Matrix33 <T>::rotated( const AxisAngle <T> & aa ) const {
	return rotated( aa.axis.x, aa.axis.y, aa.axis.z, aa.angle );
}

template <typename T> Matrix33 <T> Matrix33 <T>::rotated( const Quaternion <T> & q ) const {
	T i2 = 2*q.i;
	T j2 = 2*q.j;
	T k2 = 2*q.k;

	T ii2 = i2*q.i;
	T ij2 = i2*q.j;
	T ik2 = i2*q.k;
	T ir2 = i2*q.r;
	T jj2 = j2*q.j;
	T jk2 = j2*q.k;
	T jr2 = j2*q.r;
	T kk2 = k2*q.k;
	T kr2 = k2*q.r;

	T r00 = 1 - jj2 - kk2;
	T r01 = ij2 - kr2;
	T r02 = ik2 + jr2;
	T r10 = ij2 + kr2;
	T r11 = 1 - ii2 - kk2;
	T r12 = jk2 - ir2;
	T r20 = ik2 - jr2;
	T r21 = jk2 + ir2;
	T r22 = 1 - ii2 - jj2;

	T m00 = r00*d[0] + r01*d[3] + r02*d[6];
	T m01 = r00*d[1] + r01*d[4] + r02*d[7];
	T m02 = r00*d[2] + r01*d[5] + r02*d[8];

	T m10 = r10*d[0] + r11*d[3] + r12*d[6];
	T m11 = r10*d[1] + r11*d[4] + r12*d[7];
	T m12 = r10*d[2] + r11*d[5] + r12*d[8];

	T m20 = r20*d[0] + r22*d[6] + r22*d[6];
	T m21 = r20*d[1] + r22*d[7] + r22*d[7];
	T m22 = r20*d[2] + r22*d[8] + r22*d[8];

	return Matrix33 <T>( m00, m01, m02,
						 m10, m11, m12,
						 m20, m21, m22 );
}

template <typename T> Matrix33 <T> Matrix33 <T>::scaled( T x, T y, T z ) const {
	return Matrix33 <T>( d[0]*x, d[1]*x, d[2]*x,
						 d[3]*y, d[4]*y, d[5]*y,
						 d[6]*z, d[7]*z, d[8]*z );
}

template <typename T> Matrix33 <T> Matrix33 <T>::scaled( const Vector3 <T> & s ) const {
	return scaled( s.x, s.y, s.z );
}

template <typename T> T Matrix33 <T>::determinant() const {
	return d[0]*(d[8]*d[4] - d[7]*d[5]) + d[3]*(d[7]*d[2] - d[8]*d[1]) + d[6]*(d[5]*d[1] - d[4]*d[2]);
}

template <typename T> T Matrix33 <T>::trace() const {
	return d[0] + d[4] + d[8];
}

template <typename T> const T * Matrix33 <T>::getArray() const {
	return d;
}

template <typename T> Ray3 <T> Matrix33 <T>::transformRay( const Ray3 <T> & r ) const {
	return Ray3 <T>(
		*this * ray.origin,
		*this * ray.direction );
}

template <typename T> Matrix33 <T> Matrix33 <T>::getNormalTransformation() const {
	inverse().transposed();
}

#endif