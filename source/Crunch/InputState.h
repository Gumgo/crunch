/** @file InputState.h
 *  @brief Provides a structure to store data about user input.
 */

#ifndef INPUTSTATE_DEFINED
#define INPUTSTATE_DEFINED

#include "Common.h"
#include "InputEvent.h"
#include <vector>

/** @brief Used to store data about user input.
 */
struct InputState {
	bool closePressed;			/**< Whether the close button was just pressed.*/
	bool focus;					/**< Whether the window has focus.*/
	bool gainedFocus;			/**< Whether the window just gained focus.*/
	bool lostFocus;				/**< Whether the window just lost focus.*/

	bool mouseButtons[MOUSE_BUTTON_CODE_COUNT];			/**< @brief Whether each mouse button is down.*/
	bool mouseButtonsPressed[MOUSE_BUTTON_CODE_COUNT];	/**< @brief Whether each mouse button has just been pressed.*/
	bool mouseButtonsReleased[MOUSE_BUTTON_CODE_COUNT];	/**< @brief Whether each mouse button has just been released.*/

	float mouseScrollWheel;	/**< The direction the mouse wheel was scrolled.*/
	int mousePosition[2];	/**< The coordinates of the mouse in pixels relative to the top-left corner of the window.*/
	int mouseDelta[2];		/**< The raw mouse delta.*/

	bool keys[256];			/**< The key-down state for each key.*/
	bool keysPressed[256];	/**< The key-pressed state for each key.*/
	bool keysReleased[256];	/**< The key-released state for each key.*/

	std::vector <InputEvent> eventStream;	/**< The stream of events since the last update.*/

	/** @brief The constructor initializes all values.
	 */
	InputState();
};

#endif