#ifndef SYSCAMERA2D_DEFINED
#define SYSCAMERA2D_DEFINED

#include "Common.h"

class SysCamera2D {
public:
	void setProjection( float pLeft, float pRight, float pBottom, float pTop );
	void setProjection( float pLeft, float pRight, float pBottom, float pTop, float pNear, float pFar );
	void setCamera( const Vector2f & position );
	void setCameraCenter( const Vector2f & centerPosition );
	void setViewMatrix( const Matrix33af & matrix );
	void setCameraMatrix( const Matrix33af & matrix );

	float getLeft() const;
	float getRight() const;
	float getBottom() const;
	float getTop() const;
	float getNear() const;
	float getFar() const;

	const Matrix44f & getProjectionMatrix() const;
	const Matrix33af & getViewMatrix() const;
	const Matrix33af & getCameraMatrix() const;
	const Matrix44f & getViewProjectionMatrix() const;
	Matrix33af getModelViewMatrix( const Matrix22f & modelMatrix ) const;
	Matrix33af getModelViewMatrix( const Matrix33af & modelMatrix ) const;
	Matrix44f getModelViewProjectionMatrix( const Matrix22f & modelMatrix ) const;
	Matrix44f getModelViewProjectionMatrix( const Matrix33af & modelMatrix ) const;

	// converts a screen point to a world point
	// each component of screen ranges from 0 to 1 and represents position after projection
	Vector2f screenToWorld( const Vector2f & screen ) const;

private:
	float projLeft, projRight, projBottom, projTop, projNear, projFar;
	Matrix44f projectionMatrix;
	Matrix33af viewMatrix;
	Matrix33af cameraMatrix; // inverse of viewMatrix
	Matrix44f viewProjectionMatrix;

	void computeViewProjectionMatrix();
};

#endif