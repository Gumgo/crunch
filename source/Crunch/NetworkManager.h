#if 0

/** @file NetworkManager.h
 *  @brief Provides an interface to the network.
 */

#ifndef NETWORKMANAGER_DEFINED
#define NETWORKMANAGER_DEFINED

#include "Common.h"
#include "NetworkIdLayer.h"
#include <set>
#include <queue>

/** @brief Provides an interface to the network.
 */
class NetworkManager : private boost::noncopyable {
	NetworkIdLayer idLayer;	/**< The network ID layer.*/

	bool initialized;		/**< Whether the network manager is initialized.*/

	uint nextNetworkId;									/**< The next network ID to try and return.*/
	// the following are used to prevent the situation where a network ID
	// is created immediately after the same ID has been destroyed and delayed
	// data is read by the wrong handler. would only happen under very rare
	// circumstances, but this makes it even less likely
	// multiset in case you add and remove the same ID a lot
	std::multiset <ushort> recentlyRemovedNetworkIdSet;				/**< Set of network IDs which have recently been removed.*/
	std::queue <ushort> recentlyRemovedNetworkIdQueue;				/**< Queue of network IDs which have recently been removed.*/
	static const size_t MAX_RECENTLY_REMOVED_NETWORK_ID_COUNT = 30;	/**< How many recently removed network IDs to remember.*/

public:
	NetworkManager();		/**< The constructor.*/
	~NetworkManager();		/**< The destructor.*/

	bool init( ushort port );	/**< Initializes the ID, message, and data layers.*/
	void term();				/**< Terminates the ID, message, and data layers.*/

	/** @brief Enables acceptance of new connections.
	 *  @param onAcceptNewConnectionFn	Called when a new connection is accepted, passing the new connection's ID.
	 */
	void enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn );

	/** @brief Disables acceptance of new connections.
	 */
	void disableAcceptNewConnections();

	/** @brief Sets the function to be called when a connection times out.
	 */
	void setOnConnectionTimeout( const boost::function <void ( size_t )> & onConnectionTimeoutFn );

	/** @brief Creates a new connection.
	 */
	bool createConnection( const std::string & address, ushort port, size_t & newConnectionId );

	/** @brief Destroys a connection.
	 */
	void destroyConnection( size_t connectionId );

	/** @brief Receives and processes incoming data.
	 */
	void receiveAndProcess();

	/** @brief Queues a message with the given network ID.
	 */
	bool queueMessage( size_t connectionId, ushort networkId, NetworkWriter & writer );

	/** @brief Queues a guaranteed message with the given network ID.
	 */
	bool queueGuaranteedMessage( size_t connectionId, ushort networkId, NetworkWriter & writer );

	/** @brief Sends all queued messages.
	 */
	void sendQueued();

	/** @brief Adds or replaced a network ID handler for the given ID.
	 */
	bool addNetworkIdHandler( ushort networkId, const NetworkIdHandler & handler );

	/** @brief Removes the network ID handler for the given ID.
	 */
	bool removeNetworkIdHandler( ushort networkId );

	/** @brief Returns an unused network ID.
	 */
	ushort getUnusedNetworkId();

	/** @brief Returns whether the network manager is initialized.
	 */
	bool isInitialized() const;
};

#endif

#endif