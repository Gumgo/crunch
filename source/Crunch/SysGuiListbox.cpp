#include "SysGuiListbox.h"
#include "Context.h"
#include "SysRenderer2D.h"

int SysGuiListbox::rowAtPoint( const Vector2f & point ) const {
	if (point.x >= (float)position.x && point.y >= (float)position.y &&
		point.x < (float)(position.x + width) && point.y < (float)(position.y + 16*(int)height))
		return ((int)point.y - position.y) / 16 + scroll; // return the row
	else
		return -1;
}

void SysGuiListbox::onCreate() {
	SysGui::onCreate();
	width = 128;
	height = 4;
	scroll = 0;

	selected = -1;
}

void SysGuiListbox::recomputeDisplayedText( size_t i ) {
	if (!font)
		return;
	displayedText[i].clear();
	int maxWidth = width - 4;
	int currentWidth = 0;
	for (size_t t = 0; t < text[i].length(); ++t) {
		currentWidth += font->getGlyph( font->getGlyphIndex( text[i][t] ) ).advance;
		if (currentWidth <= maxWidth)
			displayedText[i] += text[i][t];
		else
			break;
	}
}

void SysGuiListbox::move( const Vector2i & pos ) {
	position = pos;
}

void SysGuiListbox::setWidth( int w ) {
	if (w < 16)
		w = 16;
	width = w;
	for (size_t i = 0; i < text.size(); ++i)
		recomputeDisplayedText( i );
}

void SysGuiListbox::setHeight( size_t h ) {
	height = h;
	if (scroll > (int)text.size() - (int)height)
		scroll = (int)text.size() - (int)height;
	if (scroll < 0)
		scroll = 0;
}

void SysGuiListbox::setText( size_t i, const std::string & t ) {
	if (i < text.size()) {
		text[i] = t;
		recomputeDisplayedText( i );
	}
}

void SysGuiListbox::insertText( size_t i, const std::string & t ) {
	if (i <= text.size()) {
		text.insert( text.begin() + i, t );
		displayedText.insert( displayedText.begin() + i, "" );
		recomputeDisplayedText( i );
		if (selected >= (int)i)
			++selected;
	}
}

void SysGuiListbox::pushText( const std::string & t ) {
	text.push_back( t );
	displayedText.push_back( "" );
	recomputeDisplayedText( text.size() - 1 );
}

void SysGuiListbox::removeText( size_t i, bool callback ) {
	if (i < text.size()) {
		int oldSelected = selected;
		std::string oldText = oldSelected < 0 ? "" : text[oldSelected];
		text.erase( text.begin() + i );
		displayedText.erase( displayedText.begin() + i );
		if (scroll > (int)text.size() - (int)height)
			scroll = (int)text.size() - (int)height;
		if (scroll < 0)
			scroll = 0;
		if (selected == (int)i) {
			selected = -1;
			if (callback) {
				if (onSelect)
					onSelect( selected, selected < 0 ? "" : text[selected] );
				else if (onSelect2)
					onSelect2( selected, selected < 0 ? "" : text[selected], oldSelected, oldText );
			}
		} else if (selected > (int)i)
			--selected;
	}
}

void SysGuiListbox::popText( bool callback ) {
	if (!text.empty()) {
		int oldSelected = selected;
		std::string oldText = oldSelected < 0 ? "" : text[oldSelected];
		text.pop_back();
		displayedText.pop_back();
		if (scroll > (int)text.size() - (int)height)
			scroll = (int)text.size() - (int)height;
		if (scroll < 0)
			scroll = 0;
		if (selected == (int)text.size()) {
			selected = -1;
			if (callback) {
				if (onSelect)
					onSelect( selected, selected < 0 ? "" : text[selected] );
				else if (onSelect2)
					onSelect2( selected, selected < 0 ? "" : text[selected], oldSelected, oldText );
			}
		}
	}
}

std::string SysGuiListbox::getText( size_t i ) const {
	if (i < text.size())
		return text[i];
	else
		return "";
}

size_t SysGuiListbox::getRows() const {
	return text.size();
}

void SysGuiListbox::select( int i, bool callback ) {
	int oldSelected = selected;
	if (i >= 0 && i < (int)text.size())
		selected = i;
	else
		selected = -1;
	if (callback) {
		if (onSelect)
			onSelect( selected, selected < 0 ? "" : text[selected] );
		else if (onSelect2)
			onSelect2( selected, selected < 0 ? "" : text[selected], oldSelected, oldSelected < 0 ? "" : text[oldSelected] );
	}
}

int SysGuiListbox::getSelection() const {
	return selected;
}

void SysGuiListbox::setOnSelect( const boost::function <void ( int, const std::string & )> & callback ) {
	onSelect = callback;
	onSelect2.clear();
}

void SysGuiListbox::setOnSelect( const boost::function <void ( int, const std::string &, int, const std::string & )> & callback ) {
	onSelect.clear();
	onSelect2 = callback;
}

void SysGuiListbox::receiveEvent( const InputEvent & ev ) {
	if (ev.type == IET_MOUSE_WHEEL) {
		Vector2i mouseWindow = Context::get().inputManager.mousePosition();
		Vector2f mouseWorld = camera->screenToWorld( Vector2f(
			(float)mouseWindow.x / (float)Context::get().window.getWidth(),
			(float)mouseWindow.y / (float)Context::get().window.getHeight() ) );

		if (rowAtPoint( mouseWorld ) >= 0) {
			scroll -= (int)sign( ev.mouseWheelEvent.scroll );
			if (scroll > (int)text.size() - (int)height)
				scroll = (int)text.size() - (int)height;
			if (scroll < 0)
				scroll = 0;
		}
	} else if (ev.type == IET_MOUSE_BUTTON && ev.mouseButtonEvent.button == MBC_LEFT && ev.mouseButtonEvent.type == MBET_PRESSED) {
		Vector2i mouseWindow = Context::get().inputManager.mousePosition();
		Vector2f mouseWorld = (Vector2f)mouseWindow; // TODO: do this conversion

		int row = rowAtPoint( mouseWorld );
		if (row >= 0) {
			int oldSelected = selected;
			if (row < (int)text.size()) {
				if (selected == row)
					selected = -1;
				else
					selected = row;
				if (onSelect)
					onSelect( selected, selected < 0 ? "" : text[selected] );
				else if (onSelect2)
					onSelect2( selected, selected < 0 ? "" : text[selected], oldSelected, oldSelected < 0 ? "" : text[oldSelected] );
			} else if (selected != -1) {
				selected = -1;
				if (onSelect)
					onSelect( selected, selected < 0 ? "" : text[selected] );
				else if (onSelect2)
					onSelect2( selected, selected < 0 ? "" : text[selected], oldSelected, oldSelected < 0 ? "" : text[oldSelected] );
			}
		}
	}
}

void SysGuiListbox::onDismissParent() {
}

void SysGuiListbox::draw() {
	if (camera != NULL) {
		Matrix33af modelMatrix;
		modelMatrix.translate( (float)position.x, (float)position.y );
		Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

		GpuBlendState blendState = Context::get().gpuState.createBlendState();
		blendState.setBlendFunction( GpuBlendState::BM_NONE );
		blendState.bind();

		SysRenderer2D::get().drawGradientVert( mvpMatrix,
			Vector2f( 0.0f, 0.0f ), Vector2f( (float)width, (float)height * 16.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );

		for (size_t i = 0; i < height; ++i) {
			Vector2f pos( 1.0f, 16.0f * (float)i );
			Vector2f size( (float)width - 2.0f, 16.0f );
			if (i == 0) {
				pos.y += 1.0f;
				size.y -= 1.0f;
			}
			if (i == height - 1)
				size.y -= 1.0f;
			if ((int)i + scroll == selected)
				SysRenderer2D::get().drawGradientVert( mvpMatrix,
					pos, size,
					Vector4f( 0.5625f, 0.5625f, 0.75f, 1.0f ),
					Vector4f( 0.75f, 0.75f, 1.0f, 1.0f ) );
			else
				SysRenderer2D::get().drawGradientVert( mvpMatrix,
					pos, size,
					Vector4f( 0.75f, 0.75f, 0.75f, 1.0f ),
					Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ) );
		}

		modelMatrix.translate( 2.0f, 8.0f );

		for (size_t i = scroll; i < text.size() && i - scroll < height; ++i) {
			mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );
			SysRenderer2D::get().drawText( displayedText[i], *font, mvpMatrix, TAH_LEFT, TAV_MIDDLE );
			modelMatrix.translate( 0.0f, 16.0f );
		}
	}
}