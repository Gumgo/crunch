#include "SysSkybox.h"
#include "Context.h"

SysSkybox::SysSkybox() {
	skyboxProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___skybox", true );
	camera = NULL;

	static const Vertex vertices[] = {
		{ -1.0f, -1.0f, -1.0f },
		{  1.0f, -1.0f, -1.0f },
		{ -1.0f,  1.0f, -1.0f },
		{  1.0f,  1.0f, -1.0f },
		{ -1.0f, -1.0f,  1.0f },
		{  1.0f, -1.0f,  1.0f },
		{ -1.0f,  1.0f,  1.0f },
		{  1.0f,  1.0f,  1.0f }
	};

	// make faces using cc winding order
	static const ushort indices[] = {
		0, 1, 2, 2, 1, 3, // -z
		5, 4, 7, 7, 4, 6, // +z
		2, 6, 0, 0, 6, 4, // -x
		1, 5, 3, 3, 5, 7, // +x
		0, 4, 1, 1, 4, 5, // -y
		3, 7, 2, 2, 7, 6  // +y
	};

	GpuBuffer::BufferDescription vDesc;
	vDesc.type = GL_ARRAY_BUFFER;
	vDesc.size = sizeof( vertices );
	vDesc.data = vertices;
	vDesc.usage = GL_STATIC_DRAW;
	vertexBuffer = Context::get().gpuState.createBuffer( vDesc );

	GpuBuffer::BufferDescription iDesc;
	iDesc.type = GL_ELEMENT_ARRAY_BUFFER;
	iDesc.size = sizeof( indices );
	iDesc.data = indices;
	iDesc.usage = GL_STATIC_DRAW;
	indexBuffer = Context::get().gpuState.createBuffer( iDesc );

	autoProgram.setProgram( skyboxProgram->getProgram() );
	autoProgram.setAttribute( "position", vertexBuffer, 3, GL_FLOAT, false, false, sizeof( Vertex ),
		bufferOffset( offsetof( Vertex, position ) ) );
	autoProgram.setUniform( "mvpMatrix", GL_FLOAT_MAT4, &uniforms.mvpMatrix );
	autoProgram.setUniform( "skyRotationMatrix", GL_FLOAT_MAT3, &uniforms.skyRotationMatrix );
	autoProgram.setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
	autoProgram.setUniform( "colorMap", GL_SAMPLER_CUBE, &uniforms.colorMap );
	uniforms.color.set( 1.0f, 1.0f, 1.0f );
	uniforms.colorMap = 0;
}

SysSkybox::~SysSkybox() {
}

void SysSkybox::setTexture( TextureResourceReference & tex ) {
	texture = tex;
}

void SysSkybox::setCamera( const SysCamera3D * cam ) {
	camera = cam;
}

void SysSkybox::setColor( const Vector3f & c ) {
	uniforms.color = c;
}

Vector3f SysSkybox::getColor() const {
	return uniforms.color;
}

void SysSkybox::setSkyRotation( const Matrix33f & r ) {
	uniforms.skyRotationMatrix = r;
}

const Matrix33f & SysSkybox::getSkyRotation() const {
	return uniforms.skyRotationMatrix;
}

void SysSkybox::draw() {
	if (skyboxProgram->getStatus() != Resource::S_LOADED || !texture || texture->getStatus() != Resource::S_LOADED || camera == NULL)
		return;

	vertexBuffer->bind();
	indexBuffer->bind();

	texture->getTexture()->bind( (size_t)uniforms.colorMap );

	Matrix44f viewMatrix;
	memcpy( &viewMatrix[0], camera->getViewMatrix().getArray(), sizeof( Matrix44af ) );
	viewMatrix[3] = viewMatrix[7] = viewMatrix[11] = 0.0f;
	uniforms.mvpMatrix = camera->getProjectionMatrix() * viewMatrix;
	autoProgram.bind();

	GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
	drawCall.setParameters( GL_TRIANGLES, 0, 7, indexBuffer->getSize() / sizeof( ushort ), GL_UNSIGNED_SHORT,
		bufferOffset( 0 ) );
	drawCall.draw();
}