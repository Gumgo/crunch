#include "TextUtil.h"

Vector2i getTextDimens( FontResource & font, const std::string & text ) {
	uint maxWidth = 0;
	uint currentWidth = 0;
	uint height = font.getLineHeight();

	for (size_t i = 0; i < text.length(); ++i) {
		char ch = text[i];
		if (ch == '\n') {
			currentWidth = 0;
			height += font.getLineHeight();
		} else {
			currentWidth += font.getGlyph( font.getGlyphIndex( ch ) ).advance;
			maxWidth = std::max( currentWidth, maxWidth );
		}
	}

	return Vector2i( (int)maxWidth, (int)height );
}

std::string breakString( FontResource & font, const std::string & text, int maxWidth ) {
	std::string brokenString;
	brokenString.reserve( text.length() );

	int currentWidth = 0;
	bool spaceFound = false;
	int lastSpaceIndex = 0;
	size_t i = 0;

	while (i < text.length()) {
		char ch = text[i];
		// if a newline, immediately break
		if (ch == '\n') {
			++i;
			// pop trailing spaces
			while (!brokenString.empty() && brokenString.back() == ' ')
				brokenString.pop_back();
			brokenString.push_back( '\n' );
			currentWidth = 0;
			spaceFound = false;
			lastSpaceIndex = -1;
			continue;
		}

		// find the new width
		int chWidth = (int)font.getGlyph( font.getGlyphIndex( ch ) ).advance;
		int newWidth = currentWidth + chWidth;
		if (ch == ' ') {
			// if the character is a space
			spaceFound = true;
			lastSpaceIndex = (int)brokenString.size();
			brokenString.push_back( ' ' );
			currentWidth = newWidth;
			++i;
			continue;
		}

		// otherwise, potentially linebreak
		if (currentWidth == 0 || newWidth <= maxWidth) {
			// always add at least one
			// also, if we don't go over, no problem
			brokenString.push_back( ch );
			currentWidth = newWidth;
			++i;
		} else {
			// otherwise, we need to find where to break
			if (!spaceFound) {
				// if no space has been found yet, immediately break
				brokenString.push_back( '\n' );
				brokenString.push_back( ch );
				currentWidth = 0;
				++i;
			} else {
				// there was a previous space, so we break there
				brokenString[lastSpaceIndex] = '\n';
				// remove trailing spaces before the linebreak
				for (int t = lastSpaceIndex-1; t >= 0 && brokenString[t] == ' '; --t)
					brokenString.erase( t, 1 );
				brokenString.push_back( ch );
				spaceFound = false;
				// compute the length of the current line so far
				currentWidth = 0;
				for (size_t t = lastSpaceIndex+1; t < brokenString.length(); ++t)
					currentWidth += (int)font.getGlyph( font.getGlyphIndex( brokenString[t] ) ).advance;
				++i;
			}
		}
	}

	// pop trailing spaces
	while (!brokenString.empty() && brokenString.back() == ' ')
		brokenString.pop_back();

	return brokenString;
}

uint getLineWidth( FontResource & font, const std::string & text, size_t index ) {
	uint currentWidth = 0;
	for (size_t i = index; i < text.length() && text[i] != '\n'; ++i) {
		uint chWidth = font.getGlyph( font.getGlyphIndex( text[i] ) ).advance;
		currentWidth += chWidth;
	}

	return currentWidth;
}