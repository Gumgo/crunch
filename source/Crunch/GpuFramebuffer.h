/** @file GpuFramebuffer.h
 *  @brief Encapsulates a framebuffer on the GPU.
 */

#ifndef GPUFRAMEBUFFER_DEFINED
#define GPUFRAMEBUFFER_DEFINED

#include "Common.h"
#include "GpuObject.h"
#include "GpuRenderbuffer.h"
#include "GpuTexture.h"
#include <vector>

/** @brief Encapsulates a framebuffer on the GPU.
 */
class GpuFramebuffer : public ReferenceCountedGpuObject {
public:
	GLuint getId() const;	/**< Returns the ID of the underlying buffer.*/

	void bind( GLenum location );	/**< Binds the buffer to the given location.*/

	/** @brief Attaches the renderbuffer to the given attachment location.
	 */
	void attach( GLenum attachment, const GpuRenderbufferReference & renderbuffer );

	/** @brief Attaches the texture to the given attachment location.
	 */
	void attach( GLenum attachment, const GpuTextureReference & texture, int mipmapLevel = 0 );

	/** @brief Attaches the layer in the texture to the given attachment location.
	 */
	void attachLayer( GLenum attachment, const GpuTextureReference & texture, uint layer, int mipmapLevel = 0 );

	/** @brief Detatches the texture or renderbuffer from the given attachment location.
	 */
	void detatch( GLenum attachment );

	/** @brief Returns the renderbuffer attached to the given attachment location, or an empty reference if none is attached.
	 */
	GpuRenderbufferReference getAttachedRenderbuffer( GLenum attachment );

	/** @brief Returns the renderbuffer attached to the given attachment location, or an empty reference if none is attached.
	 */
	GpuRenderbufferConstReference getAttachedRenderbuffer( GLenum attachment ) const;

	/** @brief Returns the texture attached to the given attachment location, or an empty reference if none is attached.
	 */
	GpuTextureReference getAttachedTexture( GLenum attachment );

	/** @brief Returns the texture attached to the given attachment location, or an empty reference if none is attached.
	 */
	GpuTextureConstReference getAttachedTexture( GLenum attachment ) const;

	/** @brief Returns the mipmap level of the texture attached to the given attachment location.
	 */
	int getAttachmentLevel( GLenum attachment ) const;

	/** @brief Returns the layer of the texture attached to the given attachment location.
	 */
	uint getAttachmentLayer( GLenum attachment ) const;

private:
	friend class GpuState;
	GpuFramebuffer( GpuState * gpuState );

	~GpuFramebuffer();

	void onRelease() const;

	GLuint id;

	// attachment holds either renderbuffer OR texture (or none)
	struct Attachment {
		GpuRenderbufferReference renderbuffer;
		GpuTextureReference texture;
		int mipmapLevel;
		uint layer;
		bool layered;
	};
	Attachment & getAttachment( GLenum attachment );
	const Attachment & getAttachment( GLenum attachment ) const;

	Attachment depthAttachment;
	Attachment stencilAttachment;
	std::vector <Attachment> colorAttachments;
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <GpuFramebuffer> GpuFramebufferReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <GpuFramebuffer> GpuFramebufferConstReference;

#endif