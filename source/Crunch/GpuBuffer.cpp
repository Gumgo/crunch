#include "GpuBuffer.h"
#include "GpuState.h"

GpuBuffer::BufferDescription::BufferDescription() {
	type = GL_ARRAY_BUFFER;
	size = 0;
	data = NULL;
	usage = GL_STATIC_DRAW;
}

GpuBuffer::GpuBuffer( GpuState * gpuState, const BufferDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	type = desc.type;
	size = desc.size;
	usage = desc.usage;

	glGenBuffers( 1, &id );
	bind();
	glBufferData( type, (GLsizeiptr)size, desc.data, usage );
}

GpuBuffer::~GpuBuffer() {
	// whent the buffer is deleted, all references to it have been released
	glDeleteBuffers( 1, &id );
}

GLuint GpuBuffer::getId() const {
	return id;
}

GLenum GpuBuffer::getType() const {
	return type;
}

size_t GpuBuffer::getSize() const {
	return size;
}

GLenum GpuBuffer::getUsage() const {
	return usage;
}

void GpuBuffer::bind() {
	// implicit conversion to "smart reference"
	getGpuState()->bindBuffer( this );
}

void GpuBuffer::bind( GLenum location ) {
	// implicit conversion to "smart reference"
	getGpuState()->bindBuffer( location, this );
}

bool GpuBuffer::write( size_t bufferOffset, size_t dataSize, const void * data ) {
	return write( type, bufferOffset, dataSize, data );
}

bool GpuBuffer::write( GLenum location, size_t bufferOffset, size_t dataSize, const void * data ) {
	if (bufferOffset + dataSize > size)
		return false;
	else {
		bind( location );
		glBufferSubData( location, (GLintptr)bufferOffset, (GLsizeiptr)dataSize, data );
		return true;
	}
}

void GpuBuffer::orphan() {
	orphan( type );
}

void GpuBuffer::orphan( GLenum location ) {
	bind( location );
	glBufferData( location, (GLsizeiptr)size, NULL, usage );
}

void GpuBuffer::onRelease() const {
	// no references left
	delete this;
}