/** @file Physics2DBody.h
 *  @brief The object defining physical bodies in the simulation.
 */
#ifndef PHYSICS2DBODY_DEFINED
#define PHYSICS2DBODY_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DShape.h"

namespace Physics2D {

class Simulation;
struct BodyDefinition;

/** @brief Defines physical bodies in the simulation.
 */
class Body : private boost::noncopyable {
public:
	friend class Simulation;

	/** @brief The type of body.
	 */
	enum Type {
		T_STATIC,			/**< The body is not affected by other bodies and cannot move.*/
		T_STATIC_MOVABLE,	/**< The body is not affected by other bodies but can move and have velocity.*/
		T_DYNAMIC,			/**< The body is fully dynamic and affected by other bodies.*/
		TYPE_COUNT			/**< The number of types.*/
	};

	// getters

	Simulation * getSimulation();				/**< Returns a pointer to the simulation.*/
	const Simulation * getSimulation() const;	/**< Returns a pointer to the simulation.*/

	Type getType() const;						/**< Returns the type of the body.*/

	Shape * attachShape( const ShapeDefinition & d );		/**< Attaches a shape to this body.*/
	void removeShape( Shape * s );							/**< Removes the given shape from this body.*/
	Shape * getShapes();									/**< Returns the list of attached shapes.*/
	const Shape * getShapes() const;						/**< Returns the list of attached shapes.*/
	Shape * getNextShape( Shape * s );						/**< Returns the next shape given a shape.*/
	const Shape * getNextShape( const Shape * s ) const;	/**< Returns the next shape given a shape.*/
	size_t getShapeCount() const;							/**< Returns the number of attached shapes.*/

	MassData getMassData() const;				/**< Returns the body's mass data.*/

	Vector2r getPosition() const;				/**< Returns the body's position.*/
	Real getRotation() const;					/**< Returns the body's rotation.*/
	Matrix33ar getMatrixTransform() const;		/**< Returns the body's transformation in matrix form.*/

	Vector2r getLinearVelocity() const;			/**< Returns the body's linear velocity.*/
	Real getAngularVelocity() const;			/**< Returns the body's angular velocity.*/

	Vector2r getForce() const;					/**< Returns the force on the body.*/
	Real getTorque() const;						/**< Returns the torque on the body.*/

	bool isRotationFixed() const;				/**< Returns whether rotation is fixed.*/

	// setters/mutators

	void setTransform( const Vector2r & pos, Real rot );				/**< Sets the body's position and rotation.*/

	void setLinearVelocity( const Vector2r & vel );						/**< Sets the body's linear velocity.*/
	void setAngularVelocity( Real vel );								/**< Sets the body's angular velocity.*/

	void applyForceToCenter( const Vector2r & f );						/**< Applies a force to the center of the body.*/
	void applyForce( const Vector2r & f, const Vector2r & pt );			/**< Applies a force to a point on the body.*/
	void applyTorque( Real t );											/**< Applies a torque to the body.*/

	void applyLinearImpulseToCenter( const Vector2r & i );				/**< Applies a linear impulse to the center of the body.*/
	void applyLinearImpulse( const Vector2r & i, const Vector2r & pt );	/**< Applies a linear impulse to a point on the body.*/
	void applyAngularImpulse( Real i );									/**< Applies an angular impulse to the body.*/

	void setFixedRotation( bool f );									/**< Sets whether rotation is fixed.*/

private:
	Body( const BodyDefinition & d, Simulation * s );

	Simulation * simulation;	// pointer back to the simulation

	Type type;					// type of the body

	Shape * shapes;				// the list of attached shapes
	size_t shapeCount;			// the number of attached shapes

	MassData massData;			// body's mass data

	Vector2r position;			// body's position
	Real rotation;				// body's rotation

	Vector2r linearVelocity;	// body's linear velocity
	Real angularVelocity;		// body's angular velocity

	Vector2r force;				// The force on the body
	Real torque;				// The torque on the body

	bool fixedRotation;			// whether rotation is fixed

	void computeMassData();		// computes the mass data from the attached shapes

	// data used while the simulation is updating.
	struct SimulationData {
		Body * next;							// the next body in the simulation
		Body * prev;							// the previous body in the simulation
		Vector2r intermediateLinearVelocity;	// linear velocity after integration
		Real intermediateAngularVelocity;		// angular velocity after integration
		Vector2r intermediateWorldCenterOfMass;	// the center of mass relative to the world at intermediate points in the simulation
		Real intermediateRotation;				// rotation at intermediate points in the simulation
		Vector2r worldCenterOfMass;				// the center of mass in world space
	};

	SimulationData simulationData;
	SimulationData & getSimulationData();
	const SimulationData & getSimulationData() const;
};

/** @brief Used to create new bodies in the physical simulation.
 */
struct BodyDefinition {
	Body::Type type;				/**< The type of body.*/

	Vector2r initialPosition;		/**< The body's initial position in meters.*/
	Real initialRotation;			/**< The body's initial rotation in radians.*/

	Vector2r initialLinearVelocity;	/**< The body's initial linear velocity in meters per second.*/
	Real initialAngularVelocity;	/**< The body's initial angular velocity in radians per second.*/

	bool fixedRotation;				/**< Whether rotation should be fixed.*/

	BodyDefinition();				/**< Assigns default values.*/
};

}

#endif