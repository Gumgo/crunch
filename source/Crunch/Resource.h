/** @file Resource.h
 *  @brief Contains the base class for externally loaded resources.
 */

#ifndef RESOURCE_DEFINED
#define RESOURCE_DEFINED

#include "Common.h"
#include "Semaphore.h"
#include "FastDelegate.h"
#include "KeyValueReader.h"

class ResourceManager;

/** @brief The base class for externally loaded resources.
 */
class Resource : private boost::noncopyable {
public:
	/** @brief The status of a resource.
	 */
	enum Status {
		S_NOT_LOADED,			/**< The resource has not yet been loaded.*/
		S_ASYNC_PENDING,		/**< The resource is queued for asynchronous loading.*/
		S_ASYNC_LOADING,		/**< The resource is currently being asynchronously loaded.*/
		S_SYNC_PENDING,			/**< The resource is queued for synchronous loading.*/
		S_SYNC_PENDING_FAILED,	/**< The resource is queued for synchronous loading but failed to load asynchronously.*/
		S_LOADED,				/**< The resource has been loaded and is ready to use.*/
		S_FAILED,				/**< The resource failed to load.*/
	};

protected:
	/** @brief The constructor.
	 */
	Resource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

	/** @brief The destructor.
	 */
	virtual ~Resource();

	/** @brief Responsible for loading the asynchronous component of the data.
	 */
	virtual bool loadDataAsync() = 0;

	/** @brief Responsible for loading the synchronous component of the data.
	 */
	virtual bool loadDataSync() = 0;

	/** @brief Responsible for freeing the data.
	 */
	virtual void freeData() = 0;

public:
	// for reading metadata, implement the following methods:
	//static bool readMetadata( uint & metadata, KeyValueReader::Iterator it );
	//static uint getDefaultMetadata();

	/** @brief Returns the unique string identifying the class of this resource (e.g. "texture").
	 */
	virtual const char * getResourceClassName() const = 0;

	/** @brief Returns the unique string identifying the category of this resource (e.g. "textures").
	 */
	virtual const char * getResourceCategoryName() const = 0;

	std::string getName() const;		/**< Returns the name of this resource.*/
	std::string getPath() const;		/**< Returns the path of this resource.*/
	uint getMetadata() const;			/**< Returns the metadata of this resource.*/

	size_t getReferenceCount() const;	/**< Returns the reference count.*/
	void acquire() const;				/**< Increments the reference count.*/
	void release() const;				/**< Decrements the reference count, freeing the resource if it reaches 0.*/

	Status getStatus() const;			/**< Returns the status of this resource.*/
	Status getDetailedStatus() const;	/**< Returns the detailed status of this resource.*/

private:
	friend class ResourceManager;
	ResourceManager * resourceManager;	// a pointer to the resource manager

	void setStatus( Status s );			// sets the status of this resource
	void setDetailedStatus( Status s );	// sets the detailed status of this resource

	std::string name;					// the name of the resource
	std::string path;					// the path of the resource
	uint metadata;						// the metadata of this resource

	mutable size_t referenceCount;		// the number of times this particular resource has been acquired

	Status status;						// the resource status
	Status detailedStatus;				// the detailed resource status
};

/** @brief Used to define a class of resources.
 */
#define DEFINE_RESOURCE_CLASS( name, className, categoryName )	\
class name : public Resource {									\
public:															\
	const char * getResourceClassName() const {					\
		return className;										\
	}															\
																\
	const char * getResourceCategoryName() const {				\
		return categoryName;									\
	}															\
																\
	static const char * getStaticResourceClassName() {			\
		return className;										\
	}															\
																\
	static const char * getStaticResourceCategoryName() {		\
		return categoryName;									\
	}															\
																\
private:

/** @brief Base class for resource references.
 */
typedef ReferenceCountedPointer <Resource> ResourceReference;

/** @brief Base class for resource references.
 */
typedef ReferenceCountedConstPointer <Resource> ResourceConstReference;

#endif