#vertex_shader
#version 330

in vec2 corner;

out vec2 uv;

void main() {
	uv = corner;
	vec2 clip = corner * 2.0 - vec2( 1.0 );
	gl_Position = vec4( clip, 0.0f, 1.0 );
}

#fragment_shader
#version 330
precision highp float;

in vec2 uv;

uniform sampler2D source;
// size of 1 source pixel in UV coordinates (i.e. 1/(image size))
uniform vec2 uvPixelSize;

out vec4 outColor;

float luma( in vec3 c ) {
	return dot( c, vec3( 0.2126, 0.7152, 0.0722 ) );
}

void main() {
	// 5-tap blur
	//    +---+---+---+---+---+
	// +2 | 1 | 4 | 6 | 4 | 1 |
	//    +---+---+---+---+---+
	// +1 | 4 |16 |24 |16 | 4 |
	//    +---+---+---+---+---+
	//  0 | 6 |24 |36 |24 | 6 |
	//    +---+---+---+---+---+
	// -1 | 4 |16 |24 |16 | 4 |
	//    +---+---+---+---+---+
	// -1 | 1 | 4 | 6 | 4 | 1 |
	//    +---+---+---+---+---+
	//     -2  -1   0  +1  +2
	// performing a separable blur would result in 6 samples
	// however, this requires an extra render target, so we do it in one pass
	// we can be clever with linear interpolation to reduce the number of samples from 25 to 9
	// the total weight is 256, so all weights should be divided by that amount
	// - we sample the center individually
	//   - sample at (0,0), weight of 36
	// - we can capture the two center strips with 2 samples each
	//   - each edge of each strip is weighted by 6 + 24 = 30
	//   - we sample 6/30 toward the outside
	//   - this causes the inner pixel to get 24/30 weight and the outer pixel to get 6/30 weight
	// - we can capture the four corners with 1 sample each
	//   - in general this would not be possible; however the weights in this case make it work
	//   - each corner should be weighted by 1 + 4 + 4 + 16 = 25
	//   - we sample 1/5 toward the outside in each direction
	//   - this causes the inner pixel to get (4/5)^2 = 16/25 weight,
	//     the outer pixel to get (1/5)^2 = 1/16 weight,
	//     and the other two pixels to get (1/5)(4/5) = 4/15 weight

	const vec3 offsetsWeights[9] = {
		vec3( 0.0, 0.0, 0.140625 ), // center
		vec3( 1.2, 0.0, 0.1171875 ), vec3( -1.2, 0.0, 0.1171875 ), vec3( 0.0, 1.2, 0.1171875 ), vec3( 0.0, -1.2, 0.1171875 ), // center strips
		vec3( 1.2, 1.2, 0.09765625 ), vec3( -1.2, 1.2, 0.09765625 ), vec3( 1.2, -1.2, 0.09765625 ), vec3( -1.2, -1.2, 0.09765625 ) // corners
	};

	outColor = vec4( 0.0 );
	for (int i = 0; i < 9; ++i)
		outColor += texture( source, uv + uvPixelSize*offsetsWeights[i].xy ) * offsetsWeights[i].z;
}