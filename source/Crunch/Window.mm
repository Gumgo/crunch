#ifdef __APPLE__
// the apple implementation of the window class

#include "Window.h"
#include "Context.h"
#include "Log.h"

void WindowWrapper::setActive( bool a ) {
	window->active = a;
}

void WindowWrapper::setFullscreen( bool f ) {
	window->fullscreen = f;
}

void WindowWrapper::setQuit( bool q ) {
	window->quit = q;
}

void WindowWrapper::pushEvent( InputEvent ie ) {
	window->eventData.push_back( ie );
}

@implementation InternalWindow

- (void)setWindow:(Window *)win {
	window.window = win;
}

- (void)mouseDown:(NSEvent *)theEvent {
	InputEvent ev;
	ev.type = IET_MOUSE_BUTTON;
	ev.mouseButtonEvent.type = MBET_PRESSED;
	ev.mouseButtonEvent.button = (int)MBC_LEFT;
	// TODO mouse position
	window.pushEvent( ev );
}

- (void)mouseUp:(NSEvent *)theEvent {
	InputEvent ev;
	ev.type = IET_MOUSE_BUTTON;
	ev.mouseButtonEvent.type = MBET_RELEASED;
	ev.mouseButtonEvent.button = (int)MBC_LEFT;
	// TODO mouse position
	window.pushEvent( ev );
}

- (void)rightMouseDown:(NSEvent *)theEvent {
	InputEvent ev;
	ev.type = IET_MOUSE_BUTTON;
	ev.mouseButtonEvent.type = MBET_PRESSED;
	ev.mouseButtonEvent.button = (int)MBC_RIGHT;
	// TODO mouse position
	window.pushEvent( ev );
}

- (void)rightMouseUp:(NSEvent *)theEvent {
	InputEvent ev;
	ev.type = IET_MOUSE_BUTTON;
	ev.mouseButtonEvent.type = MBET_RELEASED;
	ev.mouseButtonEvent.button = (int)MBC_RIGHT;
	// TODO mouse position
	window.pushEvent( ev );
}

- (void)scrollWheel:(NSEvent *)theEvent {
	if (theEvent.deltaY != 0.0f) {
		InputEvent ev;
		ev.mouseWheelEvent.scroll = theEvent.deltaY;
		// TODO mouse position
		window.pushEvent( ev );
	}
}

- (void)keyDown:(NSEvent *)theEvent {
	InputEvent ev;
	ev.type = IET_KEY;
	ev.keyEvent.type = KET_PRESSED;
	ev.keyEvent.keyCode = (int)theEvent.keyCode;
	window.pushEvent( ev );
	// add the interpreted characters
	NSString * characters = theEvent.characters;
	for (UINT i = 0; i < characters.length; ++i) {
		unichar ch = [characters characterAtIndex:i];
		if (ch < 256) { // valid ASCII
			ev.type = IET_CHARACTER;
			ev.characterEvent.characterCode = (int)ch;
			window.pushEvent( ev );
		}
	}
}

- (void)keyUp:(NSEvent *)theEvent {
	InputEvent ev;
	ev.type = IET_KEY;
	ev.keyEvent.type = KET_RELEASED;
	ev.keyEvent.keyCode = (int)theEvent.keyCode;
	window.pushEvent( ev );
}

- (void)windowShouldClose:(id)sender {
	InputEvent ev;
	ev.windowEvent.type = WET_CLOSE;
	window.pushEvent( ev );
}

- (void)windowDidBecomeKey:(NSNotification *)notification {
	InputEvent ev;
	ev.type = IET_WINDOW;
	ev.windowEvent.type = WET_GAINED_FOCUS;
	window.pushEvent( ev );
}

- (void)windowDidResignKey:(NSNotification *)notification {
	InputEvent ev;
	ev.type = IET_WINDOW;
	ev.windowEvent.type = WET_LOST_FOCUS;
	window.pushEvent( ev );
}

@end

Window::Window() {
	window = nil;

	active = true;
	fullscreen = false;
	
	created = false;
	quit = false;
	
	width = 0;
	height = 0;
	bitsPerPixel = 0;
}

Window::~Window() {
	destroy();
}

void Window::create( const std::string & text, int w, int h, int bits, bool full ) {
	if (created)
		return;

	bitsPerPixel = bits;
	title = text;
	fullscreen = full;

	NSRect mainRect;
	if (full) {
		mainRect = [[NSScreen mainScreen] frame];
		window = [[InternalWindow alloc] initWithContentRect:mainRect
												   styleMask:NSClosableWindowMask
													 backing:NSBackingStoreBuffered
													   defer:YES];
		if (window != nil)
			[window setHidesOnDeactivate:YES];
	} else {
		mainRect = NSMakeRect( 0.0f, 0.0f, (float)w, (float)h );
		window = [[InternalWindow alloc] initWithContentRect:mainRect
												   styleMask:NSClosableWindowMask
													 backing:NSBackingStoreBuffered
													   defer:YES];
	}

	[window setWindow:this];
	[window setDelegate:window];

	width = (int)mainRect.size.width;
	height = (int)mainRect.size.height;

	if (window == nil)
		throw std::runtime_error( "Failed to create window: failed to allocate window" );

	[window setTitle:[NSString stringWithCString:text.c_str() encoding:NSASCIIStringEncoding]];

	// set up OpenGL

	glewInit();

	NSOpenGLPixelFormatAttribute attribs[] = {
		NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion3_2Core,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFAColorSize, bits,
		0
	};
	NSOpenGLPixelFormat * pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attribs];
	NSRect viewRect = NSMakeRect( 0.0f, 0.0f, mainRect.size.width, mainRect.size.height );
	NSOpenGLView * glView = [[NSOpenGLView alloc] initWithFrame:viewRect pixelFormat:pixelFormat];
	if (glView == nil) {
		[window release];
		throw std::runtime_error( "Failed to create window: failed to initialize OpenGL" );
	}
	[[glView openGLContext] makeCurrentContext];
	[window setContentView:glView];

	[window makeKeyAndOrderFront:nil]; // can "sender" be nil?

	quit = false;
	created = true;
	active = true;
	Log::info() << "Window initialized";
}

void Window::destroy() {
	[window close];
	[window release];
	window = nil;
	
	created = false;
}

int Window::getWidth() const {
	if (!created) {
		Log::warning() << "Unable to get width: window not initialized";
		return -1;
	}
	return width;
}

int Window::getHeight() const {
	if (!created) {
		Log::warning() << "Unable to get height: window not initialized";
		return -1;
	}
	return height;
}

std::string Window::getTitle() const {
	if (!created) {
		Log::warning() << "Unable to get title: window not initialized";
		return "";
	}
	return title;
}

void Window::setTitle( std::string text ) {
	if (!created) {
		Log::warning() << "Unable to set title: window not initialized";
		return;
	}
	[window setTitle:[NSString stringWithCString:text.c_str() encoding:NSASCIIStringEncoding]];
}

void Window::setMousePosition( int x, int y ) {
	if (!created) {
		Log::warning() << "Unable to set mouse position: window not initialized";
		return;
	}

	// TODO: can't figure out how to get this working!
}

bool Window::getActive() const {
	if (!created) {
		Log::warning() << "Unable to get active status: window not initialized";
		return false;
	}
	return active;
}

bool Window::getFullscreen() const {
	if (!created) {
		Log::warning() << "Unable to get fullscreen status: window not initialized";
		return false;
	}
	return fullscreen;
}

bool Window::setFullscreen( bool flag ) {
	if (!created) {
		Log::warning() << "Unable to set fullscreen status: window not initialized";
		return false;
	}
	if (fullscreen == flag)
		return true;
	else {
		destroy();
		fullscreen = !fullscreen;
		try {
			create( title, width, height, bitsPerPixel, fullscreen );
		} catch (const std::exception & e) {
			Log::error() << e.what();
			Log::warning() << "Failed to switch to fullscreen mode";
			fullscreen = !fullscreen;
			try {
				create( title, width, height, bitsPerPixel, fullscreen );
			} catch (const std::exception & e) {
				Log::error() << e.what();
				throw std::runtime_error( "Failed to restore previous window settings after failing to switch to fullscreen mode" );
			}
			return false;
		}
		if (fullscreen)
			Log::info() << "Entered fullscreen mode";
		else
			Log::info() << "Entered windowed mode";
		return true;
	}
}

bool Window::resize( int newW, int newH ) {
	if (!created) {
		Log::warning() << "Unable to resize window: window not initialized";
		return false;
	}

	if (!fullscreen) {
		NSSize newSize = NSMakeSize( (float)newW, (float)newH );
		[window setContentSize:newSize];
		width = newW;
		height = newH;
		return true;
	} else
		// it appears that "fullscreen" is always your native resolution? check up on this
		// the way this should be done is have OpenGL's buffers be a different size than the resolution
		// todo later...
		return false;
}

bool Window::getQuit() const {
	return quit;
}

void Window::update() {
	if (!created) {
		Log::warning() << "Unable to update: window not initialized";
		return;
	}

	quit = false;
	eventData.clear();

	// update all the key/mouse inputs, etc.

	// set all pressed/released flags to false initially
	// we will detect these by comparing to the previous state

	NSPoint cursor = [window mouseLocationOutsideOfEventStream];
	cursor.y = height - cursor.y;
	InputEvent ev;
	ev.type = IET_MOUSE_POSITION;
	ev.mousePositionEvent.x = (int)cursor.x;
	ev.mousePositionEvent.y = (int)cursor.y;
	eventData.push_back( ev );

	NSEvent * event;
	do {
		event = [NSApp nextEventMatchingMask:NSAnyEventMask untilDate:[NSDate distantPast] inMode:NSDefaultRunLoopMode dequeue:YES];
		[NSApp sendEvent:event];
	} while (event != nil);
}

void Window::updateDisplay() {
	[window update];
	[window display];
}

const std::vector <WindowEventData> & Window::getEventData() const {
	return eventData;
}

#endif