/** @file Maths_Ray2Dec.h
 *  @brief Contains an implementation of a 2D ray class.
 */

#ifndef MATHS_RAY2DEC_DEFINED
#define MATHS_RAY2DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for 2D rays.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Ray2 {
public:
	Vector2 <T> origin;		/**< The ray origin.*/
	Vector2 <T> direction;	/**< The ray direction.*/

	/** @brief Constructs a ray at the origin with no direction.
	 */
	Ray2();

	/** @brief Constructs a ray with the given origin and direction.
	 */
	Ray2( const Vector2 <T> & orig, const Vector2 <T> & dir );

	/** @brief Constructs a ray by copying the one provided.
	 */
	Ray2( const Ray2 <T> & r );

	/** @brief The destructor.
	 */
	~Ray2();

	// operators

	/** @brief Sets the current value to the ray r.
	 */
	Ray2 <T> operator=( const Ray2 <T> & r );

	/** @brief Returns whether the current value is equal to the ray r.
	 */
	bool operator==( const Ray2 <T> & r ) const;

	/** @brief Returns whether the current value is not equal to the ray r.
	 */
	bool operator!=( const Ray2 <T> & r ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Ray2 <U> () const;

	// mutable

	/** @brief Sets the current value's components to (px, py).
	 */
	void set( const Vector2 <T> & orig, const Vector2 <T> & dir );
};

/** @brief Ray of type float.
 */
typedef Ray2 <float> Ray2f;

/** @brief Ray of type double.
 */
typedef Ray2 <double> Ray2d;

#endif