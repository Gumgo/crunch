#ifndef MATHS_VECTOR4DEF_DEFINED
#define MATHS_VECTOR4DEF_DEFINED

template <typename T> Vector4 <T>::Vector4()
	: x( 0 )
	, y( 0 )
	, z( 0 )
	, w( 0 ) {
}

template <typename T> Vector4 <T>::Vector4( T px, T py, T pz, T pw )
	: x( px )
	, y( py )
	, z( pz )
	, w( pw ) {
}

template <typename T> Vector4 <T>::Vector4( const Vector4 <T> & v )
	: x( v.x )
	, y ( v.y )
	, z( v.z )
	, w( v.w ) {
}

template <typename T> Vector4 <T>::Vector4( const Vector3 <T> & pxyz, T pw )
	: x( pxyz.x )
	, y( pxyz.y )
	, z( pxyz.z )
	, w( pw ) {
}

template <typename T> Vector4 <T>::Vector4( T px, const Vector3 <T> & pyzw )
	: x( px )
	, y( pyzw.x )
	, z( pyzw.y )
	, w( pyzw.z ) {
}

template <typename T> Vector4 <T>::Vector4( const Vector2 <T> & pxy, const Vector2 <T> & pzw )
	: x( pxy.x )
	, y( pxy.y )
	, z( pzw.x )
	, w( pzw.y ) {
}

template <typename T> Vector4 <T>::Vector4( const Vector2 <T> & pxy, T pz, T pw )
	: x( pxy.x )
	, y( pxy.y )
	, z( pz )
	, w( pw ) {
}

template <typename T> Vector4 <T>::Vector4( T px, const Vector2 <T> & pyz, T pw )
	: x( px )
	, y( pyz.x )
	, z( pyz.y )
	, w( pw ) {
}

template <typename T> Vector4 <T>::Vector4( T px, T py, const Vector2 <T> & pzw )
	: x( px )
	, y( py )
	, z( pzw.x )
	, w( pzw.y ) {
}

template <typename T> Vector4 <T>::~Vector4() {
}

template <typename T> Vector4 <T> Vector4 <T>::operator+( const Vector4 <T> & v ) const {
	return Vector4 <T>( x + v.x, y + v.y, z + v.z, w + v.w );
}

template <typename T> Vector4 <T> Vector4 <T>::operator+() const {
	return *this;
}

template <typename T> Vector4 <T> Vector4 <T>::operator-( const Vector4 <T> & v ) const {
	return Vector4 <T>( x - v.x, y - v.y, z - v.z, z - v.z );
}

template <typename T> Vector4 <T> Vector4 <T>::operator-() const {
	return Vector4 <T>( -x, -y, -z, -w );
}

template <typename T> Vector4 <T> Vector4 <T>::operator*( T r ) const {
	return Vector4 <T>( x*r, y*r, z*r, w*r );
}

template <typename T> Vector4 <T> operator*( T r, const Vector4 <T> & v ) {
	return v*r;
}

template <typename T> Vector4 <T> Vector4 <T>::operator/( T r ) const {
	return Vector4 <T>( x/r, y/r, z/r, w/r );
}

template <typename T> Vector4 <T> operator/( T r, const Vector4 <T> & v ) {
	return Vector4 <T>( r/v.x, r/v.y, r/v.z, r/v.w );
}

template <typename T> Vector4 <T> Vector4 <T>::operator*( const Vector4 <T> & v ) const {
	return Vector4 <T>( x*v.x, y*v.y, z*v.z, w*v.w );
}

template <typename T> Vector4 <T> Vector4 <T>::operator/( const Vector4 <T> & v ) const {
	return Vector4 <T>( x/v.x, y/v.y, z/v.z, w/v.w );
}

template <typename T> Vector4 <T> Vector4 <T>::operator=( const Vector4 <T> & v ) {
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;
	return *this;
}

template <typename T> Vector4 <T> Vector4 <T>::operator+=( const Vector4 <T> & v ) {
	x += v.x;
	y += v.y;
	z += v.z;
	w += v.w;
	return *this;
}

template <typename T> Vector4 <T> Vector4 <T>::operator-=( const Vector4 <T> & v ) {
	x -= v.x;
	y -= v.y;
	z -= v.z;
	w -= v.w;
	return *this;
}

template <typename T> Vector4 <T> Vector4 <T>::operator*=( T r ) {
	x *= r;
	y *= r;
	z *= r;
	w *= r;
	return *this;
}

template <typename T> Vector4 <T> Vector4 <T>::operator/=( T r ) {
	x /= r;
	y /= r;
	z /= r;
	w /= r;
	return *this;
}

template <typename T> Vector4 <T> Vector4 <T>::operator*=( const Vector4 <T> & v ) {
	x *= v.x;
	y *= v.y;
	z *= v.z;
	w *= v.w;
	return *this;
}

template <typename T> Vector4 <T> Vector4 <T>::operator/=( const Vector4 <T> & v ) {
	x /= v.x;
	y /= v.y;
	z /= v.z;
	w /= v.w;
	return *this;
}

template <typename T> bool Vector4 <T>::operator==( const Vector4 <T> & v ) const {
	return (x == v.x && y == v.y && z == v.z && w == v.w);
}

template <typename T> bool Vector4 <T>::operator!=( const Vector4 <T> & v ) const {
	return (x != v.x || y != v.y || z != v.z || w != v.w);
}

template <typename T> template <typename U> T & Vector4 <T>::operator[]( U i ) {
	return *(&x+i);
}

template <typename T> template <typename U> T Vector4 <T>::operator[]( U i ) const {
	return *(&x+i);
}

template <typename T> template <typename U> Vector4 <T>::operator Vector4 <U> () const {
	return Vector4 <U>( (U)x, (U)y, (U)z, (U)w );
}

template <typename T> void Vector4 <T>::set( T px, T py, T pz, T pw ) {
	x = px;
	y = py;
	z = pz;
	w = pw;
}

template <typename T> void Vector4 <T>::normalize() {
	setMagnitude( 1 );
}

template <typename T> void Vector4 <T>::setMagnitude( T m ) {
	T mag = magnitudeSquared();
	if (mag == 0)
		return;
	mag = m/sqrt( mag );
	x *= mag;
	y *= mag;
	z *= mag;
	w *= mag;
}

template <typename T> Vector4 <T> Vector4 <T>::normalized() const {
	T mag = magnitudeSquared();
	if (mag == 0)
		return Vector4 <T>( 0, 0, 0, 0 );
	mag = 1/sqrt( mag );
	return Vector4 <T>( x*mag, y*mag, z*mag, w*mag );
}

template <typename T> T Vector4 <T>::magnitude() const {
	return sqrt( x*x + y*y + z*z + w*w );
}

template <typename T> T Vector4 <T>::magnitudeSquared() const {
	return (x*x + y*y + z*z + w*w);
}

template <typename T> T Vector4 <T>::dot( const Vector4 <T> & v ) const {
	return (x*v.x + y*v.y + z*v.z + w*v.w);
}

template <typename T> bool Vector4 <T>::isZero() const {
	return (x == 0 && y == 0 && z == 0 && w == 0);
}

template <typename T> Vector4 <T> Vector4 <T>::xxxx() const { return Vector4 <T>( x, x, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xxxy() const { return Vector4 <T>( x, x, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xxxz() const { return Vector4 <T>( x, x, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xxxw() const { return Vector4 <T>( x, x, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xxyx() const { return Vector4 <T>( x, x, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xxyy() const { return Vector4 <T>( x, x, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xxyz() const { return Vector4 <T>( x, x, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xxyw() const { return Vector4 <T>( x, x, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xxzx() const { return Vector4 <T>( x, x, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xxzy() const { return Vector4 <T>( x, x, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xxzz() const { return Vector4 <T>( x, x, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xxzw() const { return Vector4 <T>( x, x, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xxwx() const { return Vector4 <T>( x, x, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xxwy() const { return Vector4 <T>( x, x, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xxwz() const { return Vector4 <T>( x, x, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xxww() const { return Vector4 <T>( x, x, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xyxx() const { return Vector4 <T>( x, y, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xyxy() const { return Vector4 <T>( x, y, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xyxz() const { return Vector4 <T>( x, y, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xyxw() const { return Vector4 <T>( x, y, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xyyx() const { return Vector4 <T>( x, y, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xyyy() const { return Vector4 <T>( x, y, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xyyz() const { return Vector4 <T>( x, y, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xyyw() const { return Vector4 <T>( x, y, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xyzx() const { return Vector4 <T>( x, y, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xyzy() const { return Vector4 <T>( x, y, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xyzz() const { return Vector4 <T>( x, y, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xyzw() const { return Vector4 <T>( x, y, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xywx() const { return Vector4 <T>( x, y, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xywy() const { return Vector4 <T>( x, y, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xywz() const { return Vector4 <T>( x, y, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xyww() const { return Vector4 <T>( x, y, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xzxx() const { return Vector4 <T>( x, z, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xzxy() const { return Vector4 <T>( x, z, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xzxz() const { return Vector4 <T>( x, z, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xzxw() const { return Vector4 <T>( x, z, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xzyx() const { return Vector4 <T>( x, z, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xzyy() const { return Vector4 <T>( x, z, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xzyz() const { return Vector4 <T>( x, z, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xzyw() const { return Vector4 <T>( x, z, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xzzx() const { return Vector4 <T>( x, z, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xzzy() const { return Vector4 <T>( x, z, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xzzz() const { return Vector4 <T>( x, z, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xzzw() const { return Vector4 <T>( x, z, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xzwx() const { return Vector4 <T>( x, z, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xzwy() const { return Vector4 <T>( x, z, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xzwz() const { return Vector4 <T>( x, z, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xzww() const { return Vector4 <T>( x, z, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xwxx() const { return Vector4 <T>( x, w, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xwxy() const { return Vector4 <T>( x, w, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xwxz() const { return Vector4 <T>( x, w, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xwxw() const { return Vector4 <T>( x, w, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xwyx() const { return Vector4 <T>( x, w, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xwyy() const { return Vector4 <T>( x, w, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xwyz() const { return Vector4 <T>( x, w, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xwyw() const { return Vector4 <T>( x, w, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xwzx() const { return Vector4 <T>( x, w, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xwzy() const { return Vector4 <T>( x, w, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xwzz() const { return Vector4 <T>( x, w, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xwzw() const { return Vector4 <T>( x, w, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::xwwx() const { return Vector4 <T>( x, w, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::xwwy() const { return Vector4 <T>( x, w, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::xwwz() const { return Vector4 <T>( x, w, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::xwww() const { return Vector4 <T>( x, w, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yxxx() const { return Vector4 <T>( y, x, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yxxy() const { return Vector4 <T>( y, x, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yxxz() const { return Vector4 <T>( y, x, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yxxw() const { return Vector4 <T>( y, x, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yxyx() const { return Vector4 <T>( y, x, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yxyy() const { return Vector4 <T>( y, x, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yxyz() const { return Vector4 <T>( y, x, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yxyw() const { return Vector4 <T>( y, x, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yxzx() const { return Vector4 <T>( y, x, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yxzy() const { return Vector4 <T>( y, x, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yxzz() const { return Vector4 <T>( y, x, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yxzw() const { return Vector4 <T>( y, x, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yxwx() const { return Vector4 <T>( y, x, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yxwy() const { return Vector4 <T>( y, x, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yxwz() const { return Vector4 <T>( y, x, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yxww() const { return Vector4 <T>( y, x, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yyxx() const { return Vector4 <T>( y, y, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yyxy() const { return Vector4 <T>( y, y, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yyxz() const { return Vector4 <T>( y, y, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yyxw() const { return Vector4 <T>( y, y, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yyyx() const { return Vector4 <T>( y, y, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yyyy() const { return Vector4 <T>( y, y, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yyyz() const { return Vector4 <T>( y, y, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yyyw() const { return Vector4 <T>( y, y, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yyzx() const { return Vector4 <T>( y, y, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yyzy() const { return Vector4 <T>( y, y, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yyzz() const { return Vector4 <T>( y, y, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yyzw() const { return Vector4 <T>( y, y, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yywx() const { return Vector4 <T>( y, y, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yywy() const { return Vector4 <T>( y, y, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yywz() const { return Vector4 <T>( y, y, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yyww() const { return Vector4 <T>( y, y, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yzxx() const { return Vector4 <T>( y, z, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yzxy() const { return Vector4 <T>( y, z, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yzxz() const { return Vector4 <T>( y, z, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yzxw() const { return Vector4 <T>( y, z, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yzyx() const { return Vector4 <T>( y, z, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yzyy() const { return Vector4 <T>( y, z, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yzyz() const { return Vector4 <T>( y, z, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yzyw() const { return Vector4 <T>( y, z, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yzzx() const { return Vector4 <T>( y, z, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yzzy() const { return Vector4 <T>( y, z, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yzzz() const { return Vector4 <T>( y, z, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yzzw() const { return Vector4 <T>( y, z, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::yzwx() const { return Vector4 <T>( y, z, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::yzwy() const { return Vector4 <T>( y, z, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::yzwz() const { return Vector4 <T>( y, z, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::yzww() const { return Vector4 <T>( y, z, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::ywxx() const { return Vector4 <T>( y, w, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::ywxy() const { return Vector4 <T>( y, w, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::ywxz() const { return Vector4 <T>( y, w, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::ywxw() const { return Vector4 <T>( y, w, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::ywyx() const { return Vector4 <T>( y, w, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::ywyy() const { return Vector4 <T>( y, w, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::ywyz() const { return Vector4 <T>( y, w, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::ywyw() const { return Vector4 <T>( y, w, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::ywzx() const { return Vector4 <T>( y, w, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::ywzy() const { return Vector4 <T>( y, w, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::ywzz() const { return Vector4 <T>( y, w, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::ywzw() const { return Vector4 <T>( y, w, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::ywwx() const { return Vector4 <T>( y, w, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::ywwy() const { return Vector4 <T>( y, w, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::ywwz() const { return Vector4 <T>( y, w, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::ywww() const { return Vector4 <T>( y, w, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zxxx() const { return Vector4 <T>( z, x, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zxxy() const { return Vector4 <T>( z, x, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zxxz() const { return Vector4 <T>( z, x, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zxxw() const { return Vector4 <T>( z, x, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zxyx() const { return Vector4 <T>( z, x, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zxyy() const { return Vector4 <T>( z, x, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zxyz() const { return Vector4 <T>( z, x, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zxyw() const { return Vector4 <T>( z, x, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zxzx() const { return Vector4 <T>( z, x, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zxzy() const { return Vector4 <T>( z, x, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zxzz() const { return Vector4 <T>( z, x, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zxzw() const { return Vector4 <T>( z, x, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zxwx() const { return Vector4 <T>( z, x, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zxwy() const { return Vector4 <T>( z, x, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zxwz() const { return Vector4 <T>( z, x, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zxww() const { return Vector4 <T>( z, x, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zyxx() const { return Vector4 <T>( z, y, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zyxy() const { return Vector4 <T>( z, y, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zyxz() const { return Vector4 <T>( z, y, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zyxw() const { return Vector4 <T>( z, y, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zyyx() const { return Vector4 <T>( z, y, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zyyy() const { return Vector4 <T>( z, y, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zyyz() const { return Vector4 <T>( z, y, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zyyw() const { return Vector4 <T>( z, y, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zyzx() const { return Vector4 <T>( z, y, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zyzy() const { return Vector4 <T>( z, y, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zyzz() const { return Vector4 <T>( z, y, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zyzw() const { return Vector4 <T>( z, y, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zywx() const { return Vector4 <T>( z, y, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zywy() const { return Vector4 <T>( z, y, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zywz() const { return Vector4 <T>( z, y, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zyww() const { return Vector4 <T>( z, y, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zzxx() const { return Vector4 <T>( z, z, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zzxy() const { return Vector4 <T>( z, z, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zzxz() const { return Vector4 <T>( z, z, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zzxw() const { return Vector4 <T>( z, z, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zzyx() const { return Vector4 <T>( z, z, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zzyy() const { return Vector4 <T>( z, z, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zzyz() const { return Vector4 <T>( z, z, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zzyw() const { return Vector4 <T>( z, z, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zzzx() const { return Vector4 <T>( z, z, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zzzy() const { return Vector4 <T>( z, z, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zzzz() const { return Vector4 <T>( z, z, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zzzw() const { return Vector4 <T>( z, z, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zzwx() const { return Vector4 <T>( z, z, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zzwy() const { return Vector4 <T>( z, z, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zzwz() const { return Vector4 <T>( z, z, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zzww() const { return Vector4 <T>( z, z, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zwxx() const { return Vector4 <T>( z, w, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zwxy() const { return Vector4 <T>( z, w, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zwxz() const { return Vector4 <T>( z, w, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zwxw() const { return Vector4 <T>( z, w, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zwyx() const { return Vector4 <T>( z, w, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zwyy() const { return Vector4 <T>( z, w, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zwyz() const { return Vector4 <T>( z, w, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zwyw() const { return Vector4 <T>( z, w, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zwzx() const { return Vector4 <T>( z, w, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zwzy() const { return Vector4 <T>( z, w, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zwzz() const { return Vector4 <T>( z, w, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zwzw() const { return Vector4 <T>( z, w, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::zwwx() const { return Vector4 <T>( z, w, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::zwwy() const { return Vector4 <T>( z, w, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::zwwz() const { return Vector4 <T>( z, w, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::zwww() const { return Vector4 <T>( z, w, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wxxx() const { return Vector4 <T>( w, x, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wxxy() const { return Vector4 <T>( w, x, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wxxz() const { return Vector4 <T>( w, x, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wxxw() const { return Vector4 <T>( w, x, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wxyx() const { return Vector4 <T>( w, x, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wxyy() const { return Vector4 <T>( w, x, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wxyz() const { return Vector4 <T>( w, x, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wxyw() const { return Vector4 <T>( w, x, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wxzx() const { return Vector4 <T>( w, x, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wxzy() const { return Vector4 <T>( w, x, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wxzz() const { return Vector4 <T>( w, x, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wxzw() const { return Vector4 <T>( w, x, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wxwx() const { return Vector4 <T>( w, x, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wxwy() const { return Vector4 <T>( w, x, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wxwz() const { return Vector4 <T>( w, x, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wxww() const { return Vector4 <T>( w, x, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wyxx() const { return Vector4 <T>( w, y, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wyxy() const { return Vector4 <T>( w, y, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wyxz() const { return Vector4 <T>( w, y, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wyxw() const { return Vector4 <T>( w, y, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wyyx() const { return Vector4 <T>( w, y, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wyyy() const { return Vector4 <T>( w, y, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wyyz() const { return Vector4 <T>( w, y, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wyyw() const { return Vector4 <T>( w, y, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wyzx() const { return Vector4 <T>( w, y, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wyzy() const { return Vector4 <T>( w, y, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wyzz() const { return Vector4 <T>( w, y, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wyzw() const { return Vector4 <T>( w, y, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wywx() const { return Vector4 <T>( w, y, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wywy() const { return Vector4 <T>( w, y, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wywz() const { return Vector4 <T>( w, y, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wyww() const { return Vector4 <T>( w, y, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wzxx() const { return Vector4 <T>( w, z, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wzxy() const { return Vector4 <T>( w, z, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wzxz() const { return Vector4 <T>( w, z, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wzxw() const { return Vector4 <T>( w, z, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wzyx() const { return Vector4 <T>( w, z, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wzyy() const { return Vector4 <T>( w, z, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wzyz() const { return Vector4 <T>( w, z, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wzyw() const { return Vector4 <T>( w, z, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wzzx() const { return Vector4 <T>( w, z, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wzzy() const { return Vector4 <T>( w, z, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wzzz() const { return Vector4 <T>( w, z, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wzzw() const { return Vector4 <T>( w, z, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wzwx() const { return Vector4 <T>( w, z, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wzwy() const { return Vector4 <T>( w, z, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wzwz() const { return Vector4 <T>( w, z, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wzww() const { return Vector4 <T>( w, z, w, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wwxx() const { return Vector4 <T>( w, w, x, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wwxy() const { return Vector4 <T>( w, w, x, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wwxz() const { return Vector4 <T>( w, w, x, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wwxw() const { return Vector4 <T>( w, w, x, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wwyx() const { return Vector4 <T>( w, w, y, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wwyy() const { return Vector4 <T>( w, w, y, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wwyz() const { return Vector4 <T>( w, w, y, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wwyw() const { return Vector4 <T>( w, w, y, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wwzx() const { return Vector4 <T>( w, w, z, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wwzy() const { return Vector4 <T>( w, w, z, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wwzz() const { return Vector4 <T>( w, w, z, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wwzw() const { return Vector4 <T>( w, w, z, w ); }
template <typename T> Vector4 <T> Vector4 <T>::wwwx() const { return Vector4 <T>( w, w, w, x ); }
template <typename T> Vector4 <T> Vector4 <T>::wwwy() const { return Vector4 <T>( w, w, w, y ); }
template <typename T> Vector4 <T> Vector4 <T>::wwwz() const { return Vector4 <T>( w, w, w, z ); }
template <typename T> Vector4 <T> Vector4 <T>::wwww() const { return Vector4 <T>( w, w, w, w ); }
template <typename T> Vector3 <T> Vector4 <T>::xxx() const { return Vector3 <T>( x, x, x ); }
template <typename T> Vector3 <T> Vector4 <T>::xxy() const { return Vector3 <T>( x, x, y ); }
template <typename T> Vector3 <T> Vector4 <T>::xxz() const { return Vector3 <T>( x, x, z ); }
template <typename T> Vector3 <T> Vector4 <T>::xxw() const { return Vector3 <T>( x, x, w ); }
template <typename T> Vector3 <T> Vector4 <T>::xyx() const { return Vector3 <T>( x, y, x ); }
template <typename T> Vector3 <T> Vector4 <T>::xyy() const { return Vector3 <T>( x, y, y ); }
template <typename T> Vector3 <T> Vector4 <T>::xyz() const { return Vector3 <T>( x, y, z ); }
template <typename T> Vector3 <T> Vector4 <T>::xyw() const { return Vector3 <T>( x, y, w ); }
template <typename T> Vector3 <T> Vector4 <T>::xzx() const { return Vector3 <T>( x, z, x ); }
template <typename T> Vector3 <T> Vector4 <T>::xzy() const { return Vector3 <T>( x, z, y ); }
template <typename T> Vector3 <T> Vector4 <T>::xzz() const { return Vector3 <T>( x, z, z ); }
template <typename T> Vector3 <T> Vector4 <T>::xzw() const { return Vector3 <T>( x, z, w ); }
template <typename T> Vector3 <T> Vector4 <T>::xwx() const { return Vector3 <T>( x, w, x ); }
template <typename T> Vector3 <T> Vector4 <T>::xwy() const { return Vector3 <T>( x, w, y ); }
template <typename T> Vector3 <T> Vector4 <T>::xwz() const { return Vector3 <T>( x, w, z ); }
template <typename T> Vector3 <T> Vector4 <T>::xww() const { return Vector3 <T>( x, w, w ); }
template <typename T> Vector3 <T> Vector4 <T>::yxx() const { return Vector3 <T>( y, x, x ); }
template <typename T> Vector3 <T> Vector4 <T>::yxy() const { return Vector3 <T>( y, x, y ); }
template <typename T> Vector3 <T> Vector4 <T>::yxz() const { return Vector3 <T>( y, x, z ); }
template <typename T> Vector3 <T> Vector4 <T>::yxw() const { return Vector3 <T>( y, x, w ); }
template <typename T> Vector3 <T> Vector4 <T>::yyx() const { return Vector3 <T>( y, y, x ); }
template <typename T> Vector3 <T> Vector4 <T>::yyy() const { return Vector3 <T>( y, y, y ); }
template <typename T> Vector3 <T> Vector4 <T>::yyz() const { return Vector3 <T>( y, y, z ); }
template <typename T> Vector3 <T> Vector4 <T>::yyw() const { return Vector3 <T>( y, y, w ); }
template <typename T> Vector3 <T> Vector4 <T>::yzx() const { return Vector3 <T>( y, z, x ); }
template <typename T> Vector3 <T> Vector4 <T>::yzy() const { return Vector3 <T>( y, z, y ); }
template <typename T> Vector3 <T> Vector4 <T>::yzz() const { return Vector3 <T>( y, z, z ); }
template <typename T> Vector3 <T> Vector4 <T>::yzw() const { return Vector3 <T>( y, z, w ); }
template <typename T> Vector3 <T> Vector4 <T>::ywx() const { return Vector3 <T>( y, w, x ); }
template <typename T> Vector3 <T> Vector4 <T>::ywy() const { return Vector3 <T>( y, w, y ); }
template <typename T> Vector3 <T> Vector4 <T>::ywz() const { return Vector3 <T>( y, w, z ); }
template <typename T> Vector3 <T> Vector4 <T>::yww() const { return Vector3 <T>( y, w, w ); }
template <typename T> Vector3 <T> Vector4 <T>::zxx() const { return Vector3 <T>( z, x, x ); }
template <typename T> Vector3 <T> Vector4 <T>::zxy() const { return Vector3 <T>( z, x, y ); }
template <typename T> Vector3 <T> Vector4 <T>::zxz() const { return Vector3 <T>( z, x, z ); }
template <typename T> Vector3 <T> Vector4 <T>::zxw() const { return Vector3 <T>( z, x, w ); }
template <typename T> Vector3 <T> Vector4 <T>::zyx() const { return Vector3 <T>( z, y, x ); }
template <typename T> Vector3 <T> Vector4 <T>::zyy() const { return Vector3 <T>( z, y, y ); }
template <typename T> Vector3 <T> Vector4 <T>::zyz() const { return Vector3 <T>( z, y, z ); }
template <typename T> Vector3 <T> Vector4 <T>::zyw() const { return Vector3 <T>( z, y, w ); }
template <typename T> Vector3 <T> Vector4 <T>::zzx() const { return Vector3 <T>( z, z, x ); }
template <typename T> Vector3 <T> Vector4 <T>::zzy() const { return Vector3 <T>( z, z, y ); }
template <typename T> Vector3 <T> Vector4 <T>::zzz() const { return Vector3 <T>( z, z, z ); }
template <typename T> Vector3 <T> Vector4 <T>::zzw() const { return Vector3 <T>( z, z, w ); }
template <typename T> Vector3 <T> Vector4 <T>::zwx() const { return Vector3 <T>( z, w, x ); }
template <typename T> Vector3 <T> Vector4 <T>::zwy() const { return Vector3 <T>( z, w, y ); }
template <typename T> Vector3 <T> Vector4 <T>::zwz() const { return Vector3 <T>( z, w, z ); }
template <typename T> Vector3 <T> Vector4 <T>::zww() const { return Vector3 <T>( z, w, w ); }
template <typename T> Vector3 <T> Vector4 <T>::wxx() const { return Vector3 <T>( w, x, x ); }
template <typename T> Vector3 <T> Vector4 <T>::wxy() const { return Vector3 <T>( w, x, y ); }
template <typename T> Vector3 <T> Vector4 <T>::wxz() const { return Vector3 <T>( w, x, z ); }
template <typename T> Vector3 <T> Vector4 <T>::wxw() const { return Vector3 <T>( w, x, w ); }
template <typename T> Vector3 <T> Vector4 <T>::wyx() const { return Vector3 <T>( w, y, x ); }
template <typename T> Vector3 <T> Vector4 <T>::wyy() const { return Vector3 <T>( w, y, y ); }
template <typename T> Vector3 <T> Vector4 <T>::wyz() const { return Vector3 <T>( w, y, z ); }
template <typename T> Vector3 <T> Vector4 <T>::wyw() const { return Vector3 <T>( w, y, w ); }
template <typename T> Vector3 <T> Vector4 <T>::wzx() const { return Vector3 <T>( w, z, x ); }
template <typename T> Vector3 <T> Vector4 <T>::wzy() const { return Vector3 <T>( w, z, y ); }
template <typename T> Vector3 <T> Vector4 <T>::wzz() const { return Vector3 <T>( w, z, z ); }
template <typename T> Vector3 <T> Vector4 <T>::wzw() const { return Vector3 <T>( w, z, w ); }
template <typename T> Vector3 <T> Vector4 <T>::wwx() const { return Vector3 <T>( w, w, x ); }
template <typename T> Vector3 <T> Vector4 <T>::wwy() const { return Vector3 <T>( w, w, y ); }
template <typename T> Vector3 <T> Vector4 <T>::wwz() const { return Vector3 <T>( w, w, z ); }
template <typename T> Vector3 <T> Vector4 <T>::www() const { return Vector3 <T>( w, w, w ); }
template <typename T> Vector2 <T> Vector4 <T>::xx() const { return Vector2 <T>( x, x ); }
template <typename T> Vector2 <T> Vector4 <T>::xy() const { return Vector2 <T>( x, y ); }
template <typename T> Vector2 <T> Vector4 <T>::xz() const { return Vector2 <T>( x, z ); }
template <typename T> Vector2 <T> Vector4 <T>::xw() const { return Vector2 <T>( x, w ); }
template <typename T> Vector2 <T> Vector4 <T>::yx() const { return Vector2 <T>( y, x ); }
template <typename T> Vector2 <T> Vector4 <T>::yy() const { return Vector2 <T>( y, y ); }
template <typename T> Vector2 <T> Vector4 <T>::yz() const { return Vector2 <T>( y, z ); }
template <typename T> Vector2 <T> Vector4 <T>::yw() const { return Vector2 <T>( y, w ); }
template <typename T> Vector2 <T> Vector4 <T>::zx() const { return Vector2 <T>( z, x ); }
template <typename T> Vector2 <T> Vector4 <T>::zy() const { return Vector2 <T>( z, y ); }
template <typename T> Vector2 <T> Vector4 <T>::zz() const { return Vector2 <T>( z, z ); }
template <typename T> Vector2 <T> Vector4 <T>::zw() const { return Vector2 <T>( z, w ); }
template <typename T> Vector2 <T> Vector4 <T>::wx() const { return Vector2 <T>( w, x ); }
template <typename T> Vector2 <T> Vector4 <T>::wy() const { return Vector2 <T>( w, y ); }
template <typename T> Vector2 <T> Vector4 <T>::wz() const { return Vector2 <T>( w, z ); }
template <typename T> Vector2 <T> Vector4 <T>::ww() const { return Vector2 <T>( w, w ); }

template <typename T> Vector4 <T> vecMax( const Vector4 <T> & v1, const Vector4 <T> & v2 ) {
	return Vector4 <T>( v1.x > v2.x ? v1.x : v2.x,
						v1.y > v2.y ? v1.y : v2.y,
						v1.z > v2.z ? v1.z : v2.z,
						v1.w > v2.w ? v1.w : v2.w );
}

template <typename T> Vector4 <T> vecMin( const Vector4 <T> & v1, const Vector4 <T> & v2 ) {
	return Vector4 <T>( v1.x < v2.x ? v1.x : v2.x,
						v1.y < v2.y ? v1.y : v2.y,
						v1.z < v2.z ? v1.z : v2.z,
						v1.w < v2.w ? v1.w : v2.w );
}

#endif