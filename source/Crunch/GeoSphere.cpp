#include "GeoSphere.h"

GeoSphere::Edge::Edge( size_t v0, size_t v1 )
	: vertices( v0, v1 ) {
	split = false;
}

size_t GeoSphere::Edge::getSplitEdge( size_t vertex ) {
	if (vertex == vertices[0])
		return splitEdges[0];
	else if (vertex == vertices[1])
		return splitEdges[1];
	else
		return 0;
}

GeoSphere::Triangle::Triangle( size_t v0, size_t v1, size_t v2, size_t e0, size_t e1, size_t e2 )
	: vertices( v0, v1, v2 )
	, edges( e0, e1, e2 ) {
}

void GeoSphere::splitEdge( Edge & e ) {
	if (e.split)
		return;
	e.splitVertex = vertices.size();
	vertices.push_back( (0.5f * (vertices[e.vertices[0]] + vertices[e.vertices[1]])).normalized() );
	e.splitEdges.set( newEdges.size(), newEdges.size()+1 );
	newEdges.push_back( Edge( e.vertices[0], e.splitVertex ) );
	newEdges.push_back( Edge( e.splitVertex, e.vertices[1] ) );
	e.split = true;
}

void GeoSphere::subdivideTriangle( const Triangle & t ) {
	//    0
	//    /\
	//  0/  \2
	//  /____\
	// 1  1   2
	size_t edge0 = t.edges[0];
	size_t edge1 = t.edges[1];
	size_t edge2 = t.edges[2];
	size_t vert0 = t.vertices[0];
	size_t vert1 = t.vertices[1];
	size_t vert2 = t.vertices[2];
	splitEdge( edges[edge0] );
	splitEdge( edges[edge1] );
	splitEdge( edges[edge2] );
	size_t edge01 = newEdges.size();
	size_t edge12 = newEdges.size()+1;
	size_t edge20 = newEdges.size()+2;
	newEdges.push_back( Edge( edges[edge0].splitVertex, edges[edge1].splitVertex ) );
	newEdges.push_back( Edge( edges[edge1].splitVertex, edges[edge2].splitVertex ) );
	newEdges.push_back( Edge( edges[edge2].splitVertex, edges[edge0].splitVertex ) );
	
	// important: we push the "center" triangle first
	// this is so a center-most triangle is guaranteed to be at index 0 of the final object
	newTriangles.push_back( Triangle(
		edges[edge0].splitVertex, edges[edge1].splitVertex, edges[edge2].splitVertex,
		edge01, edge12, edge20 ) );
	
	newTriangles.push_back( Triangle(
		vert0, edges[edge0].splitVertex, edges[edge2].splitVertex,
		edges[edge0].getSplitEdge( vert0 ), edge20, edges[edge2].getSplitEdge( vert0 ) ) );
	
	newTriangles.push_back( Triangle(
		edges[edge0].splitVertex, vert1, edges[edge1].splitVertex,
		edges[edge0].getSplitEdge( vert1 ), edges[edge1].getSplitEdge( vert1 ), edge01 ) );
	
	newTriangles.push_back( Triangle(
		edges[edge2].splitVertex, edges[edge1].splitVertex, vert2,
		edge12, edges[edge1].getSplitEdge( vert2 ), edges[edge2].getSplitEdge( vert2 ) ) );
}

void GeoSphere::subdivide() {
	for (size_t i = 0; i < triangles.size(); ++i)
		subdivideTriangle( triangles[i] );
	edges.swap( newEdges );
	triangles.swap( newTriangles );
	newEdges.clear();
	newTriangles.clear();
}

float GeoSphere::computeInscriptionRadiusMultiplier() const {
	// all triangles have 3 points with norm 1
	// this means for each triangle, the point on the plane
	// with the smallest norm is the centroid (average) of
	// the three vertices. Thus, since all vertices have
	// norm 1, the largest triangle will have a centroid
	// with mean closest to 0. Each time we subdivide a triangle,
	// we get 3 triangles with 2 points "pushed out" (the surrounding
	// triangles) and one triangle with all 3 points "pushed out"
	// (the center triangle). Since pushing out a point makes the
	// triangle bigger, the biggest triangle must be the
	// center-most triangle - i.e. take the path of the center
	// triangle in each subdivision to get the biggest triangle.
	// In the subdivideTriangle() method, we ensured the center
	// triangle is always pushed to the new triangle array first -
	// thus, one of the center triangles must have index 0. Therefore
	// we compute the centroid of the triangle with index 0
	// and take its norm. If we divide by this value, "expand" the
	// sphere to contain a given radius rather than be contained by
	// a given radius. However, since division is slower, we'd rather
	// multiply, so we return the reciprocal.
	// 1/3 times the sum of the three vertices
	Vector3f centroid = boost::math::constants::third <float>() *
		(vertices[triangles[0].vertices[0]] + vertices[triangles[0].vertices[1]] + vertices[triangles[0].vertices[2]]);
	return 1.0f / centroid.magnitude();
}

GeoSphere::GeoSphere() {
}

void GeoSphere::generate( size_t subdivisions ) {
	static const float PHI = 1.618033988749894848204586834365638f;

	static const Vector3f initialVertices[] = {
		Vector3f( -1.0f, 0.0f, PHI ).normalized(),	// 0
		Vector3f( 1.0f, 0.0f, PHI ).normalized(),	// 1
		Vector3f( 0.0f, PHI, 1.0f ).normalized(),	// 2
		Vector3f( -PHI, 1.0f, 0.0f ).normalized(),	// 3
		Vector3f( -PHI, -1.0f, 0.0f ).normalized(),	// 4
		Vector3f( 0.0f, -PHI, 1.0f ).normalized(),	// 5
		Vector3f( PHI, -1.0f, 0.0f ).normalized(),	// 6
		Vector3f( PHI, 1.0f, 0.0f ).normalized(),	// 7
		Vector3f( 0.0f, PHI, -1.0f ).normalized(),	// 8
		Vector3f( -1.0f, 0.0f, -PHI ).normalized(),	// 9
		Vector3f( 0.0f, -PHI, -1.0f ).normalized(),	// 10
		Vector3f( 1.0f, 0.0f, -PHI ).normalized()	// 11
	};

	static const Edge initialEdges[] = {
		Edge( 0, 1 ),	// 0
		Edge( 0, 2 ),	// 1
		Edge( 0, 3 ),	// 2
		Edge( 0, 4 ),	// 3
		Edge( 0, 5 ),	// 4
		Edge( 1, 2 ),	// 5
		Edge( 2, 3 ),	// 6
		Edge( 3, 4 ),	// 7
		Edge( 4, 5 ),	// 8
		Edge( 5, 1 ),	// 9
		Edge( 5, 6 ),	// 10
		Edge( 6, 1 ),	// 11
		Edge( 1, 7 ),	// 12
		Edge( 7, 2 ),	// 13
		Edge( 2, 8 ),	// 14
		Edge( 8, 3 ),	// 15
		Edge( 3, 9 ),	// 16
		Edge( 9, 4 ),	// 17
		Edge( 4, 10 ),	// 18
		Edge( 10, 5 ),	// 19
		Edge( 10, 6 ),	// 20
		Edge( 6, 7 ),	// 21
		Edge( 7, 8 ),	// 22
		Edge( 8, 9 ),	// 23
		Edge( 9, 10 ),	// 24
		Edge( 10, 11 ),	// 25
		Edge( 6, 11 ),	// 26
		Edge( 7, 11 ),	// 27
		Edge( 8, 11 ),	// 28
		Edge( 9, 11 )	// 29
	};

	static const Triangle initialTriangles[] = {
		Triangle( 0, 1, 2, 0, 5, 1 ),
		Triangle( 0, 2, 3, 1, 6, 2 ),
		Triangle( 0, 3, 4, 2, 7, 3 ),
		Triangle( 0, 4, 5, 3, 8, 4 ),
		Triangle( 0, 5, 1, 4, 9, 0 ),
		Triangle( 1, 6, 7, 11, 21, 12 ),
		Triangle( 1, 7, 2, 12, 13, 5 ),
		Triangle( 2, 7, 8, 13, 22, 14 ),
		Triangle( 2, 8, 3, 14, 15, 6 ),
		Triangle( 3, 8, 9, 15, 23, 16 ),
		Triangle( 3, 9, 4, 16, 17, 7 ),
		Triangle( 4, 9, 10, 17, 24, 18 ),
		Triangle( 4, 10, 5, 18, 19, 8 ),
		Triangle( 5, 10, 6, 19, 20, 10 ),
		Triangle( 5, 6, 1, 10, 11, 9 ),
		Triangle( 6, 11, 7, 26, 27, 21 ),
		Triangle( 7, 11, 8, 27, 28, 22 ),
		Triangle( 8, 11, 9, 28, 29, 23 ),
		Triangle( 9, 11, 10, 29, 25, 24 ),
		Triangle( 10, 11, 6, 25, 26, 20 )
	};

	vertices.clear();
	edges.clear();
	triangles.clear();

	size_t vertexCount = arraySize( initialVertices );
	size_t edgeCount = arraySize( initialEdges );
	size_t triangleCount = arraySize( initialTriangles );

	// reserve space
	for (size_t i = 0; i < subdivisions; ++i) {
		vertexCount += edgeCount;
		edgeCount = edgeCount*2 + triangleCount*3;
		triangleCount *= 4;
	}

	vertices.reserve( vertexCount );
	edges.reserve( edgeCount );
	newEdges.reserve( edgeCount );
	triangles.reserve( triangleCount );
	newTriangles.reserve( triangleCount );

	vertices.assign( initialVertices, initialVertices + arraySize( initialVertices ) );
	edges.assign( initialEdges, initialEdges + arraySize( initialEdges ) );
	triangles.assign( initialTriangles, initialTriangles + arraySize( initialTriangles ) );

	for (size_t i = 0; i < subdivisions; ++i)
		subdivide();
	inscriptionRadiusMultiplier = computeInscriptionRadiusMultiplier();

	// now we create the array of indices
	indices.reserve( triangles.size() * 3 );
	for (size_t i = 0; i < triangles.size(); ++i) {
		indices.push_back( (ushort)triangles[i].vertices[0] );
		indices.push_back( (ushort)triangles[i].vertices[1] );
		indices.push_back( (ushort)triangles[i].vertices[2] );
	}

	// clear out all the extra memory
	edges.clear();
	edges.shrink_to_fit();
	newEdges.clear();
	newEdges.shrink_to_fit();
	triangles.clear();
	triangles.shrink_to_fit();
}

const std::vector <Vector3f> & GeoSphere::getVertices() const {
	return vertices;
}

const std::vector <ushort> & GeoSphere::getIndices() const {
	return indices;
}

float GeoSphere::getInscriptionRadiusMultiplier() const {
	return inscriptionRadiusMultiplier;
}