REM create directory structure
mkdir "%~1Data"
mkdir "%~1Data\System"
mkdir "%~1Data\System\Shaders"

REM copy initialization files
copy Init.txt %1 /Y
copy SystemResources.txt %1 /Y

REM copy shaders
copy *.prog "%~1Data\System\Shaders" /Y