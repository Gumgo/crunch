#include "Audio.h"
#include "AudioManager.h"

SoundGroup::SoundGroup( AudioManager * am, FMOD::ChannelGroup * chg ) {
	audioManager = am;
	channelGroup = chg;
}

Sound2D SoundGroup::createSound2D( SoundResourceReference & sound, bool createPaused ) {
	return audioManager->createSound2D( *this, sound, createPaused );
}

Sound3D SoundGroup::createSound3D( SoundResourceReference & sound, const Vector3f & pos, const Vector3f & vel, bool createPaused ) {
	return audioManager->createSound3D( *this, sound, pos, vel, createPaused );
}

void SoundGroup::play() {
	audioManager->play( *this );
}

void SoundGroup::pause() {
	audioManager->pause( *this );
}

void SoundGroup::stop() {
	audioManager->stop( *this );
}

void SoundGroup::setVolume( float vol ) {
	audioManager->setVolume( *this, vol );
}

Sound::Sound( AudioManager * am, FMOD::Channel * ch ) {
	audioManager = am;
	channel = ch;
}

void Sound::play() {
	audioManager->play( *this );
}

void Sound::pause() {
	audioManager->pause( *this );
}

void Sound::stop() {
	audioManager->stop( *this );
}

void Sound::setVolume( float vol ) {
	audioManager->setVolume( *this, vol );
}

Sound2D::Sound2D( AudioManager * am, FMOD::Channel * ch )
	: Sound( am, ch ) {
}

void Sound2D::setPanning( float pan ) {
	audioManager->setPanning( *this, pan );
}

Sound3D::Sound3D( AudioManager * am, FMOD::Channel * ch )
	: Sound( am, ch ) {
}

void Sound3D::setPositionVelocity( const Vector3f & pos, const Vector3f & vel ) {
	audioManager->setPositionVelocity( *this, pos, vel );
}