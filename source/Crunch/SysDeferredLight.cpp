#include "SysDeferredLight.h"
#include "Log.h"

const float SysDeferredLight::MIN_INTENSITY = 1.0f / 256.0f;

SysDeferredLight::SysDeferredLight( LightType lt ) {
	lightType = lt;
	shadowsEnabled = false;
	camera = NULL;
	lightMatrixPointer = NULL;
	projectionMatrixPointer = NULL;
	lightMatrixMask = projectionMatrixMask = 0;
}

void SysDeferredLight::setCamera( const SysCamera3D * cam ) {
	camera = cam;
}

SysDeferredLight::LightType SysDeferredLight::getLightType() const {
	return lightType;
}

float SysDeferredLight::luma( const Vector3f & color ) {
	static const Vector3f LUMA( 0.2126f, 0.7152f, 0.0722f );
	return color.dot( LUMA );
}

bool SysDeferredLight::areShadowsEnabled() const {
	return shadowsEnabled;
}

void SysDeferredLight::setupMatrices(
	const Matrix44af * lightMatrixPtr, bool multipleLightMatrices,
	const Matrix44f * projectionMatrixPtr, bool multipleProjectionMatrices ) {
	lightMatrixPointer = lightMatrixPtr;
	projectionMatrixPointer = projectionMatrixPtr;
	lightMatrixMask = multipleLightMatrices ? ~((size_t)0) : 0;
	projectionMatrixMask = multipleProjectionMatrices ? ~((size_t)0) : 0;
}


const Matrix44af & SysDeferredLight::getLightMatrix( size_t i ) const {
	return lightMatrixPointer[i & lightMatrixMask];
}

const Matrix44f & SysDeferredLight::getProjectionMatrix( size_t i ) const {
	return projectionMatrixPointer[i & projectionMatrixMask];
}

float SysDeferredLight::computeQuadraticAttenuationRange( const Vector3f & color, const Vector3f & atten ) {
	// intensity = luminance/(attenuation[0] +
	//                        attenuation[1]*dist +
	//                        attenuation[2]*dist*dist)
	// we want to solve for when the intensity is MIN_LUMINANCE
	// i = l/(a0 + (a1)d + (a2)d^2)
	// l/i = a0 + (a1)d + (a2)d^2
	// 0 = (a2)d^2 + (a1)d + (a0 - l/i)
	//
	// if a2 != 0, this can be solved using the quadratic formula
	//     -a1 +- sqrt( a1^2 - 4(a2)(a0 - l/i) )
	// d = -------------------------------------
	//                   2(a2)
	//
	// otherwise, if a2 == 0
	// 0 = (a1)d + (a0 - l/i)
	// (a1)d = l/i - a0
	// d = (l/i - a0)/a1
	//
	// otherwise, if a1 and a2 are 0, the range is infinite

	float colorLum = luma( color );

	if (atten[2] == 0.0f) {
		if (atten[1] == 0.0f)
			// constant intensity (does not attenuate)
			return std::numeric_limits <float>::max();
		else
			// if negative, this means light is always dimmer than minimum value, so return 0 for the range
			return std::max( (colorLum/MIN_INTENSITY - atten[0])/atten[1], 0.0f );
	} else {
		float disc = atten[1]*atten[1] -
			4.0f*atten[2]*(atten[0] - colorLum/MIN_INTENSITY);
		if (disc < 0.0f)
			// if this happens, it means the light is always dimmer than the minimum value, so return 0 for the range
			return 0.0f;
		// there will be two solutions, but at least one will always be negative since the vertex
		// of this (symmetric) parabola is at most 0
		// we want the positive one (since we want a positive range) which is obtained by adding disc (as opposed to subtracting)
		// if this solution is also negative, it means the light is always dimmer than min value, so return 0 for range
		return std::max( (-atten[1] + sqrt( disc )) / (2.0f * atten[2]), 0.0f );
	}
}

Vector3f SysDeferredLight::findQuadraticAttenuationParameters( float d0, float i0, float d1, float i1 ) {
	// make sure they are ordered - distances positive increasing, intensities positive decreasing
	if ((d0 <= 0.0f) ||
		(i1 <= 0.0f) ||
		(d0 >= d1) ||
		(i0 <= i1)) {
		Log::warning() << "Invalid distances and intensities specified for quadratic attenuation";
		return Vector3f( 0.0f, 0.0f, 1.0f );
	}

	// solve 1/(a + b*x)
	// i0 = 1/(a + b*d0)
	// i1 = 1/(a + b*d1)
	// a*i0 + b*d0*i0 = 1
	// a*i1 + b*d1*i1 = 1
	// dn*in = cn
	// a*i0 + b*c0 = 1
	// a*i1 + b*c1 = 1
	// solve system:
	// [ A ]x = [ 1,1 ]
	// x = [ A ]^1 * [ 1,1 ]
	Matrix22f mat(
		i0, i0*d0,
		i1, i1*d1 );
	mat.invert();
	return Vector3f( mat[0] + mat[1], mat[2] + mat[3], 0.0f );
}

Vector3f SysDeferredLight::findQuadraticAttenuationParameters( float d0, float i0, float d1, float i1, float d2, float i2 ) {
	// make sure they are ordered - distances positive increasing, intensities positive decreasing
	if ((d0 <= 0.0f) ||
		(i2 <= 0.0f) ||
		(d0 >= d1 || d1 >= d2) ||
		(i0 <= i1 || i1 <= i2)) {
		Log::warning() << "Invalid distances and intensities specified for quadratic attenuation";
		return Vector3f( 0.0f, 0.0f, 1.0f );
	}

	// solve 1/(a + b*x + c*x^2)
	// i0 = 1/(a + b*d0 + c*d0^2)
	// i1 = 1/(a + b*d1 + c*d1^2)
	// i2 = 1/(a + b*d2 + c*d2^2)
	// 1 = a*i0 + b*d0*i0 + c*d0^2*i0
	// 1 = a*i1 + b*d1*i1 + c*d1^2*i1
	// 1 = a*i2 + b*d2*i2 + c*d2^2*i2
	// solve system:
	// [ A ]x = [ 1,1,1 ]
	// x = [ A ]^1 * [ 1,1,1 ]
	float i0d0 = i0*d0;
	float i1d1 = i1*d1;
	float i2d2 = i2*d2;
	Matrix33f mat(
		i0, i0d0, i0d0*d0,
		i1, i1d1, i1d1*d1,
		i2, i2d2, i2d2*d2 );
	mat.invert();
	return Vector3f( mat[0] + mat[1] + mat[2], mat[3] + mat[4] + mat[5], mat[6] + mat[7] + mat[8] );
}