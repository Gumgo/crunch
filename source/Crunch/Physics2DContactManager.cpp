#include "Physics2DContactManager.h"
#include "Physics2DSpatialHashMap.h"
#include "Physics2DShape.h"

namespace Physics2D {

ContactManager::ContactManager( SpatialHashMap & shm )
	: spatialHashMap( shm ) {
	contacts = NULL;
	contactCount = 0;

	contactBuffer.reserve( 50 );
}

Contact * ContactManager::createContact( Shape * s0, Shape * s1 ) {
	// create contact, put in front of list
	Contact * contact = new Contact( s0, s1 );
	contact->getSimulationData().next = contacts;
	if (contacts != NULL)
		contacts->getSimulationData().prev = contact;
	contact->getSimulationData().next = contacts;
	contact->getSimulationData().prev = NULL;
	contacts = contact;
	++contactCount;


	return contact;
}

void ContactManager::destroyContact( Contact * c ) {
	if (c->getSimulationData().prev == NULL)
		contacts = c->getSimulationData().next;
	else
		c->getSimulationData().prev->getSimulationData().next = c->getSimulationData().next;
	if (c->getSimulationData().next != NULL)
		c->getSimulationData().next->getSimulationData().prev = c->getSimulationData().prev;
	
	--contactCount;
	safeDelete( c );
}

Contact * ContactManager::getContacts() {
	return contacts;
}

const Contact * ContactManager::getContacts() const {
	return contacts;
}

Contact * ContactManager::getNextContact( Contact * c ) {
	return c->getSimulationData().next;
}

const Contact * ContactManager::getNextContact( const Contact * c ) const {
	return c->getSimulationData().next;
}

void ContactManager::findNewContacts() {
	// TODO: only query things that changed?
	// build up a buffer of new/moved objects and only query them

	spatialHashMap.queryOverlaps( contactBuffer );
	// contactBuffer is now filled with potential contacts

	for (size_t i = 0; i < contactBuffer.size(); ++i) {
		// go through each possible contact
		Shape * shapes[2] = {
			(Shape*)spatialHashMap.getShape( contactBuffer[i].first ),
			(Shape*)spatialHashMap.getShape( contactBuffer[i].second )
		};

		// this ensures that the shapes aren't part of the same body
		// and that at least one is dynamic
		if (!shapes[0]->shouldCollide( shapes[1] ))
			continue;

		// check if it already exists
		bool exists = false;
		for (Contact * c = shapes[0]->getContacts(); c != NULL; c = shapes[0]->getNextContact( c )) {
			if ((c->getShape( 0 ) == shapes[0] && c->getShape( 1 ) == shapes[1]) ||
				(c->getShape( 0 ) == shapes[1] && c->getShape( 1 ) == shapes[0])) {
				exists = true;
				break;
			}
		}
		if (exists)
			continue;

		// it doesn't exist, so create the contact
		Contact * c = createContact( shapes[0], shapes[1] );
		// add to each shape
		shapes[0]->addContact( c );
		shapes[1]->addContact( c );
	}
}

void ContactManager::updateCurrentContacts() {
	// go through each contact
	Contact * c = contacts;
	while (c != NULL) {
		Shape * shapes[2] = {
			c->getShape( 0 ),
			c->getShape( 1 )
		};
		bool destroy = false;

		// make sure the collision should still happen
		if (!shapes[0]->shouldCollide( shapes[1] ))
			destroy = true;

		if (!destroy) {
			bool result = spatialHashMap.queryOverlap(
				shapes[0]->getBodyData().spatialHashIndex, shapes[1]->getBodyData().spatialHashIndex );
			// if they don't overlap, destroy the contact
			if (!result)
				destroy = true;
		}

		if (destroy) {
			Contact * nextC = c->getSimulationData().next;
			shapes[0]->removeContact( c );
			shapes[1]->removeContact( c );
			destroyContact( c );
			c = nextC;
		} else {
			// update the manifold
			c->computeManifold();
			c = c->getSimulationData().next;
		}
	}
}

}