#include "HashFunctions.h"

#if _WIN64 || __amd64__
uint64_t MurmurHash64A ( const void * key, int len, unsigned int seed )
{
	static const size_t m = 0xC6A4A7935BD1E995;
	static const int r = 47;

	size_t h = seed ^ (len * m);

	const size_t * data = (const size_t*)key;
	const size_t * end = data + (len/8);

	while (data != end) {
		size_t k = *data++;

		k *= m; 
		k ^= k >> r; 
		k *= m; 

		h ^= k;
		h *= m; 
	}

	const byte * data2 = (const byte*)data;

	switch (len & 7) {
	case 7:
		h ^= (size_t)data2[6] << 48;
	case 6:
		h ^= (size_t)data2[5] << 40;
	case 5:
		h ^= (size_t)data2[4] << 32;
	case 4:
		h ^= (size_t)data2[3] << 24;
	case 3:
		h ^= (size_t)data2[2] << 16;
	case 2:
		h ^= (size_t)data2[1] << 8;
	case 1:
		h ^= (size_t)data2[0];
		h *= m;
	}
 
	h ^= h >> r;
	h *= m;
	h ^= h >> r;

	return h;
}
#else
size_t murmurHash2( const void * key, size_t len, size_t seed ) {
	// 'm' and 'r' are mixing constants generated offline.
	// They're not really 'magic', they just happen to work well.
	static const size_t m = 0x5BD1E995;
	static const int r = 24;

	// Initialize the hash to a 'random' value
	size_t h = seed ^ len;

	// Mix 4 bytes at a time into the hash
	const byte * data = (const byte*)key;

	while (len >= 4) {
		unsigned int k = *(size_t*)data;

		k *= m; 
		k ^= k >> r; 
		k *= m; 
		
		h *= m; 
		h ^= k;

		data += 4;
		len -= 4;
	}
	
	// Handle the last few bytes of the input array
	switch (len) {
	case 3:
		h ^= data[2] << 16;
	case 2:
		h ^= data[1] << 8;
	case 1:
		h ^= data[0];
		h *= m;
	}

	// Do a few final mixes of the hash to ensure the last few
	// bytes are well-incorporated.
	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}
#endif

size_t Hash <std::string>::operator()( const std::string & t ) const {
	return murmurHash2( t.data(), t.length(), MURMUR_HASH_2_SEED );
}