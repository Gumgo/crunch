#include "Timer.h"

Timer::Timer() {
	running = false;
#if defined CRUNCH_PLATFORM_WINDOWS
	LARGE_INTEGER freq;
	if (QueryPerformanceFrequency( &freq ) == 0)
		throw std::runtime_error( "Timer not supported" );
	frequency = freq.QuadPart;
#elif defined CRUNCH_PLATFORM_APPLE
	UnsignedWide oneSecond;
	oneSecond.hi = 0;
	oneSecond.lo = 1000000000;
	UnsignedWide freq = NanosecondsToAbsolute(oneSecond);
	frequency = (uint64_t)freq.lo + ((uint64_t)freq.hi << 32);
#elif defined CRUNCH_PLATFORM_LINUX
#error Linux not yet supported!
#else
#error Unsupported platform!
#endif
}

Timer::~Timer() {
	if (running)
		stop();
}

Timer::TimerValue Timer::querySystemClock() const {
#if defined CRUNCH_PLATFORM_WINDOWS
	LARGE_INTEGER buf;
	QueryPerformanceCounter( &buf );
	return buf.QuadPart;
#elif defined CRUNCH_PLATFORM_APPLE
	return mach_absolute_time();
#elif defined CRUNCH_PLATFORM_LINUX
#error Linux not yet supported!
#else
#error Unsupported platform!
#endif
}

void Timer::start( uint framesPerSec, uint maxLagFrames ) {
	if (running)
		throw std::runtime_error( "Timer has already been started" );
	if (framesPerSec == 0)
		throw std::runtime_error( "Frame rate must be nonzero" );

	// get the base time
	baseTime = querySystemClock();
	systemTime = baseTime;
	// initially no lag time
	lagTime = 0;
	// compute the length of each frame in terms of frequency
	framesPerSecond = framesPerSec;
	frameLength = (TimerValue)(frequency * (1.0 / (double)framesPerSecond));
	frameLengthSeconds = 1.0f / (float)framesPerSecond;
	// compute the maximum lag time in terms of frequency
	maxLagTime = frameLength * maxLagFrames;
	// reset the frame count
	frames = 0;

	running = true;
}

void Timer::stop() {
	if (running)
		running = false;
}

void Timer::update() {
	if (!running)
		return;

	// get the time at the beginning of this frame
	TimerValue frameTime = getFrameTime();
	// compute the target time for the end of this frame
	TimerValue targetTime = frameTime + frameLength;
	// find the actual time
	TimerValue actualTime = querySystemClock();

	// while we have time left over, go into spinlock
	if (actualTime < targetTime) {
		do {
			endTimeSlice();
			actualTime = querySystemClock();
		} while (actualTime < targetTime);
	} else if (actualTime - targetTime > maxLagTime)
		// if we've lagged too long, add to our allowed lag time so we don't keep trying to catch up
		lagTime += actualTime - (targetTime + maxLagTime);

	// get the system at the beginning of the next frame - now
	systemTime = querySystemClock();
	++frames;

	for (size_t i = 0; i < currentTimingQueries.size(); ++i) {
		if (currentTimingQueries[i].data.type == TQ_CPU) {
			if (currentTimingQueries[i].data.completed)
				currentTimingQueries[i].data.queryTime =
					(double)currentTimingQueries[i].clockTime / ((double)frequency / 1000.0);
		} else {
			if (currentTimingQueries[i].data.completed) {
				GLint available;
				glGetQueryObjectiv( currentTimingQueries[i].gpuQueries[1], GL_QUERY_RESULT_AVAILABLE, &available );
				if (available) {
					GLuint64 result[2];
					glGetQueryObjectui64v( currentTimingQueries[i].gpuQueries[0], GL_QUERY_RESULT, &result[0] );
					glGetQueryObjectui64v( currentTimingQueries[i].gpuQueries[1], GL_QUERY_RESULT, &result[1] );
					GLuint64 difference = result[1] - result[0];
					// convert from nanoseconds to milliseconds
					currentTimingQueries[i].data.queryTime = (double)difference / 1000000.0;
				} else
					currentTimingQueries[i].data.completed = false;
			}
			glDeleteQueries( 2, currentTimingQueries[i].gpuQueries );
		}
	}
	currentTimingQueries.swap( previousTimingQueries );
	currentTimingQueries.clear();
}

void Timer::endTimeSlice() {
	boost::this_thread::yield();
}

Timer::TimerValue Timer::getFrequency() const {
	return frequency;
}

Timer::TimerValue Timer::getFrameTime() const {
	TimerValue ret;
	if (running)
		ret = baseTime + lagTime + frames * frameLength;
	else
		ret = 0;
	return ret;
}

Timer::TimerValue Timer::getFrameCount() const {
	return frames;
}

Timer::TimerValue Timer::getCurrentTime() const {
	return querySystemClock();
}

uint Timer::getFramesPerSecond() const {
	return framesPerSecond;
}

Timer::TimerValue Timer::getFrameLength() const {
	return frameLength;
}

float Timer::getFrameLengthSeconds() const {
	return frameLengthSeconds;
}

Timer::TimerValue Timer::getSystemTime() const {
	return systemTime;
}

Timer::TimerValue Timer::getSystemTimeMillis() const {
	return getSystemTime() / (frequency / 1000);
}

size_t Timer::beginTimingQuery( const std::string & name, TimingQueryType type ) {
	currentTimingQueries.push_back( TimingQueryDataWrapper() );
	currentTimingQueries.back().data.name = name;
	currentTimingQueries.back().data.completed = false;
	currentTimingQueries.back().data.type = type;
	if (type == TQ_CPU)
		currentTimingQueries.back().clockTime = querySystemClock();
	else {
		glGenQueries( 2, currentTimingQueries.back().gpuQueries );
		glQueryCounter( currentTimingQueries.back().gpuQueries[0], GL_TIMESTAMP );
	}
	return currentTimingQueries.size()-1;
}

void Timer::endTimingQuery( size_t id ) {
	if (id >= currentTimingQueries.size())
		throw std::runtime_error( "Invalid timing query ID" );

	if (currentTimingQueries[id].data.completed)
		return;
	if (currentTimingQueries[id].data.type == TQ_CPU) {
		TimerValue newTime = querySystemClock();
		currentTimingQueries[id].clockTime = newTime - currentTimingQueries[id].clockTime;
	} else
		glQueryCounter( currentTimingQueries.back().gpuQueries[1], GL_TIMESTAMP );
	currentTimingQueries[id].data.completed = true;
}

size_t Timer::getTimingQueryCount() const {
	return previousTimingQueries.size();
}

const Timer::TimingQueryData & Timer::getTimingQueryResult( size_t i ) const {
	if (i >= previousTimingQueries.size())
		throw std::runtime_error( "Invalid timing query ID" );
	return previousTimingQueries[i].data;
}