/** @file SoundResource.h
 *  @brief Contains the resource holding sound data.
 */

#ifndef SOUNDRESOURCE_DEFINED
#define SOUNDRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "KeyValueReader.h"
#include <vector>
#include <fmod.h>
#include <fmod.hpp>

/** @brief Sound resource.
 */
DEFINE_RESOURCE_CLASS( SoundResource, "sound", "sounds" )
public:
	/** @brief The constructor.
	 */
	SoundResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:

	/** @brief Reads metadata.
	 */
	static bool readMetadata( uint & metadata, KeyValueReader::Iterator it );

	static const uint LOOP	= 0x1;	/**< The sound should loop.*/
	static const uint IS_3D	= 0x2;	/**< The sound is 3D.*/

	/** @brief Returns default metadata.
	 */
	static uint getDefaultMetadata();

	// TODO: make this better
	FMOD::Sound * getSound();				/**< Returns the sound data.*/
	const FMOD::Sound * getSound() const;	/**< Returns the sound data.*/

private:
	FMOD::Sound * sound;	/**< The sound data.*/
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <SoundResource> SoundResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <SoundResource> SoundResourceConstReference;

#endif