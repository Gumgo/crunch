#include "Physics2DBody.h"
#include "Physics2DSimulation.h"

namespace Physics2D {

BodyDefinition::BodyDefinition() {
	type = Body::T_STATIC;

	initialRotation = (Real)0;

	initialAngularVelocity = (Real)0;

	fixedRotation = false;
}

Body::Body( const BodyDefinition & d, Simulation * s ) {
	simulation = s;

	type = d.type;

	position = d.initialPosition;
	rotation = d.initialRotation;

	linearVelocity = d.initialLinearVelocity;
	angularVelocity = d.initialAngularVelocity;

	fixedRotation = d.fixedRotation;

	torque = (Real)0;

	if (type == T_DYNAMIC)
		massData = MassData( Vector2r(), (Real)1, (Real)0 );
	else
		massData = MassData( Vector2r(), (Real)0, (Real)0 );

	shapes = NULL;
	shapeCount = 0;
}

Simulation * Body::getSimulation() {
	return simulation;
}

const Simulation * Body::getSimulation() const {
	return simulation;
}

Body::Type Body::getType() const {
	return type;
}

Shape * Body::attachShape( const ShapeDefinition & d ) {
	simulation->throwIfLocked();

	Shape * s = d.createShape( this );
	s->computeMassData();

	Aabb aabb = s->getAabb( getMatrixTransform() );
	aabb.expand( Shape::AABB_EXPANSION );
	s->getBodyData().spatialHashIndex
		= simulation->spatialHashMap.addShape( s, aabb );

	s->getBodyData().next = shapes;
	shapes = s;
	++shapeCount;

	computeMassData();

	// TODO alert the simulation that a new shape has been added so that contacts can be updated

	return s;
}

void Body::removeShape( Shape * s ) {
	simulation->throwIfLocked();

	if (s->getParent() != this)
		throw std::runtime_error( "Attempted to remove a shape from the wrong body" );

	Shape ** shape = &shapes;
	while (*shape != NULL) {
		if (*shape == s)
			break;
		*shape = (*shape)->getBodyData().next;
	}

	if (*shape == NULL) {
		assert( false );
		return;
	}

	// destroy all the contacts for this shape
	Contact * c;
	while ((c = s->getContacts()) != NULL) {
		// keep getting contacts while not NULL
		// remove from each shape
		c->getShape( 0 )->removeContact( c );
		c->getShape( 1 )->removeContact( c );
		// destroy the contact
		simulation->contactManager.destroyContact( c );
	}

	simulation->spatialHashMap.removeShape( s->getBodyData().spatialHashIndex );

	// remove from list
	*shape = s->getBodyData().next;
	--shapeCount;
	safeDelete( s );

	computeMassData();
}

Shape * Body::getShapes() {
	return shapes;
}

const Shape * Body::getShapes() const {
	return shapes;
}

Shape * Body::getNextShape( Shape * s ) {
	return s->getBodyData().next;
}

const Shape * Body::getNextShape( const Shape * s ) const {
	return s->getBodyData().next;
}

size_t Body::getShapeCount() const {
	return shapeCount;
}

void Body::computeMassData() {
	Vector2r massCtr;
	Real mass = (Real)0;
	Real inertia = (Real)0;

	if (type != T_DYNAMIC) {
		massData = MassData( Vector2r(), (Real)0, (Real)0 );
		return;
	}

	// first get total mass and center of mass
	Shape * s = shapes;
	while (s != NULL) {
		MassData md = s->getMassData();
		massCtr += md.center * md.mass;
		mass += md.mass;
		s = s->getBodyData().next;
	}
	massCtr *= ((Real)1 / mass);

	// now compute inertia
	// inertia is infinity (stored as 0) if fixed rotation
	if (!fixedRotation) {
		Shape * s = shapes;
		while (s != NULL) {
			MassData md = s->getMassData();
			inertia += md.inertia + md.mass*(md.center - massCtr).magnitudeSquared();
			s = s->getBodyData().next;
		}
	}

	massData = MassData( massCtr, mass, inertia );

	simulationData.worldCenterOfMass = getMatrixTransform().transformPoint( massData.center );
}

MassData Body::getMassData() const {
	return massData;
}

Vector2r Body::getPosition() const {
	return position;
}

Real Body::getRotation() const {
	return rotation;
}

Matrix33ar Body::getMatrixTransform() const {
	return Matrix33ar().rotated( rotation ).translated( position );
}

Vector2r Body::getLinearVelocity() const {
	return linearVelocity;
}

Real Body::getAngularVelocity() const {
	return angularVelocity;
}

Vector2r Body::getForce() const {
	return force;
}

Real Body::getTorque() const {
	return torque;
}

bool Body::isRotationFixed() const {
	return fixedRotation;
}

// setters/mutators

void Body::setTransform( const Vector2r & pos, Real rot ) {
	simulation->throwIfLocked();

	position = pos;
	rotation = rot;

	Matrix33ar tf = getMatrixTransform();
	simulationData.worldCenterOfMass = tf.transformPoint( massData.center );

	Shape * s = shapes;
	while (s != NULL) {
		Aabb aabb = s->getAabb( tf );
		aabb.expand( Shape::AABB_EXPANSION );
		simulation->spatialHashMap.moveShape( s->getBodyData().spatialHashIndex, aabb );
		s = s->getBodyData().next;
	}
	// TODO: find new contacts
}

void Body::setLinearVelocity( const Vector2r & vel ) {
	if (type == T_STATIC)
		throw std::runtime_error( "Cannot set linear velocity of a static body" );

	linearVelocity = vel;
}

void Body::setAngularVelocity( Real vel ) {
	if (type == T_STATIC)
		throw std::runtime_error( "Cannot set angular velocity of a static body" );

	angularVelocity = vel;
}

void Body::applyForceToCenter( const Vector2r & f ) {
	if (type != T_DYNAMIC)
		throw std::runtime_error( "Forces can only be applied to dynamic objects" );

	force += f;
}

void Body::applyForce( const Vector2r & f, const Vector2r & pt ) {
	if (type != T_DYNAMIC)
		throw std::runtime_error( "Forces can only be applied to dynamic objects" );

	force += f;
	Vector2r vec = pt - simulationData.worldCenterOfMass;
	Real crossZ = vec.x*f.y - vec.y*f.x;
	torque += crossZ;
}

void Body::applyTorque( Real t ) {
	if (type != T_DYNAMIC)
		throw std::runtime_error( "Torques can only be applied to dynamic objects" );

	torque += t;
}

void Body::applyLinearImpulseToCenter( const Vector2r & i ) {
	if (type != T_DYNAMIC)
		throw std::runtime_error( "Impulses can only be applied to dynamic objects" );

	linearVelocity += massData.invMass * i;
}

void Body::applyLinearImpulse( const Vector2r & i, const Vector2r & pt ) {
	if (type != T_DYNAMIC)
		throw std::runtime_error( "Impulses can only be applied to dynamic objects" );

	linearVelocity += massData.invMass * i;
	Vector2r vec = pt - simulationData.worldCenterOfMass;
	Real crossZ = vec.x*i.y - vec.y*i.x;
	angularVelocity += massData.invInertia * crossZ;
}

void Body::applyAngularImpulse( Real i ) {
	if (type != T_DYNAMIC)
		throw std::runtime_error( "Impulses can only be applied to dynamic objects" );

	angularVelocity += massData.invInertia * i;
}

void Body::setFixedRotation( bool f ) {
	if (f != fixedRotation) {
		fixedRotation = f;
		computeMassData();
	}
}

Body::SimulationData & Body::getSimulationData() {
	return simulationData;
}

const Body::SimulationData & Body::getSimulationData() const {
	return simulationData;
}

}