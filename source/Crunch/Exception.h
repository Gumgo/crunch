/** @file Exception.h
 *  @brief Contains a templated exception class derived from std::exception.
 */
#ifndef EXCEPTION_DEFINED
#define EXCEPTION_DEFINED

#include <exception>
#include <stdexcept>

/** @brief Templated exception class used to distinguish exception types.
 *
 *  Often times it is important to be able
 *  to distinguish exception types. The
 *  Exception class's template parameter
 *  serves as an easy solution. If a class
 *  A needs the same functionality as
 *  std::exception but requires that only
 *  exceptions thrown by it are caught
 *  in its catch blocks, it should throw
 *  and catch Exception <A> instances.
 *
 *  @tparam T	The unique identifying type of this exception.
 */
// todo: get this working - not really important for now though
//template <typename T> class Exception : public std::runtime_error {
//public:
//	/**
//	 * @brief The default constructor.
//	 */
//	Exception() throw();
//
//	/**
//	 * @brief The constructor taking a message.
//	 */
//	Exception( const char * const & message );
//
//	/**
//	 * @brief The constructor taking a message and an integer value.
//	 */
//	Exception( const char * const & message, int i );
//
//	/**
//	 * @brief The destructor.
//	 */
//	virtual ~Exception();
//};
//
//template <typename T> Exception <T>::Exception()
//	: std::runtime_error() {
//}
//
//template <typename T> Exception <T>::Exception( const char * const & message )
//	: std::runtime_error( message ) {
//}
//
//template <typename T> Exception <T>::Exception( const char * const & message, int i )
//	: std::runtime_error( message, i ) {
//}
//
//template <typename T> Exception <T>::~Exception() {
//}

#endif