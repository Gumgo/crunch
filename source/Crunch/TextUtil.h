/** @file TextUtil.h
 *  @brief Contains text-related utility functions.
 */

#ifndef TEXTUTIL_DEFINED
#define TEXTUTIL_DEFINED

#include "Common.h"
#include "FontResource.h"

/** @brief Determines how text is aligned horizontally.
 */
enum TextAlignHoriz {
	TAH_LEFT,
	TAH_CENTER,
	TAH_RIGHT
};

/** @brief Determines how text is aligned vertically.
 */
enum TextAlignVert {
	TAV_TOP,
	TAV_MIDDLE,
	TAV_BOTTOM
};

/** @brief Returns the width and height of a string of text.
 */
Vector2i getTextDimens( FontResource & font, const std::string & text );

/** @brief Returns a string with line breaks inserted.
 */
std::string breakString( FontResource & font, const std::string & text, int maxWidth );

/** @brief Returns the width of the remainder of the line starting from index.
 */
uint getLineWidth( FontResource & font, const std::string & text, size_t index );

#endif