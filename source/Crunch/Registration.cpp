#include "Registration.h"
#include "Context.h"

// resources
#include "FontResource.h"
#include "MeshResource.h"
#include "ProgramResource.h"
#include "Skeleton2DResource.h"
#include "Skeleton2DAnimationResource.h"
#include "Skeleton3DResource.h"
#include "Skeleton3DAnimationResource.h"
#include "Skeleton3DAnimationStateMachineResource.h"
#include "Skeleton3DAnimationTreeResource.h"
#include "SoundResource.h"
#include "TextureResource.h"

void registerCoreComponents() {
	Context::get().resourceManager.registerResourceClass <FontResource>();
	Context::get().resourceManager.registerResourceClass <MeshResource>();
	Context::get().resourceManager.registerResourceClass <ProgramResource>();
	Context::get().resourceManager.registerResourceClass <Skeleton2DResource>();
	Context::get().resourceManager.registerResourceClass <Skeleton2DAnimationResource>();
	Context::get().resourceManager.registerResourceClass <Skeleton3DResource>();
	Context::get().resourceManager.registerResourceClass <Skeleton3DAnimationResource>();
	Context::get().resourceManager.registerResourceClass <Skeleton3DAnimationStateMachineResource>();
	Context::get().resourceManager.registerResourceClass <Skeleton3DAnimationTreeResource>();
	Context::get().resourceManager.registerResourceClass <SoundResource>();
	Context::get().resourceManager.registerResourceClass <TextureResource>();
}