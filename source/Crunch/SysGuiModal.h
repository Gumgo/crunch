#ifndef SYSGUIMODAL_DEFINED
#define SYSGUIMODAL_DEFINED

#include "Common.h"
#include "Actor.h"
#include "SysGui.h"

#include "InputEvent.h"
#include "SysCamera2D.h"
#include "FontResource.h"
#include "Renderer.h"

#include "HashMap.h"
#include "HashFunctions.h"
#include <boost/function.hpp>
#include <vector>

DEFINE_ACTOR( SysGuiModal, 0, 0 )

	const SysCamera2D * camera;
	FontResourceReference defaultFont;

	SysGuiModal * childModal;
	std::vector <SysGui*> children;

	struct Event {
		boost::function <void ()> onCtrlEvent;
		boost::function <void ()> onEvent;
	};

	struct I {
		HashMap <Hash <InputEvent>, InputEvent, Event> events;
		I();
	} i;

	Renderer::Call drawCall;
	Renderer::Key drawKey;

public:
	void onCreate();

	void setCamera( const SysCamera2D * cam );
	void setDefaultFont( FontResourceReference & defFont );
	FontResourceReference getDefaultFont();
	FontResourceConstReference getDefaultFont() const;
	void addGui( SysGui * gui );
	void setChildModal( SysGuiModal * modal );

	void receiveEvent( const InputEvent & ev );
	void setEventHandler( const InputEvent & ev, bool ctrl, const boost::function <void ()> & eventHandler );
	void removeEventHandler( const InputEvent & ev, bool ctrl );
	void draw();
	void onDraw();

	void onDismiss();
};

#endif