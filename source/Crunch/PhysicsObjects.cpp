#if 0

#include "PhysicsObjects.h"

LineSegment::LineSegment( const Vector2f * pts )
	: line( Line2Points( pts[0], pts[1] ) ) {
	points[0] = pts[0];
	points[1] = pts[1];
}

LineSegment::LineSegment( const Vector2f & pt0, const Vector2f & pt1 )
	: line( Line2Points( pt0, pt1 ) ) {
	points[0] = pt0;
	points[1] = pt1;
}

#endif