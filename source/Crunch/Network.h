#if 0

/** @file Network.h
 *  @brief Includes networking headers.
 */

#ifndef NETWORK_DEFINED
#define NETWORK_DEFINED

#include "Socket.h"

// TODO: change the guaranteed message backup system to use a block-like allocator of different sizes, rather than the current system
// the idea is there are preset block sizes (4, 8, 16, 32, 64 bytes, etc.) which are each allocated in an array
// initially the arrays for each size are small, but as more messages of a particular size are needed, the arrays grow
// this way, sizes which are used more often will naturally have more memory
// for messages larger than the largest block, malloc I guess
// TODO: change boost::function to use fastdelegate for efficiency

#endif

#endif