#if 0

/** @file NetworkWriter.h
 *  @brief Writes messages to send to the network.
 */

#ifndef NETWORKWRITER_DEFINED
#define NETWORKWRITER_DEFINED

#include "Common.h"
#include "Network.h"
#include <vector>

class NetworkIdLayer;

/** @brief Writes messages to send to the network.
 */
class NetworkWriter : private boost::noncopyable {
	// allow the ID layer to set our network ID
	friend class NetworkIdLayer;

	std::vector <byte> buffer;	/**< The buffer containing the data to be sent over the network.*/

	/** @brief Adds the given number of bytes to the buffer and returns a pointer to write to.
	 */
	byte * inc( size_t bytes );

	void setNetworkId( uint networkId );	/**< Alters the first 2 bytes of the message.*/
	const byte * getData() const;			/**< Returns a pointer to the data.*/
	size_t getDataSize() const;				/**< Returns the size of the data.*/

public:
	/** @brief The constructor.
	 */
	NetworkWriter( size_t reserve = 32 );

	void writeChar( char c );					/**< Writes a char.*/
	void writeByte( byte b );					/**< Writes a byte.*/
	void writeShort( short s );					/**< Writes a short.*/
	void writeUshort( ushort us );				/**< Writes an unsigned short.*/
	void writeInt( int i );						/**< Writes an int.*/
	void writeUint( uint ui );					/**< Writes an unsigned int.*/
	void writeFloat( float f );					/**< Writes a float.*/
	void writeDouble( double d );				/**< Writes a double.*/
	void writeString( const std::string & s );	/**< Writes a string.*/
	void writeData( const void * src, size_t bytes );	/**< Writes the given amount of data from the given pointer.*/
};

#endif

#endif