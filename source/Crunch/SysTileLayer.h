#ifndef SYSTILELAYER_DEFINED
#define SYSTILELAYER_DEFINED

#include "Common.h"
#include "Actor.h"
#include "Renderer.h"
#include "GpuBuffer.h"
#include "ProgramResource.h"
#include "TextureResource.h"
#include "SysCamera2D.h"
#include <vector>

DEFINE_ACTOR( SysTileLayer, 0, 0 )
public:
	void onCreate();
	void onDraw();
	void onDestroy();

	// structs for tile setup
	struct TileData {
		ushort position[2];			// tile position
		byte size[2];				// tile size
		ushort clusterPosition[2];	// position in the cluster
		byte animationFrames;		// number of animation frames
		byte animationFrameLength;	// length of each frame
		byte animationTimeOffset;	// time offset of animation
	};

	struct TileClusterData {
		std::vector <TileData> tiles;
	};

	struct TileLayerData {
		// tile layers are made up of clusters
		// each cluster is a rectangular region containing tiles
		Vector2i clusterSize;					// size of each cluster
		Vector2i firstClusterCoordinates;		// coords of first cluster relative to cluster size
		size_t clustersPerRow;					// number of clusters per row
		size_t clusterRows;						// number of cluster rows
		int renderPriority;						// rendering priority of this tile layer
		float parallaxMultiplier;				// parallax multiplier for this tile layer
		std::vector <bool> clustersEnabled;		// whether each cluster is enabled
		std::vector <TileClusterData> clusters;	// list of clusters
	};

	void init( const TileLayerData & tileLayerData );
	void setCamera( const SysCamera2D * cam );
	void setTileset( TextureResourceReference & ts );

private:
	class TileCluster {
		const SysTileLayer * layer;
		size_t firstTileIndex;
		size_t firstVertexIndex;
		size_t tileCount;

	public:
		struct TileVertex {
			float pos[2];
			float uv[2];
			float width;
			int animFrames;
			int animFrameLength;
			int animTimeOffset;
		};

		TileCluster( const SysTileLayer * l, const TileClusterData & tileClusterData, std::vector <TileVertex> & vertices, std::vector <uint> & indices );
		void draw( GpuProgram * prog, const Vector2i & clusterCoordinates, const Vector2f & layerOffset );
	};

	friend class TileCluster;

	ProgramResourceReference tilesProgram;
	TextureResourceReference tileset;
	const SysCamera2D * camera;

	GpuBufferReference vertexBuffer;
	GpuBufferReference indexBuffer;

	Vector2i clusterSize;
	Vector2i firstClusterCoordinates;
	size_t clustersPerRow;
	size_t clusterRows;
	std::vector <TileCluster*> clusters;
	float parallaxMultiplier;

	void draw();
	Renderer::Call drawCall;
	Renderer::Key drawKey;
};

#endif