#include "SysDeferredSpotLight.h"
#include "Context.h"
#include "SysRendererDeferredLighting.h"
#include "SysRenderer2D.h"
#include "Log.h"

const float SysDeferredSpotLight::FAR_RANGE_MULTIPLIER = 1.25f;

SysDeferredSpotLight::SysDeferredSpotLight()
	: SysDeferredLight( LT_SPOT ) {
	setupMatrices( &lightMatrix, false, &projectionMatrix, false );

	coneProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_spot_cone", true );
	coneShadowProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_spot_cone_shadow", true );
	quadProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_spot_quad", true );
	quadShadowProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_spot_quad_shadow", true );
	geomProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_spot_geometry", true );

	autoConeProgram.setProgram( coneProgram->getProgram() );
	autoQuadProgram.setProgram( quadProgram->getProgram() );
	autoGeomProgram.setProgram( geomProgram->getProgram() );
	GpuBufferReference coneBuffer = SysRendererDeferredLighting::get().getLightVolumeVertexBuffer();
	GpuBufferReference quadBuffer = SysRenderer2D::get().getQuadVertexBuffer();
	autoConeProgram.setAttribute( "position", coneBuffer, 3, GL_FLOAT, false, false, sizeof( Vector3f ),
		bufferOffset( SysRendererDeferredLighting::get().getConeVertexOffset() ) );
	autoQuadProgram.setAttribute( "corner", quadBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	autoGeomProgram.setAttribute( "position", coneBuffer, 3, GL_FLOAT, false, false, sizeof( Vector3f ),
		bufferOffset( SysRendererDeferredLighting::get().getConeVertexOffset() ) );
	autoConeProgram.setUniform( "modelViewMatrix", GL_FLOAT_MAT3x4, &uniforms.modelViewMatrix );
	autoConeProgram.setUniform( "projectionMatrix", GL_FLOAT_MAT4, &uniforms.projectionMatrix );
	autoConeProgram.setUniform( "screenSize", GL_FLOAT_VEC2, &uniforms.screenSize );
	autoConeProgram.setUniform( "tanAngle", GL_FLOAT, &uniforms.tanAngle );
	autoConeProgram.setUniform( "radius", GL_FLOAT, &uniforms.radius );
	autoQuadProgram.setUniform( "imagePlaneHalfSize", GL_FLOAT_VEC2, &uniforms.imagePlaneHalfSize );
	autoGeomProgram.setUniform( "mvpMatrix", GL_FLOAT_MAT4, &uniforms.projectionMatrix ); // reuse proj mat
	autoGeomProgram.setUniform( "tanAngle", GL_FLOAT, &uniforms.tanAngle );
	autoGeomProgram.setUniform( "radius", GL_FLOAT, &uniforms.radius );
	autoGeomProgram.setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
	SysAutoProgram * progs[2] = { &autoConeProgram, &autoQuadProgram };
	for (size_t i = 0; i < arraySize( progs ); ++i) {
		progs[i]->setUniform( "normalSpecMap", GL_SAMPLER_2D, &uniforms.normalSpecMap );
		progs[i]->setUniform( "depthMap", GL_SAMPLER_2D, &uniforms.depthMap );
		progs[i]->setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
		progs[i]->setUniform( "quadraticAttenuation", GL_BOOL, &uniforms.quadraticAttenuation );
		progs[i]->setUniform( "attenuation", GL_FLOAT_VEC3, &uniforms.attenuation );
		progs[i]->setUniform( "coneAngles", GL_FLOAT_VEC2, &uniforms.coneAngles );
		progs[i]->setUniform( "viewSpacePosition", GL_FLOAT_VEC3, &uniforms.viewSpacePosition );
		progs[i]->setUniform( "viewSpaceDirection", GL_FLOAT_VEC3, &uniforms.viewSpaceDirection );
	}
	uniforms.color.set( 1.0f, 1.0f, 1.0f );
	setAttenuation( AM_QUADRATIC, Vector3f( 0.0f, 0.0f, 1.0f ) );
	uniforms.normalSpecMap = 0;
	uniforms.depthMap = 2;

	direction.set( 0.0f, 0.0f, 1.0f );
	setConeAngles( Vector2f( 0.0f, 0.125f * boost::math::constants::pi <float>() ) );
	coneMatrix = computeConeMatrix();
	setPosition( Vector3f() );
}

SysDeferredSpotLight::~SysDeferredSpotLight() {
}

Matrix44af SysDeferredSpotLight::computeLightMatrix() const {
	// we need to select some arbitrary vector to be our "up" vector
	// set it to be the basis vector of the smallest component of direction
	// that should reduce any potential small float ickiness
	// and also (hopefully) make our shadow map orientation relatively consistent when moving the light
	Vector3f u;
	Vector3f a( std::abs( direction.x ), std::abs( direction.y ), std::abs( direction.z ) );
	if (a.x < a.y && a.x < a.z)
		u.x = 1.0f;
	else if (a.y < a.z)
		u.y = 1.0f;
	else
		u.z = 1.0f;
	// want direction to be in front of us
	Vector3f b = -direction;
	Vector3f r = u.cross( b );
	r.normalize();
	u = b.cross( r ); // no need to normalize
	return Matrix44af(
		r.x, r.y, r.z, -r.dot( position ),
		u.x, u.y, u.z, -u.dot( position ),
		b.x, b.y, b.z, -b.dot( position ) );
}

Matrix44af SysDeferredSpotLight::computeConeMatrix() const {
	// we need to select some arbitrary vector to be our "up" vector
	// set it to be the basis vector of the smallest component of direction
	// that should reduce any potential small float ickiness
	// and also (hopefully) make our shadow map orientation relatively consistent when moving the light
	Vector3f u;
	Vector3f a( std::abs( direction.x ), std::abs( direction.y ), std::abs( direction.z ) );
	if (a.x < a.y && a.x < a.z)
		u.x = 1.0f;
	else if (a.y < a.z)
		u.y = 1.0f;
	else
		u.z = 1.0f;
	// negate direction (b = -(-direction)) since cone is facing up +z, not -z
	Vector3f b = direction;
	Vector3f r = u.cross( b );
	r.normalize();
	u = b.cross( r ); // no need to normalize
	return Matrix44af(
		r.x, u.x, b.x, position.x,
		r.y, u.y, b.y, position.y,
		r.z, u.z, b.z, position.z );
}

Matrix44f SysDeferredSpotLight::computeProjectionMatrix() const {
	float cotFov = 1.0f / tan( uniforms.coneAngles[1] );
	float n = uniforms.lightNearFar.x;
	float f = uniforms.lightNearFar.y;
	return Matrix44f(
		cotFov,	0.0f,	0.0f,			0.0f,
		0.0f,	cotFov,	0.0f,			0.0f,
		0.0f,	0.0f,	(f+n)/(n-f),	(2.0f*f*n)/(n-f),
		0.0f,	0.0f,	-1.0f,			0.0f );
}

SysFrustum SysDeferredSpotLight::computeCullingFrustum() const {
	float cotFov = 1.0f / tan( uniforms.coneAngles[1] );
	float n = shadowMinNear;
	float f = range;
	Matrix44f projMat(
		cotFov,	0.0f,	0.0f,			0.0f,
		0.0f,	cotFov,	0.0f,			0.0f,
		0.0f,	0.0f,	(f+n)/(n-f),	(2.0f*f*n)/(n-f),
		0.0f,	0.0f,	-1.0f,			0.0f );
	SysFrustum frustum;
	frustum.setFrustum( projMat * lightMatrix );
	return frustum;
}

void SysDeferredSpotLight::enableShadow( size_t size, float minNear, float maxFar, float constantBias, float maxMultiplierBias ) {
	shadowsEnabled = true;

	GpuTexture::Texture2DDescription desc;
	desc.imageDescription.internalFormat = GL_DEPTH_COMPONENT;
	desc.imageDescription.minFilter = GL_NEAREST;
	desc.imageDescription.magFilter = GL_NEAREST;
	desc.wrapS = GL_CLAMP_TO_EDGE;
	desc.wrapT = GL_CLAMP_TO_EDGE;
	GpuTexture::Data2DDescription mip;
	desc.mipmapLevels = &mip;
	mip.width = (uint)size;
	mip.height = (uint)size;
	mip.format = GL_DEPTH_COMPONENT;
	mip.type = GL_FLOAT;
	shadowMap = Context::get().gpuState.createTexture( desc );
	shadowMinNear = minNear;
	shadowMaxFar = maxFar;

	autoConeProgram.setProgram( coneShadowProgram->getProgram() );
	autoQuadProgram.setProgram( quadShadowProgram->getProgram() );
	GpuBufferReference coneBuffer = SysRendererDeferredLighting::get().getLightVolumeVertexBuffer();
	GpuBufferReference quadBuffer = SysRenderer2D::get().getQuadVertexBuffer();
	autoConeProgram.setAttribute( "position", coneBuffer, 3, GL_FLOAT, false, false, sizeof( Vector3f ),
		bufferOffset( SysRendererDeferredLighting::get().getConeVertexOffset() ) );
	autoQuadProgram.setAttribute( "corner", quadBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	autoConeProgram.setUniform( "modelViewMatrix", GL_FLOAT_MAT3x4, &uniforms.modelViewMatrix );
	autoConeProgram.setUniform( "projectionMatrix", GL_FLOAT_MAT4, &uniforms.projectionMatrix );
	autoConeProgram.setUniform( "screenSize", GL_FLOAT_VEC2, &uniforms.screenSize );
	autoConeProgram.setUniform( "tanAngle", GL_FLOAT, &uniforms.tanAngle );
	autoConeProgram.setUniform( "radius", GL_FLOAT, &uniforms.radius );
	autoQuadProgram.setUniform( "imagePlaneHalfSize", GL_FLOAT_VEC2, &uniforms.imagePlaneHalfSize );
	SysAutoProgram * progs[2] = { &autoConeProgram, &autoQuadProgram };
	for (size_t i = 0; i < arraySize( progs ); ++i) {
		progs[i]->setUniform( "normalSpecMap", GL_SAMPLER_2D, &uniforms.normalSpecMap );
		progs[i]->setUniform( "depthMap", GL_SAMPLER_2D, &uniforms.depthMap );
		progs[i]->setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
		progs[i]->setUniform( "quadraticAttenuation", GL_BOOL, &uniforms.quadraticAttenuation );
		progs[i]->setUniform( "attenuation", GL_FLOAT_VEC3, &uniforms.attenuation );
		progs[i]->setUniform( "coneAngles", GL_FLOAT_VEC2, &uniforms.coneAngles );
		progs[i]->setUniform( "viewSpacePosition", GL_FLOAT_VEC3, &uniforms.viewSpacePosition );
		progs[i]->setUniform( "viewSpaceDirection", GL_FLOAT_VEC3, &uniforms.viewSpaceDirection );

		progs[i]->setUniform( "faceNormalMap", GL_SAMPLER_2D, &uniforms.faceNormalMap );
		progs[i]->setUniform( "shadowMap", GL_SAMPLER_2D, &uniforms.shadowMap );
		progs[i]->setUniform( "viewToLightMatrix", GL_FLOAT_MAT3x4, &uniforms.viewToLightMatrix );
		progs[i]->setUniform( "halfResolution", GL_FLOAT, &uniforms.halfResolution );
		progs[i]->setUniform( "halfSize", GL_FLOAT, &uniforms.halfSize );
		progs[i]->setUniform( "constantBias", GL_FLOAT, &uniforms.constantBias );
		progs[i]->setUniform( "maxMultiplierBias", GL_FLOAT, &uniforms.maxMultiplierBias );
		progs[i]->setUniform( "lightNearFar", GL_FLOAT_VEC2, &uniforms.lightNearFar );
	}
	uniforms.faceNormalMap = 1;
	uniforms.shadowMap = 4;
	uniforms.halfResolution = 0.5f * (float)size;
	uniforms.constantBias = constantBias;
	uniforms.maxMultiplierBias = maxMultiplierBias;
	// set the near/far uniforms only when computing shadow z range
}

void SysDeferredSpotLight::setColor( const Vector3f & c ) {
	uniforms.color = c;
	if (attenuationMode == AM_QUADRATIC) {
		range = computeQuadraticAttenuationRange( uniforms.color, uniforms.attenuation );
		uniforms.lightNearFar.y = std::min( shadowMaxFar, range * FAR_RANGE_MULTIPLIER );
	}
}

Vector3f SysDeferredSpotLight::getColor() const {
	return uniforms.color;
}

void SysDeferredSpotLight::setPosition( const Vector3f & p ) {
	position = p;
	lightMatrix = computeLightMatrix();
	coneMatrix = computeConeMatrix();
}

Vector3f SysDeferredSpotLight::getPosition() const {
	return position;
}

void SysDeferredSpotLight::setDirection( const Vector3f & d ) {
	direction = d.normalized();
	lightMatrix = computeLightMatrix();
	coneMatrix = computeConeMatrix();
}

Vector3f SysDeferredSpotLight::getDirection() const {
	return direction;
}

void SysDeferredSpotLight::setConeAngles( const Vector2f & innerOuter ) {
	if (innerOuter[0] < 0.0 || innerOuter[1] <= innerOuter[0] || innerOuter[1] >= 0.5f * boost::math::constants::pi <float>()) {
		Log::warning() << "Invalid cone angle parameters [" << innerOuter[0] << ", " << innerOuter[1] << "]";
		return;
	}
	uniforms.coneAngles = innerOuter;
	uniforms.tanAngle = uniforms.halfSize = tan( uniforms.coneAngles[1] );
	uniforms.tanAngle *= SysRendererDeferredLighting::get().getCone().getInscriptionRadiusMultiplier();
}

Vector2f SysDeferredSpotLight::getConeAngles() const {
	return uniforms.coneAngles;
}

void SysDeferredSpotLight::setAttenuation( SysDeferredLight::AttenuationMode mode, const Vector3f & params ) {
	if (mode == AM_QUADRATIC) {
		if ((params[0] == 0.0f && params[1] == 0.0f && params[2] == 0.0f) || params[0] < 0.0f || params[1] < 0.0f || params[2] < 0.0f) {
			Log::warning() << "Invalid spot light quadratic attenuation parameters [" <<
				params[0] << ", " << params[1] << ", " << params[2] << "]";
			return;
		}
		uniforms.attenuation = params;
		range = computeQuadraticAttenuationRange( uniforms.color, uniforms.attenuation );
		uniforms.quadraticAttenuation = 1;
	} else if (mode == AM_RADIAL) {
		if (params[0] < 0.0f || params[1] <= params[0]) {
			Log::warning() << "Invalid spot light radial attenuation parameters [" <<
				params[0] << ", " << params[1] << "]";
			return;
		}
		uniforms.attenuation = params;
		range = params[1];
		uniforms.quadraticAttenuation = 0;
	} else
		assert( false );
	attenuationMode = mode;
}

SysDeferredSpotLight::AttenuationMode SysDeferredSpotLight::getAttenuationMode() const {
	return attenuationMode;
}

Vector3f SysDeferredSpotLight::getAttenuationParameters() const {
	return uniforms.attenuation;
}

Vector2f SysDeferredSpotLight::getZRange() const {
	// 3 points can define z range of light - the origin and the two extents of the cone cap
	float z0 = uniforms.viewSpacePosition.z;
	float z1 = uniforms.viewSpacePosition.z + uniforms.viewSpaceDirection.z*uniforms.radius;
	float z2 = z1;
	// we are interested in what part of the cap is furthest in each z direction
	// (call "direction" x,y,z from now on)
	// we consider the case where the cap is of unit radius and then multiply by its radius
	// if we "rotate" the cap about the z axis so that it is only "tilted" on one axis, we get the following:
	//            /|
	// +z -------/=|
	//          /
	// the "=" distance is what we want to get. we can see that the | distance is the projection of
	// the direction vector onto the z-axis, which is just z. therefore since the radius of the cap is 1,
	// we solve for "=" to get sqrt( 1 - z^2 )
	float proj = uniforms.tanAngle * uniforms.radius * sqrt( 1.0f - uniforms.viewSpaceDirection.z * uniforms.viewSpaceDirection.z );
	z1 += proj;
	z2 -= proj;
	return Vector2f( std::min( z0, std::min( z1, z2 ) ), std::max( z0, std::max( z1, z2 ) ) );
}

void SysDeferredSpotLight::computeShadowMapRange( fastdelegate::FastDelegate0 <const SysAabb*> nextAabb ) {
	float aabbNear = std::numeric_limits <float>::max();
	float aabbFar = 0.0f;
	float range2 = range*range;
	const SysAabb * aabb;
	SysFrustum cullingFrustum = computeCullingFrustum();
	while ((aabb = nextAabb()) != NULL) {
		if (!cullingFrustum.contains( *aabb ))
			continue;
		// find the closest point on/in the box
		Vector3f closest = vecMin( position, aabb->maxBound );
		closest = vecMax( closest, aabb->minBound );
		const Vector3f * bounds[2] = { &aabb->minBound, &aabb->maxBound };
		float aabbNearDist2 = (position - closest).magnitudeSquared();

		Vector3f middle = (aabb->minBound + aabb->maxBound)*0.5f;
		// find the furthest point on the box
		Vector3f farthest(
			bounds[position.x < middle.x]->x,
			bounds[position.y < middle.y]->y,
			bounds[position.z < middle.z]->z );
		float aabbFarDist2 = (position - farthest).magnitudeSquared();

		// if the closest point is further than the range, don't expand far bound
		if (aabbNearDist2 <= range2)
			aabbFar = std::max( aabbFar, aabbFarDist2 );
		aabbNear = std::min( aabbNear, aabbNearDist2 );
	}
	if (aabbNear > aabbFar)
		// no objects in range; set default near/far
		uniforms.lightNearFar.set( shadowMinNear, range );
	else
		// far range is whichever is smaller: light radius or furthest object
		uniforms.lightNearFar.set( sqrt( aabbNear ), std::min( range, sqrt( aabbFar ) ) );
	uniforms.lightNearFar[1] *= FAR_RANGE_MULTIPLIER;
	// clamp min/max
	uniforms.lightNearFar[0] = std::max( uniforms.lightNearFar[0], shadowMinNear );
	uniforms.lightNearFar[1] = std::min( uniforms.lightNearFar[1], shadowMaxFar );
	projectionMatrix = computeProjectionMatrix();
}

void SysDeferredSpotLight::drawShadowMap(
	fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
	fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject ) {
	if (shadowMap.get() == NULL)
		return;

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)shadowMap.get()->getWidth(), (int)shadowMap.get()->getHeight() ) );
	vpState.bind();

	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->detatch( GL_COLOR_ATTACHMENT0 );
	fb->detatch( GL_COLOR_ATTACHMENT1 );
	fb->detatch( GL_COLOR_ATTACHMENT2 );
	fb->detatch( GL_COLOR_ATTACHMENT3 );
	fb->attach( GL_DEPTH_ATTACHMENT, shadowMap );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	dbState.setDrawBuffers( 0, NULL );
	dbState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthWriteEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.bind();

	GpuClearCall clearCall = Context::get().gpuState.createClearCall();
	clearCall.setClearDepth( 1.0f );
	clearCall.setClearBuffers( GL_DEPTH_BUFFER_BIT );
	clearCall.clear();

	ShadowMapRenderer r;
	const SysAabb * aabb;
	while (r = nextObject(), (aabb = nextAabb()) != NULL)
		r( this, 0 );
}

void SysDeferredSpotLight::draw() {
	if (camera == NULL)
		return;

	uniforms.radius = range;
	uniforms.viewSpacePosition = camera->getViewMatrix().transformPoint( position );
	uniforms.viewSpaceDirection = camera->getViewMatrix().transformVector( direction );

	Vector2f bounds = getZRange();

	if (-bounds[1] > camera->getFar() || -bounds[0] < camera->getNear())
		return;

	// if the light intersects the far plane, we must draw the front faces
	// if the light intersects the near plane, we must draw the back faces
	// otherwise we can do either (or both using stencil test, but that is probably inefficient)
	bool frontFaces = -bounds[0] > camera->getFar();
	// if intersects BOTH planes (e.g. no attenuation) then we need to draw using a fullscreen quad
	bool quad = (frontFaces && -bounds[1] < camera->getNear());

	// renderer has already bound textures

	if (shadowMap.get() != NULL) {
		if (quad && quadShadowProgram->getStatus() != Resource::S_LOADED || coneShadowProgram->getStatus() != Resource::S_LOADED)
			return;
		shadowMap->bind( (size_t)uniforms.shadowMap );
		uniforms.viewToLightMatrix = lightMatrix * camera->getCameraMatrix();
	} else {
		if (quad && quadProgram->getStatus() != Resource::S_LOADED || coneProgram->getStatus() != Resource::S_LOADED)
			return;
	}

	if (quad) {
		uniforms.imagePlaneHalfSize.y = tan( degToRad( camera->getFov() ) * 0.5f );
		uniforms.imagePlaneHalfSize.x = uniforms.imagePlaneHalfSize.y * camera->getAspectRatio();

		GpuRasterizerState rState = Context::get().gpuState.getCurrentRasterizerState();
		rState.setCullingEnabled( false );
		rState.bind();

		GpuDepthStencilState dsState = Context::get().gpuState.getCurrentDepthStencilState();
		dsState.setDepthTestEnabled( false );
		dsState.bind();

		SysRenderer2D::get().getQuadVertexBuffer()->bind();
		autoQuadProgram.bind();

		SysRenderer2D::get().drawSingleQuad();
	} else {
		uniforms.modelViewMatrix = camera->getModelViewMatrix( coneMatrix );
		uniforms.projectionMatrix = camera->getProjectionMatrix();
		uniforms.screenSize = (Vector2f)Context::get().gpuState.getCurrentViewportState().getViewportSize();

		GpuRasterizerState rState = Context::get().gpuState.getCurrentRasterizerState();
		rState.setCullingEnabled( true );

		GpuDepthStencilState dsState = Context::get().gpuState.getCurrentDepthStencilState();
		dsState.setDepthTestEnabled( true );

		if (frontFaces) {
			rState.setCullMode( GL_BACK );
			dsState.setDepthFunction( GL_LEQUAL );
		} else {
			rState.setCullMode( GL_FRONT );
			dsState.setDepthFunction( GL_GEQUAL );
		}

		rState.bind();
		dsState.bind();

		SysRendererDeferredLighting::get().getLightVolumeVertexBuffer()->bind();
		SysRendererDeferredLighting::get().getLightVolumeIndexBuffer()->bind();

		autoConeProgram.bind();

		GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
		size_t vertexCount = SysRendererDeferredLighting::get().getCone().getVertices().size();
		size_t indexCount = SysRendererDeferredLighting::get().getCone().getIndices().size();
		size_t indexOffset = SysRendererDeferredLighting::get().getConeIndexOffset();
		drawCall.setParameters(
			GL_TRIANGLES, 0, vertexCount-1, indexCount, GL_UNSIGNED_SHORT, bufferOffset( indexOffset ) );
		drawCall.draw();
	}
}

void SysDeferredSpotLight::drawGeometry() {
	if (geomProgram->getStatus() != Resource::S_LOADED || camera == NULL)
		return;

	uniforms.radius = range;
	uniforms.viewSpacePosition = camera->getViewMatrix().transformPoint( position );
	uniforms.viewSpaceDirection = camera->getViewMatrix().transformVector( direction );

	Vector2f bounds = getZRange();

	if (-bounds[1] > camera->getFar() || -bounds[0] < camera->getNear())
		return;

	// if the light intersects the far plane, we must draw the front faces
	// if the light intersects the near plane, we must draw the back faces
	// otherwise we can do either (or both using stencil test, but that is probably inefficient)
	bool frontFaces = -bounds[0] > camera->getFar();
	// if intersects BOTH planes (e.g. no attenuation) then we need to draw using a fullscreen quad
	bool quad = (frontFaces && -bounds[1] < camera->getNear());

	if (quad)
		return;

	// just reuse this for mvp
	uniforms.projectionMatrix = camera->getModelViewProjectionMatrix( coneMatrix );

	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( false );
	rState.setPolygonMode( GL_LINE );
	rState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.bind();

	SysRendererDeferredLighting::get().getLightVolumeVertexBuffer()->bind();
	SysRendererDeferredLighting::get().getLightVolumeIndexBuffer()->bind();

	autoGeomProgram.bind();

	GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
	size_t vertexCount = SysRendererDeferredLighting::get().getCone().getVertices().size();
	size_t indexCount = SysRendererDeferredLighting::get().getCone().getIndices().size();
	size_t indexOffset = SysRendererDeferredLighting::get().getConeIndexOffset();
	drawCall.setParameters(
		GL_TRIANGLES, 0, vertexCount-1, indexCount, GL_UNSIGNED_SHORT, bufferOffset( indexOffset ) );
	drawCall.draw();

	rState.setPolygonMode( GL_FILL );
	rState.bind();
}