#if 0

/** @file NetworkIdLayer.h
 *  @brief The high level networking layer.
 */

#ifndef NETWORKIDLAYER_DEFINED
#define NETWORKIDLAYER_DEFINED

#include "Common.h"
#include "NetworkMessageLayer.h"
#include "Indexer.h"
#include "HashMap.h"
#include "NetworkReader.h"
#include "NetworkWriter.h"
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <vector>

/** @brief The type of function to handle network ID messages.
 */
typedef boost::function <void ( NetworkReader & reader )> NetworkIdHandler;

/** @brief The high level networking layer.
 *
 *  The ID layer provides the functionality of creating
 *  IDs that are consistent across the network.
 */
class NetworkIdLayer : private boost::noncopyable {
	NetworkMessageLayer messageLayer;	/**< The message layer.*/

	bool initialized;					/**< Whether the ID layer is initialized.*/

	/** @brief A connection in the message layer.
	 */
	struct Connection {
		size_t messageLayerId;	/**< The ID of this connection in the message layer.*/
	};

	Indexer <Connection> connections;							/**< The list of current connections.*/
	HashMap <Hash <size_t>, size_t, size_t> connectionsMap;		/**< Maps message layer connection IDs to ID layer connection IDs.*/
	void acceptNewConnection( size_t messageLayerId );			/**< Called when the message layer accepts a new connection.*/
	boost::function <void ( size_t )> onAcceptNewConnection;	/**< Called when a new connection is accepted if new connection acceptance is enabled.*/
	void connectionTimeout( size_t messageLayerId );			/**< @brier Called when a connection on the message layer times out.*/
	boost::function <void ( size_t )> onConnectionTimeout;		/**< Called when a connection times out.*/

	/** @brief Called when a message is received.
	 */
	void receiveMessage( size_t messageLayerId, const byte * data, size_t dataSize );

	/** @brief A map of functions which handle various network IDs.
	 */
	HashMap <Hash <ushort>, ushort, NetworkIdHandler> networkIdHandlers;

	static const size_t NETWORK_ID_HANDLERS_SIZE = 300;	/**< Size of network ID handler table.*/

public:
	NetworkIdLayer();		/**< The constructor.*/
	~NetworkIdLayer();		/**< The destructor.*/

	bool init( ushort port );	/**< Initializes the ID, message, and data layers.*/
	void term();				/**< Terminates the ID, message, and data layers.*/

	/** @brief Enables acceptance of new connections.
	 *  @param onAcceptNewConnectionFn	Called when a new connection is accepted, passing the new connection's ID.
	 */
	void enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn );

	/** @brief Disables acceptance of new connections.
	 */
	void disableAcceptNewConnections();

	/** @brief Sets the function to be called when a connection times out.
	 */
	void setOnConnectionTimeout( const boost::function <void ( size_t )> & onConnectionTimeoutFn );

	/** @brief Creates a new connection.
	 */
	bool createConnection( const std::string & address, ushort port, size_t & newConnectionId );

	/** @brief Destroys a connection.
	 */
	void destroyConnection( size_t connectionId );

	/** @brief Receives and processes incoming data.
	 */
	void receiveAndProcess();

	/** @brief Queues a message with the given network ID.
	 */
	bool queueMessage( size_t connectionId, ushort networkId, NetworkWriter & writer );

	/** @brief Queues a guaranteed message with the given network ID.
	 */
	bool queueGuaranteedMessage( size_t connectionId, ushort networkId, NetworkWriter & writer );

	/** @brief Sends all queued messages.
	 */
	void sendQueued();

	/** @brief Adds or replaced a network ID handler for the given ID.
	 */
	bool addNetworkIdHandler( ushort networkId, const NetworkIdHandler & handler );

	/** @brief Removes the network ID handler for the given ID.
	 */
	bool removeNetworkIdHandler( ushort networkId );

	/** @brief Returns whether network ID handler for the given ID exists.
	 */
	bool networkIdHandlerExists( ushort networkId ) const;

	/** @brief Returns the number of network IDs which currently have handlers.
	 */
	size_t networkIdHandlerCount() const;
};

#endif

#endif