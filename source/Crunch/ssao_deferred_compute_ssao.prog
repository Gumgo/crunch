#vertex_shader
#version 330

in vec2 corner;

uniform vec2 imagePlaneHalfSize; // size at depth = 1

out vec2 uv;
out vec3 ray;

void main() {
	uv = corner;
	vec2 clip = corner * 2.0 - vec2( 1.0, 1.0 );
	ray = vec3( clip * imagePlaneHalfSize, -1.0 );
	gl_Position = vec4( corner * 2.0 - vec2( 1.0, 1.0 ), 0.0, 1.0 );
}

#fragment_shader
#version 330
precision highp float;

in vec2 uv;
in vec3 ray;

uniform sampler2D normalSpecMap;
uniform sampler2D depthMap;
uniform sampler2D rotationMap;
uniform vec2 sizeOverRotationMapSize;
uniform float worldRadius;
uniform mat4 projectionMatrix;

out vec4 outSsao;

#include "deferred_helpers.prog"

const int KERNEL_SIZE = 8;

// kernel of points to sample
const vec3 KERNEL[KERNEL_SIZE] = {
	vec3( 0.0182256, -0.0963151, 0.0197793 ),
	vec3( -0.103855, -0.00615684, 0.04676 ),
	vec3( 0.0560084, 0.144342, 0.021034 ),
	vec3( 0.153351, 0.0180027, 0.165801 ),
	vec3( -0.265085, -0.161995, 0.0954616 ),
	vec3( 0.0611778, 0.440917, 0.075881 ),
	vec3( 0.0869685, -0.363851, 0.477062 ),
	vec3( -0.00144953, -0.199054, 0.763541 )
};

void main() {
	vec4 normalSpecData = texture( normalSpecMap, uv );
	vec4 depthData = texture( depthMap, uv );
	vec3 normal = unpackNormal( normalSpecData );
	float depth = unpackDepth( depthData );

	vec3 position = depth * ray;
	vec3 rotation = vec3( texture( rotationMap, uv * sizeOverRotationMapSize ).rg, 0.0 );
	vec3 tangent = rotation - normal * dot( rotation, normal );
	// if rotation and normal are the same, rotate rotation by 90 degrees
	tangent = (tangent == vec3( 0.0 )) ? vec3( -rotation.y, rotation.x, 0.0 ) : normalize( tangent );
	vec3 binormal = cross( normal, tangent );
	mat3 tbnMatrix = mat3( tangent, binormal, normal );

	float occlusion = 0.0;
	for (int i = 0; i < KERNEL_SIZE; ++i) {
		vec3 samplePosition = position + tbnMatrix * KERNEL[i] * worldRadius;
		vec4 sampleProj = (vec4( samplePosition, 1.0 ) * projectionMatrix);
		vec2 sampleUv = 0.5 * (sampleProj.xy / sampleProj.w) + vec2( 0.5 );
		float sampleZ = -unpackDepth( texture( depthMap, sampleUv ) );

		// only contribute to occlusion if depth is in range
		float inRange = abs( position.z - sampleZ ) < worldRadius ? 1.0 : 0.0;
		// occlude if the sample position is BEHIND the queried z (i.e. "inside" the geometry)
		float occluded = (samplePosition.z <= sampleZ) ? 1.0 : 0.0;
		occlusion += inRange * occluded;
	}
	occlusion /= float( KERNEL_SIZE );

	outSsao = vec4( occlusion, occlusion, occlusion, 0.0 );
}