#ifndef SYSTONEMAP_DEFINED
#define SYSTONEMAP_DEFINED

#include "Common.h"
#include "GpuTexture.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"
#include <vector>

// performs tonemapping on an HDR image
class SysTonemap : private boost::noncopyable {
public:
	SysTonemap();
	~SysTonemap();

	void setupTonemap(
		size_t w, size_t h,
		float expAdjustRate = 0.1f, float whiteLuma = 2.0f,
		const Vector2f & lumaBounds = Vector2f( 0.5f, 100.0f ) );

	// performs tonemapping
	// frames = number of frames since last update - rate is adjusted for how many frames were skipped
	// source and dest should be RGBA16F
	bool tonemap( uint frames, bool resetExposure, GpuTextureReference & source, GpuTextureReference & dest );

private:
	ProgramResourceReference downsample1Program;	// first downsample pass
	ProgramResourceReference downsample2Program;	// following downsample passes
	ProgramResourceReference adjustExposureProgram;	// adjust the current exposure
	ProgramResourceReference applyTonemapProgram;	// apply tonemapping
	bool ready;

	SysAutoProgram downsample1Auto;
	SysAutoProgram downsample2Auto;
	SysAutoProgram adjustExposureAuto;
	SysAutoProgram applyTonemapAuto;

	struct {
		int source;
		Vector2f scale;		// used to adjust UVs when for non-even texture downsampling
		Vector2f offset;	// sampling offsets
		float boost;		// since some black regions will be sampled, this boosts
	} downsampleUniforms; // shared between 1 and 2

	struct {
		int logLuma;
		Vector2f lumaBounds;
	} adjustExposureUniforms;

	struct {
		int source;
		int averageLuma;
		float whiteLuma2; // "white" value squared
	} applyTonemapUniforms;

	// downsample 2x first pass (since we can't linear blend), 4x following passes
	std::vector <GpuTextureReference> downsampleBuffers;

	// holds current average luminance
	GpuTextureReference currentAverageLuminance;

	size_t width, height;

	bool firstFrame;
	float exposureAdjustmentRate;

	void downsample( GpuTextureReference & source );
	void adjustExposure( uint frames );
	void applyTonemap( GpuTextureReference & source, GpuTextureReference & dest );
};

#endif