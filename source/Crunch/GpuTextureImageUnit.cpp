#include "GpuTextureImageUnit.h"
#include "GpuState.h"

void GpuTextureImageUnit::setActive() {
	getGpuState()->setActiveTextureImageUnitIndex( index );
}

bool GpuTextureImageUnit::isActive() const {
	return index == getGpuState()->getActiveTextureImageUnitIndex();
}

void GpuTextureImageUnit::bindTexture( const GpuTextureReference & texture ) {
	TextureBindingLocation loc = getTextureBindingLocation( texture->getType() );
	if (textureBindingLocations[loc].get() == texture.get())
		// already bound
		return;

	setActive();
	glBindTexture( texture->getType(), texture->getId() );
	textureBindingLocations[loc] = texture;
}

void GpuTextureImageUnit::unbindTexture( GLenum location ) {
	TextureBindingLocation loc = getTextureBindingLocation( location );
	if (!textureBindingLocations[loc])
		// already unbound
		return;

	setActive();
	glBindTexture( loc, 0 );
	textureBindingLocations[loc] = GpuTextureReference();
};

GpuTextureReference GpuTextureImageUnit::getBoundTexture( GLenum location ) {
	return textureBindingLocations[getTextureBindingLocation( location )];
}

GpuTextureConstReference GpuTextureImageUnit::getBoundTexture( GLenum location ) const {
	return textureBindingLocations[getTextureBindingLocation( location )];
}

GpuTextureImageUnit::GpuTextureImageUnit( GpuState * state, size_t i )
	: ReferenceCountedGpuObject( state )
	, index( i ) {
}

void GpuTextureImageUnit::onRelease() const {
	// no more references
	delete this;
}

GpuTextureImageUnit::TextureBindingLocation GpuTextureImageUnit::getTextureBindingLocation( GLenum loc ) {
	switch (loc) {
	case GL_TEXTURE_1D:
		return TBL_TEXTURE_1D;
	case GL_TEXTURE_1D_ARRAY:
		return TBL_TEXTURE_1D_ARRAY;
	case GL_TEXTURE_2D:
		return TBL_TEXTURE_2D;
	case GL_TEXTURE_2D_ARRAY:
		return TBL_TEXTURE_2D_ARRAY;
	case GL_TEXTURE_2D_MULTISAMPLE:
		return TBL_TEXTURE_2D_MULTISAMPLE;
	case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
		return TBL_TEXTURE_2D_MULTISAMPLE_ARRAY;
	case GL_TEXTURE_3D:
		return TBL_TEXTURE_3D;
	case GL_TEXTURE_BUFFER:
		return TBL_TEXTURE_BUFFER;
	case GL_TEXTURE_CUBE_MAP:
		return TBL_TEXTURE_CUBE_MAP;
	case GL_TEXTURE_CUBE_MAP_ARRAY:
		return TBL_TEXTURE_CUBE_MAP_ARRAY;
	case GL_TEXTURE_RECTANGLE:
		return TBL_TEXTURE_RECTANGLE;
	default:
		throw std::runtime_error( "Invalid texture binding location" );
	}
}