#include "SysRenderer2D.h"
#include "Context.h"

SysRenderer2D * SysRenderer2D::sysRenderer2D = NULL;

SysRenderer2D & SysRenderer2D::get() {
	return *sysRenderer2D;
}

void SysRenderer2D::onCreate() {
	sysRenderer2D = this;

	// load all the programs
	struct Load {
		const char * name;
		ProgramResourceReference & program;
	};

	Load loadPrograms[] = {
		{ "sys___blit", blitProgram },
		{ "sys___sprite", spriteProgram },
		{ "sys___multisprite", multiSpriteProgram },
		{ "sys___rect", rectProgram },
		{ "sys___gradient", gradientProgram },
		{ "sys___text", textProgram },
		{ "sys___tiles", tilesProgram },
		{ "sys___warp", warpProgram }
#ifdef _DEBUG
		, { "sys___lines", linesProgram },
		{ "sys___capsule", capsuleProgram }
#endif
	};

	for (size_t i = 0; i < arraySize( loadPrograms ); ++i)
		loadPrograms[i].program = Context::get().resourceManager.acquire <ProgramResource>( loadPrograms[i].name, true );

	// allocate the buffers
	size_t bufferCount = std::max( MAX_MULTISPRITES, MAX_CHARACTERS ); // these two "overlap"
	std::vector <QuadVertex> vertexData( bufferCount * 4 );
	for (size_t i = 0; i < bufferCount; ++i) {
		vertexData[i*4  ].corner[0] = 0.0f;
		vertexData[i*4  ].corner[1] = 0.0f;
		vertexData[i*4+1].corner[0] = 1.0f;
		vertexData[i*4+1].corner[1] = 0.0f;
		vertexData[i*4+2].corner[0] = 0.0f;
		vertexData[i*4+2].corner[1] = 1.0f;
		vertexData[i*4+3].corner[0] = 1.0f;
		vertexData[i*4+3].corner[1] = 1.0f;
	}

	std::vector <ushort> indexData( bufferCount * 6 );
	for (size_t i = 0; i < bufferCount; ++i) {
		indexData[i*6  ] = i*4;
		indexData[i*6+1] = i*4 + 1;
		indexData[i*6+2] = i*4 + 2;
		indexData[i*6+3] = i*4 + 1;
		indexData[i*6+4] = i*4 + 3;
		indexData[i*6+5] = i*4 + 2;
	}

	GpuBuffer::BufferDescription vDesc;
	vDesc.type = GL_ARRAY_BUFFER;
	vDesc.size = vertexData.size() * sizeof( vertexData[0] );
	vDesc.data = &vertexData[0];
	vDesc.usage = GL_STATIC_DRAW;
	vertexBuffer = Context::get().gpuState.createBuffer( vDesc );

	GpuBuffer::BufferDescription iDesc;
	iDesc.type = GL_ELEMENT_ARRAY_BUFFER;
	iDesc.size = indexData.size() * sizeof( indexData[0] );
	iDesc.data = &indexData[0];
	iDesc.usage = GL_STATIC_DRAW;
	indexBuffer = Context::get().gpuState.createBuffer( iDesc );

	// no need for culling in 2D (makes drawing things simpler)
	// no depth buffer
	rasterizerState = Context::get().gpuState.createRasterizerState();
	depthStencilState = Context::get().gpuState.createDepthStencilState();
	rasterizerState.setCullingEnabled( false );
	depthStencilState.setDepthTestEnabled( false );
	depthStencilState.setDepthWriteEnabled( false );

	begin2DPassCall.bind( this, &SysRenderer2D::begin2DPass );
	begin2DPassKey = Renderer::makeKeyFirst( PASS-1 );
	blitCall.bind( this, &SysRenderer2D::blit );
	blitKey = Renderer::makeKeyLast( 255 );
}

void SysRenderer2D::onDestroy() {
	sysRenderer2D = NULL;
}

void SysRenderer2D::onDraw() {
	Context::get().renderer.submit( begin2DPassCall, begin2DPassKey );
	if (blitTexture || !getBlitTexture.empty())
		Context::get().renderer.submit( blitCall, blitKey );
}

void SysRenderer2D::setBlitTexture( GpuTextureReference & t ) {
	getBlitTexture.clear();
	blitTexture = t;
}

void SysRenderer2D::setBlitTexture( fastdelegate::FastDelegate0 <GpuTextureReference> f ) {
	blitTexture = GpuTextureReference();
	getBlitTexture = f;
}

void SysRenderer2D::begin2DPass() {
	rasterizerState.bind();
	depthStencilState.bind();
}

void SysRenderer2D::blit() {
	Context::get().gpuState.unbindFramebuffer( GL_FRAMEBUFFER );
	rasterizerState.bind();

	vertexBuffer->bind();

	GpuTextureReference bt = (!blitTexture) ? getBlitTexture() : blitTexture;
	static const int TEX_UNIT = 0;
	bt->bind( TEX_UNIT );

	GpuProgram * bp = blitProgram->getProgram();
	bp->bind();
	bp->setUniform( "framebuffer", GL_SAMPLER_2D, &TEX_UNIT );
	bp->setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( QuadVertex ),
		bufferOffset( offsetof( QuadVertex, corner ) ) );
	// can't seem to render attribute-less? annoying!

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)bt->getWidth(), (int)bt->getHeight() ) );
	vpState.bind();

	drawSingleQuad();
}

GpuBufferReference SysRenderer2D::getQuadVertexBuffer() {
	return vertexBuffer;
}

GpuBufferReference SysRenderer2D::getQuadIndexBuffer() {
	return indexBuffer;
}

void SysRenderer2D::drawSingleQuad() {
	GpuDrawCall drawCall = Context::get().gpuState.createDrawCall();
	drawCall.setParameters( GL_TRIANGLE_STRIP, 0, 4 );
	drawCall.draw();
}

void SysRenderer2D::drawSprite(
	GpuTextureReference & texture,
	const Matrix44f & mvpMatrix,
	const Vector4f & color,
	const Vector2f & origin,
	const Vector2f & pixelMin,
	const Vector2f & pixelMax ) {

	vertexBuffer->bind();
	static const int TEX_UNIT = 0;
	texture->bind( (size_t)TEX_UNIT );

	GpuProgram * sp = spriteProgram->getProgram();
	sp->bind();
	Vector4f pixelMinMax( pixelMin, pixelMax );
	Vector4f sizeOrigin( (float)texture->getWidth(), (float)texture->getHeight(), origin );
	sp->setUniform( "pixelMinMax", GL_FLOAT_VEC4, &pixelMinMax.x );
	sp->setUniform( "sizeOrigin", GL_FLOAT_VEC4, &sizeOrigin.x );
	sp->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );
	sp->setUniform( "color", GL_FLOAT_VEC4, &color.x );
	sp->setUniform( "sampler", GL_SAMPLER_2D, &TEX_UNIT );

	sp->setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( QuadVertex ),
		bufferOffset( offsetof( QuadVertex, corner ) ) );

	drawSingleQuad();
}

void SysRenderer2D::drawSprite(
	TextureResource & texture,
	const Matrix44f & mvpMatrix,
	const Vector4f & color,
	const Vector2f & origin,
	const Vector2f & pixelMin,
	const Vector2f & pixelMax ) {
	drawSprite( texture.getTexture(), mvpMatrix, color, origin, pixelMin, pixelMax );
}

void SysRenderer2D::drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix ) {
	drawSprite(
		texture,
		mvpMatrix,
		Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ),
		Vector2f( 0.0f, 0.0f ),
		Vector2f( 0.0f, 0.0f ),
		Vector2f( (float)texture.getTexture()->getWidth(), (float)texture.getTexture()->getHeight() ) );
}

void SysRenderer2D::drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector4f & color ) {
	drawSprite(
		texture,
		mvpMatrix,
		color,
		Vector2f( 0.0f, 0.0f ),
		Vector2f( 0.0f, 0.0f ),
		Vector2f( (float)texture.getTexture()->getWidth(), (float)texture.getTexture()->getHeight() ) );
}

void SysRenderer2D::drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector2f & origin ) {
	drawSprite(
		texture,
		mvpMatrix,
		Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ),
		origin,
		Vector2f( 0.0f, 0.0f ),
		Vector2f( (float)texture.getTexture()->getWidth(), (float)texture.getTexture()->getHeight() ) );
}

void SysRenderer2D::drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector4f & color, const Vector2f & origin ) {
	drawSprite(
		texture,
		mvpMatrix,
		color,
		origin,
		Vector2f( 0.0f, 0.0f ),
		Vector2f( (float)texture.getTexture()->getWidth(), (float)texture.getTexture()->getHeight() ) );
}

void SysRenderer2D::drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector2f & pixelMin, const Vector2f & pixelMax ) {
	drawSprite(
		texture,
		mvpMatrix,
		Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ),
		Vector2f( 0.0f, 0.0f ),
		pixelMin,
		pixelMax );
}

void SysRenderer2D::drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector4f & color, const Vector2f & pixelMin, const Vector2f & pixelMax ) {
	drawSprite(
		texture,
		mvpMatrix,
		color,
		Vector2f( 0.0f, 0.0f ),
		pixelMin,
		pixelMax );
}

void SysRenderer2D::drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector2f & origin, const Vector2f & pixelMin, const Vector2f & pixelMax ) {
	drawSprite(
		texture,
		mvpMatrix,
		Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ),
		origin,
		pixelMin,
		pixelMax );
}

void SysRenderer2D::drawMultiSprite( TextureResource & texture, const Matrix44f & vpMatrix, const Vector4f & color, size_t count, const Vector2f * origins, const Vector2f * pixelMins, const Vector2f * pixelMaxes, const Matrix33af * modelMatrices ) {
	if (count == 0)
		return;

	float pixelMinsMaxes[MAX_MULTISPRITES * 4];

	size_t ct = std::min( count, MAX_MULTISPRITES );
	for (size_t i = 0; i < ct; ++i) {
		size_t idx = i * 4;
		pixelMinsMaxes[idx  ] = pixelMins[i].x;
		pixelMinsMaxes[idx+1] = pixelMins[i].y;
		pixelMinsMaxes[idx+2] = pixelMaxes[i].x;
		pixelMinsMaxes[idx+3] = pixelMaxes[i].y;
	}

	vertexBuffer->bind();
	indexBuffer->bind();
	static const int TEX_UNIT = 0;
	texture.getTexture()->bind( (size_t)TEX_UNIT );

	GpuProgram * msp = multiSpriteProgram->getProgram();
	msp->bind();
	msp->setUniform( "pixelMinsMaxes", GL_FLOAT_VEC4, pixelMinsMaxes, 0, ct );
	msp->setUniform( "origins", GL_FLOAT_VEC2, origins, 0, ct );
	msp->setUniform( "modelMatrices", GL_FLOAT_MAT2x3, modelMatrices, 0, ct );
	Vector2f size( (float)texture.getTexture()->getWidth(), (float)texture.getTexture()->getWidth() );
	msp->setUniform( "size", GL_FLOAT_VEC2, &size.x );
	msp->setUniform( "vpMatrix", GL_FLOAT_MAT4, &vpMatrix );
	msp->setUniform( "color", GL_FLOAT_VEC4, &color.x );
	msp->setUniform( "sampler", GL_SAMPLER_2D, &TEX_UNIT );

	msp->setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( QuadVertex ),
		bufferOffset( offsetof( QuadVertex, corner ) ) );

	GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
	drawCall.setParameters( GL_TRIANGLES, 0, ct*4-1, ct*6, GL_UNSIGNED_SHORT, bufferOffset( 0 ) );
	drawCall.draw();
}

void SysRenderer2D::drawRect( const Matrix44f & mvpMatrix, const Vector2f & position, const Vector2f & size, const Vector4f & color ) {
	vertexBuffer->bind();

	GpuProgram * rp = rectProgram->getProgram();
	rp->bind();
	Vector4f originSize( position, size );
	rp->setUniform( "originSize", GL_FLOAT_VEC4, &originSize.x );
	rp->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );
	rp->setUniform( "color", GL_FLOAT_VEC4, &color.x );

	rp->setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( QuadVertex ),
		bufferOffset( offsetof( QuadVertex, corner ) ) );

	drawSingleQuad();
}

void SysRenderer2D::drawGradient( const Matrix44f & mvpMatrix, const Vector2f & position, const Vector2f & size, const Vector4f & topLeftColor, const Vector4f & topRightColor, const Vector4f & bottomLeftColor, const Vector4f & bottomRightColor ) {
	vertexBuffer->bind();

	GpuProgram * gp = gradientProgram->getProgram();
	gp->bind();
	Vector4f originSize( position, size );
	gp->setUniform( "originSize", GL_FLOAT_VEC4, &originSize.x );
	gp->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );
	gp->setUniform( "colorTL", GL_FLOAT_VEC4, &topLeftColor.x );
	gp->setUniform( "colorTR", GL_FLOAT_VEC4, &topRightColor.x );
	gp->setUniform( "colorBL", GL_FLOAT_VEC4, &bottomLeftColor.x );
	gp->setUniform( "colorBR", GL_FLOAT_VEC4, &bottomRightColor.x );

	gp->setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( QuadVertex ),
		bufferOffset( offsetof( QuadVertex, corner ) ) );

	drawSingleQuad();
}

void SysRenderer2D::drawGradientHoriz( const Matrix44f & mvpMatrix, const Vector2f & position, const Vector2f & size, const Vector4f & leftColor, const Vector4f & rightColor ) {
	drawGradient( mvpMatrix, position, size, leftColor, rightColor, leftColor, rightColor );
}

void SysRenderer2D::drawGradientVert( const Matrix44f & mvpMatrix, const Vector2f & position, const Vector2f & size, const Vector4f & topColor, const Vector4f & bottomColor ) {
	drawGradient( mvpMatrix, position, size, topColor, topColor, bottomColor, bottomColor );
}

void SysRenderer2D::drawText( const std::string & text, FontResource & font, const Matrix44f & mvpMatrix, TextAlignHoriz horiz, TextAlignVert vert, const Vector4f & color ) {
	// USES OF THIS METHOD SHOULD BE REPLACED WITH THE ONE BELOW
	// KEPT IN SO THAT OLD CODE STILL COMPILES
}

void SysRenderer2D::drawText( SysText & text, const Matrix44f & mvpMatrix, const Vector4f & color ) {
	if (text.getVertexCount() == 0)
		return;

	GpuBufferReference textVertices = text.getVertices();
	textVertices->bind();

	static const int TEX_UNIT = 0;
	GpuTextureReference tex = text.getFont()->getTexture();
	tex->bind( (size_t)TEX_UNIT );

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_ALPHA );
	blendState.bind();

	GpuProgram * tp = textProgram->getProgram();
	tp->bind();

	tp->setAttribute( "position", textVertices, 2, GL_FLOAT, false, false, sizeof( SysText::Vertex ),
		bufferOffset( offsetof( SysText::Vertex, position ) ) );
	tp->setAttribute( "uv", textVertices, 2, GL_FLOAT, false, false, sizeof( SysText::Vertex ),
		bufferOffset( offsetof( SysText::Vertex, uv ) ) );

	tp->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );
	tp->setUniform( "font", GL_SAMPLER_2D, &TEX_UNIT );
	tp->setUniform( "color", GL_FLOAT_VEC4, &color.x );

	GpuDrawCall drawCall = Context::get().gpuState.createDrawCall();
	drawCall.setParameters( GL_TRIANGLES, 0, text.getVertexCount() );
	drawCall.draw();
}

void SysRenderer2D::drawWarp(
	GpuTextureReference & secondaryRenderTarget,
	const Matrix33af & viewMatrix,
	const Matrix44f & projectionMatrix,
	const Vector2f & center,
	float radius,
	float power ) {

	if (radius <= 0.0f)
		return;

	if (power < 0.0f)
		power = 1.0f / (-power + 1.0f);
	else
		power += 1.0f;

	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	if (!fb) {
		Log::warning() << "Warp effect requires framebuffer";
		return;
	}

	GpuTextureReference primaryRenderTarget = fb->getAttachedTexture( GL_COLOR_ATTACHMENT0 );
	if (!primaryRenderTarget) {
		Log::warning() << "Warp effect requires render target to be a texture";
		return;
	}

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	// sample from screen, draw onto secondary buffer
	fb->attach( GL_COLOR_ATTACHMENT0, secondaryRenderTarget );

	vertexBuffer->bind();
	static const int TEX_UNIT = 0;
	primaryRenderTarget->bind( (size_t)TEX_UNIT );

	GpuProgram * wp = warpProgram->getProgram();
	wp->bind();
	Vector3f radIRadPow( radius, 1.0f / radius, power );
	Vector2f viewSize( (float)primaryRenderTarget->getWidth(), (float)primaryRenderTarget->getHeight() );
	Vector2f centerViewSpace = viewMatrix.transformPoint( center );
	wp->setUniform( "ctr", GL_FLOAT_VEC2, &centerViewSpace.x );
	wp->setUniform( "radIRadPow", GL_FLOAT_VEC3, &radIRadPow.x );
	wp->setUniform( "viewSize", GL_FLOAT_VEC2, &viewSize.x );
	wp->setUniform( "projectionMatrix", GL_FLOAT_VEC4, &projectionMatrix );
	wp->setUniform( "sampler", GL_SAMPLER_2D, &TEX_UNIT );

	wp->setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( QuadVertex ),
		bufferOffset( offsetof( QuadVertex, corner ) ) );

	drawSingleQuad();

	// flip back to the original buffer
	fb->attach( GL_COLOR_ATTACHMENT0, primaryRenderTarget );

	// draw the warp back onto it
	drawSprite(
		secondaryRenderTarget,
		projectionMatrix, Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ), -(centerViewSpace - Vector2f( radius, radius )),
		centerViewSpace - Vector2f( radius, radius ), centerViewSpace + Vector2f( radius, radius ) );
}