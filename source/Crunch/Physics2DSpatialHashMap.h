/** @file Physics2DSpatialHashMap
 *  @brief Provides an implementation of spatial hashing for collision queries.
 */

#ifndef PHYSICS2DSPATIALHASHMAP_DEFINED
#define PHYSICS2DSPATIALHASHMAP_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Indexer.h"
#include "HashFunctions.h"
#include <vector>

namespace Physics2D {

/** @brief An implementation of spatial hashing for collision queries.
 */
class SpatialHashMap {
public:
	/** @brief Initializes the spatial hash map with the given bin dimensions.
	 */
	SpatialHashMap( Real binDim );

	size_t addShape( void * shape, const Aabb & aabb );	/**< Adds a shape with the given AABB and returns its index.*/
	void removeShape( size_t index );					/**< Removes the shape with the given index.*/
	void moveShape( size_t index, const Aabb & aabb );	/**< Moves the shape with the given index using the new AABB provided.*/
	void * getShape( size_t index ) const;				/**< Returns the shape with the given index.*/
	Aabb getAabb( size_t index ) const;					/**< Returns the AABB of the shape with the given index.*/

	/** @brief Returns whether the AABBs of the two given shapes overlap.
	 */
	bool queryOverlap( size_t index0, size_t index1 ) const;

	/** @brief Fills the vector with pairs of overlapping shapes.
	 */
	void queryOverlaps( std::vector <std::pair <size_t, size_t>> & overlaps ) const;

private:
	// stored when an index points to nothing
	static const size_t NO_INDEX = 0xFFFFFFFF;

	// information about a shape stored in the map
	struct ShapeInfo {
		void * shape;	// the shape being represented
		Aabb aabb;		// the AABB of the shape
	};

	// a bin in the hash table.
	struct Bin {
		Vector2i spatialIndex;	// the index of this bin in space
		size_t shapeNodes;		// the index of the first shape node
		size_t next;			// the index of the next bin, or NO_INDEX if no next bin
	};

	// a node in the shape list of a bin
	struct ShapeNode {
		size_t shapeIndex;	// the index to the shape information
		size_t next;		// the index of the next shape in the list, or NO_INDEX if no next node
	};

	Real binDimensions;				// the dimensions of each bin
	Real invBinDimensions;			// the inverse of the dimensions of each bin

	Indexer <ShapeInfo> shapeInfo;	// the list of shape information
	Indexer <Bin> bins;				// the list of bins
	Indexer <ShapeNode> shapeNodes;	// the list of shape nodes
	std::vector <size_t> table;		// the table of bins
	float invTableSize;				// used to avoid division in load factor computation

	static const size_t MIN_TABLE_SIZE = 20;	// the table cannot shrink smaller than this size
	static const float MIN_LOAD_FACTOR;			// the minimum load factor before the table size is halved
	static const float MAX_LOAD_FACTOR;			// the maximum load factor before the table size is doubled

	size_t hash( const Vector2i & spatialIndex ) const;	// hashes a spatial index
	void resizeIfNecessary();							// resizes the table if the load is too low or high

	// returns the bins that the AABB overlaps
	// the lower bound is inclusive and the upper bound is exclusive
	// if the upper bound lies on a grid boundary, only the lesser cell is included in the returned range
	std::pair <Vector2r, Vector2r> getBins( const Aabb & aabb ) const;

	void addToBin( size_t shapeIndex, const Vector2i & spatialIndex );		// adds the shape with the given index to the given bin
	void removeFromBin( size_t shapeIndex, const Vector2i & spatialIndex );	// removes the shape with the given index from the given bin
};

}

#endif