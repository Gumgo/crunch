#ifndef SYSDEFERREDLIGHT_DEFINED
#define SYSDEFERREDLIGHT_DEFINED

#include "Common.h"
#include "SysCamera3D.h"
#include "FastDelegate.h"

// base class for deferred lights
class SysDeferredLight : private boost::noncopyable {
public:
	enum LightType {
		LT_AMBIENT,
		LT_DIRECTIONAL,
		LT_POINT,
		LT_SPOT
	};

	// quadratic: a = 1/(x + yd + zd^2)
	//             / 1,                                  d < x
	// radial: a = | -2((y-d)/(y-x)^3 + 3((y-d)(y-x))^2, y < d < x
	//             \ 0,                                  d > y
	enum AttenuationMode {
		AM_QUADRATIC,
		AM_RADIAL
	};

	// function for drawing to shadow maps
	// receives an additional index parameter for multi-pass lights
	typedef fastdelegate::FastDelegate2 <SysDeferredLight*, size_t, void> ShadowMapRenderer;

	SysDeferredLight( LightType lt );

	void setCamera( const SysCamera3D * cam );

	LightType getLightType() const;

	// returns whether shadows are enabled
	bool areShadowsEnabled() const;
	// returns ith light matrix
	const Matrix44af & getLightMatrix( size_t i ) const;
	// returns ith projection matrix
	const Matrix44f & getProjectionMatrix( size_t i ) const;
	// computes the range of the shadow map before rendering
	// takes a function which returns AABBs with subsequent calls
	// nextAabb should return NULL when all AABBs have been iterated over
	// once NULL has been returned once, nextAabb should repeat from the beginning
	virtual void computeShadowMapRange(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb ) = 0;
	// draws the shadow map
	// nextAabb is same as above
	// takes a function which returns functions to draw each object with subsequent calls
	// nextObject should return a NULL function when all functions have been iterated over
	// once NULL has been returned once, nextObject should repeat from the beginning
	// nextObject and nextAabb should "line up"
	virtual void drawShadowMap(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
		fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject ) = 0;

	virtual void draw() = 0;

	// find parameters by providing distances and intensities
	static Vector3f findQuadraticAttenuationParameters( float d0, float i0, float d1, float i1 );
	static Vector3f findQuadraticAttenuationParameters( float d0, float i0, float d1, float i1, float d2, float i2 );

protected:
	static const float MIN_INTENSITY;
	static float luma( const Vector3f & color );
	static float computeQuadraticAttenuationRange( const Vector3f & color, const Vector3f & atten );

	const SysCamera3D * camera;

	// whether shadows are enabled
	bool shadowsEnabled;
	// sets up the pointers to light/projection matrices
	// if "multiple" is false, calling getLightMatrix( i ) will always return the 0th index, regardless of i
	// same for projection matrices
	// this is useful to save space if either light or projection matrices are duplicated
	void setupMatrices(
		const Matrix44af * lightMatrixPtr, bool multipleLightMatrices,
		const Matrix44f * projectionMatrixPtr, bool multipleProjectionMatrices );

private:
	LightType lightType;							// the type of light
	const Matrix44af * lightMatrixPointer;			// pointer to light matrices
	const Matrix44f * projectionMatrixPointer;		// pointer to projection matrices
	// when selecting matrices, the index is ANDed with these values
	// this is so the first matrix is always returned (if desired)
	size_t lightMatrixMask, projectionMatrixMask;
};

#endif