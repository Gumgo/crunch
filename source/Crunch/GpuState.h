/** @file GpuState.h
 *  @brief Keeps track of state on the GPU.
 */

#ifndef GPUSTATE_DEFINED
#define GPUSTATE_DEFINED

#include "Common.h"
#include "GpuObject.h"

#include "GpuBlendState.h"
#include "GpuDepthStencilState.h"
#include "GpuRasterizerState.h"
#include "GpuScissorState.h"
#include "GpuViewportState.h"
#include "GpuDrawBufferState.h"

#include "GpuTexture.h"
#include "GpuTextureImageUnit.h"
#include "GpuRenderbuffer.h"
#include "GpuFramebuffer.h"
#include "GpuBuffer.h"
#include "GpuProgram.h"
#include "GpuAttribute.h"
#include "GpuDrawCall.h"

#include <vector>

/** @brief Keeps track of the state on the GPU.
 */
class GpuState {
public:
	/** @brief Initializes all states to the current GPU state.
	 */
	void init();

	// BASIC STATE:

	/** @brief Creates a blend state.
	 */
	GpuBlendState createBlendState();

	/** @brief Binds the blend state.
	 */
	void bind( const GpuBlendState & newState );

	/** @brief Retrieves the current blend state.
	 */
	const GpuBlendState & getCurrentBlendState() const;

	/** @brief Creates a depth/stencil state.
	 */
	GpuDepthStencilState createDepthStencilState();

	/** @brief Binds the depth/stencil state.
	 */
	void bind( const GpuDepthStencilState & newState );

	/** @brief Retrieves the current depth/stencil state.
	 */
	const GpuDepthStencilState & getCurrentDepthStencilState() const;

	/** @brief Creates a rasterizer state.
	 */
	GpuRasterizerState createRasterizerState();

	/** @brief Binds the rasterizer state.
	 */
	void bind( const GpuRasterizerState & newState );

	/** @brief Retrieves the current rasterizer state.
	 */
	const GpuRasterizerState & getCurrentRasterizerState() const;

	/** @brief Creates a scissor state.
	 */
	GpuScissorState createScissorState();

	/** @brief Binds the scissor state.
	 */
	void bind( const GpuScissorState & newState );

	/** @brief Retrieves the current scissor state.
	 */
	const GpuScissorState & getCurrentScissorState() const;

	/** @brief Creates a viewport state.
	 */
	GpuViewportState createViewportState();

	/** @brief Binds the viewport state.
	 */
	void bind( const GpuViewportState & newState );

	/** @brief Retrieves the current viewport state.
	 */
	const GpuViewportState & getCurrentViewportState() const;

	/** @brief Returns the maximum number of draw buffers.
	 */
	size_t getMaxDrawBuffers() const;

	/** @brief Creates a draw buffer state.
	 */
	GpuDrawBufferState createDrawBufferState();

	/** @brief Binds the draw buffer state.
	 */
	void bind( const GpuDrawBufferState & newState );

	/** @brief Retrieves the current draw buffer state.
	 */
	const GpuDrawBufferState & getCurrentDrawBufferState() const;

	// TEXTURES:

	/** @brief Creates a 1D texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::Texture1DDescription & desc );

	/** @brief Creates a 1D array texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::Texture1DArrayDescription & desc );

	/** @brief Creates a 2D texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::Texture2DDescription & desc );

	/** @brief Creates a 2D array texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::Texture2DArrayDescription & desc );

	/** @brief Creates a 2D multisample texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::Texture2DMultisampleDescription & desc );

	/** @brief Creates a 2D multisample array texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::Texture2DMultisampleArrayDescription & desc );

	/** @brief Creates a 3D texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::Texture3DDescription & desc );

	/** @brief Creates a buffer texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::TextureBufferDescription & desc );

	/** @brief Creates a cube map texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::TextureCubeMapDescription & desc );

	/** @brief Creates a rectangle texture.
	 */
	GpuTextureReference createTexture( const GpuTexture::TextureRectangleDescription & desc );

	/** @brief Returns the number of available texture image units.
	 */
	size_t getTextureImageUnitCount() const;

	/** @brief Sets the index of the active texture image unit.
	 */
	void setActiveTextureImageUnitIndex( size_t i );

	/** @brief Returns the index of the active texture image unit.
	 */
	size_t getActiveTextureImageUnitIndex() const;

	/** @brief Returns the given texture image unit.
	 */
	GpuTextureImageUnitReference getTextureImageUnit( size_t i );

	/** @brief Returns the given texture image unit.
	 */
	GpuTextureImageUnitConstReference getTextureImageUnit( size_t i ) const;

	/** @brief Returns the given texture image unit.
	 */
	GpuTextureImageUnitReference getActiveTextureImageUnit();

	/** @brief Returns the given texture image unit.
	 */
	GpuTextureImageUnitConstReference getActiveTextureImageUnit() const;

	// RENDERBUFFERS:

	/** @brief Creates a renderbuffer.
	 */
	GpuRenderbufferReference createRenderbuffer( const GpuRenderbuffer::RenderbufferDescription & desc );

	/** @brief Binds the given renderbuffer.
	 */
	void bindRenderbuffer( const GpuRenderbufferReference & renderbuffer );

	/** @brief Unbinds the currently bound renderbuffer.
	 */
	void unbindRenderbuffer();

	/** @brief Returns the renderbuffer currently bound.
	 */
	GpuRenderbufferReference getBoundRenderbuffer();

	/** @brief Returns the renderbuffer currently bound.
	 */
	GpuRenderbufferConstReference getBoundRenderbuffer() const;

	// FRAMEBUFFERS:

	/** @brief Returns the maximum number of color attachments.
	 */
	size_t getMaxColorAttachments() const;

	/** @brief Sets whether sRGB mode is enabled for the framebuffer.
	 */
	void setFramebufferSRgbEnabled( bool e );

	/** @brief Returns whether sRGB mode is enabled for the framebuffer.
	 */
	bool isFramebufferSRgbEnabled() const;

	/** @brief Creates a framebuffer.
	 */
	GpuFramebufferReference createFramebuffer();

	/** @brief Binds the given framebuffer to the given location.
	 */
	void bindFramebuffer( GLenum location, const GpuFramebufferReference & framebuffer );

	/** @brief Unbinds the currently bound renderbuffer from the given location.
	 */
	void unbindFramebuffer( GLenum location );

	/** @brief Returns the framebuffer currently bound.
	 */
	GpuFramebufferReference getBoundFramebuffer( GLenum location );

	/** @brief Returns the framebuffer currently bound.
	 */
	GpuFramebufferConstReference getBoundFramebuffer( GLenum location ) const;

	// BUFFERS:

	/** @brief Creates a buffer.
	 */
	GpuBufferReference createBuffer( const GpuBuffer::BufferDescription & desc );

	/** @brief Binds the given buffer to the location corresponding to its originally specified type.
	 */
	void bindBuffer( const GpuBufferReference & buffer );

	/** @brief Binds the given buffer to the given location.
	 */
	void bindBuffer( GLenum location, const GpuBufferReference & buffer );

	/** @brief Unbinds the currently bound buffer from the given location.
	 */
	void unbindBuffer( GLenum location );

	/** @brief Returns the buffer currently bound to the given location.
	 */
	GpuBufferReference getBoundBuffer( GLenum location );

	/** @brief Returns the buffer currently bound to the given location.
	 */
	GpuBufferConstReference getBoundBuffer( GLenum location ) const;

	// PROGRAMS:

	/** @brief Creates a program.
	 */
	GpuProgramReference createProgram( const GpuProgram::ProgramDescription & desc );

	/** @brief Binds the given program.
	 */
	void bindProgram( const GpuProgramReference & program );

	/** @brief Unbinds the currently bound program.
	 */
	void unbindProgram();

	/** @brief Returns the program currently bound.
	 */
	GpuProgramReference getBoundProgram();

	/** @brief Returns the program currently bound.
	 */
	GpuProgramConstReference getBoundProgram() const;

	// ATTRIBUTES:

	/** @brief Returns the number of available attributes.
	 */
	size_t getAttributeCount() const;

	/** @brief Returns the given attribute.
	 */
	GpuAttributeReference getAttribute( size_t i );

	/** @brief Returns the given attribute.
	 */
	GpuAttributeConstReference getAttribute( size_t i ) const;

	// DRAW CALLS:

	/** @brief Creates a draw state.
	 */
	GpuDrawState createDrawState();

	/** @brief Binds the draw state.
	 */
	void bind( const GpuDrawState & newState );

	/** @brief Retrieves the current draw state.
	 */
	const GpuDrawState & getCurrentDrawState();

	/** @brief Creates a draw call.
	 */
	GpuDrawCall createDrawCall();

	/** @brief Creates an instanced draw call.
	 */
	GpuInstancedDrawCall createInstancedDrawCall();

	/** @brief Creates an indexed draw call.
	 */
	GpuIndexedDrawCall createIndexedDrawCall();

	/** @brief Creates an indexed offset draw call.
	 */
	GpuIndexedOffsetDrawCall createIndexedOffsetDrawCall();

	/** @brief Creates an indexed instanced draw call.
	 */
	GpuIndexedInstancedDrawCall createIndexedInstancedDrawCall();

	/** @brief Creates an indexed instanced offset draw call.
	 */
	GpuIndexedInstancedOffsetDrawCall createIndexedInstancedOffsetDrawCall();

	/** @brief Creates an indexed ranged draw call.
	 */
	GpuIndexedRangedDrawCall createIndexedRangedDrawCall();

	/** @brief Creates an indexed ranged offset draw call.
	 */
	GpuIndexedRangedOffsetDrawCall createIndexedRangedOffsetDrawCall();

	/** @brief Creates a clear call.
	 */
	GpuClearCall createClearCall();

private:
	// current states
	GpuBlendState blendState;
	GpuDepthStencilState depthStencilState;
	GpuRasterizerState rasterizerState;
	GpuScissorState scissorState;
	GpuViewportState viewportState;
	GpuDrawBufferState drawBufferState;
	size_t maxDrawBuffers;

	// array of texture image units
	std::vector <GpuTextureImageUnitReference> textureImageUnits;
	size_t activeTextureImageUnit;

	// renderbuffer binding
	GpuRenderbufferReference renderbufferBinding;

	// framebuffer bindings
	GpuFramebufferReference drawFramebufferBinding;
	GpuFramebufferReference readFramebufferBinding;
	size_t maxColorAttachments;
	bool framebufferSRgbEnabled;

	// buffer bindings
	enum BufferBindingLocation {
		TBL_ARRAY_BUFFER,
		TBL_COPY_READ_BUFFER,
		TBL_COPY_WRITE_BUFFER,
		TBL_ELEMENT_ARRAY_BUFFER,
		TBL_PIXEL_PACK_BUFFER,
		TBL_PIXEL_UNPACK_BUFFER,
		TBL_TEXTURE_BUFFER,
		TBL_TRANSFORM_FEEDBACK_BUFFER,
		TBL_UNIFORM_BUFFER,
		BUFFER_BINDING_LOCATION_COUNT
	};
	static BufferBindingLocation getBufferBindingLocation( GLenum loc );

	GpuBufferReference bufferBindingLocations[BUFFER_BINDING_LOCATION_COUNT];

	// program binding
	GpuProgramReference programBinding;

	// array of attributes
	std::vector <GpuAttributeReference> attributes;

	// draw state
	GpuDrawState drawState;
};

#endif