#ifndef SYSGUILISTBOX_DEFINED
#define SYSGUILISTBOX_DEFINED

#include "Common.h"
#include "Actor.h"
#include "SysGui.h"

#include "TextUtil.h"
#include <vector>
#include <boost/function.hpp>

DEFINE_DERIVED_ACTOR( SysGuiListbox, SysGui, 0, 0 )

	Vector2i position;
	int width;
	size_t height;
	std::vector <std::string> text;
	int scroll;

	int selected;

	std::vector <std::string> displayedText;
	void recomputeDisplayedText( size_t i );

	boost::function <void ( int, const std::string & )> onSelect;
	boost::function <void ( int, const std::string &, int, const std::string & )> onSelect2;

	int rowAtPoint( const Vector2f & point ) const;

public:
	void onCreate();

	void move( const Vector2i & pos );
	void setWidth( int w );
	void setHeight( size_t h );

	void setText( size_t i, const std::string & t );
	void insertText( size_t i, const std::string & t );
	void pushText( const std::string & t );
	void removeText( size_t i, bool callback );
	void popText( bool callback );
	std::string getText( size_t i ) const;
	size_t getRows() const;
	void select( int i, bool callback );
	int getSelection() const;

	void setOnSelect( const boost::function <void ( int, const std::string & )> & callback );
	void setOnSelect( const boost::function <void ( int, const std::string &, int, const std::string & )> & callback );

	void receiveEvent( const InputEvent & ev );
	void onDismissParent();
	void draw();
};

#endif