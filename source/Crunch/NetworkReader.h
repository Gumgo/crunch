#if 0

/** @file NetworkReader.h
 *  @brief Reads messages received from the network.
 */

#ifndef NETWORKREADER_DEFINED
#define NETWORKREADER_DEFINED

#include "Common.h"
#include "Network.h"

class NetworkIdLayer;

/** @brief Reads messages received from the network.
 */
class NetworkReader : private boost::noncopyable {
	// allow the ID layer to construct us
	friend class NetworkIdLayer;

	size_t senderId;	/**< The ID of the sender.*/
	const byte * data;	/**< The data pointer.*/
	size_t dataSize;	/**< The size of the data.*/
	size_t position;	/**< The current read position.*/

	/** @brief Returns the current pointer and increments it by the given size, throwing an exception if reading would exceed the range of the data.*/
	const byte * inc( size_t bytes );

	/** @brief The constructor.
	 */
	NetworkReader( size_t connectionId, const byte * d, size_t size );

public:
	size_t getSenderId() const;	/**< Returns the ID of the message sender.*/
	char readChar();			/**< Reads and returns a char.*/
	byte readByte();			/**< Reads and returns a byte.*/
	short readShort();			/**< Reads and returns a short.*/
	ushort readUshort();		/**< Reads and returns an unsigned short.*/
	int readInt();				/**< Reads and returns an int.*/
	uint readUint();			/**< Reads and returns an unsigned int.*/
	float readFloat();			/**< Reads and returns a float.*/
	double readDouble();		/**< Reads and returns a double.*/
	std::string readString();	/**< Reads and returns a string.*/
	void readData( void * dest, size_t bytes );	/**< Reads the given amount of data into the given pointer.*/
};

#endif

#endif