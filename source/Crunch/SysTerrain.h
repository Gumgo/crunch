#ifndef SYSTERRAIN_DEFINED
#define SYSTERRAIN_DEFINED

#include "Common.h"
#include "TaskQueue.h"
#include "FastDelegate.h"
#include "HashMap.h"
#include "HashFunctions.h"
#include "GpuBuffer.h"
#include "SysCamera2D.h"
#include "SysCamera3D.h"
#include "SysAabb.h"
#include <vector>
#include <stack>

// a terrain system which uses chunked LOD
//
// The terrain consists of multiple LODs, where each LOD is comprised of terrain "chunks".
// The chunks are indexed by (L, x, y), where L is the LOD.
// Each chunk represents a square area of terrain.
// Each chunk is comprised of SxS data points, where S is the "chunk size", which must be a power of 2.
// Chunks in all LODs are of the same chunk size, but the area covered by a chunk on level L is four times the area of a chunk on level L-1.
// This means that chunk (L, x, y) covers the same area as chunks (L-1, 2x, 2y), (L-1, 2x+1, 2y), (L-1, 2x, 2y+1), and (L-1, 2x+1, 2y+1).
class SysTerrain : private boost::noncopyable {
public:
	static const size_t MAX_MATERIALS = 7;

	// chunk ID (LOD and integer coordinates)
	struct Chunk {
		size_t lod;
		Vector2i coords;
		Chunk();
		Chunk( size_t l, const Vector2i & c );
		bool operator==( const Chunk & o ) const;
	};

	// chunk with an offset
	// the offset is relative to LOD and chunkSize
	// i.e. it ranges from 0 to chunkSize*2^LOD
	struct Position {
		Chunk chunk;
		Vector2f offset;
		Position();
		Position( const Chunk & c, const Vector2f & o );
		Position( size_t l, const Vector2i & c, const Vector2f & o );
	};

	// data to be filled in by a user-defined function
	struct ChunkData {
		struct PointData {
			float z;						// z position of this point
			Vector3f normal;				// normal of this point
			byte materials[MAX_MATERIALS];	// material strengths of this point
		};

		struct PatchData {
			bool enabled;	// whether this patch should be rendered - allows for "holes"
		};

		std::vector <PointData> points;		// data for points in terrain; this is already properly resized
		std::vector <PatchData> patches;	// data for patches in terrain; this is already properly resized
	};

	// vertex in a chunk - 48 bytes
	struct ChunkVertex {
		float z[3];				// z position of this point and two morphs
		byte xyPatchEnabled[3];	// packed x,y index in chunk and patch enabled flag
		byte padding1;
		short normalM0[2];		// normal of this point
		short normalM1[2];		// normal of the first morph
		short normalM2[2];		// normal of the second morph
		byte materials[18];		// packed material strengths for point and two morphs - 7th value is reconstructed
		byte padding2[2];
		// each patch is represented by the following bits (where "o" is the vertex):
		// y
		// |
		// |   3 | 4
		// | ----o---
		// |   1 | 2
		// |
		// +-----------x
	};

	// DataProvider functions are used to provide terrain data when it is requested
	// DataProvider functions have the following signature
	// (note that the use of fastdelegate allows for a "context" through the use of member functions)
	// void provideData( const Chunk & chunk, ChunkData * chunkData )
	// terrain		The terrain object being generated for
	// chunk		The ID (i.e. LOD and coordinates) of the chunk to be filled with data
	// chunkData	The object to be filled with terrain data
	typedef fastdelegate::FastDelegate3 <const SysTerrain*, const Chunk &, ChunkData*, void> DataProvider;

	// ProgramBinder functions are called to setup custom shader programs before rendering
	// ProgramBinder functions have the following signature
	// void programBinder( size_t vertexBufferOffset, const SysCamera3D * camera, const SysTerrain::Chunk & chunk, const bool * materialsEnabled )
	// vertexBufferOffset	The offset in the vertex buffer of the chunk being drawn
	// chunk				The chunk being drawn
	// materialsEnabled		An array of bools of size MAX_MATERIALS representing which materials are enabled
	typedef fastdelegate::FastDelegate3 <size_t, const Chunk &, const bool*> ProgramBinder;

	struct Settings {
		size_t chunkSize;					// terrain chunk size
		size_t maxLod;						// maximum LOD that will ever be requested
		float scale;						// the scale of the terrain (i.e. the size of one patch)
		float maxViewDist;					// maximum viewing distance
		float cameraFov;					// FOV that the camera will (usually) be at
		float imagePlanePolygonFraction;	// fraction of the image plane (in 1 dimension) polygons should take up; used to determine LOD
		bool morphingEnabled;				// whether morphing between LODs is enabled
		float morphRatio;					// the ratio of LODs used for morphing - e.g. 0.25 means the last 25%
		DataProvider dataProvider;			// function that provides terrain data on request - note: this function should be thread safe
	};

private:
	//////////////////////////////////
	// general settings

	Settings settings;			// settings struct for outside access

	bool initialized;
	size_t chunkSize;			// the chunk size
	size_t maxLod;				// the highest LOD allowed
	float maxViewDist;			// the maximum viewing distance
	float scale;				// the scale of the terrain (i.e. the size of one patch)
	float invScale;				// 1/scale
	bool morphingEnabled;		// whether morphing between LODs is enabled
	float morphRatio;			// the ratio of LODs used for morphing - e.g. 0.25 means last 25%
	DataProvider dataProvider;	// the function to provide terrain data

	//////////////////////////////////
	// LOD information

	// info about a LOD
	struct Lod {
		float chunkSize;			// chunk size of LOD
		float invChunkSize;			// 1/chunk size of LOD
		float minDist2;				// the minimum distance squared to the center this LOD is allowed to be
		Position center;			// used when computing correct LOD
		Vector2f morphDistances;	// the distances morphing begins and ends
	};
	std::vector <Lod> lods;
	Position start;	// starting chunk in valid range (LOD 0)
	Position end;	// ending chunk in valid range (LOD 0)

public:
	// converts the position to a new LOD
	Position convertLod( const Position & p, size_t toLod ) const;
	Vector2i convertLod( size_t lod, const Vector2i & chunk, size_t toLod ) const;
	// ensures offset is within the bounds of a chunk
	void correctOffset( Position & p ) const;
	// returns the closest squared distance from the given position to the chunk of the same LOD
	float distanceSquared( const Position & p, const Vector2i & chunk ) const;

private:
	//////////////////////////////////
	// chunk-related data

	// chunk sections:
	// y
	// |-------------+
	// | \    2    / |
	// |   \     /   |
	// |     \ /     |
	// | 3   / \   1 |
	// |   /     \   |
	// | /    0    \ |
	// +-------------+--x
	// vertices are indexed with x increasing, then y increasing

	// metadata for chunk data
	struct ChunkMetadata {
		Vector2f zBounds;						// min and max z coordinates for the chunk
		bool materialsEnabled[4*MAX_MATERIALS];	// whether each material is enabled for each section of the chunk
	};

	// data used to fill a chunk's vertex buffer
	struct ChunkBufferData {
		std::vector <ChunkVertex> vertices;
		ChunkMetadata metadata;
	};

	// the number of chunks to reserve space for
	static const size_t INITIAL_VERTEX_BUFFER_CHUNKS = 40;
	// the size (in bytes) each vertex buffer should be
	// this value is rounded up to chunk size
	static const size_t VERTEX_BUFFER_SIZE = 2 * 1024 * 1024; // 2mb
	// size of a chunk in bytes
	size_t chunkSizeBytes;
	// actual size of the vertex buffers
	size_t vertexBufferSize;
	// the number of chunks per vertex buffer
	size_t chunksPerVertexBuffer;
	// the list of vertex buffers
	std::vector <GpuBufferReference> vertexBuffers;

	// location in a vertex buffer
	struct VertexBufferLocation {
		GpuBuffer * vertexBuffer;
		size_t byteOffset;
	};
	// list of free vertex buffer locations
	std::stack <VertexBufferLocation> freeVertexBufferLocations;
	// allocates a vertex buffer and adds all its locations to the free list
	void allocateVertexBuffer();

	struct ChunkLoadingData; // fwd dec
	struct ChunkStatus {
		static const size_t F_SHOULD_BE_LOADED		= 0x00000001;	// this chunk should be loaded
		static const size_t F_SHOULD_DISPLAY		= 0x00000002;	// this chunk should be displayed
		static const size_t F_DISPLAYED				= 0x00000004;	// this chunk was determined to display last update
		static const size_t F_DISPLAYING			= 0x00000008;	// this chunk was determined to display this update
		static const size_t F_LOADING				= 0x00000010;	// this chunk is loading
		static const size_t F_LOADED				= 0x00000020;	// this chunk is loaded
		static const size_t F_ATTEMPTED_CANCEL		= 0x00000040;	// we have attempted to cancel this chunk's loading task (unsuccessfully)
		static const size_t F_CACHED				= 0x00000080;	// this chunk is in the cache
		static const size_t F_SKIP					= 0x00000100;	// skip updating this chunk - it's about to be removed
		size_t flags;
		void addFlags( size_t f );
		void removeFlags( size_t f );
		bool hasAllFlags( size_t f ) const;		// true if all of the given flags are set
		bool hasSomeFlags( size_t f ) const;	// true if at least one of the given flags is set
		ChunkStatus();							// sets default flags

		TaskQueue::TaskId loadingTaskId;		// ID of the loading task
		ChunkLoadingData * chunkLoadingData;	// data sent to the task - store pointer so we can release it once the task is complete

		VertexBufferLocation vertexBufferLocation;	// location in vertex buffer
		ChunkMetadata metadata;						// metadata about the chunk

		size_t lodDifference[4];	// LOD difference for each section of this chunk - determined by surrounding chunks

		size_t cacheIndex;		// index in cache
		byte childrenDisplayed;	// whether child chunks were just displayed (and can therefore display again if necessary)
		bool parentDisplayed;	// whether this chunk should not be displayed (because a parent up the LOD chain is)
	};

	// contains data about chunks
	typedef HashMap <Hash <Chunk>, Chunk, ChunkStatus> ChunkMap;
	ChunkMap chunkMap;
	std::vector <Chunk> chunksToRemove;

	// the list of chunks that could possibly be displayed
	std::vector <Chunk> possiblyDisplayedChunks;

	// list of chunks to draw - constructed before drawing
	std::vector <size_t> chunksToDraw;

	// the AABB of all loaded chunks
	SysAabb aabb;

	//////////////////////////////////
	// chunk caching

	// an LRU cache for chunks
	class ChunkCache {
		static const size_t CACHE_SIZE = 10;
		static const size_t NO_INDEX = boost::integer_traits <size_t>::const_max;
		struct Entry {
			Chunk chunk;	// the chunk
			size_t next;	// next entry in cache
			size_t prev;	// prev entry in cache
		};
		std::vector <Entry> entries;
		size_t firstFreeEntry;
		size_t mostRecentEntry;
		size_t leastRecentEntry;

	public:
		ChunkCache();
		// caches a chunk
		// stores the cache index in idx
		// returns whether a chunk was removed from the cache
		// if so, that chunk is stored in removedChunk
		bool cache( const Chunk & chunk, size_t & idx, Chunk & removedChunk );
		// uncaches the chunk with the given index
		void uncache( size_t idx );
	} chunkCache;

	//////////////////////////////////
	// loading-related functionality

	// max number of chunks allowed to be loading at once
	static const size_t MAX_CHUNKS_LOADING = 10;
	// holds the data passed to the chunk loading function
	struct ChunkLoadingData {
		Chunk chunk;						// the chunk to load
		ChunkData chunkData;				// a pointer to the struct of data to fill out
		ChunkBufferData chunkBufferData;	// a pointer to the chunk's vertex data buffer
	};
	// vector of chunk loading data; since threads hold pointers to these elements, this should only be resized once at setup
	std::vector <ChunkLoadingData> chunkLoadingData;
	// stack of pointers to chunk loading data available for use
	std::stack <ChunkLoadingData*> freeChunkLoadingData;
	// tries to start a chunk loading - returns whether successful
	bool startLoadingChunk( const Chunk & chunk, ChunkStatus & status );

	// the task to load a chunk
	void loadChunkTask( void * chunkLoadingDataPtr ) const;

	// generates the chunk vertices
	void generateVertices( ChunkLoadingData * cld ) const;

	//////////////////////////////////
	// index buffers

	GLenum indexType;
	GLuint primitiveRestartIndex;
	// stores info about location in index buffer
	struct IndexBufferLocation {
		size_t byteOffset;	// offset into the index buffer
		size_t count;		// number of indices
		size_t minElement;	// min index
		size_t maxElement;	// max index
	};
	std::vector <IndexBufferLocation> indexBufferLocations;
	// the index buffer
	GpuBufferReference indexBuffer;
	// returns index of x, y
	uint xyIdx( size_t x, size_t y ) const;
	uint rotateIdx( uint i ) const;
	// generates the index buffer holding LOD combinations
	void generateIndexBuffer();
	// returns the appropriate index buffer location
	// section is the section of the chunk
	// lodDifference is the difference in LOD of the connected chunk
	const IndexBufferLocation & getIndexBufferLocation( size_t section, size_t lodDifference ) const;

	//////////////////////////////////
	// objects used in updating

	float maxViewDist2;
	std::vector <Chunk> subdivisionStack;

	//////////////////////////////////
	// debugging

	const SysCamera2D * debugCamera;
	Position currentCenter;

public:
	SysTerrain();
	~SysTerrain();

	// sets up the terrain
	// terrainChunkSize	the size of each chunk
	// maximumLod		the maximum LOD level to ever request
	// maximumViewDist
	bool setupTerrain( const Settings & s );
	// updates the point the terrain is centered around and updates which LODs should be loaded/displayed
	void update( const Vector2f & center );
	// same as above, except this version accepts coordinates in terms of LOD 0 chunk and offset
	// this is more friendly for "infinite" worlds, where a single world coordinate quickly loses precision
	// internally, this is always called
	void update( const Vector2i & chunk, const Vector2f & offset );

	struct UsageInfo {
		size_t chunksLoaded;
		size_t chunksActive;
		size_t chunksCached;
		size_t chunksInView;
		size_t vertexBuffersAllocated;
		size_t totalVertexBufferMemory;
		size_t usedVertexBufferMemory;
		size_t indexBufferMemory;
	};
	void getUsageInfo( UsageInfo & i ) const;
	const Settings & getSettings() const;
	const Position & getCenter( size_t lod ) const;
	Vector2f getMorphDistances( size_t lod ) const;
	const SysAabb & getAabb() const;

	// builds the list of chunks to draw
	// for multiple passes (like deferred rendering) only do this once
	// returns AABB of all chunks
	void prepareToDraw( const SysCamera3D * camera );

	// draws the terrain
	void draw( ProgramBinder b );

	// debugging:
	void setDebugCamera( const SysCamera2D * cam );
	void drawChunks2D();
	void drawOptimalChunks2D();
};

#endif