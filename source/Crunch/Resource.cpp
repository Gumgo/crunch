#include "Resource.h"
#include "ResourceManager.h"

Resource::Resource( ResourceManager * rm, const std::string & n, const std::string & p, uint m ) {
	resourceManager = rm;
	name = n;
	path = p;
	metadata = m;

	referenceCount = 0;

	status = S_NOT_LOADED;
	detailedStatus = S_NOT_LOADED;
}

Resource::~Resource() {
}

std::string Resource::getName() const {
	return name;
}

std::string Resource::getPath() const {
	return path;
}

uint Resource::getMetadata() const {
	return metadata;
}

size_t Resource::getReferenceCount() const {
	return referenceCount;
}

void Resource::acquire() const {
	++referenceCount;
}

void Resource::release() const {
	--referenceCount;
	if (referenceCount == 0)
		// we are converting from a const pointer to a regular pointer
		// this is pretty bad, but the reason is because "release" acts as the destructor
		// i.e. effectively there are no pointers to this object (except the resource manager)
		// the does not have the const requirements that normal methods do
		// therefore, this should also not
		resourceManager->release( (Resource*)this, false );
}

void Resource::setStatus( Resource::Status s ) {
	status = s;
}

Resource::Status Resource::getStatus() const {
	return status;
}

void Resource::setDetailedStatus( Resource::Status s ) {
	detailedStatus = s;
}

Resource::Status Resource::getDetailedStatus() const {
	return detailedStatus;
}