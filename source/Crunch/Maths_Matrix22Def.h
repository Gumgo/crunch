#ifndef MATHS_MATRIX22DEF_DEFINED
#define MATHS_MATRIX22DEF_DEFINED

template <typename T> Matrix22 <T>::Matrix22() {
	set( 1, 0,
		 0, 1 );
}

template <typename T> Matrix22 <T>::Matrix22( T m00, T m01,
											  T m10, T m11 ) {
	set( m00, m01,
		 m10, m11 );
}

template <typename T> Matrix22 <T>::Matrix22( const Matrix22 <T> & m ) {
	set( m[0], m[1],
		 m[2], m[3] );
}

template <typename T> Matrix22 <T>::~Matrix22() {
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator+( const Matrix22 <T> & m ) const {
	return Matrix22( d[0] + m[0], d[1] + m[1],
					 d[2] + m[2], d[3] + m[3] );
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator+() const {
	return *this;
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator-( const Matrix22 <T> & m ) const {
	return Matrix22( d[0] - m[0], d[1] - m[1],
					 d[2] - m[2], d[3] - m[3] );
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator-() const {
	return Matrix22( -d[0], -d[1],
					 -d[2], -d[3] );
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator*( T r ) const {
	return Matrix22( d[0]*r, d[1]*r,
					 d[2]*r, d[3]*r );
}

template <typename T> Matrix22 <T> operator*( T r, const Matrix22 <T> & m ) {
	return m*r;
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator/( T r ) const {
	return Matrix22( d[0]/r, d[1]/r,
					 d[2]/r, d[3]/r );
}

template <typename T> Vector2 <T> Matrix22 <T>::operator*( const Vector2 <T> & v ) const {
	return Vector2 <T>( d[0]*v[0] + d[1]*v[1],
						d[2]*v[0] + d[3]*v[1] );
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator*( const Matrix22 <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[2];
	T m01 = d[0]*m[1] + d[1]*m[3];

	T m10 = d[2]*m[0] + d[3]*m[2];
	T m11 = d[2]*m[1] + d[3]*m[3];

	return Matrix22 <T>( m00, m01,
						 m10, m11 );
}

template <typename T> Matrix33a <T> Matrix22 <T>::operator*( const Matrix33a <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[3];
	T m01 = d[0]*m[1] + d[1]*m[4];

	T m10 = d[2]*m[0] + d[3]*m[3];
	T m11 = d[2]*m[1] + d[3]*m[4];

	return Matrix33a <T>( m00, m01, m[2],
						  m10, m11, m[5] );
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator=( const Matrix22 <T> & m ) {
	set( m[0], m[1],
		 m[2], m[3] );
	return *this;
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator+=( const Matrix22 <T> & m ) {
	d[0] += m[0]; d[1] += m[1];
	d[2] += m[2]; d[3] += m[3];
	return *this;
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator-=( const Matrix22 <T> & m ) {
	d[0] -= m[0]; d[1] -= m[1];
	d[2] -= m[2]; d[3] -= m[3];
	return *this;
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator*=( T r ) {
	d[0] *= r; d[1] *= r;
	d[2] *= r; d[3] *= r;
	return *this;
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator/=( T r ) {
	d[0] /= r; d[1] /= r;
	d[2] /= r; d[3] /= r;
	return *this;
}

template <typename T> Matrix22 <T> Matrix22 <T>::operator*=( const Matrix22 <T> & m ) {
	T m00 = d[0]*m[0] + d[1]*m[2];
	T m01 = d[0]*m[1] + d[1]*m[3];

	T m10 = d[2]*m[0] + d[3]*m[2];
	T m11 = d[2]*m[1] + d[3]*m[3];

	set( m00, m01,
		 m10, m11 );

	return *this;
}

template <typename T> bool Matrix22 <T>::operator==( const Matrix22 <T> & m ) const {
	return (d[0] == m[0] && d[1] == m[1] &&
			d[2] == m[2] && d[3] == m[3]);
}

template <typename T> bool Matrix22 <T>::operator!=( const Matrix22 <T> & m ) const {
	return (d[0] != m[0] || d[1] != m[1] ||
			d[2] != m[2] || d[3] != m[3]);
}

template <typename T> template <typename U> T & Matrix22 <T>::operator[]( U i ) {
	return d[i];
}

template <typename T> template <typename U> T Matrix22 <T>::operator[]( U i ) const {
	return d[i];
}

template <typename T> template <typename U> T & Matrix22 <T>::operator()( U row, U col ) {
	return d[row*2+col];
}

template <typename T> template <typename U> T Matrix22 <T>::operator()( U row, U col ) const {
	return d[row*2+col];
}

template <typename T> template <typename U> Matrix22 <T>::operator Matrix22 <U> () const {
	return Matrix22 <U>( (U)d[0], (U)d[1],
						 (U)d[2], (U)d[3] );
}

template <typename T> void Matrix22 <T>::set( T m00, T m01,
											  T m10, T m11 ) {
	d[0] = m00; d[1] = m01;
	d[2] = m10; d[3] = m11;
}

template <typename T> void Matrix22 <T>::transpose() {
	T temp;
	temp = d[2];
	d[2] = d[1];
	d[1] = temp;
}

template <typename T> bool Matrix22 <T>::invert() {
	T det = determinant();
	if (det == 0) {
		set( 0, 0, 0, 0 );
		return false;
	}

	T invDet = 1/det;

	T m00 = invDet * d[3];
	T m01 = invDet * -d[1];
	T m10 = invDet * -d[2];
	T m11 = invDet * d[0];

	set( m00, m01,
		 m10, m11 );
	return true;
}

template <typename T> void Matrix22 <T>::rotate( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	Matrix22 <T> rot( cosA, -sinA,
					  sinA, cosA );

	*this = rot * *this;
}

template <typename T> void Matrix22 <T>::scale( T x, T y ) {
	d[0] *= x;
	d[1] *= x;

	d[2] *= y;
	d[3] *= y;
}

template <typename T> void Matrix22 <T>::scale( const Vector2 <T> & s ) {
	scale( s.x, s.y );
}

template <typename T> void Matrix22 <T>::zero() {
	set( 0, 0, 0, 0 );
}

template <typename T> void Matrix22 <T>::identity() {
	set( 1, 0, 0, 1 );
}

template <typename T> Matrix22 <T> Matrix22 <T>::transposed() const {
	return Matrix22 <T>( d[0], d[2],
						 d[1], d[3] );
}

template <typename T> Matrix22 <T> Matrix22 <T>::inverse() const {
	T det = determinant();
	if (det == 0)
		return Matrix22 <T>( 0, 0, 0, 0 );

	T invDet = 1/det;

	T m00 = invDet * d[3];
	T m01 = invDet * -d[1];
	T m10 = invDet * -d[2];
	T m11 = invDet * d[0];

	return Matrix22 <T>( m00, m01,
						 m10, m11 );
}

template <typename T> Matrix22 <T> Matrix22 <T>::rotated( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	Matrix22 <T> rot( cosA, -sinA,
					  sinA, cosA );

	return rot * *this;
}

template <typename T> Matrix22 <T> Matrix22 <T>::scaled( T x, T y ) const {
	return Matrix22 <T>( d[0]*x, d[1]*x,
						 d[2]*y, d[3]*y );
}

template <typename T> Matrix22 <T> Matrix22 <T>::scaled( const Vector2 <T> & s ) const {
	return scaled( s.x, s.y );
}

template <typename T> T Matrix22 <T>::determinant() const {
	return d[0]*d[3] - d[1]*d[2];
}

template <typename T> T Matrix22 <T>::trace() const {
	return d[0] + d[3];
}

template <typename T> const T * Matrix22 <T>::getArray() const {
	return d;
}

template <typename T> Ray2 <T> Matrix22 <T>::transformRay( const Ray2 <T> & r ) const {
	return Ray2 <T>(
		*this * ray.origin,
		*this * ray.direction );
}

template <typename T> Matrix22 <T> Matrix22 <T>::getNormalTransformation() const {
	inverse().transposed();
}

#endif