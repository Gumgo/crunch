#include "LodCalculator.h"

LodCalculator::LodCalculator() {
	setValues( 45.0f, 1 );
}

void LodCalculator::setValues( float verticalFov, int screenHeightPixels ) {
	screenHeightOverImagePlaneHeight = 0.5f * (float)screenHeightPixels / tan( degToRad( verticalFov * 0.5f ) );
}

float LodCalculator::projectedLength( float length, float distance ) {
	return screenHeightOverImagePlaneHeight * length / distance;
}

float LodCalculator::projectedArea( float area, float distance ) {
	return screenHeightOverImagePlaneHeight * area / (distance * distance);
}