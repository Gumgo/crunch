#include "SysDeferredAmbientLight.h"
#include "Context.h"
#include "SysRenderer2D.h"
#include "SysRendererDeferredLighting.h"

SysDeferredAmbientLight::SysDeferredAmbientLight()
	: SysDeferredLight( LT_AMBIENT ) {
	program = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_ambient", true );

	autoProgram.setProgram( program->getProgram() );
	autoProgram.setAttribute( "corner", SysRenderer2D::get().getQuadVertexBuffer(), 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	autoProgram.setUniform( "ambientOcclusionMap", GL_SAMPLER_2D, &uniforms.ambientOcclusionMap );
	autoProgram.setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
	autoProgram.setUniform( "applyAmbientOcclusion", GL_BOOL, &uniforms.applyAmbientOcclusion );
	autoProgram.setUniform( "ambientOcclusionStrength", GL_FLOAT, &uniforms.ambientOcclusionStrength );
	uniforms.color.set( 1.0f, 1.0f, 1.0f );
	uniforms.ambientOcclusionMap = 3;
	uniforms.applyAmbientOcclusion = 0;
	uniforms.ambientOcclusionStrength = 0.0f;
}

SysDeferredAmbientLight::~SysDeferredAmbientLight() {
}

void SysDeferredAmbientLight::setColor( const Vector3f & c ) {
	uniforms.color = c;
}

Vector3f SysDeferredAmbientLight::getColor() const {
	return uniforms.color;
}

void SysDeferredAmbientLight::setAmbientOcclusionStrength( float s ) {
	if (s > 0.0f) {
		uniforms.applyAmbientOcclusion = true;
		uniforms.ambientOcclusionStrength = s;
	} else {
		uniforms.applyAmbientOcclusion = false;
		uniforms.ambientOcclusionStrength = 0.0f;
	}
}

float SysDeferredAmbientLight::getAmbientOcclusionStrength() const {
	return uniforms.ambientOcclusionStrength;
}

void SysDeferredAmbientLight::computeShadowMapRange( fastdelegate::FastDelegate0 <const SysAabb*> nextAabb ) {
	// empty; no shadow for ambient lights
}

void SysDeferredAmbientLight::drawShadowMap(
	fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
	fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject ) {
	// empty; no shadow for ambient lights
}

void SysDeferredAmbientLight::draw() {
	if (program->getStatus() != Resource::S_LOADED || camera == NULL)
		return;

	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( false );
	rState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( false );
	dsState.bind();

	SysRenderer2D::get().getQuadVertexBuffer()->bind();

	// renderer has already bound textures

	autoProgram.bind();

	SysRenderer2D::get().drawSingleQuad();
}