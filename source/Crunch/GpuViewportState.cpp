#include "GpuViewportState.h"
#include "GpuState.h"

GpuViewportState::GpuViewportState()
	: GpuObject( NULL ) {
}

void GpuViewportState::bind() {
	getGpuState()->bind( *this );
}

void GpuViewportState::setViewportPosition( const Vector2i & position ) {
	viewportPosition = position;
}

void GpuViewportState::setViewportSize( const Vector2i & size ) {
	viewportSize = size;
}

void GpuViewportState::setViewport( const Vector2i & position, const Vector2i & size ) {
	viewportPosition = position;
	viewportSize = size;
}

Vector2i GpuViewportState::getViewportPosition() const {
	return viewportPosition;
}

Vector2i GpuViewportState::getViewportSize() const {
	return viewportSize;
}

GpuViewportState::GpuViewportState( GpuState * state )
	: GpuObject( state ) {
}

void GpuViewportState::apply( GpuViewportState & currentState ) const {
	if (viewportPosition != currentState.viewportPosition ||
		viewportSize != currentState.viewportSize) {
		glViewport( viewportPosition.x, viewportPosition.y, viewportSize.x, viewportSize.y );
		currentState.viewportPosition = viewportPosition;
		currentState.viewportSize = viewportSize;
	}
}

void GpuViewportState::getCurrentState() {
	GLint viewportResult[4];
	glGetIntegerv( GL_VIEWPORT, viewportResult );
	viewportPosition.set( viewportResult[0], viewportResult[1] );
	viewportPosition.set( viewportResult[2], viewportResult[3] );
}