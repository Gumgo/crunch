/** @file TextureResource.h
 *  @brief Contains the resource holding texture data.
 */

#ifndef TEXTURERESOURCE_DEFINED
#define TEXTURERESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "GpuTexture.h"
#include "KeyValueReader.h"
#include <FreeImage.h>
#include <vector>

/** @brief Texture resource.
 */
DEFINE_RESOURCE_CLASS( TextureResource, "texture", "textures" )
public:
	/** @brief The constructor.
	 */
	TextureResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief Reads metadata.
	 */
	static bool readMetadata( uint & metadata, KeyValueReader::Iterator it );

	static const uint REPEAT_X		= 0x01;	/**< Texture repeats in the x direction.*/
	static const uint REPEAT_Y		= 0x02;	/**< Texture repeats in the y direction.*/
	static const uint REPEAT_Z		= 0x04;	/**< Texture repeats in the z direction.*/
	static const uint MIPMAP		= 0x08;	/**< Mipmaps should be generated.*/
	static const uint TYPE_MASK		= 0x30;	/**< Bits representing type.*/
	static const uint TYPE_2D		= 0x00;	/**< The texture is 2D.*/
	static const uint TYPE_CUBE		= 0x10;	/**< The texture is a cube map.*/
	static const uint TYPE_ARRAY	= 0x20;	/**< The texture is 3D.*/
	static const uint TYPE_3D		= 0x30;	/**< The texture is an array.*/
	static const uint SRGB			= 0x40;	/**< The texture is sRGB format.*/

	/** @brief Returns default metadata.
	 */
	static uint getDefaultMetadata();

	GpuTextureReference getTexture();				/**< Returns the OpenGL texture object.*/
	GpuTextureConstReference getTexture() const;	/**< Returns the OpenGL texture object.*/

private:
	void loadImage();

	uint width;						// texture width in pixels
	uint height;					// texture height in pixels
	uint bytesPerPixel;				// number of bytes per pixel
	uint rowWidthBytes;				// number of bytes per row
	std::vector <byte> data;		// texture data

	bool repeatX, repeatY, repeatZ;	// whether the texture repeats in the x, y, or z directions
	bool mipmap;					// whether mipmaps have been generated

	GpuTextureReference texture;	// the OpenGL texture object
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <TextureResource> TextureResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <TextureResource> TextureResourceConstReference;

#endif