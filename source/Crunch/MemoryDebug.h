/** @file MemoryDebug.h
 *  @brief Includes functionality to detect memory leaks.
 */

#ifndef MEMORYDEBUG_DEFINED
#define MEMORYDEBUG_DEFINED

#if (defined _DEBUG && defined _WIN32)
// unfortunately this seems to be windows only - just have empty functions on other platforms
#define _CRTDBG_MAP_ALLOC
#include <Windows.h>
#include <stdlib.h>
#include <crtdbg.h>
#include <fstream>
#endif

void memDbgInit();		/**< Initializes memory debugging.*/
void memDbgSnapshot1();	/**< Takes a first snapshot of memory.*/
void memDbgSnapshot2();	/**< Takes a second snapshot of memory.*/
void memDbgCompare();	/**< Compares the two snapshots of memory.*/
void memDbgTerm();		/**< Dumps memory data to a file and terminates memory debugging.*/

#endif