/** @file Skeleton2DAnimationResource.h
 *  @brief Contains the resource holding 2D skeleton animation data.
 */

#ifndef SKELETON2DANIMATIONRESOURCE_DEFINED
#define SKELETON2DANIMATIONRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "DataReader.h"
#include <vector>

/** @brief Skeleton 2D animation resource.
 */
DEFINE_RESOURCE_CLASS( Skeleton2DAnimationResource, "skeleton2DAnimation", "skeleton2DAnimations" )
public:
	/** @brief The constructor.
	 */
	Skeleton2DAnimationResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief Holds data for an animated part in an animation.
	 */
	struct AnimatedPartData {
		Vector2f offset;	/**< The offset at this keyframe.*/
		float rotation;		/**< The rotation at this keyframe.*/
	};

	/** @brief Holds data for a keyframe in an animation.
	 */
	struct KeyframeData {
		uint keyframeTime;						/**< The time of this keyframe.*/
		std::vector <AnimatedPartData> parts;	/**< The part data at this keyframe.*/
	};

	size_t getPartCount() const;		/**< Returns the number of parts in the animation.*/
	size_t getPartId( size_t i ) const;	/**< Returns the part ID of the given part in the animation.*/
	uint getLength() const;				/**< Returns the animation length in frames.*/
	size_t getKeyframeCount() const;	/**< Returns the number of keyframes in the animation.*/
	const KeyframeData & getKeyframeData( size_t i ) const;	/**< @brief Returns keyframe data.*/

private:
	std::vector <size_t> partIds;			// part IDs corresponding to the skeleton
	uint length;							// length of the animation in frames
	std::vector <KeyframeData> keyframes;	// keyframe data
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <Skeleton2DAnimationResource> Skeleton2DAnimationResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <Skeleton2DAnimationResource> Skeleton2DAnimationResourceConstReference;

#endif