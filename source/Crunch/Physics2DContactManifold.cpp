#include "Physics2DContactManifold.h"

namespace Physics2D {

// capsule

void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const CapsuleShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const CircleShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const ConvexPolygonShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

// circle

void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const CircleShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;

	Vector2r ctr0 = tf0.transformPoint( Vector2r( s0->getTransform()[2], s0->getTransform()[5] ) );
	Vector2r ctr1 = tf1.transformPoint( Vector2r( s1->getTransform()[2], s1->getTransform()[5] ) );

	Real maxDist = s0->getRadius() + s1->getRadius();
	if ((ctr0 - ctr1).magnitudeSquared() > maxDist*maxDist)
		return;

	manifold->local = s0;
	manifold->contactPointCount = 1;

	//c1 = s0tf-1 * b0tf-1 * b1tf * s1tf * (0,0)
	// get s1's center relative to s0
	Matrix33af s0TfInv = (tf0 * s0->getTransform()).inverse();
	Vector2r ctr1Rel = s0TfInv.transformPoint( ctr1 );
	Real dist = ctr1Rel.magnitude();
	manifold->normal = ctr1Rel * ((Real)1 / dist); // vector pointing from s0 to s1
	manifold->contactPoints[0].point = manifold->normal * (dist - s1->getRadius());
	manifold->contactPoints[0].depth = s0->getRadius() + s1->getRadius() - dist;
}

void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const ConvexPolygonShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

// convex polygon

void getContactManifold( Manifold * manifold, const ConvexPolygonShape * s0, const Matrix33af & tf0, const ConvexPolygonShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const ConvexPolygonShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const ConvexPolygonShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

// edge

void getContactManifold( Manifold * manifold, const EdgeShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

void getContactManifold( Manifold * manifold, const EdgeShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

// ellipse

void getContactManifold( Manifold * manifold, const EllipseShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 ) {
	manifold->contactPointCount = 0;
	// TODO
}

}