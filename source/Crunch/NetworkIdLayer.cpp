#if 0

#include "NetworkIdLayer.h"
#include "Log.h"

NetworkIdLayer::NetworkIdLayer()
	: connectionsMap( 20 )
	, networkIdHandlers( NETWORK_ID_HANDLERS_SIZE ) {
	initialized = false;
	messageLayer.setOnReceiveMessage( boost::function <void ( size_t, const byte*, size_t )>(
		boost::bind( &NetworkIdLayer::receiveMessage, this, _1, _2, _3 ) ) );
	messageLayer.setOnConnectionTimeout( boost::function <void ( size_t )>(
		boost::bind( &NetworkIdLayer::connectionTimeout, this, _1 ) ) );
}

NetworkIdLayer::~NetworkIdLayer() {
	term();
}

bool NetworkIdLayer::init( ushort port ) {
	if (initialized) {
		Log::warning() << "Network ID layer already initialized";
		return false;
	}

	if (!messageLayer.init( port ))
		return false;

	initialized = true;
	Log::info() << "Network ID layer initialized";
	return true;

}

void NetworkIdLayer::term() {
	if (!initialized)
		return;

	messageLayer.term();
	networkIdHandlers.clear();
	Log::info() << "Network ID layer terminated";
	initialized = false;
}


void NetworkIdLayer::acceptNewConnection( size_t messageLayerId ) {
	Connection c;
	c.messageLayerId = messageLayerId;

	size_t newConnectionId = connections.add( c );
	connectionsMap.put( messageLayerId, newConnectionId );
	Log::info() << "ID layer connection " << newConnectionId << " created";

	if (onAcceptNewConnection)
		onAcceptNewConnection( newConnectionId );
}

void NetworkIdLayer::connectionTimeout( size_t messageLayerId ) {
	size_t connectionId = connectionsMap.get( messageLayerId );
	Log::info() << "ID layer connection " << connectionId << " timed out";

	if (onConnectionTimeout)
		onConnectionTimeout( connectionId );
}

void NetworkIdLayer::enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn ) {
	messageLayer.enableAcceptNewConnections( boost::function <void ( size_t )>(
		boost::bind( &NetworkIdLayer::acceptNewConnection, this, _1 ) ) );
	onAcceptNewConnection = onAcceptNewConnectionFn;
}

void NetworkIdLayer::disableAcceptNewConnections() {
	messageLayer.disableAcceptNewConnections();
}

void NetworkIdLayer::setOnConnectionTimeout( const boost::function <void ( size_t )> & onConnectionTimeoutFn ) {
	onConnectionTimeout = onConnectionTimeoutFn;
}

bool NetworkIdLayer::createConnection( const std::string & address, ushort port, size_t & newConnectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to create ID layer connection: network ID layer not initialized";
		return false;
	}

	size_t messageLayerId;
	if (!messageLayer.createConnection( address, port, messageLayerId ))
		return false;

	Connection c;
	c.messageLayerId = messageLayerId;

	newConnectionId = connections.add( c );
	connectionsMap.put( messageLayerId, newConnectionId );
	Log::info() << "ID layer connection " << newConnectionId << " created";

	return true;
}

void NetworkIdLayer::destroyConnection( size_t connectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to destroy connection: network ID layer not initialized";
		return;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to destroy a nonexistent ID layer connection";
		return;
	}

	Connection & c = connections.get( connectionId );
	messageLayer.destroyConnection( c.messageLayerId );

	connectionsMap.remove( c.messageLayerId );
	connections.remove( connectionId );
	Log::info() << "ID layer connection " << connectionId << " destroyed";
}

void NetworkIdLayer::receiveMessage( size_t messageLayerId, const byte * data, size_t dataSize ) {
	size_t connectionId = connectionsMap.get( messageLayerId );

	if (dataSize < 2) {
		Log::warning() << "Message with no network ID received";
		return;
	}

	ushort networkId = *(ushort*)data;
	networkId = ntohs( networkId );

	size_t handlerIndex;
	if (!networkIdHandlers.getIndex( networkId, handlerIndex )) {
		Log::info() << "Message with no handler received and discarded";
		return;
	}

	const NetworkIdHandler & handler = networkIdHandlers.getByIndex( handlerIndex );
	if (handler) {
		// +2 and -2 because we chop off the ID
		NetworkReader reader( connectionId, data + 2, dataSize - 2 );
		handler( reader );
	}
}

void NetworkIdLayer::receiveAndProcess() {
	if (!initialized) {
		Log::warning() << "Failed to receive and process messages: network ID layer not initialized";
		return;
	}

	messageLayer.receiveAndProcess();
}

bool NetworkIdLayer::queueMessage( size_t connectionId, ushort networkId, NetworkWriter & writer ) {
	if (!initialized) {
		Log::warning() << "Failed to queue message: network ID layer not initialized";
		return false;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to queue a message on a nonexistent ID layer connection";
		return false;
	}

	size_t messageLayerId = connections.get( connectionId ).messageLayerId;

	writer.setNetworkId( networkId );
	return messageLayer.queueMessage( messageLayerId, writer.getData(), writer.getDataSize() );
}

bool NetworkIdLayer::queueGuaranteedMessage( size_t connectionId, ushort networkId, NetworkWriter & writer ) {
	if (!initialized) {
		Log::warning() << "Failed to queue guaranteed message: network ID layer not initialized";
		return false;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to queue a guaranteed message on a nonexistent ID layer connection";
		return false;
	}

	size_t messageLayerId = connections.get( connectionId ).messageLayerId;

	writer.setNetworkId( networkId );
	return messageLayer.queueGuaranteedMessage( messageLayerId, writer.getData(), writer.getDataSize() );
}

void NetworkIdLayer::sendQueued() {
	if (!initialized) {
		Log::warning() << "Failed to send queued messages: network ID layer not initialized";
		return;
	}

	messageLayer.sendQueued();
}

bool NetworkIdLayer::addNetworkIdHandler( ushort networkId, const NetworkIdHandler & handler ) {
	if (!initialized) {
		Log::warning() << "Failed to add network ID handler: network ID layer not initialized";
		return false;
	}

	size_t index;
	if (networkIdHandlers.getIndex( networkId, index ))
		networkIdHandlers.getByIndex( index ) = handler;
	else
		networkIdHandlers.put( networkId, handler );

	return true;
}

bool NetworkIdLayer::removeNetworkIdHandler( ushort networkId ) {
	if (!initialized) {
		Log::warning() << "Failed to remove network ID handler: network ID layer not initialized";
		return false;
	}

	if (!networkIdHandlers.remove( networkId )) {
		Log::warning() << "Attempted to remove nonexistent network ID handler";
		return false;
	} else
		return true;
}

bool NetworkIdLayer::networkIdHandlerExists( ushort networkId ) const {
	return networkIdHandlers.contains( networkId );
}

size_t NetworkIdLayer::networkIdHandlerCount() const {
	return networkIdHandlers.size();
}

#endif