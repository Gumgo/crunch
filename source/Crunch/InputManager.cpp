#include "InputManager.h"

InputManager::InputManager() {
}

void InputManager::update( const std::vector <InputEvent> & eventData ) {
	inputState.eventStream.clear();

	inputState.closePressed = false;
	inputState.gainedFocus = false;
	inputState.lostFocus = false;

	std::fill( inputState.mouseButtonsPressed, inputState.mouseButtonsPressed + MOUSE_BUTTON_CODE_COUNT, false );
	std::fill( inputState.mouseButtonsReleased, inputState.mouseButtonsReleased + MOUSE_BUTTON_CODE_COUNT, false );

	inputState.mouseScrollWheel = 0.0f;
	inputState.mouseDelta[0] = 0;
	inputState.mouseDelta[1] = 0;

	std::fill( inputState.keysPressed, inputState.keysPressed + 256, false );
	std::fill( inputState.keysReleased, inputState.keysReleased + 256, false );

	// we filter out some events
	// for example, key presses will trigger multiple times if a key is held down
	// however, we only want to be alerted when the key is first pressed
	for (size_t i = 0; i < eventData.size(); ++i) {
		const InputEvent & ev = eventData[i];
		switch (eventData[i].type) {
		case IET_WINDOW:
			switch (ev.windowEvent.type) {
			case WET_CLOSE:
				if (!inputState.closePressed) {
					inputState.closePressed = true;
					inputState.eventStream.push_back( ev );
				}
				break;
			case WET_GAINED_FOCUS:
				if (!inputState.focus) {
					inputState.gainedFocus = true;
					inputState.focus = true;
					inputState.eventStream.push_back( ev );
				}
				break;
			case WET_LOST_FOCUS:
				if (inputState.focus) {
					inputState.lostFocus = true;
					inputState.focus = false;
					inputState.eventStream.push_back( ev );
				}
				break;
			}
			break;
		case IET_CHARACTER:
			inputState.eventStream.push_back( ev );
			break;
		case IET_KEY:
			switch (ev.keyEvent.type) {
			case KET_PRESSED:
				if (!inputState.keys[ev.keyEvent.keyCode]) {
					inputState.keysPressed[ev.keyEvent.keyCode] = true;
					inputState.keys[ev.keyEvent.keyCode] = true;
					inputState.eventStream.push_back( ev );
				}
				break;
			case KET_RELEASED:
				if (inputState.keys[ev.keyEvent.keyCode]) {
					inputState.keysReleased[ev.keyEvent.keyCode] = true;
					inputState.keys[ev.keyEvent.keyCode] = false;
					inputState.eventStream.push_back( ev );
				}
				break;
			}
			break;
		case IET_MOUSE_BUTTON:
			switch (ev.mouseButtonEvent.type) {
			case MBET_PRESSED:
				if (!inputState.mouseButtons[ev.mouseButtonEvent.button]) {
					inputState.mouseButtonsPressed[ev.mouseButtonEvent.button] = true;
					inputState.mouseButtons[ev.mouseButtonEvent.button] = true;
					inputState.eventStream.push_back( ev );
				}
				break;
			case MBET_RELEASED:
				if (inputState.mouseButtons[ev.mouseButtonEvent.button]) {
					inputState.mouseButtonsReleased[ev.mouseButtonEvent.button] = true;
					inputState.mouseButtons[ev.mouseButtonEvent.button] = false;
					inputState.eventStream.push_back( ev );
				}
				break;
			}
			break;
		case IET_MOUSE_WHEEL:
			inputState.mouseScrollWheel = ev.mouseWheelEvent.scroll;
			inputState.eventStream.push_back( ev );
			break;
		case IET_MOUSE_POSITION:
			inputState.mousePosition[0] = ev.mousePositionEvent.x;
			inputState.mousePosition[1] = ev.mousePositionEvent.y;
			inputState.eventStream.push_back( ev );
			break;
		case IET_MOUSE_RAW:
			inputState.mouseDelta[0] += ev.mouseRawEvent.dx;
			inputState.mouseDelta[1] += ev.mouseRawEvent.dy;
			inputState.eventStream.push_back( ev );
			break;
		default:
			assert( false ); // in case we add events but forget this
		}
	}
}

bool InputManager::mouseButton( MouseButtonCode mbc ) const {
	return inputState.mouseButtons[mbc];
}

bool InputManager::mouseButtonPressed( MouseButtonCode mbc ) const {
	return inputState.mouseButtonsPressed[mbc];
}

bool InputManager::mouseButtonReleased( MouseButtonCode mbc ) const {
	return inputState.mouseButtonsReleased[mbc];
}

float InputManager::mouseScrollWheel() const {
	return inputState.mouseScrollWheel;
}

Vector2i InputManager::mousePosition() const {
	return Vector2i( inputState.mousePosition[0], inputState.mousePosition[1] );
}

Vector2i InputManager::mouseDelta() const {
	return Vector2i( inputState.mouseDelta[0], inputState.mouseDelta[1] );
}

bool InputManager::key( byte code ) const {
	return inputState.keys[code];
}

bool InputManager::keyPressed( byte code ) const {
	return inputState.keysPressed[code];
}

bool InputManager::keyReleased( byte code ) const {
	return inputState.keysReleased[code];
}

const std::vector <InputEvent> & InputManager::getEventStream() const {
	return inputState.eventStream;
}