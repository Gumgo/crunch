/** @file GpuDrawCall.h
 *  @brief Encapsulates a draw call on the GPU.
 */

#ifndef GPUDRAWCALL_DEFINED
#define GPUDRAWCALL_DEFINED

#include "Common.h"
#include "GpuObject.h"

class GpuState;

/** @brief Encapsulates the draw state on the GPU.
 */
class GpuDrawState : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuDrawState();

	/** @brief Sets the state of the GPU to the state represented by this object.
	 */
	void bind();

	/** @brief Sets whether primitive restart is enabled.
	 */
	void setPrimitiveRestartEnabled( bool e );

	/** @brief Returns whether primitive restart is enabled.
	 */
	bool isPrimitiveRestartEnabled() const;

	/** @brief Sets the primitive restart index.
	 */
	void setPrimitiveRestartIndex( size_t index );

	/** @brief Returns the primitive restart index.
	 */
	size_t getPrimitiveRestartIndex() const;

	/** @brief Sets the clear color.
	 */
	void setClearColor( const Vector4f & color );

	/** @brief Returns the clear color.
	 */
	Vector4f getClearColor() const;

	/** @brief Sets the clear depth.
	 */
	void setClearDepth( float depth );

	/** @brief Returns the clear depth.
	 */
	float getClearDepth() const;

	/** @brief Sets the clear stencil.
	 */
	void setClearStencil( int stencil );

	/** @brief Returns the clear stencil.
	 */
	int getClearStencil() const;

private:
	friend class GpuState;
	GpuDrawState( GpuState * state );

	bool primitiveRestartEnabled;
	size_t primitiveRestartIndex;
	Vector4f clearColor;
	float clearDepth;
	int clearStencil;

	// compares changes against current state and apply differences
	void apply( GpuDrawState & currentState ) const;
	// sets values to the current OpenGL state
	void getCurrentState();
};

/** @brief Encapsulates a draw call on the GPU.
 */
class GpuDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t firstVertex, size_t vertexCount );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuDrawCall( GpuState * state );

	GLenum mode;
	size_t first;
	size_t vertices;
};

/** @brief Encapsulates an instanced draw call on the GPU.
 */
class GpuInstancedDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuInstancedDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t firstVertex, size_t vertexCount, size_t instanceCount );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuInstancedDrawCall( GpuState * state );

	GLenum mode;
	size_t first;
	size_t vertices;
	size_t instances;
};

/** @brief Encapsulates an indexed draw call on the GPU.
 */
class GpuIndexedDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuIndexedDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer );

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer,
		size_t primitiveRestartIndex );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuIndexedDrawCall( GpuState * state );

	GLenum mode;
	size_t indices;
	GLenum type;
	const void * pointer;
	bool restart;
	size_t restartIndex;
};

/** @brief Encapsulates an indexed offset draw call on the GPU.
 */
class GpuIndexedOffsetDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuIndexedOffsetDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset );

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset,
		size_t primitiveRestartIndex );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuIndexedOffsetDrawCall( GpuState * state );

	GLenum mode;
	size_t indices;
	GLenum type;
	const void * pointer;
	int offset;
	bool restart;
	size_t restartIndex;
};

/** @brief Encapsulates an indexed instanced draw call on the GPU.
 */
class GpuIndexedInstancedDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuIndexedInstancedDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount );

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount,
		size_t primitiveRestartIndex );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuIndexedInstancedDrawCall( GpuState * state );

	GLenum mode;
	size_t indices;
	GLenum type;
	const void * pointer;
	size_t instances;
	bool restart;
	size_t restartIndex;
};

/** @brief Encapsulates an indexed instanced offset draw call on the GPU.
 */
class GpuIndexedInstancedOffsetDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuIndexedInstancedOffsetDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount, int vertexOffset );

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount, int vertexOffset,
		size_t primitiveRestartIndex );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuIndexedInstancedOffsetDrawCall( GpuState * state );

	GLenum mode;
	size_t indices;
	GLenum type;
	const void * pointer;
	size_t instances;
	int offset;
	bool restart;
	size_t restartIndex;
};

/** @brief Encapsulates an indexed ranged draw call on the GPU.
 */
class GpuIndexedRangedDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuIndexedRangedDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer );

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer,
		size_t primitiveRestartIndex );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuIndexedRangedDrawCall( GpuState * state );

	GLenum mode;
	size_t smallest;
	size_t largest;
	size_t indices;
	GLenum type;
	const void * pointer;
	bool restart;
	size_t restartIndex;
};

/** @brief Encapsulates an indexed ranged offset draw call on the GPU.
 */
class GpuIndexedRangedOffsetDrawCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuIndexedRangedOffsetDrawCall();

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset );

	/** @brief Sets the draw call parameters.
	 */
	void setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset,
		size_t primitiveRestartIndex );

	/** @brief Performs the draw call.
	 */
	void draw();

private:
	friend class GpuState;
	GpuIndexedRangedOffsetDrawCall( GpuState * state );

	GLenum mode;
	size_t smallest;
	size_t largest;
	size_t indices;
	GLenum type;
	const void * pointer;
	int offset;
	bool restart;
	size_t restartIndex;
};

/** @brief Encapsulates a clear call on the GPU.
 */
class GpuClearCall : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuClearCall();

	/** @brief Sets the clear color.
	 */
	void setClearColor( const Vector4f & color );

	/** @brief Sets the clear depth.
	 */
	void setClearDepth( float depth );

	/** @brief Sets the clear stencil.
	 */
	void setClearStencil( int stencil );

	/** @brief Sets the clear buffers.
	 */
	void setClearBuffers( GLbitfield buffers );

	/** @brief Performs the draw call.
	 */
	void clear();

private:
	friend class GpuState;
	GpuClearCall( GpuState * state );

	Vector4f clearColor;
	float clearDepth;
	int clearStencil;
	GLbitfield clearBuffers;
};

#endif