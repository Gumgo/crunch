#include "ResourceManager.h"
#include "Log.h"

ResourceManager::ResourceKey::ResourceKey( size_t resourceClassId, const std::string & name ) {
	resourceClassIdName.reserve( sizeof( resourceClassId ) + name.length() );
	for (size_t i = 0; i < sizeof( resourceClassId ); ++i)
		resourceClassIdName.push_back( ((char*)(&resourceClassId))[i] );
	resourceClassIdName += name;
}

bool ResourceManager::ResourceKey::operator==( const ResourceKey & o ) const {
	return resourceClassIdName == o.resourceClassIdName;
}

size_t ResourceManager::ResourceKey::getResourceClassId() const {
	size_t ret;
	for (size_t i = 0; i < sizeof( ret ); ++i)
		((char*)(&ret))[i] = resourceClassIdName[i];
	return ret;
}

size_t ResourceManager::HashResourceKey::operator()( const ResourceKey & key ) const {
	return Hash <std::string>()( key.resourceClassIdName );
}

ResourceManager::ResourceManager()
	: resourceClassNameMap( RESOURCE_CLASS_CAPACITY * 2 )
	, resources( RESOURCE_CAPACITY )
	, paths( RESOURCE_CAPACITY ) {
	completedTasks.reserve( 100 );
	rlt.thread = boost::thread( boost::bind( &ResourceManager::ResourceLoadingThread::main, &rlt ) );
	Log::info() << "Started resource loading thread";
}

ResourceManager::~ResourceManager() {
	clear();
	flush();

	// stop the resource loading thread
	ResourceLoadingThread::Task t;
	t.type = ResourceLoadingThread::Task::T_QUIT;
	{
		boost::mutex::scoped_lock lock( rlt.mutex );
		rlt.pendingTasks.push_back( t );
	}
	rlt.tasksPending.signal();

	rlt.thread.join();
	Log::info() << "Stopped resource loading thread";
}

void ResourceManager::registerResources( KeyValueReader::Iterator root ) {
	for (size_t i = 0; i < resourceClassData.size(); ++i) {
		ResourceClassData & rcd = resourceClassData[i];
		std::string resourceCategoryName = rcd.getResourceCategoryName();
		if (!root.contains( resourceCategoryName ))
			continue;

		KeyValueReader::Iterator categoryIt = root.getArray( resourceCategoryName );

		for (size_t t = 0; t < categoryIt.getCount(); ++t) {
			std::string resName = categoryIt.getKey( t );
			std::string resPath;
			uint metadata = 0;
			// the value must be EITHER a string for path,
			// OR an array containing an anonymous string (path) followed by an anonymouns array (metadata)
			if (categoryIt.getType( t ) == KeyValueReader::VAL_STRING) {
				// a string for path
				resPath = categoryIt.getString( t );
				if (rcd.readMetadata)
					metadata = rcd.getDefaultMetadata();
			} else {
				// an array containing path followed by metadata
				KeyValueReader::Iterator pmIt = categoryIt.getArray( t );
				resPath = pmIt.getString( 0 );
				KeyValueReader::Iterator metadataIt = pmIt.getArray( 1 );

				// now we read in the metadata if a metadata reading function exists
				if (rcd.readMetadata) {
					bool failed = false;
					try {
						failed = !rcd.readMetadata( metadata, metadataIt );
					} catch (const std::exception & e) {
						Log::error() << e.what();
					}
					if (failed) {
						Log::warning() << "Failed to read metadata for " << rcd.getResourceClassName() << " resource: " << resName;
						metadata = rcd.getDefaultMetadata();
					}
				}
			}
			registerResource( i, resName, resPath, metadata );
		}
	}
}

ResourceManager::ResourceLoadingThread::ResourceLoadingThread()
	: resourceToQueueMap( 100 ) {
	signalFlag = false;
	deleteFlag = false;
}

void ResourceManager::ResourceLoadingThread::main() {
	bool quit = false;
	while (!quit) {
		// wait for a signal that new data has arrived on the queue
		tasksPending.wait();

		// lock the data structures
		mutex.lock();
		if (pendingTasks.empty())
			// if no new data, unlock immediately
			mutex.unlock();
		else {
			// if new task, pop it and unlock
			Task t = pendingTasks.front();
			pendingTasks.pop_front();
			if (t.type == Task::T_RESOURCE) {
				resourceToQueueMap.remove( t.resource );
				t.resource->setDetailedStatus( Resource::S_ASYNC_LOADING );
			}
			mutex.unlock();

			switch (t.type) {
			case Task::T_RESOURCE:
				{
					// attempt to asynchronously load the resource
					bool loaded = t.resource->loadDataAsync();

					bool signal = false;
					// add to the list of completed tasks
					{
						boost::mutex::scoped_lock lock( mutex );

						if (deleteFlag) {
							t.deleteFlag = true;
							deleteFlag = false;
						}

						completedTasks.push_back( t );
						std::list <Task>::iterator it = completedTasks.end();
						--it;
						resourceToQueueMap.put( t.resource, it );
						t.resource->setDetailedStatus( loaded ? Resource::S_SYNC_PENDING : Resource::S_SYNC_PENDING_FAILED );

						if (signalFlag) {
							signal = true;
							signalFlag = false;
						}
					}
					if (signal)
						mainThreadSignaler.signal();
				}
				break;
			case Task::T_QUIT:
				quit = true;
				break;
			case Task::T_FLUSH:
				// signal the main thread that the flush is complete
				mainThreadSignaler.signal();
				break;
			default:
				assert( false );
			}
		}
	}
}

void ResourceManager::registerResource( size_t resourceClassId, const std::string & name, const std::string & path, uint metadata ) {
	ResourceKey key( resourceClassId, name );
	if (paths.contains( key ))
		Log::warning() << "Failed to register " << resourceClassData[resourceClassId].getResourceClassName() << " resource " << name << ": already registered";
	else
		paths.put( key, std::make_pair( path, metadata ) );
}

void ResourceManager::acquire( Resource * resource, bool block ) {
	if (!block) {
		// check the status of the resource and proceed accordingly
		if (resource->getDetailedStatus() == Resource::S_NOT_LOADED) {
			// add the resource to the queue task queue on the resource loading thread
			resource->setDetailedStatus( Resource::S_ASYNC_PENDING );

			// set up the task with the resource and its class data
			ResourceLoadingThread::Task t;
			t.type = ResourceLoadingThread::Task::T_RESOURCE;
			t.resource = resource;
			t.deleteFlag = false;

			// add to the queue and signal that there are new tasks
			{
				boost::mutex::scoped_lock lock( rlt.mutex );
				rlt.pendingTasks.push_back( t );
				std::list <ResourceLoadingThread::Task>::iterator it = rlt.pendingTasks.end();
				--it;
				rlt.resourceToQueueMap.put( resource, it );
			}
			rlt.tasksPending.signal();

			Log::info() << "Added " << resource->getResourceClassName() << " resource " << resource->getName();
		}
		// if the status is anything but S_NOT_LOADED, we don't need to do anything (the resource has already been through this process)
	} else
		blockUntilLoaded( resource );
}

void ResourceManager::release( Resource * resource, bool force ) {
	if (resource->getReferenceCount() != 0 && !force)
		// there are still references - don't do anything
		return;

	bool callFree = false;			// whether we should free the resource
	bool deleteResource = false;	// whether we should (immediately) delete the resource

	// reference count is 0, we need to delete the resource
	// we need to take the appropriate action based on what the status of the resource is
	rlt.mutex.lock();
	switch (resource->getDetailedStatus()) {
	// S_NOT_LOADED will never happen - if it does, it will be caught by default
	case Resource::S_ASYNC_PENDING:
		// we remove it from the async queue and load it immediately
		{
			std::list <ResourceLoadingThread::Task>::iterator it = rlt.resourceToQueueMap.extract( resource );
			rlt.pendingTasks.erase( it );
		}
		rlt.mutex.unlock();
		deleteResource = true;
		break;
	case Resource::S_ASYNC_LOADING:
		// we flag the resource to be deleted once async loading completes on the next sync cycle
		rlt.deleteFlag = true;
		rlt.mutex.unlock();
		break;
	case Resource::S_SYNC_PENDING:
		// we remove it from the sync queue
		{
			std::list <ResourceLoadingThread::Task>::iterator it = rlt.resourceToQueueMap.extract( resource );
			rlt.completedTasks.erase( it );
		}
		rlt.mutex.unlock();
		callFree = true;
		deleteResource = true;
		break;
	case Resource::S_SYNC_PENDING_FAILED:
		// we remove it from the sync queue
		// don't set callFree since loading failed
		{
			std::list <ResourceLoadingThread::Task>::iterator it = rlt.resourceToQueueMap.extract( resource );
			rlt.completedTasks.erase( it );
		}
		rlt.mutex.unlock();
		deleteResource = true;
		break;
	case Resource::S_FAILED:
		rlt.mutex.unlock();
		deleteResource = true;
		break;
	case Resource::S_LOADED:
		rlt.mutex.unlock();
		callFree = true;
		deleteResource = true;
		break;
	default:
		assert( false );
	}

	// free the resource if necessary
	if (callFree)
		resource->freeData();

	// remove the resource from the table
	ResourceKey key( resourceClassNameMap.get( resource->getResourceClassName() ), resource->getName() );
	resources.remove( key );

	Log::info() << "Removed " << resource->getResourceClassName() << " resource " << resource->getName();

	// delete the resource if necessary
	if (deleteResource)
		delete resource;
}

void ResourceManager::blockUntilLoaded( Resource * resource ) {
	bool loadAsync = false; // do we need to load async before loading sync?

	// we need to take the appropriate action based on what the status of the resource is
	rlt.mutex.lock();
	switch (resource->getDetailedStatus()) {
	case Resource::S_NOT_LOADED:
		// in this case we load it immediately
		rlt.mutex.unlock();
		loadAsync = true;
		Log::info() << "Added " << resource->getResourceClassName() << " resource " << resource->getName();
		break;
	case Resource::S_ASYNC_PENDING:
		// we remove it from the async queue and load it immediately
		{
			std::list <ResourceLoadingThread::Task>::iterator it = rlt.resourceToQueueMap.extract( resource );
			rlt.pendingTasks.erase( it );
		}
		rlt.mutex.unlock();
		loadAsync = true;
		break;
	case Resource::S_ASYNC_LOADING:
		// we wait until async loading is finished, then load synchronously immediately
		// signal that the async thread should alert the main thread once it is done
		rlt.signalFlag = true;
		rlt.mutex.unlock();

		// wait until asynchronous loading has completed
		rlt.mainThreadSignaler.wait();

		{
			boost::mutex::scoped_lock lock( rlt.mutex );
			std::list <ResourceLoadingThread::Task>::iterator it = rlt.resourceToQueueMap.extract( resource );
			rlt.completedTasks.erase( it );
		}
		break;
	case Resource::S_SYNC_PENDING:
		// we remove it from the sync queue and load it immediately
		{
			std::list <ResourceLoadingThread::Task>::iterator it = rlt.resourceToQueueMap.extract( resource );
			rlt.completedTasks.erase( it );
		}
		rlt.mutex.unlock();
		break;
	case Resource::S_SYNC_PENDING_FAILED:
		// we remove it from the sync queue
		{
			std::list <ResourceLoadingThread::Task>::iterator it = rlt.resourceToQueueMap.extract( resource );
			rlt.completedTasks.erase( it );
		}
		rlt.mutex.unlock();
		break;
	case Resource::S_FAILED:
	case Resource::S_LOADED:
		// return immediately - it's already completed
		// it's already been through the sync queue so we don't need to signal any listeners again
		rlt.mutex.unlock();
		return;
	default:
		assert( false );
	}

	if (loadAsync) {
		// load the resource asynchronously
		if (!resource->loadDataAsync())
			// S_SYNC_PENDING_FAILED means it failed during the async phase
			resource->setDetailedStatus( Resource::S_SYNC_PENDING_FAILED );
	}

	if (resource->getDetailedStatus() != Resource::S_SYNC_PENDING_FAILED) {
		// load the resource synchronously
		bool loaded = resource->loadDataSync();
		resource->setDetailedStatus( loaded ? Resource::S_LOADED : Resource::S_FAILED );
	} else
		resource->setDetailedStatus( Resource::S_FAILED );

	resource->setStatus( resource->getDetailedStatus() );
	if (resource->getStatus() == Resource::S_LOADED)
		Log::info() << "Loaded " << resource->getResourceClassName() << " resource " << resource->getName();
	else
		Log::warning() << "Failed to load " << resource->getResourceClassName() << " resource " << resource->getName();

	// TODO (possibly): alert listeners
}

void ResourceManager::flush() {
	// set up the task with the resource and its class data
	ResourceLoadingThread::Task t;
	t.type = ResourceLoadingThread::Task::T_FLUSH;

	// add to the queue and signal that there are new tasks
	{
		boost::mutex::scoped_lock lock( rlt.mutex );
		rlt.pendingTasks.push_back( t );
	}
	rlt.tasksPending.signal();
	// wait until the loading thread encounters the task we just sent - that means it is flushed
	rlt.mainThreadSignaler.wait();
	// update all synchronous loading
	update();
}

void ResourceManager::clear() {
	// just go through all possible indices
	for (size_t i = 0; !resources.empty(); ++i) {
		if (resources.containsIndex( i ))
			release( resources.getByIndex( i ), true );
	}
}

void ResourceManager::update() {
	{
		boost::mutex::scoped_lock lock( rlt.mutex );
		while (!rlt.completedTasks.empty()) {
			completedTasks.push_back( rlt.completedTasks.front() );
			rlt.completedTasks.pop_front();
		}
	}

	// we can now safely check the detailed status without a mutex because the resource is now only on this thread,
	// and the previous mutex should have taken care of memory barrier issues
	for (size_t i = 0; i < completedTasks.size(); ++i) {
		ResourceLoadingThread::Task & t = completedTasks[i];
		assert( t.type == ResourceLoadingThread::Task::T_RESOURCE ); // only these should be on the queue

		if (t.deleteFlag) {
			// this means the resource's reference count went to 0 while asynchronous loading was occurring
			// the resource should be freed and deleted
			if (t.resource->getDetailedStatus() == Resource::S_SYNC_PENDING)
				// only free if the previous async loading acutally succeeded - status will be S_SYNC_PENDING_FAILED otherwise
				t.resource->freeData();
			delete t.resource;
		} else {
			// load the resource synchronously, but only if the previous phase of loading succeeded
			if (t.resource->getDetailedStatus() == Resource::S_SYNC_PENDING) {
				bool loaded = t.resource->loadDataSync();
				t.resource->setDetailedStatus( loaded ? Resource::S_LOADED : Resource::S_FAILED );
			} else
				t.resource->setDetailedStatus( Resource::S_FAILED );

			// now update the main status
			t.resource->setStatus( t.resource->getDetailedStatus() );
			if (t.resource->getStatus() == Resource::S_LOADED)
				Log::info() << "Loaded " << t.resource->getResourceClassName() << " resource " << t.resource->getName();
			else
				Log::warning() << "Failed to load " << t.resource->getResourceClassName() << " resource " << t.resource->getName();

			// TODO (possibly): alert listeners
		}
	}

	completedTasks.clear();
}