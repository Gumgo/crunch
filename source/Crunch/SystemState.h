#ifndef SYSTEMSTATE_DEFINED
#define SYSTEMSTATE_DEFINED

#include "Common.h"
#include "GameState.h"
#include "KeyValueReader.h"

DEFINE_GAME_STATE( SystemState, std::numeric_limits <int>::min(), 0 )
public:
	static SystemState & get();	// gets system state
	void onCreate();	// reads from SystemResources file
	void onUpdate();	// updates the counter
	void onDestroy();

	// returns a value that increments each frame
	uint getCounter() const;

private:
	static SystemState * systemState;

	bool counterStarted;
	uint counter;
};

#endif