/** @file Renderer.h
 *  @brief Orders and executes draw calls.
 */

#ifndef RENDERER_DEFINED
#define RENDERER_DEFINED

#include "Common.h"
#include "FastDelegate.h"
#include "RadixSort.h"
#include <vector>

/** @brief Orders and executes draw calls.
 */
class Renderer : private boost::noncopyable {
public:
	/** @brief The call type, which is a delegate taking 0 parameters.
	 */
	typedef fastdelegate::FastDelegate0 <> Call;

	/** @brief The key used to order rendering.
	 */
	struct Key {
		byte order[5];	/**< The ordering key.*/
	};

	Renderer();							/**< Reserves space in the calls list.*/
	void submit( Call call, Key key );	/**< Submits a render call.*/
	void execute();						/**< Executes render calls.*/

	static Key makeKey( byte pass, int order );		/**< Returns a key with the given pass and integer ordering.*/
	static Key makeKey( byte pass, uint order );	/**< Returns a key with the given pass and unsigned integer ordering.*/
	static Key makeKey( byte pass, float order );	/**< Returns a key with the given pass and floating point ordering.*/
	static Key makeKeyFirst( byte pass );			/**< Returns a key with the given pass and first ordering.*/
	static Key makeKeyLast( byte pass );			/**< Returns a key with the given pass and last ordering.*/

private:
	// list of pending calls
	std::vector <std::pair <Call, Key>> calls;
	// buffer for sorting
	std::vector <RadixBucketEntry <std::pair <Call, Key>>> sortBuffer;
};

#endif