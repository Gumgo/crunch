/** @file Timer.h
 *  @brief Contains timing functionality.
 */

#ifndef TIMER_DEFINED
#define TIMER_DEFINED

#include "Common.h"
#include "OpenGL.h"
#include <boost/thread.hpp>
#include <vector>

#if defined CRUNCH_PLATFORM_WINDOWS
#include <Windows.h>
#elif defined CRUNCH_PLATFORM_APPLE
#include <mach/mach_time.h>
#include <CoreServices/CoreServices.h>
#elif defined CRUNCH_PLATFORM_LINUX
#error Linux not yet supported!
#endif

/** @brief This class contains functionality to maintain a fixed frame rate.
 */
class Timer : private boost::noncopyable {
public:
#if defined CRUNCH_PLATFORM_WINDOWS
	typedef LONGLONG TimerValue;	/**< The type for storing time values.*/
#elif defined CRUNCH_PLATFORM_APPLE
	typedef uint64_t TimerValue;
#elif defined CRUNCH_PLATFORM_LINUX
#error Linux not yet supported!
#else
#error Unsupported platform!
#endif

	/** @brief The type of timing query.
	 */
	enum TimingQueryType {
		TQ_CPU,	/**< A query for timing on the CPU.*/
		TQ_GPU	/**< A query for timing on the GPU.*/
	};

	/** @brief Stores timing query information.
	 */
	struct TimingQueryData {
		std::string name;		/**< The name of this query.*/
		bool completed;			/**< Whether this query was completed.*/
		TimingQueryType type;	/**< The type of query.*/
		double queryTime;		/**< The time of the query in milliseconds.*/
	};

	/** @brief The constructor.
	 */
	Timer();

	/** @brief The destructor ends the timing session if it is active.
	 */
	~Timer();

	/** @brief Starts the timer.
	 *
	 *  @param framesPerSecond	The desired frame rate.
	 *  @param maxLagFrames		The maximum amount of lag allowed in frames.
	 */
	void start( uint framesPerSec = 60, uint maxLagFrames = 20 );

	/** @brief Stops the timer.
	 */
	void stop();

	/** @brief Called at the end of each frame.
	 *
	 *  This function causes the game to sleep
	 *  if the frame finished too fast and does
	 *  nothing otherwise.
	 */
	void update();

	/** @brief Gives up the timeslice to avoid throttling other processes.
	 */
	static void endTimeSlice();

	/** @brief Returns the frequency of the timer.
	 */
	TimerValue getFrequency() const;

	/** @brief Returns the current frame's time in terms of frequency.
	 */
	TimerValue getFrameTime() const;

	/** @brief Returns the current frame count.
	 */
	TimerValue getFrameCount() const;

	/** @brief Returns the current time in terms of frequency.
	 */
	TimerValue getCurrentTime() const;

	/** @brief Returns the number of frames per second.
	 */
	uint getFramesPerSecond() const;

	/** @brief Returns the number of frequency units in a frame.
	 */
	TimerValue getFrameLength() const;

	/** @brief Returns the length of a frame as a floating point value in seconds.
	 */
	float getFrameLengthSeconds() const;

	/** @brief Returns the system time for this frame in terms of frequency.
	 */
	TimerValue getSystemTime() const;

	/** @brief Returns the system time for this frame in terms of milliseconds.
	 */
	TimerValue getSystemTimeMillis() const;

	/** @brief Begins a timing query and returns its index.
	 */
	size_t beginTimingQuery( const std::string & name, TimingQueryType type );

	/** @brief Ends a timing query and returns its index.
	 */
	void endTimingQuery( size_t id );

	/** @brief Returns the number of timing queries for the last frame.
	 */
	size_t getTimingQueryCount() const;

	/** @brief Returns the timing query from the last frame with the given index.
	 */
	const TimingQueryData & getTimingQueryResult( size_t i ) const;

private:
	TimerValue frequency;		// the counter frequency in hz

	bool running;				// whether the timer is running
	TimerValue baseTime;		// the base time when the timer was started, in frequency units
	TimerValue lagTime;			// the amount of frozen or lagged time used to offset the base time, in frequency units
	TimerValue frameLength;		// the number of frequency units in a frame
	TimerValue maxLagTime;		// the maximum allowed lag time, in frequency units
	TimerValue frames;			// the current number of frames that have occurred
	TimerValue systemTime;		// the time of the system of the last update, in frequency units
	float frameLengthSeconds;	// the length of a frame in seconds
	uint framesPerSecond;		// the number of frames per second

	TimerValue querySystemClock() const; // queries the system clock

	// wraps TimingQueryData with some additional information
	struct TimingQueryDataWrapper {
		TimingQueryData data;	// the query data
		TimerValue clockTime;	// CPU clock time
		GLuint gpuQueries[2];	// the GPU query IDs
	};

	std::vector <TimingQueryDataWrapper> previousTimingQueries;	// timing queries for the last frame
	std::vector <TimingQueryDataWrapper> currentTimingQueries;	// timing queries for this frame
};

#endif