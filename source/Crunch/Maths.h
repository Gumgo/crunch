/** @file Maths.h
 *  @brief Includes all math related classes and functions.
 */

#ifndef MATHS_DEFINED
#define MATHS_DEFINED

#include "Maths_Includes.h"

#include "Maths_Util.h"

#include "Maths_Vector2Dec.h"
#include "Maths_Vector3Dec.h"
#include "Maths_Vector4Dec.h"
#include "Maths_Matrix22Dec.h"
#include "Maths_Matrix33aDec.h"
#include "Maths_Matrix33Dec.h"
#include "Maths_Matrix44aDec.h"
#include "Maths_Matrix44Dec.h"
#include "Maths_AxisAngleDec.h"
#include "Maths_QuaternionDec.h"
#include "Maths_LineDec.h"
#include "Maths_PlaneDec.h"
#include "Maths_Ray2Dec.h"
#include "Maths_Ray3Dec.h"

#include "Maths_Vector2Def.h"
#include "Maths_Vector3Def.h"
#include "Maths_Vector4Def.h"
#include "Maths_Matrix22Def.h"
#include "Maths_Matrix33aDef.h"
#include "Maths_Matrix33Def.h"
#include "Maths_Matrix44aDef.h"
#include "Maths_Matrix44Def.h"
#include "Maths_AxisAngleDef.h"
#include "Maths_QuaternionDef.h"
#include "Maths_LineDef.h"
#include "Maths_PlaneDef.h"
#include "Maths_Ray2Def.h"
#include "Maths_Ray3Def.h"

#endif