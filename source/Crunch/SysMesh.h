#ifndef SYSMESH_DEFINED
#define SYSMESH_DEFINED

#include "Common.h"
#include "MeshResource.h"
#include "SysCamera3D.h"
#include "SysAutoProgram.h"

class SysMesh : private boost::noncopyable {
public:
	SysMesh();
	~SysMesh();
	void setMesh( MeshResourceReference & m );
	MeshResourceReference getMesh() const;

	void draw( SysAutoProgram & ap );

private:
	MeshResourceReference mesh;
};

#endif