#ifndef MATHS_RAY2DEF_DEFINED
#define MATHS_RAY2DEF_DEFINED

template <typename T> Ray2 <T>::Ray2() {
}

template <typename T> Ray2 <T>::Ray2( const Vector2 <T> & orig, const Vector2 <T> & dir )
	: origin( orig )
	, direction( dir ) {
}

template <typename T> Ray2 <T>::Ray2( const Ray2 <T> & r )
	: origin( r.origin )
	, direction( r.direction ) {
}

template <typename T> Ray2 <T>::~Ray2() {
}

template <typename T> Ray2 <T> Ray2 <T>::operator=( const Ray2 <T> & r ) {
	origin = r.origin;
	direction = r.direction;
	return *this;
}

template <typename T> bool Ray2 <T>::operator==( const Ray2 <T> & r ) const {
	return (origin == r.origin && direction == r.direction);
}

template <typename T> bool Ray2 <T>::operator!=( const Ray2 <T> & r ) const {
	return (origin != r.origin || direction != r.direction);
}

template <typename T> template <typename U> Ray2 <T>::operator Ray2 <U> () const {
	return Ray2 <U>( (Vector2 <U>)origin, (Vector2 <U>)direction );
}

template <typename T> void Ray2 <T>::set( const Vector2 <T> & orig, const Vector2 <T> & dir ) {
	origin = orig;
	direction = dir;
}

#endif