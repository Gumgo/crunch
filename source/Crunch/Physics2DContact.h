/** @file Physics2DContact.h
 *  @brief Defines the class representing a contact between two objects in the simulation.
 */

#ifndef PHYSICS2DCONTACT_DEFINED
#define PHYSICS2DCONTACT_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DContactManifold.h"
#include "FastDelegate.h"

namespace Physics2D {

class Shape;

/** @brief Represents a contact between two objects in the simulation.
 */
class Contact {
public:
	friend class ContactManager;
	friend class Shape;

	Contact( Shape * s0, Shape * s1 );	/**< Constructs the contact with the given shapes.*/

	void computeManifold();					/**< Computes the contact manifold.*/
	const Manifold & getManifold() const;	/**< Returns the contact manifold.*/

	Shape * getShape( size_t idx );				/**< Returns one of the two shapes.*/
	const Shape * getShape( size_t idx ) const;	/**< Returns one of the two shapes.*/

	static const size_t NO_INDEX = 2;				/**< Represents an invalid shape.*/
	size_t getShapeIndex( const Shape * s ) const;	/**< Returns the index of the given shape, or NO_INDEX if the shape is invalid.*/

private:
	Shape * shapes[2];	// the two shapes which are in contact
	Manifold manifold;	// contact manifold

	// data used while the simulation is updating.
	struct SimulationData {
		Contact * next;			// the next contact in the simulation
		Contact * prev;			// the previous contact in the simulation

		Contact * shapeNext[2];	// the next contact in the contact list for each shape
		Contact * shapePrev[2];	// the previous contact in the contact list for each shape
	};

	SimulationData simulationData;
	SimulationData & getSimulationData();
	const SimulationData & getSimulationData() const;
};

}

#endif