#ifndef SYSDEFERREDSPOTLIGHT_DEFINED
#define SYSDEFERREDSPOTLIGHT_DEFINED

#include "Common.h"
#include "SysDeferredLight.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"
#include "GpuTexture.h"
#include "Cone.h"
#include "SysFrustum.h"

class SysDeferredSpotLight : public SysDeferredLight {
public:
	SysDeferredSpotLight();
	~SysDeferredSpotLight();

	void enableShadow(
		size_t size,
		float minNear = 0.1f, float maxFar = std::numeric_limits <float>::max(),
		float constantBias = 0.1f, float maxMultiplierBias = 0.1f );

	void setColor( const Vector3f & c );
	Vector3f getColor() const;
	void setPosition( const Vector3f & p );
	Vector3f getPosition() const;
	void setDirection( const Vector3f & d );
	Vector3f getDirection() const;
	void setConeAngles( const Vector2f & innerOuter );
	Vector2f getConeAngles() const;
	void setAttenuation( AttenuationMode mode, const Vector3f & params );
	AttenuationMode getAttenuationMode() const;
	Vector3f getAttenuationParameters() const;

	void computeShadowMapRange(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb );
	void drawShadowMap(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
		fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject );

	void draw();
	void drawGeometry();

private:
	ProgramResourceReference coneProgram;
	ProgramResourceReference coneShadowProgram;
	ProgramResourceReference quadProgram;
	ProgramResourceReference quadShadowProgram;
	ProgramResourceReference geomProgram;

	GpuTextureReference shadowMap;
	float shadowMinNear, shadowMaxFar;
	static const float FAR_RANGE_MULTIPLIER;

	AttenuationMode attenuationMode;
	Matrix44af lightMatrix;
	Matrix44af coneMatrix;
	Matrix44f projectionMatrix;

	SysAutoProgram autoConeProgram;
	SysAutoProgram autoQuadProgram;
	SysAutoProgram autoGeomProgram;
	struct {
		Matrix44af modelViewMatrix;
		Matrix44f projectionMatrix;
		Vector2f screenSize;
		float tanAngle;
		float radius;
		Vector2f imagePlaneHalfSize;
		int normalSpecMap;
		int depthMap;
		Vector3f color;
		int quadraticAttenuation;
		Vector3f attenuation;
		Vector2f coneAngles; // inner, outer
		Vector3f viewSpacePosition;
		Vector3f viewSpaceDirection;

		int faceNormalMap;
		int shadowMap;
		Matrix44af viewToLightMatrix;
		float halfResolution;
		float halfSize;
		float constantBias;
		float maxMultiplierBias;
		Vector2f lightNearFar;
	} uniforms;

	Vector3f position;
	Vector3f direction;
	float range;

	Matrix44af computeLightMatrix() const;
	Matrix44af computeConeMatrix() const; // inverse of light matrix, used to transform geometry
	Matrix44f computeProjectionMatrix() const;
	SysFrustum computeCullingFrustum() const;

	Vector2f getZRange() const;
};

#endif