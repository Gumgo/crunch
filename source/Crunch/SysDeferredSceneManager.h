#ifndef SYSDEFERREDSCENEMANAGER_DEFINED
#define SYSDEFERREDSCENEMANAGER_DEFINED

#include "Common.h"
#include "SysAabb.h"
#include "SysCamera3D.h"
#include "SysDeferredLight.h"
#include "Renderer.h"
#include "Indexer.h"
#include "FastDelegate.h"

struct SceneObject {
	// AABB for render/shadow map culling as well as determining shadow map bounds
	SysAabb aabb;

	// function responsible for registering the draw calls with the renderer
	// receives the camera from which drawing is taking place
	typedef fastdelegate::FastDelegate1 <const SysCamera3D*, void> DrawRegistrator;

	// function to register draw calls
	DrawRegistrator drawRegistrator;
	// NULL if the object shouldn't cast shadows
	SysDeferredLight::ShadowMapRenderer shadowMapRenderer;

	bool enabled;

	SceneObject();
};

struct SceneLight {
	// the light
	SysDeferredLight * light;

	bool enabled;

	SceneLight();
};

// TODO: this is just a temp implementation for now
// eventually use more complex systems to manage culling, etc.
class SysDeferredSceneManager {
public:
	SysDeferredSceneManager();

	void setCamera( const SysCamera3D * cam );

	// adds a scene object
	size_t addObject( const SceneObject & sceneObject );
	// removes a scene object
	void removeObject( size_t index );
	// updates a scene object's AABB
	void updateObjectAabb( size_t index, const SysAabb & aabb );
	// sets whether the object is enabled
	void setObjectEnabled( size_t index, bool e );

	// adds a scene light
	size_t addLight( const SceneLight & sceneLight );
	// removes a scene light
	void removeLight( size_t index );
	// notifies the scene manager that the light was updated
	void lightUpdated( size_t index );
	// sets whether the object is enabled
	void setLightEnabled( size_t index, bool e );

	// culls the scene and registers draw calls with the renderer
	void registerDrawCalls();

private:
	// the list of scene objects
	Indexer <SceneObject> sceneObjects;
	// the list of lights
	Indexer <SceneLight> sceneLights;

	const SysCamera3D * camera;

	Renderer::Key shadowMapPassKey;
	Renderer::Call shadowMapPassCall;
	void drawShadowMaps();
};

#endif