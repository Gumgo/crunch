/** @file AudioManager.h
 *  @brief Manages all audio (sound and music).
 */

#ifndef AUDIOMANAGER_DEFINED
#define AUDIOMANAGER_DEFINED

#include "Common.h"
#include "Audio.h"
#include "Indexer.h"
#include "SoundResource.h"
#include "HashMap.h"
#include "HashFunctions.h"

/** @brief Manages all audio (sound and music).
 */
class AudioManager {
public:
	/** @brief Initializes the audio playback system.
	 */
	AudioManager();

	/** @brief Terminates the audio playback system.
	 */
	~AudioManager();

	/** @brief Returns a pointer to the FMOD system.
	 */
	FMOD::System * getSystem();

	/** @brief Updates the audio system.
	 */
	void update();

	/** @brief Creates a sound group.
	 */
	SoundGroup createSoundGroup();

	/** @brief Destroys a sound group.
	 */
	void destroySoundGroup( const SoundGroup & sg );

	/** @brief Returns the default sound group.
	 */
	SoundGroup getDefaultSoundGroup();

	/** @brief Sets the 3D listener attributes.
	 */
	void set3DListenerAttributes( const Vector3f & pos, const Vector3f & vel, const Vector3f & fwd, const Vector3f & up );

	// object-oriented functions

	/** @brief Creates a 2D sound.
	 */
	Sound2D createSound2D( const SoundGroup & sg, SoundResourceReference & sound, bool createPaused );

	/** @brief Creates a 3D sound.
	 */
	Sound3D createSound3D( const SoundGroup & sg, SoundResourceReference & sound, const Vector3f & pos, const Vector3f & vel, bool createPaused );

	void play( const SoundGroup & sg );					/**< Plays/unpauses all sounds in the sound group.*/
	void pause( const SoundGroup & sg );				/**< Pauses all sounds in the sound group.*/
	void stop( const SoundGroup & sg );					/**< Stops all sounds in the sound group.*/
	void setVolume( const SoundGroup & sg, float vol );	/**< Scales the volume of all sounds in the sound group.*/

	void play( const Sound & s );					/**< Plays/unpauses the sound.*/
	void pause( const Sound & s );					/**< Pauses the sound.*/
	void stop( const Sound & s );					/**< Stops the sound.*/
	void setVolume( const Sound & s, float vol );	/**< Sets the sound's volume.*/

	void setPanning( const Sound2D & s, float pan );	/**< Sets the sound's panning.*/

	void setPositionVelocity( const Sound3D & s, const Vector3f & pos, const Vector3f & vel );	/**< Sets the sound's position in meters and velocity in meters per second.*/

private:
	FMOD::System * system;	// pointer to the FMOD system

	// data for a sound group
	struct InternalSoundGroup {
		// currently no data is needed
	};

	// data for a sound
	struct InternalSound {
		SoundResourceReference resource;
	};

	// sound groups
	HashMap <Hash <FMOD::ChannelGroup*>, FMOD::ChannelGroup*, InternalSoundGroup> soundGroups;

	// sounds
	HashMap <Hash <FMOD::Channel*>, FMOD::Channel*, InternalSound> sounds;

	// callback function to handle channel events
	static FMOD_RESULT F_CALLBACK channelCallback( FMOD_CHANNEL * channel, FMOD_CHANNEL_CALLBACKTYPE type, void * commanddata1, void * commanddata2 );

	// number of channels to use
	static const int MAX_CHANNELS = 100;
};

#endif