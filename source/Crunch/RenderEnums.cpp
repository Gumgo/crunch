#if 0

#include "RenderEnums.h"

static const GlslTypeDescription glslTypeDescriptions[] = {
	{ GL_FLOAT,							GL_FLOAT,							1 },
	{ GL_FLOAT_VEC2,					GL_FLOAT,							2 },
	{ GL_FLOAT_VEC3,					GL_FLOAT,							3 },
	{ GL_FLOAT_VEC4,					GL_FLOAT,							4 },
	{ GL_INT,							GL_INT,								1 },
	{ GL_INT_VEC2,						GL_INT,								2 },
	{ GL_INT_VEC3,						GL_INT,								3 },
	{ GL_INT_VEC4,						GL_INT,								4 },
	{ GL_UNSIGNED_INT,					GL_UNSIGNED_INT,					1 },
	{ GL_UNSIGNED_INT_VEC2,				GL_UNSIGNED_INT,					2 },
	{ GL_UNSIGNED_INT_VEC3,				GL_UNSIGNED_INT,					3 },
	{ GL_UNSIGNED_INT_VEC4,				GL_UNSIGNED_INT,					4 },
	{ GL_BOOL,							GL_BOOL,							1 },
	{ GL_BOOL_VEC2,						GL_BOOL,							2 },
	{ GL_BOOL_VEC3,						GL_BOOL,							3 },
	{ GL_BOOL_VEC4,						GL_BOOL,							4 },
	{ GL_FLOAT_MAT2,					GL_FLOAT,							4 },
	{ GL_FLOAT_MAT3,					GL_FLOAT,							9 },
	{ GL_FLOAT_MAT4,					GL_FLOAT,							16 },
	{ GL_FLOAT_MAT2x3,					GL_FLOAT,							6 },
	{ GL_FLOAT_MAT2x4,					GL_FLOAT,							8 },
	{ GL_FLOAT_MAT3x2,					GL_FLOAT,							6 },
	{ GL_FLOAT_MAT3x4,					GL_FLOAT,							12 },
	{ GL_FLOAT_MAT4x2,					GL_FLOAT,							8 },
	{ GL_FLOAT_MAT4x3,					GL_FLOAT,							12 },
	{ GL_SAMPLER_1D,					GL_SAMPLER_1D,						1 },
	{ GL_SAMPLER_2D,					GL_SAMPLER_2D,						1 },
	{ GL_SAMPLER_3D,					GL_SAMPLER_3D,						1 },
	{ GL_SAMPLER_CUBE,					GL_SAMPLER_CUBE,					1 },
	{ GL_SAMPLER_1D_ARRAY,				GL_SAMPLER_1D_ARRAY,				1 },
	{ GL_SAMPLER_2D_ARRAY,				GL_SAMPLER_2D_ARRAY,				1 },
	{ GL_SAMPLER_CUBE_MAP_ARRAY,		GL_SAMPLER_CUBE_MAP_ARRAY,			1 },
	{ GL_SAMPLER_1D_SHADOW,				GL_SAMPLER_1D_SHADOW,				1 },
	{ GL_SAMPLER_2D_SHADOW,				GL_SAMPLER_2D_SHADOW,				1 },
	{ GL_SAMPLER_CUBE_SHADOW,			GL_SAMPLER_CUBE_SHADOW,				1 },
	{ GL_SAMPLER_1D_ARRAY_SHADOW,		GL_SAMPLER_1D_ARRAY_SHADOW,			1 },
	{ GL_SAMPLER_2D_ARRAY_SHADOW,		GL_SAMPLER_2D_ARRAY_SHADOW,			1 },
	{ GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW,	GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW,	1 }
};

struct HashGLenum {
	size_t operator()( GLenum e ) const {
		return (size_t)e;
	}
};

HashMap <HashGLenum, GLenum, size_t> initTypeDescriptions() {
	HashMap <HashGLenum, GLenum, size_t> table( arraySize( glslTypeDescriptions ) * 2 );
	for (size_t i = 0; i < arraySize( glslTypeDescriptions ); ++i)
		table.put( glslTypeDescriptions[i].type, i );
	return table;
}

static HashMap <HashGLenum, GLenum, size_t> typeDescriptionTable = initTypeDescriptions();

GlslTypeDescription getTypeDescription( GLenum type ) {
	size_t descIndex;
	if (!typeDescriptionTable.get( type, descIndex ))
		throw std::runtime_error( "Invalid or unused type requested" );
	return glslTypeDescriptions[descIndex];
}

#endif