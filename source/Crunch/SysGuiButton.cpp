#include "SysGuiButton.h"
#include "Context.h"
#include "SysRenderer2D.h"

bool SysGuiButton::pointInside( const Vector2f & point ) const {
	return (point.x >= (float)position.x && point.y >= (float)position.y &&
			point.x < (float)(position.x + width) && point.y < (float)(position.y + 16));
}

void SysGuiButton::onCreate() {
	SysGui::onCreate();
	down = false;
	width = 64;
}

void SysGuiButton::move( const Vector2i & pos ) {
	position = pos;
}

void SysGuiButton::setWidth( int w ) {
	width = w;
}

void SysGuiButton::setText( const std::string & t ) {
	text = t;
}

void SysGuiButton::setOnClick( const boost::function <void ()> & callback ) {
	onClick = callback;
}

void SysGuiButton::receiveEvent( const InputEvent & ev ) {
	if (ev.type == IET_MOUSE_BUTTON && ev.mouseButtonEvent.button == MBC_LEFT &&
		(ev.mouseButtonEvent.type == MBET_PRESSED || ev.mouseButtonEvent.type == MBET_RELEASED)) {

		Vector2i mouseWindow = Context::get().inputManager.mousePosition();
		Vector2f mouseWorld = camera->screenToWorld( Vector2f(
			(float)mouseWindow.x / (float)Context::get().window.getWidth(),
			(float)mouseWindow.y / (float)Context::get().window.getHeight() ) );

		if (pointInside( mouseWorld )) {
			if (ev.mouseButtonEvent.type == MBET_PRESSED)
				down = true;
			else if (down && ev.mouseButtonEvent.type == MBET_RELEASED) {
				down = false;
				if (onClick)
					onClick();
			}
		} else if (ev.mouseButtonEvent.type == MBET_RELEASED)
			down = false;
	}
}

void SysGuiButton::onDismissParent() {
	down = false;
}

void SysGuiButton::draw() {
	if (camera != NULL) {
		Matrix33af modelMatrix;
		modelMatrix.translate( (float)position.x, (float)position.y );
		Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

		GpuBlendState blendState = Context::get().gpuState.createBlendState();
		blendState.setBlendFunction( GpuBlendState::BM_NONE );
		blendState.bind();

		SysRenderer2D::get().drawGradientVert( mvpMatrix,
			Vector2f( 0.0f, 0.0f ), Vector2f( (float)width, 16.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );

		Vector4f c1( 0.75f, 0.75f, 1.0f, 1.0f );
		Vector4f c2( 0.375f, 0.375f, 0.5f, 1.0f );

		Vector2i mouseWindow = Context::get().inputManager.mousePosition();
		Vector2f mouseWorld = camera->screenToWorld( Vector2f(
			(float)mouseWindow.x / (float)Context::get().window.getWidth(),
			(float)mouseWindow.y / (float)Context::get().window.getHeight() ) );

		if (pointInside( mouseWorld )) {
			if (down)
				std::swap( c1, c2 );
			else {
				c1.set( 0.75f, 0.75f, 1.0f, 1.0f );
				c2.set( 0.5625f, 0.5625f, 0.75f, 1.0f );
			}
		}

		SysRenderer2D::get().drawGradientVert( mvpMatrix,
			Vector2f( 1.0f, 1.0f ), Vector2f( (float)width - 2.0f, 14.0f ),
			c1, c2 );

		modelMatrix.translate( (float)(width / 2), 8.0f );
		mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

		SysRenderer2D::get().drawText( text, *font, mvpMatrix, TAH_CENTER, TAV_MIDDLE, Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );
	}
}