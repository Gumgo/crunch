#include "SysRendererDeferredLighting.h"
#include "Context.h"

SysRendererDeferredLighting * SysRendererDeferredLighting::sysRendererDeferredLighting = NULL;

SysRendererDeferredLighting & SysRendererDeferredLighting::get() {
	return *sysRendererDeferredLighting;
}

void SysRendererDeferredLighting::onCreate() {
	sysRendererDeferredLighting = this;

	// load all the programs
	struct Load {
		const char * name;
		ProgramResourceReference * program;
	};

	Load loadPrograms[] = {
		{ "sys___light_deferred_ambient", &ambientLightProgram },
		{ "sys___light_deferred_directional", &directionalLightProgram },
		{ "sys___light_deferred_point_sphere", &pointLightSphereProgram },
		{ "sys___light_deferred_point_sphere_shadow", &pointLightSphereShadowProgram },
		{ "sys___light_deferred_point_quad", &pointLightQuadProgram },
		{ "sys___light_deferred_point_quad_shadow", &pointLightQuadShadowProgram },
		{ "sys___light_deferred_point_geometry", &pointLightGeometryProgram }
	};

	for (size_t i = 0; i < arraySize( loadPrograms ); ++i)
		*loadPrograms[i].program = Context::get().resourceManager.acquire <ProgramResource>( loadPrograms[i].name, true );

	geoSphere.generate( GEOSPHERE_SUBDIVISIONS );
	const std::vector <Vector3f> & geoSphereVertices = geoSphere.getVertices();
	const std::vector <ushort> & geoSphereIndices = geoSphere.getIndices();

	cone.generate( CONE_SEGMENTS );
	const std::vector <Vector3f> & coneVertices = cone.getVertices();
	const std::vector <ushort> & coneIndices = cone.getIndices();

	std::vector <Vector3f> combinedVertices( geoSphereVertices.size() + coneVertices.size() );
	std::vector <ushort> combinedIndices( geoSphereIndices.size() + coneIndices.size() );

	geoSphereVertexOffset = 0;
	geoSphereIndexOffset = 0;
	std::copy( geoSphereVertices.begin(), geoSphereVertices.end(), combinedVertices.begin() + geoSphereVertexOffset );
	std::copy( geoSphereIndices.begin(), geoSphereIndices.end(), combinedIndices.begin() + geoSphereIndexOffset );

	coneVertexOffset = geoSphereVertexOffset + geoSphereVertices.size();
	coneIndexOffset = geoSphereIndexOffset + geoSphereIndices.size();
	std::copy( coneVertices.begin(), coneVertices.end(), combinedVertices.begin() + coneVertexOffset );
	std::copy( coneIndices.begin(), coneIndices.end(), combinedIndices.begin() + coneIndexOffset );

	// since the offsets are in bytes, multiply them by the type sizes
	geoSphereVertexOffset *= sizeof( combinedVertices[0] );
	geoSphereIndexOffset *= sizeof( combinedIndices[0] );
	coneVertexOffset *= sizeof( combinedVertices[0] );
	coneIndexOffset *= sizeof( combinedIndices[0] );

	// allocate the buffers
	GpuBuffer::BufferDescription vDesc;
	vDesc.type = GL_ARRAY_BUFFER;
	vDesc.size = combinedVertices.size() * sizeof( combinedVertices[0] );
	vDesc.data = &combinedVertices[0];
	vDesc.usage = GL_STATIC_DRAW;
	vertexBuffer = Context::get().gpuState.createBuffer( vDesc );

	GpuBuffer::BufferDescription iDesc;
	iDesc.type = GL_ELEMENT_ARRAY_BUFFER;
	iDesc.size = combinedIndices.size() * sizeof( combinedIndices[0] );
	iDesc.data = &combinedIndices[0];
	iDesc.usage = GL_STATIC_DRAW;
	indexBuffer = Context::get().gpuState.createBuffer( iDesc );

	// make these keys 1 before each actual pass to guarantee they occur very first
	beginFrameCall.bind( this, &SysRendererDeferredLighting::beginFrame );
	beginFrameKey = Renderer::makeKeyFirst( 0 );
	beginGBufferPassCall.bind( this, &SysRendererDeferredLighting::beginGBufferPass );
	beginGBufferPassKey = Renderer::makeKeyFirst( G_BUFFER_PASS-1 );
	beginLightingPassCall.bind( this, &SysRendererDeferredLighting::beginLightingPass );
	beginLightingPassKey = Renderer::makeKeyFirst( LIGHTING_PASS-1 );
	beginShadingPassCall.bind( this, &SysRendererDeferredLighting::beginShadingPass );
	beginShadingPassKey = Renderer::makeKeyFirst( SHADING_PASS-1 );
	beginTranslucentPassCall.bind( this, &SysRendererDeferredLighting::beginTranslucentPass );
	beginTranslucentPassKey = Renderer::makeKeyFirst( TRANSLUCENT_PASS-1 );
}

void SysRendererDeferredLighting::onDestroy() {
	sysRendererDeferredLighting = NULL;
}

void SysRendererDeferredLighting::setupFramebuffer( uint width, uint height ) {
	// for buffers like depth and normals, we use nearest because these should NOT be interpolated
	framebuffer = Context::get().gpuState.createFramebuffer();

	GpuTexture::Texture2DDescription tDesc;
	tDesc.wrapS = GL_CLAMP_TO_EDGE;
	tDesc.wrapT = GL_CLAMP_TO_EDGE;
	GpuTexture::Data2DDescription mip;
	tDesc.mipmapLevels = &mip;
	mip.format = GL_RGBA;
	mip.type = GL_UNSIGNED_BYTE;
	mip.width = width;
	mip.height = height;

	// create the new framebuffer textures
	GpuRenderbuffer::RenderbufferDescription rDesc;
	rDesc.width = width;
	rDesc.height = height;
	rDesc.internalFormat = GL_DEPTH24_STENCIL8;
	depthStencil = Context::get().gpuState.createRenderbuffer( rDesc );

	tDesc.imageDescription.minFilter = GL_NEAREST;
	tDesc.imageDescription.magFilter = GL_NEAREST;
	tDesc.imageDescription.internalFormat = GL_RGB10_A2;
	normalSpecBuffer = Context::get().gpuState.createTexture( tDesc );
	faceNormalBuffer = Context::get().gpuState.createTexture( tDesc );

	tDesc.imageDescription.internalFormat = GL_R32F;
	mip.type = GL_FLOAT;
	depthBuffer = Context::get().gpuState.createTexture( tDesc );

	tDesc.imageDescription.minFilter = GL_LINEAR;
	tDesc.imageDescription.magFilter = GL_LINEAR;
	tDesc.imageDescription.internalFormat = GL_RGBA16F;
	lightingBuffer = Context::get().gpuState.createTexture( tDesc );
	colorBuffer = Context::get().gpuState.createTexture( tDesc );
}

void SysRendererDeferredLighting::onDraw() {
	Context::get().renderer.submit( beginFrameCall, beginFrameKey );
	Context::get().renderer.submit( beginGBufferPassCall, beginGBufferPassKey );
	Context::get().renderer.submit( beginLightingPassCall, beginLightingPassKey );
	Context::get().renderer.submit( beginShadingPassCall, beginShadingPassKey );
	Context::get().renderer.submit( beginTranslucentPassCall, beginTranslucentPassKey );
}

void SysRendererDeferredLighting::beginFrame() {
	framebuffer->bind( GL_FRAMEBUFFER );
}

void SysRendererDeferredLighting::beginGBufferPass() {
	// set the viewport first for the whole set of passes
	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)normalSpecBuffer->getWidth(), (int)normalSpecBuffer->getHeight() ) );
	vpState.bind();

	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( true );
	rState.setCullMode( GL_BACK );
	rState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.setDepthWriteEnabled( true );
	dsState.bind();

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	// bind render targets - we write to the normal/specular power and depth buffers
	framebuffer->attach( GL_DEPTH_STENCIL_ATTACHMENT, getDepthStencilBuffer() );
	framebuffer->attach( GL_COLOR_ATTACHMENT0, getNormalSpecularBuffer() );
	framebuffer->attach( GL_COLOR_ATTACHMENT1, getFaceNormalBuffer() );
	framebuffer->attach( GL_COLOR_ATTACHMENT2, getDepthBuffer() );
	framebuffer->detatch( GL_COLOR_ATTACHMENT3 );
	framebuffer->bind( GL_FRAMEBUFFER );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	GLenum depthClear = GL_COLOR_ATTACHMENT2;
	dbState.setDrawBuffers( 1, &depthClear );
	dbState.bind();

	// clear depth buffer and depth renderbuffer
	GpuClearCall clearCall = Context::get().gpuState.createClearCall();
	clearCall.setClearColor( Vector4f( std::numeric_limits <float>::max(), 0.0f, 0.0f, 0.0f ) );
	clearCall.setClearDepth( 1.0f );
	clearCall.setClearBuffers( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	clearCall.clear();

	GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	dbState.setDrawBuffers( arraySize( buffers ), buffers );
	dbState.bind();
}

void SysRendererDeferredLighting::beginLightingPass() {
	// there is culling but the culling mode may differ between lights
	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( true );
	rState.bind();

	// there is depth testing but the depth func may differ between lights
	// don't write the lights' depths
	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthWriteEnabled( false );
	dsState.bind();

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_ADDITIVE );
	blendState.bind();

	// bind render targets - we write to the lighting buffer
	framebuffer->attach( GL_COLOR_ATTACHMENT0, getLightingBuffer() );
	framebuffer->detatch( GL_COLOR_ATTACHMENT1 );
	framebuffer->detatch( GL_COLOR_ATTACHMENT2 );
	framebuffer->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
	dbState.setDrawBuffers( arraySize( buffers ), buffers );
	dbState.bind();

	// bind normal/spec to texture 0, face normals to texture 1, and depth to texture 2, since lights will certainly use these
	getNormalSpecularBuffer()->bind( 0 );
	getFaceNormalBuffer()->bind( 1 );
	getDepthBuffer()->bind( 2 );
	getAmbientOcclusionBuffer()->bind( 3 );

	// lights are accumulated, so we need to clear
	GpuClearCall clearCall = Context::get().gpuState.createClearCall();
	clearCall.setClearBuffers( GL_COLOR_BUFFER_BIT );
	clearCall.clear();
}

void SysRendererDeferredLighting::beginShadingPass() {
	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( true );
	rState.setCullMode( GL_BACK );
	rState.bind();

	// geometry is being rendered so use the EQUAL depth function
	// again, no depth writes
	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.setDepthWriteEnabled( false );
	dsState.bind();

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	// bind render targets - we write to the color buffer
	framebuffer->attach( GL_COLOR_ATTACHMENT0, getColorBuffer() );
	framebuffer->detatch( GL_COLOR_ATTACHMENT1 );
	framebuffer->detatch( GL_COLOR_ATTACHMENT2 );
	framebuffer->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
	dbState.setDrawBuffers( arraySize( buffers ), buffers );
	dbState.bind();

	// bind normal/spec to texture 0, lighting buffer to texture 1
	// don't need depth since you already have that when re-rendering geometry
	getNormalSpecularBuffer()->bind( 0 );
	getLightingBuffer()->bind( 1 );
}

void SysRendererDeferredLighting::beginTranslucentPass() {
	// we'll default to culling backfaces, but usually we first cull front, then back for each object
	// this obviously has to be per-object though, so we can't set it globally
	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( true );
	rState.setCullMode( GL_BACK );
	rState.bind();

	// regular geometry rendering using LEQUAL
	// since these are translucent objects, we don't write depth
	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.setDepthWriteEnabled( false );
	dsState.bind();

	// we'll default to no blending, but generally objects will want to change this
	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	// bind render targets - again we just write to the color buffer
	framebuffer->attach( GL_COLOR_ATTACHMENT0, getColorBuffer() );
	framebuffer->detatch( GL_COLOR_ATTACHMENT1 );
	framebuffer->detatch( GL_COLOR_ATTACHMENT2 );
	framebuffer->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
	dbState.setDrawBuffers( arraySize( buffers ), buffers );
	dbState.bind();
}

const GeoSphere & SysRendererDeferredLighting::getGeoSphere() const {
	return geoSphere;
}

const Cone & SysRendererDeferredLighting::getCone() const {
	return cone;
}

GpuBufferReference SysRendererDeferredLighting::getLightVolumeVertexBuffer() {
	return vertexBuffer;
}

GpuBufferReference SysRendererDeferredLighting::getLightVolumeIndexBuffer() {
	return indexBuffer;
}

size_t SysRendererDeferredLighting::getGeoSphereVertexOffset() const {
	return geoSphereVertexOffset;
}

size_t SysRendererDeferredLighting::getGeoSphereIndexOffset() const {
	return geoSphereIndexOffset;
}

size_t SysRendererDeferredLighting::getConeVertexOffset() const {
	return coneVertexOffset;
}

size_t SysRendererDeferredLighting::getConeIndexOffset() const {
	return coneIndexOffset;
}

GpuRenderbufferReference SysRendererDeferredLighting::getDepthStencilBuffer() {
	return depthStencil;
}

GpuTextureReference SysRendererDeferredLighting::getNormalSpecularBuffer() {
	return normalSpecBuffer;
}

GpuTextureReference SysRendererDeferredLighting::getFaceNormalBuffer() {
	return faceNormalBuffer;
}

GpuTextureReference SysRendererDeferredLighting::getDepthBuffer() {
	return depthBuffer;
}

GpuTextureReference SysRendererDeferredLighting::getLightingBuffer() {
	return lightingBuffer;
}

GpuTextureReference SysRendererDeferredLighting::getAmbientOcclusionBuffer() {
	return colorBuffer;
}

GpuTextureReference SysRendererDeferredLighting::getColorBuffer() {
	return colorBuffer;
}

GpuTextureReference SysRendererDeferredLighting::getColorBuffer2() {
	return lightingBuffer;
}

void SysRendererDeferredLighting::swapColorBuffers() {
	std::swap( colorBuffer, lightingBuffer );
}