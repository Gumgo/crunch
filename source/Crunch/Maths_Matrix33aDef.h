#ifndef MATHS_MATRIX33ADEF_DEFINED
#define MATHS_MATRIX33ADEF_DEFINED

template <typename T> Matrix33a <T>::Matrix33a() {
	set( 1, 0, 0,
		 0, 1, 0 );
}

template <typename T> Matrix33a <T>::Matrix33a( T m00, T m01, T m02,
												T m10, T m11, T m12 ) {
	set( m00, m01, m02,
		 m10, m11, m12 );
}

template <typename T> Matrix33a <T>::Matrix33a( const Matrix33a <T> & m ) {
	set( m[0], m[1], m[2],
		 m[3], m[4], m[5] );
}

template <typename T> Matrix33a <T>::Matrix33a( const Matrix22 <T> & m ) {
	set( m[0], m[1], 0,
		 m[2], m[3], 0 );
}

template <typename T> Matrix33a <T>::~Matrix33a() {
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator+( const Matrix33a <T> & m ) const {
	return Matrix33a( d[0] + m[0], d[1] + m[1], d[2] + m[2],
					  d[3] + m[3], d[4] + m[4], d[5] + m[5] );
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator+() const {
	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator-( const Matrix33a <T> & m ) const {
	return Matrix33a( d[0] - m[0], d[1] - m[1], d[2] - m[2],
					  d[3] - m[3], d[4] - m[4], d[5] - m[5] );
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator-() const {
	return Matrix33a( -d[0], -d[1], -d[2],
					  -d[3], -d[4], -d[5] );
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator*( T r ) const {
	return Matrix33a( d[0]*r, d[1]*r, d[2]*r,
					  d[3]*r, d[4]*r, d[5]*r );
}

template <typename T> Matrix33a <T> operator*( T r, const Matrix33a <T> & m ) {
	return m*r;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator/( T r ) const {
	return Matrix33a( d[0]/r, d[1]/r, d[2]/r,
					  d[3]/r, d[4]/r, d[5]/r );
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator*( const Matrix33a <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[3];
	T m01 = d[0]*m[1] + d[1]*m[4];
	T m02 = d[0]*m[2] + d[1]*m[5] + d[2];

	T m10 = d[3]*m[0] + d[4]*m[3];
	T m11 = d[3]*m[1] + d[4]*m[4];
	T m12 = d[3]*m[2] + d[4]*m[5] + d[5];

	return Matrix33a <T>( m00, m01, m02,
						  m10, m11, m12 );
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator*( const Matrix22 <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[2];
	T m01 = d[0]*m[1] + d[1]*m[3];
	T m02 = d[2];

	T m10 = d[3]*m[0] + d[4]*m[2];
	T m11 = d[3]*m[1] + d[4]*m[3];
	T m12 = d[5];

	return Matrix33a <T>( m00, m01, m02,
						  m10, m11, m12 );
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator=( const Matrix33a <T> & m ) {
	set( m[0], m[1], m[2],
		 m[3], m[4], m[5] );
	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator=( const Matrix22 <T> & m ) {
	set( m[0], m[1], 0,
		 m[2], m[3], 0 );
	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator+=( const Matrix33a <T> & m ) {
	d[0] += m[0]; d[1] += m[1]; d[2] += m[2];
	d[3] += m[3]; d[4] += m[4]; d[5] += m[5];
	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator-=( const Matrix33a <T> & m ) {
	d[0] -= m[0]; d[1] -= m[1]; d[2] -= m[2];
	d[3] -= m[3]; d[4] -= m[4]; d[5] -= m[5];
	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator*=( T r ) {
	d[0] *= r; d[1] *= r; d[2] *= r;
	d[3] *= r; d[4] *= r; d[5] *= r;
	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator/=( T r ) {
	d[0] /= r; d[1] /= r; d[2] /= r;
	d[3] /= r; d[4] /= r; d[5] /= r;
	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator*=( const Matrix33a <T> & m ) {
	T m00 = d[0]*m[0] + d[1]*m[3];
	T m01 = d[0]*m[1] + d[1]*m[4];
	T m02 = d[0]*m[2] + d[1]*m[5] + d[2];

	T m10 = d[3]*m[0] + d[4]*m[3];
	T m11 = d[3]*m[1] + d[4]*m[4];
	T m12 = d[3]*m[2] + d[4]*m[5] + d[5];

	set( m00, m01, m02,
		 m10, m11, m12 );

	return *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::operator*=( const Matrix22 <T> & m ) {
	T m00 = d[0]*m[0] + d[1]*m[2];
	T m01 = d[0]*m[1] + d[1]*m[3];
	T m02 = d[2];

	T m10 = d[3]*m[0] + d[4]*m[2];
	T m11 = d[3]*m[1] + d[4]*m[3];
	T m12 = d[5];

	set( m00, m01, m02,
		 m10, m11, m12 );

	return *this;
}

template <typename T> bool Matrix33a <T>::operator==( const Matrix33a <T> & m ) const {
	return (d[0] == m[0] && d[1] == m[1] && d[2] == m[2] &&
			d[3] == m[3] && d[4] == m[4] && d[5] == m[5]);
}

template <typename T> bool Matrix33a <T>::operator!=( const Matrix33a <T> & m ) const {
	return (d[0] != m[0] || d[1] != m[1] || d[2] != m[2] ||
			d[3] != m[3] || d[4] != m[4] || d[5] != m[5]);
}

template <typename T> template <typename U> T & Matrix33a <T>::operator[]( U i ) {
	return d[i];
}

template <typename T> template <typename U> T Matrix33a <T>::operator[]( U i ) const {
	return d[i];
}

template <typename T> template <typename U> T & Matrix33a <T>::operator()( U row, U col ) {
	return d[row*3+col];
}

template <typename T> template <typename U> T Matrix33a <T>::operator()( U row, U col ) const {
	return d[row*3+col];
}

template <typename T> template <typename U> Matrix33a <T>::operator Matrix33a <U> () const {
	return Matrix33a <U>( (U)d[0], (U)d[1], (U)d[2],
						  (U)d[3], (U)d[4], (U)d[5] );
}

template <typename T> void Matrix33a <T>::set( T m00, T m01, T m02,
											   T m10, T m11, T m12 ) {
	d[0] = m00; d[1] = m01; d[2] = m02;
	d[3] = m10; d[4] = m11; d[5] = m12;
}

template <typename T> bool Matrix33a <T>::invert() {
	T det = determinant();
	if (det == 0) {
		set( 0, 0, 0, 0, 0, 0 );
		return false;
	}

	T invDet = 1/det;

	T m00 = invDet * d[4];
	T m01 = invDet * -d[1];
	T m02 = invDet * (d[5]*d[1] - d[4]*d[2]);
	T m10 = invDet * -d[3];
	T m11 = invDet * d[0];
	T m12 = invDet * (d[3]*d[2] - d[5]*d[0]);

	set( m00, m01, m02,
		 m10, m11, m12 );
	return true;
}

template <typename T> void Matrix33a <T>::translate( T x, T y ) {
	d[2] += x;
	d[5] += y;
}

template <typename T> void Matrix33a <T>::translate( const Vector2 <T> & t ) {
	translate( t.x, t.y );
}

template <typename T> void Matrix33a <T>::rotate( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	Matrix22 <T> rot( cosA, -sinA,
					  sinA, cosA );

	*this = rot * *this;
}

template <typename T> void Matrix33a <T>::scale( T x, T y ) {
	d[0] *= x;
	d[1] *= x;
	d[2] *= x;

	d[3] *= y;
	d[4] *= y;
	d[5] *= y;
}

template <typename T> void Matrix33a <T>::scale( const Vector2 <T> & s ) {
	scale( s.x, s.y );
}

template <typename T> void Matrix33a <T>::identity() {
	set( 1, 0, 0, 0, 1, 0 );
}

template <typename T> Matrix33a <T> Matrix33a <T>::inverse() const {
	T det = determinant();
	if (det == 0)
		return Matrix33a <T>( 0, 0, 0, 0, 0, 0 );

	T invDet = 1/det;

	T m00 = invDet * d[4];
	T m01 = invDet * -d[1];
	T m02 = invDet * (d[5]*d[1] - d[4]*d[2]);
	T m10 = invDet * -d[3];
	T m11 = invDet * d[0];
	T m12 = invDet * (d[3]*d[2] - d[5]*d[0]);

	return Matrix33a <T>( m00, m01, m02,
						  m10, m11, m12 );
}

template <typename T> Matrix33a <T> Matrix33a <T>::translated( T x, T y ) const {
	return Matrix33a <T>( d[0], d[1], d[2] + x,
						  d[3], d[4], d[5] + y );
}

template <typename T> Matrix33a <T> Matrix33a <T>::translated( const Vector2 <T> & t ) const {
	return translated( t.x, t.y );
}

template <typename T> Matrix33a <T> Matrix33a <T>::rotated( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	Matrix22 <T> rot( cosA, -sinA,
					  sinA, cosA );

	return rot * *this;
}

template <typename T> Matrix33a <T> Matrix33a <T>::scaled( T x, T y ) const {
	return Matrix33a <T>( d[0]*x, d[1]*x, d[2]*x,
						  d[3]*y, d[4]*y, d[5]*y );
}

template <typename T> Matrix33a <T> Matrix33a <T>::scaled( const Vector2 <T> & s ) const {
	return scaled( s.x, s.y );
}

template <typename T> T Matrix33a <T>::determinant() const {
	return d[0]*d[4] - d[1]*d[3];
}

template <typename T> T Matrix33a <T>::trace() const {
	return d[0] + d[4] + 1;
}

template <typename T> const T * Matrix33a <T>::getArray() const {
	return d;
}

template <typename T> Vector2 <T> Matrix33a <T>::transformPoint( const Vector2 <T> & p ) const {
	return Vector2 <T>( d[0]*p.x + d[1]*p.y + d[2],
						d[3]*p.x + d[4]*p.y + d[5] );
}

template <typename T> Vector2 <T> Matrix33a <T>::transformVector( const Vector2 <T> & v ) const {
	return Vector2 <T>( d[0]*v.x + d[1]*v.y,
						d[3]*v.x + d[4]*v.y );
}

template <typename T> Ray2 <T> Matrix33a <T>::transformRay( const Ray2 <T> & r ) const {
	return Ray2 <T>(
		transformPoint( ray.origin ),
		transformVector( ray.direction ) );
}

template <typename T> Matrix22 <T> Matrix33a <T>::getNormalTransformation() const {
	Matrix22 <T> mat22( d[0], d[1],
						d[3], d[4] );
	return mat22.inverse().transposed();
}

#endif