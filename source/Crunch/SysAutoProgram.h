#ifndef SYSAUTOPROGRAM_DEFINED
#define SYSAUTOPROGRAM_DEFINED

#include "Common.h"
#include "GpuProgram.h"
#include "GpuBuffer.h"
#include "MeshResource.h"
#include <vector>

// automatically binds program parameters
class SysAutoProgram : private boost::noncopyable {
public:
	SysAutoProgram();
	~SysAutoProgram();

	void setProgram( const GpuProgramReference & prog );
	void setAttributes( MeshResource & mesh ); // sets attributes from the mesh

	// sets an attribute to be bound using the data specified and the given buffer
	// the buffer can be an empty reference, but it must be eventually set before use
	size_t setAttribute( const std::string & name, const GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer,
		uint stride, const void * pointer, size_t arrayOffset = 0, size_t arrayCount = 1 );

	// sets a uniform to be bound from the given location
	size_t setUniform( const std::string & name, GLenum type, const void * value, size_t arrayOffset = 0, size_t arrayCount = 1 );

	// alters an already bound attribute to be bound using the data specified and the given buffer
	// the buffer can be an empty reference, but it must be eventually set before use
	bool alterAttribute( size_t index, const GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer,
		uint stride, const void * pointer, size_t arrayOffset = 0, size_t arrayCount = 1 );

	// alters only the buffer of an already bound attribute
	// the buffer can be an empty reference, but it must be eventually set before use
	bool alterAttributeBuffer( size_t index, const GpuBufferReference & buffer );

	// alters an already bound uniform
	bool alterUniform( size_t index, GLenum type, const void * value, size_t arrayOffset = 0, size_t arrayCount = 1 );

	void clear(); // clears attribs and uniforms

	void bind(); // binds program

private:
	GpuProgramReference program;

	struct Attribute {
		size_t index;
		GpuBufferReference buffer;
		uint size;
		GLenum type;
		bool normalized;
		bool integer;
		uint stride;
		const void * pointer;
		size_t arrayOffset;
		size_t arrayCount;
	};

	struct Uniform {
		size_t index;
		GLenum type;
		const void * value;
		size_t arrayOffset;
		size_t arrayCount;
	};

	std::vector <Attribute> attributes;
	std::vector <Uniform> uniforms;
};

#endif