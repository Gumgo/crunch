#if 0

#include "NetworkMessageLayer.h"
#include "Context.h"
#include "Log.h"

NetworkMessageLayer::MessageBackup::MessageBackup()
	: dataBlocks( DEFAULT_DATA_BLOCK_COUNT ) {
}

size_t NetworkMessageLayer::MessageBackup::add( const byte * data, size_t dataSize ) {
	size_t bytesWritten = 0;
	// always reserve at least 1
	size_t firstBlockId = dataBlocks.add( DataBlock() );
	DataBlock * currentBlock = &dataBlocks.get( firstBlockId );

	while (true) {
		// compute the amount to write
		size_t writeAmount = std::min( dataSize - bytesWritten, DataBlock::DATA_BLOCK_SIZE );
		// write the size and the data
		currentBlock->dataSize = writeAmount;
		memcpy( currentBlock->data, data + bytesWritten, writeAmount );
		bytesWritten += writeAmount;
		if (bytesWritten >= dataSize) {
			currentBlock->nextBlockIndex = std::numeric_limits <size_t>::max();
			break;
		}

		// more to write - grab another block
		size_t nextBlockId = dataBlocks.add( DataBlock() );
		currentBlock->nextBlockIndex = nextBlockId;
		currentBlock = &dataBlocks.get( nextBlockId );
	}

	return firstBlockId;
}

void NetworkMessageLayer::MessageBackup::remove( size_t backupId ) {
	assert( dataBlocks.contains( backupId ) );

	size_t next = backupId;
	while (next != std::numeric_limits <size_t>::max()) {
		size_t prev = next;
		next = dataBlocks.get( next ).nextBlockIndex;
		dataBlocks.remove( prev );
	}
}

bool NetworkMessageLayer::MessageBackup::get( size_t backupId, const byte *& data, size_t & dataSize, size_t & next ) const {
	assert( dataBlocks.contains( backupId ) );

	const DataBlock & block = dataBlocks.get( backupId );
	data = block.data;
	dataSize = block.dataSize;
	if (block.nextBlockIndex != std::numeric_limits <size_t>::max()) {
		next = block.nextBlockIndex;
		return true;
	} else
		return false;
}

size_t NetworkMessageLayer::MessageBackup::size( size_t backupId ) const {
	assert( dataBlocks.contains( backupId ) );

	size_t total = 0;
	size_t next = backupId;
	while (next != std::numeric_limits <size_t>::max()) {
		total += dataBlocks.get( next ).dataSize;
		next = dataBlocks.get( next ).nextBlockIndex;
	}

	return total;
}

NetworkMessageLayer::NetworkMessageLayer()
	: connectionsMap( 20 ) {
	initialized = false;
	dataLayer.setOnReceiveData( boost::function <void ( size_t, const byte*, size_t )>(
		boost::bind( &NetworkMessageLayer::receiveMessage, this, _1, _2, _3 ) ) );

	guaranteedProcessingBuffer.reserve( GUARANTEED_PROCESSING_BUFFER_SIZE );
}

NetworkMessageLayer::~NetworkMessageLayer() {
	term();
}

bool NetworkMessageLayer::init( ushort port ) {
	if (initialized) {
		Log::warning() << "Network message layer already initialized";
		return false;
	}

	if (!dataLayer.init( port ))
		return false;

	initialized = true;
	Log::info() << "Network message layer initialized";
	return true;

}

void NetworkMessageLayer::term() {
	if (!initialized)
		return;

	dataLayer.term();
	Log::info() << "Network message layer terminated";
	initialized = false;
}

void NetworkMessageLayer::acceptNewConnection( size_t dataLayerId ) {
	Connection c;
	c.dataLayerId = dataLayerId;
	c.ping = Connection::DEFAULT_PING;
	c.nextGuaranteedSendId = 0;
	c.nextGuaranteedReceiveId = 0;
	c.nextPingId = 0;
	c.lastMessageReceivedTime = 0;

	size_t newConnectionId = connections.add( c );
	connectionsMap.put( dataLayerId, newConnectionId );
	Log::info() << "Message layer connection " << newConnectionId << " created";

	queuePing( newConnectionId );

	if (onAcceptNewConnection)
		onAcceptNewConnection( newConnectionId );
}

void NetworkMessageLayer::enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn ) {
	dataLayer.enableAcceptNewConnections( boost::function <void ( size_t )>(
		boost::bind( &NetworkMessageLayer::acceptNewConnection, this, _1 ) ) );
	onAcceptNewConnection = onAcceptNewConnectionFn;
}

void NetworkMessageLayer::disableAcceptNewConnections() {
	dataLayer.disableAcceptNewConnections();
}

void NetworkMessageLayer::setOnConnectionTimeout( const boost::function <void ( size_t )> & onConnectionTimeoutFn ) {
	onConnectionTimeout = onConnectionTimeoutFn;
}

bool NetworkMessageLayer::createConnection( const std::string & address, ushort port, size_t & newConnectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to create message layer connection: network message layer not initialized";
		return false;
	}

	size_t dataLayerId;
	if (!dataLayer.createConnection( address, port, dataLayerId ))
		return false;

	Connection c;
	c.dataLayerId = dataLayerId;
	c.ping = Connection::DEFAULT_PING;
	c.nextGuaranteedSendId = 0;
	c.nextGuaranteedReceiveId = 0;
	c.nextPingId = 0;
	c.lastMessageReceivedTime = Context::get().timer.getSystemTimeMillis();

	newConnectionId = connections.add( c );
	connectionsMap.put( dataLayerId, newConnectionId );
	Log::info() << "Message layer connection " << newConnectionId << " created";

	queuePing( newConnectionId );

	return true;
}

void NetworkMessageLayer::destroyConnection( size_t connectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to destroy connection: network message layer not initialized";
		return;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to destroy a nonexistent message layer connection";
		return;
	}

	Connection & c = connections.get( connectionId );
	dataLayer.destroyConnection( c.dataLayerId );

	for (size_t i = 0; i < c.pendingAcks.size(); ++i)
		messageBackup.remove( c.pendingAcks[i].messageBackupId );
	for (size_t i = 0; i < c.pendingGuaranteed.size(); ++i)
		messageBackup.remove( c.pendingGuaranteed[i].messageBackupId );

	for (size_t i = 0; i < connectionsWithPendingAcks.size(); ++i) {
		if (connectionsWithPendingAcks[i] == connectionId) {
			connectionsWithPendingAcks[i] = connectionsWithPendingAcks.back();
			connectionsWithPendingAcks.pop_back();
			break;
		}
	}

	connectionsMap.remove( c.dataLayerId );
	connections.remove( connectionId );
	Log::info() << "Message layer connection " << connectionId << " destroyed";
}

void NetworkMessageLayer::setOnReceiveMessage( const boost::function <void ( size_t, const byte*, size_t )> & onReceiveMessageFn ) {
	onReceiveMessage = onReceiveMessageFn;
}

void NetworkMessageLayer::receiveMessage( size_t dataLayerId, const byte * data, size_t dataSize ) {
	// get the connection
	size_t connectionId = connectionsMap.get( dataLayerId );
	Connection & c = connections.get( connectionId );

	Timer::TimerValue time = Context::get().timer.getSystemTimeMillis();
	c.lastMessageReceivedTime = time;

	// read the flags
	if (dataSize < 1) {
		Log::warning() << "Invalid message received";
		return;
	}
	byte flags = data[0];

	if (flags == FLAG_ACK) {
		// if ACK, read the guaranteed message ID
		if (dataSize < 3) {
			Log::warning() << "Invalid ACK message received";
			return;
		}

		ushort guaranteedId = *((ushort*)&data[1]);
		guaranteedId = ntohs( guaranteedId );

		// remove the pending ACK
		for (size_t i = 0; i < c.pendingAcks.size(); ++i) {
			if (c.pendingAcks[i].guaranteedId == guaranteedId) {
				c.pendingAcks[i] = c.pendingAcks.back();
				c.pendingAcks.pop_back();

				// if there are no more acks pending, remove from pending list
				if (c.pendingAcks.empty()) {
					for (size_t i = 0; i < connectionsWithPendingAcks.size(); ++i) {
						if (connectionsWithPendingAcks[i] == connectionId) {
							connectionsWithPendingAcks[i] = connectionsWithPendingAcks.back();
							connectionsWithPendingAcks.pop_back();
							break;
						}
					}
				}

				break;
			}
		}
		// if it wasn't found, that's perfectly normal (and common)
	} else if (flags == FLAG_GUARANTEED) {
		// it's a guaranteed message - we need to ensure ordering and send an ACK
		// read the guaranteed message ID first
		if (dataSize < 3) {
			Log::warning() << "Invalid guaranteed message received";
			return;
		}

		ushort guaranteedIdNw = *((ushort*)&data[1]);
		ushort guaranteedId = ntohs( guaranteedIdNw );

		// first send an ACK to let the connection know we got it
		dataLayer.startQueueData( dataLayerId );
		byte flags = FLAG_ACK;
		dataLayer.queueData( dataLayerId, &flags, sizeof( flags ) );
		dataLayer.queueData( dataLayerId, (byte*)&guaranteedIdNw, sizeof( guaranteedIdNw ) );

		if (guaranteedId == c.nextGuaranteedReceiveId) {
			// good news - we can immediately process this message
			if (onReceiveMessage)
				// data+3 and dataSize-3 because we don't want them reading the flag bits and ID
				onReceiveMessage( connectionId, data + 3, dataSize - 3 );
			++c.nextGuaranteedReceiveId;

			// now keep trying to process pending messages
			// make sure the received message didn't close the connection
			while (connections.contains( connectionId )) {
				bool processed = false;
				for (size_t i = 0; i < c.pendingGuaranteed.size(); ++i) {
					// if the next one is pending, process it immediately
					if (c.pendingGuaranteed[i].guaranteedId == c.nextGuaranteedReceiveId) {

						const byte * data;
						size_t dataSize;
						size_t next;
						if (!messageBackup.get( c.pendingGuaranteed[i].messageBackupId, data, dataSize, next )) {
							// if there's only one block, good - just process it in place
							if (onReceiveMessage)
								onReceiveMessage( connectionId, data, dataSize );
						} else {
							// otherwise, we have to rebuild up the message
							// reserve space for the message
							// make it at least 1 byte so that [0] is valid
							size_t messageSize = messageBackup.size( c.pendingGuaranteed[i].messageBackupId );
							guaranteedProcessingBuffer.resize( std::max( (size_t)1, messageSize ) );
							size_t bytesRead = 0;
							// go through each data block and copy it to the buffer
							do {
								if (dataSize > 0)
									memcpy( &guaranteedProcessingBuffer[bytesRead], data, dataSize );
								bytesRead += dataSize;
							} while (messageBackup.get( next, data, dataSize, next ));

							// process the message
							if (onReceiveMessage)
								onReceiveMessage( connectionId, &guaranteedProcessingBuffer[0], messageSize );
						}

						// remove the backed up message
						messageBackup.remove( c.pendingGuaranteed[i].messageBackupId );
						// increment the next one to process
						++c.nextGuaranteedReceiveId;
						// remove this from the pending list
						c.pendingGuaranteed[i] = c.pendingGuaranteed.back();
						c.pendingGuaranteed.pop_back();
						// loop again
						processed = true;
						break;
					}
				}
				if (!processed)
					break;
			}
		} else {
			// now we want to check if the received message ID is greater than the next one to process
			// however, since we're using USHORTs, we need to take wrapping into account
			// if the value is within 32767 higher with wrapping included, it's "higher"
			ushort lowerBound = guaranteedId;
			ushort upperBound = guaranteedId + (std::numeric_limits <ushort>::max() >> 1);
			// lowerBound will be the ID
			// if upperBound > lowerBound, no wrapping occurred so we test > lower AND < upper
			// however, if upperBound < lowerBound, we wrapped around, so we test > lower OR < upper
			bool greater = (upperBound > lowerBound) ?
				(guaranteedId > lowerBound && guaranteedId < upperBound) :
				(guaranteedId > lowerBound || guaranteedId < upperBound);
			if (greater) {
				// make sure this isn't a duplicate first
				bool found = false;
				for (size_t i = 0; i < c.pendingGuaranteed.size(); ++i) {
					if (c.pendingGuaranteed[i].guaranteedId == guaranteedId) {
						found = true;
						break;
					}
				}

				if (!found) {
					// if after the next one we need to process, we have to delay processing it
					// back the message up
					Connection::PendingGuaranteed msg;
					msg.guaranteedId	= guaranteedId;
					msg.messageBackupId	= messageBackup.add( data + 3, dataSize - 3 );
					// add the backed up message to the pending list
					c.pendingGuaranteed.push_back( msg );
				}
			}
			// otherwise, if <, it's already been processed, so it's a duplicate
		}
	} else if (flags == FLAG_NORMAL) {
		// just a regular old non guaranteed message - process and discard it
		if (onReceiveMessage)
			// data+1 and dataSize-1 because we don't want them reading the flag bits
			onReceiveMessage( connectionId, data + 1, dataSize - 1 );
	} else if (flags == FLAG_PING) {
		// it's a ping message
		// read the ping ID first
		if (dataSize < 3) {
			Log::warning() << "Invalid ping message received";
			return;
		}

		ushort pingIdNw = *((ushort*)&data[1]);
		ushort pingId = ntohs( pingIdNw );

		for (size_t i = 0; i < c.pendingPings.size(); ++i) {
			if (c.pendingPings[i].pingId == pingId) {
				// update the ping time
				c.ping = (uint)(time - c.pendingPings[i].timeSent);
				c.pendingPings[i] = c.pendingPings.back();
				c.pendingPings.pop_back();
				break;
			}
		}
	} else {
		Log::warning() << "Message with invalid type received";
	}
}

void NetworkMessageLayer::receiveAndProcess() {
	if (!initialized) {
		Log::warning() << "Failed to receive and process messages: network message layer not initialized";
		return;
	}

	dataLayer.receiveAndProcess();

	Timer::TimerValue time = Context::get().timer.getSystemTimeMillis();

	size_t index = 0;
	size_t count = connections.size();
	while (count > 0) {
		if (connections.contains( index )) {
			--count;
			Connection & c = connections.get( index );
			if (c.lastMessageReceivedTime + Connection::TIMEOUT_TIME < time) {
				Log::info() << "Message layer connection " << index << " timed out";
				if (onConnectionTimeout)
					onConnectionTimeout( index );
			}
		}
		++index;
	}
}

bool NetworkMessageLayer::queueMessage( size_t connectionId, const byte * data, size_t dataSize ) {
	if (!initialized) {
		Log::warning() << "Failed to queue message: network message layer not initialized";
		return false;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to queue a message on a nonexistent message layer connection";
		return false;
	}

	size_t dataLayerId = connections.get( connectionId ).dataLayerId;

	dataLayer.startQueueData( dataLayerId );
	byte flags = FLAG_NORMAL;
	dataLayer.queueData( dataLayerId, &flags, sizeof( flags ) );
	dataLayer.queueData( dataLayerId, data, dataSize );

	return true;
}

bool NetworkMessageLayer::queueGuaranteedMessage( size_t connectionId, const byte * data, size_t dataSize ) {
	if (!initialized) {
		Log::warning() << "Failed to queue message: network message layer not initialized";
		return false;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to queue a message on a nonexistent message layer connection";
		return false;
	}

	Connection & c = connections.get( connectionId );

	// back up the message
	size_t backupId = messageBackup.add( data, dataSize );
	ushort guaranteedId = c.nextGuaranteedSendId;
	++c.nextGuaranteedSendId;

	// set up to wait for an ACK
	if (c.pendingAcks.empty())
		// if we weren't already pending, add to pending list
		connectionsWithPendingAcks.push_back( connectionId );
	Connection::PendingAck pendingAck;
	pendingAck.guaranteedId		= guaranteedId;
	pendingAck.messageBackupId	= backupId;
	pendingAck.lastSentTime		= Context::get().timer.getSystemTimeMillis();
	c.pendingAcks.push_back( pendingAck );

	dataLayer.startQueueData( c.dataLayerId );
	byte flags = FLAG_GUARANTEED;
	dataLayer.queueData( c.dataLayerId, &flags, sizeof( flags ) );
	ushort guaranteedIdNw = htons( guaranteedId );
	dataLayer.queueData( c.dataLayerId, (byte*)&guaranteedIdNw, sizeof( guaranteedIdNw ) );
	dataLayer.queueData( c.dataLayerId, data, dataSize );

	return true;
}

bool NetworkMessageLayer::queuePing( size_t connectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to queue ping: network message layer not initialized";
		return false;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to queue a ping on a nonexistent message layer connection";
		return false;
	}

	Connection & c = connections.get( connectionId );
	size_t dataLayerId = c.dataLayerId;

	dataLayer.startQueueData( dataLayerId );
	byte flags = FLAG_PING;
	dataLayer.queueData( dataLayerId, &flags, sizeof( flags ) );
	ushort pingId = htons( c.nextPingId );
	dataLayer.queueData( dataLayerId, (byte*)&pingId, sizeof( pingId ) );

	++c.nextPingId;
	c.lastSentPingTime = Context::get().timer.getSystemTimeMillis();
	return true;
}

void NetworkMessageLayer::sendQueued() {
	if (!initialized) {
		Log::warning() << "Failed to send queued messages: network message layer not initialized";
		return;
	}

	Timer::TimerValue time = Context::get().timer.getSystemTimeMillis();
	for (size_t i = 0; i < connectionsWithPendingAcks.size(); ++i) {
		Connection & c = connections.get( connectionsWithPendingAcks[i] );
		for (size_t a = 0; a < c.pendingAcks.size(); ++a) {
			uint diff = (uint)(time - c.pendingAcks[a].lastSentTime);
			// if more than twice the ping but no response, resend
			if (diff > c.ping * 2) {

				dataLayer.startQueueData( c.dataLayerId );
				byte flags = FLAG_GUARANTEED;
				dataLayer.queueData( c.dataLayerId, &flags, sizeof( flags ) );
				ushort guaranteedIdNw = htons( c.pendingAcks[a].guaranteedId );
				dataLayer.queueData( c.dataLayerId, (byte*)&guaranteedIdNw, sizeof( guaranteedIdNw ) );

				// read the backed up message and send it
				const byte * data;
				size_t dataSize;
				size_t blockId = messageBackup.get( c.pendingAcks[a].messageBackupId, data, dataSize, blockId );
				do
					dataLayer.queueData( c.dataLayerId, data, dataSize );
				while (messageBackup.get( blockId, data, dataSize, blockId ));
				dataLayer.queueData( c.dataLayerId, data, dataSize );

				c.pendingAcks[a].lastSentTime = time;
			}
		}
	}

	size_t index = 0;
	size_t count = connections.size();
	while (count > 0) {
		if (connections.contains( index )) {
			--count;
			Connection & c = connections.get( index );
			// get rid of old pings
			for (size_t i = 0; i < c.pendingPings.size(); ++i) {
				uint diff = (uint)(time - c.pendingPings[i].timeSent);
				if (diff > 30 * 1000) {
					c.pendingPings[i] = c.pendingPings.back();
					c.pendingPings.pop_back();
					--i;
				}
			}
			// send a ping if haven't in a while
			uint lastDiff = (uint)(time - c.lastSentPingTime);
			if (lastDiff > Connection::PING_SEND_FREQUENCY)
				queuePing( index );
		}
		++index;
	}

	dataLayer.sendQueued();
}

#endif