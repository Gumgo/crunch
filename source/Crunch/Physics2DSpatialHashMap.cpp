#include "Physics2DSpatialHashMap.h"

namespace Physics2D {

const float SpatialHashMap::MIN_LOAD_FACTOR = 0.25f;
const float SpatialHashMap::MAX_LOAD_FACTOR = 0.75f;

SpatialHashMap::SpatialHashMap( Real binDim ) {
	binDimensions = binDim;
	invBinDimensions = (Real)1 / binDim;

	table.resize( MIN_TABLE_SIZE );
	invTableSize = 1.0f / (float)table.size();
	for (size_t i = 0; i < table.size(); ++i)
		table[i] = NO_INDEX;
}

size_t SpatialHashMap::hash( const Vector2i & spatialIndex ) const {
	return Hash <Vector2i>()( spatialIndex );
}

void SpatialHashMap::resizeIfNecessary() {
	float loadFactor = (float)bins.size() * invTableSize;

	size_t newSize = table.size();
	if (loadFactor < MIN_LOAD_FACTOR)
		newSize = std::max( table.size() / 2, MIN_TABLE_SIZE );
	else
		newSize = table.size() * 2;

	if (newSize != table.size()) {
		// rehash everything
		invTableSize = 1.0f / (float)newSize;

		std::vector <size_t> newTable;
		newTable.resize( newSize );
		for (size_t i = 0; i < newSize; ++i)
			newTable[i] = NO_INDEX;

		size_t rehashCount = 0;
		size_t nextBinIndex = 0;
		while (rehashCount < bins.size()) {
			while (!bins.contains( nextBinIndex ))
				++nextBinIndex;

			// get the next bin and clear its old link
			Bin & b = bins.get( nextBinIndex );
			b.next = NO_INDEX;

			// find its new index in the table
			size_t newTableIndex = hash( b.spatialIndex ) % newSize;
			if (newTable[newTableIndex] == NO_INDEX)
				// if not already a bin there, simply add it
				newTable[newTableIndex] = nextBinIndex;
			else {
				// otherwise, traverse the chain of bins and add to the end
				Bin * nextBin = &bins.get( newTable[newTableIndex] );
				while (nextBin->next != NO_INDEX)
					nextBin = &bins.get( nextBin->next );
				nextBin->next = nextBinIndex;
			}

			++nextBinIndex;
			++rehashCount;
		}

		table.swap( newTable );
	}
}

std::pair <Vector2r, Vector2r> SpatialHashMap::getBins( const Aabb & aabb ) const {
	Vector2r minBnd = aabb.minBound * invBinDimensions;
	Vector2r maxBnd = aabb.maxBound * invBinDimensions;
	return std::make_pair(
		Vector2i( (int)floor( minBnd.x ), (int)floor( minBnd.y ) ),
		Vector2i( (int)ceil( maxBnd.x ), (int)ceil( maxBnd.y ) ) );
}

void SpatialHashMap::addToBin( size_t shapeIndex, const Vector2i & spatialIndex ) {
	size_t tableIndex = hash( spatialIndex ) % table.size();

	// the bin we will add to
	Bin * binToAddTo = NULL;

	// go through the chain of bins
	// loop until we encounter NO_INDEX
	size_t binIndex = table[tableIndex];
	while (binIndex != NO_INDEX) {
		Bin & b = bins.get( binIndex );
		if (spatialIndex == b.spatialIndex) {
			// we found the right bin
			binToAddTo = &b;
			break;
		}
		binIndex = b.next;
	}

	if (binToAddTo == NULL) {
		// we need to make a new bin
		size_t newBinIndex = bins.add( Bin() );
		binToAddTo = &bins.get( newBinIndex );
		binToAddTo->spatialIndex = spatialIndex;
		// add the new bin to the head of the list
		binToAddTo->next = table[tableIndex];
		binToAddTo->shapeNodes = NO_INDEX;
		table[tableIndex] = newBinIndex;
	}

	// make a new shape node to add to the bin
	size_t newNodeIndex = shapeNodes.add( ShapeNode() );
	ShapeNode & newNode = shapeNodes.get( newNodeIndex );
	newNode.shapeIndex = shapeIndex;
	// put the new shape node at the head of the list
	newNode.next = binToAddTo->shapeNodes;
	binToAddTo->shapeNodes = newNodeIndex;
}

void SpatialHashMap::removeFromBin( size_t shapeIndex, const Vector2i & spatialIndex ) {
	size_t tableIndex = hash( spatialIndex ) % table.size();

	// go through the chain of bins
	// we use a pointer to the bin index variable because we
	// will need to modify if if we end up removing the bin
	size_t * binIndex = &table[tableIndex];
	while (*binIndex != NO_INDEX) {
		Bin & b = bins.get( *binIndex );
		if (spatialIndex == b.spatialIndex)
			// we found the right bin
			break;
		binIndex = &b.next;
	}

	if (*binIndex != NO_INDEX) {
		// remove the correct node from the bin
		// do a similar pointer trick
		size_t * nodeIndex = &bins.get( *binIndex ).shapeNodes;
		while (*nodeIndex != NO_INDEX) {
			ShapeNode & n = shapeNodes.get( *nodeIndex );
			if (shapeIndex == n.shapeIndex)
				// we found the right shape
				break;
			nodeIndex = &n.next;
		}

		if (*nodeIndex != NO_INDEX) {
			// remove the shape node
			size_t tempNodeIndex = *nodeIndex;
			// the pointer to this node should now point to the next node (which might be NO_INDEX)
			*nodeIndex = shapeNodes.get( *nodeIndex ).next;
			// finally, remove the shape
			shapeNodes.remove( tempNodeIndex );

			// if we removed a shape, the bin might be empty now, so we check and remove it if so
			if (bins.get( *binIndex ).shapeNodes == NO_INDEX) {
				// the bin has no nodes, so remove it
				size_t tempBinIndex = *binIndex;
				// point the pointer to this node to its next node
				*binIndex = bins.get( *binIndex ).next;
				// remove the bin
				bins.remove( tempBinIndex );
			}
		}
	}
}

size_t SpatialHashMap::addShape( void * shape, const Aabb & aabb ) {
	size_t shapeIndex = shapeInfo.add( ShapeInfo() );
	ShapeInfo & s = shapeInfo.get( shapeIndex );
	s.shape = shape;
	s.aabb = aabb;

	Vector2i idx;
	std::pair <Vector2i, Vector2i> overlap = getBins( aabb );
	for (idx.y = overlap.first.y; idx.y < overlap.second.y; ++idx.y) {
		for (idx.x = overlap.first.x; idx.x < overlap.second.x; ++idx.x)
			addToBin( shapeIndex, idx );
	}

	resizeIfNecessary();

	return shapeIndex;
}

void SpatialHashMap::removeShape( size_t index ) {
	if (!shapeInfo.contains( index ))
		return;

	ShapeInfo & s = shapeInfo.get( index );

	Vector2i idx;
	std::pair <Vector2i, Vector2i> overlap = getBins( s.aabb );
	for (idx.y = overlap.first.y; idx.y < overlap.second.y; ++idx.y) {
		for (idx.x = overlap.first.x; idx.x < overlap.second.x; ++idx.x)
			removeFromBin( index, idx );
	}

	shapeInfo.remove( index );

	resizeIfNecessary();
}

void SpatialHashMap::moveShape( size_t index, const Aabb & aabb ) {
	if (!shapeInfo.contains( index ))
		return;

	ShapeInfo & s = shapeInfo.get( index );

	// find the old bin overlap and new bin overlap
	std::pair <Vector2i, Vector2i> oldOverlap = getBins( s.aabb );
	std::pair <Vector2i, Vector2i> newOverlap = getBins( aabb );

	// update the AABB
	s.aabb = aabb;

	if (oldOverlap == newOverlap)
		// didn't move enough - don't do anything
		return;

	// the intersection of these regions is not altered
	// it is the maximum of the minimum bounds and the minimum of the maximum bounds
	std::pair <Vector2i, Vector2i> isect = std::make_pair(
		vecMax( oldOverlap.first, newOverlap.first ),
		vecMin( oldOverlap.second, newOverlap.second ) );

	// if there is no overlap, isect will naturally be invalid and will
	// always fail point-in-region tests, since the max bound will be less than
	// the min bound

	Vector2i idx;

	// remove from old (do this first so bins can potentially be reused)
	for (idx.y = oldOverlap.first.y; idx.y < oldOverlap.second.y; ++idx.y) {
		for (idx.x = oldOverlap.first.x; idx.x < oldOverlap.second.x; ++idx.x) {
			if (idx.x < isect.first.x || idx.x >= isect.second.x ||
				idx.y < isect.first.y || idx.y >= isect.second.y)
				removeFromBin( index, idx );
		}
	}

	// add to new
	for (idx.y = newOverlap.first.y; idx.y < newOverlap.second.y; ++idx.y) {
		for (idx.x = newOverlap.first.x; idx.x < newOverlap.second.x; ++idx.x) {
			if (idx.x < isect.first.x || idx.x >= isect.second.x ||
				idx.y < isect.first.y || idx.y >= isect.second.y)
				addToBin( index, idx );
		}
	}
}

void * SpatialHashMap::getShape( size_t index ) const {
	if (shapeInfo.contains( index ))
		return shapeInfo.get( index ).shape;
	else
		return NULL;
}

Aabb SpatialHashMap::getAabb( size_t index ) const {
	if (shapeInfo.contains( index ))
		return shapeInfo.get( index ).aabb;
	else
		return Aabb();
}

bool SpatialHashMap::queryOverlap( size_t index0, size_t index1 ) const {
	ShapeInfo si0, si1;
	if (!shapeInfo.get( index0, si0 ) || !shapeInfo.get( index1, si1 ))
		return false;

	return Aabb::overlap( si0.aabb, si1.aabb );
}

void SpatialHashMap::queryOverlaps( std::vector <std::pair <size_t, size_t>> & overlaps ) const {
	overlaps.clear();

	// examine each active bin
	size_t binsLeft = bins.size();
	size_t binIndex = 0;
	while (binsLeft > 0) {
		if (!bins.contains( binIndex )) {
			++binIndex;
			continue;
		} else
			--binsLeft;

		// look at the bin
		const Bin & bin = bins.get( binIndex );

		// overlap all pairs of shapes in the bin
		// outer loop: start at the first shape node and get the next one until NO_INDEX reached
		for (size_t sn0 = bin.shapeNodes; sn0 != NO_INDEX; sn0 = shapeNodes.get( sn0 ).next) {
			// inner loop: start at the shape node after the one in the outer loop and get the next one until NO_INDEX reached
			for (size_t sn1 = shapeNodes.get( sn0 ).next; sn1 != NO_INDEX; sn1 = shapeNodes.get( sn1 ).next) {
				size_t idx0 = shapeNodes.get( sn0 ).shapeIndex;
				size_t idx1 = shapeNodes.get( sn1 ).shapeIndex;
				assert( idx0 != idx1 ); // should never be comparing same shape!
				const ShapeInfo & si0 = shapeInfo.get( idx0 );
				const ShapeInfo & si1 = shapeInfo.get( idx1 );

				// try overlapping the shape AABBs
				bool overlap = Aabb::overlap( si0.aabb, si1.aabb );
				// if they overlap, add to the list
				if (overlap) {
					// pair convention: (low, high), just so it's easier to avoid duplicates
					std::pair <size_t, size_t> p( idx0, idx1 );
					if (idx0 > idx1)
						std::swap( p.first, p.second );
					overlaps.push_back( p );
				}
			}
		}
	}

	struct Comp {
		bool operator()( const std::pair <size_t, size_t> & p0, const std::pair <size_t, size_t> & p1 ) const {
			if (p0.first == p1.first)
				return p0.second < p1.second;
			else
				return p0.first < p1.first;
		}
	};

	// sort and make unique
	std::sort( overlaps.begin(), overlaps.end(), Comp() );
	std::unique( overlaps.begin(), overlaps.end() );
}

}