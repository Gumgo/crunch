/** @file StaticIf.h
 *  @brief Provides static if functionality.
 */

#ifndef STATICIF_DEFINED
#define STATICIF_DEFINED

/** @brief Instantiates IfTrue if condition is true and IfFalse otherwise.
 */
template <bool condition, typename IfTrue, typename IfFalse> struct StaticIf {
	typedef IfTrue result;
};

template <typename IfTrue, typename IfFalse> struct StaticIf <false, IfTrue, IfFalse> {
	typedef IfFalse result;
};

#endif