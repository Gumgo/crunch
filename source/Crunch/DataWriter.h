/** @file DataWriter.h
 *  @brief Includes functionality for writing binary files.
 */

#ifndef DATAWRITER_DEFINED
#define DATAWRITER_DEFINED

#include "Common.h"
#include <fstream>
#include <vector>

/** @brief A class for writing binary data to a file.
 *
 *  DataWriter adds data to a buffer with calls to various
 *  write methods. Once all writes are done, the file is
 *  written to disk.
 */
class DataWriter {
public:
	/** @brief Used to indicate the current status of the DataWriter instance.
	 */
	enum Status {
		ST_SUCCESS,				/**< Indicates that no error has occurred.*/
		ST_FILE_OPEN_FAILURE,	/**< Indicates that the file could not be opened.*/
		ST_FILE_WRITE_FAILURE	/**< Indicates that the file could not be written.*/
	};

	/** @brief Constructor for the DataWriter class.
	 *
	 *  The constructor opens a file and resizes the buffer to
	 *  the default size specified.
	 *
	 *  @param bufferSize	The default size of the buffer.
	 */
	DataWriter( size_t bufferSize = 64 );

	/** @brief Writes data to the data buffer.
	 *
	 *  This function appends the data provided to the end of the
	 *  buffer.
	 *
	 *  @param src		The location to copy the data from.
	 *  @param amount	The amount in bytes of data to write.
	 */
	void writeData( void * src, size_t amount );

	/** @brief Writes data to the data buffer.
	 *
	 *  This function appends the data provided to the end of the
	 *  buffer.
	 *
	 *  @tparam T		The type of data to write.
	 *  @param src		The location to copy the data from.
	 *  @param amount	The amount of elements of T to write.
	 */
	template <typename T> void writeElements( T * src, size_t amount );

	/** @brief Writes the buffer to a file.
	 *
	 *  @param fname	The name of the file to write to.
	 */
	Status writeToFile( const std::string & fname );

private:
	std::vector <char> data; // the block of data to write
};

template <typename T> void DataWriter::writeElements( T * src, size_t amount ) {
	writeData( src, amount * sizeof( T ) );
}

#endif