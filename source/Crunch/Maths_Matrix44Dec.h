/** @file Maths_Matrix44Dec.h
 *  @brief Contains an implementation of a \htmlonly4&#0215&#0052\endhtmlonly matrix class.
 */

#ifndef MATHS_MATRIX44DEC_DEFINED
#define MATHS_MATRIX44DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief An class for \htmlonly4&#0215&#0052\endhtmlonly matrices.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Matrix44 {
	T d[16]; // the matrix components

public:
	/** @brief Constructs a \htmlonly4&#0215&#0052\endhtmlonly identity matrix.
	 */
	Matrix44();

	/** @brief Constructs a \htmlonly4&#0215&#0052\endhtmlonly matrix with the given components.
	 */
	Matrix44( T m00, T m01, T m02, T m03,
			  T m10, T m11, T m12, T m13,
			  T m20, T m21, T m22, T m23,
			  T m30, T m31, T m32, T m33 );

	/** @brief Constructs a \htmlonly4&#0215&#0052\endhtmlonly matrix by copying the one provided.
	 */
	Matrix44( const Matrix44 <T> & m );

	/** @brief The destructor.
	 */
	~Matrix44();

	// operators

	/** @brief Returns the sum of the current value and the matrix m.
	 */
	Matrix44 <T> operator+( const Matrix44 <T> & m ) const;

	/** @brief Returns the current value unmodified.
	 */
	Matrix44 <T> operator+() const;

	/** @brief Returns the difference between the current value and the matrix m.
	 */
	Matrix44 <T> operator-( const Matrix44 <T> & m ) const;

	/** @brief Returns the current value negated.
	 */
	Matrix44 <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Matrix44 <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Matrix44 <T> operator/( T r ) const;

	/** @brief Returns the product of the current value and the vector v.
	 */
	Vector4 <T> operator*( const Vector4 <T> & v ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix44 <T> operator*( const Matrix44 <T> & m ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix44 <T> operator*( const Matrix44a <T> & m ) const;

	/** @brief Sets the current value to the matrix m.
	 */
	Matrix44 <T> operator=( const Matrix44 <T> & m );

	/** @brief Increments the current value by the matrix m.
	 */
	Matrix44 <T> operator+=( const Matrix44 <T> & m );

	/** @brief Decrements the current value by the matrix m.
	 */
	Matrix44 <T> operator-=( const Matrix44 <T> & m );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Matrix44 <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Matrix44 <T> operator/=( T r );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix44 <T> operator*=( const Matrix44 <T> & m );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix44 <T> operator*=( const Matrix44a <T> & m );

	/** @brief Returns whether the current value is equal to the matrix m.
	 */
	bool operator==( const Matrix44 <T> & m ) const;

	/** @brief Returns whether the current value is not equal to the matrix m.
	 */
	bool operator!=( const Matrix44 <T> & m ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Accesses the component of the current value at (row, col).
	 */
	template <typename U> T & operator()( U row, U col );

	/** @brief Returns the component of the current value at (row, col).
	 */
	template <typename U> T operator()( U row, U col ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Matrix44 <U> () const;

	// mutable

	/** @brief Sets the components of the current value to the components provided.
	 */
	void set( T m00, T m01, T m02, T m03,
			  T m10, T m11, T m12, T m13,
			  T m20, T m21, T m22, T m23,
			  T m30, T m31, T m32, T m33 );

	/** @brief Transposes the current value.
	 */
	void transpose();

	/** @brief Inverts the current value.
	 */
	bool invert();

	/** @brief Sets all components to 0.
	 */
	void zero();

	/** @brief Sets the current value to the identity matrix.
	 */
	void identity();

	// immutable

	/** @brief Returns the transpose of the current value.
	 */
	Matrix44 <T> transposed() const;

	/** @brief Returns the inverse of the current value.
	 */
	Matrix44 <T> inverse() const;

	/** @brief Returns the determinant of the current value.
	 */
	T determinant() const;

	/** @brief Returns the trace of the current value.
	 */
	T trace() const;

	/** @brief Returns a pointer to the component array.
	 */
	const T * getArray() const;
};

/** @brief Returns the product of the scalar r and the matrix m.
 */
template <typename T> Matrix44 <T> operator*( T r, const Matrix44 <T> & m );

/** @brief Matrix44 of type float.
 */
typedef Matrix44 <float> Matrix44f;

/** @brief Matrix44 of type double.
 */
typedef Matrix44 <double> Matrix44d;

#endif