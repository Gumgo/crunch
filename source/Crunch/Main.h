/** @file Main.h
 *  @brief Entry point for execution of the game engine.
 */

#ifndef MAIN_DEFINED
#define MAIN_DEFINED

#include "Common.h"
#include "Context.h"
#include "Log.h"
#include "Runner.h"
#include "MemoryDebug.h"
#include "FastDelegate.h"
#include <vector>
#include <iostream>

/** @brief Crunch's main function.
 *
 *  @param argc					The number of arguments provided.
 *  @param argv					The arguments provided.
 *  @param registrationFunction	A function to register resources and specify the initial state.
 */
int crunchMain( int argc, char ** argv, Context::RegistrationFunction registrationFunction );

#endif