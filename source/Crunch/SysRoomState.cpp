#if 0

#include "SysRoomState.h"
#include "Context.h"

void SysRoomState::onCreate() {
	if (!Context::get().isServer())
		camera = (SysCamera2D*)getActor( createActor( "SysCamera2D" ) );

	physicsManager = (SysPhysicsManager*)getActor( createActor( "SysPhysicsManager" ) );
#ifdef _DEBUG
	if (!Context::get().isServer())
		physicsManager->setCamera( camera );
#endif
}

void SysRoomState::onDestroy() {
	unloadRoom();
}

void SysRoomState::init( const RoomData * rmData, bool createActors ) {
	roomData = rmData;
	physicsManager->physicsManager.initRoom( roomData );

	// load the resources for the room

	// don't need to do anything with actor types or material names since they're handled custom by the game

	if (!Context::get().isServer()) {
		for (size_t i = 0; i < roomData->tilesets.size(); ++i)
			Context::get().resourceManager.acquire <TextureData>( roomData->tilesets[i] );

		for (size_t i = 0; i < roomData->backgrounds.size(); ++i)
			Context::get().resourceManager.acquire <TextureData>( roomData->backgrounds[i] );

		// add backgrounds and tile layers

		for (size_t i = 0; i < roomData->tileLayers.size(); ++i) {
			tileLayers.push_back( (SysTileLayer*)getActor(
				createActorR( "SysTileLayer", roomData->tileLayers[i].renderPriority ) ) );
			tileLayers.back()->setCamera( camera );
			tileLayers.back()->init( roomData->tileLayers[i] );
			const Resource <TextureData> * tilesetRes = Context::get().resourceManager.get <TextureData>(
				roomData->tilesets[roomData->tileLayers[i].tilesetId] );
			if (tilesetRes != NULL)
				tileLayers.back()->setTileset( tilesetRes->getData() );
		}

		for (size_t i = 0; i < roomData->backgroundLayers.size(); ++i) {
			backgroundLayers.push_back( (SysBackgroundLayer*)getActor(
				createActorR( "SysBackgroundLayer", roomData->backgroundLayers[i].renderPriority ) ) );
			backgroundLayers.back()->setCamera( camera );
			backgroundLayers.back()->init( roomData->backgroundLayers[i] );
			const Resource <TextureData> * backgroundRes = Context::get().resourceManager.get <TextureData>(
				roomData->backgrounds[roomData->backgroundLayers[i].backgroundId] );
			if (backgroundRes != NULL)
				backgroundLayers.back()->setBackground( backgroundRes->getData() );
		}
	}

	if (createActors) {
		// create actors last so that everything is set up beforehand for them
		for (size_t i = 0; i < roomData->actors.size(); ++i) {
			const ::ActorData & actorData = roomData->actors[i];

			std::vector <Actor::Property> properties;
			properties.reserve( actorData.properties.size() );
			for (size_t i = 0; i < actorData.properties.size(); ++i) {
				properties.push_back( Actor::Property() );
				properties.back().name = actorData.properties[i].name;
				properties.back().value = actorData.properties[i].value;
			}

			if (actorData.overridesUpdatePriority) {
				if (actorData.overridesRenderPriority)
					createActorUR( roomData->actorTypes[actorData.typeId], actorData.updatePriority, actorData.renderPriority, properties );
				else
					createActorU( roomData->actorTypes[actorData.typeId], actorData.updatePriority, properties );
			} else {
				if (actorData.overridesRenderPriority)
					createActorR( roomData->actorTypes[actorData.typeId], actorData.renderPriority, properties );
				else
					createActor( roomData->actorTypes[actorData.typeId], properties );
			}
		}
	}
}

void SysRoomState::unloadRoom() {
	if (roomData == NULL)
		return;

	if (!Context::get().isServer()) {
		for (size_t i = 0; i < roomData->tilesets.size(); ++i)
			Context::get().resourceManager.release <TextureData>( roomData->tilesets[i] );

		for (size_t i = 0; i < roomData->backgrounds.size(); ++i)
			Context::get().resourceManager.release <TextureData>( roomData->backgrounds[i] );
	}

	roomData = NULL;
}

SysCamera2D * SysRoomState::getCamera() {
	return camera;
}

PhysicsManager * SysRoomState::getPhysicsManager() {
	return &physicsManager->physicsManager;
}

#endif