#ifndef MATHS_MATRIX44ADEF_DEFINED
#define MATHS_MATRIX44ADEF_DEFINED

template <typename T> Matrix44a <T>::Matrix44a() {
	set( 1, 0, 0, 0,
		 0, 1, 0, 0,
		 0, 0, 1, 0 );
}

template <typename T> Matrix44a <T>::Matrix44a( T m00, T m01, T m02, T m03,
												T m10, T m11, T m12, T m13,
												T m20, T m21, T m22, T m23 ) {
	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23 );
}

template <typename T> Matrix44a <T>::Matrix44a( const Quaternion <T> & q, const Vector3 <T> & v ) {
	T i2 = 2*q.i;
	T j2 = 2*q.j;
	T k2 = 2*q.k;

	T ii2 = i2*q.i;
	T ij2 = i2*q.j;
	T ik2 = i2*q.k;
	T ir2 = i2*q.r;
	T jj2 = j2*q.j;
	T jk2 = j2*q.k;
	T jr2 = j2*q.r;
	T kk2 = k2*q.k;
	T kr2 = k2*q.r;

	set( 1 - jj2 - kk2, ij2 - kr2,     ik2 + jr2,     v.x,
		 ij2 + kr2,     1 - ii2 - kk2, jk2 - ir2,     v.y,
		 ik2 - jr2,     jk2 + ir2,     1 - ii2 - jj2, v.z );
}

template <typename T> Matrix44a <T>::Matrix44a( const Matrix44a <T> & m ) {
	set( m[0], m[1], m[ 2], m[ 3],
		 m[4], m[5], m[ 6], m[ 7],
		 m[8], m[9], m[10], m[11] );
}

template <typename T> Matrix44a <T>::Matrix44a( const Matrix33 <T> & m ) {
	set( m[0], m[1], m[2], 0,
		 m[3], m[4], m[5], 0,
		 m[6], m[7], m[8], 0 );
}

template <typename T> Matrix44a <T>::~Matrix44a() {
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator+( const Matrix44a <T> & m ) const {
	return Matrix44a( d[0] + m[0], d[1] + m[1], d[ 2] + m[ 2], d[ 3] + m[ 3],
					  d[4] + m[4], d[5] + m[5], d[ 6] + m[ 6], d[ 7] + m[ 7],
					  d[8] + m[8], d[9] + m[9], d[10] + m[10], d[11] + m[11] );
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator+() const {
	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator-( const Matrix44a <T> & m ) const {
	return Matrix44a( d[0] - m[0], d[1] - m[1], d[ 2] - m[ 2], d[ 3] - m[ 3],
					  d[4] - m[4], d[5] - m[5], d[ 6] - m[ 6], d[ 7] - m[ 7],
					  d[8] - m[8], d[9] - m[9], d[10] - m[10], d[11] - m[11] );
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator-() const {
	return Matrix44a( -d[0], -d[1], -d[ 2], -d[ 3],
					  -d[4], -d[5], -d[ 6], -d[ 7],
					  -d[8], -d[9], -d[10], -d[11] );
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator*( T r ) const {
	return Matrix44a( d[0]*r, d[1]*r, d[ 2]*r, d[ 3]*r,
					  d[4]*r, d[5]*r, d[ 6]*r, d[ 7]*r,
					  d[8]*r, d[9]*r, d[10]*r, d[11]*r );
}

template <typename T> Matrix44a <T> operator*( T r, const Matrix44a <T> & m ) {
	return m*r;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator/( T r ) const {
	return Matrix44a( d[0]/r, d[1]/r, d[ 2]/r, d[ 3]/r,
					  d[4]/r, d[5]/r, d[ 6]/r, d[ 7]/r,
					  d[8]/r, d[9]/r, d[10]/r, d[11]/r );
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator*( const Matrix44a <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[4] + d[ 2]*m[ 8];
	T m01 = d[0]*m[1] + d[1]*m[5] + d[ 2]*m[ 9];
	T m02 = d[0]*m[2] + d[1]*m[6] + d[ 2]*m[10];
	T m03 = d[0]*m[3] + d[1]*m[7] + d[ 2]*m[11] + d[ 3];

	T m10 = d[4]*m[0] + d[5]*m[4] + d[ 6]*m[ 8];
	T m11 = d[4]*m[1] + d[5]*m[5] + d[ 6]*m[ 9];
	T m12 = d[4]*m[2] + d[5]*m[6] + d[ 6]*m[10];
	T m13 = d[4]*m[3] + d[5]*m[7] + d[ 6]*m[11] + d[ 7];

	T m20 = d[8]*m[0] + d[9]*m[4] + d[10]*m[ 8];
	T m21 = d[8]*m[1] + d[9]*m[5] + d[10]*m[ 9];
	T m22 = d[8]*m[2] + d[9]*m[6] + d[10]*m[10];
	T m23 = d[8]*m[3] + d[9]*m[7] + d[10]*m[11] + d[11];

	return Matrix44a <T>( m00, m01, m02, m03,
						  m10, m11, m12, m13,
						  m20, m21, m22, m23 );
}

template <typename T> Matrix44 <T> Matrix44a <T>::operator*( const Matrix44 <T> & m ) const {
	T m00 = d[0]*m[0] + d[1]*m[4] + d[ 2]*m[ 8] + d[ 3]*m[12];
	T m01 = d[0]*m[1] + d[1]*m[5] + d[ 2]*m[ 9] + d[ 3]*m[13];
	T m02 = d[0]*m[2] + d[1]*m[6] + d[ 2]*m[10] + d[ 3]*m[14];
	T m03 = d[0]*m[3] + d[1]*m[7] + d[ 2]*m[11] + d[ 3]*m[15];

	T m10 = d[4]*m[0] + d[5]*m[4] + d[ 6]*m[ 8] + d[ 7]*m[12];
	T m11 = d[4]*m[1] + d[5]*m[5] + d[ 6]*m[ 9] + d[ 7]*m[13];
	T m12 = d[4]*m[2] + d[5]*m[6] + d[ 6]*m[10] + d[ 7]*m[14];
	T m13 = d[4]*m[3] + d[5]*m[7] + d[ 6]*m[11] + d[ 7]*m[15];

	T m20 = d[8]*m[0] + d[9]*m[4] + d[10]*m[ 8] + d[11]*m[12];
	T m21 = d[8]*m[1] + d[9]*m[5] + d[10]*m[ 9] + d[11]*m[13];
	T m22 = d[8]*m[2] + d[9]*m[6] + d[10]*m[10] + d[11]*m[14];
	T m23 = d[8]*m[3] + d[9]*m[7] + d[10]*m[11] + d[11]*m[15];

	T m30 = m[12];
	T m31 = m[13];
	T m32 = m[14];
	T m33 = m[15];

	return Matrix44 <T>( m00, m01, m02, m03,
						 m10, m11, m12, m13,
						 m20, m21, m22, m23,
						 m30, m31, m32, m33 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator*( const Matrix33 <T> & m ) const {
	T m00 = d[ 0]*m[0] + d[1]*m[3] + d[ 2]*m[6];
	T m01 = d[ 0]*m[1] + d[1]*m[4] + d[ 2]*m[7];
	T m02 = d[ 0]*m[2] + d[1]*m[5] + d[ 2]*m[8];
	T m03 = d[ 3];

	T m10 = d[ 4]*m[0] + d[5]*m[3] + d[ 6]*m[6];
	T m11 = d[ 4]*m[1] + d[5]*m[4] + d[ 6]*m[7];
	T m12 = d[ 4]*m[2] + d[5]*m[5] + d[ 6]*m[8];
	T m13 = d[ 7];

	T m20 = d[ 8]*m[0] + d[9]*m[3] + d[10]*m[6];
	T m21 = d[ 8]*m[1] + d[9]*m[4] + d[10]*m[7];
	T m22 = d[ 8]*m[2] + d[9]*m[5] + d[10]*m[8];
	T m23 = d[11];

	return Matrix44a <T>( m00, m01, m02, m03,
						  m10, m11, m12, m13,
						  m20, m21, m22, m23 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator=( const Matrix44a <T> & m ) {
	set( m[0], m[1], m[ 2], m[ 3],
		 m[4], m[5], m[ 6], m[ 7],
		 m[8], m[9], m[10], m[11] );
	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator=( const Matrix33 <T> & m ) {
	set( m[0], m[1], m[2], 0,
		 m[3], m[4], m[5], 0,
		 m[6], m[7], m[8], 0 );
	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator+=( const Matrix44a <T> & m ) {
	d[0] += m[0]; d[1] += m[1]; d[ 2] += m[ 2]; d[ 3] += m[ 3];
	d[4] += m[4]; d[5] += m[5]; d[ 6] += m[ 6]; d[ 7] += m[ 7];
	d[8] += m[8]; d[9] += m[9]; d[10] += m[10]; d[11] += m[11];
	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator-=( const Matrix44a <T> & m ) {
	d[0] -= m[0]; d[1] -= m[1]; d[ 2] -= m[ 2]; d[ 3] -= m[ 3];
	d[4] -= m[4]; d[5] -= m[5]; d[ 6] -= m[ 6]; d[ 7] -= m[ 7];
	d[8] -= m[8]; d[9] -= m[9]; d[10] -= m[10]; d[11] -= m[11];
	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator*=( T r ) {
	d[0] *= r; d[1] *= r; d[ 2] *= r; d[ 3] *= r;
	d[4] *= r; d[5] *= r; d[ 6] *= r; d[ 7] *= r;
	d[8] *= r; d[9] *= r; d[10] *= r; d[11] *= r;
	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator/=( T r ) {
	d[0] /= r; d[1] /= r; d[ 2] /= r; d[ 3] /= r;
	d[4] /= r; d[5] /= r; d[ 6] /= r; d[ 7] /= r;
	d[8] /= r; d[9] /= r; d[10] /= r; d[11] /= r;
	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator*=( const Matrix44a <T> & m ) {
	T m00 = d[0]*m[0] + d[1]*m[4] + d[ 2]*m[ 8];
	T m01 = d[0]*m[1] + d[1]*m[5] + d[ 2]*m[ 9];
	T m02 = d[0]*m[2] + d[1]*m[6] + d[ 2]*m[10];
	T m03 = d[0]*m[3] + d[1]*m[7] + d[ 2]*m[11] + d[ 3];

	T m10 = d[4]*m[0] + d[5]*m[4] + d[ 6]*m[ 8];
	T m11 = d[4]*m[1] + d[5]*m[5] + d[ 6]*m[ 9];
	T m12 = d[4]*m[2] + d[5]*m[6] + d[ 6]*m[10];
	T m13 = d[4]*m[3] + d[5]*m[7] + d[ 6]*m[11] + d[ 7];

	T m20 = d[8]*m[0] + d[9]*m[4] + d[10]*m[ 8];
	T m21 = d[8]*m[1] + d[9]*m[5] + d[10]*m[ 9];
	T m22 = d[8]*m[2] + d[9]*m[6] + d[10]*m[10];
	T m23 = d[8]*m[3] + d[9]*m[7] + d[10]*m[11] + d[11];

	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23 );

	return *this;
}

template <typename T> Matrix44a <T> Matrix44a <T>::operator*=( const Matrix33 <T> & m ) {
	T m00 = d[ 0]*m[0] + d[1]*m[3] + d[ 2]*m[6];
	T m01 = d[ 0]*m[1] + d[1]*m[4] + d[ 2]*m[7];
	T m02 = d[ 0]*m[2] + d[1]*m[5] + d[ 2]*m[8];
	T m03 = d[ 3];

	T m10 = d[ 4]*m[0] + d[5]*m[3] + d[ 6]*m[6];
	T m11 = d[ 4]*m[1] + d[5]*m[4] + d[ 6]*m[7];
	T m12 = d[ 4]*m[2] + d[5]*m[5] + d[ 6]*m[8];
	T m13 = d[ 7];

	T m20 = d[ 8]*m[0] + d[9]*m[3] + d[10]*m[6];
	T m21 = d[ 8]*m[1] + d[9]*m[4] + d[10]*m[7];
	T m22 = d[ 8]*m[2] + d[9]*m[5] + d[10]*m[8];
	T m23 = d[11];

	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23 );

	return *this;
}

template <typename T> bool Matrix44a <T>::operator==( const Matrix44a <T> & m ) const {
	return (d[0] == m[0] && d[1] == m[1] && d[ 2] == m[ 2] && d[ 3] == m[ 3] &&
			d[4] == m[4] && d[5] == m[5] && d[ 6] == m[ 6] && d[ 7] == m[ 7] &&
			d[8] == m[8] && d[9] == m[9] && d[10] == m[10] && d[11] == m[11]);
}

template <typename T> bool Matrix44a <T>::operator!=( const Matrix44a <T> & m ) const {
	return (d[0] != m[0] || d[1] != m[1] || d[ 2] != m[ 2] || d[ 3] != m[ 3] ||
			d[4] != m[4] || d[5] != m[5] || d[ 6] != m[ 6] || d[ 7] != m[ 7] ||
			d[8] != m[8] || d[9] != m[9] || d[10] != m[10] || d[11] != m[11]);
}

template <typename T> template <typename U> T & Matrix44a <T>::operator[]( U i ) {
	return d[i];
}

template <typename T> template <typename U> T Matrix44a <T>::operator[]( U i ) const {
	return d[i];
}

template <typename T> template <typename U> T & Matrix44a <T>::operator()( U row, U col ) {
	return d[row*4+col];
}

template <typename T> template <typename U> T Matrix44a <T>::operator()( U row, U col ) const {
	return d[row*4+col];
}

template <typename T> template <typename U> Matrix44a <T>::operator Matrix44a <U> () const {
	return Matrix44a <U>( (U)d[0], (U)d[1], (U)d[ 2], (U)d[ 3],
						  (U)d[4], (U)d[5], (U)d[ 6], (U)d[ 7],
						  (U)d[8], (U)d[9], (U)d[10], (U)d[11] );
}

template <typename T> void Matrix44a <T>::set( T m00, T m01, T m02, T m03,
											   T m10, T m11, T m12, T m13,
											   T m20, T m21, T m22, T m23 ) {
	d[0] = m00; d[1] = m01; d[ 2] = m02; d[ 3] = m03;
	d[4] = m10; d[5] = m11; d[ 6] = m12; d[ 7] = m13;
	d[8] = m20; d[9] = m21; d[10] = m22; d[11] = m23;
}

template <typename T> bool Matrix44a <T>::invert() {
	T det = determinant();
	if (det == 0) {
		set( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
		return false;
	}

	T invDet = 1/det;

	T m00 = invDet*(d[ 5]*d[10] - d[ 6]*d[ 9]);
	T m01 = invDet*(d[ 2]*d[ 9] - d[ 1]*d[10]);
	T m02 = invDet*(d[ 1]*d[ 6] - d[ 2]*d[ 5]);
	T m03 = invDet*(d[ 1]*d[ 7]*d[10] + d[ 2]*d[ 5]*d[11] + d[ 3]*d[ 6]*d[ 9] - d[ 1]*d[ 6]*d[11] - d[ 2]*d[ 7]*d[ 9] - d[ 3]*d[ 5]*d[10]);
	T m10 = invDet*(d[ 6]*d[ 8] - d[ 4]*d[10]);
	T m11 = invDet*(d[ 0]*d[10] - d[ 2]*d[ 8]);
	T m12 = invDet*(d[ 2]*d[ 4] - d[ 0]*d[ 6]);
	T m13 = invDet*(d[ 0]*d[ 6]*d[11] + d[ 2]*d[ 7]*d[ 8] + d[ 3]*d[ 4]*d[10] - d[ 0]*d[ 7]*d[10] - d[ 2]*d[ 4]*d[11] - d[ 3]*d[ 6]*d[ 8]);
	T m20 = invDet*(d[ 4]*d[ 9] - d[ 5]*d[ 8]);
	T m21 = invDet*(d[ 1]*d[ 8] - d[ 0]*d[ 9]);
	T m22 = invDet*(d[ 0]*d[ 5] - d[ 1]*d[ 4]);
	T m23 = invDet*(d[ 0]*d[ 7]*d[ 9] + d[ 1]*d[ 4]*d[11] + d[ 3]*d[ 5]*d[ 8] - d[ 0]*d[ 5]*d[11] - d[ 1]*d[ 7]*d[ 8] - d[ 3]*d[ 4]*d[ 9]);

	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23 );
	return true;
}

template <typename T> void Matrix44a <T>::translate( T x, T y, T z ) {
	d[ 3] += x;
	d[ 7] += y;
	d[11] += z;
}

template <typename T> void Matrix44a <T>::translate( const Vector3 <T> & t ) {
	translate( t.x, t.y, t.z );
}

template <typename T> void Matrix44a <T>::rotateX( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	T r11 = cosA;
	T r12 = -sinA;
	T r21 = sinA;
	T r22 = cosA;

	T m10 = r11*d[4] + r12*d[ 8];
	T m11 = r11*d[5] + r12*d[ 9];
	T m12 = r11*d[6] + r12*d[10];
	T m13 = r11*d[7] + r12*d[11];

	T m20 = r21*d[4] + r22*d[ 8];
	T m21 = r21*d[5] + r22*d[ 9];
	T m22 = r21*d[6] + r22*d[10];
	T m23 = r21*d[7] + r22*d[11];

	d[4] = m10; d[5] = m11; d[ 6] = m12; d[ 7] = m13;
	d[8] = m20; d[9] = m21; d[10] = m22; d[11] = m23;
}

template <typename T> void Matrix44a <T>::rotateY( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r02 = sinA;
	T r20 = -sinA;
	T r22 = cosA;

	T m00 = r00*d[0] + r02*d[ 8];
	T m01 = r00*d[1] + r02*d[ 9];
	T m02 = r00*d[2] + r02*d[10];
	T m03 = r00*d[3] + r02*d[11];

	T m20 = r20*d[0] + r22*d[ 8];
	T m21 = r20*d[1] + r22*d[ 9];
	T m22 = r20*d[2] + r22*d[10];
	T m23 = r20*d[3] + r22*d[11];

	d[0] = m00; d[1] = m01; d[ 2] = m02; d[ 3] = m03;
	d[8] = m20; d[9] = m21; d[10] = m22; d[11] = m23;
}

template <typename T> void Matrix44a <T>::rotateZ( T a ) {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r01 = -sinA;
	T r10 = sinA;
	T r11 = cosA;

	T m00 = r00*d[0] + r01*d[4];
	T m01 = r00*d[1] + r01*d[5];
	T m02 = r00*d[2] + r01*d[6];
	T m03 = r00*d[3] + r01*d[7];

	T m10 = r10*d[0] + r11*d[4];
	T m11 = r10*d[1] + r11*d[5];
	T m12 = r10*d[2] + r11*d[6];
	T m13 = r10*d[3] + r11*d[7];

	d[0] = m00; d[1] = m01; d[2] = m02; d[3] = m03;
	d[4] = m10; d[5] = m11; d[6] = m12; d[7] = m13;
}

template <typename T> void Matrix44a <T>::rotate( T x, T y, T z, T a ) {
	T sinA = sin( a );
	T cosA = cos( a );
	T xy = x*y;
	T xz = x*z;
	T yz = y*z;

	T r00 = cosA + x*x*(1 - cosA);
	T r01 = xy*(1 - cosA) - z*sinA;
	T r02 = xz*(1 - cosA) + y*sinA;
	T r10 = xy*(1 - cosA) + z*sinA;
	T r11 = cosA + y*y*(1 - cosA);
	T r12 = yz*(1 - cosA) - x*sinA;
	T r20 = xz*(1 - cosA) - y*sinA;
	T r21 = yz*(1 - cosA) - x*sinA;
	T r22 = cosA + z*z*(1 - cosA);

	T m00 = r00*d[0] + r01*d[4] + r02*d[ 8];
	T m01 = r00*d[1] + r01*d[5] + r02*d[ 9];
	T m02 = r00*d[2] + r01*d[6] + r02*d[10];
	T m03 = r00*d[3] + r01*d[7] + r02*d[11];

	T m10 = r10*d[0] + r11*d[4] + r12*d[ 8];
	T m11 = r10*d[1] + r11*d[5] + r12*d[ 9];
	T m12 = r10*d[2] + r11*d[6] + r12*d[10];
	T m13 = r10*d[3] + r11*d[7] + r12*d[11];

	T m20 = r20*d[0] + r21*d[4] + r22*d[ 8];
	T m21 = r20*d[1] + r21*d[5] + r22*d[ 9];
	T m22 = r20*d[2] + r21*d[6] + r22*d[10];
	T m23 = r20*d[3] + r21*d[7] + r22*d[11];

	d[0] = m00; d[1] = m01; d[ 2] = m02; d[ 3] = m03;
	d[4] = m10; d[5] = m11; d[ 6] = m12; d[ 7] = m13;
	d[8] = m20; d[9] = m21; d[10] = m22; d[11] = m23;
}

template <typename T> void Matrix44a <T>::rotate( const AxisAngle <T> & aa ) {
	rotate( aa.axis.x, aa.axis.y, aa.axis.z, aa.angle );
}

template <typename T> void Matrix44a <T>::rotate( const Quaternion <T> & q ) {
	T i2 = 2*q.i;
	T j2 = 2*q.j;
	T k2 = 2*q.k;

	T ii2 = i2*q.i;
	T ij2 = i2*q.j;
	T ik2 = i2*q.k;
	T ir2 = i2*q.r;
	T jj2 = j2*q.j;
	T jk2 = j2*q.k;
	T jr2 = j2*q.r;
	T kk2 = k2*q.k;
	T kr2 = k2*q.r;

	T r00 = 1 - jj2 - kk2;
	T r01 = ij2 - kr2;
	T r02 = ik2 + jr2;
	T r10 = ij2 + kr2;
	T r11 = 1 - ii2 - kk2;
	T r12 = jk2 - ir2;
	T r20 = ik2 - jr2;
	T r21 = jk2 + ir2;
	T r22 = 1 - ii2 + jj2;

	T m00 = r00*d[0] + r01*d[4] + r02*d[ 8];
	T m01 = r00*d[1] + r01*d[5] + r02*d[ 9];
	T m02 = r00*d[2] + r01*d[6] + r02*d[10];
	T m03 = r00*d[3] + r01*d[7] + r02*d[11];

	T m10 = r10*d[0] + r11*d[4] + r12*d[ 8];
	T m11 = r10*d[1] + r11*d[5] + r12*d[ 9];
	T m12 = r10*d[2] + r11*d[6] + r12*d[10];
	T m13 = r10*d[3] + r11*d[7] + r12*d[11];

	T m20 = r20*d[0] + r21*d[4] + r22*d[ 8];
	T m21 = r20*d[1] + r21*d[5] + r22*d[ 9];
	T m22 = r20*d[2] + r21*d[6] + r22*d[10];
	T m23 = r20*d[3] + r21*d[7] + r22*d[11];

	d[0] = m00; d[1] = m01; d[ 2] = m02; d[ 3] = m03;
	d[4] = m10; d[5] = m11; d[ 6] = m12; d[ 7] = m13;
	d[8] = m20; d[9] = m21; d[10] = m22; d[11] = m23;
}

template <typename T> void Matrix44a <T>::scale( T x, T y, T z ) {
	d[ 0] *= x;
	d[ 1] *= x;
	d[ 2] *= x;
	d[ 3] *= x;

	d[ 4] *= y;
	d[ 5] *= y;
	d[ 6] *= y;
	d[ 7] *= y;

	d[ 8] *= z;
	d[ 9] *= z;
	d[10] *= z;
	d[11] *= z;
}

template <typename T> void Matrix44a <T>::scale( const Vector3 <T> & s ) {
	scale( s.x, s.y, s.z );
}

template <typename T> void Matrix44a <T>::identity() {
	set( 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::inverse() const {
	T det = determinant();
	if (det == 0)
		return Matrix44a <T>( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );

	T invDet = 1/det;

	T m00 = invDet*(d[ 5]*d[10] - d[ 6]*d[ 9]);
	T m01 = invDet*(d[ 2]*d[ 9] - d[ 1]*d[10]);
	T m02 = invDet*(d[ 1]*d[ 6] - d[ 2]*d[ 5]);
	T m03 = invDet*(d[ 1]*d[ 7]*d[10] + d[ 2]*d[ 5]*d[11] + d[ 3]*d[ 6]*d[ 9] - d[ 1]*d[ 6]*d[11] - d[ 2]*d[ 7]*d[ 9] - d[ 3]*d[ 5]*d[10]);
	T m10 = invDet*(d[ 6]*d[ 8] - d[ 4]*d[10]);
	T m11 = invDet*(d[ 0]*d[10] - d[ 2]*d[ 8]);
	T m12 = invDet*(d[ 2]*d[ 4] - d[ 0]*d[ 6]);
	T m13 = invDet*(d[ 0]*d[ 6]*d[11] + d[ 2]*d[ 7]*d[ 8] + d[ 3]*d[ 4]*d[10] - d[ 0]*d[ 7]*d[10] - d[ 2]*d[ 4]*d[11] - d[ 3]*d[ 6]*d[ 8]);
	T m20 = invDet*(d[ 4]*d[ 9] - d[ 5]*d[ 8]);
	T m21 = invDet*(d[ 1]*d[ 8] - d[ 0]*d[ 9]);
	T m22 = invDet*(d[ 0]*d[ 5] - d[ 1]*d[ 4]);
	T m23 = invDet*(d[ 0]*d[ 7]*d[ 9] + d[ 1]*d[ 4]*d[11] + d[ 3]*d[ 5]*d[ 8] - d[ 0]*d[ 5]*d[11] - d[ 1]*d[ 7]*d[ 8] - d[ 3]*d[ 4]*d[ 9]);

	return Matrix44a <T>( m00, m01, m02, m03,
						  m10, m11, m12, m13,
						  m20, m21, m22, m23 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::translated( T x, T y, T z ) const {
	return Matrix44a <T>( d[0], d[1], d[ 2], d[ 3] + x,
						  d[4], d[5], d[ 6], d[ 7] + y,
						  d[8], d[9], d[10], d[11] + z );
}

template <typename T> Matrix44a <T> Matrix44a <T>::translated( const Vector3 <T> & t ) const {
	return translated( t.x, t.y, t.z );
}

template <typename T> Matrix44a <T> Matrix44a <T>::rotatedX( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	T r11 = cosA;
	T r12 = -sinA;
	T r21 = sinA;
	T r22 = cosA;

	T m10 = r11*d[4] + r12*d[ 8];
	T m11 = r11*d[5] + r12*d[ 9];
	T m12 = r11*d[6] + r12*d[10];
	T m13 = r11*d[7] + r12*d[11];

	T m20 = r21*d[4] + r22*d[ 8];
	T m21 = r21*d[5] + r22*d[ 9];
	T m22 = r21*d[6] + r22*d[10];
	T m23 = r21*d[7] + r22*d[11];

	return Matrix44a <T>( d[0], d[1], d[2], d[3],
						  m10, m11, m12, m13,
						  m20, m21, m22, m23 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::rotatedY( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r02 = sinA;
	T r20 = -sinA;
	T r22 = cosA;

	T m00 = r00*d[0] + r02*d[ 8];
	T m01 = r00*d[1] + r02*d[ 9];
	T m02 = r00*d[2] + r02*d[10];
	T m03 = r00*d[3] + r02*d[11];

	T m20 = r20*d[0] + r22*d[ 8];
	T m21 = r20*d[1] + r22*d[ 9];
	T m22 = r20*d[2] + r22*d[10];
	T m23 = r20*d[3] + r22*d[11];

	return Matrix44a <T>( m00, m01, m02, m03,
						  d[4], d[5], d[6], d[7],
						  m20, m21, m22, m23 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::rotatedZ( T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );

	T r00 = cosA;
	T r01 = -sinA;
	T r10 = sinA;
	T r11 = cosA;

	T m00 = r00*d[0] + r01*d[4];
	T m01 = r00*d[1] + r01*d[5];
	T m02 = r00*d[2] + r01*d[6];
	T m03 = r00*d[3] + r01*d[7];

	T m10 = r10*d[0] + r11*d[4];
	T m11 = r10*d[1] + r11*d[5];
	T m12 = r10*d[2] + r11*d[6];
	T m13 = r10*d[3] + r11*d[7];

	return Matrix44a <T>( m00, m01, m02, m03,
						  m10, m11, m12, m13,
						  d[8], d[9], d[10], d[11] );
}

template <typename T> Matrix44a <T> Matrix44a <T>::rotated( T x, T y, T z, T a ) const {
	T sinA = sin( a );
	T cosA = cos( a );
	T xy = x*y;
	T xz = x*z;
	T yz = y*z;

	T r00 = cosA + x*x*(1 - cosA);
	T r01 = xy*(1 - cosA) - z*sinA;
	T r02 = xz*(1 - cosA) + y*sinA;
	T r10 = xy*(1 - cosA) + z*sinA;
	T r11 = cosA + y*y*(1 - cosA);
	T r12 = yz*(1 - cosA) - x*sinA;
	T r20 = xz*(1 - cosA) - y*sinA;
	T r21 = yz*(1 - cosA) - x*sinA;
	T r22 = cosA + z*z*(1 - cosA);

	T m00 = r00*d[0] + r01*d[4] + r02*d[ 8];
	T m01 = r00*d[1] + r01*d[5] + r02*d[ 9];
	T m02 = r00*d[2] + r01*d[6] + r02*d[10];
	T m03 = r00*d[3] + r01*d[7] + r02*d[11];

	T m10 = r10*d[0] + r11*d[4] + r12*d[ 8];
	T m11 = r10*d[1] + r11*d[5] + r12*d[ 9];
	T m12 = r10*d[2] + r11*d[6] + r12*d[10];
	T m13 = r10*d[3] + r11*d[7] + r12*d[11];

	T m20 = r20*d[0] + r21*d[4] + r22*d[ 8];
	T m21 = r20*d[1] + r21*d[5] + r22*d[ 9];
	T m22 = r20*d[2] + r21*d[6] + r22*d[10];
	T m23 = r20*d[3] + r21*d[7] + r22*d[11];

	return Matrix44a <T>( m00, m01, m02, m03,
						  m10, m11, m12, m13,
						  m20, m21, m22, m23 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::rotated( const AxisAngle <T> & aa ) const {
	return rotated( aa.axis.x, aa.axis.y, aa.axis.z, aa.angle );
}

template <typename T> Matrix44a <T> Matrix44a <T>::rotated( const Quaternion <T> & q ) const {
	T i2 = 2*q.i;
	T j2 = 2*q.j;
	T k2 = 2*q.k;

	T ii2 = i2*q.i;
	T ij2 = i2*q.j;
	T ik2 = i2*q.k;
	T ir2 = i2*q.r;
	T jj2 = j2*q.j;
	T jk2 = j2*q.k;
	T jr2 = j2*q.r;
	T kk2 = k2*q.k;
	T kr2 = k2*q.r;

	T r00 = 1 - jj2 - kk2;
	T r01 = ij2 - kr2;
	T r02 = ik2 + jr2;
	T r10 = ij2 + kr2;
	T r11 = 1 - ii2 - kk2;
	T r12 = jk2 - ir2;
	T r20 = ik2 - jr2;
	T r21 = jk2 + ir2;
	T r22 = 1 - ii2 - jj2;

	T m00 = r00*d[0] + r01*d[4] + r02*d[ 8];
	T m01 = r00*d[1] + r01*d[5] + r02*d[ 9];
	T m02 = r00*d[2] + r01*d[6] + r02*d[10];
	T m03 = r00*d[3] + r01*d[7] + r02*d[11];

	T m10 = r10*d[0] + r11*d[4] + r12*d[ 8];
	T m11 = r10*d[1] + r11*d[5] + r12*d[ 9];
	T m12 = r10*d[2] + r11*d[6] + r12*d[10];
	T m13 = r10*d[3] + r11*d[7] + r12*d[11];

	T m20 = r20*d[0] + r21*d[4] + r22*d[ 8];
	T m21 = r20*d[1] + r21*d[5] + r22*d[ 9];
	T m22 = r20*d[2] + r21*d[6] + r22*d[10];
	T m23 = r20*d[3] + r21*d[7] + r22*d[11];

	return Matrix44a <T>( m00, m01, m02, m03,
						  m10, m11, m12, m13,
						  m20, m21, m22, m23 );
}

template <typename T> Matrix44a <T> Matrix44a <T>::scaled( T x, T y, T z ) const {
	return Matrix44a <T>( d[0]*x, d[1]*x, d[ 2]*x, d[ 3]*x,
						  d[4]*y, d[5]*y, d[ 6]*y, d[ 7]*y,
						  d[8]*z, d[9]*z, d[10]*z, d[11]*z );
}

template <typename T> Matrix44a <T> Matrix44a <T>::scaled( const Vector3 <T> & s ) const {
	return scaled( s.x, s.y, s.z );
}

template <typename T> T Matrix44a <T>::determinant() const {
	return d[0]*(d[10]*d[5] - d[9]*d[6]) + d[4]*(d[9]*d[2] - d[10]*d[1]) + d[8]*(d[6]*d[1] - d[5]*d[2]);
}

template <typename T> T Matrix44a <T>::trace() const {
	return d[0] + d[5] + d[10] + 1;
}

template <typename T> const T * Matrix44a <T>::getArray() const {
	return d;
}

template <typename T> Vector3 <T> Matrix44a <T>::transformPoint( const Vector3 <T> & p ) const {
	return Vector3 <T>( d[0]*p.x + d[1]*p.y + d[ 2]*p.z + d[ 3],
						d[4]*p.x + d[5]*p.y + d[ 6]*p.z + d[ 7],
						d[8]*p.x + d[9]*p.y + d[10]*p.z + d[11] );
}

template <typename T> Vector3 <T> Matrix44a <T>::transformVector( const Vector3 <T> & v ) const {
	return Vector3 <T>( d[0]*v.x + d[1]*v.y + d[ 2]*v.z,
						d[4]*v.x + d[5]*v.y + d[ 6]*v.z,
						d[8]*v.x + d[9]*v.y + d[10]*v.z );
}

template <typename T> Ray3 <T> Matrix44a <T>::transformRay( const Ray3 <T> & r ) const {
	return Ray3 <T>(
		transformPoint( ray.origin ),
		transformVector( ray.direction ) );
}

template <typename T> Matrix33 <T> Matrix44a <T>::getNormalTransformation() const {
	Matrix33 <T> mat33( d[0], d[1], d[ 2],
						d[4], d[5], d[ 6],
						d[8], d[9], d[10] );
	return mat33.inverse().transposed();
}

#endif