#include "SysDeferredSsao.h"
#include "Context.h"
#include "SysRenderer2D.h"
#include "SysRendererDeferredLighting.h"
#include "Log.h"

SysDeferredSsao::SysDeferredSsao() {
	computeSsaoProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___ssao_deferred_compute_ssao", true );
	ready =
		(computeSsaoProgram->getStatus() == Resource::S_LOADED);

	GpuBufferReference vertexBuffer = SysRenderer2D::get().getQuadVertexBuffer();
	computeSsaoAuto.setProgram( computeSsaoProgram->getProgram() );
	computeSsaoAuto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	computeSsaoAuto.setUniform( "imagePlaneHalfSize", GL_FLOAT_VEC2, &computeSsaoUniforms.imagePlaneHalfSize );
	computeSsaoAuto.setUniform( "normalSpecMap", GL_SAMPLER_2D, &computeSsaoUniforms.normalSpecMap );
	computeSsaoAuto.setUniform( "depthMap", GL_SAMPLER_2D, &computeSsaoUniforms.depthMap );
	computeSsaoAuto.setUniform( "rotationMap", GL_SAMPLER_2D, &computeSsaoUniforms.rotationMap );
	computeSsaoAuto.setUniform( "sizeOverRotationMapSize", GL_FLOAT_VEC2, &computeSsaoUniforms.sizeOverRotationMapSize );
	computeSsaoAuto.setUniform( "worldRadius", GL_FLOAT, &computeSsaoUniforms.worldRadius );
	computeSsaoAuto.setUniform( "projectionMatrix", GL_FLOAT_MAT4, &computeSsaoUniforms.projectionMatrix );
	computeSsaoUniforms.depthMap = 0;
	computeSsaoUniforms.normalSpecMap = 1;
	computeSsaoUniforms.rotationMap = 2;

	camera = NULL;

	// code to generate kernel (just run this once, then comment out):
	/*{
		// distribute uniform points in hemisphere, normalize
		// then distribute radius non-uniformly
		static const size_t PTS = 8;
		std::ofstream out( "kernel.txt" );
		std::default_random_engine rnd( 132948 );
		for (size_t i = 0; i < PTS; ++i) {
			Vector3f p;
			do {
				// generate points in a box, ignore ones outside sphere
				p.x = 2.0f * (float)rnd() / (float)rnd.max() - 1.0f;
				p.y = 2.0f * (float)rnd() / (float)rnd.max() - 1.0f;
				p.z = (float)rnd() / (float)rnd.max();
			} while (p.magnitude() > 1.0f);
			p.normalize();
			// causes point radii to increase outward non linearly
			float scale = (float)i / (float)PTS;
			scale = lerp( 0.1f, 1.0f, scale*scale );
			p *= scale;
			out << p.x << ", " << p.y << ", " << p.z << "\n";
		}
	}*/
}

SysDeferredSsao::~SysDeferredSsao() {
}

void SysDeferredSsao::setCamera( SysCamera3D * cam ) {
	camera = cam;
}

void SysDeferredSsao::setupSsao( float radius ) {
	computeSsaoUniforms.worldRadius = radius;

	// generate random texture
	static const uint SIZE = 4;
	std::default_random_engine rnd( 489532 ); // just some random seed
	Vector2f randomValues[SIZE*SIZE];
	for (size_t i = 0; i < arraySize( randomValues ); ++i) {
		float angle = 2.0f * boost::math::constants::pi <float>() * (float)rnd() / (float)rnd.max();
		randomValues[i] = Vector2f( cos( angle ), sin( angle ) );
	}

	GpuTexture::Texture2DDescription desc;
	desc.imageDescription.internalFormat = GL_RG16F;
	desc.imageDescription.minFilter = GL_NEAREST;
	desc.imageDescription.magFilter = GL_NEAREST;
	GpuTexture::Data2DDescription data;
	data.width = SIZE;
	data.height = SIZE;
	data.format = GL_RG;
	data.type = GL_FLOAT;
	data.data = &randomValues[0];
	desc.mipmapLevels = &data;
	rotationTexture = Context::get().gpuState.createTexture( desc );
}

void SysDeferredSsao::computeSsao() {
	size_t w = SysRendererDeferredLighting::get().getNormalSpecularBuffer()->getWidth();
	size_t h = SysRendererDeferredLighting::get().getNormalSpecularBuffer()->getHeight();

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( false );
	rState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( false );
	dsState.setDepthWriteEnabled( false );
	dsState.bind();

	SysRenderer2D::get().getQuadVertexBuffer()->bind();

	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->attach( GL_COLOR_ATTACHMENT0, SysRendererDeferredLighting::get().getAmbientOcclusionBuffer() );
	fb->detatch( GL_COLOR_ATTACHMENT1 );
	fb->detatch( GL_COLOR_ATTACHMENT2 );
	fb->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	GLenum drawBuf = GL_COLOR_ATTACHMENT0;
	dbState.setDrawBuffers( 1, &drawBuf );
	dbState.bind();

	SysRendererDeferredLighting::get().getNormalSpecularBuffer()->bind( (size_t)computeSsaoUniforms.normalSpecMap );
	SysRendererDeferredLighting::get().getDepthBuffer()->bind( (size_t)computeSsaoUniforms.depthMap );
	rotationTexture->bind( (size_t)computeSsaoUniforms.rotationMap );
	computeSsaoUniforms.imagePlaneHalfSize.y = tan( degToRad( camera->getFov() * 0.5f ) );
	computeSsaoUniforms.imagePlaneHalfSize.x = computeSsaoUniforms.imagePlaneHalfSize.y * camera->getAspectRatio();
	computeSsaoUniforms.sizeOverRotationMapSize = Vector2f(
		(float)w / (float)rotationTexture->getWidth(),
		(float)h / (float)rotationTexture->getHeight() );
	computeSsaoUniforms.projectionMatrix = camera->getProjectionMatrix();

	computeSsaoAuto.bind();

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)w, (int)h ) );
	vpState.bind();

	SysRenderer2D::get().drawSingleQuad();
}

bool SysDeferredSsao::ssao() {
	if (!ready || camera == NULL)
		return false;

	computeSsao();
	return true;
}