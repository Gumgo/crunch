#if 0

/** @file RenderEnums.h
 *  @brief Defines enums used by the renderer.
 */

#ifndef RENDERENUMS_DEFINED
#define RENDERENUMS_DEFINED

#include "Common.h"
#include "OpenGL.h"
#include "HashMap.h"

/** @brief The type of a shader.
 */
enum ShaderType {
	ST_VERTEX = 0,		/**< A vertex shader.*/
	ST_GEOMETRY,		/**< A geometry shader.*/
	ST_FRAGMENT,		/**< A fragment shader.*/
	SHADER_TYPE_COUNT	/**< The number of shader types.*/
};

/** @brief The blending mode used by the shader.
 */
enum BlendMode {
	BM_NONE,				/**< No blending.*/
	BM_ALPHA,				/**< Alpha blending (src_alpha, 1 - src_alpha).*/
	BM_PREMULTIPLIED_ALPHA,	/**< Premultiplied alpha blending (src_alpha, 1).*/
	BM_ADDITIVE,			/**< Additive blending (one, one).*/
	BM_MULTIPLICATIVE,		/**< Multiplicative blending (zero, src_color).*/
	BM_INVERSE,				/**< Inverse blending (1 - dst_color, 1 - src_color).*/
	BLEND_MODE_COUNT		/**< The number of blend modes.*/
};

/** @brief Describes a GLSL type.
 */
struct GlslTypeDescription {
	GLenum type;		/**< The type.*/
	GLenum baseType;	/**< GL_FLOAT, GL_INT, GL_UNSIGNED_INT, GL_BOOL, or one of the GL_SAMPLER types.*/
	size_t size;		/**< The number of elements.*/
};

/** @brief Returns a description of the given type.
 */
GlslTypeDescription getTypeDescription( GLenum type );

#endif

#endif