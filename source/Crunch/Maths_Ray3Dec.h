/** @file Maths_Ray3Dec.h
 *  @brief Contains an implementation of a 3D ray class.
 */

#ifndef MATHS_RAY3DEC_DEFINED
#define MATHS_RAY3DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for 3D rays.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Ray3 {
public:
	Vector3 <T> origin;		/**< The ray origin.*/
	Vector3 <T> direction;	/**< The ray direction.*/

	/** @brief Constructs a ray at the origin with no direction.
	 */
	Ray3();

	/** @brief Constructs a ray with the given origin and direction.
	 */
	Ray3( const Vector3 <T> & orig, const Vector3 <T> & dir );

	/** @brief Constructs a ray by copying the one provided.
	 */
	Ray3( const Ray3 <T> & r );

	/** @brief The destructor.
	 */
	~Ray3();

	// operators

	/** @brief Sets the current value to the ray r.
	 */
	Ray3 <T> operator=( const Ray3 <T> & r );

	/** @brief Returns whether the current value is equal to the ray r.
	 */
	bool operator==( const Ray3 <T> & r ) const;

	/** @brief Returns whether the current value is not equal to the ray r.
	 */
	bool operator!=( const Ray3 <T> & r ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Ray3 <U> () const;

	// mutable

	/** @brief Sets the current value's components to (px, py).
	 */
	void set( const Vector3 <T> & orig, const Vector3 <T> & dir );
};

/** @brief Ray of type float.
 */
typedef Ray3 <float> Ray3f;

/** @brief Ray of type double.
 */
typedef Ray3 <double> Ray3d;

#endif