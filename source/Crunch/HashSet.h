/** @file HashSet.h
 *  @brief Contains a hash set implementation.
 */

#ifndef HASHSET_DEFINED
#define HASHSET_DEFINED

#include "Common.h"
#include "Indexer.h"
#include <vector>
#include <stack>

/** @brief Provides a hash set implementation.
 *
 *  @tparam H	The hash function object.
 *  @tparam V	Type of value used.
 */
template <typename H, typename V> class HashSet {
public:
	/** @brief The index type.
	 */
	typedef size_t Index;

	struct Entry;

	/** @brief Provides a way to iterate over all entries.
	 */
	class Iterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		V & get();				/**< Returns the current entry.*/
		Index getIndex();		/**< Returns the index of the current entry.*/
		void remove();			/**< Removes the current entry.*/

	private:
		friend class HashSet <H, V>;

		HashSet <H, V> * base;					// the underlying hash set
		typename Indexer <Entry>::Iterator it;	// the underlying iterator

		Iterator( HashSet <H, V> * b );
	};

	/** @brief Provides a way to iterate over all entries.
	 */
	class ConstIterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		const V & get();		/**< Returns the current entry.*/
		Index getIndex();		/**< Returns the index of the current entry.*/

	private:
		friend class HashSet <H, V>;

		typename Indexer <Entry>::ConstIterator it; // the underlying iterator

		ConstIterator( const HashSet <H, V> * b );
	};

	friend class Iterator;
	friend class ConstIterator;

	/** @brief The constructor.
	 *
	 *  @param defaultCapacity	The capacity of the hash table.
	 *  @param h				The hash object.
	 */
	HashSet( size_t defaultCapacity = 20, const H & h = H(),
		float minLoadFactor = -std::numeric_limits <float>::max(),
		float maxLoadFactor = std::numeric_limits <float>::max() );

	/** @brief The copy constructor.
	 *
	 *  @param other	The HashSet to copy.
	 */
	HashSet( const HashSet <H, V> & other );

	/** @brief The move constructor.
	 *
	 *  @param other	The HashSet to move.
	 */
	HashSet( HashSet <H, V> && other );

	/** @brief The copy assignment operator.
	 *
	 *  @param other	The HashSet to assign.
	 */
	HashSet <H, V> & operator=( const HashSet <H, V> & other );

	/** @brief The move assignment operator.
	 *
	 *  @param other	The HashSet to move.
	 */
	HashSet <H, V> & operator=( HashSet <H, V> && other );

	/** @brief Puts the value into the hash table.
	 *
	 *  @param value	The value to store in the table.
	 *  @return			The index of the newly inserted value.
	 */
	Index put( const V & value );

	/** @brief Returns the value associated with the given index.
	 *
	 *  @note Because altering an an object could change its hash,
	 *  it is not possible to obtain a non-const reference to an object
	 *  in the table.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @return			The value associated with the given index.
	 */
	const V & getByIndex( Index index ) const;

	/** @brief Assigns the value associated with the given index to value if it exists.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @param value	The destination to store the value.
	 *  @return			Whether the value was found and assigned.
	 */
	bool getByIndex( Index index, V & value ) const;

	/** @brief Returns the index associated with the given value.
	 *
	 *  @param value	The value for which to find the associated index.
	 *  @return			The index associated with the given value.
	 */
	Index getIndex( const V & value ) const;

	/** @brief Assigns the index associated with the given value to index if it exists.
	 *
	 *  @param value	The value for which to find the associated index.
	 *  @param index	The destination to store the index.
	 *  @return			Whether the index was found and assigned.
	 */
	bool getIndex( const V & value, Index & index ) const;

	/** @brief Removes the value from the table.
	 *
	 *  @param value	The value to remove.
	 *  @return			Whether the value was found and removed.
	 */
	bool remove( const V & value );

	/** @brief Removes the value associated with the index from the table.
	 *
	 *  @param index	The index associated with the value to remove.
	 *  @return			Whether the value was found and removed.
	 */
	bool removeByIndex( Index index );

	/** @brief Removes and returns the value associated with the given index.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @return			The value associated with the given index.
	 */
	V extractByIndex( Index index );

	/** @brief Assigns the value associated with the given index to value and removes it from the table if it exists.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @param value	The destination to store the value.
	 *  @return			Whether the value was found and assigned.
	 */
	bool extractByIndex( Index index, V & value );

	/** @brief Returns whether the table contains the given value.
	 *
	 *  @param value	The value to check for.
	 *  @return			Whether the table contains the given value.
	 */
	bool contains( const V & value ) const;

	/** @brief Returns whether the table contains a value associated with the given index.
	 *
	 *  @param index	The index to check for.
	 *  @return			Whether the table contains a value associated with the given index.
	 */
	bool containsIndex( Index index ) const;

	/** @brief Removes all values.
	 */
	void clear();

	/** @brief Resizes the capacity.
	 *
	 *  @param newCapacity	The new capacity.
	 */
	void resize( Index newCapacity );

	/** @brief Returns the capacity of the hash table.
	 *
	 *  Because separate chaining is used,
	 *  this capacity can be exceeded, but
	 *  performance will decrease.
	 */
	size_t capacity() const;

	/** @brief Returns the number of values currently stored in the hash table.
	 */
	size_t size() const;

	/** @brief Returns true if the hash table is empty.
	 */
	bool empty() const;

	/** @brief Returns an iterator.
	 */
	Iterator getIterator();

	/** @brief Returns a const iterator.
	 */
	ConstIterator getConstIterator() const;

	/** @brief Returns the amount of memory used.
	 */
	size_t memory() const;

	/** @brief Returns minimum load factor.
	 */
	float getMinLoadFactor() const;

	/** @brief Returns maximum load factor.
	 */
	float getMaxLoadFactor() const;

private:
	H hash; // hash function object
	float minimumLoadFactor;
	float maximumLoadFactor;
	void adjustForMinLoadFactor( size_t newSize );
	void adjustForMaxLoadFactor( size_t newSize );

	// the value representing no entry
	static const Index NO_INDEX = boost::integer_traits <Index>::const_max;

	// an entry in the table
	struct Entry {
		V value;			// the value associated with the entry
		Index prevEntry;	// the next entry in this table bucket, or the bucket index if the highest bit is 1
		Index nextEntry;	// the previous entry in this table bucket

		Entry( const V & v );					// constructs entry with the given value
		Entry( V && v );						// constructs entry with the given value
		Entry( const Entry & e );				// constructs entry from another entry
		Entry( Entry && e );					// constructs entry from another entry
		Entry & operator=( const Entry & e );	// assigns the entry from another entry
		Entry & operator=( Entry && e );		// assigns the entry from another entry

		bool isFirstEntry() const;		// returns whether this is the first entry in the table bucket
		Index getTableIndex() const;	// returns the bucket index if this is the first entry in the table bucket
		void setTableIndex( Index b );	// sets the bucket index
	};

	std::vector <Index> table;	// the table of indices pointing to entries
	Indexer <Entry> entries;	// the entries
};

template <typename H, typename V>
HashSet <H, V>::Entry::Entry( const V & v )
	: value( v ) {
}

template <typename H, typename V>
HashSet <H, V>::Entry::Entry( V && v )
	: value( v ) {
}

template <typename H, typename V>
HashSet <H, V>::Entry::Entry( const Entry & e )
	: value( e.value ) {
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
}

template <typename H, typename V>
HashSet <H, V>::Entry::Entry( Entry && e )
	: value( std::move( e.value ) ) {
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
}

template <typename H, typename V>
typename HashSet <H, V>::Entry & HashSet <H, V>::Entry::operator=( const Entry & e ) {
	value = e.value;
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
	return *this;
}

template <typename H, typename V>
typename HashSet <H, V>::Entry & HashSet <H, V>::Entry::operator=( Entry && e ) {
	value = std::move( e.value );
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
	return *this;
}

template <typename H, typename V>
bool HashSet <H, V>::Entry::isFirstEntry() const {
	return (prevEntry & (1 << (sizeof( prevEntry )*8 - 1))) != 0;
}

template <typename H, typename V>
typename HashSet <H, V>::Index HashSet <H, V>::Entry::getTableIndex() const {
	return prevEntry & ~(1 << (sizeof( prevEntry )*8 - 1));
}

template <typename H, typename V>
void HashSet <H, V>::Entry::setTableIndex( Index b ) {
	prevEntry = b | (1 << (sizeof( prevEntry )*8 - 1));
}

template <typename H, typename V> HashSet <H, V>::Iterator::Iterator( HashSet <H, V> * b )
	: it( b->entries.getIterator() ) {
	base = b;
}

template <typename H, typename V> bool HashSet <H, V>::Iterator::hasNext() const {
	return it.hasNext();
}

template <typename H, typename V> bool HashSet <H, V>::Iterator::next() {
	return it.next();
}

template <typename H, typename V> V & HashSet <H, V>::Iterator::get() {
	return it.get().value;
}

template <typename H, typename V> typename HashSet <H, V>::Index HashSet <H, V>::Iterator::getIndex() {
	return it.getIndex();
}

template <typename H, typename V> void HashSet <H, V>::Iterator::remove() {
	Index index = it.getIndex();

	Entry & e = base->entries.get( index );
	if (e.isFirstEntry())
		base->table[e.getTableIndex()] = e.nextEntry;
	else
		base->entries.get( e.prevEntry ).nextEntry = e.nextEntry;
	if (e.nextEntry != NO_INDEX)
		base->entries.get( e.nextEntry ).prevEntry = e.prevEntry;

	it.remove();
}

template <typename H, typename V> HashSet <H, V>::ConstIterator::ConstIterator( const HashSet <H, V> * b )
	: it( b->entries.getConstIterator() ) {
}

template <typename H, typename V> bool HashSet <H, V>::ConstIterator::hasNext() const {
	return it.hasNext();
}

template <typename H, typename V> bool HashSet <H, V>::ConstIterator::next() {
	return it.next();
}

template <typename H, typename V> const V & HashSet <H, V>::ConstIterator::get() {
	return it.get().value;
}

template <typename H, typename V> typename HashSet <H, V>::Index HashSet <H, V>::ConstIterator::getIndex() {
	return it.getIndex();
}

template <typename H, typename V>
HashSet <H, V>::HashSet( size_t defaultCapacity, const H & h, float minLoadFactor, float maxLoadFactor )
	: hash( h )
	, minimumLoadFactor( minLoadFactor )
	, maximumLoadFactor( maxLoadFactor )
	, entries( defaultCapacity ) {
	// to avoid useless allocations, defaultCapacity can be 0
	// however, attempting to use the data structure in nearly way is undefined (most likely will crash)
	table.resize( defaultCapacity, NO_INDEX );
}

template <typename H, typename V>
HashSet <H, V>::HashSet( const HashSet <H, V> & other )
	: hash( other.hash )
	, minimumLoadFactor( other.minimumLoadFactor )
	, maximumLoadFactor( other.maximumLoadFactor )
	, table( other.table )
	, entries( other.entries ) {
}

template <typename H, typename V>
HashSet <H, V>::HashSet( HashSet <H, V> && other )
	: hash( other.hash )
	, minimumLoadFactor( other.minimumLoadFactor )
	, maximumLoadFactor( other.maximumLoadFactor )
	, entries( std::move( other.entries ) )
	, table( std::move( other.table ) ) {
}

template <typename H, typename V>
HashSet <H, V> & HashSet <H, V>::operator=( const HashSet <H, V> & other ) {
	hash = other.hash;
	minimumLoadFactor = other.minimumLoadFactor;
	maximumLoadFactor = other.maximumLoadFactor;
	entries = other.entries;
	table = other.table;
	return *this;
}

template <typename H, typename V>
HashSet <H, V> & HashSet <H, V>::operator=( HashSet <H, V> && other ) {
	std::swap( hash, other.hash );
	minimumLoadFactor = other.minimumLoadFactor;
	maximumLoadFactor = other.maximumLoadFactor;
	table.swap( other.table );
	entries = std::move( other.entries );
	return *this;
}

template <typename H, typename V>
typename HashSet <H, V>::Index HashSet <H, V>::put( const V & value ) {
	adjustForMaxLoadFactor( entries.size() + 1 );

	size_t tableIndex = (size_t)(hash( value ) % table.size());

	// add to front of list the bucket
	Index index = entries.add( Entry( value ) );
	Entry & e = entries.get( index );
	e.setTableIndex( tableIndex );
	e.nextEntry = table[tableIndex];
	table[tableIndex] = index;
	if (e.nextEntry != NO_INDEX)
		entries.get( e.nextEntry ).prevEntry = index;

	return index;
}

template <typename H, typename V>
const V & HashSet <H, V>::getByIndex( Index index ) const {
	try {
		return entries.get( index ).value;
	} catch (const std::exception &) {
		throw std::runtime_error( "No value exists with the specified index" );
	}
}

template <typename H, typename V>
bool HashSet <H, V>::getByIndex( Index index, V & value ) const {
	if (entries.contains( index )) {
		value = entries.get( index ).value;
		return true;
	} else
		return false;
}

template <typename H, typename V>
typename HashSet <H, V>::Index HashSet <H, V>::getIndex( const V & value ) const {
	Index tableIndex = (Index)(hash( value ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		const Entry & e = entries.get( index );
		if (value == e.value)
			return index;
		index = e.nextEntry;
	}

	throw std::runtime_error( "No index exists with the specified value" );
}

template <typename H, typename V>
bool HashSet <H, V>::getIndex( const V & value, Index & index ) const {
	Index tableIndex = (Index)(hash( value ) % table.size());

	Index idx = table[tableIndex];
	while (idx != NO_INDEX) {
		const Entry & e = entries.get( idx );
		if (value == e.value) {
			index = idx;
			return true;
		}
		idx = e.nextEntry;
	}

	return false;
}

template <typename H, typename V>
bool HashSet <H, V>::remove( const V & value ) {
	Index tableIndex = (Index)(hash( value ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		Entry & e = entries.get( index );
		if (value == e.value) {
			if (e.isFirstEntry())
				table[tableIndex] = e.nextEntry;
			else
				entries.get( e.prevEntry ).nextEntry = e.nextEntry;
			if (e.nextEntry != NO_INDEX)
				entries.get( e.nextEntry ).prevEntry = e.prevEntry;
			entries.remove( index );
			adjustForMinLoadFactor( entries.size() - 1 );
			return true;
		}
		index = e.nextEntry;
	}

	return false;
}

template <typename H, typename V>
bool HashSet <H, V>::removeByIndex( Index index ) {
	if (entries.contains( index )) {
		Entry & e = entries.get( index );
		if (e.isFirstEntry())
			table[e.getTableIndex()] = e.nextEntry;
		else
			entries.get( e.prevEntry ).nextEntry = e.nextEntry;
		if (e.nextEntry != NO_INDEX)
			entries.get( e.nextEntry ).prevEntry = e.prevEntry;
		entries.remove( index );
		adjustForMinLoadFactor( entries.size() );
		return true;
	} else
		return false;
}

template <typename H, typename V>
V HashSet <H, V>::extractByIndex( Index index ) {
	try {
		Entry & e = entries.get( index );
		if (e.isFirstEntry())
			table[e.getTableIndex()] = e.nextEntry;
		else
			entries.get( e.prevEntry ).nextEntry = e.nextEntry;
		if (e.nextEntry != NO_INDEX)
			entries.get( e.nextEntry ).prevEntry = e.prevEntry;
		V ret = entries.extract( index ).value;
		adjustForMinLoadFactor( entries.size() );
		return ret;
	} catch (const std::exception &) {
		throw std::runtime_error( "No value exists with the specified index" );
	}
}

template <typename H, typename V>
bool HashSet <H, V>::extractByIndex( Index index, V & value ) {
	if (entries.contains( index )) {
		Entry & e = entries.get( index );
		if (e.isFirstEntry())
			table[e.getTableIndex()] = e.nextEntry;
		else
			entries.get( e.prevEntry ).nextEntry = e.nextEntry;
		if (e.nextEntry != NO_INDEX)
			entries.get( e.nextEntry ).prevEntry = e.prevEntry;
		value = entries.extract( index ).value;
		adjustForMinLoadFactor( entries.size() );
		return true;
	} else
		return false;
}

template <typename H, typename V>
bool HashSet <H, V>::contains( const V & value ) const {
	Index tableIndex = (Index)(hash( value ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		const Entry & e = entries.get( index );
		if (value == e.value)
			return true;
		index = e.nextEntry;
	}

	return false;
}

template <typename H, typename V>
bool HashSet <H, V>::containsIndex( Index index ) const {
	return entries.contains( index );
}

template <typename H, typename V>
void HashSet <H, V>::clear() {
	for (size_t i = 0; i < table.size(); ++i)
		table[i] = NO_INDEX;

	entries.clear();
}

template <typename H, typename V>
void HashSet <H, V>::resize( size_t newCapacity ) {
	if (newCapacity == 0 && table.size() > 0)
		throw std::runtime_error( "Invalid HashSet capacity " + toString( newCapacity ) );

	table.assign( newCapacity, NO_INDEX );

	// rehash
	size_t remaining = entries.size();
	Index index = 0;
	while (remaining > 0) {
		if (entries.contains( index )) {
			Entry & e = entries.get( index );
			Index tableIndex = (Index)(hash( e.value ) % table.size());

			// add to front of list the bucket
			e.setTableIndex( tableIndex );
			e.nextEntry = table[tableIndex];
			table[tableIndex] = index;
			if (e.nextEntry != NO_INDEX)
				entries.get( e.nextEntry ).prevEntry = index;
			--remaining;
		}
		++index;
	}
}

template <typename H, typename V>
size_t HashSet <H, V>::capacity() const {
	return table.size();
}

template <typename H, typename V>
size_t HashSet <H, V>::size() const {
	return entries.size();
}

template <typename H, typename V>
bool HashSet <H, V>::empty() const {
	return entries.size() == 0;
}

template <typename H, typename V> typename HashSet <H, V>::Iterator HashSet <H, V>::getIterator() {
	return Iterator( this );
}

template <typename H, typename V> typename HashSet <H, V>::ConstIterator HashSet <H, V>::getConstIterator() const {
	return ConstIterator( this );
}

template <typename H, typename V> size_t HashSet <H, V>::memory() const {
	return sizeof( table[0] ) * table.capacity() + entries.memory();
}

template <typename H, typename V> void HashSet <H, V>::adjustForMinLoadFactor( size_t newSize ) {
	size_t newTableSize = table.size();
	while ((float)newSize / (float)newTableSize < minimumLoadFactor && newTableSize > 1)
		newTableSize /= 2; // will never go to 0
	if (newTableSize < table.size())
		resize( newTableSize );
}

template <typename H, typename V> void HashSet <H, V>::adjustForMaxLoadFactor( size_t newSize ) {
	size_t newTableSize = table.size();
	while ((float)newSize / (float)newTableSize > maximumLoadFactor)
		newTableSize *= 2;
	if (newTableSize > table.size())
		resize( newTableSize );
}

#endif