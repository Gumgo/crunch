#ifndef SYSSKELETON3DANIMATIONTREE_DEFINED
#define SYSSKELETON3DANIMATIONTREE_DEFINED

#include "Common.h"
#include "Skeleton3DResource.h"
#include "Skeleton3DAnimationResource.h"
#include "Skeleton3DAnimationTreeResource.h"
#include "BinarySearch.h"
#include <vector>

// used for evaluating 3D skeletal animations
// each one corresponds to a Skeleton3DAnimationTreeResource
// only one of these should be created per character type
// i.e. all characters sharing the same skeleton and set of animations should refer to a single instance
// each separate character should have its own instance of the State subclass
class SysSkeleton3DAnimationTree {
public:
	SysSkeleton3DAnimationTree();

	bool init(
		const Skeleton3DAnimationTreeResourceReference & animTree,
		const Skeleton3DResourceReference & skel );

	bool isInitialized() const;

	const Skeleton3DAnimationTreeResourceReference & getAnimationTree() const;
	const Skeleton3DAnimationResourceReference & getAnimation( size_t i ) const;
	const Skeleton3DResourceReference & getSkeleton() const;

	// the state of a tree
	class State {
	public:
		// default construction
		State();

		const SysSkeleton3DAnimationTree * getAnimationTree() const;

		void setAnimationNodeTime( size_t node, float time );
		void setAnimationNodePlaySettings( size_t node, float playSpeed, bool loop );
		float getAnimationNodeTime( size_t node ) const;
		float getAnimationNodePlaySpeed( size_t node ) const;
		bool getAnimationNodeLoop( size_t node ) const;

		void setSelectNodeSelection( size_t node, size_t index );
		size_t getSelectNodeSelection( size_t node ) const;

		void setBlendNodeWeight( size_t node, float weight );
		void setBlendNodeTransitionSettings( size_t node, float targetWeight, float transitionSpeed );
		float getBlendNodeWeight( size_t node ) const;
		float getBlendNodeTargetWeight( size_t node ) const;
		float getBlendNodeTransitionSpeed( size_t node ) const;

		void setSequenceNodeWeight( size_t node, size_t index, float weight );
		void setSequenceNodeTransitionSettings( size_t node, size_t index, float targetWeight, float transitionSpeed );
		float getSequenceNodeWeight( size_t node, size_t index ) const;
		float getSequenceNodeTargetWeight( size_t node, size_t index ) const;
		float getSequenceNodeTransitionSpeed( size_t node, size_t index ) const;

		// evaluates the state, placing the result in poseMatrices
		void evaluate( Matrix44af * poseMatrices ) const;

		// updates the state, advancing animations and blend transitions
		void update( float dt );

	private:
		friend class SysSkeleton3DAnimationTree;
		State( const SysSkeleton3DAnimationTree & t );
		// pointer back to the tree
		const SysSkeleton3DAnimationTree * tree;

		// throws an exception if the given node is out of bounds or is not of the correct type
		void checkNode( size_t index, Skeleton3DAnimationTreeResource::Node::Type type ) const;

		// possible values a node can have
		struct Value {
			struct {
				float time;				// current animation time
				float playSpeed;		// speed at which animation is playing
				bool loop;				// whether to loop the animation
			} animationValue;
			struct {
				size_t index;			// current index of select node
			} selectValue;
			struct {
				float weight;			// current weight of blend node
				float targetWeight;		// target weight of blend node
				float transitionSpeed;	// weight transition speed
			} blendValue;
			struct {
				float weight;			// current weight of sequence node child
				float targetWeight;		// target weight of sequence node child
				float transitionSpeed;	// weight transition speed
			} sequenceValue;
		};
		// array of values for nodes
		// some nodes (sequence nodes) can have multiple values
		std::vector <Value> nodeValues;

		// bitfield representing node values which need updates applied
		// e.g. if an animation speed is 0, no update is required
		std::vector <bool> valuesRequiringUpdates;
		void setValueRequiresUpdating( size_t valueIndex, bool r );

		void updateValue( float dt, size_t nodeIndex, size_t valueIndex );
	};

	// returns a new state based on this tree
	State getState() const;

private:
	bool initialized;

	// animation tree
	Skeleton3DAnimationTreeResourceReference animationTree;

	// list of animations
	std::vector <Skeleton3DAnimationResourceReference> animations;

	// skeleton
	Skeleton3DResourceReference skeleton;

	// each state instance stores values for each node
	// however, some nodes require multiple values (sequence nodes)
	// this maps the node index to the first value index
	std::vector <size_t> nodeValueIndices;
	size_t requiredNodeValues;
	size_t getNodeValueIndex( size_t nodeIndex ) const;
	size_t getRequiredNodeValues() const;

	// each node has an "active bones" bitfield associated with it
	// mutable because skeleton evaluation is not part of logical state
	mutable std::vector <bool> activeBonesBitfields;

	// stores intermediate transformations when the skeleton is being computed
	struct Transformation {
		Vector3f translation;
		Quaternionf rotation;
		// points to the last occurrence of an active bone in the current pose
		const Transformation * lastActiveTransformation;
	};
	// buffer for storing intermediate transformations
	// mutable because skeleton evaluation is not part of logical state
	mutable std::vector <Transformation> intermediateTransformationBuffer;

	// evaluates the animation tree using the given state
	friend class State;
	void evaluate( const State & state, Matrix44af * poseMatrices ) const;
	// evaluates a node
	// buffer is the buffer to write the evaluation result into
	// bitfieldStart is the bitfield starting index to write the active bone state info into
	// currentPose is a buffer to the most recently evaluated current pose - initially currentPose is undefined
	// since sequence nodes are the only nodes that ever "fully define" a pose before moving on,
	// this means that currentPose always points to the most recent sequence node
	// blending an active bone with a non-active bone should blend with whatever is in the current pose
	void evaluate( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const;
	void evaluateAnimationNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const;
	void evaluateSelectNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const;
	void evaluateBlendNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const;
	void evaluateSequenceNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const;

	void setPoseMatrices( Matrix44af * matrices ) const;
	void setPoseMatrices( Matrix44af * matrices, size_t boneId, size_t parentId ) const;
};

#endif