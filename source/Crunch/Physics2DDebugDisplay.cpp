#include "Physics2DDebugDisplay.h"
#include "Context.h"
#include "SysRenderer2D.h"

namespace Physics2D {

DebugDisplay::DebugDisplay() {
	sim = NULL;
	camera = NULL;

	program = Context::get().resourceManager.acquire <ProgramResource>( "sys___physics2d", true );
	capsuleProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___physics2d_capsule", true );
	convexPolygonProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___physics2d_convex_polygon", true );

	std::vector <Vertex> vertices( AABB_VERTICES + CIRCLE_VERTICES + CAPSULE_VERTICES + X_VERTICES );

	vertices[0].pos[0] = 0.0f;
	vertices[0].pos[1] = 0.0f;
	vertices[1].pos[0] = 1.0f;
	vertices[1].pos[1] = 0.0f;
	vertices[2].pos[0] = 1.0f;
	vertices[2].pos[1] = 1.0f;
	vertices[3].pos[0] = 0.0f;
	vertices[3].pos[1] = 1.0f;

	size_t start = AABB_VERTICES;
	for (size_t i = 0; i < CIRCLE_VERTICES; ++i) {
		float ang = 2.0f * boost::math::constants::pi <float>() * (float)i / (float)CIRCLE_VERTICES;
		vertices[start + i].pos[0] = cos( ang );
		vertices[start + i].pos[1] = sin( ang );
	}

	start += CIRCLE_VERTICES;
	for (size_t i = 0; i <= CIRCLE_VERTICES/2; ++i) {
		float ang = 2.0f * boost::math::constants::pi <float>() * (float)i / (float)CIRCLE_VERTICES;
		vertices[start + i].pos[0] = cos( ang );
		vertices[start + i].pos[1] = sin( ang );
	}

	start += CIRCLE_VERTICES/2 + 1;
	for (size_t i = 0; i <= CIRCLE_VERTICES/2; ++i) {
		float ang = 2.0f * boost::math::constants::pi <float>() * (float)(i+CIRCLE_VERTICES/2) / (float)CIRCLE_VERTICES;
		vertices[start + i].pos[0] = cos( ang );
		vertices[start + i].pos[1] = sin( ang );
	}

	start += CIRCLE_VERTICES/2 + 1;
	vertices[start  ].pos[0] = -1.0f;
	vertices[start  ].pos[1] = -1.0f;
	vertices[start+1].pos[0] =  1.0f;
	vertices[start+1].pos[1] =  1.0f;
	vertices[start+2].pos[0] = -1.0f;
	vertices[start+2].pos[1] =  1.0f;
	vertices[start+3].pos[0] =  1.0f;
	vertices[start+3].pos[1] = -1.0f;

	GpuBuffer::BufferDescription vDesc;
	vDesc.type = GL_ARRAY_BUFFER;
	vDesc.size = vertices.size() * sizeof( Vertex );
	vDesc.data = &vertices[0];
	vDesc.usage = GL_STATIC_DRAW;
	vertexBuffer = Context::get().gpuState.createBuffer( vDesc );

	GpuBuffer::BufferDescription lvDesc;
	lvDesc.type = GL_ARRAY_BUFFER;
	lvDesc.size = 200 * sizeof( Vertex );
	lvDesc.usage = GL_DYNAMIC_DRAW;
	lineVertexBuffer = Context::get().gpuState.createBuffer( lvDesc );
	lineVertices.reserve( 200 );
}

DebugDisplay::~DebugDisplay() {
}

void DebugDisplay::setSimulation( const Simulation * s ) {
	sim = s;
}

void DebugDisplay::setCamera( const SysCamera2D * cam ) {
	camera = cam;
}

void DebugDisplay::setScale( float s ) {
	scale = s;
}

void DebugDisplay::preDraw() {
	for (const Body * b = sim->getBodies(); b != NULL; b = sim->getNextBody( b )) {
		Matrix33af tf = b->getMatrixTransform();
		for (const Shape * s = b->getShapes(); s != NULL; s = b->getNextShape( s )) {
			// if it's a line shape, build up the line vertex buffer
			if (s->getType() == Shape::T_EDGE) {
				Matrix33af shapeTf = tf * s->getTransform();
				EdgeShape * es = (EdgeShape*)s;
				Vertex v0, v1;
				Vector2f p0 = shapeTf.transformPoint( es->getPoint( 0 ) );
				Vector2f p1 = shapeTf.transformPoint( es->getPoint( 1 ) );
				v0.pos[0] = p0.x;
				v0.pos[1] = p0.y;
				v1.pos[0] = p1.x;
				v1.pos[1] = p1.y;
				lineVertices.push_back( v0 );
				lineVertices.push_back( v1 );
			}
			// also use lines for contacts
			for (const Contact * c = s->getContacts(); c != NULL; c = s->getNextContact( c )) {
				const Manifold & m = c->getManifold();
				// since each contact is "repeated" twice, we ignore it one of the times
				if (m.local != s)
					continue;
				Matrix33af shapeTf = tf * s->getTransform();
				for (uint i = 0; i < m.contactPointCount; ++i) {
					Vertex v0, v1;
					Vector2f p0 = shapeTf.transformPoint( m.contactPoints[i].point );
					Vector2f p1 = shapeTf.transformPoint( m.contactPoints[i].point + m.normal*m.contactPoints[i].depth );
					v0.pos[0] = p0.x;
					v0.pos[1] = p0.y;
					v1.pos[0] = p1.x;
					v1.pos[1] = p1.y;
					lineVertices.push_back( v0 );
					lineVertices.push_back( v1 );
				}
			}
		}
	}
	if (lineVertices.size() > 0) {
		size_t vertices = std::min( lineVertices.size(), lineVertexBuffer->getSize() / sizeof( Vertex ) );
		lineVertexBuffer->write( 0, vertices*sizeof( Vertex ), &lineVertices[0] );
	}
	lineVertices.clear();
}

void DebugDisplay::draw() {
	static const float X_SIZE = 3.0f; // size in pixels

	static const size_t CIRCLE_BUFFER_OFFSET = AABB_VERTICES * sizeof( Vertex );
	static const size_t CAPSULE_BUFFER_OFFSET = CIRCLE_BUFFER_OFFSET + CIRCLE_VERTICES * sizeof( Vertex );
	static const size_t CONVEX_POLYGON_BUFFER_OFFSET = 0; // doesn't need specific verts
	static const size_t X_BUFFER_OFFSET = CAPSULE_BUFFER_OFFSET + CAPSULE_VERTICES * sizeof( Vertex );

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_ALPHA );
	blendState.bind();

	GpuDrawCall drawCall = Context::get().gpuState.createDrawCall();

	size_t col = 0;
	size_t lineVerticesDrawn = 0;
	for (const Body * b = sim->getBodies(); b != NULL; b = sim->getNextBody( b )) {
		vertexBuffer->bind();

		static const Vector3f colors[] = {
			Vector3f( 1.0f, 0.0f, 0.0f ),
			Vector3f( 1.0f, 1.0f, 0.0f ),
			Vector3f( 0.0f, 1.0f, 0.0f ),
			Vector3f( 0.0f, 1.0f, 1.0f ),
			Vector3f( 0.0f, 0.0f, 1.0f ),
			Vector3f( 1.0f, 0.0f, 1.0f )
		};
		Matrix33af tf = b->getMatrixTransform();
		Vector4f color( colors[col % arraySize( colors )], 1.0f );
		++col;

		size_t lineVertices = 0;
		for (const Shape * s = b->getShapes(); s != NULL; s = b->getNextShape( s )) {
			// first draw the bounding box
			Aabb aabb = s->getAabb( tf );
			// transform the aabb to the right size/position
			Matrix33af aabbTf;
			aabbTf.scale( aabb.maxBound - aabb.minBound );
			aabbTf.translate( aabb.minBound );
			aabbTf.scale( scale, scale );
			Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( aabbTf );

			GpuProgram * prog = program->getProgram();
			prog->bind();
			prog->setAttribute( "pos", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( Vertex ), bufferOffset( offsetof( Vertex, pos ) ) );
			prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

			// draw the inside
			color.a = 0.25f;
			prog->setUniform( "color", GL_FLOAT_VEC4, &color );
			drawCall.setParameters( GL_TRIANGLE_FAN, 0, 4 );
			drawCall.draw();

			// draw the border
			color.a = 1.0f;
			prog->setUniform( "color", GL_FLOAT_VEC4, &color );
			drawCall.setParameters( GL_LINE_LOOP, 0, 4 );
			drawCall.draw();

			size_t verts; // number of vertices to draw
			Matrix33af shapeTf;
			switch (s->getType()) {
			case Shape::T_CAPSULE:
				prog = capsuleProgram->getProgram();
				prog->bind();
				{
					float rad = ((CapsuleShape*)s)->getRadius();
					Vector4f pts( ((CapsuleShape*)s)->getVertex( 0 ), ((CapsuleShape*)s)->getVertex( 1 ) );
					prog->setUniform( "circlePoints", GL_INT, &CIRCLE_VERTICES );
					prog->setUniform( "radius", GL_FLOAT, &rad );
					prog->setUniform( "v0v1", GL_FLOAT_VEC4, &pts );
				}

				shapeTf = tf * s->getTransform() * shapeTf;
				shapeTf.scale( scale, scale );
				mvpMatrix = camera->getModelViewProjectionMatrix( shapeTf );

				prog->setAttribute( "pos", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( Vertex ),
					bufferOffset( offsetof( Vertex, pos ) + CAPSULE_BUFFER_OFFSET ) );
				prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

				verts = CAPSULE_VERTICES;
				break;
			case Shape::T_CIRCLE:
				shapeTf.scale( ((CircleShape*)s)->getRadius(), ((CircleShape*)s)->getRadius() );
				shapeTf = tf * s->getTransform() * shapeTf;
				shapeTf.scale( scale, scale );
				mvpMatrix = camera->getModelViewProjectionMatrix( shapeTf );

				prog->setAttribute( "pos", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( Vertex ),
					bufferOffset( offsetof( Vertex, pos ) + CIRCLE_BUFFER_OFFSET ) );
				prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

				verts = CIRCLE_VERTICES;
				break;
			case Shape::T_CONVEX_POLYGON:
				prog = convexPolygonProgram->getProgram();
				prog->bind();

				{
					verts = ((ConvexPolygonShape*)s)->getVertexCount();
					Vector2f vertices[ConvexPolygonShape::MAX_VERTICES];
					for (size_t i = 0; i < verts; ++i)
						vertices[i] = (Vector2f)((ConvexPolygonShape*)s)->getVertex( i );
					prog->setUniform( "vertices", GL_FLOAT_VEC2, &vertices, 0, verts );
				}

				shapeTf = tf * s->getTransform() * shapeTf;
				shapeTf.scale( scale, scale );
				mvpMatrix = camera->getModelViewProjectionMatrix( shapeTf );

				// we DON'T need position in this case - it's handled by uniforms
				// apparently it's okay to have no attributes enabled only in core profile mode
				// fortunately that's what is being used
				//prog->setAttribute( "pos", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( Vertex ), bufferOffset( offsetof( Vertex, pos ) + CONVEX_POLYGON_BUFFER_OFFSET ) );
				prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

				break;
			case Shape::T_EDGE:
				lineVertices += 2;
				verts = 0;
				break;
			case Shape::T_ELLIPSE:
				shapeTf.scale( ((EllipseShape*)s)->getRadii() );
				shapeTf = tf * s->getTransform() * shapeTf;
				shapeTf.scale( scale, scale );
				mvpMatrix = camera->getModelViewProjectionMatrix( shapeTf );

				prog->setAttribute( "pos", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( Vertex ),
					bufferOffset( offsetof( Vertex, pos ) + CIRCLE_BUFFER_OFFSET ) );
				prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

				verts = CIRCLE_VERTICES;
				break;
			}

			if (verts > 0) {
				// draw the inside
				color.a = 0.25f;
				prog->setUniform( "color", GL_FLOAT_VEC4, &color );
				drawCall.setParameters( GL_TRIANGLE_FAN, 0, verts );
				drawCall.draw();

				// draw the border
				color.a = 1.0f;
				prog->setUniform( "color", GL_FLOAT_VEC4, &color );
				drawCall.setParameters( GL_LINE_LOOP, 0, verts );
				drawCall.draw();
			}

			for (const Contact * c = s->getContacts(); c != NULL; c = s->getNextContact( c )) {
				const Manifold & m = c->getManifold();
				// since each contact is "repeated" twice, we ignore it one of the times
				if (m.local != s)
					continue;
				lineVertices += m.contactPointCount*2;
			}
		}

		// draw an X for the center of mass
		Vector2f center = (Vector2f)b->getMassData().center;
		center = tf.transformPoint( center );
		Matrix33af xTf;
		// make the size constant
		float sc = X_SIZE / scale;
		xTf.scale( sc, sc );
		xTf.translate( center );
		xTf.scale( scale, scale );
		Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( xTf );

		GpuProgram * prog = program->getProgram();
		prog->bind();

		prog->setAttribute( "pos", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( Vertex ),
			bufferOffset( offsetof( Vertex, pos ) + X_BUFFER_OFFSET ) );
		prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

		color.a = 1.0f;
		prog->setUniform( "color", GL_FLOAT_VEC4, &color );
		drawCall.setParameters( GL_LINES, 0, 4 );
		drawCall.draw();

		if (lineVertices > 0) {
			// draw the edge shapes
			lineVertexBuffer->bind();

			prog = program->getProgram();
			prog->bind();

			Matrix33af shapeTf;
			shapeTf.scale( scale, scale );
			Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( shapeTf );

			prog->setAttribute( "pos", lineVertexBuffer, 2, GL_FLOAT, false, false, sizeof( Vertex ),
				bufferOffset( offsetof( Vertex, pos ) + lineVerticesDrawn*sizeof( Vertex ) ) );
			prog->setUniform( "mvpMatrix", GL_FLOAT_MAT4, &mvpMatrix );

			color.a = 1.0f;
			prog->setUniform( "color", GL_FLOAT_VEC4, &color );
			drawCall.setParameters( GL_LINES, 0, lineVertices );
			drawCall.draw();

			lineVerticesDrawn += lineVertices;
		}
	}
}

}