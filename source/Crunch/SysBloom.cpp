#include "SysBloom.h"
#include "Context.h"
#include "SysRenderer2D.h"
#include "Log.h"

SysBloom::SysBloom() {
	brightpassProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___bloom_brightpass", true );
	blurProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___bloom_blur", true );
	downsampleProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___bloom_downsample", true );
	blendProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___bloom_blend", true );
	ready =
		(brightpassProgram->getStatus() == Resource::S_LOADED) &&
		(blurProgram->getStatus() == Resource::S_LOADED) &&
		(downsampleProgram->getStatus() == Resource::S_LOADED) &&
		(blendProgram->getStatus() == Resource::S_LOADED);

	GpuBufferReference vertexBuffer = SysRenderer2D::get().getQuadVertexBuffer();

	brightpassAuto.setProgram( brightpassProgram->getProgram() );
	brightpassAuto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	brightpassAuto.setUniform( "source", GL_SAMPLER_2D, &brightpassUniforms.source );
	brightpassAuto.setUniform( "constantReduction", GL_FLOAT, &brightpassUniforms.constantReduction );
	brightpassAuto.setUniform( "asymptoticCutoff2", GL_FLOAT, &brightpassUniforms.asymptoticCutoff2 );
	brightpassUniforms.source = 0;

	blurAuto.setProgram( blurProgram->getProgram() );
	blurAuto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	blurAuto.setUniform( "source", GL_SAMPLER_2D, &blurUniforms.source );
	blurAuto.setUniform( "uvPixelSize", GL_FLOAT_VEC2, &blurUniforms.uvPixelSize );
	blurUniforms.source = 0;

	downsampleAuto.setProgram( downsampleProgram->getProgram() );
	downsampleAuto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	downsampleAuto.setUniform( "source", GL_SAMPLER_2D, &downsampleUniforms.source );
	downsampleAuto.setUniform( "scale", GL_FLOAT_VEC2, &downsampleUniforms.scale );
	downsampleUniforms.source = 0;

	blendAuto.setProgram( blendProgram->getProgram() );
	blendAuto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	blendAuto.setUniform( "source", GL_SAMPLER_2D, &blendUniforms.source );
	blendAuto.setUniform( "scale", GL_FLOAT_VEC2, &blendUniforms.scale );
	blendAuto.setUniform( "multiplier", GL_FLOAT, &blendUniforms.multiplier );
	blendUniforms.source = 0;

	width = height = 0;
}

SysBloom::~SysBloom() {
}

void SysBloom::setupBloom( size_t w, size_t h, size_t passes, float multiplier, float constantReduction, float asymptoticCutoff ) {
	downsamplePasses = passes;
	blendUniforms.multiplier = multiplier;
	brightpassUniforms.constantReduction = constantReduction;
	brightpassUniforms.asymptoticCutoff2 = asymptoticCutoff * asymptoticCutoff;

	if (w == 0 || h == 0) {
		Log::warning() << "Invalid tonemapping size";
		return;
	}

	if (w == width && h == height)
		return;

	width = w;
	height = h;

	downsampleBuffers.clear();

	GpuTexture::Texture2DDescription desc;
	// linear filtering for downsampling (default)
	desc.imageDescription.internalFormat = GL_RGB10_A2;
	desc.wrapS = GL_CLAMP_TO_EDGE;
	desc.wrapT = GL_CLAMP_TO_EDGE;
	GpuTexture::Data2DDescription mip;
	desc.mipmapLevels = &mip;
	mip.width = (uint)w;
	mip.height = (uint)h;

	for (size_t i = 0; i <= passes; ++i) {
		// for the first "pass" push back NULL - this is because we use a provided "temp" buffer for storing the initial brightpass
		if (i == 0)
			downsampleBuffers.push_back( GpuTextureReference() );
		else
			downsampleBuffers.push_back( Context::get().gpuState.createTexture( desc ) );
		downsampleBuffers.push_back( Context::get().gpuState.createTexture( desc ) );

		mip.width = (mip.width + 1) / 2;
		mip.height = (mip.height + 1) / 2;
	}
}

void SysBloom::downsample( GpuTextureReference & source, GpuTextureReference & tempBuffer ) {
	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( false );
	rState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( false );
	dsState.setDepthWriteEnabled( false );
	dsState.bind();

	SysRenderer2D::get().getQuadVertexBuffer()->bind();

	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->detatch( GL_DEPTH_STENCIL_ATTACHMENT );
	fb->attach( GL_COLOR_ATTACHMENT0, tempBuffer );
	fb->detatch( GL_COLOR_ATTACHMENT1 );
	fb->detatch( GL_COLOR_ATTACHMENT2 );
	fb->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	GLenum drawBuf = GL_COLOR_ATTACHMENT0;
	dbState.setDrawBuffers( 1, &drawBuf );
	dbState.bind();

	// perform brightpass

	// bind main source texture
	source->bind( (size_t)brightpassUniforms.source );

	brightpassAuto.bind();

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)source->getWidth(), (int)source->getHeight() ) );
	vpState.bind();

	SysRenderer2D::get().drawSingleQuad();

	// blur and downsample

	downsampleBuffers[0] = tempBuffer;
	for (size_t i = 0; i <= downsamplePasses; ++i) {
		// don't downsample the very first pass, just blur
		if (i > 0) {
			// downsample
			fb->attach( GL_COLOR_ATTACHMENT0, downsampleBuffers[i*2] );

			// bind texture 1 level up
			downsampleBuffers[i*2-1]->bind( (size_t)downsampleUniforms.source );
			// scale is (2 * target size) / (source size)
			// this is to sample correctly when halving an odd-sized texture
			downsampleUniforms.scale = Vector2f(
				(float)(downsampleBuffers[i*2]->getWidth()*2) / (float)downsampleBuffers[i*2-1]->getWidth(),
				(float)(downsampleBuffers[i*2]->getHeight()*2) / (float)downsampleBuffers[i*2-1]->getHeight() );

			downsampleAuto.bind();

			vpState.setViewportSize( Vector2i( (int)downsampleBuffers[i*2]->getWidth(), (int)downsampleBuffers[i*2]->getHeight() ) );
			vpState.bind();
			SysRenderer2D::get().drawSingleQuad();
		}

		// blur
		fb->attach( GL_COLOR_ATTACHMENT0, downsampleBuffers[i*2+1] );

		downsampleBuffers[i*2]->bind( (size_t)blurUniforms.source );
		blurUniforms.uvPixelSize = Vector2f(
			1.0f / (float)downsampleBuffers[i*2]->getWidth(), 1.0f / (float)downsampleBuffers[i*2]->getHeight() );

		blurAuto.bind();

		SysRenderer2D::get().drawSingleQuad();
	}

	downsampleBuffers[0] = GpuTextureReference();
}

void SysBloom::applyBloom( GpuTextureReference & source ) {
	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->attach( GL_COLOR_ATTACHMENT0, source );

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendingEnabled( true );
	blendState.setBlendFunction( GpuBlendState::BM_ADDITIVE );
	blendState.bind();

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)source->getWidth(), (int)source->getHeight() ) );
	vpState.bind();

	for (size_t i = 0; i <= downsamplePasses; ++i) {
		downsampleBuffers[i*2+1]->bind( blendUniforms.source );

		if (i == 0)
			blendUniforms.scale.set( 1.0f, 1.0f );
		else
			// we're upsampling, so the scale is the opposite as it has previously been
			blendUniforms.scale.set(
				(float)downsampleBuffers[i*2-1]->getWidth() / (float)(downsampleBuffers[i*2]->getWidth()*2),
				(float)downsampleBuffers[i*2-1]->getHeight() / (float)(downsampleBuffers[i*2]->getHeight()*2) );

		blendAuto.bind();

		SysRenderer2D::get().drawSingleQuad();
	}

	// reset blending
	Context::get().gpuState.createBlendState().bind();
}

bool SysBloom::bloom( GpuTextureReference & source, GpuTextureReference & tempBuffer ) {
	if (!ready)
		return false;
	if (!source) {
		Log::warning() << "No source texture provided for bloom";
		return false;
	}
	if (!tempBuffer) {
		Log::warning() << "No buffer texture provided for bloom";
		return false;
	}
	if (source->getWidth() != width || source->getHeight() != height ||
		tempBuffer->getWidth() != width || tempBuffer->getHeight() != height) {
		Log::warning() << "Invalid source or buffer texture size for tonemapping";
		return false;
	}

	downsample( source, tempBuffer );
	applyBloom( source );
	return true;
}