#ifndef MATHS_MATRIX44DEF_DEFINED
#define MATHS_MATRIX44DEF_DEFINED

template <typename T> Matrix44 <T>::Matrix44() {
	set( 1, 0, 0, 0,
		 0, 1, 0, 0,
		 0, 0, 1, 0,
		 0, 0, 0, 1 );
}

template <typename T> Matrix44 <T>::Matrix44( T m00, T m01, T m02, T m03,
											  T m10, T m11, T m12, T m13,
                                              T m20, T m21, T m22, T m23,
                                              T m30, T m31, T m32, T m33 ) {
	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23,
		 m30, m31, m32, m33 );
}

template <typename T> Matrix44 <T>::Matrix44( const Matrix44 <T> & m ) {
	set( m[ 0], m[ 1], m[ 2], m[ 3],
		 m[ 4], m[ 5], m[ 6], m[ 7],
		 m[ 8], m[ 9], m[10], m[11],
		 m[12], m[13], m[14], m[15] );
}

template <typename T> Matrix44 <T>::~Matrix44() {
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator+( const Matrix44 <T> & m ) const {
	return Matrix44( d[ 0] + m[ 0], d[ 1] + m[ 1], d[ 2] + m[ 2], d[ 3] + m[ 3],
					 d[ 4] + m[ 4], d[ 5] + m[ 5], d[ 6] + m[ 6], d[ 7] + m[ 7],
					 d[ 8] + m[ 8], d[ 9] + m[ 9], d[10] + m[10], d[11] + m[11],
					 d[12] + m[12], d[13] + m[13], d[14] + m[14], d[15] + m[15] );
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator+() const {
	return *this;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator-( const Matrix44 <T> & m ) const {
	return Matrix44( d[ 0] - m[ 0], d[ 1] - m[ 1], d[ 2] - m[ 2], d[ 3] - m[ 3],
					 d[ 4] - m[ 4], d[ 5] - m[ 5], d[ 6] - m[ 6], d[ 7] - m[ 7],
					 d[ 8] - m[ 8], d[ 9] - m[ 9], d[10] - m[10], d[11] - m[11],
					 d[12] - m[12], d[13] - m[13], d[14] - m[14], d[15] - m[15] );
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator-() const {
	return Matrix44( -d[ 0], -d[ 1], -d[ 2], -d[ 3],
					 -d[ 4], -d[ 5], -d[ 6], -d[ 7],
					 -d[ 8], -d[ 9], -d[10], -d[11],
					 -d[12], -d[13], -d[14], -d[15] );
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator*( T r ) const {
	return Matrix44( d[ 0]*r, d[ 1]*r, d[ 2]*r, d[ 3]*r,
					 d[ 4]*r, d[ 5]*r, d[ 6]*r, d[ 7]*r,
					 d[ 8]*r, d[ 9]*r, d[10]*r, d[11]*r,
					 d[12]*r, d[13]*r, d[14]*r, d[15]*r );
}

template <typename T> Matrix44 <T> operator*( T r, const Matrix44 <T> & m ) {
	return m*r;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator/( T r ) const {
	return Matrix44( d[ 0]/r, d[ 1]/r, d[ 2]/r, d[ 3]/r,
					 d[ 4]/r, d[ 5]/r, d[ 6]/r, d[ 7]/r,
					 d[ 8]/r, d[ 9]/r, d[10]/r, d[11]/r,
					 d[12]/r, d[13]/r, d[14]/r, d[15]/r );
}

template <typename T> Vector4 <T> Matrix44 <T>::operator*( const Vector4 <T> & v ) const {
	return Vector4 <T>( d[ 0]*v[0] + d[ 1]*v[1] + d[ 2]*v[2] + d[ 3]*v[3],
						d[ 4]*v[0] + d[ 5]*v[1] + d[ 6]*v[2] + d[ 7]*v[3],
						d[ 8]*v[0] + d[ 9]*v[1] + d[10]*v[2] + d[11]*v[3],
						d[12]*v[0] + d[13]*v[1] + d[14]*v[2] + d[15]*v[3] );
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator*( const Matrix44 <T> & m ) const {
	T m00 = d[ 0]*m[0] + d[ 1]*m[4] + d[ 2]*m[ 8] + d[ 3]*m[12];
	T m01 = d[ 0]*m[1] + d[ 1]*m[5] + d[ 2]*m[ 9] + d[ 3]*m[13];
	T m02 = d[ 0]*m[2] + d[ 1]*m[6] + d[ 2]*m[10] + d[ 3]*m[14];
	T m03 = d[ 0]*m[3] + d[ 1]*m[7] + d[ 2]*m[11] + d[ 3]*m[15];

	T m10 = d[ 4]*m[0] + d[ 5]*m[4] + d[ 6]*m[ 8] + d[ 7]*m[12];
	T m11 = d[ 4]*m[1] + d[ 5]*m[5] + d[ 6]*m[ 9] + d[ 7]*m[13];
	T m12 = d[ 4]*m[2] + d[ 5]*m[6] + d[ 6]*m[10] + d[ 7]*m[14];
	T m13 = d[ 4]*m[3] + d[ 5]*m[7] + d[ 6]*m[11] + d[ 7]*m[15];

	T m20 = d[ 8]*m[0] + d[ 9]*m[4] + d[10]*m[ 8] + d[11]*m[12];
	T m21 = d[ 8]*m[1] + d[ 9]*m[5] + d[10]*m[ 9] + d[11]*m[13];
	T m22 = d[ 8]*m[2] + d[ 9]*m[6] + d[10]*m[10] + d[11]*m[14];
	T m23 = d[ 8]*m[3] + d[ 9]*m[7] + d[10]*m[11] + d[11]*m[15];

	T m30 = d[12]*m[0] + d[13]*m[4] + d[14]*m[ 8] + d[15]*m[12];
	T m31 = d[12]*m[1] + d[13]*m[5] + d[14]*m[ 9] + d[15]*m[13];
	T m32 = d[12]*m[2] + d[13]*m[6] + d[14]*m[10] + d[15]*m[14];
	T m33 = d[12]*m[3] + d[13]*m[7] + d[14]*m[11] + d[15]*m[15];

	return Matrix44 <T>( m00, m01, m02, m03,
						 m10, m11, m12, m13,
						 m20, m21, m22, m23,
						 m30, m31, m32, m33 );
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator*( const Matrix44a <T> & m ) const {
	T m00 = d[ 0]*m[0] + d[ 1]*m[4] + d[ 2]*m[ 8];
	T m01 = d[ 0]*m[1] + d[ 1]*m[5] + d[ 2]*m[ 9];
	T m02 = d[ 0]*m[2] + d[ 1]*m[6] + d[ 2]*m[10];
	T m03 = d[ 0]*m[3] + d[ 1]*m[7] + d[ 2]*m[11] + d[ 3];

	T m10 = d[ 4]*m[0] + d[ 5]*m[4] + d[ 6]*m[ 8];
	T m11 = d[ 4]*m[1] + d[ 5]*m[5] + d[ 6]*m[ 9];
	T m12 = d[ 4]*m[2] + d[ 5]*m[6] + d[ 6]*m[10];
	T m13 = d[ 4]*m[3] + d[ 5]*m[7] + d[ 6]*m[11] + d[ 7];

	T m20 = d[ 8]*m[0] + d[ 9]*m[4] + d[10]*m[ 8];
	T m21 = d[ 8]*m[1] + d[ 9]*m[5] + d[10]*m[ 9];
	T m22 = d[ 8]*m[2] + d[ 9]*m[6] + d[10]*m[10];
	T m23 = d[ 8]*m[3] + d[ 9]*m[7] + d[10]*m[11] + d[11];

	T m30 = d[12]*m[0] + d[13]*m[4] + d[14]*m[ 8];
	T m31 = d[12]*m[1] + d[13]*m[5] + d[14]*m[ 9];
	T m32 = d[12]*m[2] + d[13]*m[6] + d[14]*m[10];
	T m33 = d[12]*m[3] + d[13]*m[7] + d[14]*m[11] + d[15];

	return Matrix44 <T>( m00, m01, m02, m03,
						 m10, m11, m12, m13,
						 m20, m21, m22, m23,
						 m30, m31, m32, m33 );
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator=( const Matrix44 <T> & m ) {
	set( m[ 0], m[ 1], m[ 2], m[ 3],
		 m[ 4], m[ 5], m[ 6], m[ 7],
		 m[ 8], m[ 9], m[10], m[11],
		 m[12], m[13], m[14], m[15] );
	return *this;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator+=( const Matrix44 <T> & m ) {
	d[ 0] += m[ 0]; d[ 1] += m[ 1]; d[ 2] += m[ 2]; d[ 3] += m[ 3];
	d[ 4] += m[ 4]; d[ 5] += m[ 5]; d[ 6] += m[ 6]; d[ 7] += m[ 7];
	d[ 8] += m[ 8]; d[ 9] += m[ 9]; d[10] += m[10]; d[11] += m[11];
	d[12] += m[12]; d[13] += m[13]; d[14] += m[14]; d[15] += m[15];
	return *this;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator-=( const Matrix44 <T> & m ) {
	d[ 0] -= m[ 0]; d[ 1] -= m[ 1]; d[ 2] -= m[ 2]; d[ 3] -= m[ 3];
	d[ 4] -= m[ 4]; d[ 5] -= m[ 5]; d[ 6] -= m[ 6]; d[ 7] -= m[ 7];
	d[ 8] -= m[ 8]; d[ 9] -= m[ 9]; d[10] -= m[10]; d[11] -= m[11];
	d[12] -= m[12]; d[13] -= m[13]; d[14] -= m[14]; d[15] -= m[15];
	return *this;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator*=( T r ) {
	d[ 0] *= r; d[ 1] *= r; d[ 2] *= r; d[ 3] *= r;
	d[ 4] *= r; d[ 5] *= r; d[ 6] *= r; d[ 7] *= r;
	d[ 8] *= r; d[ 9] *= r; d[10] *= r; d[11] *= r;
	d[12] *= r; d[13] *= r; d[14] *= r; d[15] *= r;
	return *this;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator/=( T r ) {
	d[ 0] /= r; d[ 1] /= r; d[ 2] /= r; d[ 3] /= r;
	d[ 4] /= r; d[ 5] /= r; d[ 6] /= r; d[ 7] /= r;
	d[ 8] /= r; d[ 9] /= r; d[10] /= r; d[11] /= r;
	d[12] /= r; d[13] /= r; d[14] /= r; d[15] /= r;
	return *this;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator*=( const Matrix44 <T> & m ) {
	T m00 = d[ 0]*m[0] + d[ 1]*m[4] + d[ 2]*m[ 8] + d[ 3]*m[12];
	T m01 = d[ 0]*m[1] + d[ 1]*m[5] + d[ 2]*m[ 9] + d[ 3]*m[13];
	T m02 = d[ 0]*m[2] + d[ 1]*m[6] + d[ 2]*m[10] + d[ 3]*m[14];
	T m03 = d[ 0]*m[3] + d[ 1]*m[7] + d[ 2]*m[11] + d[ 3]*m[15];

	T m10 = d[ 4]*m[0] + d[ 5]*m[4] + d[ 6]*m[ 8] + d[ 7]*m[12];
	T m11 = d[ 4]*m[1] + d[ 5]*m[5] + d[ 6]*m[ 9] + d[ 7]*m[13];
	T m12 = d[ 4]*m[2] + d[ 5]*m[6] + d[ 6]*m[10] + d[ 7]*m[14];
	T m13 = d[ 4]*m[3] + d[ 5]*m[7] + d[ 6]*m[11] + d[ 7]*m[15];

	T m20 = d[ 8]*m[0] + d[ 9]*m[4] + d[10]*m[ 8] + d[11]*m[12];
	T m21 = d[ 8]*m[1] + d[ 9]*m[5] + d[10]*m[ 9] + d[11]*m[13];
	T m22 = d[ 8]*m[2] + d[ 9]*m[6] + d[10]*m[10] + d[11]*m[14];
	T m23 = d[ 8]*m[3] + d[ 9]*m[7] + d[10]*m[11] + d[11]*m[15];

	T m30 = d[12]*m[0] + d[13]*m[4] + d[14]*m[ 8] + d[15]*m[12];
	T m31 = d[12]*m[1] + d[13]*m[5] + d[14]*m[ 9] + d[15]*m[13];
	T m32 = d[12]*m[2] + d[13]*m[6] + d[14]*m[10] + d[15]*m[14];
	T m33 = d[12]*m[3] + d[13]*m[7] + d[14]*m[11] + d[15]*m[15];

	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23,
		 m30, m31, m32, m33 );

	return *this;
}

template <typename T> Matrix44 <T> Matrix44 <T>::operator*=( const Matrix44a <T> & m ) {
	T m00 = d[ 0]*m[0] + d[ 1]*m[4] + d[ 2]*m[ 8];
	T m01 = d[ 0]*m[1] + d[ 1]*m[5] + d[ 2]*m[ 9];
	T m02 = d[ 0]*m[2] + d[ 1]*m[6] + d[ 2]*m[10];
	T m03 = d[ 0]*m[3] + d[ 1]*m[7] + d[ 2]*m[11] + d[ 3];

	T m10 = d[ 4]*m[0] + d[ 5]*m[4] + d[ 6]*m[ 8];
	T m11 = d[ 4]*m[1] + d[ 5]*m[5] + d[ 6]*m[ 9];
	T m12 = d[ 4]*m[2] + d[ 5]*m[6] + d[ 6]*m[10];
	T m13 = d[ 4]*m[3] + d[ 5]*m[7] + d[ 6]*m[11] + d[ 7];

	T m20 = d[ 8]*m[0] + d[ 9]*m[4] + d[10]*m[ 8];
	T m21 = d[ 8]*m[1] + d[ 9]*m[5] + d[10]*m[ 9];
	T m22 = d[ 8]*m[2] + d[ 9]*m[6] + d[10]*m[10];
	T m23 = d[ 8]*m[3] + d[ 9]*m[7] + d[10]*m[11] + d[11];

	T m30 = d[12]*m[0] + d[13]*m[4] + d[14]*m[ 8];
	T m31 = d[12]*m[1] + d[13]*m[5] + d[14]*m[ 9];
	T m32 = d[12]*m[2] + d[13]*m[6] + d[14]*m[10];
	T m33 = d[12]*m[3] + d[13]*m[7] + d[14]*m[11] + d[15];

	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23,
		 m30, m31, m32, m33 );

	return *this;
}

template <typename T> bool Matrix44 <T>::operator==( const Matrix44 <T> & m ) const {
	return (d[ 0] == m[ 0] && d[ 1] == m[ 1] && d[ 2] == m[ 2] && d[ 3] == m[ 3] &&
			d[ 4] == m[ 4] && d[ 5] == m[ 5] && d[ 6] == m[ 6] && d[ 7] == m[ 7] &&
			d[ 8] == m[ 8] && d[ 9] == m[ 9] && d[10] == m[10] && d[11] == m[11] &&
			d[12] == m[12] && d[13] == m[13] && d[14] == m[14] && d[15] == m[15]);
}

template <typename T> bool Matrix44 <T>::operator!=( const Matrix44 <T> & m ) const {
	return (d[ 0] != m[ 0] || d[ 1] != m[ 1] || d[ 2] != m[ 2] || d[ 3] != m[ 3] ||
			d[ 4] != m[ 4] || d[ 5] != m[ 5] || d[ 6] != m[ 6] || d[ 7] != m[ 7] ||
			d[ 8] != m[ 8] || d[ 9] != m[ 9] || d[10] != m[10] || d[11] != m[11] ||
			d[12] != m[12] || d[13] != m[13] || d[14] != m[14] || d[15] != m[15]);
}

template <typename T> template <typename U> T & Matrix44 <T>::operator[]( U i ) {
	return d[i];
}

template <typename T> template <typename U> T Matrix44 <T>::operator[]( U i ) const {
	return d[i];
}

template <typename T> template <typename U> T & Matrix44 <T>::operator()( U row, U col ) {
	return d[row*4+col];
}

template <typename T> template <typename U> T Matrix44 <T>::operator()( U row, U col ) const {
	return d[row*4+col];
}

template <typename T> template <typename U> Matrix44 <T>::operator Matrix44 <U> () const {
	return Matrix44 <U>( (U)d[ 0], (U)d[ 1], (U)d[ 2], (U)d[ 3],
						 (U)d[ 4], (U)d[ 5], (U)d[ 6], (U)d[ 7],
						 (U)d[ 8], (U)d[ 9], (U)d[10], (U)d[11],
						 (U)d[12], (U)d[13], (U)d[14], (U)d[15] );
}

template <typename T> void Matrix44 <T>::set( T m00, T m01, T m02, T m03,
											  T m10, T m11, T m12, T m13,
											  T m20, T m21, T m22, T m23,
											  T m30, T m31, T m32, T m33 ) {
	d[ 0] = m00; d[ 1] = m01; d[ 2] = m02; d[ 3] = m03;
	d[ 4] = m10; d[ 5] = m11; d[ 6] = m12; d[ 7] = m13;
	d[ 8] = m20; d[ 9] = m21; d[10] = m22; d[11] = m23;
	d[12] = m30; d[13] = m31; d[14] = m32; d[15] = m33;
}

template <typename T> void Matrix44 <T>::transpose() {
	T temp;
	temp = d[ 4]; d[ 4] = d[ 1]; d[ 1] = temp;
	temp = d[ 8]; d[ 8] = d[ 2]; d[ 2] = temp;
	temp = d[12]; d[12] = d[ 3]; d[ 3] = temp;
	temp = d[ 9]; d[ 9] = d[ 6]; d[ 6] = temp;
	temp = d[13]; d[13] = d[ 7]; d[ 7] = temp;
	temp = d[14]; d[14] = d[11]; d[11] = temp;
}

template <typename T> bool Matrix44 <T>::invert() {
	T det = determinant();
	if (det == 0) {
		set( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
		return false;
	}

	T invDet = 1/det;

	// could make this more efficient by storing repeated pairs
	T m00 = d[ 5]*d[10]*d[15] + d[ 6]*d[11]*d[13] + d[ 7]*d[ 9]*d[14] - d[ 5]*d[11]*d[14] - d[ 6]*d[ 9]*d[15] - d[ 7]*d[10]*d[13];
	T m01 = d[ 1]*d[11]*d[14] + d[ 2]*d[ 9]*d[15] + d[ 3]*d[10]*d[13] - d[ 1]*d[10]*d[15] - d[ 2]*d[11]*d[13] - d[ 3]*d[ 9]*d[14];
	T m02 = d[ 1]*d[ 6]*d[15] + d[ 2]*d[ 7]*d[13] + d[ 3]*d[ 5]*d[14] - d[ 1]*d[ 7]*d[14] - d[ 2]*d[ 5]*d[15] - d[ 3]*d[ 6]*d[13];
	T m03 = d[ 1]*d[ 7]*d[10] + d[ 2]*d[ 5]*d[11] + d[ 3]*d[ 6]*d[ 9] - d[ 1]*d[ 6]*d[11] - d[ 2]*d[ 7]*d[ 9] - d[ 3]*d[ 5]*d[10];
	T m10 = d[ 4]*d[11]*d[14] + d[ 6]*d[ 8]*d[15] + d[ 7]*d[10]*d[12] - d[ 4]*d[10]*d[15] - d[ 6]*d[11]*d[12] - d[ 7]*d[ 8]*d[14];
	T m11 = d[ 0]*d[10]*d[15] + d[ 2]*d[11]*d[12] + d[ 3]*d[ 8]*d[14] - d[ 0]*d[11]*d[14] - d[ 2]*d[ 8]*d[15] - d[ 3]*d[10]*d[12];
	T m12 = d[ 0]*d[ 7]*d[14] + d[ 2]*d[ 4]*d[15] + d[ 3]*d[ 6]*d[12] - d[ 0]*d[ 6]*d[15] - d[ 2]*d[ 7]*d[12] - d[ 3]*d[ 4]*d[14];
	T m13 = d[ 0]*d[ 6]*d[11] + d[ 2]*d[ 7]*d[ 8] + d[ 3]*d[ 4]*d[10] - d[ 0]*d[ 7]*d[10] - d[ 2]*d[ 4]*d[11] - d[ 3]*d[ 6]*d[ 8];
	T m20 = d[ 4]*d[ 9]*d[15] + d[ 5]*d[11]*d[12] + d[ 7]*d[ 8]*d[13] - d[ 4]*d[11]*d[13] - d[ 5]*d[ 8]*d[15] - d[ 7]*d[ 9]*d[12];
	T m21 = d[ 0]*d[11]*d[13] + d[ 1]*d[ 8]*d[15] + d[ 3]*d[ 9]*d[12] - d[ 0]*d[ 9]*d[15] - d[ 1]*d[11]*d[12] - d[ 3]*d[ 8]*d[13];
	T m22 = d[ 0]*d[ 5]*d[15] + d[ 1]*d[ 7]*d[12] + d[ 3]*d[ 4]*d[13] - d[ 0]*d[ 7]*d[13] - d[ 1]*d[ 4]*d[15] - d[ 3]*d[ 5]*d[12];
	T m23 = d[ 0]*d[ 7]*d[ 9] + d[ 1]*d[ 4]*d[11] + d[ 3]*d[ 5]*d[ 8] - d[ 0]*d[ 5]*d[11] - d[ 1]*d[ 7]*d[ 8] - d[ 3]*d[ 4]*d[ 9];
	T m30 = d[ 4]*d[10]*d[13] + d[ 5]*d[ 8]*d[14] + d[ 6]*d[ 9]*d[12] - d[ 4]*d[ 9]*d[14] - d[ 5]*d[10]*d[12] - d[ 6]*d[ 8]*d[13];
	T m31 = d[ 0]*d[ 9]*d[14] + d[ 1]*d[10]*d[12] + d[ 2]*d[ 8]*d[13] - d[ 0]*d[10]*d[13] - d[ 1]*d[ 8]*d[14] - d[ 2]*d[ 9]*d[12];
	T m32 = d[ 0]*d[ 6]*d[13] + d[ 1]*d[ 4]*d[14] + d[ 2]*d[ 5]*d[12] - d[ 0]*d[ 5]*d[14] - d[ 1]*d[ 6]*d[12] - d[ 2]*d[ 4]*d[13];
	T m33 = d[ 0]*d[ 5]*d[10] + d[ 1]*d[ 6]*d[ 8] + d[ 2]*d[ 4]*d[ 9] - d[ 0]*d[ 6]*d[ 9] - d[ 1]*d[ 4]*d[10] - d[ 2]*d[ 5]*d[ 8];

	set( m00, m01, m02, m03,
		 m10, m11, m12, m13,
		 m20, m21, m22, m23,
		 m30, m31, m32, m33 );
	return true;
}

template <typename T> void Matrix44 <T>::zero() {
	set( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
}

template <typename T> void Matrix44 <T>::identity() {
	set( 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 );
}

template <typename T> Matrix44 <T> Matrix44 <T>::transposed() const {
	return Matrix44 <T>( d[0], d[4], d[ 8], d[12],
						 d[1], d[5], d[ 9], d[13],
						 d[2], d[6], d[10], d[14],
						 d[3], d[7], d[11], d[15] );
}

template <typename T> Matrix44 <T> Matrix44 <T>::inverse() const {
	T det = determinant();
	if (det == 0)
		return Matrix44 <T>( 0, 0, 0, 0, 0, 0, 0, 0, 0 );

	T invDet = 1/det;

	// could make this more efficient by storing repeated pairs
	T m00 = d[ 5]*d[10]*d[15] + d[ 6]*d[11]*d[13] + d[ 7]*d[ 9]*d[14] - d[ 5]*d[11]*d[14] - d[ 6]*d[ 9]*d[15] - d[ 7]*d[10]*d[13];
	T m01 = d[ 1]*d[11]*d[14] + d[ 2]*d[ 9]*d[15] + d[ 3]*d[10]*d[13] - d[ 1]*d[10]*d[15] - d[ 2]*d[11]*d[13] - d[ 3]*d[ 9]*d[14];
	T m02 = d[ 1]*d[ 6]*d[15] + d[ 2]*d[ 7]*d[13] + d[ 3]*d[ 5]*d[14] - d[ 1]*d[ 7]*d[14] - d[ 2]*d[ 5]*d[15] - d[ 3]*d[ 6]*d[13];
	T m03 = d[ 1]*d[ 7]*d[10] + d[ 2]*d[ 5]*d[11] + d[ 3]*d[ 6]*d[ 9] - d[ 1]*d[ 6]*d[11] - d[ 2]*d[ 7]*d[ 9] - d[ 3]*d[ 5]*d[10];
	T m10 = d[ 4]*d[11]*d[14] + d[ 6]*d[ 8]*d[15] + d[ 7]*d[10]*d[12] - d[ 4]*d[10]*d[15] - d[ 6]*d[11]*d[12] - d[ 7]*d[ 8]*d[14];
	T m11 = d[ 0]*d[10]*d[15] + d[ 2]*d[11]*d[12] + d[ 3]*d[ 8]*d[14] - d[ 0]*d[11]*d[14] - d[ 2]*d[ 8]*d[15] - d[ 3]*d[10]*d[12];
	T m12 = d[ 0]*d[ 7]*d[14] + d[ 2]*d[ 4]*d[15] + d[ 3]*d[ 6]*d[12] - d[ 0]*d[ 6]*d[15] - d[ 2]*d[ 7]*d[12] - d[ 3]*d[ 4]*d[14];
	T m13 = d[ 0]*d[ 6]*d[11] + d[ 2]*d[ 7]*d[ 8] + d[ 3]*d[ 4]*d[10] - d[ 0]*d[ 7]*d[10] - d[ 2]*d[ 4]*d[11] - d[ 3]*d[ 6]*d[ 8];
	T m20 = d[ 4]*d[ 9]*d[15] + d[ 5]*d[11]*d[12] + d[ 7]*d[ 8]*d[13] - d[ 4]*d[11]*d[13] - d[ 5]*d[ 8]*d[15] - d[ 7]*d[ 9]*d[12];
	T m21 = d[ 0]*d[11]*d[13] + d[ 1]*d[ 8]*d[15] + d[ 3]*d[ 9]*d[12] - d[ 0]*d[ 9]*d[15] - d[ 1]*d[11]*d[12] - d[ 3]*d[ 8]*d[13];
	T m22 = d[ 0]*d[ 5]*d[15] + d[ 1]*d[ 7]*d[12] + d[ 3]*d[ 4]*d[13] - d[ 0]*d[ 7]*d[13] - d[ 1]*d[ 4]*d[15] - d[ 3]*d[ 5]*d[12];
	T m23 = d[ 0]*d[ 7]*d[ 9] + d[ 1]*d[ 4]*d[11] + d[ 3]*d[ 5]*d[ 8] - d[ 0]*d[ 5]*d[11] - d[ 1]*d[ 7]*d[ 8] - d[ 3]*d[ 4]*d[ 9];
	T m30 = d[ 4]*d[10]*d[13] + d[ 5]*d[ 8]*d[14] + d[ 6]*d[ 9]*d[12] - d[ 4]*d[ 9]*d[14] - d[ 5]*d[10]*d[12] - d[ 6]*d[ 8]*d[13];
	T m31 = d[ 0]*d[ 9]*d[14] + d[ 1]*d[10]*d[12] + d[ 2]*d[ 8]*d[13] - d[ 0]*d[10]*d[13] - d[ 1]*d[ 8]*d[14] - d[ 2]*d[ 9]*d[12];
	T m32 = d[ 0]*d[ 6]*d[13] + d[ 1]*d[ 4]*d[14] + d[ 2]*d[ 5]*d[12] - d[ 0]*d[ 5]*d[14] - d[ 1]*d[ 6]*d[12] - d[ 2]*d[ 4]*d[13];
	T m33 = d[ 0]*d[ 5]*d[10] + d[ 1]*d[ 6]*d[ 8] + d[ 2]*d[ 4]*d[ 9] - d[ 0]*d[ 6]*d[ 9] - d[ 1]*d[ 4]*d[10] - d[ 2]*d[ 5]*d[ 8];

	return Matrix44 <T>( m00, m01, m02, m03,
						 m10, m11, m12, m13,
						 m20, m21, m22, m23,
						 m30, m31, m32, m33 );
}

template <typename T> T Matrix44 <T>::determinant() const {
	T det22_33 = d[10]*d[15] - d[14]*d[11];
	T det21_33 = d[ 9]*d[15] - d[13]*d[11];
	T det21_32 = d[ 9]*d[14] - d[13]*d[10];
	T det20_33 = d[ 8]*d[15] - d[12]*d[11];
	T det20_32 = d[ 8]*d[14] - d[12]*d[10];
	T det20_31 = d[ 8]*d[13] - d[12]*d[ 9];

	T det11_22_33 = d[5]*det22_33 - d[6]*det21_33 + d[7]*det21_32;
	T det10_22_33 = d[4]*det22_33 - d[6]*det20_33 + d[7]*det20_32;
	T det10_21_33 = d[4]*det21_33 - d[5]*det20_33 + d[7]*det20_31;
	T det10_21_32 = d[4]*det21_32 - d[5]*det20_32 + d[6]*det20_31;

	return d[0]*det11_22_33 - d[1]*det10_22_33 + d[2]*det10_21_33 - d[3]*det10_21_32;
}

template <typename T> T Matrix44 <T>::trace() const {
	return d[0] + d[5] + d[10] + d[15];
}

template <typename T> const T * Matrix44 <T>::getArray() const {
	return d;
}

#endif