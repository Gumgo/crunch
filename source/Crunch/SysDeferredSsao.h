#ifndef SYSDEFERREDSSAO_DEFINED
#define SYSDEFERREDSSAO_DEFINED

#include "Common.h"
#include "GpuTexture.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"
#include "SysCamera3D.h"
#include <vector>
#include <random>

// applies SSAO
class SysDeferredSsao : private boost::noncopyable {
public:
	SysDeferredSsao();
	~SysDeferredSsao();

	void setCamera( SysCamera3D * cam );

	void setupSsao( float radius );

	bool ssao();

private:
	ProgramResourceReference computeSsaoProgram;
	bool ready;

	SysAutoProgram computeSsaoAuto;
	struct {
		Vector2f imagePlaneHalfSize;
		int normalSpecMap;
		int depthMap;
		int rotationMap;
		Vector2f sizeOverRotationMapSize;
		float worldRadius;
		Matrix44f projectionMatrix;
	} computeSsaoUniforms;

	SysCamera3D * camera;

	// texture for rotating the kernel
	GpuTextureReference rotationTexture;

	void computeSsao();
};

#endif