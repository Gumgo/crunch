/** @file Skeleton3DAnimationStateMachineResource.h
 *  @brief Contains the resource holding 3D skeleton animation tree data.
 */

#ifndef SKELETON3DANIMATIONSTATEMACHINERESOURCE_DEFINED
#define SKELETON3DANIMATIONSTATEMACHINERESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "HashMap.h"
#include "HashFunctions.h"
#include <vector>
#include <queue>

/** @brief Skeleton 3D animation tree resource.
 */
DEFINE_RESOURCE_CLASS( Skeleton3DAnimationStateMachineResource, "skeleton3DAnimationStateMachine", "skeleton3DAnimationStateMachines" )
public:
	/** @brief The constructor.
	 */
	Skeleton3DAnimationStateMachineResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief Type of a bindable variable.
	 */
	enum VariableType {
		VT_FLOAT,	/**< A float variable.*/
		VT_UINT		/**< An unsigned int variable.*/
	};

	/** @brief Returns the number of variables.
	 */
	size_t getVariableCount() const;

	/** @brief Returns the type of the variable with the given index.
	 */
	VariableType getVariableType( size_t i ) const;

	/** @brief Returns the index of the variable with the given name.
	 */
	bool getVariableIndex( const std::string & name, size_t & index ) const;

	/** @brief Returns the name of the variable with the given index.
	 */
	std::string getVariableName( size_t index ) const;

	/** @brief A value, which can either be a constant or bound to a variable.
	 */
	template <typename T> class Value {
	public:
		/** @brief The type of value.
		 */
		enum Type {
			T_CONSTANT,	/**< This value is a constant.*/
			T_VARIABLE	/**< This value is a variable.*/
		};

		Type getType() const;					/**< Returns the type of value.*/
		T getConstantValue() const;				/**< Returns the constant value (undefined if not a constant).*/
		size_t getVariableIndex() const;		/**< Returns the variable index (undefined if not a variable).*/

		void setConstantValue( T val );			/**< Sets the constant value.*/
		void setVariableIndex( size_t idx );	/**< Sets the variable index.*/

	private:
		Type type;	/**< The type of value.*/
		union {
			T constantValue;		// value of the constant, if a constant
			size_t variableIndex;	// index of the variable, if a variable
		};
	};

	typedef Value <float> FloatValue;	/**< A float value.*/
	typedef Value <uint> UintValue;		/**< A uint value.*/

	/** @brief A command to play an animation node.
	 */
	class AnimationPlayCommand {
	public:
		bool getLoop() const;					/**< Returns whether the animation should loop.*/
		const FloatValue & getSpeed() const;	/**< Returns the speed multiplier at which the animation should play.*/

		void init( bool l, const FloatValue & s );	/**< Initializes the command.*/

	private:
		bool loop;			// whether the animation should loop
		FloatValue speed;	// speed at which the animation should play
	};

	/** @brief A command to set the time on an animation node.
	 */
	class AnimationSetTimeCommand {
	public:
		const FloatValue & getTime() const;	/**< Returns the time to set the animation to.*/

		void init( const FloatValue & t );	/**< Initializes the command.*/

	private:
		FloatValue time;	// time to set the animation to
	};

	/** @brief A command to set the index on a select node.
	 */
	class SelectSetIndexCommand {
	public:
		const UintValue & getIndex() const;	/**< Returns the index the select node should be set to.*/

		void init( const UintValue & i );	/**< Initializes the command.*/

	private:
		UintValue index;	// index the select node should be set to
	};

	/** @brief A command to transition the weight on a blend node.
	 */
	class BlendTransitionWeightCommand {
	public:
		const FloatValue & getWeight() const;	/**< Returns the weight the blend node should transition to.*/
		const FloatValue & getSpeed() const;	/**< Returns the speed in units per second at which the transition should occur.*/

		void init( const FloatValue & w, const FloatValue & s );	/**< Initializes the command.*/

	private:
		FloatValue weight;	// weight the blend node should transition to
		FloatValue speed;	// speed in units per second at which the transition should occur
	};

	/** @brief A command to set the weight on a blend node.
	 */
	class BlendSetWeightCommand {
	public:
		const FloatValue & getWeight() const;	/**< Returns the weight the blend node should be set to.*/

		void init( const FloatValue & w );	/**< Initializes the command.*/

	private:
		FloatValue weight;	// weight the blend node should be set to
	};

	/** @brief A command to transition the weight on a sequence node.
	 */
	class SequenceTransitionWeightCommand {
	public:
		uint getIndex() const;					/**< Returns the index of the child this command applies to.*/
		const FloatValue & getWeight() const;	/**< Returns the weight the sequence node should transition to.*/
		const FloatValue & getSpeed() const;	/**< Returns the speed in units per second at which the transition should occur.*/

		void init( uint i, const FloatValue & w, const FloatValue & s );	/**< Initializes the command.*/

	private:
		uint index;			// index of the child this command applies to
		FloatValue weight;	// weight the blend node should transition to
		FloatValue speed;	// speed in units per second at which the transition should occur
	};

	/** @brief A command to set the weight on a sequence node.
	 */
	class SequenceSetWeightCommand {
	public:
		uint getIndex() const;					/**< Returns the index of the child this command applies to.*/
		const FloatValue & getWeight() const;	/**< Returns the weight the blend node should be set to.*/

		void init( uint i, const FloatValue & w );	/**< Initializes the command.*/

	private:
		uint index;			// index of the child this command applies to
		FloatValue weight;	// weight the blend node should be set to
	};

	/** @brief A command to activate or deactivate a state.
	 */
	class StateSetActiveCommand {
	public:
		size_t getStateIndex() const;	/**< Returns the index of the state to activated or deactivated.*/
		bool getActive() const;			/**< Returns whether the state should be activated or deactivated.*/

		void init( size_t i, bool a );	/**< Initializes the command.*/

	private:
		size_t stateIndex;	// index of the state to modify
		bool active;		// whether to activate or deactivate the state
	};

	/** @brief A command to trigger an event.
	 */
	class EventTriggerCommand {
	public:
		size_t getEventIndex() const;	/**< Returns the index of the event to trigger.*/

		void init( size_t i );	/**< Initializes the command.*/

	private:
		size_t eventIndex;	// index of the event to trigger
	};

	/** @brief A command to be executed.
	 */
	class Command {
	public:
		/** @brief The command type.
		 */
		enum Type {
			T_ANIMATION_PLAY,				/**< Plays an animation node.*/
			T_ANIMATION_SET_TIME,			/**< Sets the time on an animation node.*/
			T_SELECT_SET_INDEX,				/**< Sets the index on a select node.*/
			T_BLEND_TRANSITION_WEIGHT,		/**< Transitions the weight on a blend node.*/
			T_BLEND_SET_WEIGHT,				/**< Sets the weight on a blend node.*/
			T_SEQUENCE_TRANSITION_WEIGHT,	/**< Transitions the weight on a sequence node.*/
			T_SEQUENCE_SET_WEIGHT,			/**< Sets the weight on a sequence node.*/
			T_STATE_SET_ACTIVE,				/**< Sets whether a state is activated or deactivated.*/
			T_EVENT_TRIGGER,				/**< Triggers an event.*/
			TYPE_COUNT						/**< Number of command types.*/
		};

		/** @brief Returns the name of the node this command should apply to.
		 */
		const std::string & getNodeName() const;

		/** @brief Returns the command type.
		 */
		Type getType() const;

		/** @brief Returns the contained animation play command, or throws an exception if this command is of another type.
		 */
		const AnimationPlayCommand & getAnimationPlayCommand() const;

		/** @brief Returns the contained animation set time command, or throws an exception if this command is of another type.
		 */
		const AnimationSetTimeCommand & getAnimationSetTimeCommand() const;

		/** @brief Returns the contained select set index command, or throws an exception if this command is of another type.
		 */
		const SelectSetIndexCommand & getSelectSetIndexCommand() const;

		/** @brief Returns the contained blend transition weight command, or throws an exception if this command is of another type.
		 */
		const BlendTransitionWeightCommand & getBlendTransitionWeightCommand() const;

		/** @brief Returns the contained blend set weight command, or throws an exception if this command is of another type.
		 */
		const BlendSetWeightCommand & getBlendSetWeightCommand() const;

		/** @brief Returns the contained sequence transition weight command, or throws an exception if this command is of another type.
		 */
		const SequenceTransitionWeightCommand & getSequenceTransitionWeightCommand() const;

		/** @brief Returns the contained sequence set weight command, or throws an exception if this command is of another type.
		 */
		const SequenceSetWeightCommand & getSequenceSetWeightCommand() const;

		/** @brief Returns the contained state set active command, or throws an exception if this command is of another type.
		 */
		const StateSetActiveCommand & getStateSetActiveCommand() const;

		/** @brief Returns the contained event trigger command, or throws an exception if this command is of another type.
		 */
		const EventTriggerCommand & getEventTriggerCommand() const;

	private:
		friend class Skeleton3DAnimationStateMachineResource;
		// constructors for each type
		Command( const std::string & nName, const AnimationPlayCommand & c );
		Command( const std::string & nName, const AnimationSetTimeCommand & c );
		Command( const std::string & nName, const SelectSetIndexCommand & c );
		Command( const std::string & nName, const BlendTransitionWeightCommand & c );
		Command( const std::string & nName, const BlendSetWeightCommand & c );
		Command( const std::string & nName, const SequenceTransitionWeightCommand & c );
		Command( const std::string & nName, const SequenceSetWeightCommand & c );
		Command( const StateSetActiveCommand & c );
		Command( const EventTriggerCommand & c );

		// name of node this command applies to
		std::string nodeName;

		// command type
		Type type;

		// command data for each possible command
		union {
			AnimationPlayCommand animationPlayCommand;
			AnimationSetTimeCommand animationSetTimeCommand;
			SelectSetIndexCommand selectSetIndexCommand;
			BlendTransitionWeightCommand blendTransitionWeightCommand;
			BlendSetWeightCommand blendSetWeightCommand;
			SequenceTransitionWeightCommand sequenceTransitionWeightCommand;
			SequenceSetWeightCommand sequenceSetWeightCommand;
			StateSetActiveCommand stateSetActiveCommand;
			EventTriggerCommand eventTriggerCommand;
		};
	};

	/** @brief Returns the total number of commands.
	 */
	size_t getCommandCount() const;

	/** @brief Returns the command with the given index.
	 */
	const Command & getCommand( size_t i ) const;

	/** @brief A state in the state machine.
	 */
	class State {
	public:
		/** @brief Returns the index of the first command.
		 */
		size_t getFirstCommandIndex() const;

		/** @brief Returns the number of commands in this state.
		 */
		size_t getCommandCount() const;

	private:
		friend class Skeleton3DAnimationStateMachineResource;
		State( size_t first, size_t count );

		size_t firstCommandIndex;
		size_t commandCount;
	};

	/** @brief Returns the number of states.
	 */
	size_t getStateCount() const;

	/** @brief Returns the state with the given index.
	 */
	const State & getState( size_t i ) const;

	/** @brief Returns the index of the state with the given name.
	 */
	bool getStateIndex( const std::string & name, size_t & index ) const;

	/** @brief Returns the name of the state with the given index.
	 */
	std::string getStateName( size_t index ) const;

	/** @brief An event in the state machine.
	 */
	class Event {
	public:
		/** @brief Returns the index of the first command.
		 */
		size_t getFirstCommandIndex() const;

		/** @brief Returns the number of commands in this state.
		 */
		size_t getCommandCount() const;

	private:
		friend class Skeleton3DAnimationStateMachineResource;
		Event( size_t first, size_t count );

		size_t firstCommandIndex;
		size_t commandCount;
	};

	/** @brief Returns the number of events.
	 */
	size_t getEventCount() const;

	/** @brief Returns the event with the given index.
	 */
	const Event & getEvent( size_t i ) const;

	/** @brief Returns the index of the event with the given name.
	 */
	bool getEventIndex( const std::string & name, size_t & index ) const;

	/** @brief Returns the name of the event with the given index.
	 */
	std::string getEventName( size_t index ) const;

private:
	// utility functions to read either constant or variable values
	FloatValue getFloatValue( const KeyValueReader::Iterator & it, const std::string & name ) const;
	UintValue getUintValue( const KeyValueReader::Iterator & it, const std::string & name ) const;
	// adds a command to the list (or possibly multiple commands if multiple node names were specified)
	void addCommand( const KeyValueReader::Iterator & it );
	Command::Type getCommandType( const KeyValueReader::Iterator & commandIt );

	std::vector <VariableType> variableTypes;
	HashMap <Hash <std::string>, std::string, size_t> variableNameMap;
	std::vector <size_t> variableNameIndices;

	std::vector <State> states;
	HashMap <Hash <std::string>, std::string, size_t> stateNameMap;
	std::vector <size_t> stateNameIndices;

	std::vector <Event> events;
	HashMap <Hash <std::string>, std::string, size_t> eventNameMap;
	std::vector <size_t> eventNameIndices;

	std::vector <Command> commands;
};

template <typename T>
typename Skeleton3DAnimationStateMachineResource::Value <T>::Type Skeleton3DAnimationStateMachineResource::Value <T>::getType() const {
	return type;
}

template <typename T>
T Skeleton3DAnimationStateMachineResource::Value <T>::getConstantValue() const {
	return constantValue;
}

template <typename T>
size_t Skeleton3DAnimationStateMachineResource::Value <T>::getVariableIndex() const {
	return variableIndex;
}

template <typename T>
void Skeleton3DAnimationStateMachineResource::Value <T>::setConstantValue( T val ) {
	type = T_CONSTANT;
	constantValue = val;
}

template <typename T>
void Skeleton3DAnimationStateMachineResource::Value <T>::setVariableIndex( size_t idx ) {
	type = T_VARIABLE;
	variableIndex = idx;
}

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <Skeleton3DAnimationStateMachineResource> Skeleton3DAnimationStateMachineResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <Skeleton3DAnimationStateMachineResource> Skeleton3DAnimationStateMachineResourceConstReference;

#endif