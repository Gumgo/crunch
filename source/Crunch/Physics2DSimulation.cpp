#include "Physics2DSimulation.h"

namespace Physics2D {

Simulation::Simulation( Real binDimension )
	: spatialHashMap( binDimension )
	, contactManager( spatialHashMap ) {
	locked = false;

	bodies = NULL;
	bodyCount = 0;
}

Simulation::~Simulation() {
	// remove everything
	while (bodies != NULL)
		destroyBody( bodies );
}

void Simulation::update( TimeUnit dt ) {
	TimeStep timeStep( dt );

	// find all new contacts
	// TODO: only do this for new objects? perhaps not
	contactManager.findNewContacts();

	// update contacts, destroy ones that no longer exist
	contactManager.updateCurrentContacts();

	solve( timeStep );
	// integrate velocities, solve velocity constraints, integrate position
	{
		// integrate velocities
		// apply velocity damping

		// solve velocity constraints

		// integrate positions

		// solve position constraints
	}
}

bool Simulation::isLocked() const {
	return locked;
}

void Simulation::throwIfLocked() {
	if (isLocked())
		throw std::runtime_error( "Simulation is locked" );
}

Body * Simulation::createBody( const BodyDefinition & def ) {
	throwIfLocked();

	// create body, put in front of list
	Body * body = new Body( def, this );
	body->getSimulationData().next = bodies;
	if (bodies != NULL)
		bodies->getSimulationData().prev = body;
	body->getSimulationData().next = bodies;
	body->getSimulationData().prev = NULL;
	bodies = body;
	++bodyCount;

	return body;
}

void Simulation::destroyBody( Body * b ) {
	throwIfLocked();
	if (b->getSimulation() != this)
		throw std::runtime_error( "Attempted to destroy a body from the wrong simulation" );

	// TODO delete the joints

	// remove shapes, which also removes contacts (the removeShape() function does this)
	while (b->getShapes() != NULL)
		b->removeShape( b->getShapes() );

	if (b->getSimulationData().prev == NULL)
		bodies = b->getSimulationData().next;
	else
		b->getSimulationData().prev->getSimulationData().next = b->getSimulationData().next;
	if (b->getSimulationData().next != NULL)
		b->getSimulationData().next->getSimulationData().prev = b->getSimulationData().prev;
	
	--bodyCount;
	safeDelete( b );
}

Body * Simulation::getBodies() {
	return bodies;
}

const Body * Simulation::getBodies() const {
	return bodies;
}

Body * Simulation::getNextBody( Body * b ) {
	return b->getSimulationData().next;
}

const Body * Simulation::getNextBody( const Body * b ) const {
	return b->getSimulationData().next;
}

size_t Simulation::getBodyCount() const {
	return bodyCount;
}

void Simulation::solve( const TimeStep & timeStep ) {
	Body * b = bodies;
	while (b != NULL) {
		Vector2r linearVelocity = b->linearVelocity;
		Real angularVelocity = b->angularVelocity;

		if (b->getType() == Body::T_DYNAMIC) {
			// integrate velocity using implicit euler
			linearVelocity += timeStep.dt * b->massData.invMass * b->force;
			angularVelocity += timeStep.dt * b->massData.invInertia * b->torque;

			// todo: velocity damping?
		}

		b->getSimulationData().intermediateLinearVelocity = linearVelocity;
		b->getSimulationData().intermediateAngularVelocity = angularVelocity;
		b->getSimulationData().intermediateWorldCenterOfMass = b->getSimulationData().worldCenterOfMass;
		b->getSimulationData().intermediateRotation = b->rotation;

		b = b->getSimulationData().next;
	}

	// TODO: solve velocity constraints

	b = bodies;
	while (b != NULL) {
		Vector2r worldCenterOfMass = b->getSimulationData().intermediateWorldCenterOfMass;
		Real rotation = b->getSimulationData().intermediateRotation;
		Vector2r linearVelocity = b->getSimulationData().intermediateLinearVelocity;
		Real angularVelocity = b->getSimulationData().intermediateAngularVelocity;

		// todo: large translation and rotation clamping?

		worldCenterOfMass += timeStep.dt * linearVelocity;
		rotation += timeStep.dt * angularVelocity;

		b->getSimulationData().intermediateWorldCenterOfMass = worldCenterOfMass;
		b->getSimulationData().intermediateRotation = rotation;
		// todo: if velocities were clamped:
		//b->getSimulationData().intermediateLinearVelocity = linearVelocity;
		//b->getSimulationData().intermediateAngularVelocity = angularVelocity;

		b = b->getSimulationData().next;
	}

	// TODO: solve position constraints

	// assign the new values back to the bodies
	b = bodies;
	while (b != NULL) {
		// we need to compute position from world center of mass
		b->getSimulationData().worldCenterOfMass = b->getSimulationData().intermediateWorldCenterOfMass;

		// position = worldCenterOfMass - matrix(rotation)*localCenterOfMass
		b->position = b->getSimulationData().worldCenterOfMass
			- Matrix22 <Real>().rotated( b->getSimulationData().intermediateRotation ) * b->getMassData().center;
		b->rotation = b->getSimulationData().intermediateRotation;
		b->linearVelocity = b->getSimulationData().intermediateLinearVelocity;
		b->angularVelocity = b->getSimulationData().intermediateAngularVelocity;

		// update the shapes in the spatial hash map
		// TODO: only do this if body moved
		Matrix33ar tf = b->getMatrixTransform();
		Shape * s = b->getShapes();
		while (s != NULL) {
			Aabb aabb = s->getAabb( tf );
			aabb.expand( Shape::AABB_EXPANSION );
			spatialHashMap.moveShape( s->getBodyData().spatialHashIndex, aabb );
			s = b->getNextShape( s );
		}

		b = b->getSimulationData().next;
	}
}

}