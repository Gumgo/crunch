#include "TaskQueue.h"
#include "Log.h"

TaskQueue::TaskQueue( size_t threadCount ) {
	// spawn threads
	threads.resize( threadCount );
	for (size_t i = 0; i < threads.size(); ++i)
		threads[i] = boost::thread( boost::bind( &TaskQueue::taskThreadMain, this ) );
	Log::info() << "Started task queue";
}

TaskQueue::~TaskQueue() {
	flushTasks();

	{
		// lock the pending task data structure, push terminate signals for each thread
		PendingTask terminationSignal;
		terminationSignal.type = PendingTask::TT_TERMINATE;

		boost::mutex::scoped_lock lock( taskMutex );
		for (size_t i = 0; i < threads.size(); ++i)
			pendingTasks.push_back( terminationSignal );
	}

	// alert each thread
	for (size_t i = 0; i < threads.size(); ++i)
		tasksPending.signal();

	// wait for threads to terminate
	for (size_t i = 0; i < threads.size(); ++i)
		threads[i].join();

	Log::info() << "Stopped task queue";
}

void TaskQueue::taskThreadMain() {
	bool quit = false;

	while (!quit) {
		// wait until there are tasks pending
		tasksPending.wait();

		// grab the mutex to access the task list
		taskMutex.lock();
		if (pendingTasks.empty())
			taskMutex.unlock();
		else {
			PendingTask pendingTask = pendingTasks.front();
			pendingTasks.pop_front();
			if (pendingTask.type == PendingTask::TT_NORMAL)
				boost::interprocess::ipcdetail::atomic_write32( &tasks.get( pendingTask.id ).status, TS_EXECUTING );
			taskMutex.unlock();

			switch (pendingTask.type) {
			case PendingTask::TT_NORMAL:
				// execute the task
				pendingTask.function( pendingTask.data );
				{
					boost::mutex::scoped_lock lock( taskMutex );
					boost::interprocess::ipcdetail::atomic_write32( &tasks.get( pendingTask.id ).status, TS_COMPLETE );
				}
				break;
			case PendingTask::TT_TERMINATE:
				// terminate this thread
				quit = true;
				break;
			case PendingTask::TT_FLUSH:
				{
					size_t newFlushedCount;
					{
						// decrement the number of threads still needing to be flushed
						boost::mutex::scoped_lock lock( taskMutex );
						--flushedCount;
						newFlushedCount = flushedCount;
					}

					// if this is the last thread that needed flushing, alert the main thread that we're done
					if (newFlushedCount == 0)
						mainFlushSemaphore.signal();

					// wait until the main thread detects that all threads have been flushed
					taskFlushSemaphore.wait();
				}
				break;
			}
		}
	}
}

TaskQueue::TaskId TaskQueue::addTask( TaskFunction function, void * data ) {
	Task task;
	task.status = TS_PENDING;

	PendingTask pendingTask;
	pendingTask.type = PendingTask::TT_NORMAL;
	pendingTask.function = function;
	pendingTask.data = data;

	{
		boost::mutex::scoped_lock lock( taskMutex );
		pendingTask.id = tasks.add( task );
		pendingTasks.push_back( pendingTask );
		tasks.get( pendingTask.id ).pendingTask = --pendingTasks.end();
	}

	tasksPending.signal();

	return pendingTask.id;
}

bool TaskQueue::cancelTask( TaskId id ) {
	if (!tasks.contains( id ))
		return false;

	// need to lock the data structure in this case
	{
		boost::mutex::scoped_lock lock( taskMutex );
		TaskStatus ts = (TaskStatus)boost::interprocess::ipcdetail::atomic_read32( &tasks.get( id ).status );
		if (ts != TS_PENDING)
			return false;
		pendingTasks.erase( tasks.get( id ).pendingTask ); 
		tasks.remove( id );
		return true;
	}
}

TaskQueue::TaskStatus TaskQueue::getTaskStatus( TaskId id, bool releaseTaskId ) {
	if (!tasks.contains( id ))
		return TS_INVALID_TASK;
	// use atomics so we don't need to lock the data structure for just checking status
	TaskStatus ts = (TaskStatus)boost::interprocess::ipcdetail::atomic_read32( &tasks.get( id ).status );
	if (ts == TS_COMPLETE && releaseTaskId) {
		boost::mutex::scoped_lock lock( taskMutex );
		tasks.remove( id );
	}
	return ts;
}

void TaskQueue::flushTasks() {
	PendingTask flushSignal;
	flushSignal.type = PendingTask::TT_FLUSH;

	// initially no threads have been flushed
	{
		// use the lock to ensure proper memory barriers, etc.
		// push the flush signals for each thread
		boost::mutex::scoped_lock lock( taskMutex );
		flushedCount = threads.size();
		for (size_t i = 0; i < threads.size(); ++i)
			pendingTasks.push_back( flushSignal );
	}

	// alert each thread
	for (size_t i = 0; i < threads.size(); ++i)
		tasksPending.signal();

	// wait until the flush semaphore is alerted
	mainFlushSemaphore.wait();

	// now allow all the task threads to resume
	for (size_t i = 0; i < threads.size(); ++i)
		taskFlushSemaphore.signal();
}