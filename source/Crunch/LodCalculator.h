/** @file LodCalculator.h
 *  @brief Helps determine appropriate LODs.
 */

#ifndef LODCALCULATOR_DEFINED
#define LODCALCULATOR_DEFINED

#include "Common.h"

/** @brief Helps determine appropriate LODs.
 */
class LodCalculator {
public:
	LodCalculator();	/**< The constructor.*/
	void setValues( float verticalFov, int screenHeightPixels );	/**< Sets values for making calculations.*/

	/** @brief Returns the projected length in pixels of a curve vertical to the image plane at the given distance along the z-axis.
	 */
	float projectedLength( float length, float distance );

	/** @brief Returns the projected area in square pixels of a region vertical to the image plane at the given distance along the z-axis.
	 */
	float projectedArea( float area, float distance );

private:
	// the screen height over the image plane height
	float screenHeightOverImagePlaneHeight;
};

#endif