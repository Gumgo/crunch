#include "GpuAttribute.h"
#include "GpuState.h"

GpuAttribute::GpuAttribute( GpuState * gpuState, size_t i )
	: ReferenceCountedGpuObject( gpuState ) {
	index = i;

	GLint attributeEnabledResult;
	glGetVertexAttribiv( (uint)index, GL_VERTEX_ATTRIB_ARRAY_ENABLED, &attributeEnabledResult );
	attributeEnabled = attributeEnabledResult != 0;

	glGetVertexAttribiv( (uint)index, GL_VERTEX_ATTRIB_ARRAY_SIZE, (GLint*)&attributeSize );

	glGetVertexAttribiv( (uint)index, GL_VERTEX_ATTRIB_ARRAY_TYPE, (GLint*)&attributeType );

	GLint attributeNormalizedResult;
	glGetVertexAttribiv( (uint)index, GL_VERTEX_ATTRIB_ARRAY_NORMALIZED, &attributeNormalizedResult );
	attributeNormalized = attributeNormalizedResult != 0;

	attributeInteger = false;

	glGetVertexAttribiv( (uint)index, GL_VERTEX_ATTRIB_ARRAY_STRIDE, (GLint*)&attributeStride );

	GLvoid * attributePointerResult;
	glGetVertexAttribPointerv( (uint)index, GL_VERTEX_ATTRIB_ARRAY_POINTER, &attributePointerResult );
	attributePointer = attributePointerResult;
}

GpuAttribute::~GpuAttribute() {
	// don't need to do anything
}

void GpuAttribute::setEnabled( bool e ) {
	if (e && !attributeEnabled) {
		glEnableVertexAttribArray( (uint)index );
		attributeEnabled = true;
	} else if (!e && attributeEnabled) {
		glDisableVertexAttribArray( (uint)index );
		attributeEnabled = false;
	}
}

bool GpuAttribute::isEnabled() const {
	return attributeEnabled;
}

bool GpuAttribute::setPointer( uint size, GLenum type, bool normalized, bool integer, uint stride, const void * pointer ) {
	GpuBufferReference buffer = getGpuState()->getBoundBuffer( GL_ARRAY_BUFFER );
	if (!buffer)
		// can't set pointer - no buffer bound
		return false;
	setPointer( buffer, size, type, normalized, integer, stride, pointer );
	return true;
}

bool GpuAttribute::setPointer( const GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer, uint stride, const void * pointer ) {
	if (attributeBuffer.get() == buffer.get() &&
		attributeSize == size &&
		attributeType == type &&
		attributeInteger == integer &&
		(attributeInteger || attributeNormalized == normalized) && // only compare normalization if NOT an integer
		attributeStride == stride &&
		attributePointer == pointer)
		// already set
		return true;

	if (integer) {
		if (type != GL_BYTE && type != GL_UNSIGNED_BYTE &&
			type != GL_SHORT && type != GL_UNSIGNED_SHORT &&
			type != GL_INT && type != GL_UNSIGNED_INT)
			// not a valid type for IPointer
			return false;
		getGpuState()->bindBuffer( GL_ARRAY_BUFFER, buffer );
		glVertexAttribIPointer( (uint)index, (int)size, type, stride, pointer );
	} else {
		getGpuState()->bindBuffer( GL_ARRAY_BUFFER, buffer );
		glVertexAttribPointer( (uint)index, (int)size, type, normalized, stride, pointer );
	}
	attributeBuffer = buffer;
	attributeSize = size;
	attributeType = type;
	attributeNormalized = normalized;
	attributeInteger = integer;
	attributeStride = stride;
	attributePointer = pointer;
	return true;
}

void GpuAttribute::clearPointer() {
	if (!attributeBuffer)
		// already unset
		return;

	// keep same pointer, but "forget" the buffer
	attributeBuffer = GpuBufferReference();
}

void GpuAttribute::onRelease() const {
	delete this;
}