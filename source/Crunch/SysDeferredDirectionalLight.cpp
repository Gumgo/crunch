#include "SysDeferredDirectionalLight.h"
#include "Context.h"
#include "SysRenderer2D.h"
#include "SysRendererDeferredLighting.h"

const float SysDeferredDirectionalLight::DEFAULT_MAX_SLOPE_BIAS_ANGLE =
	boost::math::constants::pi <float>() * (7.0f / 16.0f);

SysDeferredDirectionalLight::SysDeferredDirectionalLight()
	: SysDeferredLight( LT_DIRECTIONAL ) {
	setupMatrices( &lightMatrix, false, projectionMatrices, true );

	program = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_directional", true );
	shadowProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_directional_shadow", true );

	autoProgram.setProgram( program->getProgram() );
	autoProgram.setAttribute( "corner", SysRenderer2D::get().getQuadVertexBuffer(), 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	autoProgram.setUniform( "imagePlaneHalfSize", GL_FLOAT_VEC2, &uniforms.imagePlaneHalfSize );
	autoProgram.setUniform( "normalSpecMap", GL_SAMPLER_2D, &uniforms.normalSpecMap );
	autoProgram.setUniform( "depthMap", GL_SAMPLER_2D, &uniforms.depthMap );
	autoProgram.setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
	autoProgram.setUniform( "viewSpaceDirection", GL_FLOAT_VEC3, &uniforms.viewSpaceDirection );
	uniforms.color.set( 1.0f, 1.0f, 1.0f );
	uniforms.normalSpecMap = 0;
	uniforms.depthMap = 2;

	setDirection( Vector3f( 0.0f, 0.0f, -1.0f ) );
}

SysDeferredDirectionalLight::~SysDeferredDirectionalLight() {
}

Matrix44af SysDeferredDirectionalLight::computeLightMatrix() const {
	// we need to select some arbitrary vector to be our "up" vector
	// set it to be the basis vector of the smallest component of direction
	Vector3f u;
	Vector3f a( std::abs( direction.x ), std::abs( direction.y ), std::abs( direction.z ) );
	if (a.x < a.y && a.x < a.z)
		u.x = 1.0f;
	else if (a.y < a.z)
		u.y = 1.0f;
	else
		u.z = 1.0f;
	// want direction to be in front of us
	Vector3f b = -direction;
	Vector3f r = u.cross( b );
	r.normalize();
	u = b.cross( r ); // no need to normalize
	// for better precision, the light "position" is relative to the camera center
	Vector3f p( camera->getCameraMatrix()[3], camera->getCameraMatrix()[7], camera->getCameraMatrix()[11] );
	return Matrix44af(
		r.x, r.y, r.z, -r.dot( p ),
		u.x, u.y, u.z, -u.dot( p ),
		b.x, b.y, b.z, -b.dot( p ) );
}

void SysDeferredDirectionalLight::computeSlices() {
	slicePositions[0] = 0.0f;
	// we want each slice to have twice the resolution as the last
	float invTotalResolution = 1.0f / (float)((1 << shadowMapSlices) - 1);
	for (size_t i = 1; i < shadowMapSlices; ++i) {
		float resolutionFraction = (float)((1 << i) - 1) * invTotalResolution;
		slicePositions[i] = resolutionFraction;
	}
	slicePositions[shadowMapSlices] = 1.0f;
}

void SysDeferredDirectionalLight::enableShadow( size_t size, size_t slices, float constantBias, float maxSlopeBiasAngle ) {
	shadowsEnabled = true;
	shadowMapSlices = clamp( slices, (size_t)1, MAX_SLICES );
	computeSlices();

	maxSlopeBiasAngleTan = tan( maxSlopeBiasAngle );

	GpuTexture::Texture2DArrayDescription desc;
	desc.imageDescription.internalFormat = GL_DEPTH_COMPONENT;
	desc.imageDescription.minFilter = GL_NEAREST;
	desc.imageDescription.magFilter = GL_NEAREST;
	desc.wrapS = GL_CLAMP_TO_EDGE;
	desc.wrapT = GL_CLAMP_TO_EDGE;
	GpuTexture::Data3DDescription mip;
	desc.mipmapLevels = &mip;
	mip.width = (uint)size;
	mip.height = (uint)size;
	mip.depth = (uint)shadowMapSlices;
	mip.format = GL_DEPTH_COMPONENT;
	mip.type = GL_FLOAT;
	shadowMap = Context::get().gpuState.createTexture( desc );

	autoProgram.setProgram( shadowProgram->getProgram() );
	autoProgram.setAttribute( "corner", SysRenderer2D::get().getQuadVertexBuffer(), 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	autoProgram.setUniform( "imagePlaneHalfSize", GL_FLOAT_VEC2, &uniforms.imagePlaneHalfSize );
	autoProgram.setUniform( "normalSpecMap", GL_SAMPLER_2D, &uniforms.normalSpecMap );
	autoProgram.setUniform( "depthMap", GL_SAMPLER_2D, &uniforms.depthMap );
	autoProgram.setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
	autoProgram.setUniform( "viewSpaceDirection", GL_FLOAT_VEC3, &uniforms.viewSpaceDirection );

	autoProgram.setUniform( "faceNormalMap", GL_SAMPLER_2D, &uniforms.faceNormalMap );
	autoProgram.setUniform( "shadowMap", GL_SAMPLER_2D_ARRAY, &uniforms.shadowMap );
	autoProgram.setUniform( "viewToLightMatrix", GL_FLOAT_MAT3x4, &uniforms.viewToLightMatrix );
	autoProgram.setUniform( "halfResolution", GL_FLOAT, &uniforms.halfResolution );
	autoProgram.setUniform( "constantBias", GL_FLOAT, &uniforms.constantBias );
	autoProgram.setUniform( "maxSlopeBias", GL_FLOAT, &uniforms.maxSlopeBias, 0, MAX_SLICES );
	autoProgram.setUniform( "slices", GL_INT, &uniforms.slices );
	autoProgram.setUniform( "zSplits", GL_FLOAT, uniforms.zSplits, 0, MAX_SLICES-1 );
	autoProgram.setUniform( "clipSpaceHalfSize", GL_FLOAT_VEC2, uniforms.clipSpaceHalfSize, 0, MAX_SLICES );
	autoProgram.setUniform( "clipSpaceCenter", GL_FLOAT_VEC2, uniforms.clipSpaceCenter, 0, MAX_SLICES );
	autoProgram.setUniform( "lightNearFar", GL_FLOAT_VEC2, uniforms.lightNearFar, 0, MAX_SLICES );

	uniforms.faceNormalMap = 1;
	uniforms.shadowMap = 4;
	uniforms.halfResolution = 0.5f * (float)size;
	uniforms.constantBias = constantBias;
	uniforms.slices = shadowMapSlices;
	// set the near/far uniforms only when computing shadow z range
}

void SysDeferredDirectionalLight::setColor( const Vector3f & c ) {
	uniforms.color = c;
}

Vector3f SysDeferredDirectionalLight::getColor() const {
	return uniforms.color;
}

void SysDeferredDirectionalLight::setDirection( const Vector3f & d ) {
	direction = d.normalized();
}

Vector3f SysDeferredDirectionalLight::getDirection() const {
	return direction;
}

void SysDeferredDirectionalLight::computeShadowMapRange( fastdelegate::FastDelegate0 <const SysAabb*> nextAabb ) {
	lightMatrix = computeLightMatrix();

	const SysFrustum & camFrustum = camera->getFrustum();
	// get the frustum corners
	// we do this by solving intersection of 3 planes
	// n_i . p = -d_i
	// [ a1 b1 c1 ][ x ]   [ -d1 ]
	// [ a2 b2 c2 ][ y ] = [ -d2 ]
	// [ a3 b3 c3 ][ z ]   [ -d3 ]
	// write this as Mp = -d
	// the solution is p = M^-1 * -d
	// first 4 corners are on the near plane, last 4 are on the far plane
	static const SysFrustum::FrustumPlane intersectionPlanes[8][3] = {
		{ SysFrustum::FP_NEAR, SysFrustum::FP_LEFT,  SysFrustum::FP_BOTTOM },
		{ SysFrustum::FP_NEAR, SysFrustum::FP_LEFT,  SysFrustum::FP_TOP },
		{ SysFrustum::FP_NEAR, SysFrustum::FP_RIGHT, SysFrustum::FP_BOTTOM },
		{ SysFrustum::FP_NEAR, SysFrustum::FP_RIGHT, SysFrustum::FP_TOP },
		{ SysFrustum::FP_FAR,  SysFrustum::FP_LEFT,  SysFrustum::FP_BOTTOM },
		{ SysFrustum::FP_FAR,  SysFrustum::FP_LEFT,  SysFrustum::FP_TOP },
		{ SysFrustum::FP_FAR,  SysFrustum::FP_RIGHT, SysFrustum::FP_BOTTOM },
		{ SysFrustum::FP_FAR,  SysFrustum::FP_RIGHT, SysFrustum::FP_TOP }
	};
	Vector3f frustumCorners[8];
	for (size_t i = 0; i < 8; ++i) {
		const Planef * planes[3] = {
			&camFrustum.getPlanes()[intersectionPlanes[i][0]],
			&camFrustum.getPlanes()[intersectionPlanes[i][2]],
			&camFrustum.getPlanes()[intersectionPlanes[i][1]]
		};
		Matrix33f system(
			planes[0]->normal.x, planes[0]->normal.y, planes[0]->normal.z,
			planes[1]->normal.x, planes[1]->normal.y, planes[1]->normal.z,
			planes[2]->normal.x, planes[2]->normal.y, planes[2]->normal.z );
		Vector3f dVec( -planes[0]->d, -planes[1]->d, -planes[2]->d );
		frustumCorners[i] = system.inverse() * dVec;
	}

	// construct light vectors for use in constructing frustum
	Vector3f lightRightVec( lightMatrix[0], lightMatrix[1], lightMatrix[2] );
	Vector3f lightUpVec( lightMatrix[4], lightMatrix[5], lightMatrix[6] );
	Vector3f lightBackVec( lightMatrix[8], lightMatrix[9], lightMatrix[10] );
	Vector3f lightOrigin( camera->getCameraMatrix()[3], camera->getCameraMatrix()[7], camera->getCameraMatrix()[11] );
	Matrix44af invLightMatrix(
		lightRightVec.x, lightUpVec.x, lightBackVec.x, lightOrigin.x,
		lightRightVec.y, lightUpVec.y, lightBackVec.y, lightOrigin.y,
		lightRightVec.z, lightUpVec.z, lightBackVec.z, lightOrigin.z );
	for (size_t i = 0; i < shadowMapSlices; ++i) {
		// now we have the corners of the view frustum
		// we can interpolate between near and far to find corners of each slice
		// we use this to find the bounding volume of the frustum slice in light space
		SysAabb lightSpaceAabb = SysAabb::EMPTY;
		for (size_t t = 0; t < 4; ++t) {
			Vector3f nearCorner = lerp( frustumCorners[t], frustumCorners[t+4], slicePositions[i] );
			Vector3f farCorner = lerp( frustumCorners[t], frustumCorners[t+4], slicePositions[i+1] );
			nearCorner = lightMatrix.transformPoint( nearCorner );
			farCorner = lightMatrix.transformPoint( farCorner );
			lightSpaceAabb.minBound = vecMin( lightSpaceAabb.minBound, nearCorner );
			lightSpaceAabb.maxBound = vecMax( lightSpaceAabb.maxBound, nearCorner );
			lightSpaceAabb.minBound = vecMin( lightSpaceAabb.minBound, farCorner );
			lightSpaceAabb.maxBound = vecMax( lightSpaceAabb.maxBound, farCorner );
		}
		// we now have the light space bounding box
		// next we determine the z range for the shadow map slice
		// we build a frustum to test objects against
		// our side and far planes correspond to the AABB
		// however, our near plane should be initially at -infinity
		// this is because distant objects can still cast shadows into the camera frustum
		// to do this, we just need to transform 2 of the AABB points back into world space
		Vector3f aabbMinWorld = invLightMatrix.transformPoint( lightSpaceAabb.minBound );
		Vector3f aabbMaxWorld = invLightMatrix.transformPoint( lightSpaceAabb.maxBound );
		SysFrustum sliceFrustum;
		sliceFrustum.getPlanes()[SysFrustum::FP_LEFT] =
			Planef( lightRightVec, aabbMinWorld );
		sliceFrustum.getPlanes()[SysFrustum::FP_RIGHT] =
			Planef( -lightRightVec, aabbMaxWorld );
		sliceFrustum.getPlanes()[SysFrustum::FP_BOTTOM] =
			Planef( lightUpVec, aabbMinWorld );
		sliceFrustum.getPlanes()[SysFrustum::FP_TOP] =
			Planef( -lightUpVec, aabbMaxWorld );
		sliceFrustum.getPlanes()[SysFrustum::FP_FAR] =
			Planef( lightBackVec, aabbMinWorld );
		// for the near plane, we solve for d to be as far back as possible:
		// n = -back
		// p = back * MAX
		// n.p + d = 0
		// -back.back*MAX = -d
		// d = MAX
		sliceFrustum.getPlanes()[SysFrustum::FP_NEAR] =
			Planef( -lightBackVec, std::numeric_limits <float>::max() );

		Vector2f zRange( std::numeric_limits <float>::max(), -std::numeric_limits <float>::max() );
		// now we have a frustum for testing objects
		const SysAabb * aabb;
		while ((aabb = nextAabb()) != NULL) {
			if (!sliceFrustum.contains( *aabb ))
				continue;
			const Vector3f * bounds[2] = { &aabb->minBound, &aabb->maxBound };
			// find the points on the AABB that are least and most in the light's direction
			bool inDir[3] = {
				direction.x > 0.0f,
				direction.y > 0.0f,
				direction.z > 0.0f
			};
			Vector3f most(
				bounds[inDir[0]]->x,
				bounds[inDir[1]]->y,
				bounds[inDir[2]]->z );
			Vector3f least(
				bounds[!inDir[0]]->x,
				bounds[!inDir[1]]->y,
				bounds[!inDir[2]]->z );
			// dot with direction to project z onto light vector
			zRange[0] = std::min( zRange[0], (least - lightOrigin).dot( direction ) );
			zRange[1] = std::max( zRange[1], (most - lightOrigin).dot( direction ) );
		}

		float n, f;
		// make sure there were actually objects
		if (zRange[0] > zRange[1]) {
			// no objects found; just set the z range to the AABB
			// min and max are "reversed" since -z is forward
			n = -lightSpaceAabb.minBound.z;
			f = -lightSpaceAabb.maxBound.z;
		} else {
			n = zRange[0];
			f = std::max( zRange[1], -lightSpaceAabb.maxBound.z );
		}

		// set up the projection matrix
		float l = lightSpaceAabb.minBound.x;
		float r = lightSpaceAabb.maxBound.x;
		float b = lightSpaceAabb.minBound.y;
		float t = lightSpaceAabb.maxBound.y;
		projectionMatrices[i].set(
			2.0f / (r - l),	0.0f,			0.0f,			(r + l) / (l - r),
			0.0f,			2.0f / (t - b),	0.0f,			(t + b) / (b - t),
			0.0f,			0.0f,			2.0f / (n - f),	(f + n) / (n - f),
			0.0f,			0.0f,			0.0f,			1.0f );
		uniforms.clipSpaceHalfSize[i] = (lightSpaceAabb.maxBound.xy() - lightSpaceAabb.minBound.xy()) * 0.5f;
		uniforms.clipSpaceCenter[i] = (lightSpaceAabb.minBound.xy() + lightSpaceAabb.maxBound.xy()) * 0.5f;
		uniforms.lightNearFar[i].set( n, f );

		// set the max slope bias for each slice - tan(angle) * (distance from pixel corner to center)
		Vector2f pixelHalfSize = 0.5f * uniforms.clipSpaceHalfSize[i] / uniforms.halfResolution;
		uniforms.maxSlopeBias[i] = maxSlopeBiasAngleTan * pixelHalfSize.magnitude();
	}
}

void SysDeferredDirectionalLight::drawShadowMap(
	fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
	fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject ) {
	if (shadowMap.get() == NULL)
		return;

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)shadowMap.get()->getWidth(), (int)shadowMap.get()->getHeight() ) );
	vpState.bind();

	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->detatch( GL_COLOR_ATTACHMENT0 );
	fb->detatch( GL_COLOR_ATTACHMENT1 );
	fb->detatch( GL_COLOR_ATTACHMENT2 );
	fb->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	dbState.setDrawBuffers( 0, NULL );
	dbState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthWriteEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.bind();

	GpuClearCall clearCall = Context::get().gpuState.createClearCall();
	clearCall.setClearDepth( 1.0f );
	clearCall.setClearBuffers( GL_DEPTH_BUFFER_BIT );

	for (size_t i = 0; i < shadowMapSlices; ++i) {
		fb->attachLayer( GL_DEPTH_ATTACHMENT, shadowMap, i );
		clearCall.clear();

		ShadowMapRenderer r;
		const SysAabb * aabb;
		while (r = nextObject(), (aabb = nextAabb()) != NULL)
			// TODO: cull per-slice eventually
			r( this, i );
	}
}

void SysDeferredDirectionalLight::draw() {
	if (camera == NULL)
		return;

	if (shadowMap.get() != NULL) {
		if (shadowProgram->getStatus() != Resource::S_LOADED)
			return;
		shadowMap->bind( (size_t)uniforms.shadowMap );
		uniforms.viewToLightMatrix = lightMatrix * camera->getCameraMatrix();
		for (size_t i = 0; i < shadowMapSlices-1; ++i)
			uniforms.zSplits[i] = -lerp( camera->getNear(), camera->getFar(), slicePositions[i+1] );
	} else {
		if (program->getStatus() != Resource::S_LOADED)
			return;
	}

	GpuRasterizerState rState = Context::get().gpuState.getCurrentRasterizerState();
	rState.setCullingEnabled( false );
	rState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.getCurrentDepthStencilState();
	dsState.setDepthTestEnabled( false );
	dsState.bind();

	SysRenderer2D::get().getQuadVertexBuffer()->bind();

	// renderer has already bound textures

	uniforms.imagePlaneHalfSize.y = tan( degToRad( camera->getFov() ) * 0.5f );
	uniforms.imagePlaneHalfSize.x = uniforms.imagePlaneHalfSize.y * camera->getAspectRatio();
	uniforms.viewSpaceDirection =  camera->getViewMatrix().transformVector( direction );
	autoProgram.bind();

	SysRenderer2D::get().drawSingleQuad();
}