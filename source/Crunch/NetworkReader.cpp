#if 0

#include "NetworkReader.h"

NetworkReader::NetworkReader( size_t connectionId, const byte * d, size_t size )
	: senderId( connectionId )
	, data( d )
	, dataSize( size )
	, position( 0 ) {
}

const byte * NetworkReader::inc( size_t bytes ) {
	if (position + bytes > dataSize)
		throw std::runtime_error( "Attempted to read too much data from the network" );
	const byte * ret = data + position;
	position += bytes;
	return ret;
}

size_t NetworkReader::getSenderId() const {
	return senderId;
}

char NetworkReader::readChar() {
	return *(char*)inc( 1 );
}

byte NetworkReader::readByte() {
	return *(byte*)inc( 1 );
}

short NetworkReader::readShort() {
	return (short)ntohs( *(ushort*)inc( 2 ) );
}

ushort NetworkReader::readUshort() {
	return ntohs( *(ushort*)inc( 2 ) );
}

int NetworkReader::readInt() {
	return (int)ntohl( *(uint*)inc( 4 ) );
}

uint NetworkReader::readUint() {
	return ntohl( *(uint*)inc( 4 ) );
}

float NetworkReader::readFloat() {
	return *(float*)inc( 4 );
}

double NetworkReader::readDouble() {
	return *(float*)inc( 8 );
}

std::string NetworkReader::readString() {
	ushort size = readUshort();
	std::string ret;
	ret.assign( (char*)inc( (size_t)size ), (size_t)size );
	return ret;
}

void NetworkReader::readData( void * dest, size_t bytes ) {
	memcpy( dest, inc( bytes ), bytes );
}

#endif