/** @file Physics2DEllipseShape.h
 *  @brief Defines ellipse shapes.
 */
#ifndef PHYSICS2DELLIPSESHAPE_DEFINED
#define PHYSICS2DELLIPSESHAPE_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DShape.h"

namespace Physics2D {

class Body;
struct EllipseShapeDefinition;

/** @brief An ellipse shape.
 */
class EllipseShape : public Shape {
public:
	EllipseShape( const EllipseShapeDefinition & d, Body * par );	/**< The constructor.*/

	/** @brief Returns the bounding box of the transformed shape.
	 */
	Aabb getAabb( const Matrix33ar & transformation ) const;

	/** @brief Computes the mass data of the shape.
	 */
	void computeMassData();

	void setRadii( const Vector2r & rad );	/**< Sets the radii.*/
	Vector2r getRadii() const;				/**< Returns the radii.*/

private:
	Vector2r radii;	// the radii

	Real computeArea() const;
};

/** @brief Defines ellipse shapes.
 */
struct EllipseShapeDefinition : public ShapeDefinition {
	Vector2r radii;	/**< The radii.*/

	EllipseShapeDefinition();					/**< Assigns default values.*/
	Shape * createShape( Body * parent ) const;	/**< Creates an ellipse shape from the definition.*/
};

}

#endif