#if 0

/** @file PhysicsObjects.h
 *  @brief Defines physics objects.
 */

#ifndef PHYSICSOBJECTS_DEFINED
#define PHYSICSOBJECTS_DEFINED

#include "Common.h"

/** @brief A line segment.
 */
struct LineSegment {
	Vector2f points[2];	/**< The points bounding this line segment.*/
	Linef line;			/**< The line defined by this line segment.*/
	LineSegment( const Vector2f * pts );						/**< Initializes a line segment.*/
	LineSegment( const Vector2f & pt0, const Vector2f & pt1 );	/**< Initializes a line segment.*/
};

/** @brief An axis-aligned box.
 */
struct Box {
	Vector2f bounds[2];	/**< The two points defining the box.*/
};

/** @brief A circle.
 */
struct Circle {
	Vector2f position;	/**< The circle's position.*/
	float radius;		/**< The circle's radius.*/
};

/** @brief A capsule.
 */
struct Capsule {
	Vector2f position;	/**< The position of the center of the capsule.*/
	float radius;		/**< The radius of the capsule.*/
	float height;		/**< The height of the line segment portion of the capsule.*/
};

#endif

#endif