#ifndef SYSDEFERREDPOINTLIGHT_DEFINED
#define SYSDEFERREDPOINTLIGHT_DEFINED

#include "Common.h"
#include "SysDeferredLight.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"
#include "GpuTexture.h"
#include "GeoSphere.h"

class SysDeferredPointLight : public SysDeferredLight {
public:
	SysDeferredPointLight();
	~SysDeferredPointLight();

	void enableShadow(
		size_t size,
		float minNear = 0.1f, float maxFar = std::numeric_limits <float>::max(),
		float constantBias = 0.1f, float maxMultiplierBias = 0.1f );

	void setColor( const Vector3f & c );
	Vector3f getColor() const;
	void setPosition( const Vector3f & p );
	Vector3f getPosition() const;
	void setAttenuation( AttenuationMode mode, const Vector3f & params );
	AttenuationMode getAttenuationMode() const;
	Vector3f getAttenuationParameters() const;

	void computeShadowMapRange(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb );
	void drawShadowMap(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
		fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject );

	void draw();
	void drawGeometry();

private:
	ProgramResourceReference sphereProgram;
	ProgramResourceReference sphereShadowProgram;
	ProgramResourceReference quadProgram;
	ProgramResourceReference quadShadowProgram;
	ProgramResourceReference geomProgram;

	GpuTextureReference shadowMap;
	float shadowMinNear, shadowMaxFar;
	static const float FAR_RANGE_MULTIPLIER;

	AttenuationMode attenuationMode;
	Matrix44f projectionMatrix;

	SysAutoProgram autoSphereProgram;
	SysAutoProgram autoQuadProgram;
	SysAutoProgram autoGeomProgram;
	struct {
		Matrix44af modelViewMatrix;
		Matrix44f projectionMatrix;
		Vector2f screenSize;
		float radius;
		Vector2f imagePlaneHalfSize;
		int normalSpecMap;
		int depthMap;
		Vector3f color;
		int quadraticAttenuation;
		Vector3f attenuation;
		Vector3f viewSpacePosition;

		int faceNormalMap;
		int shadowMap;
		Matrix44af viewToLightMatrix;
		float halfResolution;
		float constantBias;
		float maxMultiplierBias;
		Matrix44af faceMatrices[6];
		Vector2f lightNearFar;
	} uniforms;

	Vector3f position;
	float range;

	Matrix44af computeFaceMatrix( size_t i ) const;
	Matrix44f computeProjectionMatrix() const;
};

#endif