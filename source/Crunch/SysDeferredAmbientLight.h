#ifndef SYSDEFERREDAMBIENTLIGHT_DEFINED
#define SYSDEFERREDAMBIENTLIGHT_DEFINED

#include "Common.h"
#include "SysDeferredLight.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"

class SysDeferredAmbientLight : public SysDeferredLight {
public:
	SysDeferredAmbientLight();
	~SysDeferredAmbientLight();

	void setColor( const Vector3f & c );
	Vector3f getColor() const;

	void setAmbientOcclusionStrength( float s );
	float getAmbientOcclusionStrength() const;

	void computeShadowMapRange(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb );
	void drawShadowMap(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
		fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject );

	void draw();

private:
	ProgramResourceReference program;

	SysAutoProgram autoProgram;
	struct {
		int ambientOcclusionMap;
		Vector3f color;
		int applyAmbientOcclusion;
		float ambientOcclusionStrength;
	} uniforms;
};

#endif