/** @file InputEvent.h
 *  @brief Represents an input event.
 */

#ifndef INPUTEVENT_DEFINED
#define INPUTEVENT_DEFINED

#include "Common.h"

/** @brief Represents the type of input event.
 */
enum InputEventType {
	IET_WINDOW,			/**< A window-related event.*/

	IET_CHARACTER,		/**< A character event.*/
	IET_KEY,			/**< A key event.*/

	IET_MOUSE_BUTTON,	/**< A mouse button event.*/
	IET_MOUSE_WHEEL,	/**< A mouse wheel event.*/
	IET_MOUSE_POSITION,	/**< A mouse position event.*/
	IET_MOUSE_RAW		/**< A raw mouse event.*/
};

/** @brief Represents the type of window event.
 */
enum WindowEventType {
	WET_CLOSE,			/**< The close button was pressed.*/
	WET_GAINED_FOCUS,	/**< The window gained focus.*/
	WET_LOST_FOCUS		/**< The window lost focus.*/
};

/** @brief Data associated with a window event.
 */
struct WindowEvent {
	WindowEventType type;	/**< The event type.*/
	bool operator==( const WindowEvent & o ) const;	/**< Compares two window events for equality.*/
};

/** @brief Data associated with a character event.
 */
struct CharacterEvent {
	int characterCode;	/**< The character that was typed.*/
	bool operator==( const CharacterEvent & o ) const;	/**< Compares two character events for equality.*/
};

/** @brief Represents the type of key event.
 */
enum KeyEventType {
	KET_PRESSED,	/**< The key was pressed.*/
	KET_RELEASED	/**< The key was released.*/
};

/** @brief Data associated with a key event.
 */
struct KeyEvent {
	KeyEventType type;	/**< The event type.*/
	int keyCode;		/**< The key the event applies to.*/
	bool operator==( const KeyEvent & o ) const;	/**< Compares two key events for equality.*/
};

/** @brief Represents the type of mouse button event.
 */
enum MouseButtonEventType {
	MBET_PRESSED,	/**< The mouse button was pressed.*/
	MBET_RELEASED	/**< The mouse button was released.*/
};

/** @brief Data associated with a mouse button event.
 */
struct MouseButtonEvent {
	MouseButtonEventType type;	/**< The event type.*/
	int button;					/**< Which mouse button the event applies to.*/
	int x;						/**< The x position of the mouse.*/
	int y;						/**< The y position of the mouse.*/
	bool operator==( const MouseButtonEvent & o ) const;	/**< Compares two mouse button events for equality.*/
};

/** @brief Data associated with a mouse wheel event.
 */
struct MouseWheelEvent {
	float scroll;	/**< The amount the wheel was scrolled.*/
	int x;			/**< The x position of the mouse.*/
	int y;			/**< The y position of the mouse.*/
	bool operator==( const MouseWheelEvent & o ) const;	/**< Compares two mouse wheel events for equality.*/
};

/** @brief Data associated with a mouse position event.
 */
struct MousePositionEvent {
	int x;	/**< The x position of the mouse.*/
	int y;	/**< The y position of the mouse.*/
	bool operator==( const MousePositionEvent & o ) const;	/**< Compares two mouse position events for equality.*/
};

/** @brief Data associated with a mouse raw event.
 */
struct MouseRawEvent {
	int dx;	/**< The x delta of the mouse.*/
	int dy;	/**< The y delta of the mouse.*/
	bool operator==( const MouseRawEvent & o ) const;	/**< Compares two mouse raw events for equalitiy.*/
};

/** @brief An input event.
 */
struct InputEvent {
	InputEventType type;	/**< The event type.*/
	union {
		WindowEvent windowEvent;				/**< The window event data.*/
		CharacterEvent characterEvent;			/**< The character event data.*/
		KeyEvent keyEvent;						/**< The key event data.*/
		MouseButtonEvent mouseButtonEvent;		/**< The mouse button event data.*/
		MouseWheelEvent mouseWheelEvent;		/**< The mouse wheel event data.*/
		MousePositionEvent mousePositionEvent;	/**< The mouse position event data.*/
		MouseRawEvent mouseRawEvent;			/**< The mouse raw event data.*/
	};

	/** @brief Compares two input events for equality.
	 */
	bool operator==( const InputEvent & o ) const;
};

#if defined CRUNCH_PLATFORM_WINDOWS
#define MAKE_KEY_CODE( windows, linux, apple ) windows
static const uint KC_UNSUPPORTED = 0xE8;	/**< Value for unsupported keys (value corresponds to a code that happens to be unused).*/
#elif defined CRUNCH_PLATFORM_LINUX
#define MAKE_KEY_CODE( windows, linux, apple ) linux
static const uint KC_UNSUPPORTED = 0;
#elif defined CRUNCH_PLATFORM_APPLE
#define MAKE_KEY_CODE( windows, linux, apple ) apple
static const uint KC_UNSUPPORTED = 0xE8;
#else
#error Unsupported platform!
#endif

/** @brief Possible virtual key code values.
 */
enum KeyCode {
	KC_A	= MAKE_KEY_CODE( 'A',	0,		kVK_ANSI_A ),
	KC_B	= MAKE_KEY_CODE( 'B',	0,		kVK_ANSI_B ),
	KC_C	= MAKE_KEY_CODE( 'C',	0,		kVK_ANSI_C ),
	KC_D	= MAKE_KEY_CODE( 'D',	0,		kVK_ANSI_D ),
	KC_E	= MAKE_KEY_CODE( 'E',	0,		kVK_ANSI_E ),
	KC_F	= MAKE_KEY_CODE( 'F',	0,		kVK_ANSI_F ),
	KC_G	= MAKE_KEY_CODE( 'G',	0,		kVK_ANSI_G ),
	KC_H	= MAKE_KEY_CODE( 'H',	0,		kVK_ANSI_H ),
	KC_I	= MAKE_KEY_CODE( 'I',	0,		kVK_ANSI_I ),
	KC_J	= MAKE_KEY_CODE( 'J',	0,		kVK_ANSI_J ),
	KC_K	= MAKE_KEY_CODE( 'K',	0,		kVK_ANSI_K ),
	KC_L	= MAKE_KEY_CODE( 'L',	0,		kVK_ANSI_L ),
	KC_M	= MAKE_KEY_CODE( 'M',	0,		kVK_ANSI_M ),
	KC_N	= MAKE_KEY_CODE( 'N',	0,		kVK_ANSI_N ),
	KC_O	= MAKE_KEY_CODE( 'O',	0,		kVK_ANSI_O ),
	KC_P	= MAKE_KEY_CODE( 'P',	0,		kVK_ANSI_P ),
	KC_Q	= MAKE_KEY_CODE( 'Q',	0,		kVK_ANSI_Q ),
	KC_R	= MAKE_KEY_CODE( 'R',	0,		kVK_ANSI_R ),
	KC_S	= MAKE_KEY_CODE( 'S',	0,		kVK_ANSI_S ),
	KC_T	= MAKE_KEY_CODE( 'T',	0,		kVK_ANSI_T ),
	KC_U	= MAKE_KEY_CODE( 'U',	0,		kVK_ANSI_U ),
	KC_V	= MAKE_KEY_CODE( 'V',	0,		kVK_ANSI_V ),
	KC_W	= MAKE_KEY_CODE( 'W',	0,		kVK_ANSI_W ),
	KC_X	= MAKE_KEY_CODE( 'X',	0,		kVK_ANSI_X ),
	KC_Y	= MAKE_KEY_CODE( 'Y',	0,		kVK_ANSI_Y ),
	KC_Z	= MAKE_KEY_CODE( 'Z',	0,		kVK_ANSI_Z ),

	KC_0	= MAKE_KEY_CODE( '0',	0,		kVK_ANSI_0 ),
	KC_1	= MAKE_KEY_CODE( '1',	0,		kVK_ANSI_1 ),
	KC_2	= MAKE_KEY_CODE( '2',	0,		kVK_ANSI_2 ),
	KC_3	= MAKE_KEY_CODE( '3',	0,		kVK_ANSI_3 ),
	KC_4	= MAKE_KEY_CODE( '4',	0,		kVK_ANSI_4 ),
	KC_5	= MAKE_KEY_CODE( '5',	0,		kVK_ANSI_5 ),
	KC_6	= MAKE_KEY_CODE( '6',	0,		kVK_ANSI_6 ),
	KC_7	= MAKE_KEY_CODE( '7',	0,		kVK_ANSI_7 ),
	KC_8	= MAKE_KEY_CODE( '8',	0,		kVK_ANSI_8 ),
	KC_9	= MAKE_KEY_CODE( '9',	0,		kVK_ANSI_9 ),

	KC_EQUAL			= MAKE_KEY_CODE( VK_OEM_PLUS,	0,		kVK_ANSI_Equal ),
	KC_MINUS			= MAKE_KEY_CODE( VK_OEM_MINUS,	0,		kVK_ANSI_Minus ),
	KC_RIGHT_BRACKET	= MAKE_KEY_CODE( VK_OEM_6,		0,		kVK_ANSI_RightBracket ),
	KC_LEFT_BRACKET		= MAKE_KEY_CODE( VK_OEM_4,		0,		kVK_ANSI_LeftBracket ),
	KC_QUOTE			= MAKE_KEY_CODE( VK_OEM_7,		0,		kVK_ANSI_Quote ),
	KC_SEMICOLON		= MAKE_KEY_CODE( VK_OEM_1,		0,		kVK_ANSI_Semicolon ),
	KC_BACKSLASH		= MAKE_KEY_CODE( VK_OEM_5,		0,		kVK_ANSI_Backslash ),
	KC_COMMA			= MAKE_KEY_CODE( VK_OEM_COMMA,	0,		kVK_ANSI_Comma ),
	KC_SLASH			= MAKE_KEY_CODE( VK_OEM_2,		0,		kVK_ANSI_Slash ),
	KC_PERIOD			= MAKE_KEY_CODE( VK_OEM_PERIOD,	0,		kVK_ANSI_Period ),
	KC_GRAVE			= MAKE_KEY_CODE( VK_OEM_3,		0,		kVK_ANSI_Grave ),

	KC_NUMPAD_DECIMAL	= MAKE_KEY_CODE( VK_DECIMAL,		0,		kVK_ANSI_KeypadDecimal ),
	KC_NUMPAD_MULTIPLY	= MAKE_KEY_CODE( VK_MULTIPLY,		0,		kVK_ANSI_KeypadMultiply ),
	KC_NUMPAD_PLUS		= MAKE_KEY_CODE( VK_ADD,			0,		kVK_ANSI_KeypadPlus ),
	KC_NUMPAD_CLEAR		= MAKE_KEY_CODE( VK_CLEAR,			0,		kVK_ANSI_KeypadClear ),
	KC_NUMPAD_DIVIDE	= MAKE_KEY_CODE( VK_DIVIDE,			0,		kVK_ANSI_KeypadDivide ),
	KC_NUMPAD_ENTER		= MAKE_KEY_CODE( KC_UNSUPPORTED,	0,		kVK_ANSI_KeypadEnter ),
	KC_NUMPAD_MINUS		= MAKE_KEY_CODE( VK_SUBTRACT,		0,		kVK_ANSI_KeypadMinus ),
	KC_NUMPAD_EQUALS	= MAKE_KEY_CODE( KC_UNSUPPORTED,	0,		kVK_ANSI_KeypadEquals ),
	KC_NUMPAD_0			= MAKE_KEY_CODE( VK_NUMPAD0,		0,		kVK_ANSI_Keypad0 ),
	KC_NUMPAD_1			= MAKE_KEY_CODE( VK_NUMPAD1,		0,		kVK_ANSI_Keypad1 ),
	KC_NUMPAD_2			= MAKE_KEY_CODE( VK_NUMPAD2,		0,		kVK_ANSI_Keypad2 ),
	KC_NUMPAD_3			= MAKE_KEY_CODE( VK_NUMPAD3,		0,		kVK_ANSI_Keypad3 ),
	KC_NUMPAD_4			= MAKE_KEY_CODE( VK_NUMPAD4,		0,		kVK_ANSI_Keypad4 ),
	KC_NUMPAD_5			= MAKE_KEY_CODE( VK_NUMPAD5,		0,		kVK_ANSI_Keypad5 ),
	KC_NUMPAD_6			= MAKE_KEY_CODE( VK_NUMPAD6,		0,		kVK_ANSI_Keypad6 ),
	KC_NUMPAD_7			= MAKE_KEY_CODE( VK_NUMPAD7,		0,		kVK_ANSI_Keypad7 ),
	KC_NUMPAD_8			= MAKE_KEY_CODE( VK_NUMPAD8,		0,		kVK_ANSI_Keypad8 ),
	KC_NUMPAD_9			= MAKE_KEY_CODE( VK_NUMPAD9,		0,		kVK_ANSI_Keypad9 ),

	KC_RETURN			= MAKE_KEY_CODE( VK_RETURN,		0,		kVK_Return ),
	KC_TAB				= MAKE_KEY_CODE( VK_TAB,		0,		kVK_Tab ),
	KC_SPACE			= MAKE_KEY_CODE( VK_SPACE,		0,		kVK_Space ),
	KC_BACKSPACE		= MAKE_KEY_CODE( VK_BACK,		0,		kVK_Delete ),
	KC_ESCAPE			= MAKE_KEY_CODE( VK_ESCAPE,		0,		kVK_Escape ),
	KC_CAPS_LOCK		= MAKE_KEY_CODE( VK_CAPITAL,	0,		kVK_CapsLock ),

	KC_CONTROL			= MAKE_KEY_CODE( VK_CONTROL,	0,		kVK_Control ),
	KC_LEFT_CONTROL		= MAKE_KEY_CODE( VK_LCONTROL,	0,		KC_UNSUPPORTED ),
	KC_RIGHT_CONTROL	= MAKE_KEY_CODE( VK_RCONTROL,	0,		kVK_RightControl ),
	KC_SHIFT			= MAKE_KEY_CODE( VK_SHIFT,		0,		kVK_Shift ),
	KC_LEFT_SHIFT		= MAKE_KEY_CODE( VK_LSHIFT,		0,		KC_UNSUPPORTED ),
	KC_RIGHT_SHIFT		= MAKE_KEY_CODE( VK_RSHIFT,		0,		kVK_RightShift ),
	KC_ALT				= MAKE_KEY_CODE( VK_MENU,		0,		kVK_Option ),
	KC_LEFT_ALT			= MAKE_KEY_CODE( VK_LMENU,		0,		KC_UNSUPPORTED ),
	KC_RIGHT_ALT		= MAKE_KEY_CODE( VK_RMENU,		0,		kVK_RightOption ),
	KC_COMMAND			= MAKE_KEY_CODE( 0,				0,		kVK_Command ),

	KC_DELETE			= MAKE_KEY_CODE( VK_DELETE,		0,		kVK_ForwardDelete ),
	KC_HOME				= MAKE_KEY_CODE( VK_HOME,		0,		kVK_Home ),
	KC_END				= MAKE_KEY_CODE( VK_END,		0,		kVK_End ),
	KC_PAGE_UP			= MAKE_KEY_CODE( VK_PRIOR,		0,		kVK_PageUp ),
	KC_PAGE_DOWN		= MAKE_KEY_CODE( VK_NEXT,		0,		kVK_PageDown ),
	KC_LEFT_ARROW		= MAKE_KEY_CODE( VK_LEFT,		0,		kVK_LeftArrow ),
	KC_RIGHT_ARROW		= MAKE_KEY_CODE( VK_RIGHT,		0,		kVK_RightArrow ),
	KC_UP_ARROW			= MAKE_KEY_CODE( VK_UP,			0,		kVK_UpArrow ),
	KC_DOWN_ARROW		= MAKE_KEY_CODE( VK_DOWN,		0,		kVK_DownArrow ),

	KC_F1				= MAKE_KEY_CODE( VK_F1,			0,		kVK_F1 ),
	KC_F2				= MAKE_KEY_CODE( VK_F2,			0,		kVK_F2 ),
	KC_F3				= MAKE_KEY_CODE( VK_F3,			0,		kVK_F3 ),
	KC_F4				= MAKE_KEY_CODE( VK_F4,			0,		kVK_F4 ),
	KC_F5				= MAKE_KEY_CODE( VK_F5,			0,		kVK_F5 ),
	KC_F6				= MAKE_KEY_CODE( VK_F6,			0,		kVK_F6 ),
	KC_F7				= MAKE_KEY_CODE( VK_F7,			0,		kVK_F7 ),
	KC_F8				= MAKE_KEY_CODE( VK_F8,			0,		kVK_F8 ),
	KC_F9				= MAKE_KEY_CODE( VK_F9,			0,		kVK_F9 ),
	KC_F10				= MAKE_KEY_CODE( VK_F10,		0,		kVK_F10 ),
	KC_F11				= MAKE_KEY_CODE( VK_F11,		0,		kVK_F11 ),
	KC_F12				= MAKE_KEY_CODE( VK_F12,		0,		kVK_F12 )
};

/** @brief Possible mouse button codes.
 */
enum MouseButtonCode {
	MBC_LEFT	= 0,
	MBC_RIGHT	= 1,
	MBC_MIDDLE	= 2,
	MOUSE_BUTTON_CODE_COUNT
};

#endif