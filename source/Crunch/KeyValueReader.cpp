#include "KeyValueReader.h"

KeyValueReader::FilePos::FilePos( std::ifstream & i )
	: in( i ) {
	line = 0;
	linePos = 0;
}

KeyValueReader::Value::Value() {
}

KeyValueReader::Value::Value( Value && other ) {
	type = other.type;
	key.swap( other.key );
	switch (type) {
	case VAL_INT:
		intValue = other.intValue;
		break;
	case VAL_UINT:
		intValue = other.intValue;
		break;
	case VAL_FLOAT:
		floatValue = other.floatValue;
		break;
	case VAL_BOOL:
		boolValue = other.boolValue;
		break;
	case VAL_STRING:
		stringValue.swap( other.stringValue );
		break;
	case VAL_BINARY:
		binaryValue.swap( other.binaryValue );
		break;
	case VAL_ARRAY:
		arrayValue.swap( other.arrayValue );
		arrayMap.swap( other.arrayMap );
	}
}

KeyValueReader::Value & KeyValueReader::Value::operator=( Value && other ) {
	type = other.type;
	key.swap( other.key );
	switch (type) {
		case VAL_INT:
			intValue = other.intValue;
			break;
		case VAL_UINT:
			intValue = other.intValue;
			break;
		case VAL_FLOAT:
			floatValue = other.floatValue;
			break;
		case VAL_BOOL:
			boolValue = other.boolValue;
			break;
		case VAL_STRING:
			stringValue.swap( other.stringValue );
			break;
		case VAL_BINARY:
			binaryValue.swap( other.binaryValue );
			break;
		case VAL_ARRAY:
			arrayValue.swap( other.arrayValue );
			arrayMap.swap( other.arrayMap );
	}

	return *this;
}

KeyValueReader::KeyValueReader( const std::string & fname ) {
	root.type = VAL_ARRAY;

	std::ifstream in( fname );
	if (!in.is_open())
		throw std::runtime_error( "Failed to open file" );

	std::vector <byte> binaryBuffer;
	FilePos fp( in );

	std::stack <Value*> arrayStack;
	// start the recursive reading at the root
	arrayStack.push( &root );
	// start reading the file
	while (true) {
		bool anonymous = false;

		// first read the key
		std::pair <TokenType, std::string> key = getNextToken( fp, binaryBuffer );
		if (key.first == TT_NONE) {
			// if none, verify that we really are done
			if (arrayStack.size() == 1)
				break; // we're done
			else
				throw std::runtime_error( "Unexpected end of file" );
		} else if (key.first == TT_OPERATOR && key.second == "}") {
			if (arrayStack.size() <= 1)
				throw std::runtime_error( "Mismatched array (" + toString( fp.line ) + ")" );
			else {
				arrayStack.pop();
				continue;
			}
		} else if (key.first == TT_OPERATOR && key.second == "*")
			anonymous = true;
		else if (key.first != TT_KEY)
			throw std::runtime_error( "Invalid key " + key.second + " specified (" + toString( fp.line ) + ")" );

		if (!anonymous) {
			// make sure it isn't duplicate
			std::string upperString = convertToUpper( key.second );
			if (arrayStack.top()->arrayMap.find( upperString ) != arrayStack.top()->arrayMap.end())
				throw std::runtime_error( "Duplicate key " + key.second + " specified (" + toString( fp.line ) + ")" );

			// next read the assignment operator
			std::pair <TokenType, std::string> assignment = getNextToken( fp, binaryBuffer );
			if (key.first == TT_NONE)
				throw std::runtime_error( "Unexpected end of file" );
			else if (assignment.first != TT_OPERATOR || assignment.second != "=")
				throw std::runtime_error( "Invalid key assignment operator " + assignment.second + " specified (" + toString( fp.line ) + ")" );

			// read value
			arrayStack.top()->arrayMap.insert( std::make_pair( upperString, arrayStack.top()->arrayValue.size() ) );
		}

		arrayStack.top()->arrayValue.push_back( Value() );
		Value & v = arrayStack.top()->arrayValue.back();
		if (!anonymous)
			v.key = key.second;
		std::pair <TokenType, std::string> value = getNextToken( fp, binaryBuffer );
		if (value.first == TT_NONE)
			throw std::runtime_error( "Unexpected end of file" );
		else if (value.first == TT_INT) {
			v.type = VAL_INT;
			bool success = fromString( value.second, v.intValue );
			assert( success );
		} else if (value.first == TT_FLOAT) {
			v.type = VAL_FLOAT;
			bool success = fromString( value.second, v.floatValue );
			assert( success );
		} else if (value.first == TT_BOOLEAN) {
			v.type = VAL_BOOL;
			v.boolValue = (value.second[0] == 't' || value.second[0] == 'T');
		} else if (value.first == TT_STRING) {
			v.type = VAL_STRING;
			v.stringValue = value.second;
		} else if (value.first == TT_BINARY) {
			v.type = VAL_BINARY;
			v.binaryValue.swap( binaryBuffer );
		} else if (value.first == TT_OPERATOR && value.second == "{") {
			v.type = VAL_ARRAY;
			arrayStack.push( &v );
		} else
			throw std::runtime_error( "Invalid value " + value.second + " specified (" + toString( fp.line ) + ")" );
	}
}

std::pair <KeyValueReader::TokenType, std::string> KeyValueReader::getNextToken( KeyValueReader::FilePos & fp, std::vector <byte> & binaryBuffer ) {
	std::pair <TokenType, std::string> token( TT_NONE, std::string() );

	// consume whitespace at beginning
	bool done = false;
	while (!done) {
		// if end of line or comment, get next line
		while (fp.linePos >= fp.currentLine.length() || fp.currentLine[fp.linePos] == '#') {
			if (fp.in.eof())
				return token; // return empty; return token for named return value optimization
			else {
				std::getline( fp.in, fp.currentLine );
				fp.linePos = 0;
				++fp.line;
			}
		}

		// keep skipping until non-whitespace encountered
		char nextChar = fp.currentLine[fp.linePos];
		if (nextChar == ' ' || nextChar == '\t')
			++fp.linePos;
		else
			done = true;
	}

	// used for checking syntax
	struct Checker {
		static bool isOperator( char ch ) {
			return (ch == '{' || ch == '}' || ch == '=' || ch == '*');
		}

		static bool isAlpha( char ch ) {
			return ((ch >= 'A' && ch <= 'Z') ||
					(ch >= 'a' && ch <= 'z') ||
					(ch == '_'));
		}

		static bool isNumeric( char ch ) {
			return (ch >= '0' && ch <= '9');
		}

		static bool isAlphaNumeric( char ch ) {
			return isAlpha( ch ) || isNumeric( ch );
		}

		static bool isNumericStart( char ch ) {
			return isNumeric( ch ) || ch == '-' || ch == '+';
		}

		static bool isStringStart( char ch ) {
			return (ch == '"');
		}

		static bool isBinaryStart( char ch ) {
			return (ch == '\'');
		}
	};

	// get the next type of token
	if (Checker::isOperator( fp.currentLine[fp.linePos] )) {
		// read the operator
		token.first = TT_OPERATOR;
		token.second = fp.currentLine[fp.linePos];
		++fp.linePos;
		return token;
	} else if (Checker::isAlpha( fp.currentLine[fp.linePos] )) {
		// keep adding to the key while there are more alphanumerics
		do {
			token.second.push_back( fp.currentLine[fp.linePos] );
			++fp.linePos;
		} while (fp.linePos < fp.currentLine.length() &&
				 Checker::isAlphaNumeric( fp.currentLine[fp.linePos] ));

		// check for boolean
		std::string upper = convertToUpper( token.second );
		if (upper == "TRUE" || upper == "FALSE")
			token.first = TT_BOOLEAN;
		else
			token.first = TT_KEY;
		return token;
	} else if (Checker::isNumericStart( fp.currentLine[fp.linePos] )) {
		// if . or e is encountered, mark it as float
		bool isFloat = false;
		bool dotFound = false;
		bool eFound = false;
		bool digitsFound = Checker::isNumeric( fp.currentLine[fp.linePos] );
		bool more;
		do {
			token.second.push_back( fp.currentLine[fp.linePos] );
			++fp.linePos;

			// parse the number; exceptions should be self-explanatory
			more = true;
			if (fp.linePos >= fp.currentLine.length())
				more = false;
			else if (fp.currentLine[fp.linePos] == 'e' || fp.currentLine[fp.linePos] == 'E') {
				if (eFound)
					throw std::runtime_error( "Numeric values cannot contain more than one exponent (" + toString( fp.line ) + ")" );
				else if (!digitsFound)
					throw std::runtime_error( "Numeric values must contain at least one digit before the exponent (" + toString( fp.line ) + ")" );
				else {
					isFloat = true;
					eFound = true;
				}
			} else if (Checker::isAlpha( fp.currentLine[fp.linePos] ))
				throw std::runtime_error( "Numeric values cannot contain letters or underscores (" + toString( fp.line ) + ")" );
			else if (fp.currentLine[fp.linePos] == '.') {
				if (dotFound)
					throw std::runtime_error( "Numeric values cannot contain more than one decimal point (" + toString( fp.line ) + ")" );
				else if (eFound)
					throw std::runtime_error( "Numeric values cannot contain fractional exponents (" + toString( fp.line ) + ")" );
				else {
					isFloat = true;
					dotFound = true;
				}
			} else if (fp.currentLine[fp.linePos] == '+' || fp.currentLine[fp.linePos] == '-') {
				if (token.second.back() != 'e' && token.second.back() != 'E')
					throw std::runtime_error( "Numeric values cannot contain signs except at beginnings or following exponents (" + toString( fp.line ) + ")" );
			} else if (Checker::isNumeric( fp.currentLine[fp.linePos] ))
				digitsFound = true;
			else
				more = false;
		} while (more);
		if (!digitsFound)
			throw std::runtime_error( "Numeric values must contain at least one digit (" + toString( fp.line ) + ")" );
		if (token.second.back() == 'e' || token.second.back() == 'E' || token.second.back() == '-' || token.second.back() == '+')
			throw std::runtime_error( "Numeric values must contain at least one digit following an exponent (" + toString( fp.line ) + ")" );

		if (isFloat)
			token.first = TT_FLOAT;
		else
			token.first = TT_INT;
		return token;
	} else if (Checker::isStringStart( fp.currentLine[fp.linePos] )) {
		// skip the quote
		++fp.linePos;

		// read values
		done = false;
		do {
			if (fp.linePos >= fp.currentLine.length())
				throw std::runtime_error( "Unclosed string (" + toString( fp.line ) + ")" );
			else if (fp.currentLine[fp.linePos] == '\\') {
				// parse escapes
				++fp.linePos;
				if (fp.linePos >= fp.currentLine.length())
					throw std::runtime_error( "Escape sequence terminates early (" + toString( fp.line ) + ")" );
				else if (fp.currentLine[fp.linePos] == 'n')
					token.second.push_back( '\n' );
				else if (fp.currentLine[fp.linePos] == 't')
					token.second.push_back( '\t' );
				else if (fp.currentLine[fp.linePos] == '"')
					token.second.push_back( '"' );
				else if (fp.currentLine[fp.linePos] == '\\')
					token.second.push_back( '\\' );
				else if (fp.currentLine[fp.linePos] == 'x') {
					// used to convert characters to hex
					struct Hex {
						static bool isHex( char ch ) {
							return ((ch >= '0' && ch <= '9') ||
									(ch >= 'a' && ch <= 'f') ||
									(ch >= 'A' && ch <= 'F'));
						}

						static int toHex( char ch ) {
							if (ch >= '0' && ch <= '9')
								return (int)ch - (int)'0';
							else if (ch >= 'a' && ch <= 'f')
								return (int)ch - (int)'a' + 10;
							else if (ch >= 'A' && ch <= 'F')
								return (int)ch - (int)'A' + 10;
							else
								return -1;
						}
					};

					// read the 2 hex digits
					char hex[2];
					for (int i = 0; i < 2; ++i) {
						++fp.linePos;
						if (fp.linePos >= fp.currentLine.length())
							throw std::runtime_error( "Escape sequence terminates early (" + toString( fp.line ) + ")" );
						hex[i] = fp.currentLine[fp.linePos];
					}
					// make sure they're valid
					if (!Hex::isHex( hex[0] ) || !Hex::isHex( hex[1] ))
						throw std::runtime_error( std::string( "Invalid hex string \"" ) + hex[0] + hex[1] + "\" in escape sequence (" + toString( fp.line ) + ")" );
					// get the hex value
					int hexVal = Hex::toHex( hex[0] )*16 + Hex::toHex( hex[1] );
					token.second.push_back( (char)hexVal );
				} else
					throw std::runtime_error( std::string( "Unknown escape sequence " ) + fp.currentLine[fp.linePos] + " (" + toString( fp.line ) + ")" );

				++fp.linePos;
			} else if (fp.currentLine[fp.linePos] == '"')
				done = true;
			else
				token.second.push_back( fp.currentLine[fp.linePos] );

			++fp.linePos;
		} while (!done);

		token.first = TT_STRING;
		return token;
	} else if (Checker::isBinaryStart( fp.currentLine[fp.linePos] )) {
		// skip the quote
		++fp.linePos;

		// read values
		done = false;
		char hex0;
		bool first = false;
		do {
			if (fp.linePos >= fp.currentLine.length())
				throw std::runtime_error( "Unclosed binary data (" + toString( fp.line ) + ")" );
			else if (fp.currentLine[fp.linePos] == '\'') {
				if (!first)
					done = true;
				else
					// we cut off in the middle of a byte
					throw std::runtime_error( "Binary data terminates early (" + toString( fp.line ) + ")" );
			} else {
				// used to convert characters to hex
				struct Hex {
					static bool isHex( char ch ) {
						return ((ch >= '0' && ch <= '9') ||
								(ch >= 'a' && ch <= 'f') ||
								(ch >= 'A' && ch <= 'F'));
					}

					static int toHex( char ch ) {
						if (ch >= '0' && ch <= '9')
							return (int)ch - (int)'0';
						else if (ch >= 'a' && ch <= 'f')
							return (int)ch - (int)'a' + 10;
						else if (ch >= 'A' && ch <= 'F')
							return (int)ch - (int)'A' + 10;
						else
							return -1;
					}
				};

				// read the hex digit
				char h = fp.currentLine[fp.linePos];

				// make sure they're valid
				if (!Hex::isHex( h ))
					throw std::runtime_error( std::string( "Invalid hex character \"" ) + h + "\" in binary data (" + toString( fp.line ) + ")" );

				if (!first)
					// this is the first character of the two
					hex0 = h;
				else {
					// we have both characters - process it
					// get the hex value
					int hexVal = Hex::toHex( hex0 )*16 + Hex::toHex( h );
					binaryBuffer.push_back( (byte)hexVal );
				}
				first = !first;
				if (token.second.length() < 16)
					token.second.push_back( h );
				else if (token.second.length() == 16)
					// if hex data exceeds 16 characters, add "..." to the end
					// this is just for error display info, as the real binary data is in binaryBuffer
					token.second += "...";
			}

			++fp.linePos;
		} while (!done);

		token.first = TT_BINARY;
		return token;
	} else
		throw std::runtime_error( std::string( "Invalid character " ) + fp.currentLine[fp.linePos] + " (" + toString( fp.line ) + ")" );
}

KeyValueReader::Iterator KeyValueReader::getIterator() const {
	return Iterator( root );
}

KeyValueReader::Iterator::Iterator( const KeyValueReader::Value & src )
	: source( &src ) {
}

KeyValueReader::Iterator::Iterator( const KeyValueReader::Iterator & it )
	: source( it.source ) {
}

KeyValueReader::Iterator & KeyValueReader::Iterator::operator=( const KeyValueReader::Iterator & it ) {
	source = it.source;
	return *this;
}

const KeyValueReader::Value & KeyValueReader::Iterator::getValue( size_t i ) const {
	if (i >= source->arrayValue.size())
		throw std::runtime_error( "Index " + toString( i ) + " out of range" );
	return source->arrayValue[i];
}

const KeyValueReader::Value & KeyValueReader::Iterator::getValue( const std::string & key ) const {
	std::string upper = convertToUpper( key );
	std::map <std::string, size_t>::const_iterator i = source->arrayMap.find( upper );
	if (i == source->arrayMap.end())
		throw std::runtime_error( "Nonexistent key " + key + " specified" );
	return source->arrayValue[i->second];
}

size_t KeyValueReader::Iterator::getCount() const {
	return source->arrayValue.size();
}

bool KeyValueReader::Iterator::isAnonymous( size_t i ) const {
	if (i >= source->arrayValue.size())
		throw std::runtime_error( "Index " + toString( i ) + " out of range" );
	return source->arrayValue[i].key.empty();
}

std::string KeyValueReader::Iterator::getKey( size_t i ) const {
	if (i >= source->arrayValue.size())
		throw std::runtime_error( "Index " + toString( i ) + " out of range" );
	if (source->arrayValue[i].key.empty())
		throw std::runtime_error( "Value at index " + toString( i ) + " is anonymous" );
	return source->arrayValue[i].key;
}

KeyValueReader::ValueType KeyValueReader::Iterator::getType( size_t i ) const {
	return getValue( i ).type;
}

KeyValueReader::ValueType KeyValueReader::Iterator::getType( const std::string & key ) const {
	return getValue( key ).type;
}

bool KeyValueReader::Iterator::isType( const std::string & key, KeyValueReader::ValueType type ) const {
	const KeyValueReader::Value & v = getValue( key );
	return ((v.type == type) ||
			(v.type == KeyValueReader::VAL_INT && type == KeyValueReader::VAL_FLOAT) ||
			(v.type == KeyValueReader::VAL_INT && type == KeyValueReader::VAL_UINT && v.intValue >= 0));
}

bool KeyValueReader::Iterator::contains( const std::string & key ) const {
	std::string upper = convertToUpper( key );
	std::map <std::string, size_t>::const_iterator i = source->arrayMap.find( upper );
	return (i != source->arrayMap.end());

}
bool KeyValueReader::Iterator::contains( const std::string & key, KeyValueReader::ValueType type ) const {
	std::string upper = convertToUpper( key );
	std::map <std::string, size_t>::const_iterator i = source->arrayMap.find( upper );
	if (i == source->arrayMap.end())
		return false;
	const KeyValueReader::Value & v = source->arrayValue[i->second];
	return ((v.type == type) ||
			(v.type == KeyValueReader::VAL_INT && type == KeyValueReader::VAL_FLOAT) ||
			(v.type == KeyValueReader::VAL_INT && type == KeyValueReader::VAL_UINT && v.intValue >= 0));
}

int KeyValueReader::Iterator::getInt( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_INT)
		return v.intValue;
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type int" );
}

int KeyValueReader::Iterator::getInt( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_INT)
		return v.intValue;
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type int" );
}

uint KeyValueReader::Iterator::getUint( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_INT && v.type >= 0)
		return (uint)v.intValue;
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type unsigned int" );
}

uint KeyValueReader::Iterator::getUint( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_INT && v.type >= 0)
		return (uint)v.intValue;
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type unsigned int" );
}

float KeyValueReader::Iterator::getFloat( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_FLOAT)
		return v.floatValue;
	else if (v.type == KeyValueReader::VAL_INT)
		return (float)v.intValue;
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type float" );
}

float KeyValueReader::Iterator::getFloat( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_FLOAT)
		return v.floatValue;
	else if (v.type == KeyValueReader::VAL_INT)
		return (float)v.intValue;
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type float" );
}

bool KeyValueReader::Iterator::getBool( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_BOOL)
		return v.boolValue;
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type bool" );
}

bool KeyValueReader::Iterator::getBool( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_BOOL)
		return v.boolValue;
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type bool" );
}

std::string KeyValueReader::Iterator::getString( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_STRING)
		return v.stringValue;
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type string" );
}

std::string KeyValueReader::Iterator::getString( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_STRING)
		return v.stringValue;
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type string" );
}

size_t KeyValueReader::Iterator::getBinaryDataLength( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_BINARY)
		return v.binaryValue.size();
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type binary" );
}

size_t KeyValueReader::Iterator::getBinaryDataLength( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_BINARY)
		return v.binaryValue.size();
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type binary" );
}

const byte * KeyValueReader::Iterator::getBinaryData( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_BINARY)
		return v.binaryValue.empty() ? NULL : &v.binaryValue[0];
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type binary" );
}

const byte * KeyValueReader::Iterator::getBinaryData( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_BINARY)
		return v.binaryValue.empty() ? NULL : &v.binaryValue[0];
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type binary" );
}

KeyValueReader::Iterator KeyValueReader::Iterator::getArray( const std::string & key ) const {
	const KeyValueReader::Value & v = getValue( key );
	if (v.type == KeyValueReader::VAL_ARRAY)
		return Iterator( v );
	else
		throw std::runtime_error( "Key " + key + " does not have a value of type array" );
}

KeyValueReader::Iterator KeyValueReader::Iterator::getArray( size_t i ) const {
	const KeyValueReader::Value & v = getValue( i );
	if (v.type == KeyValueReader::VAL_ARRAY)
		return Iterator( v );
	else
		throw std::runtime_error( "Index " + toString( i ) + " does not have a value of type array" );
}