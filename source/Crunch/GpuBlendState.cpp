#include "GpuBlendState.h"
#include "GpuState.h"

GpuBlendState::GpuBlendState()
	: GpuObject( NULL ) {
}

void GpuBlendState::bind() {
	getGpuState()->bind( *this );
}

void GpuBlendState::setBlendingEnabled( bool e ) {
	blendingEnabled = e;
}

void GpuBlendState::setBlendEquation( GLenum equation ) {
	blendEquationColor = equation;
	blendEquationAlpha = equation;
}

void GpuBlendState::setBlendEquations( GLenum equationColor, GLenum equationAlpha ) {
	blendEquationColor = equationColor;
	blendEquationAlpha = equationAlpha;
}

void GpuBlendState::setBlendFunction( GLenum sourceFactor, GLenum destFactor ) {
	blendSourceFactorColor = sourceFactor;
	blendSourceFactorAlpha = sourceFactor;
	blendDestinationFactorColor = destFactor;
	blendDestinationFactorAlpha = destFactor;
}

void GpuBlendState::setBlendFunctions( GLenum sourceFactorColor, GLenum destFactorColor, GLenum sourceFactorAlpha, GLenum destFactorAlpha ) {
	blendSourceFactorColor = sourceFactorColor;
	blendSourceFactorAlpha = sourceFactorAlpha;
	blendDestinationFactorColor = destFactorColor;
	blendDestinationFactorAlpha = destFactorAlpha;
}

void GpuBlendState::setBlendFunction( BlendMode blendMode ) {
	static const GLenum BLEND_MODES[BLEND_MODE_COUNT][3] = {
		{ 0,			0,						0 },
		{ GL_FUNC_ADD,	GL_SRC_ALPHA,			GL_ONE_MINUS_SRC_ALPHA },
		{ GL_FUNC_ADD,	GL_SRC_ALPHA,			GL_ONE },
		{ GL_FUNC_ADD,	GL_ONE,					GL_ONE },
		{ GL_FUNC_ADD,	GL_ZERO,				GL_SRC_COLOR },
		{ GL_FUNC_ADD,	GL_ONE_MINUS_DST_COLOR,	GL_ONE_MINUS_SRC_COLOR }
	};

	if (blendMode == BM_NONE)
		blendingEnabled = false;
	else {
		blendingEnabled = true;
		blendEquationColor = BLEND_MODES[blendMode][0];
		blendEquationAlpha = BLEND_MODES[blendMode][0];
		blendSourceFactorColor = BLEND_MODES[blendMode][1];
		blendSourceFactorAlpha = BLEND_MODES[blendMode][1];
		blendDestinationFactorColor = BLEND_MODES[blendMode][2];
		blendDestinationFactorAlpha = BLEND_MODES[blendMode][2];
	}
}

void GpuBlendState::setBlendColor( const Vector4f & color ) {
	blendColor = color;
}

bool GpuBlendState::isBlendingEnabled() const {
	return blendingEnabled;
}

GLenum GpuBlendState::getBlendEquationColor() const {
	return blendEquationColor;
}

GLenum GpuBlendState::getBlendEquationAlpha() const {
	return blendEquationAlpha;
}

GLenum GpuBlendState::getBlendFunctionSourceFactorColor() const {
	return blendSourceFactorColor;
}

GLenum GpuBlendState::getBlendFunctionDestinationFactorColor() const {
	return blendDestinationFactorColor;
}

GLenum GpuBlendState::getBlendFunctionSourceFactorAlpha() const {
	return blendSourceFactorAlpha;
}

GLenum GpuBlendState::getBlendFunctionDestinationFactorAlpha() const {
	return blendDestinationFactorAlpha;
}

Vector4f GpuBlendState::getBlendColor() const {
	return blendColor;
}

GpuBlendState::GpuBlendState( GpuState * state )
	: GpuObject( state ) {
	blendingEnabled = false;
	blendEquationColor = GL_FUNC_ADD;
	blendEquationAlpha = GL_FUNC_ADD;
	blendSourceFactorColor = GL_ONE;
	blendSourceFactorAlpha = GL_ONE;
	blendDestinationFactorColor = GL_ZERO;
	blendDestinationFactorAlpha = GL_ZERO;
}

void GpuBlendState::apply( GpuBlendState & currentState ) const {
	setStateEnabled( blendingEnabled, currentState.blendingEnabled, GL_BLEND );

	if (blendingEnabled) {
		if (blendEquationColor != currentState.blendEquationColor ||
			blendEquationAlpha != currentState.blendEquationAlpha) {
			glBlendEquationSeparate( blendEquationColor, blendEquationAlpha );
			currentState.blendEquationColor = blendEquationColor;
			currentState.blendEquationAlpha = blendEquationAlpha;
		}

		if (blendSourceFactorColor != currentState.blendSourceFactorColor ||
			blendSourceFactorAlpha != currentState.blendSourceFactorAlpha ||
			blendDestinationFactorColor != currentState.blendDestinationFactorColor ||
			blendDestinationFactorAlpha != currentState.blendDestinationFactorAlpha) {
			glBlendFuncSeparate(
				blendSourceFactorColor, blendDestinationFactorColor,
				blendSourceFactorAlpha, blendDestinationFactorAlpha );
			currentState.blendSourceFactorColor = blendSourceFactorColor;
			currentState.blendSourceFactorAlpha = blendSourceFactorAlpha;
			currentState.blendDestinationFactorColor = blendDestinationFactorColor;
			currentState.blendDestinationFactorAlpha = blendDestinationFactorAlpha;
		}

		if (blendColor != currentState.blendColor) {
			glBlendColor( blendColor.r, blendColor.g, blendColor.b, blendColor.a );
			currentState.blendColor = blendColor;
		}
	}
}

void GpuBlendState::getCurrentState() {
	blendingEnabled = glIsEnabled( GL_BLEND ) != 0;
	glGetIntegerv( GL_BLEND_EQUATION_RGB, (GLint*)&blendEquationColor );
	glGetIntegerv( GL_BLEND_EQUATION_ALPHA, (GLint*)&blendEquationAlpha );
	glGetIntegerv( GL_BLEND_SRC_RGB, (GLint*)&blendSourceFactorColor );
	glGetIntegerv( GL_BLEND_SRC_ALPHA, (GLint*)&blendSourceFactorAlpha );
	glGetIntegerv( GL_BLEND_DST_RGB, (GLint*)&blendDestinationFactorColor );
	glGetIntegerv( GL_BLEND_DST_ALPHA, (GLint*)&blendDestinationFactorAlpha );
	glGetFloatv( GL_BLEND_COLOR, &blendColor[0] );
}