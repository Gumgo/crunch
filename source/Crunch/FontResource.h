/** @file FontResource.h
 *  @brief Contains the resource holding font data.
 */

#ifndef FONTRESOURCE_DEFINED
#define FONTRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "GpuTexture.h"
#include "DataReader.h"
#include <vector>

/** @brief Font resource.
 */
DEFINE_RESOURCE_CLASS( FontResource, "font", "fonts" )
public:
	/** @brief The constructor.
	 */
	FontResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief Holds data for a single glyph (character) of a font.
	 */
	struct Glyph {
		float x1;		/**< The left x-coordinate on the font texture.*/
		float y1;		/**< The top y-coordinate on the font texture.*/
		float x2;		/**< The right x-coordinate on the font texture.*/
		uint advance;	/**< The number of horizontal pixels this character uses.*/
	};

	uint getLineHeight() const;						/**< Returns the line height in pixels.*/
	float getTextureLineHeight() const;				/**< Returns the line height in texture coordinates.*/
	bool containsGlyph( char c ) const;				/**< Returns whether the font contains a glyph for the given character.*/
	size_t getGlyphIndex( char c ) const;			/**< Returns the index of the glyph for the given character.*/
	const Glyph & getGlyph( size_t i ) const;		/**< Returns the glyph with the given index.*/
	const Glyph getCursorGlyph() const;				/**< Returns the glyph representing the cursor.*/
	GpuTextureReference getTexture();				/**< Returns the OpenGL texture object for the font.*/
	GpuTextureConstReference getTexture() const;	/**< Returns the OpenGL texture object for the font.*/

private:
	uint width, height;					// texture size
	uint lineHeight;					// the line height in pixels
	float textureLineHeight;			// the line height in texture coordinates
	std::vector <Glyph> glyphs;			// the array of glyphs
	std::vector <size_t> glyphTable;	// indices of glyphs for each character
	std::vector <bool> glyphsValid;		// whether the font has each glyph
	Glyph cursor;						// the cursor glyph

	std::vector <byte> data;			// the font texture data
	GpuTextureReference texture;		// the OpenGL texture object
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <FontResource> FontResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <FontResource> FontResourceConstReference;

#endif