#vertex_shader
#version 330

in vec3 position;
in vec3 normal;

uniform mat3x4 modelViewMatrix;
uniform mat4 mvpMatrix;

out vec3 viewSpaceNormal;
out vec3 viewSpacePosition;

void main() {
	viewSpaceNormal = (vec4( normal, 0.0f ) * modelViewMatrix).xyz;
	viewSpacePosition = (vec4( position, 1.0f ) * modelViewMatrix).xyz;

	gl_Position = vec4( position, 1.0f ) * mvpMatrix;
}

#fragment_shader
#version 330
precision highp float;

in vec3 viewSpaceNormal;
in vec3 viewSpacePosition;

uniform float specularPower;

layout( location = 0 ) out vec4 outNormalSpec;
layout( location = 1 ) out vec4 outFaceNormal;
layout( location = 2 ) out vec4 outDepth;

#include "deferred_helpers.prog"

void main() {
	outNormalSpec = packNormalSpec( normalize( viewSpaceNormal ), viewSpacePosition, specularPower );
	outFaceNormal = packFaceNormal( viewSpacePosition );
	outDepth = packDepth( viewSpacePosition );
}