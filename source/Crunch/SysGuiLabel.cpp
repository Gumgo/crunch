#include "SysGuiLabel.h"
#include "Context.h"
#include "SysRenderer2D.h"

void SysGuiLabel::onCreate() {
	SysGui::onCreate();
}

void SysGuiLabel::move( const Vector2i & pos ) {
	position = pos;
}

void SysGuiLabel::setText( const std::string & t ) {
	text = t;
}

void SysGuiLabel::receiveEvent( const InputEvent & ev ) {
}

void SysGuiLabel::onDismissParent() {
}

void SysGuiLabel::draw() {
	if (camera != NULL) {
		Matrix33af modelMatrix;
		modelMatrix.translate( (float)position.x, (float)position.y + 8.0f );
		Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );
		SysRenderer2D::get().drawText( text, *font, mvpMatrix, TAH_LEFT, TAV_MIDDLE, Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ) );
	}
}