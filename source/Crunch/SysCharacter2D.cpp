#include "SysCharacter2D.h"
#include "SysRenderer2D.h"

SysCharacter2D::SysCharacter2D()
	: color( 1.0f, 1.0f, 1.0f, 1.0f ) {
}

void SysCharacter2D::setSkeleton( Skeleton2DResourceReference & skel ) {
	skeleton = skel;

	pixelMins.resize( skel->getVisiblePartCount() );
	pixelMaxes.resize( skel->getVisiblePartCount() );
	origins.resize( skel->getVisiblePartCount() );
	transforms.resize( skel->getPartCount() );
	drawnTransforms.resize( skel->getVisiblePartCount() );
	animationTransforms.resize( skel->getPartCount() );
	for (size_t i = 0; i < skel->getVisiblePartCount(); ++i) {
		size_t index = skel->getPartDrawingOrder( i );
		pixelMins[i] = (Vector2f)skel->getPartData( index ).imageMin;
		pixelMaxes[i] = (Vector2f)skel->getPartData( index ).imageMax;
		origins[i] = (Vector2f)skel->getPartData( index ).origin;
	}
}

void SysCharacter2D::setTexture( TextureResourceReference & tex ) {
	texture = tex;
}

void SysCharacter2D::setColor( const Vector4f & c ) {
	color = c;
}

void SysCharacter2D::startAnimation() {
	for (size_t i = 0; i < animationTransforms.size(); ++i) {
		animationTransforms[i].first.set( 0.0f, 0.0f );
		animationTransforms[i].second = 0.0f;
	}
}

static std::pair <size_t, size_t> getSurroundingKeyframes( Skeleton2DAnimationResource & anim, uint frame ) {
	struct KeyframeComparator {
		const Skeleton2DAnimationResource & anim;
		uint refFrame;

		int operator()( size_t index ) const {
			uint t = anim.getKeyframeData( index ).keyframeTime;
			// maps > to 1, == to 0, < to -1
			return (t > refFrame) - (t < refFrame);
		}

		KeyframeComparator( const Skeleton2DAnimationResource & a, uint rFrame )
			: anim( a )
			, refFrame( rFrame ) {
		}
	};

	std::pair <size_t, size_t> surroundingKeyframes;
	size_t index;
	if (binarySearch( 0, anim.getKeyframeCount(), index, KeyframeComparator( anim, frame ) ))
		surroundingKeyframes.first = surroundingKeyframes.second = index;
	else {
		if (index == 0)
			surroundingKeyframes.first = surroundingKeyframes.second = 0;
		else if (index == anim.getKeyframeCount())
			surroundingKeyframes.first = surroundingKeyframes.second = anim.getKeyframeCount() - 1;
		else {
			surroundingKeyframes.first = index - 1;
			surroundingKeyframes.second = index;
		}
	}

	return surroundingKeyframes;
}

static float getKeyframeRatio( Skeleton2DAnimationResource & anim, const std::pair <size_t, size_t> keyframes, uint frame ) {
	if (keyframes.first == keyframes.second)
		return 1.0f;
	else
		return
			(float)(frame - anim.getKeyframeData( keyframes.first ).keyframeTime) /
			(float)(anim.getKeyframeData( keyframes.second ).keyframeTime - anim.getKeyframeData( keyframes.first ).keyframeTime);
}

void SysCharacter2D::applyAnimation( Skeleton2DAnimationResource & anim, uint frame, bool clamp ) {
	if (anim.getKeyframeCount() == 0)
		return;

	if (!clamp && frame >= anim.getLength())
		frame %= anim.getLength();

	std::pair <size_t, size_t> surrKf = getSurroundingKeyframes( anim, frame );
	float ratio = getKeyframeRatio( anim, surrKf, frame );

	for (size_t i = 0; i < anim.getPartCount(); ++i) {
		const Skeleton2DAnimationResource::KeyframeData & kf0 = anim.getKeyframeData( surrKf.first );
		const Skeleton2DAnimationResource::KeyframeData & kf1 = anim.getKeyframeData( surrKf.second );

		size_t partId = anim.getPartId( i );
		animationTransforms[partId].first = lerp( kf0.parts[i].offset, kf1.parts[i].offset, ratio );
		animationTransforms[partId].second = lerp( kf0.parts[i].rotation, kf1.parts[i].rotation, ratio );
	}
}

void SysCharacter2D::blendAnimation( Skeleton2DAnimationResource & anim, uint frame, float blend, bool clamp ) {
	if (blend == 0.0f)
		return;
	if (blend == 1.0f) {
		applyAnimation( anim, frame, clamp );
		return;
	}

	if (anim.getKeyframeCount() == 0)
		return;

	if (!clamp && frame >= anim.getLength())
		frame %= anim.getLength();

	std::pair <size_t, size_t> surrKf = getSurroundingKeyframes( anim, frame );
	float ratio = getKeyframeRatio( anim, surrKf, frame );

	for (size_t i = 0; i < anim.getPartCount(); ++i) {
		const Skeleton2DAnimationResource::KeyframeData & kf0 = anim.getKeyframeData( surrKf.first );
		const Skeleton2DAnimationResource::KeyframeData & kf1 = anim.getKeyframeData( surrKf.second );

		size_t partId = anim.getPartId( i );
		animationTransforms[partId].first = lerp(
			animationTransforms[partId].first,
			lerp( kf0.parts[i].offset, kf1.parts[i].offset, ratio ),
			blend );
		animationTransforms[partId].second = lerp(
			animationTransforms[partId].second,
			lerp( kf0.parts[i].rotation, kf1.parts[i].rotation, ratio ),
			blend );
	}
}

void SysCharacter2D::endAnimation() {
	for (size_t i = 0; i < skeleton->getPartCount(); ++i) {
		Vector2f offset = skeleton->getPartData( i ).offset + animationTransforms[i].first;
		float rotation = skeleton->getPartData( i ).rotation + animationTransforms[i].second;

		transforms[i].identity();
		transforms[i].rotate( rotation );
		transforms[i].translate( offset );
	}

	if (skeleton->getPartCount() == 0)
		return;

	// part of <child ID, parent ID>
	std::stack <std::pair <size_t, size_t>> childParentStack;
	childParentStack.push( std::make_pair( 0, 0 ) );
	while (!childParentStack.empty()) {
		std::pair <size_t, size_t> childParent = childParentStack.top();
		childParentStack.pop();

		// multiply with parent matrix
		if (childParent.first != childParent.second)
			transforms[childParent.first] = transforms[childParent.second] * transforms[childParent.first];

		for (size_t c = 0; c < skeleton->getPartData( childParent.first ).children.size(); ++c)
			childParentStack.push( std::make_pair( skeleton->getPartData( childParent.first ).children[c], childParent.first ) );
	}

	for (size_t i = 0; i < drawnTransforms.size(); ++i)
		drawnTransforms[i] = transforms[skeleton->getPartDrawingOrder( i )];
}

void SysCharacter2D::draw( const Matrix44f & mvpMatrix ) {
	if (!texture || !skeleton || skeleton->getVisiblePartCount() == 0)
		return;

	SysRenderer2D::get().drawMultiSprite(
		*texture, mvpMatrix, color, skeleton->getVisiblePartCount(), &origins[0], &pixelMins[0], &pixelMaxes[0], &drawnTransforms[0] );
}