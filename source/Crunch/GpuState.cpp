#include "GpuState.h"

void GpuState::init() {
	// initialize all states to their current states
	blendState = createBlendState();
	blendState.getCurrentState();

	depthStencilState = createDepthStencilState();
	depthStencilState.getCurrentState();

	rasterizerState = createRasterizerState();
	rasterizerState.getCurrentState();

	scissorState = createScissorState();
	scissorState.getCurrentState();

	viewportState = createViewportState();
	viewportState.getCurrentState();

	GLint maxDrawBuffersResult;
	glGetIntegerv( GL_MAX_DRAW_BUFFERS, &maxDrawBuffersResult );
	maxDrawBuffers = std::min( (size_t)maxDrawBuffersResult, GpuDrawBufferState::MAX_DRAW_BUFFERS );
	drawBufferState = createDrawBufferState();
	drawBufferState.getCurrentState();

	GLint textureImageUnitCount;
	glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &textureImageUnitCount );
	textureImageUnits.resize( (size_t)textureImageUnitCount );
	for (size_t i = 0; i < textureImageUnits.size(); ++i)
		// implicit reference wrapping
		textureImageUnits[i] = new GpuTextureImageUnit( this, i );
	GLenum activeTextureImageUnitResult;
	glGetIntegerv( GL_ACTIVE_TEXTURE, (GLint*)&activeTextureImageUnitResult );
	activeTextureImageUnit = (size_t)(activeTextureImageUnitResult - GL_TEXTURE0);

	GLint maxColorAttachmentsResult;
	glGetIntegerv( GL_MAX_COLOR_ATTACHMENTS, &maxColorAttachmentsResult );
	maxColorAttachments = (size_t)maxColorAttachmentsResult;

	framebufferSRgbEnabled = glIsEnabled( GL_FRAMEBUFFER_SRGB ) != 0;

	GLint maxAttributesResult;
	glGetIntegerv( GL_MAX_VERTEX_ATTRIBS, &maxAttributesResult );
	attributes.resize( (size_t)maxAttributesResult );
	for (size_t i = 0; i < attributes.size(); ++i)
		// implicit reference wraping
		attributes[i] = new GpuAttribute( this, (uint)i );

	drawState = createDrawState();
	drawState.getCurrentState();
}

GpuBlendState GpuState::createBlendState() {
	return GpuBlendState( this );
}

void GpuState::bind( const GpuBlendState & newState ) {
	newState.apply( blendState );
}

const GpuBlendState & GpuState::getCurrentBlendState() const {
	return blendState;
}

GpuDepthStencilState GpuState::createDepthStencilState() {
	return GpuDepthStencilState( this );
}

void GpuState::bind( const GpuDepthStencilState & newState ) {
	newState.apply( depthStencilState );
}

const GpuDepthStencilState & GpuState::getCurrentDepthStencilState() const {
	return depthStencilState;
}

GpuRasterizerState GpuState::createRasterizerState() {
	return GpuRasterizerState( this );
}

void GpuState::bind( const GpuRasterizerState & newState ) {
	newState.apply( rasterizerState );
}

const GpuRasterizerState & GpuState::getCurrentRasterizerState() const {
	return rasterizerState;
}

GpuScissorState GpuState::createScissorState() {
	return GpuScissorState( this );
}

void GpuState::bind( const GpuScissorState & newState ) {
	newState.apply( scissorState );
}

const GpuScissorState & GpuState::getCurrentScissorState() const {
	return scissorState;
}

GpuViewportState GpuState::createViewportState() {
	return GpuViewportState( this );
}

void GpuState::bind( const GpuViewportState & newState ) {
	newState.apply( viewportState );
}

const GpuViewportState & GpuState::getCurrentViewportState() const {
	return viewportState;
}

size_t GpuState::getMaxDrawBuffers() const {
	return maxDrawBuffers;
}

GpuDrawBufferState GpuState::createDrawBufferState() {
	return GpuDrawBufferState( this );
}

void GpuState::bind( const GpuDrawBufferState & newState ) {
	newState.apply( drawBufferState );
}

const GpuDrawBufferState & GpuState::getCurrentDrawBufferState() const {
	return drawBufferState;
}

GpuTextureReference GpuState::createTexture( const GpuTexture::Texture1DDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::Texture1DArrayDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::Texture2DDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::Texture2DArrayDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::Texture2DMultisampleDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::Texture2DMultisampleArrayDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::Texture3DDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::TextureBufferDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::TextureCubeMapDescription & desc ) {
	return new GpuTexture( this, desc );
}

GpuTextureReference GpuState::createTexture( const GpuTexture::TextureRectangleDescription & desc ) {
	return new GpuTexture( this, desc );
}

size_t GpuState::getTextureImageUnitCount() const {
	return textureImageUnits.size();
}

void GpuState::setActiveTextureImageUnitIndex( size_t i ) {
	if (i != activeTextureImageUnit) {
		glActiveTexture( GL_TEXTURE0 + (GLuint)i );
		activeTextureImageUnit = i;
	}
}

size_t GpuState::getActiveTextureImageUnitIndex() const {
	return activeTextureImageUnit;
}

GpuTextureImageUnitReference GpuState::getTextureImageUnit( size_t i ) {
	return textureImageUnits[i];
}

GpuTextureImageUnitConstReference GpuState::getTextureImageUnit( size_t i ) const {
	return textureImageUnits[i];
}

GpuTextureImageUnitReference GpuState::getActiveTextureImageUnit() {
	return textureImageUnits[activeTextureImageUnit];
}

GpuTextureImageUnitConstReference GpuState::getActiveTextureImageUnit() const {
	return textureImageUnits[activeTextureImageUnit];
}

GpuRenderbufferReference GpuState::createRenderbuffer( const GpuRenderbuffer::RenderbufferDescription & desc ) {
	return new GpuRenderbuffer( this, desc );
}

void GpuState::bindRenderbuffer( const GpuRenderbufferReference & renderbuffer ) {
	if (renderbufferBinding.get() == renderbuffer.get())
		// already bound
		return;

	glBindRenderbuffer( GL_RENDERBUFFER, renderbuffer->getId() );
	renderbufferBinding = renderbuffer;
}

void GpuState::unbindRenderbuffer() {
	if (!renderbufferBinding.get())
		// already unbound
		return;

	glBindRenderbuffer( GL_RENDERBUFFER, 0 );
	renderbufferBinding = GpuRenderbufferReference();
}

GpuRenderbufferReference GpuState::getBoundRenderbuffer() {
	return renderbufferBinding;
}

GpuRenderbufferConstReference GpuState::getBoundRenderbuffer() const {
	return renderbufferBinding;
}

size_t GpuState::getMaxColorAttachments() const {
	return maxColorAttachments;
}

void GpuState::setFramebufferSRgbEnabled( bool e ) {
	GpuObject::setStateEnabled( e, framebufferSRgbEnabled, GL_FRAMEBUFFER_SRGB );
}

bool GpuState::isFramebufferSRgbEnabled() const {
	return framebufferSRgbEnabled;
}

GpuFramebufferReference GpuState::createFramebuffer() {
	return new GpuFramebuffer( this );
}

void GpuState::bindFramebuffer( GLenum location, const GpuFramebufferReference & framebuffer ) {
	switch (location) {
	case GL_DRAW_FRAMEBUFFER:
		if (drawFramebufferBinding.get() == framebuffer.get())
			// already bound
			return;

		glBindFramebuffer( location, framebuffer->getId() );
		drawFramebufferBinding = framebuffer;
		break;
	case GL_READ_FRAMEBUFFER:
		if (readFramebufferBinding.get() == framebuffer.get())
			// already bound
			return;

		glBindFramebuffer( location, framebuffer->getId() );
		readFramebufferBinding = framebuffer;
		break;
	case GL_FRAMEBUFFER:
		if (drawFramebufferBinding.get() == framebuffer.get() &&
			readFramebufferBinding.get() == framebuffer.get())
			// already bound
			return;

		glBindFramebuffer( location, framebuffer->getId() );
		drawFramebufferBinding = framebuffer;
		readFramebufferBinding = framebuffer;
		break;
	}
}

void GpuState::unbindFramebuffer( GLenum location ) {
	switch (location) {
	case GL_DRAW_FRAMEBUFFER:
		if (!drawFramebufferBinding.get())
			// already unbound
			return;

		glBindFramebuffer( location, 0 );
		drawFramebufferBinding = GpuFramebufferReference();
		break;
	case GL_READ_FRAMEBUFFER:
		if (!readFramebufferBinding.get())
			// already unbound
			return;

		glBindFramebuffer( location, 0 );
		drawFramebufferBinding = GpuFramebufferReference();
		break;
	case GL_FRAMEBUFFER:
		if (!drawFramebufferBinding.get() &&
			!readFramebufferBinding.get())
			// already unbound
			return;

		glBindFramebuffer( location, 0 );
		drawFramebufferBinding = GpuFramebufferReference();
		readFramebufferBinding = GpuFramebufferReference();
		break;
	}
}

GpuFramebufferReference GpuState::getBoundFramebuffer( GLenum location ) {
	if (location == GL_FRAMEBUFFER || location == GL_DRAW_FRAMEBUFFER)
		return drawFramebufferBinding;
	else if (location == GL_READ_FRAMEBUFFER)
		return readFramebufferBinding;
	else
		throw std::runtime_error( "Invalid framebuffer binding location" );
}

GpuFramebufferConstReference GpuState::getBoundFramebuffer( GLenum location ) const {
	if (location == GL_FRAMEBUFFER || location == GL_DRAW_FRAMEBUFFER)
		return drawFramebufferBinding;
	else if (location == GL_READ_FRAMEBUFFER)
		return readFramebufferBinding;
	else
		throw std::runtime_error( "Invalid framebuffer binding location" );
}

GpuBufferReference GpuState::createBuffer( const GpuBuffer::BufferDescription & desc ) {
	return new GpuBuffer( this, desc );
}

void GpuState::bindBuffer( const GpuBufferReference & buffer ) {
	bindBuffer( buffer->getType(), buffer );
}

void GpuState::bindBuffer( GLenum location, const GpuBufferReference & buffer ) {
	BufferBindingLocation loc = getBufferBindingLocation( location );
	if (bufferBindingLocations[loc].get() == buffer.get())
		// already bound
		return;

	glBindBuffer( location, buffer->getId() );
	bufferBindingLocations[loc] = buffer;
}

void GpuState::unbindBuffer( GLenum location ) {
	BufferBindingLocation loc = getBufferBindingLocation( location );
	if (!bufferBindingLocations[loc])
		// already unbound
		return;

	glBindBuffer( location, 0 );
	bufferBindingLocations[loc] = GpuBufferReference();
}

GpuBufferReference GpuState::getBoundBuffer( GLenum location ) {
	return bufferBindingLocations[getBufferBindingLocation( location )];
}

GpuBufferConstReference GpuState::getBoundBuffer( GLenum location ) const {
	return bufferBindingLocations[getBufferBindingLocation( location )];
}

GpuState::BufferBindingLocation GpuState::getBufferBindingLocation( GLenum loc ) {
	switch (loc) {
	case GL_ARRAY_BUFFER:
		return TBL_ARRAY_BUFFER;
	case GL_COPY_READ_BUFFER:
		return TBL_COPY_READ_BUFFER;
	case GL_COPY_WRITE_BUFFER:
		return TBL_COPY_WRITE_BUFFER;
	case GL_ELEMENT_ARRAY_BUFFER:
		return TBL_ELEMENT_ARRAY_BUFFER;
	case GL_PIXEL_PACK_BUFFER:
		return TBL_PIXEL_PACK_BUFFER;
	case GL_PIXEL_UNPACK_BUFFER:
		return TBL_PIXEL_UNPACK_BUFFER;
	case GL_TEXTURE_BUFFER:
		return TBL_TEXTURE_BUFFER;
	case GL_TRANSFORM_FEEDBACK_BUFFER:
		return TBL_TRANSFORM_FEEDBACK_BUFFER;
	case GL_UNIFORM_BUFFER:
		return TBL_UNIFORM_BUFFER;
	default:
		throw std::runtime_error( "Invalid buffer binding location" );
	}
}

GpuProgramReference GpuState::createProgram( const GpuProgram::ProgramDescription & desc ) {
	return new GpuProgram( this, desc );
}

void GpuState::bindProgram( const GpuProgramReference & program ) {
	if (programBinding.get() == program.get())
		// already bound
		return;

	glUseProgram( program->getProgramId() );
	if (programBinding)
		programBinding->onUnbind();
	programBinding = program;
}

void GpuState::unbindProgram() {
	if (!programBinding)
		// already unbound
		return;

	glUseProgram( 0 );
	if (programBinding)
		programBinding->onUnbind();
	programBinding = GpuProgramReference();
}

GpuProgramReference GpuState::getBoundProgram() {
	return programBinding;
}

GpuProgramConstReference GpuState::getBoundProgram() const {
	return programBinding;
}

size_t GpuState::getAttributeCount() const {
	return attributes.size();
}

GpuAttributeReference GpuState::getAttribute( size_t i ) {
	return attributes[i];
}

GpuAttributeConstReference GpuState::getAttribute( size_t i ) const {
	return attributes[i];
}

GpuDrawState GpuState::createDrawState() {
	return GpuDrawState( this );
}

void GpuState::bind( const GpuDrawState & newState ) {
	newState.apply( drawState );
}

const GpuDrawState & GpuState::getCurrentDrawState() {
	return drawState;
}

GpuDrawCall GpuState::createDrawCall() {
	return GpuDrawCall( this );
}

GpuInstancedDrawCall GpuState::createInstancedDrawCall() {
	return GpuInstancedDrawCall( this );
}

GpuIndexedDrawCall GpuState::createIndexedDrawCall() {
	return GpuIndexedDrawCall( this );
}

GpuIndexedOffsetDrawCall GpuState::createIndexedOffsetDrawCall() {
	return GpuIndexedOffsetDrawCall( this );
}

GpuIndexedInstancedDrawCall GpuState::createIndexedInstancedDrawCall() {
	return GpuIndexedInstancedDrawCall( this );
}

GpuIndexedInstancedOffsetDrawCall GpuState::createIndexedInstancedOffsetDrawCall() {
	return GpuIndexedInstancedOffsetDrawCall( this );
}

GpuIndexedRangedDrawCall GpuState::createIndexedRangedDrawCall() {
	return GpuIndexedRangedDrawCall( this );
}

GpuIndexedRangedOffsetDrawCall GpuState::createIndexedRangedOffsetDrawCall() {
	return GpuIndexedRangedOffsetDrawCall( this );
}

GpuClearCall GpuState::createClearCall() {
	return GpuClearCall( this );
}