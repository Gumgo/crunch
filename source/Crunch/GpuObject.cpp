#include "GpuObject.h"
#include "GpuState.h"

GpuObject::GpuObject( GpuState * state )
	: gpuState( state ) {
}

GpuState * GpuObject::getGpuState() {
	return gpuState;
}

const GpuState * GpuObject::getGpuState() const {
	return gpuState;
}

void GpuObject::setStateEnabled( bool newState, bool & currentState, GLenum stateName ) {
	if (newState && !currentState) {
		glEnable( stateName );
		currentState = true;
	} else if (!newState && currentState) {
		glDisable( stateName );
		currentState = false;
	}
}

ReferenceCountedGpuObject::ReferenceCountedGpuObject( GpuState * state )
	: GpuObject( state ) {
	referenceCount = 0;
}

size_t ReferenceCountedGpuObject::getReferenceCount() const {
	return referenceCount;
}

void ReferenceCountedGpuObject::acquire() const {
	++referenceCount;
}

void ReferenceCountedGpuObject::release() const {
	--referenceCount;
	if (referenceCount == 0)
		onRelease();
}