#include "SphereDiskDistribution.h"

SphereDiskDistribution::SphereDiskDistribution( size_t diskCount, float areaVariation, size_t iterations, uint seed ) {
	disks.reserve( diskCount );
	forces.reserve( diskCount );

	//seedRng( seed );

	// generate the initial disks
	for (size_t i = 0; i < diskCount; ++i) {
		float theta = /*random() **/ 2.0f * boost::math::constants::pi <float>();
		float cosTheta = cos( theta );
		float sinTheta = sin( theta );
		float cosPhi = /*random() **/ 2.0f - 1.0f;
		float sinPhi = sqrt( 1.0f - cosPhi*cosPhi );

		Disk d;
		d.position = Vector3f( cosTheta*sinPhi, sinTheta*sinPhi, cosPhi );
		// generate an area for the disk with some variation - temporarily store as radius
		d.radius = 1.0f + /*random()**/areaVariation;
		disks.push_back( d );
	}

	// normalize the areas
	float totalArea = 0.0f;
	for (size_t i = 0; i < disks.size(); ++i)
		totalArea += disks[i].radius;
	
	// the total area should sum to the area of the unit sphere, which is 4*pi
	float areaNormalizer = 4.0f * boost::math::constants::pi <float>() / totalArea;
	for (size_t i = 0; i < disks.size(); ++i) {
		Disk & disk = disks[i];
		disk.radius *= areaNormalizer;
		// now we compute the radius from this
		disk.radius = sqrt( disk.radius / boost::math::constants::pi <float>() );
	}

	// now iterate
	for (size_t i = 0; i < iterations; ++i)
		iterate();

	// clear the extra memory
	forces.clear();
	forces.shrink_to_fit();
}

void SphereDiskDistribution::iterate() {
	for (size_t i = 0; i < forces.size(); ++i)
		forces.push_back( 0/*???*/ );

	for (size_t i = 0; i < disks.size(); ++i) {
		for (size_t t = i+1; t < disks.size(); ++t) {
			// examine each pair of disks
			const Disk & d0 = disks[i];
			const Disk & d1 = disks[t];

			// since this is a unit sphere, the angle in radians
			// between two disk centers is also the distance across the sphere
			float distance = acos( d0.position.dot( d1.position ) );
			// find the amount the disks overlap
			float overlap = distance - 0.5f*(d0.radius + d1.radius);

			// apply some force based on the overlap ...
		}
	}

	// last, update the positions
	for (size_t i = 0; i < disks.size(); ++i) {
		//disks[i].position ??? forces[i];
		//...
	}

	forces.clear();
}

const std::vector <SphereDiskDistribution::Disk> & SphereDiskDistribution::getDisks() const {
	return disks;
}