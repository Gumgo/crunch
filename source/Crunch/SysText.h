#ifndef SYSTEXT_DEFINED
#define SYSTEXT_DEFINED

#include "Common.h"
#include "GpuBuffer.h"
#include "FontResource.h"
#include "TextUtil.h"
#include <vector>

class SysText {
public:
	struct Vertex {
		Vector2f position;
		Vector2f uv;
	};

	SysText();
	void setMaxLength( size_t l );
	void setFont( FontResourceReference & f );
	void setAlign( TextAlignHoriz h, TextAlignVert v );
	void setText( const std::string & t );
	void setText( const std::string & t, TextAlignHoriz h, TextAlignVert v );

	size_t getMaxLength() const;
	FontResourceReference getFont() const;
	TextAlignHoriz getHorizAlign() const;
	TextAlignVert getVertAlign() const;
	const std::string & getText() const;
	size_t getVertexCount() const;
	GpuBufferReference getVertices();
	GpuBufferConstReference getVertices() const;

private:
	FontResourceReference font;
	std::string text;
	TextAlignHoriz horizAlign;
	TextAlignVert vertAlign;
	size_t maxLength;

	size_t vertexCount; // not text.length()*6, because characters like \n don't use vertices
	std::vector <Vertex> vertices;
	GpuBufferReference gpuVertices;
};

#endif