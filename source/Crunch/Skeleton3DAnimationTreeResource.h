/** @file Skeleton3DAnimationTreeResource.h
 *  @brief Contains the resource holding 3D skeleton animation tree data.
 */

#ifndef SKELETON3DANIMATIONTREERESOURCE_DEFINED
#define SKELETON3DANIMATIONTREERESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "HashMap.h"
#include "HashFunctions.h"
#include <vector>
#include <queue>

/** @brief Skeleton 3D animation tree resource.
 */
DEFINE_RESOURCE_CLASS( Skeleton3DAnimationTreeResource, "skeleton3DAnimationTree", "skeleton3DAnimationTrees" )
public:
	/** @brief The constructor.
	 */
	Skeleton3DAnimationTreeResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief An animation node in the animation tree.
	 *
	 *  An animation node contains an animation to be applied.
	 */
	class AnimationNode {
	public:
		/** @brief Returns the index of the animation in the list of animations.
		 */
		size_t getAnimationIndex() const;

	private:
		friend class Skeleton3DAnimationTreeResource;
		size_t animationIndex;	// index of the animation in the list of animations
	};

	/** @brief A select node in the animation tree.
	 *
	 *  A select node selects between one of its children and only evaluates
	 *  that child's branch, ignoring all others.
	 */
	class SelectNode {
	public:
		/** @brief Returns the number of child nodes.
		 */
		size_t getChildCount() const;

		/** @brief Returns the index of the given child node.
		 */
		size_t getChildIndex( size_t i ) const;

	private:
		friend class Skeleton3DAnimationTreeResource;
		size_t childCount;	// number of children
		size_t firstChild;	// index of first child - other child indices are consecutive
	};

	/** @brief A blend node in the animation tree.
	 *
	 *  A blend node has at least two children and a single interpolation value.
	 *  Values between 0.0 and 1.0 blend between children 0 and 1, values between
	 *  1.0 and 2.0 blend between children 1 and 2, etc.
	 */
	class BlendNode {
	public:
		/** @brief Returns whether blending should wrap.
		 */
		bool getWrap() const;

		/** @brief Returns the number of child nodes.
		 */
		size_t getChildCount() const;

		/** @brief Returns the index of the given child node.
		 */
		size_t getChildIndex( size_t i ) const;

	private:
		friend class Skeleton3DAnimationTreeResource;
		bool wrap;			// whether blending should wrap between the last and first node
		size_t childCount;	// number of children
		size_t firstChild;	// index of first child - other child indices are consecutive
	};

	/** @brief A sequence node in the animation tree.
	 *
	 *  A sequence node evaluates each child node in sequence.
	 *  Child nodes are weighted.
	 */
	class SequenceNode {
	public:
		/** @brief Returns the number of child nodes.
		 */
		size_t getChildCount() const;

		/** @brief Returns the index of the given child node.
		 */
		size_t getChildIndex( size_t i ) const;

	private:
		friend class Skeleton3DAnimationTreeResource;
		size_t childCount;	// number of children
		size_t firstChild;	// index of first child - other child indices are consecutive
	};

	/** @brief A node in the animation tree.
	 */
	class Node {
	public:
		/** @brief The node type.
		 */
		enum Type {
			T_ANIMATION,	/**< Contains an animation.*/
			T_SELECT,		/**< Selects one of multiple child nodes.*/
			T_BLEND,		/**< Blends between two child nodes.*/
			T_SEQUENCE		/**< Applies child nodes in sequence, where children are weighted.*/
		};

		/** @brief Returns the node type.
		 */
		Type getType() const;

		/** @brief Returns the contained animation node, or throws an exception if this node is of another type.
		 */
		const AnimationNode & getAnimationNode() const;

		/** @brief Returns the contained select node, or throws an exception if this node is of another type.
		 */
		const SelectNode & getSelectNode() const;

		/** @brief Returns the contained blend node, or throws an exception if this node is of another type.
		 */
		const BlendNode & getBlendNode() const;

		/** @brief Returns the contained sequence node, or throws an exception if this node is of another type.
		 */
		const SequenceNode & getSequenceNode() const;

	private:
		friend class Skeleton3DAnimationTreeResource;
		// constructors for each node type
		Node( const AnimationNode & n );
		Node( const SelectNode & n );
		Node( const BlendNode & n );
		Node( const SequenceNode & n );

		// the node type
		Type type;

		// union of data for each node type
		union {
			AnimationNode animationNode;
			SelectNode selectNode;
			BlendNode blendNode;
			SequenceNode sequenceNode;
		};
	};

	/** @brief Returns the number of nodes in the tree.
	 */
	size_t getNodeCount() const;

	/** @brief Returns the node with the given index.
	 */
	const Node & getNode( size_t i ) const;

	/** @brief Returns the index of the node with the given name.
	 */
	bool getNodeIndex( const std::string & name, size_t & index ) const;

	/** @brief Returns the number of animations referenced by the tree.
	 */
	size_t getAnimationCount() const;

	/** @brief Returns the name of the animation with the given index.
	 */
	const std::string & getAnimation( size_t i ) const;

private:
	// nodes in the tree
	std::vector <Node> nodes;

	// maps node names to indices
	HashMap <Hash <std::string>, std::string, size_t> nodeNameMap;

	// animations referenced by nodes
	std::vector <std::string> animations;
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <Skeleton3DAnimationTreeResource> Skeleton3DAnimationTreeResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <Skeleton3DAnimationTreeResource> Skeleton3DAnimationTreeResourceConstReference;

#endif