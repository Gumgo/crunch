/** @file Maths_Vector3Dec.h
 *  @brief Contains an implementation of a 3D vector class.
 */

#ifndef MATHS_VECTOR3DEC_DEFINED
#define MATHS_VECTOR3DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for 3D vectors.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Vector3 {
public:
	union {
		struct {
			T x;	/**< The x component.*/
			T y;	/**< The y component.*/
			T z;	/**< The z component.*/
		};
		struct {
			T r;	/**< The r component.*/
			T g;	/**< The g component.*/
			T b;	/**< The b component.*/
		};
	};

	/** @brief Constructs a 3D vector with all components set to 0.
	 */
	Vector3();

	/** @brief Constructs a 3D vector with the provided components.
	 */
	Vector3( T px, T py, T pz );

	/** @brief Constructs a 3D vector by copying the one provided.
	 */
	Vector3( const Vector3 <T> & v );

	/** @brief Constructs a 3D vector from a 2D vector and a scalar.
	 */
	Vector3( const Vector2 <T> & pxy, T pz );

	/** @brief Constructs a 3D vector from a scalar and a 2D vector.
	 */
	Vector3( T px, const Vector2 <T> & pyz );

	/** @brief The destructor.
	 */
	~Vector3();

	// operators

	/** @brief Returns the sum of the current value and the vector v.
	 */
	Vector3 <T> operator+( const Vector3 <T> & v ) const;

	/** @brief Returns the current value unmodified.
	 */
	Vector3 <T> operator+() const;

	/** @brief Returns the difference between the current value and the vector v.
	 */
	Vector3 <T> operator-( const Vector3 <T> & v ) const;

	/** @brief Returns the current value negated.
	 */
	Vector3 <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Vector3 <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Vector3 <T> operator/( T r ) const;

	/** @brief Returns a vector containing the product of each component
	 *  in the current value and the corresponding component in the vector v.
	 */
	Vector3 <T> operator*( const Vector3 <T> & v ) const;

	/** @brief Returns a vector containing the quotient of each component
	 *  in the current value and the corresponding component in the vector v.
	 */
	Vector3 <T> operator/( const Vector3 <T> & v ) const;

	/** @brief Sets the current value to the vector v.
	 */
	Vector3 <T> operator=( const Vector3 <T> & v );

	/** @brief Increments the current value by the vector v.
	 */
	Vector3 <T> operator+=( const Vector3 <T> & v );

	/** @brief Decrements the current value by the vector v.
	 */
	Vector3 <T> operator-=( const Vector3 <T> & v );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Vector3 <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Vector3 <T> operator/=( T r );

	/** @brief Multiplies each component in the current value
	 *  by the corresponding component in the vector v.
	 */
	Vector3 <T> operator*=( const Vector3 <T> & v );

	/** @brief Divides each component in the current value
	 *  by the corresponding component in the vector v.
	 */
	Vector3 <T> operator/=( const Vector3 <T> & v );

	/** @brief Returns whether the current value is equal to the vector v.
	 */
	bool operator==( const Vector3 <T> & v ) const;

	/** @brief Returns whether the current value is not equal to the vector v.
	 */
	bool operator!=( const Vector3 <T> & v ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Vector3 <U> () const;

	// mutable

	/** @brief Sets the current value's components to (px, py, pz).
	 */
	void set( T px, T py, T pz );

	/** @brief Normalizes the current value.
	 */
	void normalize();

	/** @brief Sets the magnitude of the current value to the scalar m.
	 */
	void setMagnitude( T m );

	// immutable

	/** @brief Returns the current value normalized.
	 */
	Vector3 <T> normalized() const;

	/** @brief Returns the magnitude of the current value.
	 */
	T magnitude() const;

	/** @brief Returns the squared magnitude of the current value.
	 */
	T magnitudeSquared() const;

	/** @brief Returns the dot product of the current value and the vector v.
	 */
	T dot( const Vector3 <T> & v ) const;

	/** @brief Returns the cross product of the current value and the vector v.
	 */
	Vector3 <T> cross( const Vector3 <T> & v ) const;

	/** @brief Returns the current value rotated by \htmlonly90&#0176\endhtmlonly.
	 */
	bool isZero() const;

	// swizzling
	Vector4 <T> xxxx() const;	/**< Returns a vector with components of (x, x, x, x).*/
	Vector4 <T> xxxy() const;	/**< Returns a vector with components of (x, x, x, y).*/
	Vector4 <T> xxxz() const;	/**< Returns a vector with components of (x, x, x, z).*/
	Vector4 <T> xxyx() const;	/**< Returns a vector with components of (x, x, y, x).*/
	Vector4 <T> xxyy() const;	/**< Returns a vector with components of (x, x, y, y).*/
	Vector4 <T> xxyz() const;	/**< Returns a vector with components of (x, x, y, z).*/
	Vector4 <T> xxzx() const;	/**< Returns a vector with components of (x, x, z, x).*/
	Vector4 <T> xxzy() const;	/**< Returns a vector with components of (x, x, z, y).*/
	Vector4 <T> xxzz() const;	/**< Returns a vector with components of (x, x, z, z).*/
	Vector4 <T> xyxx() const;	/**< Returns a vector with components of (x, y, x, x).*/
	Vector4 <T> xyxy() const;	/**< Returns a vector with components of (x, y, x, y).*/
	Vector4 <T> xyxz() const;	/**< Returns a vector with components of (x, y, x, z).*/
	Vector4 <T> xyyx() const;	/**< Returns a vector with components of (x, y, y, x).*/
	Vector4 <T> xyyy() const;	/**< Returns a vector with components of (x, y, y, y).*/
	Vector4 <T> xyyz() const;	/**< Returns a vector with components of (x, y, y, z).*/
	Vector4 <T> xyzx() const;	/**< Returns a vector with components of (x, y, z, x).*/
	Vector4 <T> xyzy() const;	/**< Returns a vector with components of (x, y, z, y).*/
	Vector4 <T> xyzz() const;	/**< Returns a vector with components of (x, y, z, z).*/
	Vector4 <T> xzxx() const;	/**< Returns a vector with components of (x, z, x, x).*/
	Vector4 <T> xzxy() const;	/**< Returns a vector with components of (x, z, x, y).*/
	Vector4 <T> xzxz() const;	/**< Returns a vector with components of (x, z, x, z).*/
	Vector4 <T> xzyx() const;	/**< Returns a vector with components of (x, z, y, x).*/
	Vector4 <T> xzyy() const;	/**< Returns a vector with components of (x, z, y, y).*/
	Vector4 <T> xzyz() const;	/**< Returns a vector with components of (x, z, y, z).*/
	Vector4 <T> xzzx() const;	/**< Returns a vector with components of (x, z, z, x).*/
	Vector4 <T> xzzy() const;	/**< Returns a vector with components of (x, z, z, y).*/
	Vector4 <T> xzzz() const;	/**< Returns a vector with components of (x, z, z, z).*/
	Vector4 <T> yxxx() const;	/**< Returns a vector with components of (y, x, x, x).*/
	Vector4 <T> yxxy() const;	/**< Returns a vector with components of (y, x, x, y).*/
	Vector4 <T> yxxz() const;	/**< Returns a vector with components of (y, x, x, z).*/
	Vector4 <T> yxyx() const;	/**< Returns a vector with components of (y, x, y, x).*/
	Vector4 <T> yxyy() const;	/**< Returns a vector with components of (y, x, y, y).*/
	Vector4 <T> yxyz() const;	/**< Returns a vector with components of (y, x, y, z).*/
	Vector4 <T> yxzx() const;	/**< Returns a vector with components of (y, x, z, x).*/
	Vector4 <T> yxzy() const;	/**< Returns a vector with components of (y, x, z, y).*/
	Vector4 <T> yxzz() const;	/**< Returns a vector with components of (y, x, z, z).*/
	Vector4 <T> yyxx() const;	/**< Returns a vector with components of (y, y, x, x).*/
	Vector4 <T> yyxy() const;	/**< Returns a vector with components of (y, y, x, y).*/
	Vector4 <T> yyxz() const;	/**< Returns a vector with components of (y, y, x, z).*/
	Vector4 <T> yyyx() const;	/**< Returns a vector with components of (y, y, y, x).*/
	Vector4 <T> yyyy() const;	/**< Returns a vector with components of (y, y, y, y).*/
	Vector4 <T> yyyz() const;	/**< Returns a vector with components of (y, y, y, z).*/
	Vector4 <T> yyzx() const;	/**< Returns a vector with components of (y, y, z, x).*/
	Vector4 <T> yyzy() const;	/**< Returns a vector with components of (y, y, z, y).*/
	Vector4 <T> yyzz() const;	/**< Returns a vector with components of (y, y, z, z).*/
	Vector4 <T> yzxx() const;	/**< Returns a vector with components of (y, z, x, x).*/
	Vector4 <T> yzxy() const;	/**< Returns a vector with components of (y, z, x, y).*/
	Vector4 <T> yzxz() const;	/**< Returns a vector with components of (y, z, x, z).*/
	Vector4 <T> yzyx() const;	/**< Returns a vector with components of (y, z, y, x).*/
	Vector4 <T> yzyy() const;	/**< Returns a vector with components of (y, z, y, y).*/
	Vector4 <T> yzyz() const;	/**< Returns a vector with components of (y, z, y, z).*/
	Vector4 <T> yzzx() const;	/**< Returns a vector with components of (y, z, z, x).*/
	Vector4 <T> yzzy() const;	/**< Returns a vector with components of (y, z, z, y).*/
	Vector4 <T> yzzz() const;	/**< Returns a vector with components of (y, z, z, z).*/
	Vector4 <T> zxxx() const;	/**< Returns a vector with components of (z, x, x, x).*/
	Vector4 <T> zxxy() const;	/**< Returns a vector with components of (z, x, x, y).*/
	Vector4 <T> zxxz() const;	/**< Returns a vector with components of (z, x, x, z).*/
	Vector4 <T> zxyx() const;	/**< Returns a vector with components of (z, x, y, x).*/
	Vector4 <T> zxyy() const;	/**< Returns a vector with components of (z, x, y, y).*/
	Vector4 <T> zxyz() const;	/**< Returns a vector with components of (z, x, y, z).*/
	Vector4 <T> zxzx() const;	/**< Returns a vector with components of (z, x, z, x).*/
	Vector4 <T> zxzy() const;	/**< Returns a vector with components of (z, x, z, y).*/
	Vector4 <T> zxzz() const;	/**< Returns a vector with components of (z, x, z, z).*/
	Vector4 <T> zyxx() const;	/**< Returns a vector with components of (z, y, x, x).*/
	Vector4 <T> zyxy() const;	/**< Returns a vector with components of (z, y, x, y).*/
	Vector4 <T> zyxz() const;	/**< Returns a vector with components of (z, y, x, z).*/
	Vector4 <T> zyyx() const;	/**< Returns a vector with components of (z, y, y, x).*/
	Vector4 <T> zyyy() const;	/**< Returns a vector with components of (z, y, y, y).*/
	Vector4 <T> zyyz() const;	/**< Returns a vector with components of (z, y, y, z).*/
	Vector4 <T> zyzx() const;	/**< Returns a vector with components of (z, y, z, x).*/
	Vector4 <T> zyzy() const;	/**< Returns a vector with components of (z, y, z, y).*/
	Vector4 <T> zyzz() const;	/**< Returns a vector with components of (z, y, z, z).*/
	Vector4 <T> zzxx() const;	/**< Returns a vector with components of (z, z, x, x).*/
	Vector4 <T> zzxy() const;	/**< Returns a vector with components of (z, z, x, y).*/
	Vector4 <T> zzxz() const;	/**< Returns a vector with components of (z, z, x, z).*/
	Vector4 <T> zzyx() const;	/**< Returns a vector with components of (z, z, y, x).*/
	Vector4 <T> zzyy() const;	/**< Returns a vector with components of (z, z, y, y).*/
	Vector4 <T> zzyz() const;	/**< Returns a vector with components of (z, z, y, z).*/
	Vector4 <T> zzzx() const;	/**< Returns a vector with components of (z, z, z, x).*/
	Vector4 <T> zzzy() const;	/**< Returns a vector with components of (z, z, z, y).*/
	Vector4 <T> zzzz() const;	/**< Returns a vector with components of (z, z, z, z).*/
	Vector3 <T> xxx() const;	/**< Returns a vector with components of (x, x, x).*/
	Vector3 <T> xxy() const;	/**< Returns a vector with components of (x, x, y).*/
	Vector3 <T> xxz() const;	/**< Returns a vector with components of (x, x, z).*/
	Vector3 <T> xyx() const;	/**< Returns a vector with components of (x, y, x).*/
	Vector3 <T> xyy() const;	/**< Returns a vector with components of (x, y, y).*/
	Vector3 <T> xyz() const;	/**< Returns a vector with components of (x, y, z).*/
	Vector3 <T> xzx() const;	/**< Returns a vector with components of (x, z, x).*/
	Vector3 <T> xzy() const;	/**< Returns a vector with components of (x, z, y).*/
	Vector3 <T> xzz() const;	/**< Returns a vector with components of (x, z, z).*/
	Vector3 <T> yxx() const;	/**< Returns a vector with components of (y, x, x).*/
	Vector3 <T> yxy() const;	/**< Returns a vector with components of (y, x, y).*/
	Vector3 <T> yxz() const;	/**< Returns a vector with components of (y, x, z).*/
	Vector3 <T> yyx() const;	/**< Returns a vector with components of (y, y, x).*/
	Vector3 <T> yyy() const;	/**< Returns a vector with components of (y, y, y).*/
	Vector3 <T> yyz() const;	/**< Returns a vector with components of (y, y, z).*/
	Vector3 <T> yzx() const;	/**< Returns a vector with components of (y, z, x).*/
	Vector3 <T> yzy() const;	/**< Returns a vector with components of (y, z, y).*/
	Vector3 <T> yzz() const;	/**< Returns a vector with components of (y, z, z).*/
	Vector3 <T> zxx() const;	/**< Returns a vector with components of (z, x, x).*/
	Vector3 <T> zxy() const;	/**< Returns a vector with components of (z, x, y).*/
	Vector3 <T> zxz() const;	/**< Returns a vector with components of (z, x, z).*/
	Vector3 <T> zyx() const;	/**< Returns a vector with components of (z, y, x).*/
	Vector3 <T> zyy() const;	/**< Returns a vector with components of (z, y, y).*/
	Vector3 <T> zyz() const;	/**< Returns a vector with components of (z, y, z).*/
	Vector3 <T> zzx() const;	/**< Returns a vector with components of (z, z, x).*/
	Vector3 <T> zzy() const;	/**< Returns a vector with components of (z, z, y).*/
	Vector3 <T> zzz() const;	/**< Returns a vector with components of (z, z, z).*/
	Vector2 <T> xx() const;		/**< Returns a vector with components of (x, x).*/
	Vector2 <T> xy() const;		/**< Returns a vector with components of (x, y).*/
	Vector2 <T> xz() const;		/**< Returns a vector with components of (x, z).*/
	Vector2 <T> yx() const;		/**< Returns a vector with components of (y, x).*/
	Vector2 <T> yy() const;		/**< Returns a vector with components of (y, y).*/
	Vector2 <T> yz() const;		/**< Returns a vector with components of (y, z).*/
	Vector2 <T> zx() const;		/**< Returns a vector with components of (z, x).*/
	Vector2 <T> zy() const;		/**< Returns a vector with components of (z, y).*/
	Vector2 <T> zz() const;		/**< Returns a vector with components of (z, z).*/
};

/** @brief Returns the product of the scalar r and the vector v.
 */
template <typename T> Vector3 <T> operator*( T r, const Vector3 <T> & v );

/** @brief Returns a vector containing the max of each corresponding component in vectors v1 and v2.
 */
template <typename T> Vector3 <T> vecMax( const Vector3 <T> & v1, const Vector3 <T> & v2 );

/** @brief Returns a vector containing the min of each corresponding component in vectors v1 and v2.
 */
template <typename T> Vector3 <T> vecMin( const Vector3 <T> & v1, const Vector3 <T> & v2 );

/** @brief Vector3 of type float.
 */
typedef Vector3 <float> Vector3f;

/** @brief Vector3 of type float.
 */
typedef Vector3 <double> Vector3d;

/** @brief Vector3 of type int.
 */
typedef Vector3 <int> Vector3i;

#endif