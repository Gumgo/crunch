#include "SysText.h"
#include "Context.h"

SysText::SysText() {
	maxLength = 0;
	horizAlign = TAH_LEFT;
	vertAlign = TAV_TOP;
	vertexCount = 0;
}

void SysText::setMaxLength( size_t l ) {
	if (l == 0 || l == maxLength)
		return;

	maxLength = l;
	vertices.resize( l*6 );

	GpuBuffer::BufferDescription desc;
	desc.type = GL_ARRAY_BUFFER;
	desc.size = l * 6 * sizeof( Vertex );
	desc.usage = GL_DYNAMIC_DRAW;
	gpuVertices = Context::get().gpuState.createBuffer( desc );
}

void SysText::setFont( FontResourceReference & f ) {
	font = f;
}

void SysText::setAlign( TextAlignHoriz h, TextAlignVert v ) {
	bool reset = (h != horizAlign || v != vertAlign);
	horizAlign = h;
	vertAlign = v;
	if (reset)
		setText( text );
}

void SysText::setText( const std::string & t ) {
	if (maxLength == 0 || !font)
		return;
	text = t;

	Vector2i dimensions = getTextDimens( *font, text );

	size_t characters = 0;
	int currentWidth = 0;
	int currentHeight = 0;
	int lineWidth = getLineWidth( *font, text, 0 );
	float lineHeightF = (float)font->getLineHeight();

	float wSubtract;
	if (horizAlign == TAH_LEFT)
		wSubtract = 0;
	else if (horizAlign == TAH_CENTER)
		wSubtract = (float)(lineWidth >> 1);
	else if (horizAlign == TAH_RIGHT)
		wSubtract = (float)lineWidth;

	float hSubtract;
	if (vertAlign == TAV_TOP)
		hSubtract = 0;
	if (vertAlign == TAV_MIDDLE)
		hSubtract = (float)(dimensions.y >> 1);
	else if (vertAlign == TAV_BOTTOM)
		hSubtract = (float)dimensions.y;

	size_t i = 0;
	vertexCount = 0;
	while (i < text.length() && characters < maxLength) {
		char ch = text[i];
		++i;

		if (ch == '\n') {
			currentWidth = 0;
			currentHeight += font->getLineHeight();
			lineWidth = getLineWidth( *font, text, i );

			if (horizAlign == TAH_LEFT)
				wSubtract = 0;
			else if (horizAlign == TAH_CENTER)
				wSubtract = (float)(lineWidth >> 1);
			else if (horizAlign == TAH_RIGHT)
				wSubtract = (float)lineWidth;
			if (vertAlign == TAV_TOP)
				hSubtract = 0;
			if (vertAlign == TAV_MIDDLE)
				hSubtract = (float)(dimensions.y >> 1);
			else if (vertAlign == TAV_BOTTOM)
				hSubtract = (float)dimensions.y;
		} else {
			const FontResource::Glyph & g = font->getGlyph( font->getGlyphIndex( ch ) );
			float wF = (float)currentWidth;
			float hF = (float)currentHeight;
			float aF = (float)g.advance;

			wF -= wSubtract;
			hF -= hSubtract;

			vertices[vertexCount  ].position.set( wF, hF );
			vertices[vertexCount+1].position.set( wF + aF, hF );
			vertices[vertexCount+2].position.set( wF, hF + lineHeightF );
			vertices[vertexCount+3].position.set( wF + aF, hF );
			vertices[vertexCount+4].position.set( wF + aF, hF + lineHeightF );
			vertices[vertexCount+5].position.set( wF, hF + lineHeightF );

			vertices[vertexCount  ].uv.set( g.x1, g.y1 );
			vertices[vertexCount+1].uv.set( g.x2, g.y1 );
			vertices[vertexCount+2].uv.set( g.x1, g.y1 + font->getTextureLineHeight() );
			vertices[vertexCount+3].uv.set( g.x2, g.y1 );
			vertices[vertexCount+4].uv.set( g.x2, g.y1 + font->getTextureLineHeight() );
			vertices[vertexCount+5].uv.set( g.x1, g.y1 + font->getTextureLineHeight() );

			++characters;
			currentWidth += g.advance;
			vertexCount += 6;
		}
	}

	gpuVertices->orphan();
	gpuVertices->write( 0, vertexCount * sizeof( Vertex ), &vertices[0] );
}

void SysText::setText( const std::string & t, TextAlignHoriz h, TextAlignVert v ) {
	bool reset = (h != horizAlign || v != vertAlign || t != text);
	horizAlign = h;
	vertAlign = v;
	if (reset)
		setText( t );
}

size_t SysText::getMaxLength() const {
	return maxLength;
}

FontResourceReference SysText::getFont() const {
	return font;
}

TextAlignHoriz SysText::getHorizAlign() const {
	return horizAlign;
}

TextAlignVert SysText::getVertAlign() const {
	return vertAlign;
}

const std::string & SysText::getText() const {
	return text;
}

size_t SysText::getVertexCount() const {
	return vertexCount;
}

GpuBufferReference SysText::getVertices() {
	return gpuVertices;
}

GpuBufferConstReference SysText::getVertices() const {
	return gpuVertices;
}