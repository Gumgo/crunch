/** @file Runner.h
 *  @brief Initializes the main loop.
 */

#ifndef RUNNER_DEFINED
#define RUNNER_DEFINED

#include "Common.h"
#include "KeyValueReader.h"
#include "Context.h"
#include "Timer.h"

#include <vector>

/** @brief Runs the game by initializing the main loop.
 */
class Runner {
public:
	/** @brief Runs the game.
	 */
	Runner( const std::vector <std::string> & arguments, Context::RegistrationFunction registrationFunction );
};

#endif