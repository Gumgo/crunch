/** @file Physics2DCircleShape.h
 *  @brief Defines circle shapes.
 */
#ifndef PHYSICS2DCIRCLESHAPE_DEFINED
#define PHYSICS2DCIRCLESHAPE_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DShape.h"

namespace Physics2D {

class Body;
struct CircleShapeDefinition;

/** @brief A circle shape.
 */
class CircleShape : public Shape {
public:
	CircleShape( const CircleShapeDefinition & d, Body * par );	/**< The constructor.*/

	/** @brief Returns the bounding box of the transformed shape.
	 */
	Aabb getAabb( const Matrix33ar & transformation ) const;

	/** @brief Computes the mass data of the shape.
	 */
	void computeMassData();

	void setRadius( Real rad );	/**< Sets the radius.*/
	Real getRadius() const;		/**< Returns the radius.*/

private:
	Real radius; // the radius

	Real computeArea() const;
};

/** @brief Defines circle shapes.
 */
struct CircleShapeDefinition : public ShapeDefinition {
	Real radius;	/**< The radius.*/

	CircleShapeDefinition();					/**< Assigns default values.*/
	Shape * createShape( Body * parent ) const;	/**< Creates a circle shape from the definition.*/
};

}

#endif