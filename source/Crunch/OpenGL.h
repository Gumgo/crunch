/** @file OpenGL.h
 *  @brief Includes OpenGL headers.
 */

#ifndef OPENGL_DEFINED
#define OPENGL_DEFINED

#include "Common.h"

#if defined CRUNCH_PLATFORM_WINDOWS
#include <Windows.h>
#include <WindowsX.h>
#include <gl\glew.h>
#include <gl\wglew.h>
#include <gl\gl.h>
#include <gl\glu.h>
#elif defined CRUNCH_PLATFORM_LINUX
#error Linux not yet supported!
#elif defined CRUNCH_PLATFORM_APPLE
#include <gl/glew.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#error Unsupported platform!
#endif

#endif