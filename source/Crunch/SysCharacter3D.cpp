#include "SysCharacter3D.h"

SysCharacter3D::SysCharacter3D() {
}

void SysCharacter3D::setSkeleton( const Skeleton3DResourceReference & skel ) {
	skeleton = skel;
	poseMatrices.resize( skel->getBoneCount() );
	skinMatrices.resize( skel->getBoneCount() );
}

Skeleton3DResourceReference SysCharacter3D::getSkeleton() const {
	return skeleton;
}

std::vector <Matrix44af> & SysCharacter3D::getPoseMatrices() {
	return poseMatrices;
}

const std::vector <Matrix44af> & SysCharacter3D::getPoseMatrices() const {
	return poseMatrices;
}

const std::vector <Matrix44af> & SysCharacter3D::getSkinMatrices() const {
	return skinMatrices;
}

void SysCharacter3D::setSkinMatrices() {
	for (size_t i = 0; i < skeleton->getBoneCount(); ++i)
		skinMatrices[i] = poseMatrices[i] * skeleton->getBoneData( i ).inverseBind;
}
