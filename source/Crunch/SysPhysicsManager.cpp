#if 0

#include "SysPhysicsManager.h"

void SysPhysicsManager::onCreate() {
#ifdef _DEBUG
	camera = NULL;
#else
	hide();
#endif
}

void SysPhysicsManager::onUpdate() {
	physicsManager.update();
}

void SysPhysicsManager::onDraw() {
#ifdef _DEBUG
	if (camera != NULL)
		physicsManager.draw( camera );
#endif
}

#ifdef _DEBUG
void SysPhysicsManager::setCamera( const SysCamera2D * cam ) {
	camera = cam;
}
#endif

#endif