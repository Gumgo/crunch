#ifndef CONE_DEFINED
#define CONE_DEFINED

#include "Common.h"
#include <vector>

// provides geometry for a cone
class Cone {
public:
	Cone();
	void generate( size_t segments );
	const std::vector <Vector3f> & getVertices() const;
	const std::vector <ushort> & getIndices() const;
	float getInscriptionRadiusMultiplier() const;

private:
	std::vector <Vector3f> vertices;
	std::vector <ushort> indices;
	float inscriptionRadiusMultiplier;
};

#endif