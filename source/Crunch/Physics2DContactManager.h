/** @file Physics2DContactManager.h
 *  @brief Manages contacts between objects in the simulation.
 */
#ifndef PHYSICS2DCONTACTMANAGER_DEFINED
#define PHYSICS2DCONTACTMANAGER_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DContact.h"
#include <vector>

namespace Physics2D {

class SpatialHashMap;
class Shape;

/** @brief Manages contacts between objects in the simulation.
 */
class ContactManager {
public:
	ContactManager( SpatialHashMap & shm );	/**< The constructor takes a reference to the spatial hash map.*/

	Contact * createContact( Shape * s0, Shape * s1 );	/**< Creates a contact.*/
	void destroyContact( Contact * c );					/**< Destroys a contact.*/
	Contact * getContacts();							/**< Returns the contact list.*/
	const Contact * getContacts() const;				/**< Returns the contact list.*/
	Contact * getNextContact( Contact * c );					/**< Returns the next contact in the list.*/
	const Contact * getNextContact( const Contact * c ) const;	/**< Returns the next contact in the list.*/

	void findNewContacts();			/**< Finds and creates new contacts between objects.*/
	void updateCurrentContacts();	/**< Updates the current set of contacts, destroying ones that no longer exist.*/

private:
	SpatialHashMap & spatialHashMap;	// reference to the spatial hash map
	Contact * contacts;					// list of contacts
	size_t contactCount;				// number of contacts

	// buffer for finding new contacts
	std::vector <std::pair <size_t, size_t>> contactBuffer;
};

}

#endif