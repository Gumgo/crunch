#ifndef MATHS_RAY3DEF_DEFINED
#define MATHS_RAY3DEF_DEFINED

template <typename T> Ray3 <T>::Ray3() {
}

template <typename T> Ray3 <T>::Ray3( const Vector3 <T> & orig, const Vector3 <T> & dir )
	: origin( orig )
	, direction( dir ) {
}

template <typename T> Ray3 <T>::Ray3( const Ray3 <T> & r )
	: origin( r.origin )
	, direction( r.direction ) {
}

template <typename T> Ray3 <T>::~Ray3() {
}

template <typename T> Ray3 <T> Ray3 <T>::operator=( const Ray3 <T> & r ) {
	origin = r.origin;
	direction = r.direction;
	return *this;
}

template <typename T> bool Ray3 <T>::operator==( const Ray3 <T> & r ) const {
	return (origin == r.origin && direction == r.direction);
}

template <typename T> bool Ray3 <T>::operator!=( const Ray3 <T> & r ) const {
	return (origin != r.origin || direction != r.direction);
}

template <typename T> template <typename U> Ray3 <T>::operator Ray3 <U> () const {
	return Ray3 <U>( (Vector3 <U>)origin, (Vector3 <U>)direction );
}

template <typename T> void Ray3 <T>::set( const Vector3 <T> & orig, const Vector3 <T> & dir ) {
	origin = orig;
	direction = dir;
}

#endif