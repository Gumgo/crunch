#ifndef SYSBLOOM_DEFINED
#define SYSBLOOM_DEFINED

#include "Common.h"
#include "GpuTexture.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"
#include <vector>

// applies bloom to an image
class SysBloom : private boost::noncopyable {
public:
	SysBloom();
	~SysBloom();

	void setupBloom(
		size_t w, size_t h,
		size_t passes = 5, float multiplier = 0.125f, float constantReduction = 0.25f, float asymptoticCutoff = 0.5f );

	bool bloom( GpuTextureReference & source, GpuTextureReference & tempBuffer );

private:
	ProgramResourceReference brightpassProgram;
	ProgramResourceReference blurProgram;
	ProgramResourceReference downsampleProgram;
	ProgramResourceReference blendProgram;
	bool ready;

	SysAutoProgram brightpassAuto;
	struct {
		int source;
		float constantReduction;
		float asymptoticCutoff2;
	} brightpassUniforms;

	SysAutoProgram blurAuto;
	struct {
		int source;
		Vector2f uvPixelSize;
	} blurUniforms;

	SysAutoProgram downsampleAuto;
	struct {
		Vector2f scale;
		int source;
	} downsampleUniforms;

	SysAutoProgram blendAuto;
	struct {
		Vector2f scale;
		int source;
		float multiplier;
	} blendUniforms;

	// downsample/blur buffers
	// first buffer is temporarily set to "tempBuffer"
	// set to NULL after bloom applied
	std::vector <GpuTextureReference> downsampleBuffers;

	size_t width, height;
	size_t downsamplePasses;
	float lumaCutoff;

	void downsample( GpuTextureReference & source, GpuTextureReference & tempBuffer );
	void applyBloom( GpuTextureReference & source );
};

#endif