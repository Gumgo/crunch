#include "Physics2D.h"

namespace Physics2D {

TimeStep::TimeStep( TimeUnit pDt ) {
	dt = pDt;
	invDt = (TimeUnit)1 / pDt;
}

MassData::MassData() {
}

MassData::MassData( const Vector2r & c, Real m, Real i ) {
	center = c;
	mass = m;
	if (m != (Real)0)
		invMass = (Real)1 / m;
	else
		invMass = (Real)0;
	inertia = i;
	if (i != (Real)0)
		invInertia = (Real)1 / i;
	else
		invInertia = (Real)0;
}

Aabb::Aabb() {
}

Aabb::Aabb( const Vector2r & minBnd, const Vector2r & maxBnd )
	: minBound( minBnd )
	, maxBound( maxBnd ) {
}

void Aabb::expand( Real amount ) {
	minBound.x -= amount;
	minBound.y -= amount;
	maxBound.x += amount;
	maxBound.y += amount;
}

Aabb Aabb::from2Points( const Vector2r & p0, const Vector2r & p1 ) {
	return Aabb( vecMin( p0, p1 ), vecMax( p0, p1 ) );
}

bool Aabb::overlap( const Aabb & aabb0, const Aabb & aabb1 ) {
	Vector2r dif0 = aabb0.minBound - aabb1.maxBound;
	Vector2r dif1 = aabb1.minBound - aabb0.maxBound;

	return !(dif0.x > (Real)0 || dif0.y > (Real)0 || dif1.x > (Real)0 || dif1.y > (Real)0);
}

GroupFilter::GroupFilter( uint oneOf, uint allOf, uint noneOf ) {
	oneOfGroups = oneOf;
	allOfGroups = allOf;
	noneOfGroups = noneOf;
}

uint GroupFilter::getOneOfGroups() const {
	return oneOfGroups;
}

uint GroupFilter::getAllOfGroups() const {
	return allOfGroups;
}

uint GroupFilter::getNoneOfGroups() const {
	return noneOfGroups;
}

void GroupFilter::setOneOfGroups( uint g ) {
	oneOfGroups = g;
}

void GroupFilter::setAllOfGroups( uint g ) {
	allOfGroups = g;
}

void GroupFilter::setNoneOfGroups( uint g ) {
	noneOfGroups = g;
}

void GroupFilter::addOneOfGroup( uint idx ) {
	oneOfGroups |= (1 << idx);
}

void GroupFilter::addAllOfGroup( uint idx ) {
	allOfGroups |= (1 << idx);
}

void GroupFilter::addNoneOfGroup( uint idx ) {
	noneOfGroups |= (1 << idx);
}

void GroupFilter::removeOneOfGroup( uint idx ) {
	oneOfGroups &= ~((uint)(1 << idx));
}

void GroupFilter::removeAllOfGroup( uint idx ) {
	allOfGroups &= ~((uint)(1 << idx));
}

void GroupFilter::removeNoneOfGroup( uint idx ) {
	noneOfGroups &= ~((uint)(1 << idx));
}

bool GroupFilter::test( uint input ) const {
	return
		(input & oneOfGroups) != 0 &&
		(input & allOfGroups) == allOfGroups &&
		(input & noneOfGroups) == 0;
}

GroupFilter2D::GroupFilter2D( bool initialValue ) {
	memset( groupPairArray, initialValue ? 0xFF : 0, sizeof( groupPairArray ) );
}

void GroupFilter2D::enablePair( uint a, uint b ) {
	if (a > GROUPS || b > GROUPS)
		return;
	if (b < a)
		std::swap( a, b );

	uint idx = a * GROUPS + b;
	uint idxByte = idx >> 3; // idx / 8
	uint idxBit = idx & 7;   // idx % 8

	groupPairArray[idxByte] |= (1 << idxBit);
}

void GroupFilter2D::disablePair( uint a, uint b ) {
	if (a > GROUPS || b > GROUPS)
		return;
	if (b < a)
		std::swap( a, b );

	uint idx = a * GROUPS + b;
	uint idxByte = idx >> 3; // idx / 8
	uint idxBit = idx & 7;   // idx % 8

	groupPairArray[idxByte] &= ~((byte)(1 << idxBit));
}

bool GroupFilter2D::test( uint a, uint b ) const {
	if (a > GROUPS || b > GROUPS)
		return false;
	if (b < a)
		std::swap( a, b );

	uint idx = a * GROUPS + b;
	uint idxByte = idx >> 3; // idx / 8
	uint idxBit = idx & 7;   // idx % 8

	return (groupPairArray[idxByte] & (1 << idxBit)) != 0;
}

}