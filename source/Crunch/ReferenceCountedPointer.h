/** @file ReferenceCountedConstPointer.h
 *  @brief Provides an implementation of a reference counted pointer.
 */

#ifndef REFERENCECOUNTEDPOINTER_DEFINED
#define REFERENCECOUNTEDPOINTER_DEFINED

// note: we need to explicitly declare non-templated constructors and assignment operators to avoid having implicit ones generated

/** @brief A class encapsulating automatic reference counting.
 *
 *  Classes using this must implement void acquire(), void release(), and size_t getReferenceCount().
 */
template <typename R> class ReferenceCountedPointer {
public:
	/** @brief The default constructor.
	 */
	ReferenceCountedPointer();

	/** @brief Constructs a reference to an object, incrementing its reference count.
	 *
	 *  The object type is templated to allow for use of derived objects.
	 */
	template <typename T> ReferenceCountedPointer( T * r );

	/** @brief Duplicates a reference to an object, incrementing its reference count.
	 */
	template <typename T> ReferenceCountedPointer( const ReferenceCountedPointer <T> & r );

	/** @brief Duplicates a reference to an object, incrementing its reference count.
	 */
	ReferenceCountedPointer( const ReferenceCountedPointer <R> & r );

	/** @brief Destroys the reference, decrementing the object's reference count.
	 */
	~ReferenceCountedPointer();

	/** @brief Assigns a new object, decrementing the reference count of the old one and incrementing the new one.
	 */
	template <typename T> ReferenceCountedPointer <R> & operator=( const ReferenceCountedPointer <T> & r );

	/** @brief Assigns a new object, decrementing the reference count of the old one and incrementing the new one.
	 */
	ReferenceCountedPointer <R> & operator=( const ReferenceCountedPointer <R> & r );

	/** @brief Returns the reference associated with this object.
	 */
	R & operator*();

	/** @brief Returns the reference associated with this object.
	 */
	const R & operator*() const;

	/** @brief Returns the reference associated with this object.
	 */
	R * operator->();

	/** @brief Returns the reference associated with this object.
	 */
	const R * operator->() const;

	/** @brief Returns the pointer.
	 */
	operator R*();

	/** @brief Returns the pointer.
	 */
	operator const R*() const;

	/** @brief Returns the pointer.
	 */
	R * get();

	/** @brief Returns the pointer.
	 */
	const R * get() const;

	/** @brief Returns whether the reference is not NULL.
	 */
	operator bool() const;

private:
	// all reference counted pointers friends to allow for interaction between derived types
	template <typename> friend class ReferenceCountedPointer;
	template <typename> friend class ReferenceCountedConstPointer;
	R * object;
};

template <typename R> ReferenceCountedPointer <R>::ReferenceCountedPointer()
	: object( NULL ) {
}

template <typename R> template <typename T> ReferenceCountedPointer <R>::ReferenceCountedPointer( T * r )
	: object( r ) {
	if (object != NULL)
		object->acquire();
}

template <typename R> template <typename T> ReferenceCountedPointer <R>::ReferenceCountedPointer( const ReferenceCountedPointer <T> & r )
	: object( r.object ) {
	if (object != NULL)
		object->acquire();
}

template <typename R> ReferenceCountedPointer <R>::ReferenceCountedPointer( const ReferenceCountedPointer <R> & r )
	: object( r.object ) {
	if (object != NULL)
		object->acquire();
}

template <typename R> ReferenceCountedPointer <R>::~ReferenceCountedPointer() {
	if (object != NULL)
		object->release();
}

template <typename R> template <typename T> ReferenceCountedPointer <R> & ReferenceCountedPointer <R>::operator=( const ReferenceCountedPointer <T> & r ) {
	// acquire first
	if (r.object != NULL)
		r.object->acquire();
	if (object != NULL)
		object->release();
	object = r.object;
	return *this;
}

template <typename R> ReferenceCountedPointer <R> & ReferenceCountedPointer <R>::operator=( const ReferenceCountedPointer <R> & r ) {
	// acquire first
	if (r.object != NULL)
		r.object->acquire();
	if (object != NULL)
		object->release();
	object = r.object;
	return *this;
}

template <typename R> R & ReferenceCountedPointer <R>::operator*() {
	return *object;
}

template <typename R> const R & ReferenceCountedPointer <R>::operator*() const {
	return *object;
}

template <typename R> R * ReferenceCountedPointer <R>::operator->() {
	return object;
}

template <typename R> const R * ReferenceCountedPointer <R>::operator->() const {
	return object;
}

template <typename R> ReferenceCountedPointer <R>::operator R*() {
	return object;
}

template <typename R> ReferenceCountedPointer <R>::operator const R*() const {
	return object;
}

template <typename R> R * ReferenceCountedPointer <R>::get() {
	return object;
}

template <typename R> const R * ReferenceCountedPointer <R>::get() const {
	return object;
}

template <typename R> ReferenceCountedPointer <R>::operator bool() const {
	return object != NULL;
}

/** @brief A class encapsulating automatic reference counting.
 *
 *  Classes using this must implement void acquire(), void release(), and size_t getReferenceCount().
 */
template <typename R> class ReferenceCountedConstPointer {
public:
	/** @brief The default constructor.
	 */
	ReferenceCountedConstPointer();

	/** @brief Constructs a reference to an object, incrementing its reference count.
	 *
	 *  The object type is templated to allow for use of derived objects.
	 */
	template <typename T> ReferenceCountedConstPointer( const T * r );

	/** @brief Duplicates a reference to an object, incrementing its reference count.
	 */
	template <typename T> ReferenceCountedConstPointer( const ReferenceCountedConstPointer <T> & r );

	/** @brief Duplicates a reference to an object, incrementing its reference count.
	 */
	ReferenceCountedConstPointer( const ReferenceCountedConstPointer <R> & r );

	/** @brief Duplicates a reference to an object, incrementing its reference count.
	 */
	template <typename T> ReferenceCountedConstPointer( const ReferenceCountedPointer <T> & r );

	/** @brief Destroys the reference, decrementing the object's reference count.
	 */
	~ReferenceCountedConstPointer();

	/** @brief Assigns a new object, decrementing the reference count of the old one and incrementing the new one.
	 */
	template <typename T> ReferenceCountedConstPointer <R> & operator=( const ReferenceCountedConstPointer <T> & r );

	/** @brief Assigns a new object, decrementing the reference count of the old one and incrementing the new one.
	 */
	ReferenceCountedConstPointer <R> & operator=( const ReferenceCountedConstPointer <R> & r );

	/** @brief Assigns a new object, decrementing the reference count of the old one and incrementing the new one.
	 */
	template <typename T> ReferenceCountedConstPointer <R> & operator=( const ReferenceCountedPointer <T> & r );

	/** @brief Returns the reference associated with this object.
	 */
	const R & operator*() const;

	/** @brief Returns the reference associated with this object.
	 */
	const R * operator->() const;

	/** @brief Returns the pointer.
	 */
	operator const R*() const;

	/** @brief Returns the pointer.
	 */
	const R * get() const;

	/** @brief Returns whether the reference is not NULL.
	 */
	operator bool() const;

private:
	// all reference counted pointers friends to allow for interaction between derived types
	template <typename> friend class ReferenceCountedPointer;
	template <typename> friend class ReferenceCountedConstPointer;
	const R * object;
};

template <typename R> ReferenceCountedConstPointer <R>::ReferenceCountedConstPointer()
	: object( NULL ) {
}

template <typename R> template <typename T> ReferenceCountedConstPointer <R>::ReferenceCountedConstPointer( const T * r )
	: object( r ) {
	if (object != NULL)
		object->acquire();
}

template <typename R> template <typename T> ReferenceCountedConstPointer <R>::ReferenceCountedConstPointer( const ReferenceCountedConstPointer <T> & r )
	: object( r.object ) {
	if (object != NULL)
		object->acquire();
}

template <typename R> ReferenceCountedConstPointer <R>::ReferenceCountedConstPointer( const ReferenceCountedConstPointer <R> & r )
	: object( r.object ) {
	if (object != NULL)
		object->acquire();
}

template <typename R> template <typename T> ReferenceCountedConstPointer <R>::ReferenceCountedConstPointer( const ReferenceCountedPointer <T> & r )
	: object( r.object ) {
	if (object != NULL)
		object->acquire();
}

template <typename R> ReferenceCountedConstPointer <R>::~ReferenceCountedConstPointer() {
	if (object != NULL)
		object->release();
}

template <typename R> template <typename T> ReferenceCountedConstPointer <R> & ReferenceCountedConstPointer <R>::operator=( const ReferenceCountedConstPointer <T> & r ) {
	// acquire first
	if (r.object != NULL)
		r.object->acquire();
	if (object != NULL)
		object->release();
	object = r.object;
	return *this;
}

template <typename R> ReferenceCountedConstPointer <R> & ReferenceCountedConstPointer <R>::operator=( const ReferenceCountedConstPointer <R> & r ) {
	// acquire first
	if (r.object != NULL)
		r.object->acquire();
	if (object != NULL)
		object->release();
	object = r.object;
	return *this;
}

template <typename R> template <typename T> ReferenceCountedConstPointer <R> & ReferenceCountedConstPointer <R>::operator=( const ReferenceCountedPointer <T> & r ) {
	// acquire first
	if (r.object != NULL)
		r.object->acquire();
	if (object != NULL)
		object->release();
	object = r.get();
	return *this;
}

template <typename R> const R & ReferenceCountedConstPointer <R>::operator*() const {
	return *object;
}

template <typename R> const R * ReferenceCountedConstPointer <R>::operator->() const {
	return object;
}

template <typename R> ReferenceCountedConstPointer <R>::operator const R*() const {
	return object;
}

template <typename R> const R * ReferenceCountedConstPointer <R>::get() const {
	return object;
}

template <typename R> ReferenceCountedConstPointer <R>::operator bool() const {
	return object != NULL;
}

#endif