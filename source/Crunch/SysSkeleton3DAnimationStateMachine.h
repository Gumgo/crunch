#ifndef SYSSKELETON3DANIMATIONSTATEMACHINE_DEFINED
#define SYSSKELETON3DANIMATIONSTATEMACHINE_DEFINED

#include "Common.h"
#include "Skeleton3DAnimationStateMachineResource.h"
#include "SysSkeleton3DAnimationTree.h"
#include <vector>

// applies "state" to an underlying skeleton 3D animation tree
class SysSkeleton3DAnimationStateMachine {
public:
	SysSkeleton3DAnimationStateMachine();

	bool init(
		const Skeleton3DAnimationStateMachineResourceReference & animStateMachine,
		const SysSkeleton3DAnimationTree & animTree );

	bool isInitialized() const;

	const Skeleton3DAnimationStateMachineResourceReference & getAnimationStateMachine() const;
	const SysSkeleton3DAnimationTree * getAnimationTree() const;

	size_t getCommandNodeIndex( size_t commandIndex ) const;

	// the state of a state machine
	class State {
	public:
		// default construction
		State();

		const SysSkeleton3DAnimationStateMachine * getAnimationStateMachine() const;
		SysSkeleton3DAnimationTree::State & getAnimationTreeState();
		const SysSkeleton3DAnimationTree::State & getAnimationTreeState() const;

		// binds the variable with the given index to take its value from the given location
		void bindVariable( size_t index, const float * location );
		void bindVariable( const std::string & name, const float * location );
		void bindVariable( size_t index, const uint * location );
		void bindVariable( const std::string & name, const uint * location );

		// sets whether the given state is active
		void setStateActive( size_t index, bool a );
		void setStateActive( const std::string & name, bool a );
		// returns whether the given state is active
		bool isStateActive( size_t index ) const;
		bool isStateActive( const std::string & name ) const;
		// triggers an event
		void triggerEvent( size_t index );
		void triggerEvent( const std::string & name );

		// updates the underlying animation tree state
		void update();

	private:
		friend class SysSkeleton3DAnimationStateMachine;
		State( const SysSkeleton3DAnimationStateMachine & sm );

		// pointer back to the state machine
		const SysSkeleton3DAnimationStateMachine * stateMachine;
		// the animation tree state being controlled
		SysSkeleton3DAnimationTree::State treeState;

		// variable bindings
		struct VariableBinding {
			union {
				const float * floatBinding;
				const uint * uintBinding;
			};
		};
		std::vector <VariableBinding> variableBindings;
		float getValue( const Skeleton3DAnimationStateMachineResource::FloatValue & value ) const;
		uint getValue( const Skeleton3DAnimationStateMachineResource::UintValue & value ) const;

		// bitfield representing active states
		std::vector <uint> activeStates;

		void updateState( size_t stateIndex );
		void executeCommand( size_t commandIndex );
	};

	// returns a new state based on this state machine
	State getState() const;

private:
	bool initialized;

	// the state machine
	Skeleton3DAnimationStateMachineResourceReference animationStateMachine;

	// the animation tree state
	const SysSkeleton3DAnimationTree * animationTree;

	// in the state machine resource, tree nodes are referred to by name
	// these names are resolved to indices upon initialization
	std::vector <size_t> commandNodeIndices;
};

#endif