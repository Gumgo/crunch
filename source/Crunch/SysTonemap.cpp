#include "SysTonemap.h"
#include "Context.h"
#include "SysRenderer2D.h"
#include "Log.h"

SysTonemap::SysTonemap() {
	downsample1Program = Context::get().resourceManager.acquire <ProgramResource>( "sys___tonemap_downsample1", true );
	downsample2Program = Context::get().resourceManager.acquire <ProgramResource>( "sys___tonemap_downsample2", true );
	adjustExposureProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___tonemap_adjust_exposure", true );
	applyTonemapProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___tonemap_apply_tonemap", true );
	ready =
		(downsample1Program->getStatus() == Resource::S_LOADED) &&
		(downsample2Program->getStatus() == Resource::S_LOADED) &&
		(adjustExposureProgram->getStatus() == Resource::S_LOADED) &&
		(applyTonemapProgram->getStatus() == Resource::S_LOADED);

	GpuBufferReference vertexBuffer = SysRenderer2D::get().getQuadVertexBuffer();
	uint cornerStride = sizeof( SysRenderer2D::QuadVertex );
	const void * cornerPtr = bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) );
	downsample1Auto.setProgram( downsample1Program->getProgram() );
	downsample2Auto.setProgram( downsample2Program->getProgram() );
	downsample1Auto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, cornerStride, cornerPtr );
	downsample2Auto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, cornerStride, cornerPtr );
	downsample1Auto.setUniform( "source", GL_SAMPLER_2D, &downsampleUniforms.source );
	downsample2Auto.setUniform( "source", GL_SAMPLER_2D, &downsampleUniforms.source );
	downsample1Auto.setUniform( "scale", GL_FLOAT_VEC2, &downsampleUniforms.scale );
	downsample2Auto.setUniform( "scale", GL_FLOAT_VEC2, &downsampleUniforms.scale );
	downsample1Auto.setUniform( "offset", GL_FLOAT_VEC2, &downsampleUniforms.offset );
	downsample2Auto.setUniform( "offset", GL_FLOAT_VEC2, &downsampleUniforms.offset );
	downsample1Auto.setUniform( "boost", GL_FLOAT, &downsampleUniforms.boost );
	downsample2Auto.setUniform( "boost", GL_FLOAT, &downsampleUniforms.boost );
	downsampleUniforms.source = 0;

	adjustExposureAuto.setProgram( adjustExposureProgram->getProgram() );
	adjustExposureAuto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, cornerStride, cornerPtr );
	adjustExposureAuto.setUniform( "logLuma", GL_SAMPLER_2D, &adjustExposureUniforms.logLuma );
	adjustExposureAuto.setUniform( "lumaBounds", GL_FLOAT_VEC2, &adjustExposureUniforms.lumaBounds );
	adjustExposureUniforms.logLuma = 0;

	applyTonemapAuto.setProgram( applyTonemapProgram->getProgram() );
	applyTonemapAuto.setAttribute( "corner", vertexBuffer, 2, GL_FLOAT, false, false, cornerStride, cornerPtr );
	applyTonemapAuto.setUniform( "source", GL_SAMPLER_2D, &applyTonemapUniforms.source );
	applyTonemapAuto.setUniform( "averageLuma", GL_SAMPLER_2D, &applyTonemapUniforms.averageLuma );
	applyTonemapAuto.setUniform( "whiteLuma2", GL_FLOAT, &applyTonemapUniforms.whiteLuma2 );
	applyTonemapUniforms.source = 0;
	applyTonemapUniforms.averageLuma = 1;

	GpuTexture::Texture2DDescription desc;
	desc.imageDescription.internalFormat = GL_R16F;
	desc.imageDescription.minFilter = GL_NEAREST;
	desc.imageDescription.magFilter = GL_NEAREST;
	desc.wrapS = GL_CLAMP_TO_EDGE;
	desc.wrapT = GL_CLAMP_TO_EDGE;
	GpuTexture::Data2DDescription dataDesc;
	desc.mipmapLevels = &dataDesc;
	dataDesc.format = GL_RED;
	dataDesc.type = GL_FLOAT;
	dataDesc.width = 1;
	dataDesc.height = 1;
	currentAverageLuminance = Context::get().gpuState.createTexture( desc );

	firstFrame = true;
	width = height = 0;
}

SysTonemap::~SysTonemap() {
}

void SysTonemap::setupTonemap( size_t w, size_t h, float expAdjustRate, float whiteLuma, const Vector2f & lumaBounds ) {
	exposureAdjustmentRate = expAdjustRate;
	adjustExposureUniforms.lumaBounds = lumaBounds;
	applyTonemapUniforms.whiteLuma2 = whiteLuma*whiteLuma;

	if (w == 0 || h == 0) {
		Log::warning() << "Invalid tonemapping size";
		return;
	}

	if (w == width && h == height)
		return;

	width = w;
	height = h;

	downsampleBuffers.clear();

	GpuTexture::Texture2DDescription desc;
	desc.imageDescription.internalFormat = GL_R16F;
	// linear filtering for downsampling
	desc.imageDescription.minFilter = GL_LINEAR;
	desc.imageDescription.magFilter = GL_LINEAR;
	// want a border - black is default
	desc.wrapS = GL_CLAMP_TO_BORDER;
	desc.wrapT = GL_CLAMP_TO_BORDER;
	GpuTexture::Data2DDescription dataDesc;
	desc.mipmapLevels = &dataDesc;
	dataDesc.format = GL_RED;
	dataDesc.type = GL_FLOAT;
	// first buffer is half size
	dataDesc.width = (w + 1) / 2;
	dataDesc.height = (h + 1) / 2;

	downsampleBuffers.push_back( Context::get().gpuState.createTexture( desc ) );

	// create smaller and smaller downsampling buffers
	while (dataDesc.width > 1 || dataDesc.height > 1) {
		dataDesc.width = (dataDesc.width + 3) / 4;
		dataDesc.height = (dataDesc.height + 3) / 4;
		downsampleBuffers.push_back( Context::get().gpuState.createTexture( desc ) );
	}
}

void SysTonemap::downsample( GpuTextureReference & source ) {
	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	GpuRasterizerState rastState = Context::get().gpuState.createRasterizerState();
	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );

	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	rastState.setCullingEnabled( false );
	rastState.bind();

	dsState.setDepthTestEnabled( false );
	dsState.setDepthWriteEnabled( false );
	dsState.bind();

	SysRenderer2D::get().getQuadVertexBuffer()->bind();

	fb->detatch( GL_DEPTH_STENCIL_ATTACHMENT );
	fb->attach( GL_COLOR_ATTACHMENT0, downsampleBuffers[0] );
	fb->detatch( GL_COLOR_ATTACHMENT1 );
	fb->detatch( GL_COLOR_ATTACHMENT2 );
	fb->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	GLenum drawBuf = GL_COLOR_ATTACHMENT0;
	dbState.setDrawBuffers( 1, &drawBuf );
	dbState.bind();

	// perform first downsample pass

	// bind main source texture
	source->bind( (size_t)downsampleUniforms.source );
	// scale is (size rounded up to multiple of 2) / (size)
	downsampleUniforms.scale.set(
		(float)(downsampleBuffers[0]->getWidth()*2) / (float)source->getWidth(),
		(float)(downsampleBuffers[0]->getWidth()*2) / (float)source->getHeight() );
	// interpolated UV coordinate will sample in center of 4 texels
	// we want to sample directly on each texel, so offset is scale * 0.5 / (size)
	downsampleUniforms.offset = Vector2f(
		0.5f / (float)source->getWidth(), 0.5f / (float)source->getHeight() );
	// number of total pixels over number of non-border pixels
	downsampleUniforms.boost =
		(float)(downsampleBuffers[0]->getWidth() * downsampleBuffers[0]->getHeight() * 4) /
		(float)(source->getWidth() * source->getHeight());

	downsample1Auto.bind();

	vpState.setViewportSize( Vector2i( (int)downsampleBuffers[0]->getWidth(), (int)downsampleBuffers[0]->getHeight() ) );
	vpState.bind();
	SysRenderer2D::get().drawSingleQuad();

	for (size_t i = 1; i < downsampleBuffers.size(); ++i) {
		fb->attach( GL_COLOR_ATTACHMENT0, downsampleBuffers[i] );

		// bind main source texture
		downsampleBuffers[i-1]->bind( downsampleUniforms.source );
		// scale is (size rounded up to multiple of 4) / (size)
		downsampleUniforms.scale.set(
			(float)(downsampleBuffers[i]->getWidth()*4) / (float)downsampleBuffers[i-1]->getWidth(),
			(float)(downsampleBuffers[i]->getHeight()*4) / (float)downsampleBuffers[i-1]->getHeight() );
		// interpolated UV coordinate will sample in center of block of 4x4 texels
		// we want to sample in center of four 2x2 blocks, so offset is scale / size
		downsampleUniforms.offset = Vector2f(
			1.0f / (float)downsampleBuffers[i-1]->getWidth(), 1.0f / (float)downsampleBuffers[i-1]->getHeight() );
		// number of total pixels over number of non-border pixels
		downsampleUniforms.boost =
			(float)(downsampleBuffers[i]->getWidth() * downsampleBuffers[i]->getHeight() * 16) /
			(float)(downsampleBuffers[i-1]->getWidth() * downsampleBuffers[i-1]->getHeight());

		downsample2Auto.bind();

		GpuViewportState vpState = Context::get().gpuState.createViewportState();
		vpState.setViewport( Vector2i(),
			Vector2i( (int)downsampleBuffers[i]->getWidth(), (int)downsampleBuffers[i]->getHeight() ) );
		vpState.bind();
		SysRenderer2D::get().drawSingleQuad();
	}
}

void SysTonemap::adjustExposure( uint frames ) {
	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->attach( GL_COLOR_ATTACHMENT0, currentAverageLuminance );

	// if it's the first frame, simply overwrite contents, rather than blend with previous value
	if (!firstFrame) {
		GpuBlendState blendState = Context::get().gpuState.createBlendState();
		blendState.setBlendingEnabled( true );
		blendState.setBlendFunction( GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA );
		// the more frames pass, the higher the adjustment rate is
		blendState.setBlendColor( Vector4f( 0.0f, 0.0f, 0.0f, 1.0f - pow( 1.0f - exposureAdjustmentRate, (float)frames ) ) );
		blendState.bind();
	}
	firstFrame = false;

	downsampleBuffers[downsampleBuffers.size()-1]->bind( adjustExposureUniforms.logLuma );

	adjustExposureAuto.bind();

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( 1, 1 ) );
	vpState.bind();
	SysRenderer2D::get().drawSingleQuad();
}

void SysTonemap::applyTonemap( GpuTextureReference & source, GpuTextureReference & dest ) {
	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->attach( GL_COLOR_ATTACHMENT0, dest );

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_NONE );
	blendState.bind();

	source->bind( (size_t)applyTonemapUniforms.source );
	currentAverageLuminance->bind( (size_t)applyTonemapUniforms.averageLuma );

	applyTonemapAuto.bind();

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)dest->getWidth(), (int)dest->getHeight() ) );
	vpState.bind();
	SysRenderer2D::get().drawSingleQuad();
}

bool SysTonemap::tonemap( uint frames, bool resetExposure, GpuTextureReference & source, GpuTextureReference & dest ) {
	if (!ready)
		return false;
	if (!source) {
		Log::warning() << "No source texture provided for tonemapping";
		return false;
	}
	if (!dest) {
		Log::warning() << "No destination texture provided for tonemapping";
		return false;
	}
	if (source->getWidth() != width || source->getHeight() != height ||
		dest->getWidth() != width || dest->getHeight() != height) {
		Log::warning() << "Invalid source or destination texture size for tonemapping";
		return false;
	}

	if (resetExposure)
		firstFrame = true;

	downsample( source );
	adjustExposure( frames );
	applyTonemap( source, dest );
	return true;
}