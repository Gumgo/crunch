#include "GpuRenderbuffer.h"
#include "GpuState.h"

GpuRenderbuffer::RenderbufferDescription::RenderbufferDescription() {
	internalFormat = GL_RGBA8;
	samples = 0;
	width = 0;
	height = 0;
}

GpuRenderbuffer::GpuRenderbuffer( GpuState * gpuState, const RenderbufferDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	internalFormat = desc.internalFormat;
	samples = desc.samples;
	size[0] = desc.width;
	size[1] = desc.height;

	glGenRenderbuffers( 1, &id );
	bind();
	glRenderbufferStorageMultisample( GL_RENDERBUFFER, (int)samples, internalFormat, size[0], size[1] );
}

GpuRenderbuffer::~GpuRenderbuffer() {
	glDeleteRenderbuffers( 1, &id );
}

GLuint GpuRenderbuffer::getId() const {
	return id;
}

GLenum GpuRenderbuffer::getInternalFormat() const {
	return internalFormat;
}

uint GpuRenderbuffer::getSamples() const {
	return samples;
}

uint GpuRenderbuffer::getSize( size_t dimension ) const {
	return size[dimension];
}

uint GpuRenderbuffer::getWidth() const {
	return size[0];
}

uint GpuRenderbuffer::getHeight() const {
	return size[1];
}

void GpuRenderbuffer::bind() {
	getGpuState()->bindRenderbuffer( this );
}

void GpuRenderbuffer::onRelease() const {
	// no more references
	delete this;
}