#include "Physics2DEdgeShape.h"
#include "Physics2DBody.h"

namespace Physics2D {

EdgeShape::EdgeShape( const EdgeShapeDefinition & d, Body * par )
	: Shape( d, Shape::T_EDGE, par ) {
	points[0] = d.points[0];
	points[1] = d.points[1];
	normal = (points[1] - points[0]).orthogonal().normalized();

	setArea( (Real)0 );
	connectivity[0] = connectivity[1] = NULL;
}

Aabb EdgeShape::getAabb( const Matrix33ar & transformation ) const {
	return Aabb::from2Points( transformation.transformPoint( points[0] ), transformation.transformPoint( points[1] ) );
}

void EdgeShape::computeMassData() {
	setMassData( MassData( (Real)0.5 * (points[0] + points[1]), (Real)0, (Real)0 ) );
}

void EdgeShape::setPoints( const Vector2r & p0, const Vector2r & p1 ) {
	points[0] = p0;
	points[1] = p1;
	normal = (p1 - p0).orthogonal().normalized();
}

Vector2r EdgeShape::getPoint( size_t point ) const {
	return points[point];
}

Vector2r EdgeShape::getNormal() const {
	return normal;
}

void EdgeShape::setConnectivity( EdgeShape * point0Edge, EdgeShape * point1Edge ) {
	connectivity[0] = point0Edge;
	connectivity[1] = point1Edge;
}

EdgeShape * EdgeShape::getConnectivity( size_t point ) const {
	return connectivity[point];
}

EdgeShapeDefinition::EdgeShapeDefinition() {
}

Shape * EdgeShapeDefinition::createShape( Body * parent ) const {
	return new EdgeShape( *this, parent );
}

}