#ifndef MATHS_VECTOR2DEF_DEFINED
#define MATHS_VECTOR2DEF_DEFINED

template <typename T> Vector2 <T>::Vector2()
	: x( 0 )
	, y( 0 ) {
}

template <typename T> Vector2 <T>::Vector2( T px, T py )
	: x( px )
	, y( py ) {
}

template <typename T> Vector2 <T>::Vector2( const Vector2 <T> & v )
	: x( v.x )
	, y( v.y ) {
}

template <typename T> Vector2 <T>::~Vector2() {
}

template <typename T> Vector2 <T> Vector2 <T>::operator+( const Vector2 <T> & v ) const {
	return Vector2 <T>( x + v.x, y + v.y );
}

template <typename T> Vector2 <T> Vector2 <T>::operator+() const {
	return *this;
}

template <typename T> Vector2 <T> Vector2 <T>::operator-( const Vector2 <T> & v ) const {
	return Vector2 <T>( x - v.x, y - v.y );
}

template <typename T> Vector2 <T> Vector2 <T>::operator-() const {
	return Vector2 <T>( -x, -y );
}

template <typename T> Vector2 <T> Vector2 <T>::operator*( T r ) const {
	return Vector2 <T>( x*r, y*r );
}

template <typename T> Vector2 <T> operator*( T r, const Vector2 <T> & v ) {
	return v*r;
}

template <typename T> Vector2 <T> Vector2 <T>::operator/( T r ) const {
	return Vector2 <T>( x/r, y/r );
}

template <typename T> Vector2 <T> operator/( T r, const Vector2 <T> & v ) {
	return Vector2 <T>( r/v.x, r/v.y );
}

template <typename T> Vector2 <T> Vector2 <T>::operator*( const Vector2 <T> & v ) const {
	return Vector2 <T>( x*v.x, y*v.y );
}

template <typename T> Vector2 <T> Vector2 <T>::operator/( const Vector2 <T> & v ) const {
	return Vector2 <T>( x/v.x, y/v.y );
}

template <typename T> Vector2 <T> Vector2 <T>::operator=( const Vector2 <T> & v ) {
	x = v.x;
	y = v.y;
	return *this;
}

template <typename T> Vector2 <T> Vector2 <T>::operator+=( const Vector2 <T> & v ) {
	x += v.x;
	y += v.y;
	return *this;
}

template <typename T> Vector2 <T> Vector2 <T>::operator-=( const Vector2 <T> & v ) {
	x -= v.x;
	y -= v.y;
	return *this;
}

template <typename T> Vector2 <T> Vector2 <T>::operator*=( T r ) {
	x *= r;
	y *= r;
	return *this;
}

template <typename T> Vector2 <T> Vector2 <T>::operator/=( T r ) {
	x /= r;
	y /= r;
	return *this;
}

template <typename T> Vector2 <T> Vector2 <T>::operator*=( const Vector2 <T> & v ) {
	x *= v.x;
	y *= v.y;
	return *this;
}

template <typename T> Vector2 <T> Vector2 <T>::operator/=( const Vector2 <T> & v ) {
	x /= v.x;
	y /= v.y;
	return *this;
}

template <typename T> bool Vector2 <T>::operator==( const Vector2 <T> & v ) const {
	return (x == v.x && y == v.y);
}

template <typename T> bool Vector2 <T>::operator!=( const Vector2 <T> & v ) const {
	return (x != v.x || y != v.y);
}

template <typename T> template <typename U> T & Vector2 <T>::operator[]( U i ) {
	return *(&x+i);
}

template <typename T> template <typename U> T Vector2 <T>::operator[]( U i ) const {
	return *(&x+i);
}

template <typename T> template <typename U> Vector2 <T>::operator Vector2 <U> () const {
	return Vector2 <U>( (U)x, (U)y );
}

template <typename T> void Vector2 <T>::set( T px, T py ) {
	x = px;
	y = py;
}

template <typename T> void Vector2 <T>::normalize() {
	setMagnitude( 1 );
}

template <typename T> void Vector2 <T>::setMagnitude( T m ) {
	T mag = magnitudeSquared();
	if (mag == 0)
		return;
	mag = m/sqrt( mag );
	x *= mag;
	y *= mag;
}

template <typename T> void Vector2 <T>::orthogonalize() {
	T temp = -y;
	y = x;
	x = temp;
}

template <typename T> Vector2 <T> Vector2 <T>::normalized() const {
	T mag = magnitudeSquared();
	if (mag == 0)
		return Vector2 <T>( 0, 0 );
	mag = 1/sqrt( mag );
	return Vector2 <T>( x*mag, y*mag );
}

template <typename T> T Vector2 <T>::magnitude() const {
	return sqrt( x*x + y*y );
}

template <typename T> T Vector2 <T>::magnitudeSquared() const {
	return (x*x + y*y);
}

template <typename T> T Vector2 <T>::dot( const Vector2 <T> & v ) const {
	return (x*v.x + y*v.y);
}

template <typename T> Vector2 <T> Vector2 <T>::orthogonal() const {
	return Vector2 <T>( -y, x );
}

template <typename T> bool Vector2 <T>::isZero() const {
	return (x == 0 && y == 0);
}

template <typename T> Vector4 <T> Vector2 <T>::xxxx() const { return Vector4 <T>( x, x, x, x ); }
template <typename T> Vector4 <T> Vector2 <T>::xxxy() const { return Vector4 <T>( x, x, x, y ); }
template <typename T> Vector4 <T> Vector2 <T>::xxyx() const { return Vector4 <T>( x, x, y, x ); }
template <typename T> Vector4 <T> Vector2 <T>::xxyy() const { return Vector4 <T>( x, x, y, y ); }
template <typename T> Vector4 <T> Vector2 <T>::xyxx() const { return Vector4 <T>( x, y, x, x ); }
template <typename T> Vector4 <T> Vector2 <T>::xyxy() const { return Vector4 <T>( x, y, x, y ); }
template <typename T> Vector4 <T> Vector2 <T>::xyyx() const { return Vector4 <T>( x, y, y, x ); }
template <typename T> Vector4 <T> Vector2 <T>::xyyy() const { return Vector4 <T>( x, y, y, y ); }
template <typename T> Vector4 <T> Vector2 <T>::yxxx() const { return Vector4 <T>( y, x, x, x ); }
template <typename T> Vector4 <T> Vector2 <T>::yxxy() const { return Vector4 <T>( y, x, x, y ); }
template <typename T> Vector4 <T> Vector2 <T>::yxyx() const { return Vector4 <T>( y, x, y, x ); }
template <typename T> Vector4 <T> Vector2 <T>::yxyy() const { return Vector4 <T>( y, x, y, y ); }
template <typename T> Vector4 <T> Vector2 <T>::yyxx() const { return Vector4 <T>( y, y, x, x ); }
template <typename T> Vector4 <T> Vector2 <T>::yyxy() const { return Vector4 <T>( y, y, x, y ); }
template <typename T> Vector4 <T> Vector2 <T>::yyyx() const { return Vector4 <T>( y, y, y, x ); }
template <typename T> Vector4 <T> Vector2 <T>::yyyy() const { return Vector4 <T>( y, y, y, y ); }
template <typename T> Vector3 <T> Vector2 <T>::xxx() const { return Vector3 <T>( x, x, x ); }
template <typename T> Vector3 <T> Vector2 <T>::xxy() const { return Vector3 <T>( x, x, y ); }
template <typename T> Vector3 <T> Vector2 <T>::xyx() const { return Vector3 <T>( x, y, x ); }
template <typename T> Vector3 <T> Vector2 <T>::xyy() const { return Vector3 <T>( x, y, y ); }
template <typename T> Vector3 <T> Vector2 <T>::yxx() const { return Vector3 <T>( y, x, x ); }
template <typename T> Vector3 <T> Vector2 <T>::yxy() const { return Vector3 <T>( y, x, y ); }
template <typename T> Vector3 <T> Vector2 <T>::yyx() const { return Vector3 <T>( y, y, x ); }
template <typename T> Vector3 <T> Vector2 <T>::yyy() const { return Vector3 <T>( y, y, y ); }
template <typename T> Vector2 <T> Vector2 <T>::xx() const { return Vector2 <T>( x, x ); }
template <typename T> Vector2 <T> Vector2 <T>::xy() const { return Vector2 <T>( x, y ); }
template <typename T> Vector2 <T> Vector2 <T>::yx() const { return Vector2 <T>( y, x ); }
template <typename T> Vector2 <T> Vector2 <T>::yy() const { return Vector2 <T>( y, y ); }

template <typename T> Vector2 <T> vecMax( const Vector2 <T> & v1, const Vector2 <T> & v2 ) {
	return Vector2 <T>( v1.x > v2.x ? v1.x : v2.x,
						v1.y > v2.y ? v1.y : v2.y );
}

template <typename T> Vector2 <T> vecMin( const Vector2 <T> & v1, const Vector2 <T> & v2 ) {
	return Vector2 <T>( v1.x < v2.x ? v1.x : v2.x,
						v1.y < v2.y ? v1.y : v2.y );
}

#endif