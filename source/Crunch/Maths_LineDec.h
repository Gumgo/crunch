/** @file Maths_LineDec.h
 *  @brief Contains an implementation of a line class.
 */

#ifndef MATHS_LINEDEC_DEFINED
#define MATHS_LINEDEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for lines.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Line {
public:
	Vector2 <T> normal;	/**< The normal of the line.*/
	T c;				/**< The c component of the line equation.*/

	/** @brief Constructs a line with default values.
	 */
	Line();

	/** @brief Constructs a line from the equation n \htmlonly&#0183\endhtmlonly p + d = 0.
	 *
	 *  @param norm	The normal of the line, or a and b in the line equation.
	 *  @param pc	The c component in the line equation.
	 */
	Line( const Vector2 <T> & norm, T pc ); // ax + by + c = 0; norm = ab, offset = c

	/** @brief Constructs a line by copying the one provided.
	 */
	Line( const Line <T> & p );

	/** @brief The destructor.
	 */
	~Line();

	// operators

	/** @brief Sets the current value to the line m.
	 */
	Line <T> operator=( const Line <T> & p );

	/** @brief Returns whether the current value is equal to the line p.
	 */
	bool operator==( const Line <T> & p ) const;

	/** @brief Returns whether the current value is not equal to the line p.
	 */
	bool operator!=( const Line <T> & p ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Line <U> () const;

	// mutable

	/** @brief Sets the current value by constructing a line from 2 points.
	 */
	void set2Points( const Vector2 <T> & p0, const Vector2 <T> & p1 );

	/** @brief Sets the current value by constructing a line from the equation n \htmlonly&#0183\endhtmlonly p + d = 0.
	 *
	 *  @param norm	The normal of the line, or a and b in the line equation.
	 *  @param pc	The c component in the line equation.
	 */
	void set( const Vector2 <T> & norm, T pc );

	/** @brief Sets the current value by constructing a line from a normal vector and a point.
	 */
	void setNormalPoint( const Vector2 <T> & norm, const Vector2 <T> & point );

	/** @brief Normalizes the normal of the line.
	 */
	void normalize();

	// immutable

	/** @brief Returns a copy of the current value with the normal of the line normalized.
	 */
	Line <T> normalized() const;

	/** @brief Computes the intersection between the line and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the line's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param origin		Origin of the ray.
	 *  @param direction	Direction of the ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Vector2 <T> & origin, const Vector2 <T> & direction, Vector2 <T> & intersection, T & signedDist ) const;

	/** @brief Computes the intersection between the line and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the line's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param ray			The ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Ray2 <T> & ray, Vector2 <T> & intersection, T & signedDist ) const;

	/** @brief Computes the intersection between the line and a ray.
	 *
	 *  @param origin		Origin of the ray.
	 *  @param direction	Direction of the ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Vector2 <T> & origin, const Vector2 <T> & direction, Vector2 <T> & intersection ) const;

	/** @brief Computes the intersection between the line and a ray.
	 *
	 *  @param ray			The ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Ray2 <T> & ray, Vector2 <T> & intersection ) const;

	/** @brief Computes the intersection between the line and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the line's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param origin		Origin of the ray.
	 *  @param direction	Direction of the ray.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Vector2 <T> & origin, const Vector2 <T> & direction, T & signedDist ) const;

	/** @brief Computes the intersection between the line and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the line's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param ray			The ray.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Ray2 <T> & ray, T & signedDist ) const;

	/** @brief Computes the distance between a point and the line.
	 *
	 *  @warning In order for the resulting
	 *  distance to be correct, the
	 *  line's normal must be normalized.
	 *
	 *  @param point	The point.
	 *  @return			The distance of point to the current value.
	 */
	T distance( const Vector2 <T> & point ) const;

	/** @brief Computes the signed distance between a point and the line.
	 *
	 *  @warning In order for the resulting
	 *  signed distance to be correct, the
	 *  line's normal must be normalized.
	 *
	 *  @param point	The point.
	 *  @return		The signed distance of point to the current value.
	 */
	T signedDistance( const Vector2 <T> & point ) const;

	/** @brief Returns which side of the line a point is on.
	 *
	 *  @param point	The point.
	 *  @return			-1, 0, or 1, for behind, on, or in front of the line, respectively.
	 */
	T side( const Vector2 <T> & point ) const;

	/** @brief Computes the x coordinate of the point on the line with the given y coordinate.
	 *
	 *  @param x	Location to store the x coordinate.
	 *  @param y	The y coordinate.
	 *  @return		True if the x coordinate can be computed, false otherwise.
	 */
	bool x( T & x, T y ) const;

	/** @brief Computes the y coordinate of the point on the line with the given x coordinate.
	 *
	 *  @param x	The x coordinate.
	 *  @param y	Location to store the y coordinate.
	 *  @return		True if the y coordinate can be computed, false otherwise.
	 */
	bool y( T x, T & y ) const;
};

/** @brief Constructs a line from 2 points, rotating clockwise.
 */
template <typename T> Line <T> Line2Points( const Vector2 <T> & p0, const Vector2 <T> & p1 );

/** @brief Constructs a line from a normal vector and a point.
 */
template <typename T> Line <T> LinePointNormal( const Vector2 <T> & norm, const Vector2 <T> & point );

/** @brief Line of type float.
 */
typedef Line <float> Linef;

/** @brief Line of type double.
 */
typedef Line <double> Lined;

#endif