#ifndef SYSGUIBUTTON_DEFINED
#define SYSGUIBUTTON_DEFINED

#include "Common.h"
#include "Actor.h"
#include "SysGui.h"

#include <boost/function.hpp>

DEFINE_DERIVED_ACTOR( SysGuiButton, SysGui, 0, 0 )

	Vector2i position;
	int width;
	std::string text;

	bool down;

	boost::function <void ()> onClick;

	bool pointInside( const Vector2f & point ) const;

public:
	void onCreate();

	void move( const Vector2i & pos );
	void setWidth( int w );
	void setText( const std::string & t );

	void setOnClick( const boost::function <void ()> & callback );

	void receiveEvent( const InputEvent & ev );
	void onDismissParent();
	void draw();
};

#endif