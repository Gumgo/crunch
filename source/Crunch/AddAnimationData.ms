attribDef = attributes animationData
(
	parameters main rollout:params
	(
		firstFrame type:#integer ui:firstFrame default:0
		lastFrame type:#integer ui:lastFrame default:0
	)

	rollout params "Animation Range"
	(
		spinner firstFrame "Start frame" type:#integer range:[0,99999,0]
		spinner lastFrame "Last frame" type:#integer range:[0,99999,0]
	)
)

for i = 1 to selection.count do
(
	obj = selection[i]
	custAttributes.add obj attribDef
)