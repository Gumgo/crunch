#include "InputEvent.h"

bool WindowEvent::operator==( const WindowEvent & o ) const {
	return type == o.type;
}

bool CharacterEvent::operator==( const CharacterEvent & o ) const {
	return characterCode == o.characterCode;
}

bool KeyEvent::operator==( const KeyEvent & o ) const {
	return type == o.type && keyCode == o.keyCode;
}

bool MouseButtonEvent::operator==( const MouseButtonEvent & o ) const {
	return type == o.type && button == o.button && x == o.x && y == o.y;
}

bool MouseWheelEvent::operator==( const MouseWheelEvent & o ) const {
	return scroll == o.scroll && x == o.x && y == o.y;
}

bool MousePositionEvent::operator==( const MousePositionEvent & o ) const {
	return x == o.x && y == o.y;
}

bool MouseRawEvent::operator==( const MouseRawEvent & o ) const {
	return dx == o.dx && dy == o.dy;
}

bool InputEvent::operator==( const InputEvent & o ) const {
	if (type != o.type)
		return false;
	switch (type) {
	case IET_WINDOW:
		return windowEvent == o.windowEvent;
	case IET_CHARACTER:
		return characterEvent == o.characterEvent;
	case IET_KEY:
		return keyEvent == o.keyEvent;
	case IET_MOUSE_BUTTON:
		return mouseButtonEvent == o.mouseButtonEvent;
	case IET_MOUSE_WHEEL:
		return mouseWheelEvent == o.mouseWheelEvent;
	case IET_MOUSE_POSITION:
		return mousePositionEvent == o.mousePositionEvent;
	case IET_MOUSE_RAW:
		return mouseRawEvent == o.mouseRawEvent;
	default:
		assert( false );
		return false;
	}
}
