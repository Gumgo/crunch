/** @file GpuRasterizerState.h
 *  @brief Encapsulates the state of the rasterizer on the GPU.
 */

#ifndef GPURASTERIZERSTATE_DEFINED
#define GPURASTERIZERSTATE_DEFINED

#include "Common.h"
#include "GpuObject.h"

/**  @brief Encapsulates the state of the rasterizer on the GPU.
 */
class GpuRasterizerState : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuRasterizerState();

	/** @brief Sets the state of the GPU to the state represented by this object.
	 */
	void bind();

	/** @brief Sets the point size.
	 */
	void setPointSize( float size );

	/** @brief Sets the line width.
	 */
	void setLineWidth( float width );

	/** @brief Sets the polygon mode.
	 */
	void setPolygonMode( GLenum mode );

	/** @brief Sets the polygon modes for front and back faces.
	 */
	void setPolygonModes( GLenum modeFront, GLenum modeBack );

	/** @brief Sets whether culling is enabled.
	 */
	void setCullingEnabled( bool e );

	/** @brief Sets which faces should be culled.
	 */
	void setCullMode( GLenum mode );

	/** @brief Sets the orientation of front facing polygons.
	 */
	void setFrontFaceOrientation( GLenum orientation );

	/** @brief Sets whether polygon offset is enabled for polygons drawn in fill mode.
	 */
	void setPolygonOffsetFillEnabled( bool e );

	/** @brief Sets whether polygon offset is enabled for polygons drawn in line mode.
	 */
	void setPolygonOffsetLineEnabled( bool e );

	/** @brief Sets whether polygon offset is enabled for polygons drawn in point mode.
	 */
	void setPolygonOffsetPointEnabled( bool e );

	/** @brief Sets the polygon offset parameters.
	 */
	void setPolygonOffset( float factor, float units );

	/** @brief Returns the point size.
	 */
	float getPointSize() const;

	/** @brief Returns the line width.
	 */
	float getLineWidth() const;

	/** @brief Returns the polygon mode for front faces.
	 */
	GLenum getPolygonModeFront() const;

	/** @brief Returns the polygon mode for back faces.
	 */
	GLenum getPolygonModeBack() const;

	/** @brief Returns whether culling is enabled.
	 */
	bool isCullingEnabled() const;

	/** @brief Returns which faces should be culled.
	 */
	GLenum getCullMode() const;

	/** @brief Returns the orientation of front facing polygons.
	 */
	GLenum getFrontFaceOrientation() const;

	/** @brief Returns whether polygon offset is enabled for polygons drawn in fill mode.
	 */
	bool isPolygonOffsetFillEnabled() const;

	/** @brief Returns whethes polygon offset is enabled for polygons drawn in line mode.
	 */
	bool isPolygonOffsetLineEnabled() const;

	/** @brief Returns whether polygon offset is enabled for polygons drawn in point mode.
	 */
	bool isPolygonOffsetPointEnabled() const;

	/** @brief Returns the polygon offset factor parameter.
	 */
	float getPolygonOffsetFactor() const;

	/** @brief Returns the polygon offset units parameter.
	 */
	float getPolygonOffsetUnits() const;

private:
	friend class GpuState;
	GpuRasterizerState( GpuState * state );

	// NOTE: it appears that polygon mode is only implemented to work with GL_FRONT_AND_BACK
	// attempting to use it with GL_FRONT or GL_BACK separately results in GL_INVALID_ENUM
	// additionally, glGet with GL_POLYGON_MODE results in GL_INVALID_ENUM
	// workaround: use GL_FRONT_AND_BACK only

	float pointSize;				// the size of points
	float lineWidth;				// the size of lines
	GLenum polygonModeFront;		// the polygon mode for front faces
	GLenum polygonModeBack;			// the polygon mode for back faces

	bool cullingEnabled;			// whether polygon culling is enabled
	GLenum cullMode;				// the mode in which polygons are culled
	GLenum frontFaceOrientation;	// the orientation of front-facing polygons

	bool polygonOffsetFillEnabled;	// whether polygon offset is enabled for polygons drawn in fill mode
	bool polygonOffsetLineEnabled;	// whether polygon offset is enabled for polygons drawn in line mode
	bool polygonOffsetPointEnabled;	// whether polygon offset is enabled for polygons drawn in point mode
	float polygonOffsetFactor;		// the factor parameter for polygon offset
	float polygonOffsetUnits;		// the units parameter for polygon offset

	// compares changes against current state and apply differences
	void apply( GpuRasterizerState & currentState ) const;
	// sets values to the current OpenGL state
	void getCurrentState();
};

#endif