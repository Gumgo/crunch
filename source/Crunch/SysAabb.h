#ifndef SYSAABB_DEFINED
#define SYSAABB_DEFINED

#include "Common.h"

/** @brief An axis-aligned bounding box.
 */
struct SysAabb {
	static const SysAabb EMPTY;

	Vector3f minBound;
	Vector3f maxBound;

	SysAabb();
	// constructs an AABB defined by two bounds
	SysAabb( const Vector3f & minBnd, const Vector3f & maxBnd );
	// constructs an AABB containing the transformed AABB defined by two bounds
	SysAabb( const Vector3f & minBnd, const Vector3f & maxBnd, const Matrix44af & tf );
	// constructs an AABB containing the transformed AABB defined by two bounds
	SysAabb( const Vector3f & minBnd, const Vector3f & maxBnd, const Matrix33f & tf );
	// constructs an AABB from two AABBs
	SysAabb( const SysAabb & aabb1, const SysAabb & aabb2 );
};

#endif