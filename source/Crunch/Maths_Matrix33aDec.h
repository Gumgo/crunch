/** @file Maths_Matrix33aDec.h
 *  @brief Contains an implementation of an affine \htmlonly3&#0215&#0051\endhtmlonly matrix class.
 */

#ifndef MATHS_MATRIX33ADEC_DEFINED
#define MATHS_MATRIX33ADEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief An class for affine \htmlonly3&#0215&#0051\endhtmlonly matrices.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Matrix33a {
	T d[6]; // the matrix components

public:
	/** @brief Constructs a \htmlonly3&#0215&#0051\endhtmlonly identity matrix.
	 */
	Matrix33a();

	/** @brief Constructs a \htmlonly3&#0215&#0051\endhtmlonly matrix with the given components.
	 */
	Matrix33a( T m00, T m01, T m02,
			   T m10, T m11, T m12 );

	/** @brief Constructs a \htmlonly3&#0215&#0051\endhtmlonly matrix by copying the one provided.
	 */
	Matrix33a( const Matrix33a <T> & m );

	/** @brief Constructs a \htmlonly3&#0215&#0051\endhtmlonly matrix by copying the one provided.
	 */
	Matrix33a( const Matrix22 <T> & m );

	/** @brief The destructor.
	 */
	~Matrix33a();

	// operators

	/** @brief Returns the sum of the current value and the matrix m.
	 */
	Matrix33a <T> operator+( const Matrix33a <T> & m ) const;

	/** @brief Returns the current value unmodified.
	 */
	Matrix33a <T> operator+() const;

	/** @brief Returns the difference between the current value and the matrix m.
	 */
	Matrix33a <T> operator-( const Matrix33a <T> & m ) const;

	/** @brief Returns the current value negated.
	 */
	Matrix33a <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Matrix33a <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Matrix33a <T> operator/( T r ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix33a <T> operator*( const Matrix33a <T> & m ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix33a <T> operator*( const Matrix22 <T> & m ) const;

	/** @brief Sets the current value to the matrix m.
	 */
	Matrix33a <T> operator=( const Matrix33a <T> & m );

	/** @brief Sets the current value to the matrix m.
	 */
	Matrix33a <T> operator=( const Matrix22 <T> & m );

	/** @brief Increments the current value by the matrix m.
	 */
	Matrix33a <T> operator+=( const Matrix33a <T> & m );

	/** @brief Decrements the current value by the matrix m.
	 */
	Matrix33a <T> operator-=( const Matrix33a <T> & m );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Matrix33a <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Matrix33a <T> operator/=( T r );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix33a <T> operator*=( const Matrix33a <T> & m );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix33a <T> operator*=( const Matrix22 <T> & m );

	/** @brief Returns whether the current value is equal to the matrix m.
	 */
	bool operator==( const Matrix33a <T> & m ) const;

	/** @brief Returns whether the current value is not equal to the matrix m.
	 */
	bool operator!=( const Matrix33a <T> & m ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Accesses the component of the current value at (row, col).
	 */
	template <typename U> T & operator()( U row, U col );

	/** @brief Returns the component of the current value at (row, col).
	 */
	template <typename U> T operator()( U row, U col ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Matrix33a <U> () const;

	// mutable

	/** @brief Sets the components of the current value to the components provided.
	 */
	void set( T m00, T m01, T m02,
			  T m10, T m11, T m12 );

	/** @brief Inverts the current value.
	 */
	bool invert();

	/** @brief Translates the current value by the vector (x, y).
	 */
	void translate( T x, T y );

	/** @brief Translates the current value by the vector t.
	 */
	void translate( const Vector2 <T> & t );

	/** @brief Rotates the current value by a radians.
	 */
	void rotate( T a );

	/** @brief Scales the current value by the vector (x, y).
	 */
	void scale( T x, T y );

	/** @brief Scales the current value by the vector s.
	 */
	void scale( const Vector2 <T> & s );

	/** @brief Sets the current value to the identity matrix.
	 */
	void identity();

	// immutable

	/** @brief Returns the inverse of the current value.
	 */
	Matrix33a <T> inverse() const;

	/** @brief Returns the current value translated by the vector (x, y).
	 */
	Matrix33a <T> translated( T x, T y ) const;

	/** @brief Returns the current value translated by the vector t.
	 */
	Matrix33a <T> translated( const Vector2 <T> & t ) const;

	/** @brief Returns the current value rotated by a radians.
	 */
	Matrix33a <T> rotated( T a ) const;

	/** @brief Returns the current value scaled by the vector (x, y).
	 */
	Matrix33a <T> scaled( T x, T y ) const;

	/** @brief Returns the current value scaled by the vector s.
	 */
	Matrix33a <T> scaled( const Vector2 <T> & s ) const;

	/** @brief Returns the determinant of the current value.
	 */
	T determinant() const;

	/** @brief Returns the trace of the current value.
	 */
	T trace() const;

	/** @brief Returns a pointer to the component array.
	 */
	const T * getArray() const;

	/** @brief Returns the point p transformed by the current value (where p.z = 1).
	 */
	Vector2 <T> transformPoint( const Vector2 <T> & p ) const;

	/** @brief Returns the vector v transformed by the current value (where v.z = 0).
	 */
	Vector2 <T> transformVector( const Vector2 <T> & v ) const;

	/** @brief Returns the ray r transformed by the current value.
	 */
	Ray2 <T> transformRay( const Ray2 <T> & r ) const;

	/** @brief Returns a matrix used to transform normals.
	 *
	 *  The matrix returned is the inverse transpose
	 *  of the rotation and scaling portion of the
	 *  current value.
	 */
	Matrix22 <T> getNormalTransformation() const;
};

/** @brief Returns the product of the scalar r and the matrix m.
 */
template <typename T> Matrix33a <T> operator*( T r, const Matrix33a <T> & m );

/** @brief Matrix33a of type float.
 */
typedef Matrix33a <float> Matrix33af;

/** @brief Matrix33a of type double.
 */
typedef Matrix33a <double> Matrix33ad;

#endif