#include "Actor.h"
#include "Context.h"

Actor::Actor( int updatePrior, int renderPrior, size_t idx, size_t gsIdx ) {
	updatePriority = updatePrior;
	renderPriority = renderPrior;
	index = idx;
	gameStateIndex = gsIdx;
}

Actor::~Actor() {
}

int Actor::getUpdatePriority() const {
	return updatePriority;
}

int Actor::getRenderPriority() const {
	return renderPriority;
}

size_t Actor::getIndex() const {
	return index;
}

size_t Actor::getGameStateIndex() const {
	return gameStateIndex;
}

bool Actor::isActive() const {
	return Context::get().gameStateManager.getGameState( gameStateIndex )->isActorActive( getIndex() );
}

bool Actor::isVisible() const {
	return Context::get().gameStateManager.getGameState( gameStateIndex )->isActorVisible( getIndex() );
}

void Actor::activate() {
	if (!isActive())
		Context::get().gameStateManager.getGameState( gameStateIndex )->activateActor( getIndex() );
}

void Actor::deactivate() {
	if (isActive())
		Context::get().gameStateManager.getGameState( gameStateIndex )->deactivateActor( getIndex() );
}

void Actor::hide() {
	if (isVisible())
		Context::get().gameStateManager.getGameState( gameStateIndex )->hideActor( getIndex() );
}

void Actor::show() {
	if (!isVisible())
		Context::get().gameStateManager.getGameState( gameStateIndex )->showActor( getIndex() );
}

void Actor::destroy() {
	Context::get().gameStateManager.getGameState( gameStateIndex )->destroyActor( getIndex() );
}

void Actor::onCreate() {
}

void Actor::onUpdate() {
}

void Actor::onDestroy() {
}

void Actor::onDraw() {
}

void Actor::onActivate() {
}

void Actor::onDeactivate() {
}