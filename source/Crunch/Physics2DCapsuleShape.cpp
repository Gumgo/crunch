#include "Physics2DCapsuleShape.h"
#include "Physics2DBody.h"

namespace Physics2D {

CapsuleShape::CapsuleShape( const CapsuleShapeDefinition & d, Body * par )
	: Shape( d, Shape::T_CAPSULE, par ) {
	vertices[0] = d.vertices[0];
	vertices[1] = d.vertices[1];
	radius = d.radius;

	setArea( computeArea() );
}

Real CapsuleShape::computeArea() const {
	return boost::math::constants::pi <Real>() * radius * radius
		+ (vertices[1] - vertices[0]).magnitude() * radius * (Real)2;
}

void CapsuleShape::setVertices( const Vector2r & v0, const Vector2r & v1 ) {
	vertices[0] = v0;
	vertices[1] = v1;
	setArea( computeArea() );
}

Vector2r CapsuleShape::getVertex( size_t vertex ) const {
	return vertices[vertex];
}

void CapsuleShape::setRadius( Real rad ) {
	radius = rad;
	setArea( computeArea() );
}

Real CapsuleShape::getRadius() const {
	return radius;
}

Aabb CapsuleShape::getAabb( const Matrix33ar & transformation ) const {
	Matrix33ar tf = transformation * getTransform(); // world transform * shape transform
	Vector2r v0Tf = tf.transformPoint( vertices[0] );
	Vector2r v1Tf = tf.transformPoint( vertices[1] );

	Aabb ret = Aabb::from2Points( v0Tf, v1Tf );
	ret.minBound.x -= radius;
	ret.minBound.y -= radius;
	ret.maxBound.x += radius;
	ret.maxBound.y += radius;
	return ret;
}

void CapsuleShape::computeMassData() {
	// the center of mass is the midpoint of the two vertices
	// compute for central rectangle and two end half disks separately
	Vector2r massCtr = (Real)0.5 * (vertices[0] + vertices[1]);
	Real height = (vertices[0] - vertices[1]).magnitude();
	Real halfHeight = (Real)0.5 * height;

	Real mass1 = getDensity() * (Real)2 * radius * height;
	Real mass2 = getDensity() * boost::math::constants::pi <Real>() * radius * radius;

	Real inertia1 = mass1 * ((Real)1/(Real)3) * (radius*radius + halfHeight*halfHeight);
	Real inertia2 = mass2 * (radius*radius + halfHeight*halfHeight) + getDensity()*((Real)8/(Real)3)*radius*radius*radius*halfHeight;

	Real mass = mass1+mass2;
	Real inertia = inertia1+inertia2;
	if (isCenterOfMassOverridden()) {
		inertia += mass*(massCtr - getOverriddenCenterOfMass()).magnitudeSquared();
		massCtr = getOverriddenCenterOfMass();
	}
	setMassData( MassData( getTransform().transformPoint( massCtr ), mass, inertia ) );
}

CapsuleShapeDefinition::CapsuleShapeDefinition() {
	vertices[0].set( (Real)0, (Real)1 );
	vertices[1].set( (Real)0, (Real)(-1) );
	radius = (Real)1;
}

Shape * CapsuleShapeDefinition::createShape( Body * parent ) const {
	return new CapsuleShape( *this, parent );
}

}