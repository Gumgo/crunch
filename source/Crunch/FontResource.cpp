#include "FontResource.h"
#include "Context.h"
#include "Log.h"

FontResource::FontResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m ) {
}

bool FontResource::loadDataAsync() {
	try {
		// open and load the file
		DataReader in( getPath() );
		if (in.fail())
			throw std::runtime_error( "Failed to open " + getPath() );

		// header must be "F0"
		byte headerCmp[2] = { 'F', '0' };
		byte header[2];

		// read the header
		if (!in.readData( header, sizeof( header ) ) || memcmp( header, headerCmp, sizeof( header ) ) != 0)
			throw std::runtime_error( "Failed to read header, or invalid header" );

		// read in the texture width, height, the font line height, and number of characters
		uint charCount;
		in.readElements( &width, 1 );
		in.readElements( &height, 1 );
		in.readElements( &lineHeight, 1 );
		in.readElements( &charCount, 1 );

		if (in.fail())
			throw std::runtime_error( "Failed to read dimension data" );

		// compute the font line height in texture coordinates
		textureLineHeight = (float)lineHeight/(float)height;

		// allocate space for the glyphs
		glyphs.resize( charCount );

		// allocate indices of glyphs
		glyphTable.resize( 256, 0xFF );

		// allocate validity of glyphs
		glyphsValid.resize( 256, true );

		// buffer for reading in glyph data
		struct GlyphBuffer {
			byte ascii, width;
			ushort x, y;
		} buffer;

		// read in each character
		for (uint i = 0; i < charCount; ++i) {
			if (!in.readElements( &buffer, 1 ))
				throw std::runtime_error( "Failed to read glyph data" );
			glyphs[i].x1 = (float)buffer.x/(float)width;
			glyphs[i].x2 = ((float)buffer.x + (float)buffer.width)/(float)width;
			glyphs[i].y1 = (float)buffer.y/(float)height;
			glyphs[i].advance = buffer.width;
			glyphTable[buffer.ascii] = (size_t)i;
		}

		// buffer for reading in cursor data
		struct CursorBuffer {
			byte width;
			ushort x, y;
		} cursorBuffer;

		// read the cursor
		if (!in.readElements( &cursorBuffer.width, 1 ) || !in.readElements( &cursorBuffer.x, 2 ))
			throw std::runtime_error( "Failed to read cursor data" );
		cursor.x1 = (float)cursorBuffer.x/(float)width;
		cursor.x2 = ((float)cursorBuffer.x + (float)cursorBuffer.width)/(float)width;
		cursor.y1 = (float)cursorBuffer.y/(float)height;
		cursor.advance = cursorBuffer.width;

		// get the default glyph
		size_t defaultGlyph = glyphTable[0xFF];
		if (defaultGlyph == 0xFF)
			throw std::runtime_error( "Unspecified default glyph" );

		// for each glyph, point it to the default glyph if it's NULL
		for (size_t i = 0; i < 256; ++i) {
			if (glyphTable[i] == 0xFF) {
				glyphTable[i] = defaultGlyph;
				glyphsValid[i] = false;
			}
		}

		// allocate the font texture data
		data.resize( width*height );

		// read in the font texture data
		if (!in.readData( &data[0], width*height ))
			throw std::runtime_error( "Failed to read font texture data" );
	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}

	return true;
}

bool FontResource::loadDataSync() {
	try {
		GpuTexture::Texture2DDescription desc;
		desc.imageDescription.internalFormat = GL_RED;
		// clamp to border color - black
		desc.wrapS = GL_CLAMP_TO_BORDER;
		desc.wrapT = GL_CLAMP_TO_BORDER;
		GpuTexture::Data2DDescription mip;
		desc.mipmapLevels = &mip;
		mip.width = width;
		mip.height = height;
		mip.data = &data[0];
		mip.format = GL_RED;
		mip.type = GL_UNSIGNED_BYTE;
		texture = Context::get().gpuState.createTexture( desc );

		// clear the CPU side buffer
		data.clear();
		data.shrink_to_fit();
	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}

	return true;
}

void FontResource::freeData() {
}

uint FontResource::getLineHeight() const {
	return lineHeight;
}

float FontResource::getTextureLineHeight() const {
	return textureLineHeight;
}

bool FontResource::containsGlyph( char c ) const {
	return glyphsValid[c];
}

size_t FontResource::getGlyphIndex( char c ) const {
	return glyphTable[c];
}

const FontResource::Glyph & FontResource::getGlyph( size_t i ) const {
	return glyphs[i];
}

const FontResource::Glyph FontResource::getCursorGlyph() const {
	return cursor;
}

GpuTextureReference FontResource::getTexture() {
	return texture;
}

GpuTextureConstReference FontResource::getTexture() const {
	return texture;
}