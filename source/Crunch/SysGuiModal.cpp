#include "SysGuiModal.h"
#include "Context.h"
#include "SysRenderer2D.h"

SysGuiModal::I::I()
	: events( 20 ) {
}

void SysGuiModal::onCreate() {
	camera = NULL;
	childModal = NULL;

	drawCall.bind( this, &SysGuiModal::draw );
	drawKey = Renderer::makeKey( SysRenderer2D::PASS, getRenderPriority() );
}

void SysGuiModal::setCamera( const SysCamera2D * cam ) {
	camera = cam;
	for (size_t i = 0; i < children.size(); ++i)
		children[i]->setCamera( cam );
	if (childModal != NULL)
		childModal->setCamera( cam );
}

void SysGuiModal::setDefaultFont( FontResourceReference & defFont ) {
	defaultFont = defFont;
}

FontResourceReference SysGuiModal::getDefaultFont() {
	return defaultFont;
}

FontResourceConstReference SysGuiModal::getDefaultFont() const {
	return defaultFont;
}

void SysGuiModal::addGui( SysGui * gui ) {
	children.push_back( gui );
	gui->setCamera( camera );
	if (!gui->getFont())
		gui->setFont( defaultFont );
}

void SysGuiModal::setChildModal( SysGuiModal * modal ) {
	if (childModal != NULL && childModal != modal)
		childModal->onDismiss();
	childModal = modal;
	if (modal != NULL)
		modal->setCamera( camera );
}

void SysGuiModal::receiveEvent( const InputEvent & ev ) {
	if (childModal != NULL)
		childModal->receiveEvent( ev );
	else {
		size_t eventIndex;
		if (i.events.getIndex( ev, eventIndex )) {
			// if the event is registered, do it
			Event & e = i.events.getByIndex( eventIndex );
			if (Context::get().inputManager.key( KC_CONTROL )) {
				if (e.onCtrlEvent)
					e.onCtrlEvent();
			} else {
				if (e.onEvent)
					e.onEvent();
			}
		} else {
			// if not registered, pass the event to children
			for (size_t i = 0; i < children.size(); ++i)
				children[i]->receiveEvent( ev );
		}
	}
}

void SysGuiModal::setEventHandler( const InputEvent & ev, bool ctrl, const boost::function <void ()> & eventHandler ) {
	size_t eventIndex;
	if (!i.events.getIndex( ev, eventIndex ))
		eventIndex = i.events.put( ev, Event() );
	Event & e = i.events.getByIndex( eventIndex );
	if (ctrl)
		e.onCtrlEvent = eventHandler;
	else
		e.onEvent = eventHandler;
}

void SysGuiModal::removeEventHandler( const InputEvent & ev, bool ctrl ) {
	size_t eventIndex;
	if (i.events.getIndex( ev, eventIndex )) {
		Event & e = i.events.getByIndex( eventIndex );
		if (ctrl)
			e.onCtrlEvent.clear();
		else
			e.onEvent.clear();
		if (!e.onEvent && !e.onCtrlEvent)
			i.events.removeByIndex( eventIndex );
	}
}

void SysGuiModal::draw() {
	for (size_t i = 0; i < children.size(); ++i)
		children[i]->draw();
	if (childModal != NULL)
		childModal->draw();
}

void SysGuiModal::onDraw() {
	Context::get().renderer.submit( drawCall, drawKey );
}

void SysGuiModal::onDismiss() {
	for (size_t i = 0; i < children.size(); ++i)
		children[i]->onDismissParent();
}