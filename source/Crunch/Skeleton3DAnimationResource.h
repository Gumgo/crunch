/** @file Skeleton3DAnimationResource.h
 *  @brief Contains the resource holding 3D skeleton animation data.
 */

#ifndef SKELETON3DANIMATIONRESOURCE_DEFINED
#define SKELETON3DANIMATIONRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include <vector>

/** @brief Skeleton 3D animation resource.
 */
DEFINE_RESOURCE_CLASS( Skeleton3DAnimationResource, "skeleton3DAnimation", "skeleton3DAnimations" )
public:
	/** @brief The constructor.
	 */
	Skeleton3DAnimationResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief Holds data for a keyframe in an animation.
	 */
	struct KeyframeData {
		float time;				/**< The time in seconds of this keyframe.*/
		Vector3f translation;	/**< The translation of the bone at this keyframe.*/
		Quaternionf rotation;	/**< The rotation of the bone at this keyframe.*/
	};

	/** @brief Holds data for an animated bone in an animation.
	 */
	struct AnimatedBoneData {
		size_t boneId;							/**< The ID of the bone in the skeleton.*/
		std::vector <KeyframeData> keyframes;	/**< The keyframes for this bone.*/
	};

	size_t getAnimatedBoneCount() const;							/**< Returns the number of bones in the animation.*/
	const AnimatedBoneData & getAnimatedBoneData( size_t i ) const;	/**< @brief Returns animated bone data.*/
	float getLength() const;										/**< Returns the animation length in seconds.*/
	float getFadeInTime() const;									/**< Returns the time to fade in the animation in seconds.*/
	float getFadeOutTime() const;									/**< Returns the time to fade out the animation in seconds.*/

private:
	float length;									// the length of the animation in seconds
	float fadeInTime;								// the time to fade in the animation in seconds
	float fadeOutTime;								// the time to fade out the animation in seconds
	std::vector <AnimatedBoneData> animatedBones;	// the animated bones in the animation
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <Skeleton3DAnimationResource> Skeleton3DAnimationResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <Skeleton3DAnimationResource> Skeleton3DAnimationResourceConstReference;

#endif