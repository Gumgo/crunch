/** @file Physics2DDebugDisplay.h
 *  @brief Draws the objects in a simulation.
 */

#ifndef PHYSICS2DDEBUGDISPLAY_DEFINED
#define PHYSICS2DDEBUGDISPLAY_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DSimulation.h"
#include "Physics2DBody.h"
#include "Physics2DShape.h"
#include "Physics2DContact.h"
#include "Physics2DCapsuleShape.h"
#include "Physics2DCircleShape.h"
#include "Physics2DConvexPolygonShape.h"
#include "Physics2DEdgeShape.h"
#include "Physics2DEllipseShape.h"

#include "Renderer.h"
#include "GpuBuffer.h"
#include "ProgramResource.h"
#include "SysCamera2D.h"

namespace Physics2D {

/** @brief Draws objects in a simulation.
 */
class DebugDisplay {
public:
	DebugDisplay();		/**< Loads required shaders.*/
	~DebugDisplay();	/**< Frees required shaders.*/

	void setSimulation( const Simulation * s );	/**< Sets the simulation to draw.*/
	void setCamera( const SysCamera2D * cam );	/**< Sets the camera.*/
	void setScale( float s );					/**< Sets the scale of the simulation.*/
	void preDraw();								/**< Prepares the line vertex buffer.*/
	void draw();								/**< Draws the simulation.*/

private:
	const Simulation * sim;		// the simulation to draw
	const SysCamera2D * camera;	// the camera
	float scale;				// the scale of the simulation

	ProgramResourceReference program;				// program to draw most shapes
	ProgramResourceReference capsuleProgram;		// program to draw capsules
	ProgramResourceReference convexPolygonProgram;	// program to draw convex polygons

	// vertex for drawing shapes
	struct Vertex {
		float pos[2];
	};

	static const size_t AABB_VERTICES = 4;						// number of vertices for AABBs
	static const size_t CIRCLE_VERTICES = 32;					// number of vertices to use in circular shapes
	static const size_t CAPSULE_VERTICES = CIRCLE_VERTICES + 2;	// number of vertices to use in capsule shapes
	static const size_t CONVEX_POLYGON_VERTICES =				// maximum number of vertices to use in convex polygon shapes
		ConvexPolygonShape::MAX_VERTICES;
	static const size_t X_VERTICES = 4;							// number of vertices for drawing Xs
	GpuBufferReference vertexBuffer;							// vertex buffer
	std::vector <Vertex> lineVertices;							// list of line vertices
	GpuBufferReference lineVertexBuffer;						// vertex buffer for lines
};

}

#endif