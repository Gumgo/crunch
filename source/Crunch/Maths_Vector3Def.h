#ifndef MATHS_VECTOR3DEF_DEFINED
#define MATHS_VECTOR3DEF_DEFINED

template <typename T> Vector3 <T>::Vector3()
	: x( 0 )
	, y( 0 )
	, z( 0 ) {
}

template <typename T> Vector3 <T>::Vector3( T px, T py, T pz )
	: x( px )
	, y( py )
	, z( pz ) {
}

template <typename T> Vector3 <T>::Vector3( const Vector3 <T> & v )
	: x( v.x )
	, y( v.y )
	, z( v.z ) {
}

template <typename T> Vector3 <T>::Vector3( const Vector2 <T> & pxy, T pz )
	: x( pxy.x )
	, y( pxy.y )
	, z( pz ) {
}

template <typename T> Vector3 <T>::Vector3( T px, const Vector2 <T> & pyz )
	: x( px )
	, y( pyz.x )
	, z( pyz.y ) {
}

template <typename T> Vector3 <T>::~Vector3() {
}

template <typename T> Vector3 <T> Vector3 <T>::operator+( const Vector3 <T> & v ) const {
	return Vector3 <T>( x + v.x, y + v.y, z + v.z );
}

template <typename T> Vector3 <T> Vector3 <T>::operator+() const {
	return *this;
}

template <typename T> Vector3 <T> Vector3 <T>::operator-( const Vector3 <T> & v ) const {
	return Vector3 <T>( x - v.x, y - v.y, z - v.z );
}

template <typename T> Vector3 <T> Vector3 <T>::operator-() const {
	return Vector3 <T>( -x, -y, -z );
}

template <typename T> Vector3 <T> Vector3 <T>::operator*( T r ) const {
	return Vector3 <T>( x*r, y*r, z*r );
}

template <typename T> Vector3 <T> operator*( T r, const Vector3 <T> & v ) {
	return v*r;
}

template <typename T> Vector3 <T> Vector3 <T>::operator/( T r ) const {
	return Vector3 <T>( x/r, y/r, z/r );
}

template <typename T> Vector3 <T> operator/( T r, const Vector3 <T> & v ) {
	return Vector3 <T>( r/v.x, r/v.y, r/v.z );
}

template <typename T> Vector3 <T> Vector3 <T>::operator*( const Vector3 <T> & v ) const {
	return Vector3 <T>( x*v.x, y*v.y, z*v.z );
}

template <typename T> Vector3 <T> Vector3 <T>::operator/( const Vector3 <T> & v ) const {
	return Vector3 <T>( x/v.x, y/v.y, z/v.z );
}

template <typename T> Vector3 <T> Vector3 <T>::operator=( const Vector3 <T> & v ) {
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}

template <typename T> Vector3 <T> Vector3 <T>::operator+=( const Vector3 <T> & v ) {
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

template <typename T> Vector3 <T> Vector3 <T>::operator-=( const Vector3 <T> & v ) {
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

template <typename T> Vector3 <T> Vector3 <T>::operator*=( T r ) {
	x *= r;
	y *= r;
	z *= r;
	return *this;
}

template <typename T> Vector3 <T> Vector3 <T>::operator/=( T r ) {
	x /= r;
	y /= r;
	z /= r;
	return *this;
}

template <typename T> Vector3 <T> Vector3 <T>::operator*=( const Vector3 <T> & v ) {
	x *= v.x;
	y *= v.y;
	z *= v.z;
	return *this;
}

template <typename T> Vector3 <T> Vector3 <T>::operator/=( const Vector3 <T> & v ) {
	x /= v.x;
	y /= v.y;
	z /= v.z;
	return *this;
}

template <typename T> bool Vector3 <T>::operator==( const Vector3 <T> & v ) const {
	return (x == v.x && y == v.y && z == v.z);
}

template <typename T> bool Vector3 <T>::operator!=( const Vector3 <T> & v ) const {
	return (x != v.x || y != v.y || z != v.z);
}

template <typename T> template <typename U> T & Vector3 <T>::operator[]( U i ) {
	return *(&x+i);
}

template <typename T> template <typename U> T Vector3 <T>::operator[]( U i ) const {
	return *(&x+i);
}

template <typename T> template <typename U> Vector3 <T>::operator Vector3 <U> () const {
	return Vector3 <U>( (U)x, (U)y, (U)z );
}

template <typename T> void Vector3 <T>::set( T px, T py, T pz ) {
	x = px;
	y = py;
	z = pz;
}

template <typename T> void Vector3 <T>::normalize() {
	setMagnitude( 1 );
}

template <typename T> void Vector3 <T>::setMagnitude( T m ) {
	T mag = magnitudeSquared();
	if (mag == 0)
		return;
	mag = m/sqrt( mag );
	x *= mag;
	y *= mag;
	z *= mag;
}

template <typename T> Vector3 <T> Vector3 <T>::normalized() const {
	T mag = magnitudeSquared();
	if (mag == 0)
		return Vector3 <T>( 0, 0, 0 );
	mag = 1/sqrt( mag );
	return Vector3 <T>( x*mag, y*mag, z*mag );
}

template <typename T> T Vector3 <T>::magnitude() const {
	return sqrt( x*x + y*y + z*z );
}

template <typename T> T Vector3 <T>::magnitudeSquared() const {
	return (x*x + y*y + z*z);
}

template <typename T> T Vector3 <T>::dot( const Vector3 <T> & v ) const {
	return (x*v.x + y*v.y + z*v.z);
}

template <typename T> Vector3 <T> Vector3 <T>::cross( const Vector3 <T> & v ) const {
	return Vector3 <T>( y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x );
}

template <typename T> bool Vector3 <T>::isZero() const {
	return (x == 0 && y == 0 && z == 0);
}

template <typename T> Vector4 <T> Vector3 <T>::xxxx() const { return Vector4 <T>( x, x, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xxxy() const { return Vector4 <T>( x, x, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xxxz() const { return Vector4 <T>( x, x, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xxyx() const { return Vector4 <T>( x, x, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xxyy() const { return Vector4 <T>( x, x, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xxyz() const { return Vector4 <T>( x, x, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xxzx() const { return Vector4 <T>( x, x, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xxzy() const { return Vector4 <T>( x, x, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xxzz() const { return Vector4 <T>( x, x, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xyxx() const { return Vector4 <T>( x, y, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xyxy() const { return Vector4 <T>( x, y, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xyxz() const { return Vector4 <T>( x, y, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xyyx() const { return Vector4 <T>( x, y, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xyyy() const { return Vector4 <T>( x, y, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xyyz() const { return Vector4 <T>( x, y, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xyzx() const { return Vector4 <T>( x, y, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xyzy() const { return Vector4 <T>( x, y, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xyzz() const { return Vector4 <T>( x, y, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xzxx() const { return Vector4 <T>( x, z, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xzxy() const { return Vector4 <T>( x, z, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xzxz() const { return Vector4 <T>( x, z, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xzyx() const { return Vector4 <T>( x, z, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xzyy() const { return Vector4 <T>( x, z, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xzyz() const { return Vector4 <T>( x, z, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::xzzx() const { return Vector4 <T>( x, z, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::xzzy() const { return Vector4 <T>( x, z, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::xzzz() const { return Vector4 <T>( x, z, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yxxx() const { return Vector4 <T>( y, x, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yxxy() const { return Vector4 <T>( y, x, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yxxz() const { return Vector4 <T>( y, x, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yxyx() const { return Vector4 <T>( y, x, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yxyy() const { return Vector4 <T>( y, x, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yxyz() const { return Vector4 <T>( y, x, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yxzx() const { return Vector4 <T>( y, x, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yxzy() const { return Vector4 <T>( y, x, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yxzz() const { return Vector4 <T>( y, x, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yyxx() const { return Vector4 <T>( y, y, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yyxy() const { return Vector4 <T>( y, y, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yyxz() const { return Vector4 <T>( y, y, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yyyx() const { return Vector4 <T>( y, y, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yyyy() const { return Vector4 <T>( y, y, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yyyz() const { return Vector4 <T>( y, y, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yyzx() const { return Vector4 <T>( y, y, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yyzy() const { return Vector4 <T>( y, y, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yyzz() const { return Vector4 <T>( y, y, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yzxx() const { return Vector4 <T>( y, z, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yzxy() const { return Vector4 <T>( y, z, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yzxz() const { return Vector4 <T>( y, z, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yzyx() const { return Vector4 <T>( y, z, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yzyy() const { return Vector4 <T>( y, z, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yzyz() const { return Vector4 <T>( y, z, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::yzzx() const { return Vector4 <T>( y, z, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::yzzy() const { return Vector4 <T>( y, z, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::yzzz() const { return Vector4 <T>( y, z, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zxxx() const { return Vector4 <T>( z, x, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zxxy() const { return Vector4 <T>( z, x, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zxxz() const { return Vector4 <T>( z, x, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zxyx() const { return Vector4 <T>( z, x, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zxyy() const { return Vector4 <T>( z, x, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zxyz() const { return Vector4 <T>( z, x, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zxzx() const { return Vector4 <T>( z, x, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zxzy() const { return Vector4 <T>( z, x, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zxzz() const { return Vector4 <T>( z, x, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zyxx() const { return Vector4 <T>( z, y, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zyxy() const { return Vector4 <T>( z, y, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zyxz() const { return Vector4 <T>( z, y, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zyyx() const { return Vector4 <T>( z, y, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zyyy() const { return Vector4 <T>( z, y, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zyyz() const { return Vector4 <T>( z, y, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zyzx() const { return Vector4 <T>( z, y, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zyzy() const { return Vector4 <T>( z, y, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zyzz() const { return Vector4 <T>( z, y, z, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zzxx() const { return Vector4 <T>( z, z, x, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zzxy() const { return Vector4 <T>( z, z, x, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zzxz() const { return Vector4 <T>( z, z, x, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zzyx() const { return Vector4 <T>( z, z, y, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zzyy() const { return Vector4 <T>( z, z, y, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zzyz() const { return Vector4 <T>( z, z, y, z ); }
template <typename T> Vector4 <T> Vector3 <T>::zzzx() const { return Vector4 <T>( z, z, z, x ); }
template <typename T> Vector4 <T> Vector3 <T>::zzzy() const { return Vector4 <T>( z, z, z, y ); }
template <typename T> Vector4 <T> Vector3 <T>::zzzz() const { return Vector4 <T>( z, z, z, z ); }
template <typename T> Vector3 <T> Vector3 <T>::xxx() const { return Vector3 <T>( x, x, x ); }
template <typename T> Vector3 <T> Vector3 <T>::xxy() const { return Vector3 <T>( x, x, y ); }
template <typename T> Vector3 <T> Vector3 <T>::xxz() const { return Vector3 <T>( x, x, z ); }
template <typename T> Vector3 <T> Vector3 <T>::xyx() const { return Vector3 <T>( x, y, x ); }
template <typename T> Vector3 <T> Vector3 <T>::xyy() const { return Vector3 <T>( x, y, y ); }
template <typename T> Vector3 <T> Vector3 <T>::xyz() const { return Vector3 <T>( x, y, z ); }
template <typename T> Vector3 <T> Vector3 <T>::xzx() const { return Vector3 <T>( x, z, x ); }
template <typename T> Vector3 <T> Vector3 <T>::xzy() const { return Vector3 <T>( x, z, y ); }
template <typename T> Vector3 <T> Vector3 <T>::xzz() const { return Vector3 <T>( x, z, z ); }
template <typename T> Vector3 <T> Vector3 <T>::yxx() const { return Vector3 <T>( y, x, x ); }
template <typename T> Vector3 <T> Vector3 <T>::yxy() const { return Vector3 <T>( y, x, y ); }
template <typename T> Vector3 <T> Vector3 <T>::yxz() const { return Vector3 <T>( y, x, z ); }
template <typename T> Vector3 <T> Vector3 <T>::yyx() const { return Vector3 <T>( y, y, x ); }
template <typename T> Vector3 <T> Vector3 <T>::yyy() const { return Vector3 <T>( y, y, y ); }
template <typename T> Vector3 <T> Vector3 <T>::yyz() const { return Vector3 <T>( y, y, z ); }
template <typename T> Vector3 <T> Vector3 <T>::yzx() const { return Vector3 <T>( y, z, x ); }
template <typename T> Vector3 <T> Vector3 <T>::yzy() const { return Vector3 <T>( y, z, y ); }
template <typename T> Vector3 <T> Vector3 <T>::yzz() const { return Vector3 <T>( y, z, z ); }
template <typename T> Vector3 <T> Vector3 <T>::zxx() const { return Vector3 <T>( z, x, x ); }
template <typename T> Vector3 <T> Vector3 <T>::zxy() const { return Vector3 <T>( z, x, y ); }
template <typename T> Vector3 <T> Vector3 <T>::zxz() const { return Vector3 <T>( z, x, z ); }
template <typename T> Vector3 <T> Vector3 <T>::zyx() const { return Vector3 <T>( z, y, x ); }
template <typename T> Vector3 <T> Vector3 <T>::zyy() const { return Vector3 <T>( z, y, y ); }
template <typename T> Vector3 <T> Vector3 <T>::zyz() const { return Vector3 <T>( z, y, z ); }
template <typename T> Vector3 <T> Vector3 <T>::zzx() const { return Vector3 <T>( z, z, x ); }
template <typename T> Vector3 <T> Vector3 <T>::zzy() const { return Vector3 <T>( z, z, y ); }
template <typename T> Vector3 <T> Vector3 <T>::zzz() const { return Vector3 <T>( z, z, z ); }
template <typename T> Vector2 <T> Vector3 <T>::xx() const { return Vector2 <T>( x, x ); }
template <typename T> Vector2 <T> Vector3 <T>::xy() const { return Vector2 <T>( x, y ); }
template <typename T> Vector2 <T> Vector3 <T>::xz() const { return Vector2 <T>( x, z ); }
template <typename T> Vector2 <T> Vector3 <T>::yx() const { return Vector2 <T>( y, x ); }
template <typename T> Vector2 <T> Vector3 <T>::yy() const { return Vector2 <T>( y, y ); }
template <typename T> Vector2 <T> Vector3 <T>::yz() const { return Vector2 <T>( y, z ); }
template <typename T> Vector2 <T> Vector3 <T>::zx() const { return Vector2 <T>( z, x ); }
template <typename T> Vector2 <T> Vector3 <T>::zy() const { return Vector2 <T>( z, y ); }
template <typename T> Vector2 <T> Vector3 <T>::zz() const { return Vector2 <T>( z, z ); }

template <typename T> Vector3 <T> vecMax( const Vector3 <T> & v1, const Vector3 <T> & v2 ) {
	return Vector3 <T>( v1.x > v2.x ? v1.x : v2.x,
						v1.y > v2.y ? v1.y : v2.y,
						v1.z > v2.z ? v1.z : v2.z );
}

template <typename T> Vector3 <T> vecMin( const Vector3 <T> & v1, const Vector3 <T> & v2 ) {
	return Vector3 <T>( v1.x < v2.x ? v1.x : v2.x,
						v1.y < v2.y ? v1.y : v2.y,
						v1.z < v2.z ? v1.z : v2.z );
}

#endif