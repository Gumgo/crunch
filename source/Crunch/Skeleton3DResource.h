/** @file Skeleton3DResource.h
 *  @brief Contains the resource holding 3D skeleton data.
 */

#ifndef SKELETON3DRESOURCE_DEFINED
#define SKELETON3DRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "HashMap.h"
#include "HashFunctions.h"
#include <vector>

/** @brief Skeleton 3D resource.
 */
DEFINE_RESOURCE_CLASS( Skeleton3DResource, "skeleton3D", "skeleton3Ds" )
public:
	/** @brief The constructor.
	 */
	Skeleton3DResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief Holds data for a bone in a skeleton.
	 */
	struct BoneData {
		Matrix44af inverseBind;			/**< The inverse bind matrix of this bone.*/
		std::vector <size_t> children;	/**< The child bone IDs.*/
	};

	size_t getBoneCount() const;											/**< Returns the number of bones in the skeleton.*/
	const BoneData & getBoneData( size_t i ) const;							/**< Returns bone data.*/
	bool getBoneIndex( const std::string & name, size_t & index ) const;	/**< Returns the index of the bone with the given name.*/
	size_t getRootBoneCount() const;										/**< Returns the number of root bones in the skeleton.*/
	size_t getRootBoneIndex( size_t i ) const;								/**< Returns the indices of root bones.*/

private:
	std::vector <BoneData> bones;									/**< The bones of the skeleton.*/
	std::vector <size_t> rootBones;									/**< The IDs of the root bones.*/
	HashMap <Hash <std::string>, std::string, size_t> boneNames;	/**< Maps bone names to bone IDs.*/
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <Skeleton3DResource> Skeleton3DResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <Skeleton3DResource> Skeleton3DResourceConstReference;

#endif