/** @file GpuDrawBufferState.h
 *  @brief Encapsulates the state of the which buffers should be drawn to on the GPU.
 */

#ifndef GPUDRAWBUFFERSTATE_DEFINED
#define GPUDRAWBUFFERSTATE_DEFINED

#include "Common.h"
#include "GpuObject.h"

/** @brief Encapsulates the state of the which buffers should be drawn to on the GPU.
 */
class GpuDrawBufferState : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuDrawBufferState();

	/** @brief Sets the state of the GPU to the state represented by this object.
	 */
	void bind();

	/** @brief Sets which buffers are drawn to.
	 */
	bool setDrawBuffers( size_t count, const GLenum * buffers );

	/** @brief Returns the number of buffers being drawn to.
	 */
	size_t getDrawBufferCount() const;

	/** @brief Returns the array of buffers being drawn to.
	 */
	const GLenum * getDrawBuffers() const;

private:
	friend class GpuState;
	GpuDrawBufferState( GpuState * state );

	static const size_t MAX_DRAW_BUFFERS = 16; // also impose GPU limit

	size_t drawBufferCount;
	GLenum drawBuffers[MAX_DRAW_BUFFERS];

	// compares changes against current state and apply differences
	void apply( GpuDrawBufferState & currentState ) const;
	// sets values to the current OpenGL state
	void getCurrentState();
};

#endif