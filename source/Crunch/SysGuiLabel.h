#ifndef SYSGUILABEL_DEFINED
#define SYSGUILABEL_DEFINED

#include "Common.h"
#include "Actor.h"
#include "SysGui.h"

DEFINE_DERIVED_ACTOR( SysGuiLabel, SysGui, 0, 0 )

	Vector2i position;
	std::string text;

public:
	void onCreate();

	void move( const Vector2i & pos );
	void setText( const std::string & t );

	void receiveEvent( const InputEvent & ev );
	void onDismissParent();
	void draw();
};

#endif