#include "Skeleton2DResource.h"
#include "Log.h"

Skeleton2DResource::Skeleton2DResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m ) {
}

bool Skeleton2DResource::loadDataAsync() {
	try {
		// open and load the file
		DataReader in( getPath() );
		if (in.fail())
			throw std::runtime_error( "Failed to open " + getPath() );

		uint partCount;
		if (!in.readElements( &partCount, 1 ))
			throw std::runtime_error( "Failed to read part count" );

		parts.reserve( partCount );
		size_t visibleParts = 0;
		for (uint i = 0; i < partCount; ++i) {
			parts.push_back( PartData() );
			PartData & partData = parts.back();

			uint partNameLength;
			if (!in.readElements( &partNameLength, 1 ))
				throw std::runtime_error( "Failed to read part name length" );
			std::vector <char> partName( (size_t)partNameLength + 1 );
			if (!in.readData( &partName[0], (size_t)partNameLength ))
				throw std::runtime_error( "Failed to read part name" );
			partName.back() = '\0';
			partNames.insert( std::make_pair( &partName[0], (size_t)i ) );

			struct PartTransform {
				float offset[2];
				float rotation;
			} partTransform;
			byte visibility;

			if (!in.readElements( &partTransform, 1 ))
				throw std::runtime_error( "Failed to read part transform data" );
			if (!in.readElements( &visibility, 1 ))
				throw std::runtime_error( "Failed to read part visibility" );

			partData.offset.set( partTransform.offset[0], partTransform.offset[1] );
			partData.rotation = partTransform.rotation;
			partData.visible = visibility != 0;

			if (partData.visible) {
				++visibleParts;
				struct PartImageRegion {
					int imageMin[2];
					int imageMax[2];
					int origin[2];
				} partImageRegion;

				if (!in.readElements( &partImageRegion, 1 ))
					throw std::runtime_error( "Failed to read image region data" );

				partData.imageMin.set( partImageRegion.imageMin[0], partImageRegion.imageMin[1] );
				partData.imageMax.set( partImageRegion.imageMax[0], partImageRegion.imageMax[1] );
				partData.origin.set( partImageRegion.origin[0], partImageRegion.origin[1] );
			}

			uint childCount;
			if (!in.readElements( &childCount, 1 ))
				throw std::runtime_error( "Failed to read child count data" );

			partData.children.reserve( (size_t)childCount );

			for (uint c = 0; c < childCount; ++c) {
				uint childId;
				if (!in.readElements( &childId, 1 ))
					throw std::runtime_error( "Failed to read child part ID data" );
				if (childId >= partCount)
					throw std::runtime_error( "Invalid child part ID" );
				partData.children.push_back( childId );
			}
		}

		partDrawingOrder.reserve( visibleParts );
		for (size_t i = 0; i < visibleParts; ++i) {
			uint partId;
			if (!in.readElements( &partId, 1 ))
				throw std::runtime_error( "Failed to read part order ID data" );
			if ((size_t)partId >= parts.size())
				throw std::runtime_error( "Invalid part order ID" );
			partDrawingOrder.push_back( partId );
		}

		// verify
		std::vector <bool> partsUsed( (size_t)partCount, false );
		for (size_t i = 0; i < partDrawingOrder.size(); ++i) {
			if (partsUsed[partDrawingOrder[i]])
				throw std::runtime_error( "Invalid part drawing order specified" );
			if (!parts[partDrawingOrder[i]].visible)
				throw std::runtime_error( "Invisible part specified in drawing order in" );
			partsUsed[partDrawingOrder[i]] = true;
		}

		partsUsed.assign( partsUsed.size(), false );
		std::stack <size_t> partStack;
		if (!parts.empty())
			partStack.push( 0 );

		while (!partStack.empty()) {
			size_t id = partStack.top();
			partStack.pop();
			if (partsUsed[id])
				throw std::runtime_error( "Invalid child part ID specified" );
			partsUsed[id] = true;
			for (size_t i = 0; i < parts[id].children.size(); ++i)
				partStack.push( parts[id].children[i] );
		}

	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}

	return true;
}

bool Skeleton2DResource::loadDataSync() {
	return true;
}

void Skeleton2DResource::freeData() {
}

size_t Skeleton2DResource::getPartCount() const {
	return parts.size();
}

const Skeleton2DResource::PartData & Skeleton2DResource::getPartData( size_t i ) const {
	return parts[i];
}

bool Skeleton2DResource::getPartIndex( const std::string & name, size_t & index ) const {
	std::map <std::string, size_t>::const_iterator it = partNames.find( name );
	if (it == partNames.end())
		return false;
	else {
		index = it->second;
		return true;
	}
}

size_t Skeleton2DResource::getVisiblePartCount() const {
	return partDrawingOrder.size();
}

size_t Skeleton2DResource::getPartDrawingOrder( size_t i ) const {
	return partDrawingOrder[i];
}