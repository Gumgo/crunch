/** @file Maths_Vector2Dec.h
 *  @brief Contains an implementation of a 2D vector class.
 */

#ifndef MATHS_VECTOR2DEC_DEFINED
#define MATHS_VECTOR2DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for 2D vectors.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Vector2 {
public:
	T x;	/**< The x component.*/
	T y;	/**< The y component.*/

	/** @brief Constructs a 2D vector with all components set to 0.
	 */
	Vector2();

	/** @brief Constructs a 2D vector with the provided components.
	 */
	Vector2( T px, T py );

	/** @brief Constructs a 2D vector by copying the one provided.
	 */
	Vector2( const Vector2 <T> & v );

	/** @brief The destructor.
	 */
	~Vector2();

	// operators

	/** @brief Returns the sum of the current value and the vector v.
	 */
	Vector2 <T> operator+( const Vector2 <T> & v ) const;

	/** @brief Returns the current value unmodified.
	 */
	Vector2 <T> operator+() const;

	/** @brief Returns the difference between the current value and the vector v.
	 */
	Vector2 <T> operator-( const Vector2 <T> & v ) const;

	/** @brief Returns the current value negated.
	 */
	Vector2 <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Vector2 <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Vector2 <T> operator/( T r ) const;

	/** @brief Returns a vector containing the product of each component
	 *  in the current value and the corresponding component in the vector v.
	 */
	Vector2 <T> operator*( const Vector2 <T> & v ) const;

	/** @brief Returns a vector containing the quotient of each component
	 *  in the current value and the corresponding component in the vector v.
	 */
	Vector2 <T> operator/( const Vector2 <T> & v ) const;

	/** @brief Sets the current value to the vector v.
	 */
	Vector2 <T> operator=( const Vector2 <T> & v );

	/** @brief Increments the current value by the vector v.
	 */
	Vector2 <T> operator+=( const Vector2 <T> & v );

	/** @brief Decrements the current value by the vector v.
	 */
	Vector2 <T> operator-=( const Vector2 <T> & v );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Vector2 <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Vector2 <T> operator/=( T r );

	/** @brief Multiplies each component in the current value
	 *  by the corresponding component in the vector v.
	 */
	Vector2 <T> operator*=( const Vector2 <T> & v );

	/** @brief Divides each component in the current value
	 *  by the corresponding component in the vector v.
	 */
	Vector2 <T> operator/=( const Vector2 <T> & v );

	/** @brief Returns whether the current value is equal to the vector v.
	 */
	bool operator==( const Vector2 <T> & v ) const;

	/** @brief Returns whether the current value is not equal to the vector v.
	 */
	bool operator!=( const Vector2 <T> & v ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Vector2 <U> () const;

	// mutable

	/** @brief Sets the current value's components to (px, py).
	 */
	void set( T px, T py );

	/** @brief Normalizes the current value.
	 */
	void normalize();

	/** @brief Sets the magnitude of the current value to the scalar m.
	 */
	void setMagnitude( T m );

	/** @brief Rotates the current value by \htmlonly90&#0176\endhtmlonly.
	 */
	void orthogonalize();

	// immutable

	/** @brief Returns the current value normalized.
	 */
	Vector2 <T> normalized() const;

	/** @brief Returns the magnitude of the current value.
	 */
	T magnitude() const;

	/** @brief Returns the squared magnitude of the current value.
	 */
	T magnitudeSquared() const;

	/** @brief Returns the dot product of the current value and the vector v.
	 */
	T dot( const Vector2 <T> & v ) const;

	/** @brief Returns the current value rotated by \htmlonly90&#0176\endhtmlonly.
	 */
	Vector2 <T> orthogonal() const;

	/** @brief Returns whether all components of the current value are 0.
	 */
	bool isZero() const;

	// swizzling
	Vector4 <T> xxxx() const;	/**< Returns a vector with components of (x, x, x, x).*/
	Vector4 <T> xxxy() const;	/**< Returns a vector with components of (x, x, x, y).*/
	Vector4 <T> xxyx() const;	/**< Returns a vector with components of (x, x, y, x).*/
	Vector4 <T> xxyy() const;	/**< Returns a vector with components of (x, x, y, y).*/
	Vector4 <T> xyxx() const;	/**< Returns a vector with components of (x, y, x, x).*/
	Vector4 <T> xyxy() const;	/**< Returns a vector with components of (x, y, x, y).*/
	Vector4 <T> xyyx() const;	/**< Returns a vector with components of (x, y, y, x).*/
	Vector4 <T> xyyy() const;	/**< Returns a vector with components of (x, y, y, y).*/
	Vector4 <T> yxxx() const;	/**< Returns a vector with components of (y, x, x, x).*/
	Vector4 <T> yxxy() const;	/**< Returns a vector with components of (y, x, x, y).*/
	Vector4 <T> yxyx() const;	/**< Returns a vector with components of (y, x, y, x).*/
	Vector4 <T> yxyy() const;	/**< Returns a vector with components of (y, x, y, y).*/
	Vector4 <T> yyxx() const;	/**< Returns a vector with components of (y, y, x, x).*/
	Vector4 <T> yyxy() const;	/**< Returns a vector with components of (y, y, x, y).*/
	Vector4 <T> yyyx() const;	/**< Returns a vector with components of (y, y, y, x).*/
	Vector4 <T> yyyy() const;	/**< Returns a vector with components of (y, y, y, y).*/
	Vector3 <T> xxx() const;	/**< Returns a vector with components of (x, x, x).*/
	Vector3 <T> xxy() const;	/**< Returns a vector with components of (x, x, y).*/
	Vector3 <T> xyx() const;	/**< Returns a vector with components of (x, y, x).*/
	Vector3 <T> xyy() const;	/**< Returns a vector with components of (x, y, y).*/
	Vector3 <T> yxx() const;	/**< Returns a vector with components of (y, x, x).*/
	Vector3 <T> yxy() const;	/**< Returns a vector with components of (y, x, y).*/
	Vector3 <T> yyx() const;	/**< Returns a vector with components of (y, y, x).*/
	Vector3 <T> yyy() const;	/**< Returns a vector with components of (y, y, y).*/
	Vector2 <T> xx() const;		/**< Returns a vector with components of (x, x).*/
	Vector2 <T> xy() const;		/**< Returns a vector with components of (x, y).*/
	Vector2 <T> yx() const;		/**< Returns a vector with components of (y, x).*/
	Vector2 <T> yy() const;		/**< Returns a vector with components of (y, y).*/
};

/** @brief Returns the product of the scalar r and the vector v.
 */
template <typename T> Vector2 <T> operator*( T r, const Vector2 <T> & v );

/** @brief Returns a vector containing the max of each corresponding component in vectors v1 and v2.
 */
template <typename T> Vector2 <T> vecMax( const Vector2 <T> & v1, const Vector2 <T> & v2 );

/** @brief Returns a vector containing the min of each corresponding component in vectors v1 and v2.
 */
template <typename T> Vector2 <T> vecMin( const Vector2 <T> & v1, const Vector2 <T> & v2 );

/** @brief Vector2 of type float.
 */
typedef Vector2 <float> Vector2f;

/** @brief Vector2 of type double.
 */
typedef Vector2 <double> Vector2d;

/** @brief Vector2 of type int.
 */
typedef Vector2 <int> Vector2i;

#endif