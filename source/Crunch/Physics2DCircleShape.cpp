#include "Physics2DCircleShape.h"
#include "Physics2DBody.h"

namespace Physics2D {

CircleShape::CircleShape( const CircleShapeDefinition & d, Body * par )
	: Shape( d, Shape::T_CIRCLE, par ) {
	radius = d.radius;

	setArea( computeArea() );
}

Real CircleShape::computeArea() const {
	return boost::math::constants::pi <Real>() * radius * radius;
}

void CircleShape::setRadius( Real rad ) {
	radius = rad;
	setArea( computeArea() );
}

Real CircleShape::getRadius() const {
	return radius;
}

Aabb CircleShape::getAabb( const Matrix33ar & transformation ) const {
	Matrix33ar tf = transformation * getTransform(); // world transform * shape transform
	Vector2r center( tf[2], tf[5] );
	return Aabb( center - Vector2r( radius, radius ), center + Vector2r( radius, radius ) );
}

void CircleShape::computeMassData() {
	// moment of inertia through center of mass is (1/2)mr^2
	Real mass = getDensity() * getArea();
	Real inertia = mass*radius*radius*(Real)0.5;
	Vector2r massCtr;
	if (isCenterOfMassOverridden()) {
		inertia += mass*getOverriddenCenterOfMass().magnitudeSquared();
		massCtr = getOverriddenCenterOfMass();
	}
	setMassData( MassData( getTransform().transformPoint( Vector2r() ), mass, inertia ) );
}

CircleShapeDefinition::CircleShapeDefinition() {
	radius = (Real)1;
}

Shape * CircleShapeDefinition::createShape( Body * parent ) const {
	return new CircleShape( *this, parent );
}

}