/** @file GpuBlendState.h
 *  @brief Encapsulates the state of the blending operations on the GPU.
 */

#ifndef GPUBLENDSTATE_DEFINED
#define GPUBLENDSTATE_DEFINED

#include "Common.h"
#include "GpuObject.h"

/** @brief Encapsulates the state of the blending operations on the GPU.
 */
class GpuBlendState : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuBlendState();

	/** @brief Predefined blend modes for convenience.
	 */
	enum BlendMode {
		BM_NONE,				/**< No blending.*/
		BM_ALPHA,				/**< Alpha blending (src_alpha, 1 - src_alpha).*/
		BM_PREMULTIPLIED_ALPHA,	/**< Premultiplied alpha blending (src_alpha, 1).*/
		BM_ADDITIVE,			/**< Additive blending (one, one).*/
		BM_MULTIPLICATIVE,		/**< Multiplicative blending (zero, src_color).*/
		BM_INVERSE,				/**< Inverse blending (1 - dst_color, 1 - src_color).*/
		BLEND_MODE_COUNT		/**< The number of blend modes.*/
	};

	/** @brief Sets the state of the GPU to the state represented by this object.
	 */
	void bind();

	/** @brief Sets Whether blending is enabled.
	 */
	void setBlendingEnabled( bool e );

	/** @brief Sets the blending equation.
	 */
	void setBlendEquation( GLenum equation );

	/** @brief Sets the blending equations for color and alpha.
	 */
	void setBlendEquations( GLenum equationColor, GLenum equationAlpha );

	/** @brief Sets the blending function source and destination factors.
	 */
	void setBlendFunction( GLenum sourceFactor, GLenum destFactor );

	/** @brief Sets the blending function source and destination factors for color and alpha.
	 */
	void setBlendFunctions( GLenum sourceFactorColor, GLenum destFactorColor, GLenum sourceColorAlpha, GLenum destColorAlpha );

	/** @brief Sets the blending function source and destination factors using the predefined blend mode.
	 */
	void setBlendFunction( BlendMode blendMode );

	/** @brief Sets the blending color.
	 */
	void setBlendColor( const Vector4f & color );

	/** @brief Returns whether blending is enabled.
	 */
	bool isBlendingEnabled() const;

	/** @brief Returns the blending equation for color.
	 */
	GLenum getBlendEquationColor() const;

	/** @brief Returns the blending equation for alpha.
	 */
	GLenum getBlendEquationAlpha() const;

	/** @brief Returns the blending function source factor for color.
	 */
	GLenum getBlendFunctionSourceFactorColor() const;

	/** @brief Returns the blending function destination factor for color.
	 */
	GLenum getBlendFunctionDestinationFactorColor() const;

	/** @brief Returns the blending function source factor for alpha.
	 */
	GLenum getBlendFunctionSourceFactorAlpha() const;

	/** @brief Returns the blending function destination factor for alpha.
	 */
	GLenum getBlendFunctionDestinationFactorAlpha() const;

	/** @brief Returns the blending color.
	 */
	Vector4f getBlendColor() const;

private:
	friend class GpuState;
	GpuBlendState( GpuState * state );

	bool blendingEnabled;				// whether blending is enabled
	GLenum blendEquationColor;			// the blend equation for color
	GLenum blendEquationAlpha;			// the blend equation for alpha
	GLenum blendSourceFactorColor;		// the source factor for color
	GLenum blendSourceFactorAlpha;		// the source factor for alpha
	GLenum blendDestinationFactorColor;	// the destination factor for color
	GLenum blendDestinationFactorAlpha;	// the destination factor for alpha
	Vector4f blendColor;				// the blend color

	// compares changes against current state and apply differences
	void apply( GpuBlendState & currentState ) const;
	// sets values to the current OpenGL state
	void getCurrentState();
};

#endif