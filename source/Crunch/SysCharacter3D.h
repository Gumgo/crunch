#ifndef SYSCHARACTER3D_DEFINED
#define SYSCHARACTER3D_DEFINED

#include "Common.h"
#include "Skeleton3DResource.h"
#include "Skeleton3DAnimationResource.h"
#include "SysSkeleton3DAnimationTree.h"
#include <vector>
#include <stack>

class SysCharacter3D {
public:
	SysCharacter3D();
	void setSkeleton( const Skeleton3DResourceReference & skel );
	Skeleton3DResourceReference getSkeleton() const;

	std::vector <Matrix44af> & getPoseMatrices();
	const std::vector <Matrix44af> & getPoseMatrices() const;
	const std::vector <Matrix44af> & getSkinMatrices() const;

	void setSkinMatrices();

private:
	std::vector <Matrix44af> poseMatrices;
	std::vector <Matrix44af> skinMatrices;
	Skeleton3DResourceReference skeleton;
};

#endif