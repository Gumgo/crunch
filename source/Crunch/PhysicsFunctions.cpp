#if 0

#include "PhysicsFunctions.h"

bool intersect( const Vector2f & pt, const Box & b ) {
	return
		pt.x >= b.bounds[0].x && pt.y >= b.bounds[0].y &&
		pt.x <= b.bounds[1].x && pt.y <= b.bounds[1].y;
}

bool intersect( const Box & b0, const Box & b1 ) {
	return
		!(b0.bounds[1].x < b1.bounds[0].x ||
		  b0.bounds[1].y < b1.bounds[0].y ||
		  b0.bounds[0].x > b1.bounds[1].x ||
		  b0.bounds[0].y > b1.bounds[1].y);
}

bool intersect( const LineSegment & ls0, const LineSegment & ls1 ) {
	float ls0p0 = ls0.line.side( ls1.points[0] );
	float ls0p1 = ls0.line.side( ls1.points[1] );
	if (ls0p0 == ls0p1 && ls0p0 != 0.0f)
		return false;
	float ls1p0 = ls1.line.side( ls0.points[0] );
	float ls1p1 = ls1.line.side( ls0.points[1] );
	if (ls1p0 == ls1p1 && ls1p1 != 0.0f)
		return false;
	if (ls0p0 != 0.0f || ls0p1 != 0.0f || ls1p0 != 0.0f || ls1p1 != 0.0f)
		return true;
	else {
		// lie on the same line
		Vector2f lineVec = ls0.line.normal.orthogonal();
		ls0p0 = lineVec.dot( ls0.points[0] );
		ls0p1 = lineVec.dot( ls0.points[1] );
		ls1p0 = lineVec.dot( ls1.points[0] );
		ls1p1 = lineVec.dot( ls1.points[1] );
		return
			(ls0p0 < ls1p0 && ls0p0 < ls1p1 &&
			 ls0p1 < ls1p0 && ls0p1 < ls1p1) ||
			(ls0p0 > ls1p0 && ls0p0 > ls1p1 &&
			 ls0p1 > ls1p0 && ls0p1 > ls1p1);
	}
}

bool intersect( const LineSegment & ls, const Box & b ) {
	// test points inside, test 4 line segment intersections
	return
		intersect( ls.points[0], b ) ||
		intersect( ls.points[1], b ) ||
		intersect( ls, LineSegment( b.bounds[0], Vector2f( b.bounds[1].x, b.bounds[0].y ) ) ) ||
		intersect( ls, LineSegment( Vector2f( b.bounds[1].x, b.bounds[0].y ), b.bounds[1] ) ) ||
		intersect( ls, LineSegment( b.bounds[1], Vector2f( b.bounds[0].x, b.bounds[1].y ) ) ) ||
		intersect( ls, LineSegment( Vector2f( b.bounds[0].x, b.bounds[1].y ), b.bounds[0] ) );
}

float distance2( const Vector2f & pt, const LineSegment & ls, Vector2f & normal ) {
	Vector2f lsVec = ls.line.normal.orthogonal();
	float proj0 = lsVec.dot( ls.points[0] );
	float proj1 = lsVec.dot( ls.points[1] );
	if (proj0 > proj1)
		std::swap( proj0, proj1 );
	float projPt = lsVec.dot( pt );
	if (projPt >= proj0 && projPt <= proj1) {
		// if all the way behind, return 0
		float d = ls.line.distance( pt );
		normal = ls.line.normal;
		return d*d;
	} else {
		float d[2] = {
			(pt - ls.points[0]).magnitudeSquared(),
			(pt - ls.points[1]).magnitudeSquared()
		};
		size_t i = d[0] < d[1] ? 0 : 1;
		normal = pt - ls.points[i];
		return d[i];
	}
}

float distance2( const Vector2f & pt, const LineSegment & ls, Vector2f & normal, Vector2f & closestPoint ) {
	Vector2f lsVec = ls.line.normal.orthogonal();
	float proj0 = lsVec.dot( ls.points[0] );
	float proj1 = lsVec.dot( ls.points[1] );
	if (proj0 > proj1)
		std::swap( proj0, proj1 );
	float projPt = lsVec.dot( pt );
	if (projPt >= proj0 && projPt <= proj1) {
		// if all the way behind, return 0
		float d = ls.line.distance( pt );
		normal = ls.line.normal;
		closestPoint = lerp( ls.points[0], ls.points[1], (projPt-proj0)/(proj1-proj0) );
		return d*d;
	} else {
		float d[2] = {
			(pt - ls.points[0]).magnitudeSquared(),
			(pt - ls.points[1]).magnitudeSquared()
		};
		size_t i = d[0] < d[1] ? 0 : 1;
		normal = pt - ls.points[i];
		closestPoint = ls.points[i];
		return d[i];
	}
}

float distance2( const LineSegment & ls0, const LineSegment & ls1, Vector2f & normal ) {
	if (intersect( ls0, ls1 ))
		return 0.0f;
	float best = std::numeric_limits <float>::max();
	Vector2f norm;
	float s;
	if ((s = distance2( ls0.points[0], ls1, norm )) < best) {
		best = s;
		normal = norm;
	}
	if ((s = distance2( ls0.points[1], ls1, norm )) < best) {
		best = s;
		normal = norm;
	}
	if ((s = distance2( ls1.points[0], ls0, norm )) < best) {
		best = s;
		normal = -norm;
	}
	if ((s = distance2( ls1.points[1], ls0, norm )) < best) {
		best = s;
		normal = -norm;
	}
	return best;
}

float distance2( const LineSegment & ls0, const LineSegment & ls1, Vector2f & normal, Vector2f & closestPoint0, Vector2f & closestPoint1 ) {
	if (intersect( ls0, ls1 ))
		return 0.0f;
	float best = std::numeric_limits <float>::max();
	Vector2f norm;
	Vector2f closest;
	float s;
	if ((s = distance2( ls0.points[0], ls1, norm, closest )) < best) {
		best = s;
		normal = norm;
		closestPoint0 = ls0.points[0];
		closestPoint1 = closest;
	}
	if ((s = distance2( ls0.points[1], ls1, norm, closest )) < best) {
		best = s;
		normal = norm;
		closestPoint0 = ls0.points[1];
		closestPoint1 = closest;
	}
	if ((s = distance2( ls1.points[0], ls0, norm, closest )) < best) {
		best = s;
		normal = -norm;
		closestPoint0 = closest;
		closestPoint1 = ls1.points[0];
	}
	if ((s = distance2( ls1.points[1], ls0, norm, closest )) < best) {
		best = s;
		normal = -norm;
		closestPoint0 = closest;
		closestPoint1 = ls1.points[1];
	}
	return best;
}

float distance2( const Circle & c, const LineSegment & ls, Vector2f & normal ) {
	Vector2f closest = c.position;
	if (ls.line.side( c.position ) > 0.0f)
		closest -= ls.line.normal*c.radius;
	else
		closest += ls.line.normal*c.radius;
	Vector2f lsVec = ls.line.normal.orthogonal();
	float proj0 = lsVec.dot( ls.points[0] );
	float proj1 = lsVec.dot( ls.points[1] );
	if (proj0 > proj1)
		std::swap( proj0, proj1 );
	float projPt = lsVec.dot( closest );
	if (projPt >= proj0 && projPt <= proj1) {
		float d = ls.line.distance( closest );
		normal = ls.line.normal;
		return d*d;
	} else {
		float d[2] = {
			(c.position - ls.points[0]).magnitude() - c.radius,
			(c.position - ls.points[1]).magnitude() - c.radius
		};
		size_t i = d[0] < d[1] ? 0 : 1;
		normal = c.position - ls.points[i];
		return std::max( 0.0f, d[i]*d[i] );
	}
}

float distance2( const Circle & c, const LineSegment & ls, Vector2f & normal, Vector2f & closestPoint ) {
	Vector2f closest = c.position;
	if (ls.line.side( c.position ) > 0.0f)
		closest -= ls.line.normal*c.radius;
	else
		closest += ls.line.normal*c.radius;
	Vector2f lsVec = ls.line.normal.orthogonal();
	float proj0 = lsVec.dot( ls.points[0] );
	float proj1 = lsVec.dot( ls.points[1] );
	bool swap = proj0 > proj1;
	if (swap)
		std::swap( proj0, proj1 );
	float projPt = lsVec.dot( closest );
	if (projPt >= proj0 && projPt <= proj1) {
		float d = ls.line.distance( closest );
		normal = ls.line.normal;
		if (swap)
			closestPoint = lerp( ls.points[1], ls.points[0], (projPt-proj0)/(proj1-proj0) );
		else
			closestPoint = lerp( ls.points[0], ls.points[1], (projPt-proj0)/(proj1-proj0) );
		return d*d;
	} else {
		float d[2] = {
			(c.position - ls.points[0]).magnitude() - c.radius,
			(c.position - ls.points[1]).magnitude() - c.radius
		};
		size_t i = d[0] < d[1] ? 0 : 1;
		normal = c.position - ls.points[i];
		closestPoint = ls.points[i];
		return std::max( 0.0f, d[i]*d[i] );
	}
}

float distance2( const Capsule & c, const LineSegment & ls, Vector2f & normal ) {
	if (c.height == 0.0f) {
		Circle circ;
		circ.position = c.position;
		circ.radius = c.radius;
		return distance2( circ, ls, normal );
	} else {
		// test the 2 circles and the 2 line segments making up the sides
		float halfHeight = c.height * 0.5f;
		// make sure to construct lines with outward normal
		LineSegment ls0(
			Vector2f( c.position.x + c.radius, c.position.y + halfHeight ),
			Vector2f( c.position.x + c.radius, c.position.y - halfHeight ) );
		LineSegment ls1(
			Vector2f( c.position.x - c.radius, c.position.y - halfHeight ),
			Vector2f( c.position.x - c.radius, c.position.y + halfHeight ) );
		float iHeight = 1.0f / c.height;
		// since we know the height of the capsule's line segments, we can quickly normalize without a sqrt
		ls0.line.c *= iHeight;
		ls0.line.normal *= iHeight;
		ls1.line.c *= iHeight;
		ls1.line.normal *= iHeight;

		Circle circ0, circ1;
		circ0.position = circ1.position = c.position;
		circ0.position.y += halfHeight;
		circ1.position.y -= halfHeight;
		circ0.radius = circ1.radius = c.radius;

		float best = std::numeric_limits <float>::max();
		Vector2f norm;
		float s;
		if ((s = distance2( ls.points[0], ls0, norm )) < best) {
			best = s;
			normal = Vector2f( -1.0f, 0.0f );
		}
		if ((s = distance2( ls.points[1], ls0, norm )) < best) {
			best = s;
			normal = Vector2f( -1.0f, 0.0f );
		}
		if ((s = distance2( ls.points[0], ls1, norm )) < best) {
			best = s;
			normal = Vector2f( 1.0f, 0.0f );
		}
		if ((s = distance2( ls.points[1], ls1, norm )) < best) {
			best = s;
			normal = Vector2f( 1.0f, 0.0f );
		}
		// make sure to check case where line is contained
		if (ls.points[0].x >= ls1.points[0].x && ls.points[0].y >= ls1.points[0].y &&
			ls.points[0].x <= ls0.points[0].x && ls.points[0].y <= ls0.points[0].y &&
			ls.points[1].x >= ls1.points[1].x && ls.points[1].y >= ls1.points[1].y &&
			ls.points[1].x <= ls0.points[1].x && ls.points[1].y <= ls0.points[1].y)
			best = 0.0f;
		// make sure to test circles last and have it <=
		// this is because line segments don't overwrite normal if they intersect
		if ((s = distance2( circ0, ls, norm )) <= best) {
			best = s;
			normal = norm;
		}
		if ((s = distance2( circ1, ls, norm )) <= best) {
			best = s;
			normal = norm;
		}
		return best;
	}
}

float distance2( const Capsule & c, const LineSegment & ls, Vector2f & normal, Vector2f & closestPoint, Vector2f & capsulePoint ) {
	if (c.height == 0.0f) {
		Circle circ;
		circ.position = c.position;
		circ.radius = c.radius;
		float d = distance2( circ, ls, normal, closestPoint );
		capsulePoint = circ.position - normal*circ.radius;
		return d;
	} else {
		// test the 2 circles and the 2 line segments making up the sides
		float halfHeight = c.height * 0.5f;
		// make sure to construct lines with outward normal
		LineSegment ls0(
			Vector2f( c.position.x + c.radius, c.position.y + halfHeight ),
			Vector2f( c.position.x + c.radius, c.position.y - halfHeight ) );
		LineSegment ls1(
			Vector2f( c.position.x - c.radius, c.position.y - halfHeight ),
			Vector2f( c.position.x - c.radius, c.position.y + halfHeight ) );
		float iHeight = 1.0f / c.height;
		// since we know the height of the capsule's line segments, we can quickly normalize without a sqrt
		ls0.line.c *= iHeight;
		ls0.line.normal *= iHeight;
		ls1.line.c *= iHeight;
		ls1.line.normal *= iHeight;

		Circle circ0, circ1;
		circ0.position = circ1.position = c.position;
		circ0.position.y += halfHeight;
		circ1.position.y -= halfHeight;
		circ0.radius = circ1.radius = c.radius;

		float best = std::numeric_limits <float>::max();
		Vector2f norm;
		Vector2f closest;
		float s;
		if ((s = distance2( ls.points[0], ls0, norm, closest )) < best) {
			best = s;
			normal = Vector2f( -1.0f, 0.0f );
			closestPoint = closest;
			capsulePoint = ls.points[0];
		}
		if ((s = distance2( ls.points[1], ls0, norm, closest )) < best) {
			best = s;
			normal = Vector2f( -1.0f, 0.0f );
			closestPoint = closest;
			capsulePoint = ls.points[1];
		}
		if ((s = distance2( ls.points[0], ls1, norm, closest )) < best) {
			best = s;
			normal = Vector2f( 1.0f, 0.0f );
			closestPoint = closest;
			capsulePoint = ls.points[0];
		}
		if ((s = distance2( ls.points[1], ls1, norm, closest )) < best) {
			best = s;
			normal = Vector2f( 1.0f, 0.0f );
			closestPoint = closest;
			capsulePoint = ls.points[1];
		}
		// make sure to check case where line is contained
		if (ls.points[0].x >= ls1.points[0].x && ls.points[0].y >= ls1.points[0].y &&
			ls.points[0].x <= ls0.points[0].x && ls.points[0].y <= ls0.points[0].y &&
			ls.points[1].x >= ls1.points[1].x && ls.points[1].y >= ls1.points[1].y &&
			ls.points[1].x <= ls0.points[1].x && ls.points[1].y <= ls0.points[1].y)
			best = 0.0f;
		// make sure to test circles last and have it <=
		// this is because line segments don't overwrite normal if they intersect
		if ((s = distance2( circ0, ls, norm, closest )) <= best) {
			best = s;
			normal = norm;
			closestPoint = closest;
			capsulePoint = circ0.position - normal.normalized() * circ0.radius;
		}
		if ((s = distance2( circ1, ls, norm, closest )) <= best) {
			best = s;
			normal = norm;
			closestPoint = closest;
			capsulePoint = circ1.position - normal.normalized() * circ1.radius;
		}
		return best;
	}
}

float sweep( const Vector2f & pt, const Vector2f & v, const LineSegment & ls, Vector2f & isectNormal, Vector2f & isectPoint ) {
	isectNormal = ls.line.normal;
	float dist;
	if (ls.line.intersects( pt, v, isectPoint, dist )) {
		// if intersection is behind or greater than v's magnitude, we can safely move forward
		if (dist < 0.0f || dist >= 1.0f)
			return 1.0f;
		// test to make sure the intersection point is actually on the line
		// do this by projecting the line's points onto the line vector and seeing if this point falls between them
		Vector2f lineVec = ls.line.normal.orthogonal();
		float proj0 = lineVec.dot( ls.points[0] );
		float proj1 = lineVec.dot( ls.points[1] );
		float projIsect = lineVec.dot( isectPoint );
		// order so that proj0 is less
		if (proj0 > proj1)
			std::swap( proj0, proj1 );
		// if isect is between, there was an intersection so return dist up to that intersection
		// otherwise, no intersection, so return 1
		if (projIsect >= proj0 && projIsect <= proj1)
			return dist;
		else
			return 1.0f;
	}

	return 1.0f;
}

float sweep( const LineSegment & ls, const Vector2f & v, const Vector2f & pt, Vector2f & isectNormal, Vector2f & isectPoint ) {
	// instead of sweeping line, raycast point into line and check bounds
	Vector2f placeholder;
	isectPoint = pt;
	return sweep( pt, -v, ls, isectNormal, placeholder );
}

float sweep( const Circle & c, const Vector2f & v, const LineSegment & ls, Vector2f & isectNormal, Vector2f & isectPoint ) {
	// find the point that would intersect the line segment first
	Vector2f closest = c.position - ls.line.normal*c.radius;
	float closestSweep = sweep( closest, v, ls, isectNormal, isectPoint );
	if (closestSweep < 1.0f)
		// if the closest point to the line hits, return that
		return closestSweep;
	else {
		float bestT = 1.0f;
		size_t bestI = 2; // invalid initial value
		// otherwise, sweep the circle against the two endpoints
		for (size_t i = 0; i < 2; ++i) {
			// want to find when distance between line segment endpoint and circle center is circle radius
			// get rid of line segment point by subtracting it
			Vector2f pt = c.position - ls.points[i];
			// P = pt + v*t
			// r = mag( pt + v*t )
			// r = sqrt( (pt + v*t).x^2 + (pt + v*t).y^2 )
			// r = sqrt( (pt + v*t).(pt + v*t) )
			// r^2 = (pt + v*t).(pt + v*t)
			// r^2 = pt.pt + t*(2*pt.v) + t^2*(v.v)
			// t^2(v.v) + t(2pt.v) + (pt.pt - r^2) = 0
			float pa = v.dot( v );
			float pb = 2.0f * pt.dot( v );
			float pc = pt.dot( pt ) - c.radius*c.radius;
			// t = (-b +- sqrt( b^2 - 4ac )) / 2a
			float disc = pb * pb - 4.0f * pa * pc;
			// if < 0, no intersection
			if (disc < 0.0f)
				continue;
			else if (disc > 0.0f)
				disc = sqrt( disc );
			float p2a = 2.0f * pa;
			float t[2] = {
				(-pb + disc) / p2a,
				(-pb - disc) / p2a
			};
			if (t[0] > t[1])
				std::swap( t[0], t[1] );
			if (t[0] >= 0.0f) {
				if (t[0] < bestT) {
					bestT = t[0];
					bestI = i;
				}
			} else if (t[1] >= 0.0f) {
				if (t[1] < bestT) {
					bestT = t[1];
					bestI = i;
				}
			}
		}
		if (bestI != 2) {
			isectNormal = ((c.position + v * bestT) - ls.points[bestI]) / c.radius;
			isectPoint = ls.points[bestI];
		}
		return bestT;
	}
}

float sweep( const Capsule & c, const Vector2f & v, const LineSegment & ls, Vector2f & isectNormal, Vector2f & isectPoint ) {
	if (c.height == 0.0f) {
		Circle circ;
		circ.position = c.position;
		circ.radius = c.radius;
		return sweep( circ, v, ls, isectNormal, isectPoint );
	} else {
		// sweep the 2 circles and the 2 line segments making up the sides
		float halfHeight = c.height * 0.5f;
		LineSegment ls0(
			Vector2f( c.position.x + c.radius, c.position.y - halfHeight ),
			Vector2f( c.position.x + c.radius, c.position.y + halfHeight ) );
		LineSegment ls1(
			Vector2f( c.position.x - c.radius, c.position.y - halfHeight ),
			Vector2f( c.position.x - c.radius, c.position.y + halfHeight ) );
		float iHeight = 1.0f / c.height;
		// since we know the height of the capsule's line segments, we can quickly normalize without a sqrt
		ls0.line.c *= iHeight;
		ls0.line.normal *= iHeight;
		ls1.line.c *= iHeight;
		ls1.line.normal *= iHeight;

		Circle circ0, circ1;
		circ0.position = circ1.position = c.position;
		circ0.position.y += halfHeight;
		circ1.position.y -= halfHeight;
		circ0.radius = circ1.radius = c.radius;

		float best = 1.0f;
		Vector2f norm;
		Vector2f point;
		float s;
		if ((s = sweep( circ0, v, ls, norm, point )) < best) {
			best = s;
			isectNormal = norm;
			isectPoint = point;
		}
		if ((s = sweep( circ1, v, ls, norm, point )) < best) {
			best = s;
			isectNormal = norm;
			isectPoint = point;
		}
		if ((s = sweep( ls0, v, ls.points[0], norm, point )) < best) {
			best = s;
			isectNormal = norm;
			isectPoint = point;
		}
		if ((s = sweep( ls0, v, ls.points[1], norm, point )) < best) {
			best = s;
			isectNormal = norm;
			isectPoint = point;
		}
		if ((s = sweep( ls1, v, ls.points[0], norm, point )) < best) {
			best = s;
			isectNormal = norm;
			isectPoint = point;
		}
		if ((s = sweep( ls1, v, ls.points[1], norm, point )) < best) {
			best = s;
			isectNormal = norm;
			isectPoint = point;
		}
		return best;
	}
}

// given:
// a normal vector
// an origin
// a point on an object
// a distance
// returns the vector required to move point such that it is distance units in front of the line defined by (origin, normal)
Vector2f moveOutToDist( const Vector2f & normal, const Vector2f & origin, const Vector2f & point, float distance ) {
	// push the point out
	// "translate" the line to the origin (point - origin)
	// dot with the normal to find how far back it is
	// negate (or just flip order of subtraction) to find how far to move to the line segment
	// add the desired distance from the line segment to that distance
	float distToMove = normal.dot( origin - point ) + distance;
	// return this times the vector we'll be moving in (the normal)
	return normal * distToMove;
}

void moveOut( const Capsule & c, const LineSegment & ls, float dist, Vector2f & moveVec ) {
	Vector2f norm;
	Vector2f closestPoint;
	Vector2f capsulePoint;
	float lsDist = distance2( c, ls, norm, closestPoint, capsulePoint );
	norm.normalize();
	if (lsDist < dist*dist)
		moveVec = moveOutToDist( norm, closestPoint, capsulePoint, dist );
	else
		moveVec.set( 0.0f, 0.0f );
}

bool moveOut( const Capsule & c, const LineSegment & ls0, const LineSegment & ls1, float dist, Vector2f & moveVec ) {
	Vector2f norm[2];
	Vector2f closestPoint[2];
	Vector2f capsulePoint[2];
	float lsDist[2] = {
		distance2( c, ls0, norm[0], closestPoint[0], capsulePoint[0] ),
		distance2( c, ls1, norm[1], closestPoint[1], capsulePoint[1] )
	};
	float dist2 = dist*dist;
	bool less[2] = {
		lsDist[0] < dist2,
		lsDist[1] < dist2
	};
	if (!less[0] && !less[1]) {
		// both are fine
		moveVec.set( 0.0f, 0.0f );
		return true;
	} else if (less[0] != less[1]) {
		// only one of them needs to be moved
		size_t i = less[0] ? 0 : 1;
		norm[i].normalize();
		moveVec = moveOutToDist( norm[i], closestPoint[i], capsulePoint[i], dist );
		return true;
	} else {
		norm[0].normalize();
		norm[1].normalize();
		// else both need to be moved
		// push the lines forward by dist
		closestPoint[0] += norm[0] * dist;
		closestPoint[1] += norm[1] * dist;
		// vector containing difference between the two points
		Vector2f diffVec = capsulePoint[1] - capsulePoint[0];
		// now we slide point 0 down line 0 until point0+diffVec (which is the same distance as point0 is to point1) hits line 1
		Vector2f lineVec = norm[0].orthogonal();
		// p0' = p0 + lineVec*t
		// p1' = p0 + diffVec + lineVec*t
		// want to find when the distance from line to p1' is 0
		// line1 defined by closestPoint[1] and norm[1] (p1 and n1)
		// dist to line = n.(p-o)
		// 0 = n1 . (p1' - p1)
		// 0 = n1 . (p0 + diffVec + lineVec*t - p1)
		// 0 = n1.(p0 + diffVec - p1) + n1.lineVec*t
		// -n1.(p0 + diffVec - p1) = n1.lineVec*t
		// -n1.(p0 + diffVec - p1) / n1.lineVec = t
		float denom = norm[1].dot( lineVec );
		if (denom == 0.0f) {
			// there's a chance they're actually the same line
			// in this case, both closest points will be the same
			if (norm[0].dot( norm[1] ) > 1.0f - epsilon <float>() &&	// if normals are same,
				abs( closestPoint[0].dot( norm[0] ) - closestPoint[1].dot( norm[1] ) ) < epsilon <float>()) {
				moveVec = moveOutToDist( norm[0], closestPoint[0], capsulePoint[0], dist );
				return true;
			} else
				return false;
		}
		float t = -norm[1].dot( closestPoint[0] + diffVec - closestPoint[1] ) / denom;
		// now that we have t, find the point on line0 that it corresponds to
		Vector2f capsulePoint0Dest = closestPoint[0] + lineVec*t;
		// the difference between this and the original point0 is the move vector
		moveVec = capsulePoint0Dest - capsulePoint[0];
		return true;
	}
}

Box getBounds( const Capsule & c, float padding ) {
	Box bounds;
	float halfHeight = c.height * 0.5f;
	bounds.bounds[0] = Vector2f(
		c.position.x - c.radius - padding,
		c.position.y - c.radius - halfHeight - padding );
	bounds.bounds[1] = Vector2f(
		c.position.x + c.radius + padding,
		c.position.y + c.radius + halfHeight + padding );
	return bounds;
}

bool raycast( const Vector2f & o, const Vector2f & d, const LineSegment & ls, float & t ) {
	if (d.dot( ls.line.normal ) <= 0.0f)
		return false;
	if (!ls.line.intersects( o, d, t ) || t < 0.0f) // t should never be < 0 but just in case of float weirdness
		return false;
	Vector2f pt = o + t*d;
	Vector2f lineVec = ls.line.normal.orthogonal();
	float proj0 = lineVec.dot( ls.points[0] );
	float proj1 = lineVec.dot( ls.points[0] );
	if (proj1 < proj0)
		std::swap( proj0, proj1 );
	float projPt = lineVec.dot( pt );
	return (projPt >= proj0 && projPt <= proj1);
}

bool raycast( const Vector2f & o, const Vector2f & d, const Circle & c, float & t ) {
	Vector2f newO = o - c.position;
	// solve for when dist2 is rad*rad
	// p = o + td
	// (o + td).(o + td) = rad^2
	// o.o + 2o.td + td.td = rad^2
	// t^2(d.d) + t(2o.d) + (o.o - rad^2) = 0
	// then use quadratic formula
	float pA = d.dot( d );
	float pB = 2.0f * newO.dot( d );
	float pC = newO.dot( newO ) - c.radius*c.radius;
	float disc = pB*pB - 4.0f*pA*pC;
	if (disc < 0.0f)
		return false; // no hit
	disc = sqrt( disc );
	float p2AInv = 1.0f / (2.0f * pA);
	float ts[2] = {
		(-pB + disc) * p2AInv,
		(-pB - disc) * p2AInv
	};
	if (ts[0] > ts[1])
		std::swap( ts[0], ts[1] );
	if (ts[1] < 0.0f)
		return false;
	else {
		// either inside (0) or ts[0]
		t = std::max( 0.0f, ts[0] );
		return true;
	}
}

bool raycast( const Vector2f & o, const Vector2f & d, const Capsule & c, float & t ) {
	if (c.height == 0) {
		Circle circ;
		circ.position = c.position;
		circ.radius = c.radius;
		return raycast( o, d, circ, t );
	}

	float halfHeight = c.height * 0.5f;
	LineSegment ls(
		Vector2f( c.position.x, c.position.y - halfHeight ),
		Vector2f( c.position.x, c.position.y + halfHeight ) );
	float iHeight = 1.0f / c.height;
	ls.line.c *= iHeight;
	ls.line.normal *= iHeight;

	Vector2f norm;
	if (distance2( o, ls, norm ) <= c.radius*c.radius) {
		// if inside, return 0
		t = 0.0f;
		return true;
	}

	// make sure to construct lines with outward normal
	LineSegment ls0(
		Vector2f( c.position.x + c.radius, c.position.y + halfHeight ),
		Vector2f( c.position.x + c.radius, c.position.y - halfHeight ) );
	LineSegment ls1(
		Vector2f( c.position.x - c.radius, c.position.y - halfHeight ),
		Vector2f( c.position.x - c.radius, c.position.y + halfHeight ) );
	// since we know the height of the capsule's line segments, we can quickly normalize without a sqrt
	ls0.line.c *= iHeight;
	ls0.line.normal *= iHeight;
	ls1.line.c *= iHeight;
	ls1.line.normal *= iHeight;

	Circle circ0, circ1;
	circ0.position = circ1.position = c.position;
	circ0.position.y += halfHeight;
	circ1.position.y -= halfHeight;
	circ0.radius = circ1.radius = c.radius;

	t = std::numeric_limits <float>::max();
	bool found = false;
	float time;

	if (raycast( o, d, circ0, time ) && time < t) {
		t = time;
		found = true;
	}
	if (raycast( o, d, circ1, time ) && time < t) {
		t = time;
		found = true;
	}
	if (raycast( o, d, ls0, time ) && time < t) {
		t = time;
		found = true;
	}
	if (raycast( o, d, ls1, time ) && time < t) {
		t = time;
		found = true;
	}

	return found;
}

#endif