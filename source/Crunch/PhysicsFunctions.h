#if 0

/** @file PhysicsFunctions.h
 *  @brief Defines physics functions.
 */

#ifndef PHYSICSFUNCTIONS_DEFINED
#define PHYSICSFUNCTIONS_DEFINED

#include "Common.h"
#include "PhysicsObjects.h"

/** @brief Returns whether a point intersects a box.
 */
bool intersect( const Vector2f & pt, const Box & b );

/** @brief Returns whether two boxes intersect.
 */
bool intersect( const Box & b0, const Box & b1 );

/** @brief Returns whether two line segments intersect.
 */
bool intersect( const LineSegment & ls0, const LineSegment & ls1 );

/** @brief Returns whether a line segment and a box intersect.
 */
bool intersect( const LineSegment & ls, const Box & b );

/** @brief Returns the shortest distance squared between a point and a line segment.
 *  @note Assumes the line segment is normalized.
 */
float distance2( const Vector2f & pt, const LineSegment & ls, Vector2f & normal );

/** @brief Returns the shortest distance squared between a point and a line segment.
 *  @note Assumes the line segment is normalized.
 */
float distance2( const Vector2f & pt, const LineSegment & ls, Vector2f & normal, Vector2f & closestPoint );

/** @brief Returns the shortest distance squared between two line segments.
 *  @note Assumes the line segments are normalized.
 */
float distance2( const LineSegment & ls0, const LineSegment & ls1, Vector2f & normal );

/** @brief Returns the shortest distance squared between two line segments.
 *  @note Assumes the line segments are normalized.
 */
float distance2( const LineSegment & ls0, const LineSegment & ls1, Vector2f & normal, Vector2f & closestPoint0, Vector2f & closestPoint1 );

/** @brief Returns the shortest distance squared between a circle and a line segment.
 *  @note Assumes the line segment is normalized.
 *  @note The resulting normal is not normalized.
 */
float distance2( const Circle & c, const LineSegment & ls, Vector2f & normal );

/** @brief Returns the shortest distance squared between a circle and a line segment.
 *  @note Assumes the line segment is normalized.
 *  @note The resulting normal is not normalized.
 */
float distance2( const Circle & c, const LineSegment & ls, Vector2f & normal, Vector2f & closestPoint );

/** @brief Returns the shortest distance squared between a capsule and a line segment.
 *  @note Assumes the line segment is normalized.
 *  @note The resulting normal is not normalized.
 */
float distance2( const Capsule & c, const LineSegment & ls, Vector2f & normal );

/** @brief Returns the shortest distance squared between a capsule and a line segment.
 *  @note Assumes the line segment is normalized.
 *  @note The resulting normal is not normalized.
 */
float distance2( const Capsule & c, const LineSegment & ls, Vector2f & normal, Vector2f & closestPoint, Vector2f & capsulePoint );

/** @brief Sweeps a point along the given vector and returns the amount traveled before hitting the line segment, ranging from 0 to 1.
 *  @note Assumes the line segment is normalized.
 */
float sweep( const Vector2f & pt, const Vector2f & v, const LineSegment & ls, Vector2f & isectNormal, Vector2f & isectPoint );

/** @brief Sweeps a line segment along the given vector and returns the amount traveled before hitting the point, ranging from 0 to 1.
 *  @note Assumes the line segment is normalized.
 */
float sweep( const LineSegment & ls, const Vector2f & v, const Vector2f & pt, Vector2f & isectNormal, Vector2f & isectPoint );

/** @brief Sweeps a circle along the given vector and returns the amount traveled before hitting the line segment, ranging from 0 to 1.
 *  @note Assumes the line segment is normalized.
 */
float sweep( const Circle & c, const Vector2f & v, const LineSegment & ls, Vector2f & isectNormal, Vector2f & isectPoint );

/** @brief Sweeps a capsule along the given vector and returns the amount traveled before hitting the line segment, ranging from 0 to 1.
 *  @note Assumes the line segment is normalized.
 */
float sweep( const Capsule & c, const Vector2f & v, const LineSegment & ls, Vector2f & isectNormal, Vector2f & isectPoint );

/** @brief Calculates the vector which will move the capsule the given distance away from the line segment in the most efficient way.
 *  @note Assumes the line segment is normalized.
 */
void moveOut( const Capsule & c, const LineSegment & ls, float dist, Vector2f & moveVec );

/** @brief Calculates the vector which will move the capsule out of the both line segments in the most efficient way.
 *  @note Assumes the line segment is normalized.
 */
bool moveOut( const Capsule & c, const LineSegment & ls0, const LineSegment & ls1, float dist, Vector2f & moveVec );

/** @brief Returns the bounds of a capsule with some padding added.
 */
Box getBounds( const Capsule & c, float padding );

/** @brief Casts a ray against a line segment and returns whether there was an intersection, storing the time if so.
 */
bool raycast( const Vector2f & o, const Vector2f & d, const LineSegment & ls, float & t );

/** @brief Casts a ray against a circle and returns whether there was an intersection, storing the time if so.
 */
bool raycast( const Vector2f & o, const Vector2f & d, const Circle & c, float & t );

/** @brief Casts a ray against a capsule and returns whether there was an intersection, storing the time if so.
 */
bool raycast( const Vector2f & o, const Vector2f & d, const Capsule & c, float & t );

#endif

#endif