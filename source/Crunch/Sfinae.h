/** @file Sfinae.h
 *  @brief Provides SFINAE-related utilities.
 */

#ifndef SFINAE_DEFINED
#define SFINAE_DEFINED

/** @brief Creates a templated struct used to determine whether a class contains the given function.
 */
#define CLASS_HAS_FUNCTION( name, func )									\
template <typename T, typename S>											\
struct name {																\
	typedef char Yes[1];													\
	typedef char No[2];														\
	template <typename U, U> struct TypeCheck;								\
	template <typename C> static Yes & check( TypeCheck <S, &C::func> * );	\
	template <typename> static No & check( ... );							\
	static const bool value = sizeof( check <T>( 0 ) ) == sizeof( Yes );	\
};

#endif