#include "DataWriter.h"

DataWriter::DataWriter( size_t bufferSize ) {
	data.reserve( bufferSize );
}

void DataWriter::writeData( void * src, size_t amount ) {
	if (amount > 0) {
		size_t writePos = data.size();
		data.resize( data.size() + amount );
		memcpy( &data[writePos], src, amount );
	}
}

DataWriter::Status DataWriter::writeToFile( const std::string & fname ) {
	std::ofstream file( fname, std::ios::binary );
	if (!file.is_open())
		return ST_FILE_OPEN_FAILURE;

	if (!data.empty()) {
		file.write( &data[0], data.size() );
		if (file.fail())
			return ST_FILE_WRITE_FAILURE;
	}

	return ST_SUCCESS;
}