/** @file Audio.h
 *  @brief Includes audio headers.
 */

#ifndef AUDIO_DEFINED
#define AUDIO_DEFINED

#include "Common.h"
#include "SoundResource.h"
#include <fmod.h>
#include <fmod.hpp>

class AudioManager;
class Sound;
class Sound2D;
class Sound3D;

/** @brief A group of sounds.
 */
class SoundGroup {
public:
	/** @brief Creates a 2D sound.
	 */
	Sound2D createSound2D( SoundResourceReference & sound, bool createPaused = false );

	/** @brief Creates a 3D sound.
	 */
	Sound3D createSound3D( SoundResourceReference & sound, const Vector3f & pos, const Vector3f & vel, bool createPaused = false );

	void play();					/**< Plays/unpauses all sounds in the sound group.*/
	void pause();					/**< Pauses all sounds in the sound group.*/
	void stop();					/**< Stops all sounds in the sound group.*/
	void setVolume( float vol );	/**< Scales the volume of all sounds in the sound group.*/

	friend class AudioManager;

public:
	AudioManager * audioManager;		// pointer to the audio manager
	FMOD::ChannelGroup * channelGroup;	// the FMOD channel group

	SoundGroup( AudioManager * am, FMOD::ChannelGroup * chg );
};

/** @brief An active sound.
 */
class Sound {
public:
	void play();					/**< Plays/unpauses the sound.*/
	void pause();					/**< Pauses the sound.*/
	void stop();					/**< Stops the sound.*/
	void setVolume( float vol );	/**< Sets the sound's volume.*/

	friend class AudioManager;
	friend class SoundGroup;

protected:
	// called only by subclasses
	Sound( AudioManager * am, FMOD::Channel * ch );

	AudioManager * audioManager;	// pointer to the audio manager
	FMOD::Channel * channel;		// the FMOD channel

};

/** @brief An active sound positioned in 2D.
 */
class Sound2D : public Sound {
public:
	void setPanning( float pan );	/**< Sets the sound's panning.*/

	friend class AudioManager;
	friend class SoundGroup;

private:
	Sound2D( AudioManager * am, FMOD::Channel * ch );
};

/** @brief An active sound positioned in 3D.
 */
class Sound3D : public Sound {
public:
	/** @brief Sets the sound's position in meters and velocity in meters per second.
	 */
	void setPositionVelocity( const Vector3f & pos, const Vector3f & vel );

	friend class AudioManager;
	friend class SoundGroup;

private:
	Sound3D( AudioManager * am, FMOD::Channel * ch );
};

#endif