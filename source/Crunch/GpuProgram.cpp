#include "GpuProgram.h"
#include "GpuState.h"
#include "Log.h"

GpuProgram::ProgramDescription::ProgramDescription() {
	vertexShaderSource = NULL;
	vertexShaderSourceCount = 1;
	geometryShaderSource = NULL;
	geometryShaderSourceCount = 1;
	fragmentShaderSource = NULL;
	fragmentShaderSourceCount = 1;
}

GpuProgram::GpuProgram( GpuState * gpuState, const ProgramDescription & desc )
	: ReferenceCountedGpuObject( gpuState )
	, uniformMap( 1 )
	, attributeMap( 1 ) {
	std::fill( shaderIds, shaderIds + arraySize( shaderIds ), 0 );
	programId = 0;

	static const char * SHADER_NAMES[SHADER_TYPE_COUNT] = {
		"vertex", "geometry", "fragment"
	};
	static const GLenum SHADER_TYPES[SHADER_TYPE_COUNT] = {
		GL_VERTEX_SHADER, GL_GEOMETRY_SHADER, GL_FRAGMENT_SHADER
	};
	const char ** shaderSources[SHADER_TYPE_COUNT] = {
		desc.vertexShaderSource, desc.geometryShaderSource, desc.fragmentShaderSource
	};
	size_t shaderSourceCounts[SHADER_TYPE_COUNT] = {
		desc.vertexShaderSourceCount, desc.geometryShaderSourceCount, desc.fragmentShaderSourceCount
	};

	try {
		for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i) {
			if (shaderSources[i] == NULL)
				continue;

			shaderIds[i] = glCreateShader( SHADER_TYPES[i] );
			glShaderSource( shaderIds[i], (GLsizei)shaderSourceCounts[i], shaderSources[i], NULL );
			glCompileShader( shaderIds[i] );

			// get compile results
			int logLength;
			glGetShaderiv( shaderIds[i], GL_INFO_LOG_LENGTH, &logLength );
			if (logLength > 0) {
				std::vector <char> logContent( logLength );
				glGetShaderInfoLog( shaderIds[i], logLength, NULL, &logContent[0] );
				std::string logString = &logContent[0];
				Log::info() << logString;
			}

			int compileStatus;
			glGetShaderiv( shaderIds[i], GL_COMPILE_STATUS, &compileStatus );
			if (compileStatus == GL_FALSE)
				throw std::runtime_error( "Failed to compile " + std::string( SHADER_NAMES[i] ) + " shader" );
		}

		programId = glCreateProgram();
		for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i) {
			if (shaderIds[i] != 0)
				glAttachShader( programId, shaderIds[i] );
		}
		glLinkProgram( programId );

		// get link results
		int logLength;
		glGetProgramiv( programId, GL_INFO_LOG_LENGTH, &logLength );
		if (logLength > 0) {
			std::vector <char> logContent( logLength );
			glGetProgramInfoLog( programId, logLength, NULL, &logContent[0] );
			std::string logString = &logContent[0];
			Log::info() << logString;
		}

		int linkStatus;
		glGetProgramiv( programId, GL_LINK_STATUS, &linkStatus );
		if (linkStatus != GL_TRUE)
			throw std::runtime_error( "Failed to link program" );

		// set up the uniforms

		uint uniformCount;
		glGetProgramiv( programId, GL_ACTIVE_UNIFORMS, (GLint*)&uniformCount );
		uint maxUniformLength;
		glGetProgramiv( programId, GL_ACTIVE_UNIFORM_MAX_LENGTH, (GLint*)&maxUniformLength );
		std::vector <char> uniformNameBuffer( (size_t)maxUniformLength );

		uniformData.reserve( uniformCount );
		uniformLocations.reserve( uniformCount ); // arrays will need more
		uniformMap.resize( uniformCount * 2 + 1 );
		size_t requiredCacheSize = 0;
		for (uint i = 0; i < uniformCount; ++i) {
			int arrayCount;
			GLenum type;
			glGetActiveUniform( programId, i, maxUniformLength, NULL, &arrayCount, &type, &uniformNameBuffer[0] );
			size_t nameLength = strlen( &uniformNameBuffer[0] );
			// check for built in uniform names (they start with "gl_")
			// we want to ignore these
			if (nameLength >= 3 &&
				uniformNameBuffer[0] == 'g' &&
				uniformNameBuffer[1] == 'l' &&
				uniformNameBuffer[2] == '_')
				continue;

			UniformData ud;
			ud.desc.name = &uniformNameBuffer[0];
			// remove array notation - arrays might end in "[0]"
			if (ud.desc.name.back() == ']') {
				while (!ud.desc.name.empty()) {
					if (ud.desc.name.back() == '[') {
						ud.desc.name.pop_back();
						break;
					} else
						ud.desc.name.pop_back();
				}
			}
			ud.desc.type = type;
			ud.desc.arrayCount = (size_t)arrayCount;
			ud.typeDescription = getGlslUniformTypeDescription( type );
			ud.locationIndex = uniformLocations.size();
			if (ud.desc.arrayCount == 1)
				uniformLocations.push_back( glGetUniformLocation( programId, ud.desc.name.c_str() ) );
			else {
				// query the location of each element
				// use array [t] notation
				for (size_t t = 0; t < ud.desc.arrayCount; ++t)
					uniformLocations.push_back( glGetUniformLocation( programId, (ud.desc.name + '[' + toString( t ) + ']').c_str() ) );
			}

			// add to required cache size
			// ensure alignment
			size_t baseTypeSize = getGlslUniformBaseTypeSize( ud.typeDescription.baseType );
			requiredCacheSize = ((requiredCacheSize + baseTypeSize - 1) / baseTypeSize) * baseTypeSize;
			ud.cacheOffset = requiredCacheSize;
			requiredCacheSize += baseTypeSize * ud.typeDescription.elements * ud.desc.arrayCount;

			// add uniform
			uniformMap.put( ud.desc.name, uniformData.size() );
			uniformData.push_back( ud );
		}

		// set up initial values in the cache
		uniformCache.resize( (requiredCacheSize + sizeof( uniformCache[0] ) - 1) / sizeof( uniformCache[0] ) );
		for (size_t i = 0; i < uniformData.size(); ++i) {
			const UniformData & ud = uniformData[i];
			byte * cache = ((byte*)&uniformCache[0]) + ud.cacheOffset;
			for (size_t t = 0; t < ud.desc.arrayCount; ++t) {
				GLint loc = uniformLocations[ud.locationIndex + t];
				std::string name;
				// use array syntax if an array
				if (ud.desc.arrayCount == 1)
					name = ud.desc.name;
				else
					name = ud.desc.name + '[' + toString( t ) + ']';
				switch (ud.typeDescription.baseType) {
				case GL_FLOAT:
					glGetUniformfv( programId, loc, (float*)cache );
					break;
				case GL_INT:
				case GL_BOOL:
					glGetUniformiv( programId, loc, (int*)cache );
					break;
				case GL_UNSIGNED_INT:
					glGetUniformuiv( programId, loc, (uint*)cache );
					break;
				}
			}
		}

		// set up attributes

		uint attributeCount;
		glGetProgramiv( programId, GL_ACTIVE_ATTRIBUTES, (int*)&attributeCount );
		uint attributeMaxLength;
		glGetProgramiv( programId, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, (int*)&attributeMaxLength );
		std::vector <char> attributeNameBuffer( (size_t)attributeMaxLength );

		attributeData.reserve( attributeCount );
		attributeMap.resize( (size_t)attributeCount * 2 + 1 );
		for (uint i = 0; i < attributeCount; ++i) {
			int arrayCount;
			GLenum type;
			glGetActiveAttrib( programId, i, attributeMaxLength, NULL, &arrayCount, &type, &attributeNameBuffer[0] );
			size_t nameLength = strlen( &attributeNameBuffer[0] );
			// check for built in uniform names (they start with "gl_")
			// we want to ignore these
			if (nameLength >= 3 &&
				attributeNameBuffer[0] == 'g' &&
				attributeNameBuffer[1] == 'l' &&
				attributeNameBuffer[2] == '_')
				continue;

			AttributeData ad;
			ad.desc.name = &attributeNameBuffer[0];
			ad.desc.type = type;
			ad.desc.arrayCount = (size_t)arrayCount;
			ad.typeDescription = getGlslAttributeTypeDescription( type );
			ad.location = glGetAttribLocation( programId, ad.desc.name.c_str() );

			// add attribute
			attributeMap.put( ad.desc.name, attributeData.size() );
			attributeData.push_back( ad );
		}
	} catch (const std::exception &) {
		cleanup();
		throw;
	}
}

GpuProgram::~GpuProgram() {
	cleanup();
}

void GpuProgram::cleanup() {
	if (programId != 0) {
		for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i) {
			if (shaderIds[i] != 0)
				glDetachShader( programId, shaderIds[i] );
		}
		glDeleteProgram( programId );
	}
	for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i) {
		if (shaderIds[i] != 0)
			glDeleteShader( shaderIds[i] );
	}
}

GLuint GpuProgram::getVertexShaderId() const {
	return shaderIds[ST_VERTEX_SHADER];
}

GLuint GpuProgram::getGeometryShaderId() const {
	return shaderIds[ST_GEOMETRY_SHADER];
}

GLuint GpuProgram::getFragmentShaderId() const {
	return shaderIds[ST_FRAGMENT_SHADER];
}

GLuint GpuProgram::getProgramId() const {
	return programId;
}

size_t GpuProgram::getUniformCount() const {
	return uniformData.size();
}

const GpuProgram::UniformDescription & GpuProgram::getUniformDescription( size_t index ) const {
	if (index >= uniformData.size())
		throw std::runtime_error( "Invalid uniform index" );
	return uniformData[index].desc;
}

GLint GpuProgram::getUniformLocation( size_t index, size_t arrayOffset ) const {
	if (index >= uniformData.size())
		return -1;
	if (arrayOffset >= uniformData[index].desc.arrayCount)
		return -1;
	return uniformLocations[uniformData[index].locationIndex + arrayOffset];
};

GLint GpuProgram::getUniformLocation( const std::string & name, size_t arrayOffset ) const {
	size_t index;
	if (!getUniformIndex( name, index ))
		return -1;
	return getUniformLocation( index, arrayOffset );
}

bool GpuProgram::getUniformIndex( const std::string & name, size_t & index ) const {
	return uniformMap.get( name, index );
}

bool GpuProgram::setUniform( size_t index, GLenum type, const void * data, size_t arrayOffset, size_t arrayCount ) {
	if (index >= uniformData.size())
		return false;
	const UniformData & ud = uniformData[index];
	if (ud.desc.type != type)
		return false;
	if (arrayOffset >= ud.desc.arrayCount)
		// we don't return errors on array size problems - this is because GLSL sometimes optimizes away some entries
		return true;
	// adjust so we don't go past the end
	arrayCount = std::min( arrayCount, ud.desc.arrayCount - arrayOffset );

	size_t entrySize = ud.typeDescription.elements * getGlslUniformBaseTypeSize( ud.typeDescription.baseType );
	size_t cacheOffset = ud.cacheOffset + entrySize * arrayOffset;
	size_t entries = arrayCount * ud.typeDescription.elements;
	bool upload = false;
	// compare entries in the cache
	switch (ud.typeDescription.baseType) {
	case GL_FLOAT:
		{
			float * cachePtr = (float*)((byte*)&uniformCache[0] + cacheOffset);
			const float * dataPtr = (const float*)data;
			for (size_t i = 0; i < entries; ++i) {
				if (cachePtr[i] != dataPtr[i]) {
					upload = true;
					break;
				}
			}
		}
		break;
	case GL_INT:
	case GL_BOOL:
		{
			int * cachePtr = (int*)((byte*)&uniformCache[0] + cacheOffset);
			const int * dataPtr = (const int*)data;
			for (size_t i = 0; i < entries; ++i) {
				if (cachePtr[i] != dataPtr[i]) {
					upload = true;
					break;
				}
			}
		}
		break;
	case GL_UNSIGNED_INT:
		{
			uint * cachePtr = (uint*)((byte*)&uniformCache[0] + cacheOffset);
			const uint * dataPtr = (const uint*)data;
			for (size_t i = 0; i < entries; ++i) {
				if (cachePtr[i] != dataPtr[i]) {
					upload = true;
					break;
				}
			}
		}
		break;
	}

	if (!upload)
		// nothing to upload - success
		return true;

	bind();

	// upload and update cache
	GLuint loc = uniformLocations[ud.locationIndex + arrayOffset];
	switch (ud.desc.type) {
	case GL_FLOAT:
		glUniform1fv( loc, (uint)arrayCount, (const float*)data );
		break;
	case GL_FLOAT_VEC2:
		glUniform2fv( loc, (uint)arrayCount, (const float*)data );
		break;
	case GL_FLOAT_VEC3:
		glUniform3fv( loc, (uint)arrayCount, (const float*)data );
		break;
	case GL_FLOAT_VEC4:
		glUniform4fv( loc, (uint)arrayCount, (const float*)data );
		break;
	case GL_INT:
	case GL_BOOL:
		glUniform1iv( loc, (uint)arrayCount, (const int*)data );
		break;
	case GL_INT_VEC2:
	case GL_BOOL_VEC2:
		glUniform2iv( loc, (uint)arrayCount, (const int*)data );
		break;
	case GL_INT_VEC3:
	case GL_BOOL_VEC3:
		glUniform3iv( loc, (uint)arrayCount, (const int*)data );
		break;
	case GL_INT_VEC4:
	case GL_BOOL_VEC4:
		glUniform4iv( loc, (uint)arrayCount, (const int*)data );
		break;
	case GL_UNSIGNED_INT:
		glUniform1uiv( loc, (uint)arrayCount, (const uint*)data );
		break;
	case GL_UNSIGNED_INT_VEC2:
		glUniform2uiv( loc, (uint)arrayCount, (const uint*)data );
		break;
	case GL_UNSIGNED_INT_VEC3:
		glUniform3uiv( loc, (uint)arrayCount, (const uint*)data );
		break;
	case GL_UNSIGNED_INT_VEC4:
		glUniform4uiv( loc, (uint)arrayCount, (const uint*)data );
		break;
	case GL_FLOAT_MAT2:
		glUniformMatrix2fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT3:
		glUniformMatrix3fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT4:
		glUniformMatrix4fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT2x3:
		glUniformMatrix2x3fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT2x4:
		glUniformMatrix2x4fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT3x2:
		glUniformMatrix3x2fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT3x4:
		glUniformMatrix3x4fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT4x2:
		glUniformMatrix4x2fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_FLOAT_MAT4x3:
		glUniformMatrix4x3fv( loc, (uint)arrayCount, false, (const float*)data );
		break;
	case GL_SAMPLER_1D:
	case GL_SAMPLER_2D:
	case GL_SAMPLER_3D:
	case GL_SAMPLER_CUBE:
	case GL_SAMPLER_1D_SHADOW:
	case GL_SAMPLER_2D_SHADOW:
	case GL_SAMPLER_1D_ARRAY:
	case GL_SAMPLER_2D_ARRAY:
	case GL_SAMPLER_1D_ARRAY_SHADOW:
	case GL_SAMPLER_2D_ARRAY_SHADOW:
	case GL_SAMPLER_2D_MULTISAMPLE:
	case GL_SAMPLER_2D_MULTISAMPLE_ARRAY:
	case GL_SAMPLER_CUBE_SHADOW:
	case GL_SAMPLER_BUFFER:
	case GL_SAMPLER_2D_RECT:
	case GL_SAMPLER_2D_RECT_SHADOW:
	case GL_INT_SAMPLER_1D:
	case GL_INT_SAMPLER_2D:
	case GL_INT_SAMPLER_3D:
	case GL_INT_SAMPLER_CUBE:
	case GL_INT_SAMPLER_1D_ARRAY:
	case GL_INT_SAMPLER_2D_ARRAY:
	case GL_INT_SAMPLER_2D_MULTISAMPLE:
	case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
	case GL_INT_SAMPLER_BUFFER:
	case GL_INT_SAMPLER_2D_RECT:
	case GL_UNSIGNED_INT_SAMPLER_1D:
	case GL_UNSIGNED_INT_SAMPLER_2D:
	case GL_UNSIGNED_INT_SAMPLER_3D:
	case GL_UNSIGNED_INT_SAMPLER_CUBE:
	case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
	case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
	case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:
	case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
	case GL_UNSIGNED_INT_SAMPLER_BUFFER:
	case GL_UNSIGNED_INT_SAMPLER_2D_RECT:
		glUniform1iv( loc, (uint)arrayCount, (const int*)data );
		break;
	}

	std::copy( (byte*)data, (byte*)data + entrySize * arrayCount, (byte*)&uniformCache[0] + cacheOffset );

	return true;
}

bool GpuProgram::setUniform( const std::string & name, GLenum type, const void * data, size_t arrayOffset, size_t arrayCount ) {
	size_t index;
	if (!getUniformIndex( name, index ))
		return false;
	setUniform( index, type, data, arrayOffset, arrayCount );
	return true;
}

size_t GpuProgram::getAttributeCount() const {
	return attributeData.size();
}

const GpuProgram::AttributeDescription & GpuProgram::getAttributeDescription( size_t index ) const {
	if (index >= attributeData.size())
		throw std::runtime_error( "Invalid attribute index" );
	return attributeData[index].desc;
}

GLint GpuProgram::getAttributeLocation( size_t index ) const {
	if (index >= attributeData.size())
		return -1;
	return attributeData[index].location;
}

GLint GpuProgram::getAttributeLocation( const std::string & name ) const {
	size_t index;
	if (!getAttributeIndex( name, index ))
		return -1;
	return getAttributeLocation( index );
}

bool GpuProgram::getAttributeIndex( const std::string & name, size_t & index ) const {
	return attributeMap.get( name, index );
}

bool GpuProgram::setAttribute( size_t index, uint size, GLenum type, bool normalized, bool integer, uint stride,
	const void * data, size_t arrayOffset, size_t arrayCount ) {
	if (index >= attributeData.size())
		return false;
	const AttributeData & ad = attributeData[index];
	if (ad.typeDescription.elements != size)
		return false;
	if (arrayOffset >= ad.desc.arrayCount)
		// don't make array bound issues an error
		return true;
	arrayCount = std::min( arrayCount, ad.desc.arrayCount - arrayOffset );
	size_t entrySize = getOpenGLAttributeTypeSize( type, size );
	bool success = true;
	for (size_t i = 0; i < arrayCount; ++i) {
		GpuAttributeReference attrib = getGpuState()->getAttribute( (size_t)ad.location + arrayOffset + i );
		attrib->setEnabled( true );
		success &= attrib->setPointer( size, type, normalized, integer, stride,
			(const byte*)data + i*entrySize );
	}
	return success;
}

bool GpuProgram::setAttribute( const std::string & name, uint size, GLenum type, bool normalized, bool integer, uint stride,
	const void * data, size_t arrayOffset, size_t arrayCount ) {
	size_t index;
	if (!getAttributeIndex( name, index ))
		return false;
	return setAttribute( index, size, type, normalized, integer, stride, data, arrayOffset, arrayCount );
}

bool GpuProgram::setAttribute( size_t index, GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer, uint stride,
	const void * data, size_t arrayOffset, size_t arrayCount ) {
	if (index >= attributeData.size())
		return false;
	const AttributeData & ad = attributeData[index];
	if (ad.typeDescription.elements != size)
		return false;
	if (arrayOffset >= ad.desc.arrayCount)
		// don't make array bound issues an error
		return true;
	arrayCount = std::min( arrayCount, ad.desc.arrayCount - arrayOffset );
	size_t entrySize = getOpenGLAttributeTypeSize( type, size );
	bool success = true;
	for (size_t i = 0; i < arrayCount; ++i) {
		GpuAttributeReference attrib = getGpuState()->getAttribute( (size_t)ad.location + arrayOffset + i );
		attrib->setEnabled( true );
		success &= attrib->setPointer( buffer, size, type, normalized, integer, stride,
			(const byte*)data + i*entrySize );
	}
	return success;
}

bool GpuProgram::setAttribute( const std::string & name, GpuBufferReference & buffer,uint size, GLenum type, bool normalized, bool integer, uint stride,
	const void * data, size_t arrayOffset, size_t arrayCount ) {
	size_t index;
	if (!getAttributeIndex( name, index ))
		return false;
	return setAttribute( index, buffer, size, type, normalized, integer, stride, data, arrayOffset, arrayCount );
}

void GpuProgram::bind() {
	getGpuState()->bindProgram( this );
}

void GpuProgram::onUnbind() {
	for (size_t i = 0; i < attributeData.size(); ++i) {
		// disable attributes
		const AttributeData & ad = attributeData[i];
		for (size_t t = 0; t < ad.desc.arrayCount; ++t)
			getGpuState()->getAttribute( (size_t)ad.location + t )->setEnabled( false );
	}
}

void GpuProgram::onRelease() const {
	// no more references
	delete this;
}

GpuProgram::ShaderType GpuProgram::getShaderType( GLenum shader ) {
	switch (shader) {
	case GL_VERTEX_SHADER:
		return ST_VERTEX_SHADER;
	case GL_GEOMETRY_SHADER:
		return ST_GEOMETRY_SHADER;
	case GL_FRAGMENT_SHADER:
		return ST_FRAGMENT_SHADER;
	default:
		throw std::runtime_error( "Invalid shader" );
	}
}

GpuProgram::GlslUniformTypeDescription::GlslUniformTypeDescription() {
}

GpuProgram::GlslUniformTypeDescription::GlslUniformTypeDescription( GLenum base, size_t elem )
	: baseType( base )
	, elements( elem ) {
}

GpuProgram::GlslUniformTypeDescription GpuProgram::getGlslUniformTypeDescription( GLenum type ) {
	switch (type) {
	case GL_FLOAT:
		return GlslUniformTypeDescription( GL_FLOAT, 1 );
	case GL_FLOAT_VEC2:
		return GlslUniformTypeDescription( GL_FLOAT, 2 );
	case GL_FLOAT_VEC3:
		return GlslUniformTypeDescription( GL_FLOAT, 3 );
	case GL_FLOAT_VEC4:
		return GlslUniformTypeDescription( GL_FLOAT, 4 );
	case GL_INT:
		return GlslUniformTypeDescription( GL_INT, 1 );
	case GL_INT_VEC2:
		return GlslUniformTypeDescription( GL_INT, 2 );
	case GL_INT_VEC3:
		return GlslUniformTypeDescription( GL_INT, 3 );
	case GL_INT_VEC4:
		return GlslUniformTypeDescription( GL_INT, 4 );
	case GL_UNSIGNED_INT:
		return GlslUniformTypeDescription( GL_UNSIGNED_INT, 1 );
	case GL_UNSIGNED_INT_VEC2:
		return GlslUniformTypeDescription( GL_UNSIGNED_INT, 2 );
	case GL_UNSIGNED_INT_VEC3:
		return GlslUniformTypeDescription( GL_UNSIGNED_INT, 3 );
	case GL_UNSIGNED_INT_VEC4:
		return GlslUniformTypeDescription( GL_UNSIGNED_INT, 4 );
	case GL_BOOL:
		return GlslUniformTypeDescription( GL_BOOL, 1 );
	case GL_BOOL_VEC2:
		return GlslUniformTypeDescription( GL_BOOL, 2 );
	case GL_BOOL_VEC3:
		return GlslUniformTypeDescription( GL_BOOL, 3 );
	case GL_BOOL_VEC4:
		return GlslUniformTypeDescription( GL_BOOL, 4 );
	case GL_FLOAT_MAT2:
		return GlslUniformTypeDescription( GL_FLOAT, 4 );
	case GL_FLOAT_MAT3:
		return GlslUniformTypeDescription( GL_FLOAT, 9 );
	case GL_FLOAT_MAT4:
		return GlslUniformTypeDescription( GL_FLOAT, 16 );
	case GL_FLOAT_MAT2x3:
		return GlslUniformTypeDescription( GL_FLOAT, 6 );
	case GL_FLOAT_MAT2x4:
		return GlslUniformTypeDescription( GL_FLOAT, 8 );
	case GL_FLOAT_MAT3x2:
		return GlslUniformTypeDescription( GL_FLOAT, 6 );
	case GL_FLOAT_MAT3x4:
		return GlslUniformTypeDescription( GL_FLOAT, 12 );
	case GL_FLOAT_MAT4x2:
		return GlslUniformTypeDescription( GL_FLOAT, 8  );
	case GL_FLOAT_MAT4x3:
		return GlslUniformTypeDescription( GL_FLOAT, 16 );
	case GL_SAMPLER_1D:
	case GL_SAMPLER_2D:
	case GL_SAMPLER_3D:
	case GL_SAMPLER_CUBE:
	case GL_SAMPLER_1D_SHADOW:
	case GL_SAMPLER_2D_SHADOW:
	case GL_SAMPLER_1D_ARRAY:
	case GL_SAMPLER_2D_ARRAY:
	case GL_SAMPLER_1D_ARRAY_SHADOW:
	case GL_SAMPLER_2D_ARRAY_SHADOW:
	case GL_SAMPLER_2D_MULTISAMPLE:
	case GL_SAMPLER_2D_MULTISAMPLE_ARRAY:
	case GL_SAMPLER_CUBE_SHADOW:
	case GL_SAMPLER_BUFFER:
	case GL_SAMPLER_2D_RECT:
	case GL_SAMPLER_2D_RECT_SHADOW:
	case GL_INT_SAMPLER_1D:
	case GL_INT_SAMPLER_2D:
	case GL_INT_SAMPLER_3D:
	case GL_INT_SAMPLER_CUBE:
	case GL_INT_SAMPLER_1D_ARRAY:
	case GL_INT_SAMPLER_2D_ARRAY:
	case GL_INT_SAMPLER_2D_MULTISAMPLE:
	case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
	case GL_INT_SAMPLER_BUFFER:
	case GL_INT_SAMPLER_2D_RECT:
	case GL_UNSIGNED_INT_SAMPLER_1D:
	case GL_UNSIGNED_INT_SAMPLER_2D:
	case GL_UNSIGNED_INT_SAMPLER_3D:
	case GL_UNSIGNED_INT_SAMPLER_CUBE:
	case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
	case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
	case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:
	case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
	case GL_UNSIGNED_INT_SAMPLER_BUFFER:
	case GL_UNSIGNED_INT_SAMPLER_2D_RECT:
		// all samplers are treated as ints
		return GlslUniformTypeDescription( GL_INT, 1 );
	default:
		throw std::runtime_error( "Invalid GLSL uniform type" );
	}
}

size_t GpuProgram::getGlslUniformBaseTypeSize( GLenum type ) {
	switch (type) {
	case GL_FLOAT:
	case GL_INT:
	case GL_UNSIGNED_INT:
	case GL_BOOL:
		return 4;
	default:
		throw std::runtime_error( "Invalid GLSL uniform base type" );
	}
}

GpuProgram::GlslAttributeTypeDescription::GlslAttributeTypeDescription() {
}

GpuProgram::GlslAttributeTypeDescription::GlslAttributeTypeDescription( GLenum base, size_t elem, size_t locs )
	: baseType( base )
	, elements( elem )
	, locations( locs ) {
}

GpuProgram::GlslAttributeTypeDescription GpuProgram::getGlslAttributeTypeDescription( GLenum type ) {
	switch (type) {
	case GL_FLOAT:
		return GlslAttributeTypeDescription( GL_FLOAT, 1, 1 );
	case GL_FLOAT_VEC2:
		return GlslAttributeTypeDescription( GL_FLOAT, 2, 1 );
	case GL_FLOAT_VEC3:
		return GlslAttributeTypeDescription( GL_FLOAT, 3, 1 );
	case GL_FLOAT_VEC4:
		return GlslAttributeTypeDescription( GL_FLOAT, 4, 1 );
	case GL_FLOAT_MAT2:
		return GlslAttributeTypeDescription( GL_FLOAT, 2, 2 );
	case GL_FLOAT_MAT3:
		return GlslAttributeTypeDescription( GL_FLOAT, 3, 3 );
	case GL_FLOAT_MAT4:
		return GlslAttributeTypeDescription( GL_FLOAT, 4, 4 );
	case GL_FLOAT_MAT2x3:
		return GlslAttributeTypeDescription( GL_FLOAT, 2, 3 );
	case GL_FLOAT_MAT2x4:
		return GlslAttributeTypeDescription( GL_FLOAT, 2, 4 );
	case GL_FLOAT_MAT3x2:
		return GlslAttributeTypeDescription( GL_FLOAT, 3, 2 );
	case GL_FLOAT_MAT3x4:
		return GlslAttributeTypeDescription( GL_FLOAT, 3, 4 );
	case GL_FLOAT_MAT4x2:
		return GlslAttributeTypeDescription( GL_FLOAT, 4, 2 );
	case GL_FLOAT_MAT4x3:
		return GlslAttributeTypeDescription( GL_FLOAT, 4, 3 );
	case GL_INT:
		return GlslAttributeTypeDescription( GL_INT, 1, 1 );
	case GL_INT_VEC2:
		return GlslAttributeTypeDescription( GL_INT, 2, 1 );
	case GL_INT_VEC3:
		return GlslAttributeTypeDescription( GL_INT, 3, 1 );
	case GL_INT_VEC4:
		return GlslAttributeTypeDescription( GL_INT, 4, 1 );
	case GL_UNSIGNED_INT:
		return GlslAttributeTypeDescription( GL_UNSIGNED_INT, 1, 1 );
	case GL_UNSIGNED_INT_VEC2:
		return GlslAttributeTypeDescription( GL_UNSIGNED_INT, 2, 1 );
	case GL_UNSIGNED_INT_VEC3:
		return GlslAttributeTypeDescription( GL_UNSIGNED_INT, 3, 1 );
	case GL_UNSIGNED_INT_VEC4:
		return GlslAttributeTypeDescription( GL_UNSIGNED_INT, 4, 1 );
	default:
		throw std::runtime_error( "Invalid GLSL attribute type" );
	}
}

size_t GpuProgram::getOpenGLAttributeTypeSize( GLenum type, size_t elements ) {
	switch (type) {
	case GL_BYTE:
		return elements;
	case GL_UNSIGNED_BYTE:
		return elements;
	case GL_SHORT:
		return 2 * elements;
	case GL_UNSIGNED_SHORT:
		return 2 * elements;
	case GL_INT:
		return 4 * elements;
	case GL_UNSIGNED_INT:
		return 4 * elements;
	case GL_HALF_FLOAT:
		return 2 * elements;
	case GL_FLOAT:
		return 4 * elements;
	case GL_DOUBLE:
		return 8 * elements;
	case GL_INT_2_10_10_10_REV:
		return 4;
	case GL_UNSIGNED_INT_2_10_10_10_REV:
		return 4;
	default:
		throw std::runtime_error( "Invalid OpenGL attribute type" );
	}
}