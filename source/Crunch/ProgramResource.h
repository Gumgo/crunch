/** @file ProgramResource.h
 *  @brief Contains the resource holding program data.
 */

#ifndef PROGRAMRESOURCE_DEFINED
#define PROGRAMRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "GpuProgram.h"
#include <vector>

/** @brief Program resource.
 */
DEFINE_RESOURCE_CLASS( ProgramResource, "program", "programs" )
public:
	/** @brief The constructor.
	 */
	ProgramResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	GpuProgramReference getProgram();				/**< Returns the program.*/
	GpuProgramConstReference getProgram() const;	/**< Returns the program.*/

private:
	static const size_t SHADER_TYPE_COUNT = 3;
	std::string source[3];			// the vertex, geometry, and fragment shader source code
	GpuProgramReference program;	// the program
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <ProgramResource> ProgramResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <ProgramResource> ProgramResourceConstReference;

#endif