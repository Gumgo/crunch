#if 0

#include "RenderState.h"
#include "Log.h"
#include "Context.h"
#include "SystemState.h"

#define USE_INT_ATTRIB_WORKAROUND

RenderState::RenderState( const Window & w )
	: window( w )
	, gpuBufferState( shaderState )
	, drawCallState( shaderState ) {
	frames = 0;
}

void RenderState::init() {
	shaderState.init();
	renderTargetState.init();
}

void RenderState::term() {
	shaderState.term();
	renderTargetState.term();
}

void RenderState::beginFrame() {
	glClearDepth( 1.0f );
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	shaderState.reset();
	renderTargetState.reset();
	textureState.reset();
	gpuBufferState.reset();
	blendState.reset();
	cullingState.reset();
	depthStencilState.reset();
	viewportState.reset();
	drawCallState.reset();
}

void RenderState::endFrame() {
	renderTargetState.endFrame();
	shaderState.endFrame();
}

////////

RenderState::ShaderState::ShaderState() {
	uniformCache.reserve( 50 );
}

void RenderState::ShaderState::init() {
	glGenVertexArrays( 1, &vertexArrayObject );
	if (vertexArrayObject == 0)
		throw std::runtime_error( "Failed to initialize vertex attribute state" );
	glBindVertexArray( vertexArrayObject );
}

void RenderState::ShaderState::term() {
	if (vertexArrayObject != 0)
		glDeleteVertexArrays( 1, &vertexArrayObject );
}

void RenderState::ShaderState::reset() {
	program = NULL;
	glUseProgram( 0 );
}

void RenderState::ShaderState::endFrame() {
	if (program != NULL) {
		for (size_t i = 0; i < attributeCache.size(); ++i) {
			if (attributeCache[i].enabled)
				glDisableVertexAttribArray( program->getAttributeData( i ).location );
		}
	}
}

void RenderState::ShaderState::disableAllAttributes() {
	if (program != NULL) {
		for (size_t i = 0; i < attributeCache.size(); ++i) {
			if (attributeCache[i].enabled) {
				glDisableVertexAttribArray( program->getAttributeData( i ).location );
				attributeCache[i].enabled = false;
			}
		}
		attributesRemaining = program->getAttributeCount();
	}
}

void RenderState::ShaderState::setProgram( const ProgramResource * p ) {
	if (program != p) {
		// if not the same...
		if (program != NULL) {
			// if current program is non NULL, disable all attributes
			for (size_t i = 0; i < attributeCache.size(); ++i) {
				if (attributeCache[i].enabled)
					glDisableVertexAttribArray( program->getAttributeData( i ).location );
			}
		}

		// set new program
		program = p;
		if (p == NULL)
			glUseProgram( 0 );
		else {
			glUseProgram( p->getProgramId() );

			uniformCache.resize( p->getCacheSize() );

			// assign each uniform space in the cache
			for (size_t i = 0; i < uniformCache.size(); ++i)
				uniformCache[i].valid = false;

			// disable all attributes
			attributeCache.resize( p->getAttributeCount() );
			for (size_t i = 0; i < attributeCache.size(); ++i)
				attributeCache[i].enabled = false;

			attributesRemaining = program->getAttributeCount();
			uniformsRemaining = program->getUniformCount();
		}
	}
}

void RenderState::ShaderState::setUniform( size_t index, GLenum type, const void * value ) {
	setUniformArray( index, type, value, 1 );
}

void RenderState::ShaderState::setUniform( const std::string & name, GLenum type, const void * value ) {
	setUniformArray( name, type, value, 1 );
}

void RenderState::ShaderState::setUniformArray( size_t index, GLenum type, const void * value, size_t count ) {
	if (program == NULL) {
		Log::warning() << "Failed to set uniform with index " << index << ": no program set";
		return;
	}

	if (index >= program->getUniformCount()) {
		Log::warning() << "Failed to set uniform with index " << index << ": uniform does not exist";
		return;
	}

	const ProgramResource::UniformData & uniform = program->getUniformData( index );
	if (uniform.type != type) {
		Log::warning() << "Failed to set uniform " << uniform.name << ": invalid type";
		return;
	}

	if (count > uniform.size) {
		Log::warning() << "Failed to set uniform " << uniform.name << ": array is too large";
		return;
	}

	bool set = true;

	// compare in cache
	if (uniformCache[uniform.cacheIndex].valid) {
		switch (uniform.baseType) {
		case GL_FLOAT:
			{
				float * castValue = (float*)value;
				bool different = false;
				// for each array element
				for (size_t c = 0; c < count; ++c) {
					UniformCache & cache = uniformCache[uniform.cacheIndex + c];
					// for each uniform element
					for (size_t i = 0; i < uniform.typeSize; ++i) {
						if (cache.floatValue[i] != castValue[uniform.typeSize*c + i]) {
							different = true;
							break;
						}
						if (different)
							break;
					}
				}
				if (!different)
					set = false;
			}
			break;
		case GL_INT:
			{
				int * castValue = (int*)value;
				bool different = false;
				// for each array element
				for (size_t c = 0; c < count; ++c) {
					UniformCache & cache = uniformCache[uniform.cacheIndex + c];
					if (memcmp( cache.intValue, castValue + c*uniform.typeSize, uniform.typeSize*sizeof( int ) ) != 0) {
						different = true;
						break;
					}
				}
				if (!different)
					set = false;
			}
			break;
		case GL_UNSIGNED_INT:
			{
				uint * castValue = (uint*)value;
				bool different = false;
				// for each array element
				for (size_t c = 0; c < count; ++c) {
					UniformCache & cache = uniformCache[uniform.cacheIndex + c];
					if (memcmp( cache.uintValue, castValue + c*uniform.typeSize, uniform.typeSize*sizeof( uint ) ) != 0) {
						different = true;
						break;
					}
				}
				if (!different)
					set = false;
			}
			break;
		case GL_BOOL:
			{
				int * castValue = (int*)value;
				bool different = false;
				// for each array element
				for (size_t c = 0; c < count; ++c) {
					UniformCache & cache = uniformCache[uniform.cacheIndex + c];
					if (memcmp( cache.boolValue, castValue + c*uniform.typeSize, uniform.typeSize*sizeof( int ) ) != 0) {
						different = true;
						break;
					}
				}
				if (!different)
					set = false;
			}
			break;
		case GL_SAMPLER_1D:
		case GL_SAMPLER_2D:
		case GL_SAMPLER_3D:
		case GL_SAMPLER_CUBE:
			{
				int * castValue = (int*)value;
				bool different = false;
				// for each array element
				for (size_t c = 0; c < count; ++c) {
					UniformCache & cache = uniformCache[uniform.cacheIndex + c];
					if (memcmp( &cache.samplerValue, castValue + c*uniform.typeSize, uniform.typeSize*sizeof( int ) ) != 0) {
						different = true;
						break;
					}
				}
				if (!different)
					set = false;
			}
			break;
		}
	} else {
		uniformCache[uniform.cacheIndex].valid = true;
		--uniformsRemaining;
	}

	// update the cache and send to the program
	if (set) {
		switch (uniform.baseType) {
		case GL_FLOAT:
			{
				float * castValue = (float*)value;
				// for each array element
				for (size_t c = 0; c < count; ++c)
					memcpy(
						uniformCache[uniform.cacheIndex + c].floatValue,
						castValue + uniform.typeSize*c,
						uniform.typeSize*sizeof( float ) );
			}
			break;
		case GL_INT:
			{
				int * castValue = (int*)value;
				// for each array element
				for (size_t c = 0; c < count; ++c)
					memcpy(
						uniformCache[uniform.cacheIndex + c].intValue,
						castValue + uniform.typeSize*c,
						uniform.typeSize*sizeof( int ) );
			}
			break;
		case GL_UNSIGNED_INT:
			{
				uint * castValue = (uint*)value;
				// for each array element
				for (size_t c = 0; c < count; ++c)
					memcpy(
						uniformCache[uniform.cacheIndex + c].uintValue,
						castValue + uniform.typeSize*c,
						uniform.typeSize*sizeof( uint ) );
			}
			break;
		case GL_BOOL:
			{
				int * castValue = (int*)value;
				// for each array element
				for (size_t c = 0; c < count; ++c)
					memcpy(
						uniformCache[uniform.cacheIndex + c].boolValue,
						castValue + uniform.typeSize*c,
						uniform.typeSize*sizeof( int ) );
			}
			break;
		case GL_SAMPLER_2D:
			{
				int * castValue = (int*)value;
				// for each array element
				for (size_t c = 0; c < count; ++c)
					memcpy(
						&uniformCache[uniform.cacheIndex + c].samplerValue,
						castValue + uniform.typeSize*c,
						uniform.typeSize*sizeof( int ) );
			}
			break;
		}

		switch (uniform.type) {
		case GL_FLOAT:
			glUniform1fv( uniform.location, (int)count, (float*)value );
			break;
		case GL_FLOAT_VEC2:
			glUniform2fv( uniform.location, (int)count, (float*)value );
			break;
		case GL_FLOAT_VEC3:
			glUniform3fv( uniform.location, (int)count, (float*)value );
			break;
		case GL_FLOAT_VEC4:
			glUniform4fv( uniform.location, (int)count, (float*)value );
			break;
		case GL_INT:
			glUniform1iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_INT_VEC2:
			glUniform2iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_INT_VEC3:
			glUniform3iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_INT_VEC4:
			glUniform4iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_UNSIGNED_INT:
			glUniform1uiv( uniform.location, (int)count, (uint*)value );
			break;
		case GL_UNSIGNED_INT_VEC2:
			glUniform2uiv( uniform.location, (int)count, (uint*)value );
			break;
		case GL_UNSIGNED_INT_VEC3:
			glUniform3uiv( uniform.location, (int)count, (uint*)value );
			break;
		case GL_UNSIGNED_INT_VEC4:
			glUniform4uiv( uniform.location, (int)count, (uint*)value );
			break;
		case GL_BOOL:
			glUniform1iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_BOOL_VEC2:
			glUniform2iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_BOOL_VEC3:
			glUniform3iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_BOOL_VEC4:
			glUniform4iv( uniform.location, (int)count, (int*)value );
			break;
		case GL_FLOAT_MAT2:
			glUniformMatrix2fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT3:
			glUniformMatrix3fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT4:
			glUniformMatrix4fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT2x3:
			glUniformMatrix2x3fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT2x4:
			glUniformMatrix2x4fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT3x2:
			glUniformMatrix3x2fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT3x4:
			glUniformMatrix3x4fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT4x2:
			glUniformMatrix4x2fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_FLOAT_MAT4x3:
			glUniformMatrix4x3fv( uniform.location, (int)count, false, (float*)value );
			break;
		case GL_SAMPLER_1D:
		case GL_SAMPLER_2D:
		case GL_SAMPLER_3D:
		case GL_SAMPLER_CUBE:
		case GL_SAMPLER_1D_ARRAY:
		case GL_SAMPLER_2D_ARRAY:
		case GL_SAMPLER_1D_SHADOW:
		case GL_SAMPLER_2D_SHADOW:
		case GL_SAMPLER_CUBE_MAP_ARRAY:
		case GL_SAMPLER_CUBE_SHADOW:
		case GL_SAMPLER_1D_ARRAY_SHADOW:
		case GL_SAMPLER_2D_ARRAY_SHADOW:
		case GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW:
			glUniform1iv( uniform.location, (int)count, (int*)value );
		}
	}
}

void RenderState::ShaderState::setUniformArray( const std::string & name, GLenum type, const void * value, size_t count ) {
	if (program == NULL) {
		Log::warning() << "Failed to set uniform " << name << ": no program set";
		return;
	}

	size_t uniformIndex;
	if (!program->getUniformIndex( name, uniformIndex )) {
		Log::warning() << "Failed to set uniform " << name << ": uniform does not exist";
		return;
	}

	setUniformArray( uniformIndex, type, value, count );
}

void RenderState::ShaderState::enableVertexAttribute( const std::string & name, size_t stride, size_t bufOffset, size_t attribOffset, GLenum dataType, bool normalized ) {
	if (program == NULL) {
		Log::warning() << "Failed to enable vertex attribute " << name << ": no program set";
		return;
	}

	size_t attributeIndex;
	if (!program->getAttributeIndex( name, attributeIndex ))
		Log::warning() << "Failed to enable vertex attribute " << name << ": vertex attribute does not exist";

	enableVertexAttribute( attributeIndex, stride, bufOffset, attribOffset, dataType, normalized );
}

void RenderState::ShaderState::enableVertexAttribute( size_t index, size_t stride, size_t bufOffset, size_t attribOffset, GLenum dataType, bool normalized ) {
	if (program == NULL) {
		Log::warning() << "Failed to enable vertex attribute with index " << index << ": no program set";
		return;
	}

	if (index >= program->getAttributeCount())
		Log::warning() << "Failed to enable vertex attribute with index " << index << ": vertex attribute does not exist";

	const ProgramResource::AttributeData & attribute = program->getAttributeData( index );

	bool wasEnabled = attributeCache[index].enabled;
	if (!attributeCache[index].enabled) {
		attributeCache[index].enabled = true;
		glEnableVertexAttribArray( attribute.location );
		--attributesRemaining;
	}

	if (dataType == 0)
		dataType = attribute.baseType;

	if (!wasEnabled || stride != attributeCache[index].stride || bufOffset + attribOffset != attributeCache[index].offset ||
		dataType != attributeCache[index].dataType || normalized != attributeCache[index].normalized) {
		// todo (minor): add support for all types: GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, GL_UNSIGNED_INT / GL_HALF_FLOAT, GL_FLOAT, GL_DOUBLE, GL_INT_2_10_10_10_REV, GL_UNSIGNED_INT_2_10_10_10_REV
		if (attribute.baseType == GL_FLOAT)
			glVertexAttribPointer(
				attribute.location, (int)attribute.typeSize, dataType, normalized, (GLsizei)stride, bufferOffset( bufOffset + attribOffset ) );
		else
#ifndef USE_INT_ATTRIB_WORKAROUND
			glVertexAttribIPointer(
				attribute.location, (int)attribute.typeSize, dataType, (GLsizei)stride, bufferOffset( bufOffset + attribOffset ) );
#else
			// it appears that vectors (ivec, uvec, etc) work but single values don't... best we can do is this
		{
			if (attribute.typeSize > 1)
				glVertexAttribIPointer(
					attribute.location, (int)attribute.typeSize, dataType, (GLsizei)stride, bufferOffset( bufOffset + attribOffset ) );
			else // this ASSUMES that the base type is 4 bytes! so this means this hack only works with uint and int, NOT short, byte, etc.
				glVertexAttribPointer(
					attribute.location, (int)attribute.typeSize, GL_FLOAT, false, (GLsizei)stride, bufferOffset( bufOffset + attribOffset ) );
		}
#endif
		attributeCache[index].stride = stride;
		attributeCache[index].offset = bufOffset + attribOffset;
		attributeCache[index].dataType = dataType;
		attributeCache[index].normalized = normalized;
	}
}

void RenderState::ShaderState::disableVertexAttribute( const std::string & name ) {
	if (program == NULL) {
		Log::warning() << "Failed to disable vertex attribute " << name << ": no program set";
		return;
	}

	size_t attributeIndex;
	if (!program->getAttributeIndex( name, attributeIndex ))
		Log::warning() << "Failed to disable vertex attribute " << name << ": vertex attribute does not exist";

	disableVertexAttribute( attributeIndex );
}

void RenderState::ShaderState::disableVertexAttribute( size_t index ) {
	if (program == NULL) {
		Log::warning() << "Failed to disable vertex attribute with index " << index << ": no program set";
		return;
	}

	if (index >= program->getAttributeCount())
		Log::warning() << "Failed to disable vertex attribute with index " << index << ": vertex attribute does not exist";

	if (attributeCache[index].enabled) {
		attributeCache[index].enabled = false;
		glDisableVertexAttribArray( program->getAttributeData( index ).location );
		++attributesRemaining;
	}
}

size_t RenderState::ShaderState::getRemainingUniforms() const {
	return uniformsRemaining;
}

size_t RenderState::ShaderState::getRemainingAttributes() const {
	return attributesRemaining;
}

////////

void RenderState::TextureState::reset() {
	for (size_t i = 0; i < arraySize( textures ); ++i) {
		textures[i] = NULL;
		glActiveTexture( GL_TEXTURE0 + (int)i );
		glBindTexture( GL_TEXTURE_1D, 0 );
		glBindTexture( GL_TEXTURE_2D, 0 );
		glBindTexture( GL_TEXTURE_3D, 0 );
		glBindTexture( GL_TEXTURE_CUBE_MAP, 0 );
		// note: if more texture types are supported, add them here
	}
}

void RenderState::TextureState::bind( int textureUnit, const GpuTexture * t ) {
	if (textureUnit < 0 || (size_t)textureUnit >= TEXTURE_UNITS)
		Log::warning() << "Invalid texture unit specified";
	else {
		if (textures[textureUnit] != t) {
			glActiveTexture( GL_TEXTURE0 + textureUnit );
			if (t == NULL || (textures[textureUnit] != NULL && t->getTextureType() != textures[textureUnit]->getTextureType()))
				// if we are unbinding, or if the currently bound texture is of a different type,
				// unbind the currently bound texture
				glBindTexture( textures[textureUnit]->getTextureType(), 0 );
			textures[textureUnit] = t;
			if (t != NULL)
				glBindTexture( t->getTextureType(), t->getTextureId() );
		}
	}
}

////////

RenderState::GpuBufferState::GpuBufferState( ShaderState & ss )
	: shaderState( ss ) {
}

void RenderState::GpuBufferState::reset() {
	vertexBuffer = NULL;
	indexBuffer = NULL;
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void RenderState::GpuBufferState::bind( const GpuBuffer * buffer ) {
	if (buffer == NULL) {
		Log::warning() << "Attempted to bind NULL buffer: use unbindVertexBuffer() or unbindIndexBuffer() instead";
		return;
	}

	switch (buffer->getBufferType()) {
	case GL_ARRAY_BUFFER:
		if (vertexBuffer != buffer) {
			vertexBuffer = buffer;
			shaderState.disableAllAttributes();
			glBindBuffer( GL_ARRAY_BUFFER, buffer->getBufferId() );
		}
		break;
	case GL_ELEMENT_ARRAY_BUFFER:
		if (indexBuffer != buffer) {
			indexBuffer = buffer;
			glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, buffer->getBufferId() );
		}
		break;
	default:
		Log::warning() << "Attempted to bind buffer of unknown type";
	}
}

void RenderState::GpuBufferState::unbindVertexBuffer() {
	if (vertexBuffer != NULL) {
		vertexBuffer = NULL;
		glBindBuffer( GL_ARRAY_BUFFER, 0 );
	}
}

void RenderState::GpuBufferState::unbindIndexBuffer() {
	if (indexBuffer != NULL) {
		indexBuffer = NULL;
		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	}
}

////////

void RenderState::BlendState::reset() {
	blendEnabled = false;
	blendFunc1 = GL_SRC_ALPHA;
	blendFunc2 = GL_ONE_MINUS_SRC_ALPHA;
	glDisable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

void RenderState::BlendState::setBlendMode( BlendMode bm ) {
	bool shouldEnableBlend;
	GLenum func1;
	GLenum func2;
	switch (bm) {
	case BM_NONE:
		// disable blending but don't change anything else
		shouldEnableBlend = false;
		func1 = blendFunc1;
		func2 = blendFunc2;
		break;
	case BM_ALPHA:
		shouldEnableBlend = true;
		func1 = GL_SRC_ALPHA;
		func2 = GL_ONE_MINUS_SRC_ALPHA;
		break;
	case BM_PREMULTIPLIED_ALPHA:
		shouldEnableBlend = true;
		func1 = GL_SRC_ALPHA;
		func2 = GL_ONE;
		break;
	case BM_ADDITIVE:
		shouldEnableBlend = true;
		func1 = GL_ONE;
		func2 = GL_ONE;
		break;
	case BM_MULTIPLICATIVE:
		shouldEnableBlend = true;
		func1 = GL_ZERO;
		func2 = GL_SRC_COLOR;
		break;
	case BM_INVERSE:
		// src white = fully inverted, src black = normal (gray = interpolate)
		shouldEnableBlend = true;
		func1 = GL_ONE_MINUS_DST_COLOR;
		func2 = GL_ONE_MINUS_SRC_COLOR;
		break;
	}

	if (blendEnabled && !shouldEnableBlend)
		glDisable( GL_BLEND );
	else if (!blendEnabled && shouldEnableBlend)
		glEnable( GL_BLEND );
	if (func1 != blendFunc1 || func2 != blendFunc2)
		glBlendFunc( func1, func2 );
	blendEnabled = shouldEnableBlend;
	blendFunc1 = func1;
	blendFunc2 = func2;
}

void RenderState::BlendState::setBlendMode( GLenum src, GLenum dst ) {
	if (!blendEnabled)
		glEnable( GL_BLEND );
	if (src != blendFunc1 || dst != blendFunc2)
		glBlendFunc( src, dst );
	blendEnabled = true;
	blendFunc1 = src;
	blendFunc2 = dst;
}

////////

void RenderState::CullingState::reset() {
	cullingEnabled = true;
	cullMode = GL_BACK;
	glEnable( GL_CULL_FACE );
	glCullFace( cullMode );
}

void RenderState::CullingState::enableCulling() {
	if (!cullingEnabled) {
		cullingEnabled = true;
		glEnable( GL_CULL_FACE );
	}
}

void RenderState::CullingState::disableCulling() {
	if (cullingEnabled) {
		cullingEnabled = false;
		glDisable( GL_CULL_FACE );
	}
}

void RenderState::CullingState::setCullMode( GLenum mode ) {
	if (mode != cullMode) {
		cullMode = mode;
		glCullFace( mode );
	}
}

////////

void RenderState::DepthStencilState::reset() {
	depthTestEnabled = false;
	depthTestFunc = GL_LEQUAL;
	depthWriteEnabled = true;
	glDisable( GL_DEPTH_TEST );
	glDepthFunc( depthTestFunc );
	glDepthMask( GL_TRUE );

	stencilTestEnabled = false;
	stencilTestFunc = GL_ALWAYS;
	stencilTestRef = 0;
	stencilTestMask = ~((uint)0); // all 1s
	sFailOp = GL_KEEP;
	dFailOp = GL_KEEP;
	dPassOp = GL_KEEP;
	glDisable( GL_STENCIL_TEST );
	glStencilFunc( stencilTestFunc, stencilTestRef, stencilTestMask );
	glStencilOp( sFailOp, dFailOp, dPassOp );
}

void RenderState::DepthStencilState::enableDepthTest() {
	if (!depthTestEnabled) {
		depthTestEnabled = true;
		glEnable( GL_DEPTH_TEST );
	}
}

void RenderState::DepthStencilState::disableDepthTest() {
	if (depthTestEnabled) {
		depthTestEnabled = false;
		glDisable( GL_DEPTH_TEST );
	}
}

void RenderState::DepthStencilState::setDepthTestFunc( GLenum func ) {
	if (depthTestFunc != func) {
		depthTestFunc = func;
		glDepthFunc( func );
	}
}

void RenderState::DepthStencilState::enableDepthWrite() {
	if (!depthWriteEnabled) {
		depthWriteEnabled = true;
		glDepthMask( GL_TRUE );
	}
}

void RenderState::DepthStencilState::disableDepthWrite() {
	if (depthWriteEnabled) {
		depthWriteEnabled = false;
		glDepthMask( GL_FALSE );
	}
}

void RenderState::DepthStencilState::enableStencilTest() {
	if (!stencilTestEnabled) {
		stencilTestEnabled = true;
		glEnable( GL_STENCIL_TEST );
	}
}

void RenderState::DepthStencilState::disableStencilTest() {
	if (stencilTestEnabled) {
		stencilTestEnabled = false;
		glDisable( GL_STENCIL_TEST );
	}
}

void RenderState::DepthStencilState::setStencilTestFunc( GLenum func, int ref, uint mask ) {
	if (stencilTestFunc != func || stencilTestRef != ref || stencilTestMask != mask) {
		stencilTestFunc = func;
		stencilTestRef = ref;
		stencilTestMask = mask;
		glStencilFunc( func, ref, mask );
	}
}

void RenderState::DepthStencilState::setStencilTestOp( GLenum sFail, GLenum dFail, GLenum dPass ) {
	if (sFailOp != sFail || dFailOp != dFail || dPassOp != dPass) {
		sFailOp = sFail;
		dFailOp = dFail;
		dPassOp = dPass;
		glStencilOp( sFail, dFail, dPass );
	}
}

////////

RenderState::RenderTargetState::RenderTargetState() {
	initialized = false;
}

RenderState::RenderTargetState::~RenderTargetState() {
	term();
}

void RenderState::RenderTargetState::init() {
	for (size_t i = 0; i < MAX_COLOR_ATTACHMENTS; ++i) {
		colorAttachments[i].isGpuTexture = true;
		colorAttachments[i].texture = NULL;
		boundColorAttachments[i].isGpuTexture = true;
		boundColorAttachments[i].texture = NULL;
	}
	depthAttachment.isGpuTexture = true;
	depthAttachment.texture = NULL;
	boundDepthAttachment.isGpuTexture = true;
	boundDepthAttachment.texture = NULL;
	stencilAttachment.isGpuTexture = true;
	stencilAttachment.texture = NULL;
	boundStencilAttachment.isGpuTexture = true;
	boundStencilAttachment.texture = NULL;

	screenBuffer = MAX_COLOR_ATTACHMENTS;

	glGenFramebuffers( 1, &framebufferId );
	if (framebufferId == 0)
		throw std::runtime_error( "Failed to create the framebuffer" );

	initialized = true;
}

void RenderState::RenderTargetState::term() {
	if (initialized) {
		glDeleteFramebuffers( 1, &framebufferId );
		initialized = false;
	}
}

void RenderState::RenderTargetState::reset() {
	// bind the framebuffer
	framebufferEnabled = framebufferBound = true;
	glEnable( GL_FRAMEBUFFER_SRGB );
	glBindFramebuffer( GL_FRAMEBUFFER, framebufferId );

	drawBuffersCount = 1;
	drawBuffers[0] = GL_COLOR_ATTACHMENT0;
	glDrawBuffers( drawBuffersCount, drawBuffers );
}

void RenderState::RenderTargetState::endFrame() {
	if (screenBuffer >= MAX_COLOR_ATTACHMENTS || colorAttachments[screenBuffer].object == NULL)
		return;

	size_t dims[2];
	if (colorAttachments[screenBuffer].isGpuTexture) {
		dims[0] = colorAttachments[screenBuffer].texture->getTextureSize( 0 );
		dims[1] = colorAttachments[screenBuffer].texture->getTextureSize( 1 );
	} else {
		dims[0] = colorAttachments[screenBuffer].renderbuffer->getRenderbufferWidth();
		dims[1] = colorAttachments[screenBuffer].renderbuffer->getRenderbufferHeight();
	}

	// bind the screen framebuffer to draw to and the renderer's framebuffer to read from
	glBindFramebuffer( GL_READ_FRAMEBUFFER, framebufferId );
	glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );

	// we want to draw to the screen and read from color attachment 0
	glReadBuffer( GL_COLOR_ATTACHMENT0 + screenBuffer );
	glDrawBuffer( GL_BACK );

	// finally, we blit from the framebuffer to the screen
	// note: sRGB conversion does not seem to be applied with this
	glBlitFramebuffer( 0, 0, (GLint)dims[0], (GLint)dims[1], 0, 0, (GLint)dims[0], (GLint)dims[1], GL_COLOR_BUFFER_BIT, GL_NEAREST );
}

void RenderState::RenderTargetState::disableFramebuffer() {
	framebufferEnabled = false;
}

void RenderState::RenderTargetState::enableFramebuffer() {
	framebufferEnabled = true;
}

bool RenderState::RenderTargetState::RenderTarget::operator==( const RenderState::RenderTargetState::RenderTarget & o ) const {
	return
		(object == NULL && o.object == NULL) ||
		(!isGpuTexture && !o.isGpuTexture && renderbuffer == o.renderbuffer) ||
		(isGpuTexture && o.isGpuTexture && texture == o.texture && (!singleLayer && !o.singleLayer || (singleLayer && o.singleLayer && layer == o.layer)));
}

bool RenderState::RenderTargetState::RenderTarget::operator!=( const RenderState::RenderTargetState::RenderTarget & o ) const {
	return !(*this == o);
}

void RenderState::RenderTargetState::setRenderTarget( RenderTarget & rt, GpuTexture * t ) {
	rt.isGpuTexture = true;
	rt.texture = t;
	rt.singleLayer = false;
}

void RenderState::RenderTargetState::setRenderTarget( RenderTarget & rt, GpuTexture * t, size_t layer ) {
	rt.isGpuTexture = true;
	rt.texture = t;
	if (t == NULL)
		rt.singleLayer = false;
	else {
		switch (t->getTextureType()) {
		case GL_TEXTURE_1D_ARRAY:
		case GL_TEXTURE_2D_ARRAY:
		case GL_TEXTURE_3D:
		case GL_TEXTURE_CUBE_MAP:
			rt.singleLayer = true;
			rt.layer = layer;
			break;
		default:
			rt.singleLayer = false;
		}
	}
}

void RenderState::RenderTargetState::setRenderTarget( RenderTarget & rt, GpuRenderbuffer * t ) {
	rt.isGpuTexture = false;
	rt.renderbuffer = t;
	rt.singleLayer = false;
}

void RenderState::RenderTargetState::attachRenderTarget( RenderTarget & rt, GLenum attachment ) {
	if (rt.object == NULL)
		glFramebufferRenderbuffer( GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, 0 );
	else if (rt.isGpuTexture) {
		if (rt.singleLayer) {
			if (rt.texture->getTextureType() == GL_TEXTURE_CUBE_MAP)
				glFramebufferTexture2D( GL_FRAMEBUFFER, attachment, (GLenum)(GL_TEXTURE_CUBE_MAP_POSITIVE_X + rt.layer), rt.texture->getTextureId(), 0 );
			else
				glFramebufferTextureLayer( GL_FRAMEBUFFER, attachment, rt.texture->getTextureId(), 0, (GLint)rt.layer );
		} else
			glFramebufferTexture( GL_FRAMEBUFFER, attachment, rt.texture->getTextureId(), 0 );
	} else
		glFramebufferRenderbuffer( GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, rt.renderbuffer->getRenderbufferId() );
}

void RenderState::RenderTargetState::setColorAttachment( uint i, GpuTexture * target ) {
	if (i >= RenderState::RenderTargetState::MAX_COLOR_ATTACHMENTS) {
		Log::warning() << "Attempted to set framebuffer color attachment out of range";
		return;
	}
	setRenderTarget( colorAttachments[i], target );
}

void RenderState::RenderTargetState::setColorAttachment( uint i, GpuTexture * target, size_t layer ) {
	if (i >= RenderState::RenderTargetState::MAX_COLOR_ATTACHMENTS) {
		Log::warning() << "Attempted to set framebuffer color attachment out of range";
		return;
	}
	setRenderTarget( colorAttachments[i], target, layer );
}

void RenderState::RenderTargetState::setColorAttachment( uint i, GpuRenderbuffer * target ) {
	if (i >= RenderState::RenderTargetState::MAX_COLOR_ATTACHMENTS) {
		Log::warning() << "Attempted to set framebuffer color attachment out of range";
		return;
	}
	setRenderTarget( colorAttachments[i], target );
}

void RenderState::RenderTargetState::setDepthAttachment( GpuTexture * target ) {
	setRenderTarget( depthAttachment, target );
}

void RenderState::RenderTargetState::setDepthAttachment( GpuTexture * target, size_t layer ) {
	setRenderTarget( depthAttachment, target, layer );
}

void RenderState::RenderTargetState::setDepthAttachment( GpuRenderbuffer * target ) {
	setRenderTarget( depthAttachment, target );
}

void RenderState::RenderTargetState::setStencilAttachment( GpuTexture * target ) {
	setRenderTarget( stencilAttachment, target );
}

void RenderState::RenderTargetState::setStencilAttachment( GpuTexture * target, size_t layer ) {
	setRenderTarget( stencilAttachment, target, layer );
}

void RenderState::RenderTargetState::setStencilAttachment( GpuRenderbuffer * target ) {
	setRenderTarget( stencilAttachment, target );
}

void RenderState::RenderTargetState::setDepthStencilAttachment( GpuTexture * target ) {
	setRenderTarget( depthAttachment, target );
	setRenderTarget( stencilAttachment, target );
}

void RenderState::RenderTargetState::setDepthStencilAttachment( GpuTexture * target, size_t layer ) {
	setRenderTarget( depthAttachment, target, layer );
	setRenderTarget( stencilAttachment, target, layer );
}

void RenderState::RenderTargetState::setDepthStencilAttachment( GpuRenderbuffer * target ) {
	setRenderTarget( depthAttachment, target );
	setRenderTarget( stencilAttachment, target );
}

void RenderState::RenderTargetState::bind() {
	if (framebufferEnabled && !framebufferBound)
		glBindFramebuffer( GL_FRAMEBUFFER, framebufferId );
	else if (!framebufferEnabled && framebufferBound)
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );

	framebufferBound = framebufferEnabled;

	if (!framebufferBound)
		return;

	for (uint i = 0; i < MAX_COLOR_ATTACHMENTS; ++i) {
		if (colorAttachments[i] != boundColorAttachments[i]) {
			boundColorAttachments[i] = colorAttachments[i];
			attachRenderTarget( colorAttachments[i], GL_COLOR_ATTACHMENT0 + i );
		}
	}

	bool newDepth = (depthAttachment != boundDepthAttachment);
	bool newStencil = (stencilAttachment != boundStencilAttachment);

	boundDepthAttachment = depthAttachment;
	boundStencilAttachment = stencilAttachment;

	if (depthAttachment.object == stencilAttachment.object)
		// bind both at once
		attachRenderTarget( depthAttachment, GL_DEPTH_STENCIL_ATTACHMENT );
	else {
		if (newDepth)
			attachRenderTarget( depthAttachment, GL_DEPTH_ATTACHMENT );
		if (newStencil)
			attachRenderTarget( depthAttachment, GL_STENCIL_ATTACHMENT );
	}
}

RenderState::RenderTargetState::RenderTarget RenderState::RenderTargetState::getColorAttachment( uint i ) const {
	if (i >= RenderState::RenderTargetState::MAX_COLOR_ATTACHMENTS) {
		Log::warning() << "Attempted to get framebuffer color attachment out of range";
		RenderTarget rt;
		rt.isGpuTexture = true;
		rt.object = NULL;
		return rt;
	}
	return boundColorAttachments[i];
}

RenderState::RenderTargetState::RenderTarget RenderState::RenderTargetState::getDepthAttachment() const {
	return boundDepthAttachment;
}

RenderState::RenderTargetState::RenderTarget RenderState::RenderTargetState::getStencilAttachment() const {
	return boundStencilAttachment;
}

void RenderState::RenderTargetState::setDrawBuffers( uint count, GLenum * buffers ) {
	if ((size_t)count > MAX_COLOR_ATTACHMENTS) {
		Log::warning() << "Attempted to set too many draw buffers";
		return;
	}
	if (count == drawBuffersCount) {
		bool same = true;
		for (uint i = 0; i < count; ++i) {
			if (drawBuffers[i] != buffers[i]) {
				same = false;
				break;
			}
		}
		if (same)
			return;
	}
	memcpy( drawBuffers, buffers, count*sizeof( drawBuffers[0] ) );
	drawBuffersCount = count;
	glDrawBuffers( count, buffers );
}

void RenderState::RenderTargetState::disableScreenBlit() {
	screenBuffer = MAX_COLOR_ATTACHMENTS;
}

void RenderState::RenderTargetState::enableScreenBlit( uint buffer ) {
	if ((size_t)buffer >= MAX_COLOR_ATTACHMENTS) {
		Log::warning() << "Attempted to set screen buffer out of range";
		return;
	}
	screenBuffer = buffer;
}

////////

void RenderState::ViewportState::reset() {
	position.set( 0, 0 );
	size.set( Context::get().window.getWidth(), Context::get().window.getHeight() );
	glViewport( position.x, position.y, size.x, size.y );
}

void RenderState::ViewportState::setViewport( const Vector2i & vpPos, const Vector2i & vpSize ) {
	if (position != vpPos || size != vpSize) {
		position = vpPos;
		size = vpSize;
		glViewport( vpPos.x, vpPos.y, vpSize.x, vpSize.y );
	}
}

void RenderState::ViewportState::setViewportToWindow() {
	Vector2i s( Context::get().window.getWidth(), Context::get().window.getHeight() );
	setViewport( Vector2i(), s );
}

Vector2i RenderState::ViewportState::getViewportPosition() const {
	return position;
}

Vector2i RenderState::ViewportState::getViewportSize() const {
	return size;
}

////////

RenderState::DrawCallState::DrawCallState( ShaderState & ss )
	: shaderState( ss ) {
}

void RenderState::DrawCallState::reset() {
	drawMode = 0xffffffff;
	indexType = 0;
	primitiveRestartEnabled = false;
	glDisable( GL_PRIMITIVE_RESTART );
	primitiveRestartIndex = 0;
	glPrimitiveRestartIndex( primitiveRestartIndex );
}

void RenderState::DrawCallState::setPrimitiveMode( GLenum primMode ) {
	drawMode = primMode;
}

void RenderState::DrawCallState::setIndexType( GLenum indType ) {
	indexType = indType;
	if (primitiveRestartEnabled) {
		primitiveRestartEnabled = false;
		glDisable( GL_PRIMITIVE_RESTART );
	}
}

void RenderState::DrawCallState::setIndexType( GLenum indType, GLuint primRestartIndex ) {
	indexType = indType;
	if (!primitiveRestartEnabled) {
		primitiveRestartEnabled = true;
		glEnable( GL_PRIMITIVE_RESTART );
	}
	if (primitiveRestartIndex != primRestartIndex) {
		primitiveRestartIndex = primRestartIndex;
		glPrimitiveRestartIndex( primRestartIndex );
	}
}

void RenderState::DrawCallState::checkShaderState() {
	size_t u = shaderState.getRemainingUniforms();
	size_t a = shaderState.getRemainingAttributes();
	if (u > 0)
		Log::warning() << u << " uniforms are not set";
	if (a > 0)
		Log::warning() << a << " vertex attributes are not set";
}

void RenderState::DrawCallState::draw( size_t firstElement, size_t elementCount ) {
	checkShaderState();

	if (drawMode == 0xffffffff) {
		Log::warning() << "Failed to draw: primitive mode not set";
		return;
	}

	glDrawArrays( drawMode, (GLint)firstElement, (GLsizei)elementCount );
}

void RenderState::DrawCallState::drawIndexed( size_t indexBufferOffset, size_t elementCount ) {
	checkShaderState();

	if (drawMode == 0xffffffff) {
		Log::warning() << "Failed to draw: primitive mode not set";
		return;
	}

	if (indexType == 0) {
		Log::warning() << "Failed to draw: index type not set";
		return;
	}

	glDrawElements( drawMode, (GLsizei)elementCount, indexType, bufferOffset( indexBufferOffset ) );
}

void RenderState::DrawCallState::drawIndexedRange( size_t indexBufferOffset, size_t elementCount, size_t minElement, size_t maxElement ) {
	checkShaderState();

	if (drawMode == 0xffffffff) {
		Log::warning() << "Failed to draw: primitive mode not set";
		return;
	}

	if (indexType == 0) {
		Log::warning() << "Failed to draw: index type not set";
		return;
	}

	glDrawRangeElements( drawMode, (GLuint)minElement, (GLuint)maxElement, (GLsizei)elementCount, indexType, bufferOffset( indexBufferOffset ) );
}

#endif