#ifndef SYSGUI_DEFINED
#define SYSGUI_DEFINED

#include "Common.h"
#include "Actor.h"

#include "InputEvent.h"
#include "SysCamera2D.h"
#include "FontResource.h"

// note: GUIs do not use the default event processing/drawing; do not attempt to change active/visible status
// TODO: right now all GUIs use "default" rendering
// make option to change this (e.g. use custom colors or define custom images)

DEFINE_ACTOR( SysGui, 0, 0 )
protected:
	const SysCamera2D * camera;
	FontResourceReference font;

public:
	void onCreate();
	void setCamera( const SysCamera2D * cam );
	void setFont( FontResourceReference & f );
	FontResourceReference getFont();
	FontResourceConstReference getFont() const;

	virtual void receiveEvent( const InputEvent & ev ) = 0;
	virtual void onDismissParent() = 0; // called when parent modal removed from its parent
	virtual void draw() = 0; // the drawing function for the GUI object

	void onActivate();
};

#endif