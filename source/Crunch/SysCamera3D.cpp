#include "SysCamera3D.h"

void SysCamera3D::computeViewProjectionMatrix() {
	viewProjectionMatrix = projectionMatrix * viewMatrix;
	frustum.setFrustum( viewProjectionMatrix );
}

void SysCamera3D::setProjection( float pFov, float pAspectRatio, float pNear, float pFar ) {
	projFov = pFov;
	projAspectRatio = pAspectRatio;
	projNear = pNear;
	projFar = pFar;

	float cotFov = 1.0f / tan( degToRad( pFov * 0.5f ) );
	projectionMatrix.set(
		cotFov/pAspectRatio,	0.0f,	0.0f,						0.0f,
		0.0f,					cotFov,	0.0f,						0.0f,
		0.0f,					0.0f,	(pFar+pNear)/(pNear-pFar),	(2.0f*pFar*pNear)/(pNear-pFar),
		0.0f,					0.0f,	-1.0f,						0.0f );

	computeViewProjectionMatrix();
}

void SysCamera3D::setCameraTarget( const Vector3f & from, const Vector3f & to, const Vector3f & up ) {
	setCameraDirection( from, to-from, up );
}

void SysCamera3D::setCameraDirection( const Vector3f & from, const Vector3f & direction, const Vector3f & up ) {
	Vector3f bckVec = -direction.normalized();
	Vector3f rgtVec = up.cross( bckVec ).normalized();
	Vector3f upVec = bckVec.cross( rgtVec ); // already normalized
	// we want the inverse of the matrix
	// [ rgt up bck | from ]
	// the inverse of the affine matrix
	// [A b] is [A^-1 -A^-1*b]
	// [0 1]    [0    1      ]
	// A is simply a rotation, so A^-1 is just its transpose
	viewMatrix.set(
		rgtVec.x,	rgtVec.y,	rgtVec.z,	-from.dot( rgtVec ),
		upVec.x,	upVec.y,	upVec.z,	-from.dot( upVec ),
		bckVec.x,	bckVec.y,	bckVec.z,	-from.dot( bckVec ) );
	// the camera matrix is simply the matrix
	// [ rgt up bck | from ]
	cameraMatrix.set(
		rgtVec.x, upVec.x, bckVec.x, from.x,
		rgtVec.y, upVec.y, bckVec.y, from.y,
		rgtVec.z, upVec.z, bckVec.z, from.z );
	computeViewProjectionMatrix();
}

void SysCamera3D::setViewMatrix( const Matrix44af & matrix ) {
	viewMatrix = matrix;
	cameraMatrix = matrix.inverse();
	computeViewProjectionMatrix();
}

void SysCamera3D::setCameraMatrix( const Matrix44af & matrix ) {
	viewMatrix = matrix.inverse();
	cameraMatrix = matrix;
	computeViewProjectionMatrix();
}

float SysCamera3D::getFov() const {
	return projFov;
}

float SysCamera3D::getAspectRatio() const {
	return projAspectRatio;
}

float SysCamera3D::getNear() const {
	return projNear;
}

float SysCamera3D::getFar() const {
	return projFar;
}

const Matrix44f & SysCamera3D::getProjectionMatrix() const {
	return projectionMatrix;
}

const Matrix44af & SysCamera3D::getViewMatrix() const {
	return viewMatrix;
}

const Matrix44af & SysCamera3D::getCameraMatrix() const {
	return cameraMatrix;
}

const Matrix44f & SysCamera3D::getViewProjectionMatrix() const {
	return viewProjectionMatrix;
}

Matrix44af SysCamera3D::getModelViewMatrix( const Matrix33f & modelMatrix ) const {
	return viewMatrix * modelMatrix;
}

Matrix44af SysCamera3D::getModelViewMatrix( const Matrix44af & modelMatrix ) const {
	return viewMatrix * modelMatrix;
}

Matrix44f SysCamera3D::getModelViewProjectionMatrix( const Matrix33f & modelMatrix ) const {
	// viewProjectionMatrix * modelMatrix - todo: add code for this in math class
	return Matrix44f(
		viewProjectionMatrix[ 0]*modelMatrix[0] + viewProjectionMatrix[ 1]*modelMatrix[3] + viewProjectionMatrix[ 2]*modelMatrix[ 6],
		viewProjectionMatrix[ 0]*modelMatrix[1] + viewProjectionMatrix[ 1]*modelMatrix[4] + viewProjectionMatrix[ 2]*modelMatrix[ 7],
		viewProjectionMatrix[ 0]*modelMatrix[2] + viewProjectionMatrix[ 1]*modelMatrix[5] + viewProjectionMatrix[ 2]*modelMatrix[ 8],
		viewProjectionMatrix[ 3],
		viewProjectionMatrix[ 4]*modelMatrix[0] + viewProjectionMatrix[ 5]*modelMatrix[3] + viewProjectionMatrix[ 6]*modelMatrix[ 6],
		viewProjectionMatrix[ 4]*modelMatrix[1] + viewProjectionMatrix[ 5]*modelMatrix[4] + viewProjectionMatrix[ 6]*modelMatrix[ 7],
		viewProjectionMatrix[ 4]*modelMatrix[2] + viewProjectionMatrix[ 5]*modelMatrix[5] + viewProjectionMatrix[ 6]*modelMatrix[ 8],
		viewProjectionMatrix[ 7],
		viewProjectionMatrix[ 8]*modelMatrix[0] + viewProjectionMatrix[ 9]*modelMatrix[3] + viewProjectionMatrix[10]*modelMatrix[ 6],
		viewProjectionMatrix[ 8]*modelMatrix[1] + viewProjectionMatrix[ 9]*modelMatrix[4] + viewProjectionMatrix[10]*modelMatrix[ 7],
		viewProjectionMatrix[ 8]*modelMatrix[2] + viewProjectionMatrix[ 9]*modelMatrix[5] + viewProjectionMatrix[10]*modelMatrix[ 8],
		viewProjectionMatrix[11],
		viewProjectionMatrix[12]*modelMatrix[0] + viewProjectionMatrix[13]*modelMatrix[3] + viewProjectionMatrix[14]*modelMatrix[ 6],
		viewProjectionMatrix[12]*modelMatrix[1] + viewProjectionMatrix[13]*modelMatrix[4] + viewProjectionMatrix[14]*modelMatrix[ 7],
		viewProjectionMatrix[12]*modelMatrix[2] + viewProjectionMatrix[13]*modelMatrix[5] + viewProjectionMatrix[14]*modelMatrix[ 8],
		viewProjectionMatrix[15] );
}

Matrix44f SysCamera3D::getModelViewProjectionMatrix( const Matrix44af & modelMatrix ) const {
	return viewProjectionMatrix * modelMatrix;
}

const SysFrustum & SysCamera3D::getFrustum() const {
	return frustum;
}

Ray3f SysCamera3D::screenToWorld( const Vector2f & screen ) const {
	// TODO: use the projection matrix instead

	// we compute a point on the image plane first assuming no camera transformation
	// this means the camera is "pointing" down the z axis
	// x is to the screen's "right", y is to the screen's "up"
	float screenHalfHeight = tan( degToRad( projFov ) * 0.5f );
	float screenHalfWidth = screenHalfHeight * projAspectRatio;
	Vector3f untransformedDirection(
		screenHalfWidth * (2.0f*screen.x - 1.0f),
		screenHalfHeight * (1.0f - 2.0f*screen.y), // y is "flipped" since the top of the screen is 0
		-1.0f );

	// now we have the ray - the origin is the camera's position and the direction we get by transforming
	return Ray3f(
		Vector3f( cameraMatrix[3], cameraMatrix[7], cameraMatrix[11] ),
		cameraMatrix.transformVector( untransformedDirection ) );
}