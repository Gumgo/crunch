/** @file GpuObject.h
 *  @brief The base class for objects or state on the GPU.
 */

#ifndef GPUOBJECT_DEFINED
#define GPUOBJECT_DEFINED

#include "Common.h"
#include "OpenGL.h"

class GpuState;

/** @brief Base class for GPU objects.
 *
 *  This class should not be directly accessed, as methods are not virtual.
 *  Objects should derive from it for the convenience of GpuState access.
 */
class GpuObject {
public:
	/** @brief Stores a reference to the GpuState.
	 */
	GpuObject( GpuState * state );

	GpuState * getGpuState();				/**< Returns the underlying GpuState.*/
	const GpuState * getGpuState() const;	/**< Returns the underlying GpuState.*/

protected:
	/** @brief Enables or disables the given state and updates currentState.
	 */
	static void setStateEnabled( bool newState, bool & currentState, GLenum stateName );

private:
	// underlying GPU state
	GpuState * gpuState;
};

/** @brief Base class for reference counted GPU objects.
 *
 *  Implementations should define the method void onRelease()
 *  to be called when the reference count drops to zero.
 *  This object should never be used directly; only subclass
 *  pointers should be used (hence no virtual destructor).
 */
class ReferenceCountedGpuObject : public GpuObject {
public:
	/** @brief Stores a reference to the GpuState.
	 */
	ReferenceCountedGpuObject( GpuState * state );

	size_t getReferenceCount() const;	/**< Returns the number of references to this object.*/
	void acquire() const;				/**< Increments the reference count.*/
	void release() const;				/**< Decrements the reference count.*/

protected:
	/** @brief This method should be overridden to release GPU resources.
	 */
	virtual void onRelease() const = 0;

private:
	// private undefined copy and assignment operators
	ReferenceCountedGpuObject( const ReferenceCountedGpuObject & o );
	ReferenceCountedGpuObject & operator=( const ReferenceCountedGpuObject & o );

	// the reference count for this object
	// mutable because reference count shouldn't affect object's logical state
	mutable size_t referenceCount;
};

#endif