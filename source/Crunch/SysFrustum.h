#ifndef SYSFRUSTUM_DEFINED
#define SYSFRUSTUM_DEFINED

#include "Common.h"
#include "SysAabb.h"

// holds the planes of the viewing frustum
class SysFrustum {
public:
	// the frustum planes
	enum FrustumPlane {
		FP_LEFT = 0,
		FP_RIGHT,
		FP_BOTTOM,
		FP_TOP,
		FP_NEAR,
		FP_FAR,
		FRUSTUM_PLANE_COUNT
	};

	// sets the frustum planes from the view-projection matrix
	void setFrustum( const Matrix44f & viewProjectionMatrix );
	// returns the array of planes
	Planef * getPlanes();
	// returns the array of planes
	const Planef * getPlanes() const;
	// returns true if the frustum contains the given AABB
	bool contains( const SysAabb & aabb ) const;

private:
	Planef planes[6]; // the frustum planes
};

#endif