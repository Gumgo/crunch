/** @file Maths_PlaneDec.h
 *  @brief Contains an implementation of a plane class.
 */

#ifndef MATHS_PLANEDEC_DEFINED
#define MATHS_PLANEDEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for planes.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Plane {
public:
	Vector3 <T> normal;	/**< The normal of the plane.*/
	T d;				/**< The d component of the plane equation.*/

	/** @brief Constructs a plane with default values.
	 */
	Plane();

	/** @brief Constructs a plane from the equation n \htmlonly&#0183\endhtmlonly p + d = 0.
	 *
	 *  @param norm	The normal of the plane, or a, b, and c in the plane equation.
	 *  @param pd	The d component in the plane equation.
	 */
	Plane( const Vector3 <T> & norm, T pd ); // ax + by + cz + d = 0; norm = abc, offset = d

	/** @brief Constructs a plane by copying the one provided.
	 */
	Plane( const Plane <T> & p );

	/** @brief Constructs a plane from 3 points, assuming counterclockwise winding order.
	 */
	Plane( const Vector3 <T> & p0, const Vector3 <T> & p1, const Vector3 <T> & p2 );

	/** @brief Constructs a plane from a normal vector and a point.
	 */
	Plane( const Vector3 <T> & norm, const Vector3 <T> & point );

	/** @brief The destructor.
	 */
	~Plane();

	// operators

	/** @brief Sets the current value to the plane m.
	 */
	Plane <T> operator=( const Plane <T> & p );

	/** @brief Returns whether the current value is equal to the plane p.
	 */
	bool operator==( const Plane <T> & p ) const;

	/** @brief Returns whether the current value is not equal to the plane p.
	 */
	bool operator!=( const Plane <T> & p ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Plane <U> () const;

	// mutable

	/** @brief Sets the current value by constructing a plane from 3 points, assuming counterclockwise winding order.
	 */
	void set( const Vector3 <T> & p0, const Vector3 <T> & p1, const Vector3 <T> & p2 );

	/** @brief Sets the current value by constructing a plane from the equation n \htmlonly&#0183\endhtmlonly p + d = 0.
	 *
	 *  @param norm	The normal of the plane, or a and b in the plane equation.
	 *  @param pd	The d component in the plane equation.
	 */
	void set( const Vector3 <T> & norm, T pd );

	/** @brief Sets the current value by constructing a plane from a normal vector and a point.
	 */
	void set( const Vector3 <T> & norm, const Vector3 <T> & point );

	/** @brief Normalizes the normal of the plane.
	 */
	void normalize();

	// immutable

	/** @brief Returns a copy of the current value with the normal of the plane normalized.
	 */
	Plane <T> normalized() const;

	/** @brief Computes the intersection between the plane and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the plane's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param origin		Origin of the ray.
	 *  @param direction	Direction of the ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Vector3 <T> & origin, const Vector3 <T> & direction, Vector3 <T> & intersection, T & signedDist ) const;

	/** @brief Computes the intersection between the plane and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the plane's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param ray			The ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Ray3 <T> & ray, Vector3 <T> & intersection, T & signedDist ) const;

	/** @brief Computes the intersection between the plane and a ray.
	 *
	 *  @param origin		Origin of the ray.
	 *  @param direction	Direction of the ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Vector3 <T> & origin, const Vector3 <T> & direction, Vector3 <T> & intersection ) const;

	/** @brief Computes the intersection between the plane and a ray.
	 *
	 *  @param ray			The ray.
	 *  @param intersection	Location to store the intersection point.
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Ray3 <T> & ray, Vector3 <T> & intersection ) const;

	/** @brief Computes the intersection between the plane and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the plane's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param origin		Origin of the ray.
	 *  @param direction	Direction of the ray.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Vector3 <T> & origin, const Vector3 <T> & direction, T & signedDist ) const;

	/** @brief Computes the intersection between the plane and a ray.
	 *
	 *  @warning In order for signedDist to be correct,
	 *  the plane's normal and ray's direction
	 *  must be normalized.
	 *
	 *  @param ray			The ray.
	 *  @param signedDist	Location to store the distance to intersection
	 *  @return				True if an intersection occurred, false otherwise.
	 */
	bool intersects( const Ray3 <T> & ray, T & signedDist ) const;

	/** @brief Computes the distance between a point and the plane.
	 *
	 *  @warning In order for the resulting
	 *  distance to be correct, the
	 *  plane's normal must be normalized.
	 *
	 *  @param point	The point.
	 *  @return			The distance of point to the current value.
	 */
	T distance( const Vector3 <T> & point ) const;

	/** @brief Computes the signed distance between a point and the plane.
	 *
	 *  @warning In order for the resulting
	 *  signed distance to be correct, the
	 *  plane's normal must be normalized.
	 *
	 *  @param point	The point.
	 *  @return			The signed distance of point to the current value.
	 */
	T signedDistance( const Vector3 <T> & point ) const;

	/** @brief Returns which side of the plane a point is on.
	 *
	 *  @param point	The point.
	 *  @return			-1, 0, or 1, for behind, on, or in front of the plane, respectively.
	 */
	T side( const Vector3 <T> & point ) const;

	/** @brief Computes the x coordinate of the point on the plane with the given y and z coordinates.
	 *
	 *  @param x	Location to store the x coordinate.
	 *  @param y	The y coordinate.
	 *  @param z	The z coordinate.
	 *  @return		True if the x coordinate can be computed, false otherwise.
	 */
	bool x( T & x, T y, T z ) const;

	/** @brief Computes the y coordinate of the point on the plane with the given x and z coordinates.
	 *
	 *  @param x	The x coordinate.
	 *  @param y	Location to store the y coordinate.
	 *  @param z	The z coordinate.
	 *  @return		True if the y coordinate can be computed, false otherwise.
	 */
	bool y( T x, T & y, T z ) const;

	/** @brief Computes the z coordinate of the point on the plane with the given x and y coordinates.
	 *
	 *  @param x	The x coordinate.
	 *  @param y	The y coordinate.
	 *  @param z	Location to store the z coordinate.
	 *  @return		True if the y coordinate can be computed, false otherwise.
	 */
	bool z( T x, T y, T & z ) const;
};

/** @brief Plane of type float.
 */
typedef Plane <float> Planef;

/** @brief Plane of type double.
 */
typedef Plane <double> Planed;

#endif