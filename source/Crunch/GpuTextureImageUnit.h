/** @file GpuTextureImageUnit.h
 *  @brief Encapsulates a texture image unit on the GPU.
 */

#ifndef GPUTEXTUREIMAGEUNIT_DEFINED
#define GPUTEXTUREIMAGEUNIT_DEFINED

#include "Common.h"
#include "GpuObject.h"
#include "GpuTexture.h"

/** @brief Encapsulates a texture image unit on the GPU.
 */
class GpuTextureImageUnit : public ReferenceCountedGpuObject {
public:
	/** @brief Sets this to be the active texture image unit.
	 */
	void setActive();

	/** @brief Returns whether this is the active texture image unit.
	 */
	bool isActive() const;

	/** @brief Binds the given texture to this texture image unit.
	 */
	void bindTexture( const GpuTextureReference & texture );

	/** @brief Unbinds the currently bound texture from this texture image unit.
	 */
	void unbindTexture( GLenum location );

	/** @brief Returns the texture currently bound to the given location.
	 */
	GpuTextureReference getBoundTexture( GLenum location );

	/** @brief Returns the texture currently bound to the given location.
	 */
	GpuTextureConstReference getBoundTexture( GLenum location ) const;

private:
	friend class GpuState;
	GpuTextureImageUnit( GpuState * state, size_t i );
	void onRelease() const;

	enum TextureBindingLocation {
		TBL_TEXTURE_1D,
		TBL_TEXTURE_1D_ARRAY,
		TBL_TEXTURE_2D,
		TBL_TEXTURE_2D_ARRAY,
		TBL_TEXTURE_2D_MULTISAMPLE,
		TBL_TEXTURE_2D_MULTISAMPLE_ARRAY,
		TBL_TEXTURE_3D,
		TBL_TEXTURE_BUFFER,
		TBL_TEXTURE_CUBE_MAP,
		TBL_TEXTURE_CUBE_MAP_ARRAY,
		TBL_TEXTURE_RECTANGLE,
		TEXTURE_BINDING_LOCATION_COUNT
	};
	static TextureBindingLocation getTextureBindingLocation( GLenum loc );

	size_t index;
	GpuTextureReference textureBindingLocations[TEXTURE_BINDING_LOCATION_COUNT];
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <GpuTextureImageUnit> GpuTextureImageUnitReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <GpuTextureImageUnit> GpuTextureImageUnitConstReference;

#endif