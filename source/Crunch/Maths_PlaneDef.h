#ifndef MATHS_PLANEDEF_DEFINED
#define MATHS_PLANEDEF_DEFINED

template <typename T> Plane <T>::Plane()
	: normal( 0, 0, 1 )
	, d( 0 ) {
}

template <typename T> Plane <T>::Plane( const Vector3 <T> & norm, T pd )
	: normal( norm )
	, d( pd ) {
}

template <typename T> Plane <T>::Plane( const Plane <T> & p )
	: normal( p.normal )
	, d( p.d ) {
}

template <typename T> Plane <T>::Plane( const Vector3 <T> & p0, const Vector3 <T> & p1, const Vector3 <T> & p2 )
	: normal( (p1 - p0).cross( p2 - p0 ) ) {
	d = -p0.dot( normal );
}

template <typename T> Plane <T>::Plane( const Vector3 <T> & norm, const Vector3 <T> & point )
	: normal( norm )
	, d( -norm.dot( point ) ) {
}

template <typename T> Plane <T>::~Plane() {
}

// operators
template <typename T> Plane <T> Plane <T>::operator=( const Plane <T> & p ) {
	normal = p.normal;
	d = p.d;
	return *this;
}

template <typename T> bool Plane <T>::operator==( const Plane <T> & p ) const {
	return (normal == p.normal && d == p.d);
}

template <typename T> bool Plane <T>::operator!=( const Plane <T> & p ) const {
	return (normal != p.normal || d != p.d);
}

template <typename T> template <typename U> Plane <T>::operator Plane <U> () const {
	return Plane <U>( (Vector3 <U>)normal, (U)d );
}

// mutable
template <typename T> void Plane <T>::set( const Vector3 <T> & p0, const Vector3 <T> & p1, const Vector3 <T> & p2 ) {
	normal = (p1 - p0).cross( p2 - p0 );
	d = -p0.dot( normal );
}

template <typename T> void Plane <T>::set( const Vector3 <T> & norm, T pd ) {
	normal = norm;
	d = pd;
}

template <typename T> void Plane <T>::set( const Vector3 <T> & norm, const Vector3 <T> & point ) {
	normal = norm;
	d = -norm.dot( point );
}

template <typename T> void Plane <T>::normalize() {
	T mag = normal.magnitude();
	normal /= mag;
	d /= mag;
}

// immutable
template <typename T> Plane <T> Plane <T>::normalized() const {
	T mag = normal.magnitude();
	return Plane <T>( normal/mag, d/mag );
}

template <typename T> bool Plane <T>::intersects( const Vector3 <T> & origin, const Vector3 <T> & direction, Vector3 <T> & intersection, T & signedDist ) const {
	T denom = normal.dot( direction );
	if (denom == 0)
		return false;
	signedDist = -(normal.dot( origin ) + d)/denom;
	intersection = origin + direction*signedDist;
	return true;
}

template <typename T> bool Plane <T>::intersects( const Ray3 <T> & ray, Vector3 <T> & intersection, T & signedDist ) const {
	return intersects( ray.origin, ray.direction, intersection, signedDist );
}

template <typename T> bool Plane <T>::intersects( const Vector3 <T> & origin, const Vector3 <T> & direction, Vector3 <T> & intersection ) const {
	T denom = normal.dot( direction );
	if (denom == 0)
		return false;
	T signedDist = -(normal.dot( origin ) + d)/denom;
	intersection = origin + direction*signedDist;
	return true;
}

template <typename T> bool Plane <T>::intersects( const Ray3 <T> & ray, Vector3 <T> & intersection ) const {
	return intersects( ray.origin, ray.direction, intersection );
}

template <typename T> bool Plane <T>::intersects( const Vector3 <T> & origin, const Vector3 <T> & direction, T & signedDist ) const {
	T denom = normal.dot( direction );
	if (denom == 0)
		return false;
	signedDist = -(normal.dot( origin ) + d)/denom;
	return true;
}

template <typename T> bool Plane <T>::intersects( const Ray3 <T> & ray, T & signedDist ) const {
	return intersects( ray.origin, ray.direction, signedDist );
}

template <typename T> T Plane <T>::distance( const Vector3 <T> & point ) const {
	return std::abs( normal.dot( point ) + d );
}

template <typename T> T Plane <T>::signedDistance( const Vector3 <T> & point ) const {
	return normal.dot( point ) + d;
}

template <typename T> T Plane <T>::side( const Vector3 <T> & point ) const {
	return sign( normal.dot( point ) + d );
}

template <typename T> bool Plane <T>::x( T & x, T y, T z ) const {
	if (normal.x == 0)
		return false;
	x = -(normal.y*y + normal.z*z + d)/normal.x;
	return true;
}

template <typename T> bool Plane <T>::y( T x, T & y, T z ) const {
	if (normal.y == 0)
		return false;
	y = -(normal.x*x + normal.z*z + d)/normal.y;
	return true;
}

template <typename T> bool Plane <T>::z( T x, T y, T & z ) const {
	if (normal.z == 0)
		return false;
	z = -(normal.x*x + normal.y*y + d)/normal.z;
	return true;
}

#endif