#include "GpuFramebuffer.h"
#include "GpuState.h"

GpuFramebuffer::GpuFramebuffer( GpuState * gpuState )
	: ReferenceCountedGpuObject( gpuState ) {
	glGenFramebuffers( 1, &id );
	bind( GL_FRAMEBUFFER );
	colorAttachments.resize( getGpuState()->getMaxColorAttachments() );
	depthAttachment.mipmapLevel = 0;
	depthAttachment.layer = 0;
	depthAttachment.layered = false;
	stencilAttachment.mipmapLevel = 0;
	stencilAttachment.layer = 0;
	stencilAttachment.layered = false;
	for (size_t i = 0; i < colorAttachments.size(); ++i) {
		colorAttachments[i].mipmapLevel = 0;
		colorAttachments[i].layer = 0;
		colorAttachments[i].layered = false;
	}
}

GpuFramebuffer::~GpuFramebuffer() {
	glDeleteFramebuffers( 1, &id );
}

GLuint GpuFramebuffer::getId() const {
	return id;
}

void GpuFramebuffer::attach( GLenum attachment, const GpuRenderbufferReference & renderbuffer ) {
	if (attachment == GL_DEPTH_STENCIL_ATTACHMENT) {
		if (depthAttachment.renderbuffer.get() == renderbuffer.get() &&
			stencilAttachment.renderbuffer.get() == renderbuffer.get())
			// already attached
			return;

		bind( GL_FRAMEBUFFER );
		glFramebufferRenderbuffer( GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, renderbuffer->getId() );
		depthAttachment.renderbuffer = renderbuffer;
		depthAttachment.texture = GpuTextureReference();
		depthAttachment.mipmapLevel = 0;
		depthAttachment.layer = 0;
		stencilAttachment.renderbuffer = renderbuffer;
		stencilAttachment.texture = GpuTextureReference();
		stencilAttachment.mipmapLevel = 0;
		stencilAttachment.layer = 0;
	} else {
		Attachment & att = getAttachment( attachment );
		if (att.renderbuffer.get() == renderbuffer.get())
			// already attached
			return;

		bind( GL_FRAMEBUFFER );
		glFramebufferRenderbuffer( GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, renderbuffer->getId() );
		att.renderbuffer = renderbuffer;
		att.texture = GpuTextureReference();
		att.mipmapLevel = 0;
		att.layer = 0;
	}
}

void GpuFramebuffer::attach( GLenum attachment, const GpuTextureReference & texture, int mipmapLevel ) {
	if (attachment == GL_DEPTH_STENCIL_ATTACHMENT) {
		if (depthAttachment.texture.get() == texture.get() &&
			!depthAttachment.layered &&
			stencilAttachment.texture.get() == texture.get() &&
			!stencilAttachment.layered)
			// already attached
			return;

		bind( GL_FRAMEBUFFER );
		glFramebufferTexture( GL_FRAMEBUFFER, attachment, texture->getId(), mipmapLevel );
		depthAttachment.renderbuffer = GpuRenderbufferReference();
		depthAttachment.texture = texture;
		depthAttachment.mipmapLevel = mipmapLevel;
		depthAttachment.layer = 0;
		depthAttachment.layered = false;
		stencilAttachment.renderbuffer = GpuRenderbufferReference();
		stencilAttachment.texture = texture;
		stencilAttachment.mipmapLevel = mipmapLevel;
		stencilAttachment.layer = 0;
		stencilAttachment.layered = false;
	} else {
		Attachment & att = getAttachment( attachment );
		if (att.texture.get() == texture.get() &&
			!att.layered)
			// already attached
			return;

		bind( GL_FRAMEBUFFER );
		glFramebufferTexture( GL_FRAMEBUFFER, attachment, texture->getId(), mipmapLevel );
		att.renderbuffer = GpuRenderbufferReference();
		att.texture = texture;
		att.mipmapLevel = mipmapLevel;
		att.layer = 0;
		att.layered = false;
	}
}

void GpuFramebuffer::attachLayer( GLenum attachment, const GpuTextureReference & texture, uint layer, int mipmapLevel ) {
	if (attachment == GL_DEPTH_STENCIL_ATTACHMENT) {
		if (depthAttachment.texture.get() == texture.get() &&
			depthAttachment.layered &&
			depthAttachment.layer == layer &&
			stencilAttachment.texture.get() == texture.get() &&
			stencilAttachment.layered &&
			stencilAttachment.layer == layer)
			// already attached
			return;

		bind( GL_FRAMEBUFFER );
		// glFramebufferTextureLayer() doesn't seem to work for cube maps
		if (texture->getType() == GL_TEXTURE_CUBE_MAP)
			glFramebufferTexture2D( GL_FRAMEBUFFER, attachment, GL_TEXTURE_CUBE_MAP_POSITIVE_X + layer, texture->getId(), mipmapLevel );
		else
			glFramebufferTextureLayer( GL_FRAMEBUFFER, attachment, texture->getId(), mipmapLevel, (int)layer );
		depthAttachment.renderbuffer = GpuRenderbufferReference();
		depthAttachment.texture = texture;
		depthAttachment.mipmapLevel = mipmapLevel;
		depthAttachment.layer = layer;
		depthAttachment.layered = true;
		stencilAttachment.renderbuffer = GpuRenderbufferReference();
		stencilAttachment.texture = texture;
		stencilAttachment.mipmapLevel = mipmapLevel;
		stencilAttachment.layer = layer;
		stencilAttachment.layered = true;
	} else {
		Attachment & att = getAttachment( attachment );
		if (att.texture.get() == texture.get() &&
			att.layered &&
			att.layer == layer)
			// already attached
			return;

		bind( GL_FRAMEBUFFER );
		// glFramebufferTextureLayer() doesn't seem to work for cube maps
		if (texture->getType() == GL_TEXTURE_CUBE_MAP)
			glFramebufferTexture2D( GL_FRAMEBUFFER, attachment, GL_TEXTURE_CUBE_MAP_POSITIVE_X + layer, texture->getId(), mipmapLevel );
		else
			glFramebufferTextureLayer( GL_FRAMEBUFFER, attachment, texture->getId(), mipmapLevel, (int)layer );
		att.renderbuffer = GpuRenderbufferReference();
		att.texture = texture;
		att.mipmapLevel = mipmapLevel;
		att.layer = layer;
		att.layered = true;
	}
}

void GpuFramebuffer::detatch( GLenum attachment ) {
	if (attachment == GL_DEPTH_STENCIL_ATTACHMENT) {
		if (!depthAttachment.renderbuffer &&
			!depthAttachment.texture &&
			!stencilAttachment.renderbuffer &&
			!stencilAttachment.texture)
			// already detatched
			return;

		bind( GL_FRAMEBUFFER );
		glFramebufferRenderbuffer( GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, 0 );
		depthAttachment.renderbuffer = GpuRenderbufferReference();
		depthAttachment.texture = GpuTextureReference();
		depthAttachment.mipmapLevel = 0;
		depthAttachment.layer = 0;
		stencilAttachment.renderbuffer = GpuRenderbufferReference();
		stencilAttachment.texture = GpuTextureReference();
		stencilAttachment.mipmapLevel = 0;
		stencilAttachment.layer = 0;
	} else {
		Attachment & att = getAttachment( attachment );
		if (!att.renderbuffer &&
			!att.texture)
			// already detached
			return;

		bind( GL_FRAMEBUFFER );
		glFramebufferRenderbuffer( GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, 0 );
		att.renderbuffer = GpuRenderbufferReference();
		att.texture = GpuTextureReference();
		att.mipmapLevel = 0;
		att.layer = 0;
	}
}

GpuRenderbufferReference GpuFramebuffer::getAttachedRenderbuffer( GLenum attachment ) {
	return getAttachment( attachment ).renderbuffer;
}

GpuRenderbufferConstReference GpuFramebuffer::getAttachedRenderbuffer( GLenum attachment ) const {
	return getAttachment( attachment ).renderbuffer;
}

GpuTextureReference GpuFramebuffer::getAttachedTexture( GLenum attachment ) {
	return getAttachment( attachment ).texture;
}

GpuTextureConstReference GpuFramebuffer::getAttachedTexture( GLenum attachment ) const {
	return getAttachment( attachment ).texture;
}

int GpuFramebuffer::getAttachmentLevel( GLenum attachment ) const {
	return getAttachment( attachment ).mipmapLevel;
}

uint GpuFramebuffer::getAttachmentLayer( GLenum attachment ) const {
	return getAttachment( attachment ).layer;
}

void GpuFramebuffer::bind( GLenum location ) {
	getGpuState()->bindFramebuffer( location, this );
}

void GpuFramebuffer::onRelease() const {
	// no more references
	delete this;
}

GpuFramebuffer::Attachment & GpuFramebuffer::getAttachment( GLenum location ) {
	if (location == GL_DEPTH_ATTACHMENT)
		return depthAttachment;
	else if (location == GL_STENCIL_ATTACHMENT)
		return stencilAttachment;
	else {
		if (location < GL_COLOR_ATTACHMENT0 || location >= GL_COLOR_ATTACHMENT0 + (uint)colorAttachments.size())
			throw std::runtime_error( "Invalid framebuffer attachment" );
		else
			return colorAttachments[location - GL_COLOR_ATTACHMENT0];
	}
}

const GpuFramebuffer::Attachment & GpuFramebuffer::getAttachment( GLenum location ) const {
	if (location == GL_DEPTH_ATTACHMENT)
		return depthAttachment;
	else if (location == GL_STENCIL_ATTACHMENT)
		return stencilAttachment;
	else {
		if (location < GL_COLOR_ATTACHMENT0 || location >= GL_COLOR_ATTACHMENT0 + (uint)colorAttachments.size())
			throw std::runtime_error( "Invalid framebuffer attachment" );
		else
			return colorAttachments[location - GL_COLOR_ATTACHMENT0];
	}
}
