/** @file GameState.h
 *  @brief Contains the base object for a game state.
 */

#ifndef GAMESTATE_DEFINED
#define GAMESTATE_DEFINED

#include "Common.h"
#include "Actor.h"
#include "Indexer.h"
#include <map>
#include <vector>

/** @brief The base class game states are derived from.
 */
class GameState : private boost::noncopyable {
protected:
	GameState( int updatePrior, int renderPrior, size_t idx );	/**< The constructor.*/
	virtual ~GameState();										/**< The virtual destructor.*/

private:
	void update();	/**< Updates all actors.*/
	void render();	/**< Renders all actors.*/

public:
	/** @brief Create an actor.
	 */
	template <typename T> T * createActor();

	/** @brief Create an actor, overriding update priority.
	 */
	template <typename T> T * createActorU( int updatePri );

	/** @brief Create an actor, overriding render priority.
	 */
	template <typename T> T * createActorR( int renderPri );

	/** @brief Create an actor, overriding update and render priority.
	 */
	template <typename T> T * createActorUR( int updatePri, int renderPri );

	/** @brief Returns the actor with the given index.
	 */
	Actor * getActor( size_t index );

	/** @brief Destroys the actor with the given index.
	 */
	void destroyActor( size_t index );

	/** @brief Activates the actor with the given index.
	 */
	void activateActor( size_t index );

	/** @brief Deactivates the actor with the given index.
	 */
	void deactivateActor( size_t index );

	/** @brief Hides the actor with the given index.
	 */
	void hideActor( size_t index );

	/** @brief Shows the actor with the given index.
	 */
	void showActor( size_t index );

	/** @brief Returns whether the actor with the given index is active.
	 */
	bool isActorActive( size_t index ) const;

	/** @brief Returns whether the actor with the given index is visible.
	 */
	bool isActorVisible( size_t index ) const;

	int getUpdatePriority() const;	/**< Returns the update priority.*/
	int getRenderPriority() const;	/**< Returns the render priority.*/
	size_t getIndex() const;		/**< Returns the index of this game state.*/
	bool isActive() const;			/**< Returns whether the game state is active.*/
	bool isVisible() const;			/**< Returns whether the game state is visible.*/
	void activate();				/**< Activates the game state.*/
	void deactivate();				/**< Deactivates the game state.*/
	void hide();					/**< Hides the game state.*/
	void show();					/**< Shows the game state.*/
	void destroy();					/**< Destroys the game state.*/

	virtual void onCreate();		/**< The onCreate event.*/
	virtual void onUpdate();		/**< The onUpdate event.*/
	virtual void onPostUpdate();	/**< The onPostUpdate event.*/
	virtual void onDestroy();		/**< The onDestroy event.*/
	virtual void onDraw();			/**< The onDraw event.*/
	virtual void onActivate();		/**< The onActivate event.*/
	virtual void onDeactivate();	/**< The onDeactivate event.*/

	void removeActors();	/**< Removes all actors.*/

private:
	friend class GameStateManager;
	// stores the actor with metadata
	struct ActorData {
		Actor * actor;											// the actor
		std::multimap <int, size_t>::iterator updatePosition;	// location in the update list, or end if inactive
		std::multimap <int, size_t>::iterator renderPosition;	// location in the render list, or end if hidden
	};

	Indexer <ActorData> actors;				// list of current actors
	std::multimap <int, size_t> updateList;	// ordered list for updating actors
	std::multimap <int, size_t> renderList;	// ordered list for rendering actors
	std::multimap <int, size_t>::iterator updateIterator;	// Iterator used to run through update list
	std::multimap <int, size_t>::iterator renderIterator;	// Iterator used to run through render list
	// NOTE: these are fields rather than temporaries in the update()
	// and render() functions because if the object currently being updated
	// is deleted, the iterator will need to be fixed!
	std::vector <Actor*> deadList;

	int updatePriority;	// priority in the update list
	int renderPriority;	// priority in the render list
	size_t index;		// index of this game state
};

template <typename T> T * GameState::createActor() {
	return createActorUR <T>( T::getDefaultUpdatePriority(), T::getDefaultRenderPriority() );
}

template <typename T> T * GameState::createActorU( int updatePri ) {
	return createActorUR <T>( updatePri, T::getDefaultRenderPriority() );
}

template <typename T> T * GameState::createActorR( int renderPri ) {
	return createActorUR <T>( T::getDefaultUpdatePriority(), renderPri );
}

template <typename T> T * GameState::createActorUR( int updatePri, int renderPri ) {
	ActorData ad;
	ad.actor = NULL;
	ad.updatePosition = updateList.end();
	ad.renderPosition = renderList.end();

	size_t actorIndex = actors.add( ad );
	T * actor = new T( updatePri, renderPri, actorIndex, index );
	actors.get( actorIndex ).actor = actor;

	int updatePriority = actor->getUpdatePriority();
	int renderPriority = actor->getRenderPriority();
	actors.get( actorIndex ).updatePosition = updateList.insert( std::make_pair( updatePriority, actorIndex ) );
	actors.get( actorIndex ).renderPosition = renderList.insert( std::make_pair( renderPriority, actorIndex ) );

	// call onCreate
	actor->onCreate();

	return actor;
}

/** @brief Defines a game state with the given name, update priority, and render priority.
 */
#define DEFINE_GAME_STATE( name, updatePriority, renderPriority )	\
class name : public GameState {										\
public:																\
	static int getDefaultUpdatePriority() {							\
		return (updatePriority);									\
	}																\
																	\
	static int getDefaultRenderPriority() {							\
		return (renderPriority);									\
	}																\
																	\
protected:															\
	friend class GameStateManager;									\
	name( int updatePrior, int renderPrior, size_t idx )			\
		: GameState( updatePrior, renderPrior, idx ) {				\
	}																\
																	\
private:

/** @brief Defines a game state derived from parent with the given name, update priority, and render priority.
 */
#define DEFINE_DERIVED_GAME_STATE( name, parent, updatePriority, renderPriority )	\
class name : public parent {														\
public:																				\
	static int getDefaultUpdatePriority() {											\
		return (updatePriority);													\
	}																				\
																					\
	static int getDefaultRenderPriority() {											\
		return (renderPriority);													\
	}																				\
																					\
protected:																			\
	friend class GameStateManager;													\
	name( int updatePrior, int renderPrior, size_t idx )							\
		: parent( updatePrior, renderPrior, idx ) {									\
	}																				\
																					\
private:

#endif