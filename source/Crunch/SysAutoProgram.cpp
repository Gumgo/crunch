#include "SysAutoProgram.h"
#include "Context.h"
#include "Log.h"

SysAutoProgram::SysAutoProgram() {
}

SysAutoProgram::~SysAutoProgram() {
}

void SysAutoProgram::setProgram( const GpuProgramReference & prog ) {
	program = prog;
	attributes.clear();
	uniforms.clear();
}

void SysAutoProgram::setAttributes( MeshResource & mesh ) {
	if (!program)
		return;

	attributes.clear();
	for (size_t i = 0; i < mesh.getAttributeCount(); ++i) {
		const MeshResource::AttributeData & mAt = mesh.getAttribute( i );
		size_t index;
		if (!program->getAttributeIndex( mAt.name, index ))
			continue;
		const GpuProgram::AttributeDescription & pAt = program->getAttributeDescription( index );
		GpuProgram::GlslAttributeTypeDescription desc = GpuProgram::getGlslAttributeTypeDescription( pAt.type );
		if (mAt.components != desc.elements)
			continue;
		attributes.push_back( Attribute() );
		Attribute & at = attributes.back();
		at.index = index;
		at.buffer = mesh.getVertexBuffer();
		at.size = mAt.components;
		at.type = mAt.type;
		at.normalized = mAt.normalized;
		at.integer = mAt.integer;
		at.stride = (uint)mAt.stride;
		at.pointer = bufferOffset( mAt.offset );
		at.arrayOffset = 0;
		at.arrayCount = 1;
	}
}

size_t SysAutoProgram::setAttribute( const std::string & name, const GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer,
	uint stride, const void * pointer, size_t arrayOffset, size_t arrayCount ) {
	if (!program || (normalized && integer))
		return std::numeric_limits <size_t>::max();

	size_t index;
	if (!program->getAttributeIndex( name, index )) {
		Log::warning() << "Attempted to set nonexistent vertex attribute " << name;
		return std::numeric_limits <size_t>::max();
	}

	const GpuProgram::AttributeDescription pAt = program->getAttributeDescription( index );
	attributes.push_back( Attribute() );
	Attribute & at = attributes.back();
	at.index = index;
	at.buffer = buffer;
	at.size = size;
	at.type = type;
	at.normalized = normalized;
	at.integer = integer;
	at.stride = stride;
	at.pointer = pointer;
	at.arrayOffset = arrayOffset;
	at.arrayCount = arrayCount;
	return attributes.size()-1;
}

size_t SysAutoProgram::setUniform( const std::string & name, GLenum type, const void * value, size_t arrayOffset, size_t arrayCount ) {
	if (!program)
		return std::numeric_limits <size_t>::max();

	size_t index;
	if (!program->getUniformIndex( name, index )) {
		Log::warning() << "Attempted to set nonexistent uniform " << name;
		return std::numeric_limits <size_t>::max();
	}
	const GpuProgram::UniformDescription & desc = program->getUniformDescription( index );
	if (type != desc.type) {
		Log::warning() << "Attempted to set uniform " << name << " to incorrect type";
		return std::numeric_limits <size_t>::max();
	}
	uniforms.push_back( Uniform() );
	Uniform & un = uniforms.back();
	un.index = index;
	un.type = type;
	un.value = value;
	un.arrayOffset = arrayOffset;
	un.arrayCount = arrayCount;
	return uniforms.size()-1;
}

bool SysAutoProgram::alterAttribute( size_t index, const GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer,
	uint stride, const void * pointer, size_t arrayOffset, size_t arrayCount ) {
	if (!program || (normalized && integer))
		return false;

	if (index >= attributes.size()) {
		Log::warning() << "Attempted to alter invalid vertex attribute";
		return false;
	}

	Attribute & at = attributes[index];
	const GpuProgram::AttributeDescription pAt = program->getAttributeDescription( at.index );
	at.buffer = buffer;
	at.size = size;
	at.type = type;
	at.normalized = normalized;
	at.integer = integer;
	at.stride = stride;
	at.pointer = pointer;
	at.arrayOffset = arrayOffset;
	at.arrayCount = arrayCount;
	return true;
}

bool SysAutoProgram::alterAttributeBuffer( size_t index, const GpuBufferReference & buffer ) {
	if (!program)
		return false;

	if (index >= attributes.size()) {
		Log::warning() << "Attempted to alter invalid vertex attribute";
		return false;
	}

	Attribute & at = attributes[index];
	const GpuProgram::AttributeDescription pAt = program->getAttributeDescription( at.index );
	at.buffer = buffer;
	return true;
}

bool SysAutoProgram::alterUniform( size_t index, GLenum type, const void * value, size_t arrayOffset, size_t arrayCount ) {
	if (!program)
		return false;

	if (index >= uniforms.size()) {
		Log::warning() << "Attempted to alter invalid uniform";
		return false;
	}

	Uniform & un = uniforms[index];
	const GpuProgram::UniformDescription & desc = program->getUniformDescription( un.index );
	if (type != desc.type) {
		Log::warning() << "Attempted to alter uniform to incorrect type";
		return false;
	}
	un.type = type;
	un.value = value;
	un.arrayOffset = arrayOffset;
	un.arrayCount = arrayCount;
	return true;
}

void SysAutoProgram::clear() {
	attributes.clear();
	uniforms.clear();
}

void SysAutoProgram::bind() {
	if (!program)
		return;

	program->bind();

	for (size_t i = 0; i < attributes.size(); ++i) {
		Attribute & a = attributes[i];
		program->setAttribute(
			a.index, a.buffer, a.size, a.type, a.normalized, a.integer, a.stride, a.pointer, a.arrayOffset, a.arrayCount );
	}

	for (size_t i = 0; i < uniforms.size(); ++i) {
		const Uniform & u = uniforms[i];
		program->setUniform(
			u.index, u.type, u.value, u.arrayOffset, u.arrayCount );
	}
}