#include "MemoryDebug.h"

#if (defined _DEBUG && defined _WIN32)

static _CrtMemState dbgMemStateBegin;
static _CrtMemState dbgMemStateEnd;
static _CrtMemState dbgMemState1;
static _CrtMemState dbgMemState2;
static _CrtMemState dbgMemStateTemp;
static HANDLE dbgMemFile;

void memDbgInit() {
	dbgMemFile = CreateFile( "Memlog.txt", GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
	_CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
	_CrtSetReportFile( _CRT_WARN, dbgMemFile );
	_CrtMemCheckpoint( &dbgMemStateBegin );
}

void memDbgSnapshot1() {
	_CrtMemCheckpoint( &dbgMemState1 );
}

void memDbgSnapshot2() {
	_CrtMemCheckpoint( &dbgMemState2 );
}

void memDbgCompare() {
	if (_CrtMemDifference( &dbgMemStateTemp, &dbgMemState1, &dbgMemState2 ))
		MessageBox( NULL, "Memory leak detected!", "Error", MB_OK | MB_ICONEXCLAMATION );
}

void memDbgTerm() {
	_CrtMemCheckpoint( &dbgMemStateEnd );
	if (_CrtMemDifference( &dbgMemStateTemp, &dbgMemStateBegin, &dbgMemStateEnd ))
		MessageBox( NULL, "Memory leak detected!", "Error", MB_OK | MB_ICONEXCLAMATION );
	_CrtMemDumpAllObjectsSince( &dbgMemStateBegin );
	CloseHandle( dbgMemFile );
}

#else

void memDbgInit() {
}

void memDbgSnapshot1() {
}

void memDbgSnapshot2() {
}

void memDbgCompare() {
}

void memDbgTerm() {
}

#endif