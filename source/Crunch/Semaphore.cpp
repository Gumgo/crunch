#include "Semaphore.h"

Semaphore::Semaphore()
	: count( 0 ) {
}

void Semaphore::signal() {
	boost::mutex::scoped_lock lock( mutex );
	++count;
	condition.notify_one();
}

void Semaphore::wait() {
	boost::mutex::scoped_lock lock( mutex );
	while (count == 0)
		condition.wait( lock );
	--count;
}