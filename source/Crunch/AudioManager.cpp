#include "AudioManager.h"
#include "Context.h"
#include "Log.h"

AudioManager::AudioManager()
	: soundGroups( 10 )
	, sounds( (size_t)(MAX_CHANNELS * 2) ) {
	system = NULL;

	if (Context::get().isServer())
		return;

	try {
		FMOD_RESULT result;

		result = FMOD::System_Create( &system );
		if (result != FMOD_OK)
			throw std::runtime_error( "Failed to create FMOD system" );

		uint version;
		result = system->getVersion( &version );
		if (result != FMOD_OK)
			throw std::runtime_error( "Failed to get FMOD version" );

		if (version < FMOD_VERSION)
			throw std::runtime_error( "An older version of FMOD is being used" );

		int driverCount;
		result = system->getNumDrivers( &driverCount );
		if (driverCount == 0) {
			Log::warning() << "No audio drivers detected: disabling audio";
			result = system->setOutput( FMOD_OUTPUTTYPE_NOSOUND );
			if (result != FMOD_OK)
				throw std::runtime_error( "Failed to disable audio" );
		} else {
			FMOD_CAPS caps;
			FMOD_SPEAKERMODE speakerMode;
			result = system->getDriverCaps( 0, &caps, NULL, &speakerMode );
			if (result != FMOD_OK)
				throw std::runtime_error( "Failed to get audio driver capabilities" );

			result = system->setSpeakerMode( speakerMode );
			if (result != FMOD_OK)
				throw std::runtime_error( "Failed to set speaker mode" );

			if (caps & FMOD_CAPS_HARDWARE_EMULATED) {
				Log::warning() << "Audio hardware acceleration disabled; may cause poor audio latency";
				result = system->setDSPBufferSize( 1024, 10 );
				if (result != FMOD_OK)
					throw std::runtime_error( "Failed to set DSP buffer size" );
			}

			char name[256];
			result = system->getDriverInfo( 0, name, sizeof( char ), 0 );
			if (result != FMOD_OK)
				throw std::runtime_error( "Failed to get audio driver info" );

			if (strstr( name, "SigmaTel" )) {
				// fix a crackly audio problem with these drivers
				result = system->setSoftwareFormat( 48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR );
				if (result != FMOD_OK)
					throw std::runtime_error( "Failed to set software audio format" );
			}
		}

		result = system->init( MAX_CHANNELS, FMOD_INIT_3D_RIGHTHANDED, 0 );
		if (result == FMOD_ERR_OUTPUT_CREATEBUFFER) {
			result = system->setSpeakerMode( FMOD_SPEAKERMODE_STEREO );
			if (result != FMOD_OK)
				throw std::runtime_error( "Failed to switch to stereo speaker mode" );
			result = system->init( MAX_CHANNELS, FMOD_INIT_NORMAL, 0 );
		}
		if (result != FMOD_OK)
			throw std::runtime_error( "Failed to initialize FMOD system" );
	} catch (const std::exception &) {
		if (system != NULL) {
			system->release();
			system = NULL;
		}
		throw;
	}
}

AudioManager::~AudioManager() {
	if (system != NULL)
		system->release();
}

FMOD::System * AudioManager::getSystem() {
	return system;
}

FMOD_RESULT F_CALLBACK AudioManager::channelCallback( FMOD_CHANNEL * channel, FMOD_CHANNEL_CALLBACKTYPE type, void * commanddata1, void * commanddata2 ) {
	AudioManager * thisPtr = &Context::get().audioManager;
	FMOD::Channel * soundChannel = (FMOD::Channel*)channel;

	if (type == FMOD_CHANNEL_CALLBACKTYPE_END) {
		InternalSound & sound = thisPtr->sounds.get( soundChannel );
		// TODO: call sound's "on finished" callback

		thisPtr->sounds.remove( soundChannel );
	}

	return FMOD_OK;
}

void AudioManager::update() {
	system->update();
}

SoundGroup AudioManager::createSoundGroup() {
	FMOD::ChannelGroup * cg;
	FMOD_RESULT result = system->createChannelGroup( NULL, &cg );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to create sound group" );

	InternalSoundGroup isg;
	soundGroups.put( cg, isg );
	return SoundGroup( this, cg );
}

void AudioManager::destroySoundGroup( const SoundGroup & sg ) {
	if (!soundGroups.remove( sg.channelGroup )) {
		Log::warning() << "Failed to destroy nonexistent sound group";
		return;
	}

	FMOD_RESULT result = sg.channelGroup->release();
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to destroy sound group" );
}

SoundGroup AudioManager::getDefaultSoundGroup() {
	return SoundGroup( this, NULL );
}

void AudioManager::set3DListenerAttributes( const Vector3f & pos, const Vector3f & vel, const Vector3f & fwd, const Vector3f & up ) {
	FMOD_VECTOR position, velocity, forwardVec, upVec;
	position.x = pos.x;
	position.y = pos.y;
	position.z = pos.z;
	velocity.x = vel.x;
	velocity.y = vel.y;
	velocity.z = vel.z;
	forwardVec.x = fwd.x;
	forwardVec.y = fwd.y;
	forwardVec.z = fwd.z;
	upVec.x = up.x;
	upVec.y = up.y;
	upVec.z = up.z;
	system->set3DListenerAttributes( 0, &position, &velocity, &forwardVec, &upVec );
}

Sound2D AudioManager::createSound2D( const SoundGroup & sg, SoundResourceReference & sound, bool createPaused ) {
	if (!sound)
		throw std::runtime_error( "Failed to play nonexistent sound" );

	FMOD_RESULT result;

	FMOD::Channel * ch;
	result = system->playSound( FMOD_CHANNEL_FREE, sound->getSound(), true, &ch );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to play sound" );

	if (sg.channelGroup != NULL) {
		assert( soundGroups.contains( sg.channelGroup ) );
		result = ch->setChannelGroup( sg.channelGroup );
		if (result != FMOD_OK) {
			ch->stop();
			throw std::runtime_error( "Failed to assign sound to sound channel group" );
		}
	}

	result = ch->setCallback( &channelCallback );
	if (result != FMOD_OK) {
		ch->stop();
		throw std::runtime_error( "Failed to set callback for sound channel" );
	}

	if (!createPaused) {
		result = ch->setPaused( false );
		if (result != FMOD_OK) {
			ch->stop();
			throw std::runtime_error( "Failed to start sound" );
		}
	}

	InternalSound is;
	is.resource = sound;
	sounds.put( ch, is );

	return Sound2D( this, ch );
}

Sound3D AudioManager::createSound3D( const SoundGroup & sg, SoundResourceReference & sound, const Vector3f & pos, const Vector3f & vel, bool createPaused ) {
	if (!sound)
		throw std::runtime_error( "Failed to play nonexistent sound" );

	FMOD_RESULT result;

	FMOD::Channel * ch;
	result = system->playSound( FMOD_CHANNEL_FREE, sound->getSound(), true, &ch );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to play sound" );

	if (sg.channelGroup != NULL) {
		assert( soundGroups.contains( sg.channelGroup ) );
		result = ch->setChannelGroup( sg.channelGroup );
		if (result != FMOD_OK) {
			ch->stop();
			throw std::runtime_error( "Failed to assign sound to sound channel group" );
		}
	}

	result = ch->setCallback( &channelCallback );
	if (result != FMOD_OK) {
		ch->stop();
		throw std::runtime_error( "Failed to set callback for sound" );
	}

	FMOD_VECTOR position, velocity;
	position.x = pos.x;
	position.y = pos.y;
	position.z = pos.z;
	velocity.x = vel.x;
	velocity.y = vel.y;
	velocity.z = vel.z;
	result = ch->set3DAttributes( &position, &velocity );
	if (result != FMOD_OK) {
		ch->stop();
		throw std::runtime_error( "Failed to set 3D attributes for sound" );
	}

	if (!createPaused) {
		result = ch->setPaused( false );
		if (result != FMOD_OK) {
			ch->stop();
			throw std::runtime_error( "Failed to start sound" );
		}
	}

	InternalSound is;
	is.resource = sound;
	sounds.put( ch, is );

	return Sound3D( this, ch );
}

void AudioManager::play( const SoundGroup & sg ) {
	FMOD_RESULT result = sg.channelGroup->setPaused( false );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to play sound group" );
}

void AudioManager::pause( const SoundGroup & sg ) {
	FMOD_RESULT result = sg.channelGroup->setPaused( true );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to pause sound group" );
}

void AudioManager::stop( const SoundGroup & sg ) {
	FMOD_RESULT result = sg.channelGroup->stop();
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to stop sound group" );
}

void AudioManager::setVolume( const SoundGroup & sg, float vol ) {
	FMOD_RESULT result = sg.channelGroup->setVolume( vol );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to set sound group volume" );
}

void AudioManager::play( const Sound & s ) {
	FMOD_RESULT result = s.channel->setPaused( false );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to play sound" );
}

void AudioManager::pause( const Sound & s ) {
	FMOD_RESULT result = s.channel->setPaused( true );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to pause sound" );
}

void AudioManager::stop( const Sound & s ) {
	FMOD_RESULT result = s.channel->stop();
	if (!sounds.remove( s.channel ))
		throw std::runtime_error( "Failed to stop nonexistent sound" );

	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to stop sound" );
}

void AudioManager::setVolume( const Sound & s, float vol ) {
	FMOD_RESULT result = s.channel->setVolume( vol );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to set sound volume" );
}

void AudioManager::setPanning( const Sound2D & s, float pan ) {
	FMOD_RESULT result = s.channel->setPan( pan );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to set sound panning" );
}

void AudioManager::setPositionVelocity( const Sound3D & s, const Vector3f & pos, const Vector3f & vel ) {
	FMOD_VECTOR position, velocity;
	position.x = pos.x;
	position.y = pos.y;
	position.z = pos.z;
	velocity.x = vel.x;
	velocity.y = vel.y;
	velocity.z = vel.z;
	FMOD_RESULT result = s.channel->set3DAttributes( &position, &velocity );
	if (result != FMOD_OK)
		throw std::runtime_error( "Failed to set 3D attributes for sound" );
}