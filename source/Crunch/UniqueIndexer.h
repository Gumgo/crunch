/** @file UniqueIndexer.h
 *  @brief Used to assign unique indices to objects.
 */

#ifndef UNIQUEINDEXER_DEFINED
#define UNIQUEINDEXER_DEFINED

#include "Common.h"
#include <vector>

/** @brief Used to assign indices to objects.
 *
 *  @tparam T	The type of object to index.
 */
template <typename T> class UniqueIndexer {
public:
	/** @brief The index type for unsafe indices.
	 */
	typedef size_t UnsafeIndex;

	/** @brief The unique identifier type.
	 */
	typedef uint Id;

	/** @brief The index type, which consists of an array index and a unique identifier.
	 */
	class Index {
	public:
		/** @brief Constructs an index.
		 */
		Index();

		/** @brief Returns the unsafe component.
		 */
		UnsafeIndex getUnsafeIndex() const;

		/** @brief Returns the unique identifier component.
		 */
		Id getId() const;

	private:
		friend class UniqueIndexer <T>;
		Index( UnsafeIndex indexPart, Id idPart );
		UnsafeIndex index;	// the array index component
		Id id;				// the unique ID
	};

	/** @brief Provides a way to iterate over all entries.
	 */
	class Iterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		T & get();				/**< Returns the current entry.*/
		Index getIndex();		/**< Returns the index of the current entry.*/
		void remove();			/**< Removes the current entry.*/

	private:
		friend class UniqueIndexer <T>;

		UniqueIndexer <T> * base;	// the indexer this iterator is using
		size_t currentIndex;		// the current index being pointed to
		size_t nextIndex;			// the next index to be pointed to

		Iterator( UniqueIndexer <T> * b );
	};

	/** @brief Provides a way to iterate over all entries.
	 */
	class ConstIterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		const T & get();		/**< Returns the current entry.*/
		Index getIndex();		/**< Returns the index of the current entry.*/

	private:
		friend class UniqueIndexer <T>;

		const UniqueIndexer <T> * base;	// the indexer this iterator is using
		size_t currentIndex;			// the current index being pointed to
		size_t nextIndex;				// the next index to be pointed to

		ConstIterator( const UniqueIndexer <T> * b );
	};

	friend class Iterator;
	friend class ConstIterator;

	/** @brief The constructor.
	 *
	 *  @param defaultCapacity	The default capacity for entries.
	 */
	UniqueIndexer( size_t defaultCapacity = 20 );

	/** @brief The copy constructor.
	 *
	 *  @param other	The UniqueIndexer to copy.
	 */
	UniqueIndexer( const UniqueIndexer <T> & other );

	/** @brief The destructor.
	 */
	~UniqueIndexer();

	/** @brief The move constructor.
	 *
	 *  @param other	The UniqueIndexer to move.
	 */
	UniqueIndexer( UniqueIndexer <T> && other );

	/** @brief The copy assignment operator.
	 *
	 *  @param other	The UniqueIndexer to assign.
	 */
	UniqueIndexer <T> & operator=( const UniqueIndexer <T> & other );

	/** @brief The move assignment operator.
	 *
	 *  @param other	The UniqueIndexer to move.
	 */
	UniqueIndexer <T> & operator=( UniqueIndexer <T> && other );

	/** @brief Adds and assigns an index to the object.
	 *
	 *  @param t	The object to index.
	 *  @return		The assigned index.
	 */
	Index add( const T & t );

	/** @brief Adds and assigns an index to the object.
	 *
	 *  @param t	The object to index.
	 *  @return		The assigned index.
	 */
	Index add( T && t );

	/** @brief Returns the object associated with the given index.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @return			The object associated with the given index.
	 */
	T & get( const Index & index );

	/** @brief Returns the object associated with the given unsafe index.
	 *
	 *  @param index	The unsafe index for which to find the associated object.
	 *  @return			The object associated with the given unsafe index.
	 */
	T & getUnsafe( UnsafeIndex index );

	/** @brief Returns the object associated with the given index.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @return			The object associated with the given index.
	 */
	const T & get( const Index & index ) const;

	/** @brief Returns the object associated with the given unsafe index.
	 *
	 *  @param index	The unsafe index for which to find the associated object.
	 *  @return			The object associated with the given unsafe index.
	 */
	const T & getUnsafe( Index index ) const;

	/** @brief Assigns the object associated with the given index to t if it exists.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @param t		The destination to store the object.
	 *  @return			Whether the object was found and assigned.
	 */
	bool get( const Index & index, T & t ) const;

	/** @brief Assigns the object associated with the given unsafe index to t if it exists.
	 *
	 *  @param index	The unsafe index for which to find the associated object.
	 *  @param t		The destination to store the object.
	 *  @return			Whether the object was found and assigned.
	 */
	bool getUnsafe( UnsafeIndex Index, T & t ) const;

	/** @brief Removes and unindexes the object associated with the index.
	 *
	 *  @param index	The index associated with the object to remove.
	 *  @return			Whether the object was found and removed.
	 */
	bool remove( const Index & index );

	/** @brief Removes and unindexes the object associated with the unsafe index.
	 *
	 *  @param index	The unsafe index associated with the object to remove.
	 *  @return			Whether the object was found and removed.
	 */
	bool removeUnsafe( UnsafeIndex index );

	/** @brief Removes, unindexes, and returns the object associated with the given index.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @return			The object associated with the given index.
	 */
	T extract( const Index & index );

	/** @brief Removes, unindexes, and returns the object associated with the given unsafe index.
	 *
	 *  @param index	The unsafe index for which to find the associated object.
	 *  @return			The object associated with the given unsafe index.
	 */
	T extract( UnsafeIndex index );

	/** @brief Assigns the object associated with the given index to t and removes and unindexes it if it exists.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @param t		The destination to store the object.
	 *  @return			Whether the object was found and assigned.
	 */
	bool extract( const Index & index, T & t );

	/** @brief Assigns the object associated with the given unsafe index to t and removes and unindexes it if it exists.
	 *
	 *  @param index	The unsafe index for which to find the associated object.
	 *  @param t		The destination to store the object.
	 *  @return			Whether the object was found and assigned.
	 */
	bool extract( UnsafeIndex index, T & t );

	/** @brief Reserves space for future objects to be added.
	 *
	 *  @param newCapacity	The amount of objects to reserve space for.
	 */
	void reserve( size_t newCapacity );

	/** @brief Returns whether the table contains an object associated with the given index.
	 *
	 *  @param index	The index to check for.
	 *  @return			Whether the table contains an object associated with the given index.
	 */
	bool contains( const Index & index ) const;

	/** @brief Returns whether the table contains an object associated with the given unsafe index.
	 *
	 *  @param index	The unsafe index to check for.
	 *  @return			Whether the table contains an object associated with the given unsafe index.
	 */
	bool contains( UnsafeIndex index ) const;

	/** @brief Removes and unindexes all objects.
	 */
	void clear();

	/** @brief Returns the number of objects currently stored.
	 */
	size_t size() const;

	/** @brief Returns true if the indexer is empty.
	 */
	bool empty() const;

	/** @brief Returns an iterator.
	 */
	Iterator getIterator();

	/** @brief Returns a const iterator.
	 */
	ConstIterator getConstIterator() const;

	/** @brief Returns the amount of memory used.
	 */
	size_t memory() const;

private:
	// index representing no entry
	static const size_t NO_INDEX = ~(1 << (sizeof( size_t )*8 - 1));

	// an entry with the correct data to ensure alignment
	struct AlignedEntry {
		T t; // the entry data

		// a combination of the free flag and the index of the next entry
		// the free flag occupies the highest bit.
		// The remaining bits represent the index of the next free object if not used,
		// or the index of the next object if used.
		size_t usedAndNextEntry;

		// if used, holds the index of the previous object
		size_t prevEntry;

		// unique ID of this entry
		Id id;
	};

	// used to hold entries in the indexer
	struct Entry {
		char t[offsetof( AlignedEntry, usedAndNextEntry )];	// space for an instance of T
		size_t usedAndNextEntry;							// the free bit and next entry
		size_t prevEntry;									// the previous entry
		Id id;												// unique ID

		bool used() const;
		size_t getNextFreeEntry() const;
		size_t getNextUsedEntry() const;
		void setNextUsedEntry( size_t nextUsedEntry );
		void setNextFreeEntry( size_t nextFreeEntry );
	};

	size_t capacity;		// the capacity of the underlying array
	size_t count;			// the current number of entries
	union {
		char * entries;					// the indexed entries
		AlignedEntry * alignedEntries;	// provides an easier way to access entry contents
	};
	size_t firstFreeEntry;	// the index of the first free object
	size_t firstUsedEntry;	// the index of the first used entry
	size_t freeEntries;		// the number of free entries
	Id idCounter;			// used to provide unique IDs
};

template <typename T> UniqueIndexer <T>::Index::Index() {
	// can be used to represent NO_INDEX
	index = NO_INDEX;
	id = 0;
}

template <typename T> UniqueIndexer <T>::UnsafeIndex UniqueIndexer <T>::getUnsafeIndex() const {
	return index;
}

template <typename T> UniqueIndexer <T>::Id UniqueIndexer <T>::getId() const {
	return id;
}

template <typename T> UniqueIndexer <T>::Index::Index( UnsafeIndex indexPart, Id idPart )
	: index( indexPart )
	, id( idPart ) {
}

template <typename T> bool UniqueIndexer <T>::Entry::used() const {
	return (usedAndNextEntry & (1 << (sizeof( usedAndNextEntry )*8 - 1))) != 0;
}

template <typename T> size_t UniqueIndexer <T>::Entry::getNextFreeEntry() const {
	return usedAndNextEntry;
}

template <typename T> size_t UniqueIndexer <T>::Entry::getNextUsedEntry() const {
	return usedAndNextEntry & ~(1 << (sizeof( usedAndNextEntry )*8 - 1));
}

template <typename T> void UniqueIndexer <T>::Entry::setNextUsedEntry( size_t nextUsedEntry ) {
	usedAndNextEntry = nextUsedEntry | (1 << (sizeof( usedAndNextEntry )*8 - 1));
}

template <typename T> void UniqueIndexer <T>::Entry::setNextFreeEntry( size_t nextFreeEntry ) {
	usedAndNextEntry = nextFreeEntry & ~(1 << (sizeof( usedAndNextEntry )*8 - 1));
}

template <typename T> UniqueIndexer <T>::Iterator::Iterator( UniqueIndexer <T> * b ) {
	base = b;
	currentIndex = NO_INDEX;
	nextIndex = base->firstUsedEntry;
}

template <typename T> bool UniqueIndexer <T>::Iterator::hasNext() const {
	return nextIndex != NO_INDEX;
}

template <typename T> bool UniqueIndexer <T>::Iterator::next() {
	if (nextIndex == NO_INDEX)
		return false;
	else {
		currentIndex = nextIndex;
		nextIndex = ((const Entry*)(base->alignedEntries + nextIndex))->getNextUsedEntry();
		return true;
	}
}

template <typename T> T & UniqueIndexer <T>::Iterator::get() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	Entry * e = (Entry*)(base->alignedEntries + currentIndex);
	return *((T*)e->t);
}

template <typename T> typename UniqueIndexer <T>::Index UniqueIndexer <T>::Iterator::getIndex() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	Entry * e = (Entry*)(base->alignedEntries + currentIndex);
	return Index( currentIndex, e->id );
}

template <typename T> void UniqueIndexer <T>::Iterator::remove() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	// removing simply "patches up" the hole in the linked list and won't mess up iteration
	base->remove( currentIndex );
	currentIndex = NO_INDEX;
}

template <typename T> UniqueIndexer <T>::ConstIterator::ConstIterator( const UniqueIndexer <T> * b ) {
	base = b;
	currentIndex = NO_INDEX;
	nextIndex = base->firstUsedEntry;
}

template <typename T> bool UniqueIndexer <T>::ConstIterator::hasNext() const {
	return index != NO_INDEX;
}

template <typename T> bool UniqueIndexer <T>::ConstIterator::next() {
	if (nextIndex == NO_INDEX)
		return false;
	else {
		currentIndex = nextIndex;
		nextIndex = ((const Entry*)(base->alignedEntries + nextIndex))->getNextUsedEntry();
		return true;
	}
}

template <typename T> const T & UniqueIndexer <T>::ConstIterator::get() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	const Entry * e = (const Entry*)(base->alignedEntries + currentIndex);
	return *((const T*)e->t);
}

template <typename T> typename UniqueIndexer <T>::Index UniqueIndexer <T>::ConstIterator::getIndex() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	const Entry * e = (const Entry*)(base->alignedEntries + currentIndex);
	return Index( currentIndex, e->id );
}

template <typename T> UniqueIndexer <T>::UniqueIndexer( size_t defaultCapacity ) {
	capacity = 0;
	count = 0;
	entries = NULL;
	freeEntries = 0;
	reserve( defaultCapacity );
	firstUsedEntry = NO_INDEX;
	firstFreeEntry = NO_INDEX; // not actually required but good practice
	idCounter = 0;
}

template <typename T> UniqueIndexer <T>::UniqueIndexer( const UniqueIndexer <T> & other ) {
	capacity = other.capacity;
	count = other.count;
	entries = new char[capacity * sizeof( AlignedEntry )];
	firstFreeEntry = other.firstFreeEntry;
	firstUsedEntry = other.firstUsedEntry;
	freeEntries = other.freeEntries;
	idCounter = other.idCounter;
	size_t offset = 0;
	size_t remaining = other.count + other.freeEntries;
	while (remaining > 0) {
		Entry * entry = new (entries + offset) Entry;
		const Entry * otherEntry = (const Entry*)(other.entries + offset);
		entry->usedAndNextEntry = otherEntry->usedAndNextEntry;
		entry->prevEntry = otherEntry->prevEntry;
		entry->id = otherEntry->id;
		if (entry->used())
			new (entry->t) T( *((const T*)otherEntry->t) );
		offset += sizeof( AlignedEntry );
		--remaining;
	}
}

template <typename T> UniqueIndexer <T>::~UniqueIndexer() {
	if (entries == NULL)
		return;

	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * entry = (Entry*)(entries + offset);
		if (entry->used())
			((T*)entry->t)->~T();
		entry->~Entry();
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	delete[] entries;
}

template <typename T> UniqueIndexer <T>::UniqueIndexer( UniqueIndexer <T> && other ) {
	capacity = other.capacity;
	count = other.count;
	entries = other.entries;
	other.entries = NULL;
	firstFreeEntry = other.firstFreeEntry;
	firstUsedEntry = other.firstUsedEntry;
	freeEntries = other.freeEntries;
	idCounter = other.idCounter;
}

template <typename T> UniqueIndexer <T> & UniqueIndexer <T>::operator=( const UniqueIndexer <T> & other ) {
	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * entry = (Entry*)(entries + offset);
		if (entry->used())
			((T*)entry->t)->~T();
		entry->~Entry();
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	if (capacity < other.capacity) {
		delete[] entries;
		capacity = other.capacity;
		entries = new char[capacity * sizeof( AlignedEntry )];
	}
	count = other.count;
	firstFreeEntry = other.firstFreeEntry;
	firstUsedEntry = other.firstUsedEntry;
	freeEntries = other.freeEntries;
	idCounter = other.idCounter;
	offset = 0;
	remaining = other.count + other.freeEntries;
	while (remaining > 0) {
		Entry * entry = new (entries + offset) Entry;
		const Entry * otherEntry = (const Entry*)(other.entries + offset);
		entry->usedAndNextEntry = otherEntry->usedAndNextEntry;
		entry->prevEntry = otherEntry->prevEntry;
		entry->id = otherEntry->id;
		if (entry->used())
			new (entry->t) T( *((const T*)otherEntry->t) );
		offset += sizeof( AlignedEntry );
		--remaining;
	}
	return *this;
}

template <typename T> UniqueIndexer <T> & UniqueIndexer <T>::operator=( UniqueIndexer <T> && other ) {
	std::swap( capacity, other.capacity );
	std::swap( count, other.count );
	std::swap( entries, other.entries );
	std::swap( firstFreeEntry, other.firstFreeEntry );
	std::swap( firstUsedEntry, other.firstUsedEntry );
	std::swap( freeEntries, other.freeEntries );
	std::swap( idCounter, other.idCounter );
	return *this;
}

template <typename T> typename UniqueIndexer <T>::Index UniqueIndexer <T>::add( const T & t ) {
	size_t index;
	Entry * entry;
	if (freeEntries == 0) {
		if (capacity < count+1)
			reserve( capacity * 2 + 1 );
		index = count;
		size_t offset = index * sizeof( AlignedEntry );
		entry = new (entries + offset) Entry;
	} else {
		index = firstFreeEntry;
		size_t offset = index * sizeof( AlignedEntry );
		entry = (Entry*)(entries + offset);
		firstFreeEntry = entry->getNextFreeEntry();
		--freeEntries;
	}
	entry->prevEntry = NO_INDEX;
	entry->setNextUsedEntry( firstUsedEntry );
	if (firstUsedEntry != NO_INDEX)
		((Entry*)(alignedEntries + firstUsedEntry))->prevEntry = index;
	entry->id = idCounter;
	firstUsedEntry = index;
	++count;
	++idCounter;
	new (entry->t) T( t );
	return Index( index, entry->id );
}

template <typename T> typename UniqueIndexer <T>::Index UniqueIndexer <T>::add( T && t ) {
	size_t index;
	Entry * entry;
	if (freeEntries == 0) {
		if (capacity < count+1)
			reserve( capacity * 2 + 1 );
		index = count;
		size_t offset = index * sizeof( AlignedEntry );
		entry = new (entries + offset) Entry;
	} else {
		index = firstFreeEntry;
		size_t offset = index * sizeof( AlignedEntry );
		entry = (Entry*)(entries + offset);
		firstFreeEntry = entry->getNextFreeEntry();
		--freeEntries;
	}
	entry->prevEntry = NO_INDEX;
	entry->setNextUsedEntry( firstUsedEntry );
	if (firstUsedEntry != NO_INDEX)
		((Entry*)(alignedEntries + firstUsedEntry))->prevEntry = index;
	entry->id = idCounter;
	firstUsedEntry = index;
	++count;
	++idCounter;
	new (entry->t) T( t );
	return Index( index, entry->id );
}

template <typename T> T & UniqueIndexer <T>::get( const Index & index ) {
	Entry * e = (Entry*)(alignedEntries + index.index);
	if (index.index < count + freeEntries && e->used() && index.id == e->id)
		return *((T*)e->t);
	else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> T & UniqueIndexer <T>::getUnsafe( UnsafeIndex index ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used())
		return *((T*)e->t);
	else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> const T & UniqueIndexer <T>::get( const Index & index ) const {
	const Entry * e = (Entry*)(alignedEntries + index.index);
	if (index.index < count + freeEntries && e->used() && index.id == e->id)
		return *((const T*)e->t);
	else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> const T & UniqueIndexer <T>::getUnsafe( UnsafeIndex index ) const {
	const Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used())
		return *((const T*)e->t);
	else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> bool UniqueIndexer <T>::get( const Index & index, T & t ) const {
	const Entry * e = (Entry*)(alignedEntries + index.index);
	if (index.index < count + freeEntries && e->used() && index.id == e->id) {
		t = *((const T*)e->t);
		return true;
	} else
		return false;
}

template <typename T> bool UniqueIndexer <T>::getUnsafe( UnsafeIndex index, T & t ) const {
	const Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		t = *((const T*)e->t);
		return true;
	} else
		return false;
}

template <typename T> bool UniqueIndexer <T>::remove( const Index & index ) {
	Entry * e = (Entry*)(alignedEntries + index.index);
	if (index.index < count + freeEntries && e->used() && index.id == e->id) {
		--count;
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return true;
	} else
		return false;
}

template <typename T> bool UniqueIndexer <T>::removeUnsafe( UnsafeIndex index ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		--count;
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return true;
	} else
		return false;
}

template <typename T> T UniqueIndexer <T>::extract( const Index & index ) {
	Entry * e = (Entry*)(alignedEntries + index.index);
	if (index.index < count + freeEntries && e->used() && index.id == e->id) {
		--count;
		T temp( *((T*)e->t) ); // I believe if we don't cast e->t to const, it has the potential to use the move constructor... maybe
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return temp;
	} else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> T UniqueIndexer <T>::extractUnsafe( UnsafeIndex index ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		--count;
		T temp( *((T*)e->t) ); // I believe if we don't cast e->t to const, it has the potential to use the move constructor... maybe
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return temp;
	} else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> bool UniqueIndexer <T>::extract( const Index & index, T & t ) {
	Entry * e = (Entry*)(alignedEntries + index.index);
	if (index.index < count + freeEntries && e->used() && index.id == e->id) {
		--count;
		t = std::move( *((T*)e->t) );
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return true;
	} else
		return false;
}

template <typename T> bool UniqueIndexer <T>::extractUnsafe( UnsafeIndex index, T & t ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		--count;
		t = std::move( *((T*)e->t) );
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return true;
	} else
		return false;
}

template <typename T> void UniqueIndexer <T>::reserve( size_t newCapacity ) {
	if (newCapacity <= capacity)
		return;

	char * newEntries = new char[newCapacity * sizeof( AlignedEntry )];

	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * newEntry = new (newEntries + offset) Entry;
		Entry * entry = (Entry*)(entries + offset);
		newEntry->usedAndNextEntry = entry->usedAndNextEntry;
		newEntry->prevEntry = entry->prevEntry;
		if (entry->used()) {
			new (newEntry->t) T( std::move( *((T*)entry->t) ) );
			((T*)entry->t)->~T();
		}
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	if (entries != NULL)
		delete[] entries;
	entries = newEntries;
	capacity = newCapacity;
}

template <typename T> bool UniqueIndexer <T>::contains( const Index & index ) const {
	Entry * e = (Entry*)(alignedEntries + index.index);
	return (index.index < count + freeEntries && e->used() && index.id == e->id);
}

template <typename T> bool UniqueIndexer <T>::containsUnsafe( UnsafeIndex index ) const {
	Entry * e = (Entry*)(alignedEntries + index);
	return (index < count + freeEntries && e->used());
}

template <typename T> void UniqueIndexer <T>::clear() {
	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * entry = (Entry*)(entries + offset);
		if (entry->used())
			((T*)entry->t)->~T();
		entry->~Entry();
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	count = 0;
	freeEntries = 0;
	firstUsedEntry = NO_INDEX;
}

template <typename T> size_t UniqueIndexer <T>::size() const {
	return count;
}

template <typename T> bool UniqueIndexer <T>::empty() const {
	return (count == 0);
}

template <typename T> typename UniqueIndexer <T>::Iterator UniqueIndexer <T>::getIterator() {
	return Iterator( this );
}

template <typename T> typename UniqueIndexer <T>::ConstIterator UniqueIndexer <T>::getConstIterator() const {
	return ConstIterator( this );
}

template <typename T> size_t UniqueIndexer <T>::memory() const {
	return sizeof( Entry ) * capacity;
}

#endif