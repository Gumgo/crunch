#if 0

#ifndef SYSROOMSTATE_DEFINED
#define SYSROOMSTATE_DEFINED

#include "Common.h"
#include "GameState.h"
#include "SysPhysicsManager.h"
#include "Resource.h"
#include "RoomData.h"
#include "SysCamera2D.h"
#include "SysTileLayer.h"
#include "SysBackgroundLayer.h"

DEFINE_GAME_STATE( SysRoomState, 0, 0 )

	const RoomData * roomData;

	SysCamera2D * camera;
	SysPhysicsManager * physicsManager;

	std::vector <SysBackgroundLayer*> backgroundLayers;
	std::vector <SysTileLayer*> tileLayers;

	void unloadRoom();

public:
	void onCreate();
	void onDestroy();

	void init( const RoomData * rmData, bool createActors = true );
	SysCamera2D * getCamera();
	PhysicsManager * getPhysicsManager();
};

#endif

#endif