#ifndef SYSRENDERERDEFERREDLIGHTING_DEFINED
#define SYSRENDERERDEFERREDLIGHTING_DEFINED

#include "Common.h"
#include "Actor.h"
#include "Renderer.h"
#include "ProgramResource.h"
#include "TextureResource.h"
#include "GeoSphere.h"
#include "GpuState.h"
#include "Cone.h"

DEFINE_ACTOR( SysRendererDeferredLighting, 0, 0 )
public:
	static SysRendererDeferredLighting & get();
	void onCreate();
	void onDestroy();

	void setupFramebuffer( uint width, uint height );	// set up the framebuffer - deletes old framebuffer

	static const byte SHADOW_MAP_PASS			= 25;	// the shadow map pass
	static const byte G_BUFFER_PASS				= 50;	// the g-buffer pass
	static const byte AMBIENT_OCCLUSION_PASS	= 60;	// the ambient occlusion pass
	static const byte LIGHTING_PASS				= 75;	// the lighting pass
	static const byte SHADING_PASS				= 100;	// the shading pass
	static const byte TRANSLUCENT_PASS			= 125;	// the translucent (forward) pass
	static const byte POST_PASS					= 150;	// the postprocessing pass
	void onDraw();										// registers the passes

	const GeoSphere & getGeoSphere() const;				// returns geosphere for omni lights
	const Cone & getCone() const;						// returns cone for spot lights
	GpuBufferReference getLightVolumeVertexBuffer();	// returns vertex buffer for light volume geometry
	GpuBufferReference getLightVolumeIndexBuffer();		// returns index buffer for light volume geometry
	size_t getGeoSphereVertexOffset() const;			// returns the offset in the buffer of the geosphere's vertices
	size_t getGeoSphereIndexOffset() const;				// returns the offset in the buffer of the geosphere's indices
	size_t getConeVertexOffset() const;					// returns the offset in the buffer of the cone's vertices
	size_t getConeIndexOffset() const;					// returns the offset in the buffer of the cone's indices

	GpuFramebufferReference getFramebuffer();			// returns the framebuffer

	GpuRenderbufferReference getDepthStencilBuffer();	// returns depth/stencil buffer

	GpuTextureReference getNormalSpecularBuffer();		// returns the normal/specular power buffer - readable after g-buffer pass
	GpuTextureReference getFaceNormalBuffer();			// returns the face normal buffer - readable after g-buffer pass
	GpuTextureReference getDepthBuffer();				// returns the depth buffer - readable after g-buffer pass
	GpuTextureReference getLightingBuffer();			// returns the lighting buffer - readable after lighting pass and before the post pass
	GpuTextureReference getAmbientOcclusionBuffer();	// returns the ambient occlusion buffer - readable after ambient occlusion pass and before shading pass
	GpuTextureReference getColorBuffer();				// returns the color buffer - readable after shading pass
	GpuTextureReference getColorBuffer2();				// returns the secondary color buffer - readable after shading pass
	void swapColorBuffers();							// swaps primary and secondary color buffers

private:
	static SysRendererDeferredLighting * sysRendererDeferredLighting;

	// Deferred Lighting Renderer
	// Framebuffer:
	//   depth:				DEPTH24_STENCIL8
	//   normalSpecBuffer:	RGB10_A2, RG = normal (spheremap transform), B = specular power, A = (unused)
	//   faceNormalBuffer:	RGB10_A2, RG = normal (spheremap transform), B = (unused), A = (unused)
	//   depthBuffer:		R32F, R = abs( eye.z )
	//   lightingBuffer:	RGBA16F - RGB = diffuse, A = spec luma
	//   colorBuffer:		RGBA16F - RGB = color, also used to store ambient occlusion during lighting pass
	// Pass 1: g-buffer pass
	//   Write to: depth, normalSpecBuffer, faceNormalBuffer, depthBuffer
	//   Render geometry
	//   Fill depth rendertarget with depth
	//   Write both sets of normals and specular power
	//   Write linear depth (eye z) to depthBuffer
	// Pass 2: lighting pass
	//   Read from: normalSpecBuffer, depthBuffer
	//   Write to: lightingBuffer
	//   Render lights
	//   Accumulate diffuse lighting in RGB of lightingBuffer
	//   Accumulate specular lighting approximation (luma of diffuse) in A of lightingBuffer
	// Pass 3: shading pass
	//   Read from: normalSpecBuffer, faceNormalBuffer, depthBuffer, lightingBuffer
	//   Write to: colorBuffer
	//   Compute shading, apply accumulated lighting
	//   diffuse light = lightingBuffer.rgb, specular light = lightingBuffer.rgb * (lightingBuffer.a / (lum(lightingBuffer.rgb) + epsilon))
	//   Output final color to colorBuffer
	// Pass 4: translucent pass
	//   Read from: depthBuffer
	//   Write to: colorBuffer
	//   Render any translucent objects, using depthBuffer as needed

	ProgramResourceReference
		ambientLightProgram,
		directionalLightProgram,
		pointLightSphereProgram,
		pointLightSphereShadowProgram,
		pointLightQuadProgram,
		pointLightQuadShadowProgram,
		pointLightGeometryProgram;

	GpuBufferReference vertexBuffer;
	GpuBufferReference indexBuffer;

	GpuFramebufferReference framebuffer;	// framebuffer
	GpuRenderbufferReference depthStencil;	// DEPTH24_STENCIL8
	GpuTextureReference normalSpecBuffer;	// RGB10_A2
	GpuTextureReference faceNormalBuffer;	// RGB10_A2
	GpuTextureReference depthBuffer;		// R32F
	GpuTextureReference lightingBuffer;		// RGBA16F
	GpuTextureReference colorBuffer;		// RGBA16F

	void beginFrame(); // called at very beginning of frame
	Renderer::Call beginFrameCall;
	Renderer::Key beginFrameKey;

	void beginGBufferPass(); // called at the beginning of g-buffer pass
	Renderer::Call beginGBufferPassCall;
	Renderer::Key beginGBufferPassKey;

	void beginLightingPass(); // called at the beginning of lighting pass
	Renderer::Call beginLightingPassCall;
	Renderer::Key beginLightingPassKey;

	void beginShadingPass(); // called at the beginning of shading pass
	Renderer::Call beginShadingPassCall;
	Renderer::Key beginShadingPassKey;

	void beginTranslucentPass(); // called at the beginning of translucent pass
	Renderer::Call beginTranslucentPassCall;
	Renderer::Key beginTranslucentPassKey;

	static const size_t GEOSPHERE_SUBDIVISIONS = 1;
	GeoSphere geoSphere;
	size_t geoSphereVertexOffset, geoSphereIndexOffset;
	static const size_t CONE_SEGMENTS = 8;
	Cone cone;
	size_t coneVertexOffset, coneIndexOffset;
};

#endif