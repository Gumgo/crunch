/** @file GameStateManager.h
 *  @brief Manages game states.
 */

#ifndef GAMESTATEMANAGER_DEFINED
#define GAMESTATEMANAGER_DEFINED

#include "Common.h"
#include "GameState.h"
#include "Indexer.h"
#include <map>
#include <set>

/** @brief Manages all the states of the game.
 */
class GameStateManager : private boost::noncopyable {
public:
	GameStateManager();		/**< The constructor.*/
	~GameStateManager();	/**< The destructor.*/

	void update();			/**< Updates the game states.*/
	void render();			/**< Renders the game states.*/

	void clear();			/**< Destroys all game states.*/

	/** @brief Create a game state.
	 *
	 *  The game state will be immediately created but onCreate() will not be called until
	 *  the beginning of the next frame. For this reason, don't attempt to use any data
	 *  or call any functions in the game state, as it may be in an uninitialized state.
	 *  However, it is safe to call general game state management functions, including
	 *  activate(), deactivate(), hide(), show(), and destroy().
	 */
	template <typename T> T * createGameState();

	/** @brief Create a game state, overriding update priority.
	 */
	template <typename T> T * createGameStateU( int updatePri );

	/** @brief Create a game state, overriding render priority.
	 */
	template <typename T> T * createGameStateR( int renderPri );

	/** @brief Create a game state, overriding update and render priority.
	 */
	template <typename T> T * createGameStateUR( int updatePri, int renderPri );

	/** @brief Returns the game state with the given index.
	 */
	GameState * getGameState( size_t index );

	/** @brief Destroys the game state with the given index.
	 */
	void destroyGameState( size_t index );

	/** @brief Activates the game state with the given index.
	 */
	void activateGameState( size_t index );

	/** @brief Deactivates the game state with the given index.
	 */
	void deactivateGameState( size_t index );

	/** @brief Hides the game state with the given index.
	 */
	void hideGameState( size_t index );

	/** @brief Shows the game state with the given index.
	 */
	void showGameState( size_t index );

	/** @brief Returns whether the game state with the given index is active.
	 */
	bool isGameStateActive( size_t index ) const;

	/** @brief Returns whether the game state with the given index is visible.
	 */
	bool isGameStateVisible( size_t index ) const;

private:
	// stores the game state with metadata
	struct GameStateData {
		GameState * gameState;									// the game state
		bool initialized;										// whether this game state has been initialized
		bool shouldBeActive;									// whether this game state should be active
		bool shouldBeVisible;									// whether this game state should be visible
		bool shouldBeDestroyed;									// wehther this game state should be destroyed
		std::multimap <int, size_t>::iterator updatePosition;	// location in the update list, or end if inactive
		std::multimap <int, size_t>::iterator renderPosition;	// location in the render list, or end if hidden
	};

	Indexer <GameStateData> gameStates;		// list of current game states
	std::multimap <int, size_t> updateList;	// ordered list for updating game states
	std::multimap <int, size_t> renderList;	// ordered list for rendering game states

	// a command to execute at the beginning of the frame
	struct PreFrameCommand {
		// the type of command.
		enum CommandType {
			CT_CREATE,
			CT_DESTROY,
			CT_ACTIVATE,
			CT_DEACTIVATE
		};
		CommandType commandType;
		size_t gameState; // game state to perform the command on
	};
	std::vector <PreFrameCommand> preFrameCommands; // list of commands to execute at the beginning/end of each frame

	void executePreFrameCommands();
};

template <typename T> T * GameStateManager::createGameState() {
	return createGameStateUR <T>( T::getDefaultUpdatePriority(), T::getDefaultRenderPriority() );
}

template <typename T> T * GameStateManager::createGameStateU( int updatePri ) {
	return createGameStateUR <T>( updatePri, T::getDefaultRenderPriority() );
}

template <typename T> T * GameStateManager::createGameStateR( int renderPri ) {
	return createGameStateUR <T>( T::getDefaultUpdatePriority(), renderPri );
}

template <typename T> T * GameStateManager::createGameStateUR( int updatePri, int renderPri ) {
	GameStateData gsd;
	gsd.gameState = NULL;
	gsd.updatePosition = updateList.end();
	gsd.renderPosition = renderList.end();
	gsd.initialized = false;
	gsd.shouldBeActive = true;
	gsd.shouldBeVisible = true;
	gsd.shouldBeDestroyed = false;

	size_t index = gameStates.add( gsd );
	T * gameState = new T( updatePri, renderPri, index );
	gameStates.get( index ).gameState = gameState;

	PreFrameCommand cmd;
	cmd.commandType = PreFrameCommand::CT_CREATE;
	cmd.gameState = index;
	preFrameCommands.push_back( cmd );

	return gameState;
}

#endif