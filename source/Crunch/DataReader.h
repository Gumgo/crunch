/** @file DataReader.h
 *  @brief Includes functionality for reading binary files.
 */

#ifndef DATAREADER_DEFINED
#define DATAREADER_DEFINED

#include "Common.h"
#include <fstream>
#include <vector>

/** @brief A class for reading binary data from a file.
 *
 *  DataReader loads a file into a buffer in memory and
 *  closes the file. The data can then be accessed using
 *  various read methods.
 */
class DataReader {
public:
	/** @brief Used to indicate the current status of the DataReader instance.
	 */
	enum Status {
		ST_SUCCESS,				/**< Indicates that no error has occurred.*/
		ST_FILE_NOT_FOUND,		/**< Indicates that the file was not found.*/
		ST_FILE_READ_FAILURE,	/**< Indicates that the file could not be read.*/
		ST_NOT_ENOUGH_DATA,		/**< Indicates that a request was made to read more data than was available.*/
		ST_END					/**< Indicates that the end of the data has been reached.*/
	};

	/** @brief Constructor for the DataReader class.
	 *
	 *  The constructor opens a file and puts the contents into a buffer.
	 *  Once this occurs, the file is closed.
	 *
	 *  @param fname	The name of the file to load.
	 */
	DataReader( const std::string & fname );

	/** @brief Returns the current status.
	 */
	Status status();

	/** @brief Returns the size in bytes of the data currently loaded.
	 */
	size_t size();

	/** @brief Returns the amount of bytes left to read.
	 */
	size_t bytesLeft();

	/** @brief Returns whether all bytes have been read.
	 */
	bool end();

	/** @brief Returns whether some error has occurred.
	 */
	bool fail();

	/** @brief Reads data from the data buffer.
	 *
	 *  This function copies data from the current location in the data
	 *  buffer to the provided location, then increments the data pointer.
	 *
	 *  @param dest		The location to copy the data to.
	 *  @param amount	The amount in bytes of data to read.
	 *  @return			Whether or not the data was successfully read.
	 */
	bool readData( void * dest, size_t amount );

	/** @brief Reads data from the data buffer.
	 *
	 *  This function copies data from the current location in the data
	 *  buffer to the provided location, then increments the data pointer.
	 *  The data is treated as type T.
	 *
	 *  @tparam T		The type of data to read.
	 *  @param dest		The location to copy the data to.
	 *  @param amount	The amount of elements of T to read.
	 *  @return			Whether or not the data was successfully read.
	 */
	template <typename T> bool readElements( T * dest, size_t amount );

private:
	size_t dataSize;			// the size of the data loaded
	size_t dataPointer;			// the current read offset in the data loaded
	std::vector <char> data;	// the block of data loaded
	Status st;					// the current status
};

template <typename T> bool DataReader::readElements( T * dest, size_t amount ) {
	return readData( dest, amount * sizeof( T ) );
}

#endif