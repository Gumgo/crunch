#if 0

#include "NetworkWriter.h"

NetworkWriter::NetworkWriter( size_t reserve ) {
	// 2 for the ID
	buffer.reserve( reserve + 2 );
	buffer.resize( 2 );
}

byte * NetworkWriter::inc( size_t bytes ) {
	byte * ret = &buffer[0] + buffer.size();
	buffer.resize( buffer.size() + bytes );
	return ret;
}

const byte * NetworkWriter::getData() const {
	return &buffer[0];
}

size_t NetworkWriter::getDataSize() const {
	return buffer.size();
}

void NetworkWriter::setNetworkId( uint networkId ) {
	*(ushort*)&buffer[0] = htons( networkId );
}

void NetworkWriter::writeChar( char c ) {
	*(char*)inc( 1 ) = c;
}

void NetworkWriter::writeByte( byte b ) {
	*(byte*)inc( 1 ) = b;
}

void NetworkWriter::writeShort( short s ) {
	*(ushort*)inc( 2 ) = htons( (ushort)s );
}

void NetworkWriter::writeUshort( ushort us ) {
	*(ushort*)inc( 2 ) = htons( us );
}

void NetworkWriter::writeInt( int i ) {
	*(uint*)inc( 4 ) = htonl( (uint)i );
}

void NetworkWriter::writeUint( uint ui ) {
	*(uint*)inc( 4 ) = htonl( ui );
}

void NetworkWriter::writeFloat( float f ) {
	*(float*)inc( 4 ) = f;
}

void NetworkWriter::writeDouble( double d ) {
	*(double*)inc( 8 ) = d;
}

void NetworkWriter::writeString( const std::string & s ) {
	if (s.size() > (size_t)std::numeric_limits <ushort>::max())
		throw std::runtime_error( "Attempted to write string to network exceeding 32,767 characters" );
	ushort size = (ushort)s.size();
	writeUshort( size );
	memcpy( inc( (size_t)size ), s.c_str(), (size_t)size );
}

void NetworkWriter::writeData( const void * src, size_t bytes ) {
	memcpy( inc( bytes ), src, bytes );
}

#endif