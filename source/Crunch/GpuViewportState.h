/** @file GpuViewportState.h
 *  @brief Encapsulates the state of the viewport on the GPU.
 */

#ifndef GPUVIEWPORTSTATE_DEFINED
#define GPUVIEWPORTSTATE_DEFINED

#include "Common.h"
#include "GpuObject.h"

/** @brief Encapsulates the state of the viewport on the GPU.
 */
class GpuViewportState : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuViewportState();

	/** @brief Sets the state of the GPU to the state represented by this object.
	 */
	void bind();

	/** @brief Sets the viewport position.
	 */
	void setViewportPosition( const Vector2i & position );

	/** @brief Sets the viewport size.
	 */
	void setViewportSize( const Vector2i & size );

	/** @brief Sets the viewport position and size.
	 */
	void setViewport( const Vector2i & position, const Vector2i & size );

	/** @brief Returns the viewport position.
	 */
	Vector2i getViewportPosition() const;

	/** @brief Returns the viewport size.
	 */
	Vector2i getViewportSize() const;

private:
	friend class GpuState;
	GpuViewportState( GpuState * state );

	Vector2i viewportPosition;	// viewport position
	Vector2i viewportSize;		// viewport size

	// compares changes against current state and apply differences
	void apply( GpuViewportState & currentState ) const;
	// sets values to the current OpenGL state
	void getCurrentState();
};

#endif