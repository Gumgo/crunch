#include "GameState.h"
#include "Context.h"

GameState::GameState( int updatePrior, int renderPrior, size_t idx ) {
	updateIterator = updateList.end();
	renderIterator = renderList.end();

	updatePriority = updatePrior;
	renderPriority = renderPrior;
	index = idx;
}

GameState::~GameState() {
	removeActors();
}

void GameState::removeActors() {
	// clear all actors
	// outer loop is in case the destructor of an actor creates an actor
	std::vector <size_t> indicesToRemove;
	while (!actors.empty()) {
		// first accumulate indices of remaining game states
		indicesToRemove.clear();
		size_t count = actors.size();
		size_t index = 0;
		while (count > 0) {
			if (actors.contains( index )) {
				indicesToRemove.push_back( index );
				--count;
			}
			++index;
		}

		// next delete them all
		for (size_t i = 0; i < indicesToRemove.size(); ++i) {
			if (actors.contains( indicesToRemove[i] ))
				destroyActor( indicesToRemove[i] );
		}
		while (!deadList.empty()) {
			safeDelete( deadList.back() );
			deadList.pop_back();
		}
		// we may have to loop again if destructors created more
	}
}

void GameState::update() {
	updateIterator = updateList.begin();
	while (updateIterator != updateList.end()) {
		size_t index = updateIterator->second;
		++updateIterator;

		Actor * actor = actors.get( index ).actor;
		actor->onUpdate();
	}

	// actually delete dead actors
	while (!deadList.empty()) {
		safeDelete( deadList.back() );
		deadList.pop_back();
	}
}

void GameState::render() {
	renderIterator = renderList.begin();
	while (renderIterator != renderList.end()) {
		size_t index = renderIterator->second;
		++renderIterator;

		Actor * actor = actors.get( index ).actor;
		actor->onDraw();
	}
}

Actor * GameState::getActor( size_t index ) {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to access actor with invalid index";
		return NULL;
	} else
		return actors.get( index ).actor;
}

void GameState::destroyActor( size_t index ) {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to destroy actor with invalid index";
		return;
	}

	ActorData & ad = actors.get( index );

	// call onDestroy
	// check for NULL (in case actor creation previously failed)
	if (ad.actor != NULL) {
		ad.actor->onDestroy();

		// remove from update and render lists if they are in there
		if (ad.updatePosition != updateList.end()) {
			if (updateIterator == ad.updatePosition)
				++updateIterator;
			updateList.erase( ad.updatePosition );
		}
		if (ad.renderPosition != renderList.end()) {
			if (renderIterator == ad.renderPosition)
				++renderIterator;
			renderList.erase( ad.renderPosition );
		}

		// don't immediately delete, add to dead list instead
		deadList.push_back( ad.actor );
		ad.actor = NULL;
	}

	actors.remove( index );
}

void GameState::activateActor( size_t index ) {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to activate actors with invalid index";
		return;
	}

	ActorData & ad = actors.get( index );

	if (ad.updatePosition == updateList.end()) {
		ad.updatePosition = updateList.insert( std::make_pair( ad.actor->getUpdatePriority(), index ) );
		ad.actor->onActivate();
	}
}

void GameState::deactivateActor( size_t index ) {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to deactivate actor with invalid index";
		return;
	}

	ActorData & ad = actors.get( index );

	if (ad.updatePosition != updateList.end()) {
		if (updateIterator == ad.updatePosition)
			++updateIterator;
		updateList.erase( ad.updatePosition );
		ad.updatePosition = updateList.end();
		ad.actor->onDeactivate();
	}
}

void GameState::hideActor( size_t index ) {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to deactivate actor with invalid index";
		return;
	}

	ActorData & ad = actors.get( index );

	if (ad.renderPosition != renderList.end()) {
		if (renderIterator == ad.renderPosition)
			++renderIterator;
		renderList.erase( ad.renderPosition );
		ad.renderPosition = renderList.end();
	}
}

void GameState::showActor( size_t index ) {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to show actor with invalid index";
		return;
	}

	ActorData & ad = actors.get( index );

	if (ad.renderPosition == renderList.end())
		ad.renderPosition = renderList.insert( std::make_pair( ad.actor->getRenderPriority(), index ) );
}

bool GameState::isActorActive( size_t index ) const {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to query activity of actor with invalid index";
		return false;
	}

	return actors.get( index ).updatePosition != updateList.end();
}

bool GameState::isActorVisible( size_t index ) const {
	if (!actors.contains( index )) {
		Log::warning() << "Attempted to query visibility of actor with invalid index";
		return false;
	}

	return actors.get( index ).renderPosition != renderList.end();
}

int GameState::getUpdatePriority() const {
	return updatePriority;
}

int GameState::getRenderPriority() const {
	return renderPriority;
}

size_t GameState::getIndex() const {
	return index;
}

bool GameState::isActive() const {
	return Context::get().gameStateManager.isGameStateActive( getIndex() );
}

bool GameState::isVisible() const {
	return Context::get().gameStateManager.isGameStateVisible( getIndex() );
}

void GameState::activate() {
	Context::get().gameStateManager.activateGameState( getIndex() );
}

void GameState::deactivate() {
	Context::get().gameStateManager.deactivateGameState( getIndex() );
}

void GameState::hide() {
	Context::get().gameStateManager.hideGameState( getIndex() );
}

void GameState::show() {
	Context::get().gameStateManager.showGameState( getIndex() );
}

void GameState::destroy() {
	Context::get().gameStateManager.destroyGameState( getIndex() );
}

void GameState::onCreate() {
}

void GameState::onUpdate() {
}

void GameState::onPostUpdate() {
}

void GameState::onDestroy() {
}

void GameState::onDraw() {
}

void GameState::onActivate() {
}

void GameState::onDeactivate() {
}