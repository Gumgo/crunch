#include "SysSkeleton3DAnimationTree.h"
#include "Context.h"

SysSkeleton3DAnimationTree::SysSkeleton3DAnimationTree() {
	initialized = false;
}

bool SysSkeleton3DAnimationTree::init(
	const Skeleton3DAnimationTreeResourceReference & animTree,
	const Skeleton3DResourceReference & skel ) {
	initialized = false;
	// store references
	animationTree = animTree;
	skeleton = skel;
	if (!animationTree || !skeleton)
		return false;
	// load the animations
	animations.clear();
	for (size_t i = 0; i < animationTree->getAnimationCount(); ++i) {
		animations.push_back( Context::get().resourceManager.acquire <Skeleton3DAnimationResource>(
			animationTree->getAnimation( i ), true ) );
		if (!animations.back())
			return false;
	}

	// reserve value indices for each node
	nodeValueIndices.reserve( animationTree->getNodeCount() );

	// assign indices
	requiredNodeValues = 0;
	for (size_t i = 0; i < animationTree->getNodeCount(); ++i) {
		nodeValueIndices.push_back( requiredNodeValues );
		switch (animationTree->getNode( i ).getType()) {
		case Skeleton3DAnimationTreeResource::Node::T_ANIMATION:
		case Skeleton3DAnimationTreeResource::Node::T_SELECT:
		case Skeleton3DAnimationTreeResource::Node::T_BLEND:
			++requiredNodeValues;
			break;
		case Skeleton3DAnimationTreeResource::Node::T_SEQUENCE:
			requiredNodeValues += animationTree->getNode( i ).getSequenceNode().getChildCount();
			break;
		default:
			assert( false );
		}
	}

	// pre-allocate space for storing intermediate transformations
	// index, depth
	typedef std::pair <size_t, size_t> IndexDepth;
	size_t maxDepth = 0;
	std::vector <IndexDepth> nodeStack;
	nodeStack.push_back( IndexDepth( 0, 0 ) );
	while (!nodeStack.empty()) {
		IndexDepth indexDepth = nodeStack.back();
		nodeStack.pop_back();

		const Skeleton3DAnimationTreeResource::Node & n = animationTree->getNode( indexDepth.first );
		switch (n.getType()) {
		case Skeleton3DAnimationTreeResource::Node::T_ANIMATION:
			// animation nodes increase depth by 1
			++indexDepth.second;
			break;
		case Skeleton3DAnimationTreeResource::Node::T_SELECT:
			// select nodes don't crease depth - just pass through
			for (size_t c = 0; c < n.getSelectNode().getChildCount(); ++c)
				nodeStack.push_back( std::make_pair( n.getSelectNode().getChildIndex( c ), indexDepth.second ) );
			break;
		case Skeleton3DAnimationTreeResource::Node::T_BLEND:
			// blend nodes increase depth by 2, as they need an intermediate buffer
			indexDepth.second += 2;
			for (size_t c = 0; c < n.getBlendNode().getChildCount(); ++c)
				nodeStack.push_back( std::make_pair( n.getBlendNode().getChildIndex( c ), indexDepth.second ) );
			break;
		case Skeleton3DAnimationTreeResource::Node::T_SEQUENCE:
			// sequence nodes increase depth by 1, as they combine with the current set of bones
			++indexDepth.second;
			for (size_t c = 0; c < n.getSequenceNode().getChildCount(); ++c)
				nodeStack.push_back( std::make_pair( n.getSequenceNode().getChildIndex( c ), indexDepth.second ) );
			break;
		default:
			assert( false );
		}

		maxDepth = std::max( maxDepth, indexDepth.second );
	}

	// reserve space for the maximum depth
	// we need twice the max depth due to combination nodes
	activeBonesBitfields.resize( skeleton->getBoneCount() * maxDepth );
	intermediateTransformationBuffer.resize( skeleton->getBoneCount() * maxDepth );
	initialized = true;
	return true;
}

bool SysSkeleton3DAnimationTree::isInitialized() const {
	return initialized;
}

const Skeleton3DAnimationTreeResourceReference & SysSkeleton3DAnimationTree::getAnimationTree() const {
	return animationTree;
}

const Skeleton3DAnimationResourceReference & SysSkeleton3DAnimationTree::getAnimation( size_t i ) const {
	return animations[i];
}

const Skeleton3DResourceReference & SysSkeleton3DAnimationTree::getSkeleton() const {
	return skeleton;
}

SysSkeleton3DAnimationTree::State::State() {
	tree = NULL;
}

const SysSkeleton3DAnimationTree * SysSkeleton3DAnimationTree::State::getAnimationTree() const {
	return tree;
}

SysSkeleton3DAnimationTree::State::State( const SysSkeleton3DAnimationTree & t )
	: tree( &t ) {

	nodeValues.reserve( tree->getRequiredNodeValues() );

	// assign values to each node
	for (size_t i = 0; i < tree->animationTree->getNodeCount(); ++i) {
		switch (tree->animationTree->getNode( i ).getType()) {
		case Skeleton3DAnimationTreeResource::Node::T_ANIMATION:
			{
				Value v;
				v.animationValue.time = 0.0f;
				v.animationValue.playSpeed = 0.0f;
				v.animationValue.loop = false;
				nodeValues.push_back( v );
			}
			break;
		case Skeleton3DAnimationTreeResource::Node::T_SELECT:
			{
				Value v;
				v.selectValue.index = 0;
				nodeValues.push_back( v );
			}
			break;
		case Skeleton3DAnimationTreeResource::Node::T_BLEND:
			{
				Value v;
				v.blendValue.weight = 0.0f;
				v.blendValue.targetWeight = 0.0f;
				v.blendValue.transitionSpeed = 0.0f;
				nodeValues.push_back( v );
			}
			break;
		case Skeleton3DAnimationTreeResource::Node::T_SEQUENCE:
			for (size_t c = 0; c < tree->animationTree->getNode( i ).getSequenceNode().getChildCount(); ++c) {
				// value for each child
				Value v;
				// initially each child is fully weighted
				v.sequenceValue.weight = 1.0f;
				v.sequenceValue.targetWeight = 1.0f;
				v.sequenceValue.transitionSpeed = 0.0f;
				nodeValues.push_back( v );
			}
			break;
		default:
			assert( false );
		}
	}

	// initially no nodes require updating
	valuesRequiringUpdates.resize( nodeValues.size(), false );
}

void SysSkeleton3DAnimationTree::State::checkNode( size_t index, Skeleton3DAnimationTreeResource::Node::Type type ) const {
	if (index > tree->animationTree->getNodeCount())
		throw std::runtime_error( "Node index out of bounds" );
	if (type != tree->animationTree->getNode( index ).getType())
		throw std::runtime_error( "Node type mismatch" );
}

void SysSkeleton3DAnimationTree::State::setAnimationNodeTime( size_t node, float time ) {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_ANIMATION );
	size_t nodeValueIndex = tree->getNodeValueIndex( node );
	nodeValues[nodeValueIndex].animationValue.time = time;
}

void SysSkeleton3DAnimationTree::State::setAnimationNodePlaySettings( size_t node, float playSpeed, bool loop ) {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_ANIMATION );
	size_t nodeValueIndex = tree->getNodeValueIndex( node );
	nodeValues[nodeValueIndex].animationValue.playSpeed = playSpeed;
	nodeValues[nodeValueIndex].animationValue.loop = loop;
	// update if play speed is nonzero
	setValueRequiresUpdating( nodeValueIndex, playSpeed != 0.0f );
}

float SysSkeleton3DAnimationTree::State::getAnimationNodeTime( size_t node ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_ANIMATION );
	return nodeValues[tree->getNodeValueIndex( node )].animationValue.time;
}

float SysSkeleton3DAnimationTree::State::getAnimationNodePlaySpeed( size_t node ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_ANIMATION );
	return nodeValues[tree->getNodeValueIndex( node )].animationValue.playSpeed;
}

bool SysSkeleton3DAnimationTree::State::getAnimationNodeLoop( size_t node ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_ANIMATION );
	return nodeValues[tree->getNodeValueIndex( node )].animationValue.loop;
}

void SysSkeleton3DAnimationTree::State::setSelectNodeSelection( size_t node, size_t index ) {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_SELECT );
	size_t nodeValueIndex = tree->getNodeValueIndex( node );
	nodeValues[nodeValueIndex].selectValue.index = index;
}

size_t SysSkeleton3DAnimationTree::State::getSelectNodeSelection( size_t node ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_SELECT );
	return nodeValues[tree->getNodeValueIndex( node )].selectValue.index;
}

void SysSkeleton3DAnimationTree::State::setBlendNodeWeight( size_t node, float weight ) {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_BLEND );
	size_t nodeValueIndex = tree->getNodeValueIndex( node );
	nodeValues[nodeValueIndex].blendValue.weight = weight;
	setValueRequiresUpdating( nodeValueIndex,
		nodeValues[nodeValueIndex].blendValue.targetWeight != weight &&
		nodeValues[nodeValueIndex].blendValue.transitionSpeed != 0.0f );
}

void SysSkeleton3DAnimationTree::State::setBlendNodeTransitionSettings( size_t node, float targetWeight, float transitionSpeed ) {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_BLEND );
	size_t nodeValueIndex = tree->getNodeValueIndex( node );
	nodeValues[nodeValueIndex].blendValue.targetWeight = targetWeight;
	nodeValues[nodeValueIndex].blendValue.transitionSpeed = transitionSpeed;
	setValueRequiresUpdating( nodeValueIndex,
		targetWeight != nodeValues[nodeValueIndex].blendValue.weight &&
		transitionSpeed != 0.0f );
}

float SysSkeleton3DAnimationTree::State::getBlendNodeWeight( size_t node ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_BLEND );
	return nodeValues[tree->getNodeValueIndex( node )].blendValue.weight;
}

float SysSkeleton3DAnimationTree::State::getBlendNodeTargetWeight( size_t node ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_BLEND );
	return nodeValues[tree->getNodeValueIndex( node )].blendValue.targetWeight;
}

float SysSkeleton3DAnimationTree::State::getBlendNodeTransitionSpeed( size_t node ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_BLEND );
	return nodeValues[tree->getNodeValueIndex( node )].blendValue.transitionSpeed;
}

void SysSkeleton3DAnimationTree::State::setSequenceNodeWeight( size_t node, size_t index, float weight ) {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_SEQUENCE );
	if (index >= tree->animationTree->getNode( node ).getSequenceNode().getChildCount())
		throw std::runtime_error( "Invalid sequence node index" );
	size_t nodeValueIndex = tree->getNodeValueIndex( node ) + index;
	nodeValues[nodeValueIndex].sequenceValue.weight = weight;
	setValueRequiresUpdating( nodeValueIndex,
		nodeValues[nodeValueIndex].sequenceValue.targetWeight != weight &&
		nodeValues[nodeValueIndex].sequenceValue.transitionSpeed != 0.0f );
}

void SysSkeleton3DAnimationTree::State::setSequenceNodeTransitionSettings( size_t node, size_t index, float targetWeight, float transitionSpeed ) {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_SEQUENCE );
	if (index >= tree->animationTree->getNode( node ).getSequenceNode().getChildCount())
		throw std::runtime_error( "Invalid sequence node index" );
	size_t nodeValueIndex = tree->getNodeValueIndex( node ) + index;
	nodeValues[nodeValueIndex].sequenceValue.targetWeight = targetWeight;
	nodeValues[nodeValueIndex].sequenceValue.transitionSpeed = transitionSpeed;
	setValueRequiresUpdating( nodeValueIndex,
		targetWeight != nodeValues[nodeValueIndex].sequenceValue.weight &&
		transitionSpeed != 0.0f );
}

float SysSkeleton3DAnimationTree::State::getSequenceNodeWeight( size_t node, size_t index ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_SEQUENCE );
	if (index >= tree->animationTree->getNode( node ).getSequenceNode().getChildCount())
		throw std::runtime_error( "Invalid sequence node index" );
	return nodeValues[tree->getNodeValueIndex( node ) + index].sequenceValue.weight;
}

float SysSkeleton3DAnimationTree::State::getSequenceNodeTargetWeight( size_t node, size_t index ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_SEQUENCE );
	if (index >= tree->animationTree->getNode( node ).getSequenceNode().getChildCount())
		throw std::runtime_error( "Invalid sequence node index" );
	return nodeValues[tree->getNodeValueIndex( node ) + index].sequenceValue.targetWeight;
}

float SysSkeleton3DAnimationTree::State::getSequenceNodeTransitionSpeed( size_t node, size_t index ) const {
	checkNode( node, Skeleton3DAnimationTreeResource::Node::T_SEQUENCE );
	if (index >= tree->animationTree->getNode( node ).getSequenceNode().getChildCount())
		throw std::runtime_error( "Invalid sequence node index" );
	return nodeValues[tree->getNodeValueIndex( node ) + index].sequenceValue.transitionSpeed;
}

size_t SysSkeleton3DAnimationTree::getNodeValueIndex( size_t nodeIndex ) const {
	return nodeValueIndices[nodeIndex];
}

size_t SysSkeleton3DAnimationTree::getRequiredNodeValues() const {
	return requiredNodeValues;
}

void SysSkeleton3DAnimationTree::State::evaluate( Matrix44af * poseMatrices ) const {
	tree->evaluate( *this, poseMatrices );
}

void SysSkeleton3DAnimationTree::State::update( float dt ) {
	size_t nodeCount = tree->getAnimationTree()->getNodeCount();
	for (size_t i = 0; i < nodeCount; ++i) {
		// get index of start value
		size_t valueStart = tree->getNodeValueIndex( i );
		// get index of end value
		size_t valueEnd = (i < nodeCount-1) ? tree->getNodeValueIndex( i+1 ) : nodeValues.size();

		for (size_t v = valueStart; v < valueEnd; ++v)
			updateValue( dt, i, v );
	}
}

void SysSkeleton3DAnimationTree::State::updateValue( float dt, size_t nodeIndex, size_t valueIndex ) {
	Value & v = nodeValues[valueIndex];
	const Skeleton3DAnimationTreeResource::Node & node = tree->getAnimationTree()->getNode( nodeIndex );

	switch (node.getType()) {
	case Skeleton3DAnimationTreeResource::Node::T_ANIMATION:
		{
			float animationLength = tree->getAnimation( node.getAnimationNode().getAnimationIndex() )->getLength();
			v.animationValue.time += v.animationValue.playSpeed * dt;
			if (v.animationValue.loop) {
				// if animation goes out of bounds, loop it using fmod
				v.animationValue.time = fmod( v.animationValue.time, animationLength );
				if (v.animationValue.time < 0.0f)
					v.animationValue.time += animationLength;
			} else
				// no loop - clamp time to animation bounds
				v.animationValue.time = clamp( v.animationValue.time, 0.0f, animationLength );
		}
		break;
	case Skeleton3DAnimationTreeResource::Node::T_SELECT:
		break; // no update required
	case Skeleton3DAnimationTreeResource::Node::T_BLEND:
		if (v.blendValue.weight < v.blendValue.targetWeight)
			v.blendValue.weight = std::min( v.blendValue.weight + v.blendValue.transitionSpeed * dt, v.blendValue.targetWeight );
		else if (v.blendValue.weight > v.blendValue.targetWeight)
			v.blendValue.weight = std::max( v.blendValue.weight - v.blendValue.transitionSpeed * dt, v.blendValue.targetWeight );
		// if we reach the target, we can stop updating
		if (v.blendValue.weight == v.blendValue.targetWeight)
			setValueRequiresUpdating( valueIndex, false );
		break;
	case Skeleton3DAnimationTreeResource::Node::T_SEQUENCE:
		if (v.sequenceValue.weight < v.sequenceValue.targetWeight)
			v.sequenceValue.weight = std::min( v.sequenceValue.weight + v.sequenceValue.transitionSpeed * dt, v.sequenceValue.targetWeight );
		else if (v.sequenceValue.weight > v.sequenceValue.targetWeight)
			v.sequenceValue.weight = std::max( v.sequenceValue.weight - v.sequenceValue.transitionSpeed * dt, v.sequenceValue.targetWeight );
		// if we reach the target, we can stop updating
		if (v.sequenceValue.weight == v.sequenceValue.targetWeight)
			setValueRequiresUpdating( valueIndex, false );
		break;
	default:
		assert( false );
	}
}

void SysSkeleton3DAnimationTree::State::setValueRequiresUpdating( size_t valueIndex, bool r ) {
	valuesRequiringUpdates[valueIndex] = r;
}

SysSkeleton3DAnimationTree::State SysSkeleton3DAnimationTree::getState() const {
	return State( *this );
}

void SysSkeleton3DAnimationTree::evaluate( const State & state, Matrix44af * poseMatrices ) const {
	for (size_t i = 0; i < skeleton->getBoneCount(); ++i)
		// initially current pose points to itself ("undefined" for invalid animations, but won't crash)
		intermediateTransformationBuffer[i].lastActiveTransformation = &intermediateTransformationBuffer[i];
	evaluate( state, 0, &intermediateTransformationBuffer[0], 0, &intermediateTransformationBuffer[0] );

	setPoseMatrices( poseMatrices );
}

void SysSkeleton3DAnimationTree::evaluate( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const {
	// clear this node's bone index
	std::fill( activeBonesBitfields.begin() + bitfieldStart, activeBonesBitfields.begin() + bitfieldStart + skeleton->getBoneCount(), false );

	const Skeleton3DAnimationTreeResource::Node & n = animationTree->getNode( node );
	switch (n.getType()) {
	case Skeleton3DAnimationTreeResource::Node::T_ANIMATION:
		evaluateAnimationNode( state, node, buffer, bitfieldStart, currentPose );
		break;
	case Skeleton3DAnimationTreeResource::Node::T_SELECT:
		evaluateSelectNode( state, node, buffer, bitfieldStart, currentPose );
		break;
	case Skeleton3DAnimationTreeResource::Node::T_BLEND:
		evaluateBlendNode( state, node, buffer, bitfieldStart, currentPose );
		break;
	case Skeleton3DAnimationTreeResource::Node::T_SEQUENCE:
		evaluateSequenceNode( state, node, buffer, bitfieldStart, currentPose );
		break;
	default:
		assert( false );
	}
}

static std::pair <size_t, size_t> getSurroundingKeyframes( const Skeleton3DAnimationResource::AnimatedBoneData & bone, float time ) {
	struct KeyframeComparator {
		const Skeleton3DAnimationResource::AnimatedBoneData & bone;
		float refTime;

		int operator()( size_t index ) const {
			float t = bone.keyframes[index].time;
			return (t > refTime) - (t < refTime);
		}

		KeyframeComparator( const Skeleton3DAnimationResource::AnimatedBoneData & b, float rTime )
			: bone( b )
			, refTime( rTime ) {
		}
	};

	std::pair <size_t, size_t> surroundingKeyframes;
	size_t index;
	if (binarySearch( 0, bone.keyframes.size(), index, KeyframeComparator( bone, time ) ))
		surroundingKeyframes.first = surroundingKeyframes.second = index;
	else {
		if (index == 0)
			surroundingKeyframes.first = surroundingKeyframes.second = 0;
		else if (index == bone.keyframes.size())
			surroundingKeyframes.first = surroundingKeyframes.second = bone.keyframes.size() - 1;
		else {
			surroundingKeyframes.first = index - 1;
			surroundingKeyframes.second = index;
		}
	}

	return surroundingKeyframes;
}

static float getKeyframeRatio( const Skeleton3DAnimationResource::AnimatedBoneData & bone, const std::pair <size_t, size_t> keyframes, float time ) {
	return (keyframes.first == keyframes.second) ? 1.0f :
		(time - bone.keyframes[keyframes.first].time) / (bone.keyframes[keyframes.second].time - bone.keyframes[keyframes.first].time);
}

void SysSkeleton3DAnimationTree::evaluateAnimationNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const {
	const Skeleton3DAnimationResource & animation =
		*animations[animationTree->getNode( node ).getAnimationNode().getAnimationIndex()];
	// set the bone bits
	for (size_t i = 0; i < animation.getAnimatedBoneCount(); ++i)
		activeBonesBitfields[bitfieldStart + animation.getAnimatedBoneData( i ).boneId] = true;

	float time = state.getAnimationNodeTime( node );

	// find the fade ratio
	float fadeInRatio = animation.getFadeInTime() == 0.0f ? 1.0f :
		clamp( time / animation.getFadeInTime(), 0.0f, 1.0f );
	float fadeOutRatio = animation.getFadeOutTime() == 0.0f ? 1.0f :
		clamp( (animation.getLength() - time) / animation.getFadeOutTime(), 0.0f, 1.0f );
	float fadeRatio = std::min( fadeInRatio, fadeOutRatio );

	// compute each bone
	if (fadeRatio == 0.0f) {
		// take directly from the previous level
		for (size_t i = 0; i < animation.getAnimatedBoneCount(); ++i) {
			const Skeleton3DAnimationResource::AnimatedBoneData & bone = animation.getAnimatedBoneData( i );

			buffer[bone.boneId].translation = currentPose[bone.boneId].lastActiveTransformation->translation;
			buffer[bone.boneId].rotation = currentPose[bone.boneId].lastActiveTransformation->rotation;
		}
	} else if (fadeRatio == 1.0f) {
		// apply the animation fully
		for (size_t i = 0; i < animation.getAnimatedBoneCount(); ++i) {
			const Skeleton3DAnimationResource::AnimatedBoneData & bone = animation.getAnimatedBoneData( i );

			std::pair <size_t, size_t> surrKf = getSurroundingKeyframes( bone, time );
			float ratio = getKeyframeRatio( bone, surrKf, time );

			const Skeleton3DAnimationResource::KeyframeData & kf0 = bone.keyframes[surrKf.first];
			const Skeleton3DAnimationResource::KeyframeData & kf1 = bone.keyframes[surrKf.second];

			// write the animation result to the destination
			buffer[bone.boneId].translation = lerp( kf0.translation, kf1.translation, ratio );
			buffer[bone.boneId].rotation = nlerp( kf0.rotation, kf1.rotation, ratio );
		}
	} else {
		// apply the animation and blend with previous level
		for (size_t i = 0; i < animation.getAnimatedBoneCount(); ++i) {
			const Skeleton3DAnimationResource::AnimatedBoneData & bone = animation.getAnimatedBoneData( i );

			std::pair <size_t, size_t> surrKf = getSurroundingKeyframes( bone, time );
			float ratio = getKeyframeRatio( bone, surrKf, time );

			const Skeleton3DAnimationResource::KeyframeData & kf0 = bone.keyframes[surrKf.first];
			const Skeleton3DAnimationResource::KeyframeData & kf1 = bone.keyframes[surrKf.second];

			Vector3f translation = lerp( kf0.translation, kf1.translation, ratio );
			Quaternionf rotation = nlerp( kf0.rotation, kf1.rotation, ratio );

			// write the blended animation result to the destination
			buffer[bone.boneId].translation = lerp( currentPose[bone.boneId].lastActiveTransformation->translation, translation, fadeRatio );
			buffer[bone.boneId].rotation = nlerp( currentPose[bone.boneId].lastActiveTransformation->rotation, rotation, fadeRatio );
		}
	}
}

void SysSkeleton3DAnimationTree::evaluateSelectNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const {
	size_t selectedNode = animationTree->getNode( node ).getSelectNode().getChildIndex(
		state.getSelectNodeSelection( node ) );

	// simply "pass through" based on the index
	// don't even bother adjusting the buffer pointer or bitfield
	// just have the next node write directly to the current location
	evaluate( state, selectedNode, buffer, bitfieldStart, currentPose );
}

void SysSkeleton3DAnimationTree::evaluateBlendNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const {
	const Skeleton3DAnimationTreeResource::BlendNode & blendNode = animationTree->getNode( node ).getBlendNode();
	float weight = state.getBlendNodeWeight( node );
	if (blendNode.getWrap())
		weight = clamp( weight, 0.0f, (float)(blendNode.getChildCount() - 1) );
	else {
		float maxWeight = (float)blendNode.getChildCount();
		weight = clamp( weight, 0.0f, maxWeight );
		if (weight == maxWeight)
			weight = 0.0f; // wrap
	}
	// break weight up into its integer part (the child index) and the weight from 0 to 1
	float intPart;
	weight = modf( weight, &intPart );
	size_t leftChildIndex = (size_t)intPart;
	size_t rightChildIndex = (leftChildIndex == blendNode.getChildCount()-1) ? 0 : leftChildIndex + 1;

	// optimize for cases of 0 weight
	if (weight == 0.0f) {
		// pass through
		size_t child = animationTree->getNode( node ).getBlendNode().getChildIndex( leftChildIndex );
		evaluate( state, child, buffer, bitfieldStart, currentPose );
	} else {
		size_t leftChild = animationTree->getNode( node ).getBlendNode().getChildIndex( leftChildIndex );
		size_t rightChild = animationTree->getNode( node ).getBlendNode().getChildIndex( rightChildIndex );

		size_t leftBitfieldStart = bitfieldStart + skeleton->getBoneCount();
		size_t rightBitfieldStart = leftBitfieldStart + skeleton->getBoneCount();
		Transformation * leftBuffer = buffer + skeleton->getBoneCount();
		Transformation * rightBuffer = leftBuffer + skeleton->getBoneCount();

		// evaluate left and right nodes into each of their buffers
		evaluate( state, leftChild, leftBuffer, leftBitfieldStart, currentPose );
		evaluate( state, rightChild, rightBuffer, rightBitfieldStart, currentPose );

		// blend between the two
		for (size_t i = 0; i < skeleton->getBoneCount(); ++i) {
			bool leftActive = activeBonesBitfields[leftBitfieldStart + i];
			bool rightActive = activeBonesBitfields[rightBitfieldStart + i];

			if (leftActive && !rightActive) {
				// only the left is active - blend between left and the bind pose
				buffer[i].translation = lerp( leftBuffer[i].translation, currentPose[i].lastActiveTransformation->translation, weight );
				buffer[i].rotation = nlerp( leftBuffer[i].rotation, currentPose[i].lastActiveTransformation->rotation, weight );
			} else if (!leftActive && rightActive) {
				// only the right is active - blend between right and the bind pose
				buffer[i].translation = lerp( currentPose[i].lastActiveTransformation->translation, rightBuffer[i].translation, weight );
				buffer[i].rotation = nlerp( currentPose[i].lastActiveTransformation->rotation, rightBuffer[i].rotation, weight );
			} else {
				// blend between the two transformations
				buffer[i].translation = lerp( leftBuffer[i].translation, rightBuffer[i].translation, weight );
				buffer[i].rotation = nlerp( leftBuffer[i].rotation, rightBuffer[i].rotation, weight );
			}

			// OR the set of active bones
			activeBonesBitfields[bitfieldStart + i] = leftActive || rightActive;
		}
	}
}

void SysSkeleton3DAnimationTree::evaluateSequenceNode( const State & state, size_t node, Transformation * buffer, size_t bitfieldStart, const Transformation * currentPose ) const {
	// first update the current post pointers
	for (size_t i = 0; i < skeleton->getBoneCount(); ++i)
		buffer[i].lastActiveTransformation = currentPose[i].lastActiveTransformation;

	// loop over each node
	const Skeleton3DAnimationTreeResource::SequenceNode & sn = animationTree->getNode( node ).getSequenceNode();
	size_t children = sn.getChildCount();
	for (size_t i = 0; i < children; ++i) {
		size_t child = sn.getChildIndex( i );

		size_t childBitfieldStart = bitfieldStart + skeleton->getBoneCount();
		Transformation * childBuffer = buffer + skeleton->getBoneCount();

		// when we evaluate nodes, we pass them the buffer for this node as the current pose
		// this is because if a bone attempts to blend with a non-active bone, it blends with whatever the current pose is
		// this is a sequence node, so this node defines the current pose

		float weight = state.getSequenceNodeWeight( node, i );
		if (weight == 0.0f)
			// don't even apply it
			continue;
		else {
			// evaluate child node
			evaluate( state, child, childBuffer, childBitfieldStart, buffer );

			if (weight == 1.0f) {
				// if weight is 1, don't bother blending, just set
				for (size_t i = 0; i < skeleton->getBoneCount(); ++i) {
					bool childActive = activeBonesBitfields[childBitfieldStart + i];

					if (childActive) {
						// copy transformation
						buffer[i].translation = childBuffer[i].translation;
						buffer[i].rotation = childBuffer[i].rotation;
						// this bone is active, so point the current pose to itself
						buffer[i].lastActiveTransformation = &buffer[i];
						activeBonesBitfields[bitfieldStart + i] = true;
					}
				}
			} else {
				// blend between current transformations and child transformations
				for (size_t i = 0; i < skeleton->getBoneCount(); ++i) {
					bool active = activeBonesBitfields[bitfieldStart + i];
					bool childActive = activeBonesBitfields[childBitfieldStart + i];

					// if the child bone isn't active, don't even blend
					// otherwise, blend with the current pose
					if (childActive) {
						// blend with the current transformation
						buffer[i].translation = lerp( buffer[i].lastActiveTransformation->translation, childBuffer[i].translation, weight );
						buffer[i].rotation = nlerp( buffer[i].lastActiveTransformation->rotation, childBuffer[i].rotation, weight );
						// this bone is active, so point the current pose to itself
						buffer[i].lastActiveTransformation = &buffer[i];
						activeBonesBitfields[bitfieldStart + i] = true;
					}
				}
			}
		}
	}
}

void SysSkeleton3DAnimationTree::setPoseMatrices( Matrix44af * matrices ) const {
	for (size_t i = 0; i < skeleton->getRootBoneCount(); ++i) {
		size_t boneId = skeleton->getRootBoneIndex( i );
		matrices[boneId] = Matrix44af(
			intermediateTransformationBuffer[boneId].rotation,
			intermediateTransformationBuffer[boneId].translation );
		for (size_t c = 0; c < skeleton->getBoneData( boneId ).children.size(); ++c)
			setPoseMatrices( matrices, skeleton->getBoneData( boneId ).children[c], boneId );
	}
}

void SysSkeleton3DAnimationTree::setPoseMatrices( Matrix44af * matrices, size_t boneId, size_t parentId ) const {
	// multiply with parent matrix to transform into pose space
	matrices[boneId] = matrices[parentId] * Matrix44af(
		intermediateTransformationBuffer[boneId].rotation,
		intermediateTransformationBuffer[boneId].translation );
	for (size_t c = 0; c < skeleton->getBoneData( boneId ).children.size(); ++c)
		setPoseMatrices( matrices, skeleton->getBoneData( boneId ).children[c], boneId );
}