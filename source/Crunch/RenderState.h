#if 0

/** @file RenderState.h
 *  @brief Keeps track of the current state of the renderer.
 */

#ifndef RENDERSTATE_DEFINED
#define RENDERSTATE_DEFINED

#include "Common.h"
#include "OpenGL.h"
#include "RenderEnums.h"
#include "ProgramResource.h"
#include "GpuTexture.h"
#include "GpuBuffer.h"
#include "GpuRenderbuffer.h"
#include "Window.h"

/** @brief Keeps track of the current state of the renderer.
 */
class RenderState : private boost::noncopyable {
	size_t frames;			// the total number of frames rendered
	const Window & window;	// the window

public:
	/** @brief Keeps track of the state of the shaders.
	 */
	class ShaderState {
	public:
		ShaderState();									/**< The constructor.*/
		void init();									/**< Initializes vertex attribute state.*/
		void term();									/**< Terminates vertex attribute state.*/
		void reset();									/**< Resets the shader state.*/
		void endFrame();								/**< Disables vertex attributes.*/
		void disableAllAttributes();					/**< Disables vertex attributes and keeps track of this.*/
		void setProgram( const ProgramResource * p );	/**< Sets the current program and enables all attributes.*/

		// A note about matrices:
		// Crunch stores matrices in row-major format, and offers a 4x4 affine matrix class.
		// This is represented internally by having the _bottom row_ set to [0, 0, 0, 1].
		// Since there is no equivalent matrix class with the _right column_ set to [0, 0, 0, 1],
		// all transformation compositions must be performed the following way:
		// A * B * p, equivalent to A(B(p))
		// i.e. "the order is from right to left".
		// In GLSL, matrices are stored in column-major format. Ideally this shouldn't make a difference,
		// however due to some technicalities, it does. Specifically, in GLSL, a 4x3 matrix (4 columns by 3 rows)
		// actually takes up 4x4 = 16 floats, rather than 4x3 = 12, because due to the column major format,
		// columns are always stored as a 4-component vector. With a 3x4 matrix (3 columns by 4 rows), this is
		// not an issue, and so we only use the (desired) amount of space, 12 floats.
		// For this reason, in GLSL, 3x4 matrices should be used. However, treating a 3x4 matrix as an affine
		// 4x4 matrix means that the only place the implicit [0, 0, 0, 1] row can go is in the right column.
		// Therefore, in GLSL, transformations must be performed in the reverse order:
		// p * B * A, equivalent to A(B(p))
		// i.e. "left to right".
		// Fortunately, an explicit transpose does not need to be performed when transferring
		// matrices from Crunch to GLSL (4x4 to 4x4, affine 4x4 to 3x4, etc.)  Because Crunch stores in row-major
		// format and GLSL interprets in column-major format, an implicit transpose is automatically performed.
		// The same applies to all other matrices (affine 3x3 should be treated as 2x3 in GLSL, etc.)

		/** @brief Sets the uniform with the given index and type to the given value.
		 *
		 *  @note Boolean values should be passed as ints.
		 */
		void setUniform( size_t index, GLenum type, const void * value );

		/** @brief Sets the uniform with the given name and type to the given value.
		 *
		 *  @note Boolean values should be passed as ints.
		 */
		void setUniform( const std::string & name, GLenum type, const void * value );

		/** @brief Sets the uniform array with the given index and type to the given array of length count.
		 *
		 *  @note Boolean values should be passed as ints.
		 */
		void setUniformArray( size_t index, GLenum type, const void * value, size_t count );

		/** @brief Sets the uniform array with the given name and type to the given array of length count.
		 *
		 *  @note Boolean values should be passed as ints.
		 */
		void setUniformArray( const std::string & name, GLenum type, const void * value, size_t count );

		/** @brief Enables a vertex attribute.
		 *
		 *  If 0 is specified for dataType, the data type is assumed to be
		 *  the attribute's base type (e.g. GL_FLOAT for vec3). Setting a
		 *  different base type will allow for data conversion (e.g. passing
		 *  GL_UNSIGNED_BYTE to vec3).
		 */
		void enableVertexAttribute( size_t index, size_t stride, size_t bufOffset, size_t attribOffset, GLenum dataType = 0, bool normalized = false );

		/** @brief Enables a vertex attribute.
		 *
		 *  If 0 is specified for dataType, the data type is assumed to be
		 *  the attribute's base type (e.g. GL_FLOAT for vec3). Setting a
		 *  different base type will allow for data conversion (e.g. passing
		 *  GL_UNSIGNED_BYTE to vec3).
		 */
		void enableVertexAttribute( const std::string & name, size_t stride, size_t bufOffset, size_t attribOffset, GLenum dataType = 0, bool normalized = false );

		/** @brief Disables a vertex attribute.
		 */
		void disableVertexAttribute( size_t index );

		/** @brief Disables a vertex attribute.
		 */
		void disableVertexAttribute( const std::string & name );

		/** @brief Returns the number of remaining uniforms.
		 */
		size_t getRemainingUniforms() const;

		/** @brief Returns the number of remaining attributes.
		 */
		size_t getRemainingAttributes() const;

	private:
		const ProgramResource * program; // the current program

		// copy of the value of a uniform on the GPU to check against before setting
		struct UniformCache {
			union {
				int boolValue[4];		// cached bool
				int intValue[4];		// cached int
				uint uintValue[4];		// cached uint
				float floatValue[16];	// cached float
				int samplerValue;		// cached sampler
			};
			bool valid; // whether this cache is valid
		};

		// the number of uniforms that still need to be set
		size_t uniformsRemaining;

		// caches uniform values
		std::vector <UniformCache> uniformCache;

		// holds the current state of a vertex attribute
		struct AttributeCache {
			size_t stride;		// the stride
			size_t offset;		// the offset into the buffer
			GLenum dataType;	// the type of data being provided
			bool normalized;	// whether to normalize the data
			bool enabled;		// whether the attribute is enabled
		};

		// the number of attributes that still need to be set
		size_t attributesRemaining;

		// caches attributes
		std::vector <AttributeCache> attributeCache;

		// required for OpenGL to hold the state of the vertex attributes
		GLuint vertexArrayObject;
	} shaderState; /**< An instance of the ShaderState.*/

	/** @brief Keeps track of the currently bound textures.
	 */
	class TextureState {
	public:
		static const size_t TEXTURE_UNITS = 8;		/**< The number of texture units.*/

		void reset();										/**< Resets the texture state.*/
		void bind( int textureUnit, const GpuTexture * t );	/**< Binds the given texture to the given unit.*/

	private:
		// the binding in each texture unit
		const GpuTexture * textures[TEXTURE_UNITS];
	} textureState;	/**< An instance of the TextureState.*/

	/** @brief Keeps track of the currently bound vertex and index buffers.
	 */
	class GpuBufferState {
	public:
		GpuBufferState( ShaderState & ss );		/**< The constructor.*/
		void reset();							/**< Resets the GPU buffer state.*/
		void bind( const GpuBuffer * buffer );	/**< Binds a vertex or an index buffer.*/
		void unbindVertexBuffer();				/**< Unbinds the vertex buffer.*/
		void unbindIndexBuffer();				/**< Unbinds the index buffer.*/

	private:
		ShaderState & shaderState;		// reference to the shader state
		const GpuBuffer * vertexBuffer;	// the currently bound vertex buffer
		const GpuBuffer * indexBuffer;	// the currently bound index buffer
	} gpuBufferState; /**< An instance of the GpuBufferState.*/

	/** @brief Used to keep track of OpenGL's current blending mode.
	 */
	class BlendState {
	public:
		void reset();									/**< Resets the blending mode.*/
		void setBlendMode( BlendMode bm );				/**< Sets the blending mode to bm.*/
		void setBlendMode( GLenum src, GLenum dst );	/**< Sets the blending mode based on src and dst.*/

	private:
		bool blendEnabled;	// whether blending is enabled
		GLenum blendFunc1;	// OpenGL's first blending function
		GLenum blendFunc2;	// OpenGL's second blending function
	} blendState; /**< An instance of the BlendState.*/

	/** @brief Used to keep track of the culling mode.
	 */
	class CullingState {
	public:
		void reset();						/**< Resets the culling mode.*/
		void enableCulling();				/**< Enables culling.*/
		void disableCulling();				/**< Disables culling.*/
		void setCullMode( GLenum mode );	/**< Sets the cull mode.*/

	private:
		bool cullingEnabled;	// whether culling is enabled
		GLenum cullMode;		// the culling mode
	} cullingState;	/**< An instance of the CullingState.*/

	/** @brief Used to keep track of depth/stencil buffer modes.
	 */
	class DepthStencilState {
	public:
		void reset();	/**< Resets the depth/stencil buffer modes.*/

		void enableDepthTest();					/**< Enables the depth test.*/
		void disableDepthTest();				/**< Disables the depth test.*/
		void setDepthTestFunc( GLenum func );	/**< Sets the depth test function.*/
		void enableDepthWrite();				/**< Enables depth writing.*/
		void disableDepthWrite();				/**< Disables depth writing.*/

		void enableStencilTest();											/**< Enables the stencil test.*/
		void disableStencilTest();											/**< Disables the stencil test.*/
		void setStencilTestFunc( GLenum func, int ref, uint mask );			/**< Sets the stencil test function.*/
		void setStencilTestOp( GLenum sFail, GLenum dFail, GLenum dPass );	/**< Sets the stencil test operation.*/

	private:
		bool depthTestEnabled;		// whether the depth test is enabled
		GLenum depthTestFunc;		// the depth test function
		bool depthWriteEnabled;		// whether depth writing is enabled

		bool stencilTestEnabled;	// whether the stencil test is enabled
		GLenum stencilTestFunc;		// the stencil test function
		int stencilTestRef;			// the stencil test reference value
		uint stencilTestMask;		// the stencil test mask value
		GLenum sFailOp;				// the stencil fail operation
		GLenum dFailOp;				// the depth fail operation
		GLenum dPassOp;				// the depth pass operation
	} depthStencilState;	/**< An instance of the DepthStencilState.*/

	/** @brief Used to keep track of the current render target.
	 */
	class RenderTargetState {
	public:
		static const size_t MAX_COLOR_ATTACHMENTS = 4;	/**< The maximum number of render targets.*/

		/** @brief Stores a render target.
		 */
		struct RenderTarget {
			bool isGpuTexture;	/**< Whether the render target is a texture (versus a renderbuffer).*/
			union {
				void * object;					/**< A generic pointer.*/
				GpuTexture * texture;			/**< The texture pointer.*/
				GpuRenderbuffer * renderbuffer;	/**< The renderbuffer pointer.*/
			};
			bool singleLayer;
			size_t layer;

			bool operator==( const RenderTarget & o ) const;
			bool operator!=( const RenderTarget & o ) const;
		};

		RenderTargetState();	/**< The constructor.*/
		~RenderTargetState();	/**< The destructor.*/
		void init();			/**< Initializes the framebuffer.*/
		void term();			/**< Terminates the framebuffer.*/
		void reset();			/**< Ensures the framebuffer is bound but does not alter bound render targets.*/
		void endFrame();		/**< Draws the screen target to the screen.*/

		void disableFramebuffer();	/**< Disables the framebuffer so that drawing goes directly to the screen.*/
		void enableFramebuffer();	/**< Enables the framebuffer for drawing.*/

		void setColorAttachment( uint i, GpuTexture * target );						/**< Sets a color attachment.*/
		void setColorAttachment( uint i, GpuTexture * target, size_t layer );		/**< Sets a color attachment.*/
		void setColorAttachment( uint i, GpuRenderbuffer * target );				/**< Sets a color attachment.*/
		void setDepthAttachment( GpuTexture * target );								/**< Sets the depth attachment.*/
		void setDepthAttachment( GpuTexture * target, size_t layer );				/**< Sets the depth attachment.*/
		void setDepthAttachment( GpuRenderbuffer * target );						/**< Sets the depth attachment.*/
		void setStencilAttachment( GpuTexture * target );							/**< Sets the stencil attachment.*/
		void setStencilAttachment( GpuTexture * target, size_t layer );				/**< Sets the stencil attachment.*/
		void setStencilAttachment( GpuRenderbuffer * target );						/**< Sets the stencil attachment.*/
		void setDepthStencilAttachment( GpuTexture * target );						/**< Sets the depth/stencil attachment.*/
		void setDepthStencilAttachment( GpuTexture * target, size_t layer );		/**< Sets the depth/stencil attachment.*/
		void setDepthStencilAttachment( GpuRenderbuffer * target );					/**< Sets the depth/stencil attachment.*/

		void bind();													/**< Applies all changes made to attachments.*/

		RenderTarget getColorAttachment( uint i ) const;				/**< Returns a color attachment.*/
		RenderTarget getDepthAttachment() const;						/**< Returns the depth attachment.*/
		RenderTarget getStencilAttachment() const;						/**< Returns the stencil attachment.*/

		void setDrawBuffers( uint count, GLenum * buffers );			/**< Sets the draw buffers.*/

		void disableScreenBlit();										/**< Disables automatic blitting to the screen framebuffer.*/
		void enableScreenBlit( uint buffer );							/**< Enables automatic blitting to the screen buffer from the specified color buffer.*/

	private:
		// whether the render target state is initialized
		bool initialized;

		GLuint framebufferId;		// the OpenGL ID of the framebuffer
		bool framebufferEnabled;	// whether the framebuffer should be bound
		bool framebufferBound;		// whether the framebuffer is currently bound

		RenderTarget colorAttachments[MAX_COLOR_ATTACHMENTS];	// the color attachments to be bound
		RenderTarget depthAttachment;							// the depth attachment to be bound
		RenderTarget stencilAttachment;							// the stencil attachment to be bound

		RenderTarget boundColorAttachments[MAX_COLOR_ATTACHMENTS];	// the currently bound color attachments
		RenderTarget boundDepthAttachment;							// the currently bound depth attachment
		RenderTarget boundStencilAttachment;						// the currently bound stencil attachment

		GLenum drawBuffers[MAX_COLOR_ATTACHMENTS];	// the draw buffers
		uint drawBuffersCount;						// the number of active draw buffers
		uint screenBuffer;							// the buffer to be drawn to the screen at the end of the frame

		static void setRenderTarget( RenderTarget & rt, GpuTexture * t );				// sets render target properties
		static void setRenderTarget( RenderTarget & rt, GpuTexture * t, size_t layer );	// sets render target properties
		static void setRenderTarget( RenderTarget & rt, GpuRenderbuffer * t );			// sets render target properties
		static void attachRenderTarget( RenderTarget & rt, GLenum attachment );			// performs the attachment operation
	} renderTargetState;	/**< An instance of the render target state.*/

	/** @brief Used to keep track of the viewport.
	 */
	class ViewportState {
	public:
		void reset();															/**< Resets the viewport.*/
		void setViewport( const Vector2i & vpPos, const Vector2i & vpSize );	/**< Sets the viewport.*/
		void setViewportToWindow();												/**< Sets the viewport to fit the window.*/

		Vector2i getViewportPosition() const;	/**< Returns the viewport position.*/
		Vector2i getViewportSize() const;		/**< Returns the viewport size.*/

	private:
		Vector2i position;	// the viewport position
		Vector2i size;		// the viewport size
	} viewportState;	/**< An instance of the ViewportState.*/

	/** @brief Used to make draw calls.
	 */
	class DrawCallState {
	public:
		DrawCallState( ShaderState & ss );	/**< The constructor.*/

		void reset();	/**< Resets the draw state.*/

		/** @brief Sets the mode for drawing primitives.
		 */
		void setPrimitiveMode( GLenum primMode );

		/** @brief Sets the index type.
		 */
		void setIndexType( GLenum indType );

		/** @brief Sets the index type and primitive restart index.
		 */
		void setIndexType( GLenum indType, GLuint primRestartIndex );

		/** @brief Draws unindexed elements.
		 */
		void draw( size_t firstElement, size_t elementCount );

		/** @brief Draws indexed elements.
		 */
		void drawIndexed( size_t indexBufferOffset, size_t elementCount );

		/** @brief Draws ranged indexed elements.
		 */
		void drawIndexedRange( size_t indexBufferOffset, size_t elementCount, size_t minElement, size_t maxElement );

	private:
		ShaderState & shaderState;	// reference to the shader state

		GLenum drawMode;				// the primitive drawing mode
		GLenum indexType;				// the type of indices (GL_UNSIGNED_INT, etc.)
		bool primitiveRestartEnabled;	// whether primitive restarting is enabled
		GLuint primitiveRestartIndex;	// the primitive restart index

		void checkShaderState();		// outputs a warning if the shader state is invalid
	} drawCallState;	/**< An instance of the DrawCallState.*/

	/** @brief The constructor initializes all nested classes.
	 */
	RenderState( const Window & w );

	/** @brief Called after the window is created to initialize OpenGL state.
	 */
	void init();

	/** @brief Called before the window is destroyed to terminate OpenGL state.
	 */
	void term();

	/** @brief Resets all (tracked) components of OpenGL's state.
	 *
	 *  When called, all reset functions
	 *  of nested classes are called.
	 */
	void beginFrame();

	/** @brief Resets required components at the end of a frame.
	 */
	void endFrame();

	/** @brief Returns the total number of frames rendered.
	 */
	size_t getFrames() const;
};

#endif

#endif