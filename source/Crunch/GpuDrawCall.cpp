#include "GpuDrawCall.h"
#include "GpuState.h"

GpuDrawState::GpuDrawState()
	: GpuObject( NULL ) {
}

void GpuDrawState::bind() {
	getGpuState()->bind( *this );
}

void GpuDrawState::setPrimitiveRestartEnabled( bool e ) {
	primitiveRestartEnabled = e;
}

bool GpuDrawState::isPrimitiveRestartEnabled() const {
	return primitiveRestartEnabled;
}

void GpuDrawState::setPrimitiveRestartIndex( size_t index ) {
	primitiveRestartIndex = index;
}

size_t GpuDrawState::getPrimitiveRestartIndex() const {
	return primitiveRestartIndex;
}

void GpuDrawState::setClearColor( const Vector4f & color ) {
	clearColor = color;
}

Vector4f GpuDrawState::getClearColor() const {
	return clearColor;
}

void GpuDrawState::setClearDepth( float depth ) {
	clearDepth = depth;
}

float GpuDrawState::getClearDepth() const {
	return clearDepth;
}

void GpuDrawState::setClearStencil( int stencil ) {
	clearStencil = stencil;
}

int GpuDrawState::getClearStencil() const {
	return clearStencil;
}

GpuDrawState::GpuDrawState( GpuState * state )
	: GpuObject( state ) {
	primitiveRestartEnabled = false;
	primitiveRestartIndex = 0;
	clearDepth = 1.0f;
	clearStencil = 0;
}

void GpuDrawState::apply( GpuDrawState & currentState ) const {
	setStateEnabled( primitiveRestartEnabled, currentState.primitiveRestartEnabled, GL_PRIMITIVE_RESTART );
	if (primitiveRestartEnabled) {
		if (primitiveRestartIndex != currentState.primitiveRestartIndex) {
			glPrimitiveRestartIndex( (uint)primitiveRestartIndex );
			currentState.primitiveRestartIndex = primitiveRestartIndex;
		}
	}

	if (clearColor != currentState.clearColor) {
		glClearColor( clearColor.r, clearColor.g, clearColor.b, clearColor.a );
		currentState.clearColor = clearColor;
	}

	if (clearDepth != currentState.clearDepth) {
		glClearDepth( clearDepth );
		currentState.clearDepth = clearDepth;
	}

	if (clearStencil != currentState.clearStencil) {
		glClearStencil( clearStencil );
		currentState.clearStencil = clearStencil;
	}
}

void GpuDrawState::getCurrentState() {
	primitiveRestartEnabled = glIsEnabled( GL_PRIMITIVE_RESTART ) != 0;

	GLuint primitiveRestartIndexResult;
	glGetIntegerv( GL_PRIMITIVE_RESTART_INDEX, (GLint*)&primitiveRestartIndexResult );
	primitiveRestartIndex = (size_t)primitiveRestartIndexResult;

	glGetFloatv( GL_COLOR_CLEAR_VALUE, &clearColor.x );
	glGetFloatv( GL_DEPTH_CLEAR_VALUE, &clearDepth );
	glGetIntegerv( GL_STENCIL_CLEAR_VALUE, &clearStencil );
}

GpuDrawCall::GpuDrawCall()
	: GpuObject( NULL ) {
}

void GpuDrawCall::setParameters( GLenum drawMode, size_t firstVertex, size_t vertexCount ) {
	mode = drawMode;
	first = firstVertex;
	vertices = vertexCount;
}

void GpuDrawCall::draw() {
	glDrawArrays( mode, (int)first, (uint)vertices );
}

GpuDrawCall::GpuDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	first = 0;
	vertices = 0;
}

GpuInstancedDrawCall::GpuInstancedDrawCall()
	: GpuObject( NULL ) {
}

void GpuInstancedDrawCall::setParameters( GLenum drawMode, size_t firstVertex, size_t vertexCount, size_t instanceCount ) {
	mode = drawMode;
	first = firstVertex;
	vertices = vertexCount;
	instances = instanceCount;
}

void GpuInstancedDrawCall::draw() {
	glDrawArraysInstanced( mode, (int)first, (uint)vertices, (uint)instances );
}

GpuInstancedDrawCall::GpuInstancedDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	first = 0;
	vertices = 0;
	instances = 0;
}

GpuIndexedDrawCall::GpuIndexedDrawCall()
	: GpuObject( NULL ) {
}

void GpuIndexedDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	restart = false;
}

void GpuIndexedDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer,
	size_t primitiveRestartIndex ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	restart = true;
	restartIndex = primitiveRestartIndex;
}

void GpuIndexedDrawCall::draw() {
	GpuDrawState drawState = getGpuState()->getCurrentDrawState();
	drawState.setPrimitiveRestartEnabled( restart );
	drawState.setPrimitiveRestartIndex( restartIndex );
	drawState.bind();
	glDrawElements( mode, (uint)indices, type, pointer );
}

GpuIndexedDrawCall::GpuIndexedDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	indices = 0;
	type = GL_UNSIGNED_INT;
	pointer = NULL;
	restart = false;
	restartIndex = 0;
}

GpuIndexedOffsetDrawCall::GpuIndexedOffsetDrawCall()
	: GpuObject( NULL ) {
}

void GpuIndexedOffsetDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	offset = vertexOffset;
	restart = false;
}

void GpuIndexedOffsetDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset,
	size_t primitiveRestartIndex ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	offset = vertexOffset;
	restart = true;
	restartIndex = primitiveRestartIndex;
}

void GpuIndexedOffsetDrawCall::draw() {
	GpuDrawState drawState = getGpuState()->getCurrentDrawState();
	drawState.setPrimitiveRestartEnabled( restart );
	drawState.setPrimitiveRestartIndex( restartIndex );
	drawState.bind();
	// bug in spec? seems like this should be const void *
	glDrawElementsBaseVertex( mode, (uint)indices, type, (void*)pointer, offset );
}

GpuIndexedOffsetDrawCall::GpuIndexedOffsetDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	indices = 0;
	type = GL_UNSIGNED_INT;
	pointer = NULL;
	offset = 0;
	restart = false;
	restartIndex = 0;
}

GpuIndexedInstancedDrawCall::GpuIndexedInstancedDrawCall()
	: GpuObject( NULL ) {
}

void GpuIndexedInstancedDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	instances = instanceCount;
	restart = false;
}

void GpuIndexedInstancedDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount,
	size_t primitiveRestartIndex ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	instances = instanceCount;
	restart = true;
	restartIndex = primitiveRestartIndex;
}

void GpuIndexedInstancedDrawCall::draw() {
	GpuDrawState drawState = getGpuState()->getCurrentDrawState();
	drawState.setPrimitiveRestartEnabled( restart );
	drawState.setPrimitiveRestartIndex( restartIndex );
	drawState.bind();
	glDrawElementsInstanced( mode, (uint)indices, type, pointer, (uint)instances );
}

GpuIndexedInstancedDrawCall::GpuIndexedInstancedDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	indices = 0;
	type = GL_UNSIGNED_INT;
	pointer = NULL;
	instances = 0;
	restart = false;
	restartIndex = 0;
}

GpuIndexedInstancedOffsetDrawCall::GpuIndexedInstancedOffsetDrawCall()
	: GpuObject( NULL ) {
}

void GpuIndexedInstancedOffsetDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount, int vertexOffset ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	instances = instanceCount;
	offset = vertexOffset;
	restart = false;
}

void GpuIndexedInstancedOffsetDrawCall::setParameters( GLenum drawMode, size_t indexCount, GLenum indexType, const void * indexPointer, size_t instanceCount, int vertexOffset,
	size_t primitiveRestartIndex ) {
	mode = drawMode;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	instances = instanceCount;
	offset = vertexOffset;
	restart = false;
	restartIndex = primitiveRestartIndex;
}

void GpuIndexedInstancedOffsetDrawCall::draw() {
	GpuDrawState drawState = getGpuState()->getCurrentDrawState();
	drawState.setPrimitiveRestartEnabled( restart );
	drawState.setPrimitiveRestartIndex( restartIndex );
	drawState.bind();
	glDrawElementsInstancedBaseVertex( mode, (uint)indices, type, pointer, (uint)instances, offset );
}

GpuIndexedInstancedOffsetDrawCall::GpuIndexedInstancedOffsetDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	indices = 0;
	type = GL_UNSIGNED_INT;
	pointer = NULL;
	instances = 0;
	offset = 0;
	restart = false;
	restartIndex = 0;
}

GpuIndexedRangedDrawCall::GpuIndexedRangedDrawCall()
	: GpuObject( NULL ) {
}

void GpuIndexedRangedDrawCall::setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer ) {
	mode = drawMode;
	smallest = smallestIndex;
	largest = largestIndex;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	restart = false;
}

void GpuIndexedRangedDrawCall::setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer,
	size_t primitiveRestartIndex ) {
	mode = drawMode;
	smallest = smallestIndex;
	largest = largestIndex;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	restart = true;
	restartIndex = primitiveRestartIndex;
}

void GpuIndexedRangedDrawCall::draw() {
	GpuDrawState drawState = getGpuState()->getCurrentDrawState();
	drawState.setPrimitiveRestartEnabled( restart );
	drawState.setPrimitiveRestartIndex( restartIndex );
	drawState.bind();
	glDrawRangeElements( mode, (uint)smallest, (uint)largest, (uint)indices, type, pointer );
}

GpuIndexedRangedDrawCall::GpuIndexedRangedDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	smallest = 0;
	largest = 0;
	indices = 0;
	type = GL_UNSIGNED_INT;
	pointer = NULL;
	restart = false;
	restartIndex = 0;
}

GpuIndexedRangedOffsetDrawCall::GpuIndexedRangedOffsetDrawCall()
	: GpuObject( NULL ) {
}

void GpuIndexedRangedOffsetDrawCall::setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset ) {
	mode = drawMode;
	smallest = smallestIndex;
	largest = largestIndex;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	offset = vertexOffset;
	restart = false;
}

void GpuIndexedRangedOffsetDrawCall::setParameters( GLenum drawMode, size_t smallestIndex, size_t largestIndex, size_t indexCount, GLenum indexType, const void * indexPointer, int vertexOffset,
	size_t primitiveRestartIndex ) {
	mode = drawMode;
	smallest = smallestIndex;
	largest = largestIndex;
	indices = indexCount;
	type = indexType;
	pointer = indexPointer;
	offset = vertexOffset;
	restart = true;
	restartIndex = primitiveRestartIndex;
}

void GpuIndexedRangedOffsetDrawCall::draw() {
	GpuDrawState drawState = getGpuState()->getCurrentDrawState();
	drawState.setPrimitiveRestartEnabled( restart );
	drawState.setPrimitiveRestartIndex( restartIndex );
	drawState.bind();
	// bug in spec? seems like this should be const void *
	glDrawRangeElementsBaseVertex( mode, (uint)smallest, (uint)largest, (uint)indices, type, (void*)pointer, offset );
}

GpuIndexedRangedOffsetDrawCall::GpuIndexedRangedOffsetDrawCall( GpuState * state )
	: GpuObject( state ) {
	mode = GL_TRIANGLES;
	smallest = 0;
	largest = 0;
	indices = 0;
	type = GL_UNSIGNED_INT;
	pointer = NULL;
	offset = 0;
	restart = false;
	restartIndex = 0;
}

GpuClearCall::GpuClearCall()
	: GpuObject( NULL ) {
}

void GpuClearCall::setClearColor( const Vector4f & color ) {
	clearColor = color;
}

void GpuClearCall::setClearDepth( float depth ) {
	clearDepth = depth;
}

void GpuClearCall::setClearStencil( int stencil ) {
	clearStencil = stencil;
}

void GpuClearCall::setClearBuffers( GLbitfield buffers ) {
	clearBuffers = buffers;
}

void GpuClearCall::clear() {
	GpuDrawState drawState = getGpuState()->getCurrentDrawState();
	if ((clearBuffers & GL_COLOR_BUFFER_BIT) != 0)
		drawState.setClearColor( clearColor );
	if ((clearBuffers & GL_DEPTH_BUFFER_BIT) != 0)
		drawState.setClearDepth( clearDepth );
	if ((clearBuffers & GL_STENCIL_BUFFER_BIT) != 0)
		drawState.setClearStencil( clearStencil );
	drawState.bind();
	glClear( clearBuffers );
}

GpuClearCall::GpuClearCall( GpuState * state )
	: GpuObject( state ) {
	clearDepth = 1.0f;
	clearStencil = 0;
	clearBuffers = 0;
}