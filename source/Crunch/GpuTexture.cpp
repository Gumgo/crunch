#include "GpuTexture.h"
#include "GpuState.h"

GpuTexture::Data1DDescription::Data1DDescription() {
	width = 0;
	format = GL_RGBA;
	type = GL_UNSIGNED_BYTE;
	data = NULL;
}

GpuTexture::Data2DDescription::Data2DDescription() {
	width = 0;
	height = 0;
	format = GL_RGBA;
	type = GL_UNSIGNED_BYTE;
	data = NULL;
}

GpuTexture::Data3DDescription::Data3DDescription() {
	width = 0;
	height = 0;
	depth = 0;
	format = GL_RGBA;
	type = GL_UNSIGNED_BYTE;
	data = NULL;
}

GpuTexture::ImageDescription::ImageDescription() {
	internalFormat = GL_RGBA8;
	minFilter = GL_LINEAR;
	magFilter = GL_LINEAR;
	depthCompareModeEnabled = false;
	depthCompareFunction = GL_LEQUAL;
}

GpuTexture::MipmapDescription::MipmapDescription() {
	baseLevel = 0;
	maxLevel = 1000;
	minLod = -1000.0f;
	maxLod = 1000.0f;
	lodBias = 0.0f;
	levelCount = 1;
	generateMipmaps = false;
}

GpuTexture::MultisampleDescription::MultisampleDescription() {
	samples = 0;
	fixedSampleLocations = false;
}

GpuTexture::Texture1DDescription::Texture1DDescription() {
	mipmapLevels = NULL;
	wrapS = GL_REPEAT;
}

GpuTexture::Texture1DArrayDescription::Texture1DArrayDescription() {
	mipmapLevels = NULL;
	wrapS = GL_REPEAT;
}

GpuTexture::Texture2DDescription::Texture2DDescription() {
	mipmapLevels = NULL;
	wrapS = GL_REPEAT;
	wrapT = GL_REPEAT;
}

GpuTexture::Texture2DArrayDescription::Texture2DArrayDescription() {
	mipmapLevels = NULL;
	wrapS = GL_REPEAT;
	wrapT = GL_REPEAT;
}

GpuTexture::Texture2DMultisampleDescription::Texture2DMultisampleDescription() {
	width = 0;
	height = 0;
	wrapS = GL_REPEAT;
	wrapT = GL_REPEAT;
}

GpuTexture::Texture2DMultisampleArrayDescription::Texture2DMultisampleArrayDescription() {
	width = 0;
	height = 0;
	depth = 0;
	wrapS = GL_REPEAT;
	wrapT = GL_REPEAT;
}

GpuTexture::Texture3DDescription::Texture3DDescription() {
	mipmapLevels = NULL;
	wrapS = GL_REPEAT;
	wrapT = GL_REPEAT;
	wrapR = GL_REPEAT;
}

GpuTexture::TextureBufferDescription::TextureBufferDescription() {
	internalFormat = GL_RGBA8;
}

GpuTexture::TextureCubeMapDescription::TextureCubeMapDescription() {
	for (size_t i = 0; i < arraySize( mipmapLevels ); ++i)
		mipmapLevels[i] = NULL;
	wrapS = GL_CLAMP_TO_EDGE;
	wrapT = GL_CLAMP_TO_EDGE;
}

GpuTexture::TextureRectangleDescription::TextureRectangleDescription() {
	wrapS = GL_REPEAT;
	wrapT = GL_REPEAT;
}

GpuTexture::GpuTexture( GpuState * gpuState, const Texture1DDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = GL_REPEAT;
	wrapR = GL_REPEAT;
	size[0] = desc.mipmapLevels[0].width;
	size[1] = 0;
	size[2] = 0;
	init( GL_TEXTURE_1D, &desc.imageDescription, &desc.mipmapDescription, NULL, true );
	for (size_t i = 0; i < desc.mipmapDescription.levelCount; ++i)
		glTexImage1D(
			GL_TEXTURE_1D,
			desc.mipmapDescription.baseLevel + (int)i,
			desc.imageDescription.internalFormat,
			desc.mipmapLevels[i].width,
			0,
			desc.mipmapLevels[i].format,
			desc.mipmapLevels[i].type,
			desc.mipmapLevels[i].data );

	if (desc.mipmapDescription.generateMipmaps)
		glGenerateMipmap( type );
}

GpuTexture::GpuTexture( GpuState * gpuState, const Texture1DArrayDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = GL_REPEAT;
	wrapR = GL_REPEAT;
	size[0] = desc.mipmapLevels[0].width;
	size[1] = desc.mipmapLevels[0].height;
	size[2] = 0;
	init( GL_TEXTURE_1D_ARRAY, &desc.imageDescription, &desc.mipmapDescription, NULL, true );
	for (size_t i = 0; i < desc.mipmapDescription.levelCount; ++i)
		glTexImage2D(
			GL_TEXTURE_1D_ARRAY,
			desc.mipmapDescription.baseLevel + (int)i,
			desc.imageDescription.internalFormat,
			desc.mipmapLevels[i].width,
			desc.mipmapLevels[i].height,
			0,
			desc.mipmapLevels[i].format,
			desc.mipmapLevels[i].type,
			desc.mipmapLevels[i].data );

	if (desc.mipmapDescription.generateMipmaps)
		glGenerateMipmap( type );
}

GpuTexture::GpuTexture( GpuState * gpuState, const Texture2DDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = desc.wrapT;
	wrapR = GL_REPEAT;
	size[0] = desc.mipmapLevels[0].width;
	size[1] = desc.mipmapLevels[0].height;
	size[2] = 0;
	init( GL_TEXTURE_2D, &desc.imageDescription, &desc.mipmapDescription, NULL, true );
	for (size_t i = 0; i < desc.mipmapDescription.levelCount; ++i)
		glTexImage2D(
			GL_TEXTURE_2D,
			desc.mipmapDescription.baseLevel + (int)i,
			desc.imageDescription.internalFormat,
			desc.mipmapLevels[i].width,
			desc.mipmapLevels[i].height,
			0,
			desc.mipmapLevels[i].format,
			desc.mipmapLevels[i].type,
			desc.mipmapLevels[i].data );

	if (desc.mipmapDescription.generateMipmaps)
		glGenerateMipmap( type );
}

GpuTexture::GpuTexture( GpuState * gpuState, const Texture2DArrayDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = desc.wrapT;
	wrapR = GL_REPEAT;
	size[0] = desc.mipmapLevels[0].width;
	size[1] = desc.mipmapLevels[0].height;
	size[2] = desc.mipmapLevels[0].depth;
	init( GL_TEXTURE_2D_ARRAY, &desc.imageDescription, &desc.mipmapDescription, NULL, true );
	for (size_t i = 0; i < desc.mipmapDescription.levelCount; ++i)
		glTexImage3D(
			GL_TEXTURE_2D_ARRAY,
			desc.mipmapDescription.baseLevel + (int)i,
			desc.imageDescription.internalFormat,
			desc.mipmapLevels[i].width,
			desc.mipmapLevels[i].height,
			desc.mipmapLevels[i].depth,
			0,
			desc.mipmapLevels[i].format,
			desc.mipmapLevels[i].type,
			desc.mipmapLevels[i].data );

	if (desc.mipmapDescription.generateMipmaps)
		glGenerateMipmap( type );
}

GpuTexture::GpuTexture( GpuState * gpuState, const Texture2DMultisampleDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = desc.wrapT;
	wrapR = GL_REPEAT;
	size[0] = desc.width;
	size[1] = desc.height;
	size[2] = 0;
	init( GL_TEXTURE_2D_MULTISAMPLE, &desc.imageDescription, NULL, &desc.multisampleDescription, true );
	glTexImage2DMultisample(
		GL_TEXTURE_2D_MULTISAMPLE,
		desc.multisampleDescription.samples,
		desc.imageDescription.internalFormat,
		desc.width,
		desc.height,
		desc.multisampleDescription.fixedSampleLocations );
}

GpuTexture::GpuTexture( GpuState * gpuState, const Texture2DMultisampleArrayDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = desc.wrapT;
	wrapR = GL_REPEAT;
	size[0] = desc.width;
	size[1] = desc.height;
	size[2] = desc.depth;
	init( GL_TEXTURE_2D_MULTISAMPLE_ARRAY, &desc.imageDescription, NULL, &desc.multisampleDescription, true );
	glTexImage3DMultisample(
		GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
		desc.multisampleDescription.samples,
		desc.imageDescription.internalFormat,
		desc.width,
		desc.height,
		desc.depth,
		desc.multisampleDescription.fixedSampleLocations );
}

GpuTexture::GpuTexture( GpuState * gpuState, const Texture3DDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = desc.wrapT;
	wrapR = desc.wrapR;
	size[0] = desc.mipmapLevels[0].width;
	size[1] = desc.mipmapLevels[0].height;
	size[2] = desc.mipmapLevels[0].depth;
	init( GL_TEXTURE_3D, &desc.imageDescription, &desc.mipmapDescription, NULL, true );
	for (size_t i = 0; i < desc.mipmapDescription.levelCount; ++i)
		glTexImage3D(
			GL_TEXTURE_3D,
			desc.mipmapDescription.baseLevel + (int)i,
			desc.imageDescription.internalFormat,
			desc.mipmapLevels[i].width,
			desc.mipmapLevels[i].height,
			desc.mipmapLevels[i].depth,
			0,
			desc.mipmapLevels[i].format,
			desc.mipmapLevels[i].type,
			desc.mipmapLevels[i].data );

	if (desc.mipmapDescription.generateMipmaps)
		glGenerateMipmap( type );
}

GpuTexture::GpuTexture( GpuState * gpuState, const TextureBufferDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	size[0] = desc.buffer->getSize();
	size[1] = 0;
	size[2] = 0;
	buffer = desc.buffer;
	init( GL_TEXTURE_BUFFER, NULL, NULL, NULL, false );
}

GpuTexture::GpuTexture( GpuState * gpuState, const TextureCubeMapDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = desc.wrapT;
	wrapR = GL_REPEAT;
	size[0] = desc.mipmapLevels[0][0].width;
	size[1] = desc.mipmapLevels[0][0].height;
	size[2] = 0;
	init( GL_TEXTURE_CUBE_MAP, &desc.imageDescription, &desc.mipmapDescription, NULL, true );
	for (uint f = 0; f < 6; ++f) {
		for (size_t i = 0; i < desc.mipmapDescription.levelCount; ++i)
			glTexImage2D(
				GL_TEXTURE_CUBE_MAP_POSITIVE_X + f,
				desc.mipmapDescription.baseLevel + (int)i,
				desc.imageDescription.internalFormat,
				desc.mipmapLevels[f][i].width,
				desc.mipmapLevels[f][i].height,
				0,
				desc.mipmapLevels[f][i].format,
				desc.mipmapLevels[f][i].type,
				desc.mipmapLevels[f][i].data );
	}

	if (desc.mipmapDescription.generateMipmaps)
		glGenerateMipmap( type );
}

GpuTexture::GpuTexture( GpuState * gpuState, const TextureRectangleDescription & desc )
	: ReferenceCountedGpuObject( gpuState ) {
	wrapS = desc.wrapS;
	wrapT = desc.wrapT;
	wrapR = GL_REPEAT;
	size[0] = desc.mipmapLevel.width;
	size[1] = desc.mipmapLevel.height;
	size[2] = 0;
	init( GL_TEXTURE_RECTANGLE, &desc.imageDescription, NULL, NULL, true );
	glTexImage2D(
		GL_TEXTURE_RECTANGLE,
		0,
		desc.imageDescription.internalFormat,
		desc.mipmapLevel.width,
		desc.mipmapLevel.height,
		0,
		desc.mipmapLevel.format,
		desc.mipmapLevel.type,
		desc.mipmapLevel.data );
}

GpuTexture::~GpuTexture() {
	// when the texture is deleted, all references to it have been released
	// this means that we don't need to update any state (it already has been updated)
	glDeleteTextures( 1, &id );
}

GLuint GpuTexture::getId() const {
	return id;
}

GLenum GpuTexture::getType() const {
	return type;
}

GLenum GpuTexture::getInternalFormat() const {
	return internalFormat;
}

void GpuTexture::setImageFilters( GLenum minFilter, GLenum magFilter ) {
	if (minFilter != imageMinFilter) {
		bind();
		glTexParameteri( type, GL_TEXTURE_MIN_FILTER, minFilter );
		imageMinFilter = minFilter;
	}

	if (magFilter != imageMagFilter) {
		bind();
		glTexParameteri( type, GL_TEXTURE_MAG_FILTER, magFilter );
		imageMagFilter = magFilter;
	}
}

GLenum GpuTexture::getImageMinFilter() const {
	return imageMinFilter;
}

GLenum GpuTexture::getImageMagFilter() const {
	return imageMagFilter;
}

void GpuTexture::setImageBorderColor( const Vector4f & borderColor ) {
	if (borderColor != imageBorderColor) {
		bind();
		glTexParameterfv( type, GL_TEXTURE_BORDER_COLOR, &borderColor.x );
		imageBorderColor = borderColor;
	}
}

Vector4f GpuTexture::getImageBorderColor() const {
	return imageBorderColor;
}

void GpuTexture::setImageDepthCompareModeEnabled( bool e ) {
	if (e != imageDepthCompareModeEnabled) {
		bind();
		glTexParameteri( type, GL_TEXTURE_COMPARE_MODE, e ? GL_COMPARE_REF_TO_TEXTURE : GL_NONE );
		imageDepthCompareModeEnabled = e;
	}
}

bool GpuTexture::isImageDepthCompareModeEnabled() const {
	return imageDepthCompareModeEnabled;
}

void GpuTexture::setImageDepthCompareFunction( GLenum depthCompareFunction ) {
	if (imageDepthCompareFunction != depthCompareFunction) {
		bind();
		glTexParameteri( type, GL_TEXTURE_COMPARE_FUNC, depthCompareFunction );
		imageDepthCompareFunction = depthCompareFunction;
	}
}

GLenum GpuTexture::getImageDepthCompareFunction() const {
	return imageDepthCompareFunction;
}

size_t GpuTexture::getMipmapLevelCount() const {
	return mipmapLevelCount;
}

void GpuTexture::setMipmapLevels( int baseLevel, int maxLevel ) {
	if (baseLevel != mipmapBaseLevel) {
		bind();
		glTexParameteri( type, GL_TEXTURE_BASE_LEVEL, baseLevel );
		mipmapBaseLevel = baseLevel;
	}

	if (maxLevel != mipmapMaxLevel) {
		bind();
		glTexParameteri( type, GL_TEXTURE_MAX_LEVEL, maxLevel );
		mipmapMaxLevel = maxLevel;
	}
}

int GpuTexture::getMipmapBaseLevel() const {
	return mipmapBaseLevel;
}

int GpuTexture::getMipmapMaxLevel() const {
	return mipmapMaxLevel;
}

void GpuTexture::setMipmapLod( float minLod, float maxLod, float lodBias ) {
	if (minLod != mipmapMinLod) {
		bind();
		glTexParameterf( type, GL_TEXTURE_BASE_LEVEL, minLod );
		mipmapMinLod = minLod;
	}

	if (maxLod != mipmapMaxLod) {
		bind();
		glTexParameterf( type, GL_TEXTURE_BASE_LEVEL, maxLod );
		mipmapMaxLod = maxLod;
	}

	if (lodBias != mipmapLodBias) {
		bind();
		glTexParameterf( type, GL_TEXTURE_BASE_LEVEL, lodBias );
		mipmapLodBias = lodBias;
	}
}

float GpuTexture::getMipmapMinLod() const {
	return mipmapMinLod;
}

float GpuTexture::getMipmapMaxLod() const {
	return mipmapMaxLod;
}

float GpuTexture::getMipmapLodBias() const {
	return mipmapLodBias;
}

uint GpuTexture::getMultisampleSamples() const {
	return multisampleSamples;
}

bool GpuTexture::areMultisampleSampleLocationsFixed() const {
	return multisampleFixedSampleLocations;
}

uint GpuTexture::getSize( size_t dimension ) const {
	return size[dimension];
}

uint GpuTexture::getWidth() const {
	return size[0];
}

uint GpuTexture::getHeight() const {
	return size[1];
}

uint GpuTexture::getDepth() const {
	return size[2];
}

void GpuTexture::setWrapS( GLenum s ) {
	if (s != wrapS) {
		bind();
		glTexParameteri( type, GL_TEXTURE_WRAP_S, s );
		wrapS = s;
	}
}

GLenum GpuTexture::getWrapS() const {
	return wrapS;
}

void GpuTexture::setWrapT( GLenum t ) {
	if (t != wrapT) {
		bind();
		glTexParameteri( type, GL_TEXTURE_WRAP_T, t );
		wrapT = t;
	}
}

GLenum GpuTexture::getWrapT() const {
	return wrapT;
}

void GpuTexture::setWrapR( GLenum r ) {
	if (r != wrapR) {
		glTexParameteri( type, GL_TEXTURE_WRAP_R, r );
		wrapR = r;
	}
}

GLenum GpuTexture::getWrapR() const {
	return wrapR;
}

GpuBufferReference GpuTexture::getBuffer() {
	return buffer;
}

GpuBufferConstReference GpuTexture::getBuffer() const {
	return buffer;
}

void GpuTexture::bind() {
	// this pointer implicitly converted into "smart reference"
	getGpuState()->getActiveTextureImageUnit()->bindTexture( this );
}

void GpuTexture::bind( size_t textureImageUnit ) {
	// this pointer implicitly converted into "smart reference"
	getGpuState()->getTextureImageUnit( textureImageUnit )->bindTexture( this );
}

void GpuTexture::onRelease() const {
	// no references left
	delete this;
}

void GpuTexture::init(
	GLenum textureType,
	const ImageDescription * imageDesc,
	const MipmapDescription * mipmapDesc,
	const MultisampleDescription * multisampleDesc,
	bool setWrap ) {
	// generate a texture and bind it on the current texture unit
	glGenTextures( 1, &id );
	type = textureType;
	bind();

	// assign default values for missing descriptions

	if (imageDesc == NULL) {
		internalFormat = GL_RGBA8;
		imageMinFilter = GL_LINEAR;
		imageMagFilter = GL_LINEAR;
		imageDepthCompareModeEnabled = false;
		imageDepthCompareFunction = GL_LEQUAL;
	} else {
		internalFormat = imageDesc->internalFormat;

		glTexParameteri( textureType, GL_TEXTURE_MIN_FILTER, imageDesc->minFilter );
		imageMinFilter = imageDesc->minFilter;

		glTexParameteri( textureType, GL_TEXTURE_MAG_FILTER, imageDesc->magFilter );
		imageMagFilter = imageDesc->magFilter;

		glTexParameteri( textureType, GL_TEXTURE_COMPARE_MODE, imageDesc->depthCompareModeEnabled ? GL_COMPARE_REF_TO_TEXTURE : GL_NONE );
		imageDepthCompareModeEnabled = imageDesc->depthCompareModeEnabled;

		glTexParameteri( textureType, GL_TEXTURE_COMPARE_FUNC, imageDesc->depthCompareFunction );
		imageDepthCompareFunction = imageDesc->depthCompareFunction;
	}

	if (mipmapDesc == NULL) {
		mipmapLevelCount = 0;
		mipmapBaseLevel = 0;
		mipmapMaxLevel = 1000;
		mipmapMinLod = -1000.0f;
		mipmapMaxLod = 1000.0f;
		mipmapLodBias = 0.0f;
	} else {
		mipmapLevelCount = mipmapDesc->levelCount;

		glTexParameteri( textureType, GL_TEXTURE_BASE_LEVEL, mipmapDesc->baseLevel );
		mipmapBaseLevel = mipmapDesc->baseLevel;

		glTexParameteri( textureType, GL_TEXTURE_MAX_LEVEL, mipmapDesc->maxLevel );
		mipmapMaxLevel = mipmapDesc->maxLevel;

		glTexParameterf( textureType, GL_TEXTURE_MIN_LOD, mipmapDesc->minLod );
		mipmapMinLod = mipmapDesc->minLod;

		glTexParameterf( textureType, GL_TEXTURE_MAX_LOD, mipmapDesc->maxLod );
		mipmapMaxLod = mipmapDesc->maxLod;

		glTexParameterf( textureType, GL_TEXTURE_LOD_BIAS, mipmapDesc->lodBias );
		mipmapLodBias = mipmapDesc->lodBias;
	}

	if (multisampleDesc == NULL) {
		multisampleSamples = 0;
		multisampleFixedSampleLocations = false;
	} else {
		multisampleSamples = multisampleDesc->samples;
		multisampleFixedSampleLocations = multisampleDesc->fixedSampleLocations;
	}

	// these should be set by the calling function
	if (setWrap) {
		glTexParameteri( textureType, GL_TEXTURE_WRAP_S, wrapS );
		glTexParameteri( textureType, GL_TEXTURE_WRAP_T, wrapT );
		glTexParameteri( textureType, GL_TEXTURE_WRAP_R, wrapR );
	} else {
		wrapS = GL_REPEAT;
		wrapT = GL_REPEAT;
		wrapR = GL_REPEAT;
	}
}