#include "Physics2DConvexPolygonShape.h"
#include "Physics2DBody.h"

namespace Physics2D {

ConvexPolygonShape::ConvexPolygonShape( const ConvexPolygonShapeDefinition & d, Body * par )
	: Shape( d, Shape::T_CONVEX_POLYGON, par ) {
	setArea( (Real)0 );

	vertexCount = 0;
	for (size_t i = 0; i < d.vertexCount; ++i)
		addVertex( d.vertices[i] );
}

void ConvexPolygonShape::addVertex( const Vector2r & vertex ) {
	if (vertexCount >= MAX_VERTICES)
		throw std::runtime_error( "Exceeded maximum vertex count for convex polygon shape" );
	vertices[vertexCount] = vertex;
	++vertexCount;

	// if we have at least 3 vertices, the last vertex added a new triangle
	if (vertexCount >= 3) {
		// add area of the new triangle
		// a = v1 - v0
		// b = v2 - v0
		// area = 0.5 * |a x b|
		//      = 0.5 * |(ay*0 - by*0, 0*bx - 0*ax, ax*by - bx*ay)|
		//      = 0.5 * |ax*by - bx*ay|
		Vector2r a = vertices[vertexCount-2] - vertices[0];
		Vector2r b = vertices[vertexCount-1] - vertices[0];
		setArea( getArea() + (Real)0.5 * abs( a.x*b.y - b.x*a.y ) );
	}

}

void ConvexPolygonShape::clearVertices() {
	vertexCount = 0;
	setArea( (Real)0 );
}

Vector2r ConvexPolygonShape::getVertex( size_t index ) const {
	return vertices[index];
}

size_t ConvexPolygonShape::getVertexCount() const {
	return vertexCount;
}

Aabb ConvexPolygonShape::getAabb( const Matrix33ar & transformation ) const {
	Matrix33ar tf = transformation * getTransform(); // world transform * shape transform

	Vector2r minPt( std::numeric_limits <Real>::max(), std::numeric_limits <Real>::max() );
	Vector2r maxPt( std::numeric_limits <Real>::min(), std::numeric_limits <Real>::min() );

	for (size_t i = 0; i < vertexCount; ++i) {
		Vector2r ptTf = tf.transformPoint( vertices[i] );
		minPt = vecMin( minPt, ptTf );
		maxPt = vecMax( maxPt, ptTf );
	}

	return Aabb( minPt, maxPt );
}

void ConvexPolygonShape::computeMassData() {
	// first compute centroid
	// we also need signed area
	Vector2r centroid;
	Real signedArea = (Real)0;
	for (size_t i = 0; i < vertexCount; ++i) {
		const Vector2r & vi = vertices[i];
		const Vector2r & vi1 = (i < vertexCount-1) ? vertices[i+1] : vertices[0];
		Real mul = vi.x*vi1.y - vi1.x*vi.y;
		centroid.x += (vi.x + vi1.x)*mul;
		centroid.y += (vi.y + vi1.y)*mul;
		signedArea += mul;
	}

	signedArea *= (Real)0.5;
	centroid *= (Real)1 / ((Real)6 * signedArea);

	Real inertia = (Real)0;
	for (size_t i = 0; i < vertexCount-2; ++i) {
		// compute the inertia about the centroid of THIS triangle, then adjust it to be for the entire polygon's centroid
		Vector2r a = vertices[0];
		Vector2r b = vertices[i+1];
		Vector2r c = vertices[i+2];

		Vector2r triCentroid = ((Real)1/(Real)3) * (a + b + c);
		a -= triCentroid;
		b -= triCentroid;
		c -= triCentroid;

		// we integrate by changing variables:
		// x = a + u(b-a) + v(c-a)
		Vector2r s1 = b - a;
		Vector2r s2 = c - a;
		// integrating over the triangle we get the following
		Real detDG = abs( a.x*(b.y-c.y) + b.x*(c.y-a.y) + c.x*(a.y-b.y) );
		// remember that the * operator is done component-wise
		Vector2r integralParts
			= ((Real)1/(Real)12) * ((Real)6*a*a + (Real)4*a*(s1+s2) + s1*s1 + s2*s2 + s1*s2);
		Real inertiaAboutTriangleCentroid = abs( getDensity() * detDG * (integralParts.x + integralParts.y) );
		// now we have the inertia about the triangle centroid
		// we adjust it using the parallel axis theorem to be about the polygon centroid
		Real triMass = getDensity() * (Real)0.5 * abs( s1.x*s2.y - s2.x*s1.y );
		Real inertiaAboutPolygonCentroid = inertiaAboutTriangleCentroid + triMass*(centroid-triCentroid).magnitudeSquared();
		inertia += inertiaAboutPolygonCentroid;
	}

	// now we have the total inertia about the polygon centroid
	// we adjust it if necessary to be about a custom axis

	Real mass = getDensity() * getArea();
	Vector2r massCtr = centroid;
	if (isCenterOfMassOverridden()) {
		inertia += mass*(massCtr - getOverriddenCenterOfMass()).magnitudeSquared();
		massCtr = getOverriddenCenterOfMass();
	}
	setMassData( MassData( getTransform().transformPoint( massCtr ), mass, inertia ) );
}

ConvexPolygonShapeDefinition::ConvexPolygonShapeDefinition() {
	vertexCount = 4;
	vertices[0].set( (Real)1, (Real)1 );
	vertices[1].set( (Real)1, (Real)(-1) );
	vertices[2].set( (Real)(-1), (Real)(-1) );
	vertices[3].set( (Real)(-1), (Real)1 );
}

Shape * ConvexPolygonShapeDefinition::createShape( Body * parent ) const {
	return new ConvexPolygonShape( *this, parent );
}

}