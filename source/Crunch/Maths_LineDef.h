#ifndef MATHS_LINEDEF_DEFINED
#define MATHS_LINEDEF_DEFINED

template <typename T> Line <T>::Line()
	: normal( 0, 1 )
	, c( 0 ) {
}

template <typename T> Line <T>::Line( const Vector2 <T> & norm, T pc )
	: normal( norm )
	, c( pc ) {
}

template <typename T> Line <T>::Line( const Line <T> & p )
	: normal( p.normal )
	, c( p.c ) {
}

template <typename T> Line <T>::~Line() {
}

// operators
template <typename T> Line <T> Line <T>::operator=( const Line <T> & p ) {
	normal = p.normal;
	c = p.c;
	return *this;
}

template <typename T> bool Line <T>::operator==( const Line <T> & p ) const {
	return (normal == p.normal && c == p.c);
}

template <typename T> bool Line <T>::operator!=( const Line <T> & p ) const {
	return (normal != p.normal || c != p.c);
}

template <typename T> template <typename U> Line <T>::operator Line <U> () const {
	return Line <U>( (Vector2 <U>)normal, (U)c );
}

// mutable
template <typename T> void Line <T>::set2Points( const Vector2 <T> & p0, const Vector2 <T> & p1 ) {
	normal.set( p0.y - p1.y, p1.x - p0.x );
	c = -p0.dot( normal );
}

template <typename T> void Line <T>::set( const Vector2 <T> & norm, T pc ) {
	normal = norm;
	c = pc;
}

template <typename T> void Line <T>::setNormalPoint( const Vector2 <T> & norm, const Vector2 <T> & point ) {
	normal = norm;
	c = -norm.dot( point );
}

template <typename T> void Line <T>::normalize() {
	T mag = normal.magnitude();
	normal /= mag;
	c /= mag;
}

// immutable
template <typename T> Line <T> Line <T>::normalized() const {
	T mag = normal.magnitude();
	return Line <T>( normal/mag, c/mag );
}

template <typename T> bool Line <T>::intersects( const Vector2 <T> & origin, const Vector2 <T> & direction, Vector2 <T> & intersection, T & signedDist ) const {
	T denom = normal.dot( direction );
	if (denom == 0)
		return false;
	signedDist = -(normal.dot( origin ) + c)/denom;
	intersection = origin + direction*signedDist;
	return true;
}

template <typename T> bool Line <T>::intersects( const Ray2 <T> & ray, Vector2 <T> & intersection, T & signedDist ) const {
	return intersects( ray.origin, ray.direction, intersection, signedDist );
}

template <typename T> bool Line <T>::intersects( const Vector2 <T> & origin, const Vector2 <T> & direction, Vector2 <T> & intersection ) const {
	T denom = normal.dot( direction );
	if (denom == 0)
		return false;
	T signedDist = -(normal.dot( origin ) + c)/denom;
	intersection = origin + direction*signedDist;
	return true;
}

template <typename T> bool Line <T>::intersects( const Ray2 <T> & ray, Vector2 <T> & intersection ) const {
	return intersects( ray.origin, ray.direction, intersection );
}

template <typename T> bool Line <T>::intersects( const Vector2 <T> & origin, const Vector2 <T> & direction, T & signedDist ) const {
	T denom = normal.dot( direction );
	if (denom == 0)
		return false;
	signedDist = -(normal.dot( origin ) + c)/denom;
	return true;
}

template <typename T> bool Line <T>::intersects( const Ray2 <T> & ray, T & signedDist ) const {
	return intersects( ray.origin, ray.direction, signedDist );
}

template <typename T> T Line <T>::distance( const Vector2 <T> & point ) const {
	return std::abs( normal.dot( point ) + c );
}

template <typename T> T Line <T>::signedDistance( const Vector2 <T> & point ) const {
	return normal.dot( point ) + c;
}

template <typename T> T Line <T>::side( const Vector2 <T> & point ) const {
	return sign( normal.dot( point ) + c );
}

template <typename T> bool Line <T>::x( T & x, T y ) const {
	if (normal.x == 0)
		return false;
	x = -(normal.y*y + c)/normal.x;
	return true;
}

template <typename T> bool Line <T>::y( T x, T & y ) const {
	if (normal.y == 0)
		return false;
	y = -(normal.x*x + c)/normal.y;
	return true;
}

template <typename T> Line <T> Line2Points( const Vector2 <T> & p0, const Vector2 <T> & p1 ) {
	Vector2 <T> normal( p0.y - p1.y, p1.x - p0.x );
	return Line <T>( normal, -p0.dot( normal ) );
}

template <typename T> Line <T> LinePointNormal( const Vector2 <T> & norm, const Vector2 <T> & point ) {
	return Line <T>( norm, -norm.dot( point ) );
}

#endif