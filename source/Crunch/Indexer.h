/** @file Indexer.h
 *  @brief Used to assign indices to objects.
 */

#ifndef INDEXER_DEFINED
#define INDEXER_DEFINED

#include "Common.h"
#include <vector>

/** @brief Used to assign indices to objects.
 *
 *  @tparam T	The type of object to index.
 */
template <typename T> class Indexer {
public:
	/** @brief The index type.
	 */
	typedef size_t Index;

	/** @brief Provides a way to iterate over all entries.
	 */
	class Iterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		T & get();				/**< Returns the current entry.*/
		Index getIndex();		/**< Returns the index of the current entry.*/
		void remove();			/**< Removes the current entry.*/

	private:
		friend class Indexer <T>;

		Indexer <T> * base;		// the indexer this iterator is using
		size_t currentIndex;	// the current index being pointed to
		size_t nextIndex;		// the next index to be pointed to

		Iterator( Indexer <T> * b );
	};

	/** @brief Provides a way to iterate over all entries.
	 */
	class ConstIterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		const T & get();		/**< Returns the current entry.*/
		Index getIndex();		/**< Returns the index of the current entry.*/

	private:
		friend class Indexer <T>;

		const Indexer <T> * base;	// the indexer this iterator is using
		size_t currentIndex;		// the current index being pointed to
		size_t nextIndex;			// the next index to be pointed to

		ConstIterator( const Indexer <T> * b );
	};

	friend class Iterator;
	friend class ConstIterator;

	/** @brief The constructor.
	 *
	 *  @param defaultCapacity	The default capacity for entries.
	 */
	Indexer( size_t defaultCapacity = 20 );

	/** @brief The copy constructor.
	 *
	 *  @param other	The Indexer to copy.
	 */
	Indexer( const Indexer <T> & other );

	/** @brief The destructor.
	 */
	~Indexer();

	/** @brief The move constructor.
	 *
	 *  @param other	The Indexer to move.
	 */
	Indexer( Indexer <T> && other );

	/** @brief The copy assignment operator.
	 *
	 *  @param other	The Indexer to assign.
	 */
	Indexer <T> & operator=( const Indexer <T> & other );

	/** @brief The move assignment operator.
	 *
	 *  @param other	The Indexer to move.
	 */
	Indexer <T> & operator=( Indexer <T> && other );

	/** @brief Adds and assigns an index to the object.
	 *
	 *  @param t	The object to index.
	 *  @return		The assigned index.
	 */
	Index add( const T & t );

	/** @brief Adds and assigns an index to the object.
	 *
	 *  @param t	The object to index.
	 *  @return		The assigned index.
	 */
	Index add( T && t );

	/** @brief Returns the object associated with the given index.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @return			The object associated with the given index.
	 */
	T & get( Index index );

	/** @brief Returns the object associated with the given index.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @return			The object associated with the given index.
	 */
	const T & get( Index index ) const;

	/** @brief Assigns the object associated with the given index to t if it exists.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @param t		The destination to store the object.
	 *  @return			Whether the object was found and assigned.
	 */
	bool get( Index index, T & t ) const;

	/** @brief Removes and unindexes the object associated with the index.
	 *
	 *  @param index	The index associated with the object to remove.
	 *  @return			Whether the object was found and removed.
	 */
	bool remove( Index index );

	/** @brief Removes, unindexes, and returns the object associated with the given index.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @return			The object associated with the given index.
	 */
	T extract( Index index );

	/** @brief Assigns the object associated with the given index to t and removes and unindexes it if it exists.
	 *
	 *  @param index	The index for which to find the associated object.
	 *  @param t		The destination to store the object.
	 *  @return			Whether the object was found and assigned.
	 */
	bool extract( Index index, T & t );

	/** @brief Reserves space for future objects to be added.
	 *
	 *  @param newCapacity	The amount of objects to reserve space for.
	 */
	void reserve( size_t newCapacity );

	/** @brief Returns whether the table contains an object associated with the given index.
	 *
	 *  @param index	The index to check for.
	 *  @return			Whether the table contains an object associated with the given index.
	 */
	bool contains( Index index ) const;

	/** @brief Removes and unindexes all objects.
	 */
	void clear();

	/** @brief Returns the number of objects currently stored.
	 */
	size_t size() const;

	/** @brief Returns true if the indexer is empty.
	 */
	bool empty() const;

	/** @brief Returns an iterator.
	 */
	Iterator getIterator();

	/** @brief Returns a const iterator.
	 */
	ConstIterator getConstIterator() const;

	/** @brief Returns the amount of memory used.
	 */
	size_t memory() const;

private:
	// index representing no entry
	static const size_t NO_INDEX = ~(1 << (sizeof( size_t )*8 - 1));

	// an entry with the correct data to ensure alignment
	struct AlignedEntry {
		T t; // the entry data

		// a combination of the free flag and the index of the next entry
		// the free flag occupies the highest bit.
		// The remaining bits represent the index of the next free object if not used,
		// or the index of the next object if used.
		size_t usedAndNextEntry;

		// if used, holds the index of the previous object
		size_t prevEntry;
	};

	// used to hold entries in the indexer
	struct Entry {
		char t[offsetof( AlignedEntry, usedAndNextEntry )];	// space for an instance of T
		size_t usedAndNextEntry;							// the free bit and next entry
		size_t prevEntry;									// the previous entry

		bool used() const;
		size_t getNextFreeEntry() const;
		size_t getNextUsedEntry() const;
		void setNextUsedEntry( size_t nextUsedEntry );
		void setNextFreeEntry( size_t nextFreeEntry );
	};

	size_t capacity;		// the capacity of the underlying array
	size_t count;			// the current number of entries
	union {
		char * entries;					// the indexed entries
		AlignedEntry * alignedEntries;	// provides an easier way to access entry contents
	};
	size_t firstFreeEntry;	// the index of the first free object
	size_t firstUsedEntry;	// the index of the first used entry
	size_t freeEntries;		// the number of free entries
};

template <typename T> bool Indexer <T>::Entry::used() const {
	return (usedAndNextEntry & (1 << (sizeof( usedAndNextEntry )*8 - 1))) != 0;
}

template <typename T> size_t Indexer <T>::Entry::getNextFreeEntry() const {
	return usedAndNextEntry;
}

template <typename T> size_t Indexer <T>::Entry::getNextUsedEntry() const {
	return usedAndNextEntry & ~(1 << (sizeof( usedAndNextEntry )*8 - 1));
}

template <typename T> void Indexer <T>::Entry::setNextUsedEntry( size_t nextUsedEntry ) {
	usedAndNextEntry = nextUsedEntry | (1 << (sizeof( usedAndNextEntry )*8 - 1));
}

template <typename T> void Indexer <T>::Entry::setNextFreeEntry( size_t nextFreeEntry ) {
	usedAndNextEntry = nextFreeEntry & ~(1 << (sizeof( usedAndNextEntry )*8 - 1));
}

template <typename T> Indexer <T>::Iterator::Iterator( Indexer <T> * b ) {
	base = b;
	currentIndex = NO_INDEX;
	nextIndex = base->firstUsedEntry;
}

template <typename T> bool Indexer <T>::Iterator::hasNext() const {
	return nextIndex != NO_INDEX;
}

template <typename T> bool Indexer <T>::Iterator::next() {
	if (nextIndex == NO_INDEX)
		return false;
	else {
		currentIndex = nextIndex;
		nextIndex = ((const Entry*)(base->alignedEntries + nextIndex))->getNextUsedEntry();
		return true;
	}
}

template <typename T> T & Indexer <T>::Iterator::get() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	Entry * e = (Entry*)(base->alignedEntries + currentIndex);
	return *((T*)e->t);
}

template <typename T> typename Indexer <T>::Index Indexer <T>::Iterator::getIndex() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	return currentIndex;
}

template <typename T> void Indexer <T>::Iterator::remove() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	// removing simply "patches up" the hole in the linked list and won't mess up iteration
	base->remove( currentIndex );
	currentIndex = NO_INDEX;
}

template <typename T> Indexer <T>::ConstIterator::ConstIterator( const Indexer <T> * b ) {
	base = b;
	currentIndex = NO_INDEX;
	nextIndex = base->firstUsedEntry;
}

template <typename T> bool Indexer <T>::ConstIterator::hasNext() const {
	return index != NO_INDEX;
}

template <typename T> bool Indexer <T>::ConstIterator::next() {
	if (nextIndex == NO_INDEX)
		return false;
	else {
		currentIndex = nextIndex;
		nextIndex = ((const Entry*)(base->alignedEntries + nextIndex))->getNextUsedEntry();
		return true;
	}
}

template <typename T> const T & Indexer <T>::ConstIterator::get() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	const Entry * e = (const Entry*)(base->alignedEntries + currentIndex);
	return *((const T*)e->t);
}

template <typename T> typename Indexer <T>::Index Indexer <T>::ConstIterator::getIndex() {
	if (currentIndex == NO_INDEX)
		throw std::runtime_error( "Iterator points to invalid entry" );
	return currentIndex;
}

template <typename T> Indexer <T>::Indexer( size_t defaultCapacity ) {
	capacity = 0;
	count = 0;
	entries = NULL;
	freeEntries = 0;
	reserve( defaultCapacity );
	firstUsedEntry = NO_INDEX;
	firstFreeEntry = NO_INDEX; // not actually required but good practice
}

template <typename T> Indexer <T>::Indexer( const Indexer <T> & other ) {
	capacity = other.capacity;
	count = other.count;
	entries = new char[capacity * sizeof( AlignedEntry )];
	firstFreeEntry = other.firstFreeEntry;
	firstUsedEntry = other.firstUsedEntry;
	freeEntries = other.freeEntries;
	size_t offset = 0;
	size_t remaining = other.count + other.freeEntries;
	while (remaining > 0) {
		Entry * entry = new (entries + offset) Entry;
		const Entry * otherEntry = (const Entry*)(other.entries + offset);
		entry->usedAndNextEntry = otherEntry->usedAndNextEntry;
		entry->prevEntry = otherEntry->prevEntry;
		if (entry->used())
			new (entry->t) T( *((const T*)otherEntry->t) );
		offset += sizeof( AlignedEntry );
		--remaining;
	}
}

template <typename T> Indexer <T>::~Indexer() {
	if (entries == NULL)
		return;

	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * entry = (Entry*)(entries + offset);
		if (entry->used())
			((T*)entry->t)->~T();
		entry->~Entry();
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	delete[] entries;
}

template <typename T> Indexer <T>::Indexer( Indexer <T> && other ) {
	capacity = other.capacity;
	count = other.count;
	entries = other.entries;
	other.entries = NULL;
	firstFreeEntry = other.firstFreeEntry;
	firstUsedEntry = other.firstUsedEntry;
	freeEntries = other.freeEntries;
}

template <typename T> Indexer <T> & Indexer <T>::operator=( const Indexer <T> & other ) {
	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * entry = (Entry*)(entries + offset);
		if (entry->used())
			((T*)entry->t)->~T();
		entry->~Entry();
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	if (capacity < other.capacity) {
		delete[] entries;
		capacity = other.capacity;
		entries = new char[capacity * sizeof( AlignedEntry )];
	}
	count = other.count;
	firstFreeEntry = other.firstFreeEntry;
	firstUsedEntry = other.firstUsedEntry;
	freeEntries = other.freeEntries;
	offset = 0;
	remaining = other.count + other.freeEntries;
	while (remaining > 0) {
		Entry * entry = new (entries + offset) Entry;
		const Entry * otherEntry = (const Entry*)(other.entries + offset);
		entry->usedAndNextEntry = otherEntry->usedAndNextEntry;
		entry->prevEntry = otherEntry->prevEntry;
		if (entry->used())
			new (entry->t) T( *((const T*)otherEntry->t) );
		offset += sizeof( AlignedEntry );
		--remaining;
	}
	return *this;
}

template <typename T> Indexer <T> & Indexer <T>::operator=( Indexer <T> && other ) {
	std::swap( capacity, other.capacity );
	std::swap( count, other.count );
	std::swap( entries, other.entries );
	std::swap( firstFreeEntry, other.firstFreeEntry );
	std::swap( firstUsedEntry, other.firstUsedEntry );
	std::swap( freeEntries, other.freeEntries );
	return *this;
}

template <typename T> typename Indexer <T>::Index Indexer <T>::add( const T & t ) {
	size_t index;
	Entry * entry;
	if (freeEntries == 0) {
		if (capacity < count+1)
			reserve( capacity * 2 + 1 );
		index = count;
		size_t offset = index * sizeof( AlignedEntry );
		entry = new (entries + offset) Entry;
	} else {
		index = firstFreeEntry;
		size_t offset = index * sizeof( AlignedEntry );
		entry = (Entry*)(entries + offset);
		firstFreeEntry = entry->getNextFreeEntry();
		--freeEntries;
	}
	entry->prevEntry = NO_INDEX;
	entry->setNextUsedEntry( firstUsedEntry );
	if (firstUsedEntry != NO_INDEX)
		((Entry*)(alignedEntries + firstUsedEntry))->prevEntry = index;
	firstUsedEntry = index;
	++count;
	new (entry->t) T( t );
	return index;
}

template <typename T> typename Indexer <T>::Index Indexer <T>::add( T && t ) {
	size_t index;
	Entry * entry;
	if (freeEntries == 0) {
		if (capacity < count+1)
			reserve( capacity * 2 + 1 );
		index = count;
		size_t offset = index * sizeof( AlignedEntry );
		entry = new (entries + offset) Entry;
	} else {
		index = firstFreeEntry;
		size_t offset = index * sizeof( AlignedEntry );
		entry = (Entry*)(entries + offset);
		firstFreeEntry = entry->getNextFreeEntry();
		--freeEntries;
	}
	entry->prevEntry = NO_INDEX;
	entry->setNextUsedEntry( firstUsedEntry );
	if (firstUsedEntry != NO_INDEX)
		((Entry*)(alignedEntries + firstUsedEntry))->prevEntry = index;
	firstUsedEntry = index;
	++count;
	new (entry->t) T( t );
	return index;
}

template <typename T> T & Indexer <T>::get( Index index ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used())
		return *((T*)e->t);
	else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> const T & Indexer <T>::get( Index index ) const {
	const Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used())
		return *((const T*)e->t);
	else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> bool Indexer <T>::get( Index index, T & t ) const {
	const Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		t = *((const T*)e->t);
		return true;
	} else
		return false;
}

template <typename T> bool Indexer <T>::remove( Index index ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		--count;
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return true;
	} else
		return false;
}

template <typename T> T Indexer <T>::extract( Index index ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		--count;
		T temp( *((T*)e->t) ); // I believe if we don't cast e->t to const, it has the potential to use the move constructor... maybe
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return temp;
	} else
		throw std::runtime_error( "Invalid index" );
}

template <typename T> bool Indexer <T>::extract( Index index, T & t ) {
	Entry * e = (Entry*)(alignedEntries + index);
	if (index < count + freeEntries && e->used()) {
		--count;
		t = std::move( *((T*)e->t) );
		((T*)e->t)->~T();

		size_t nextUsed = e->getNextUsedEntry();
		if (e->prevEntry == NO_INDEX)
			firstUsedEntry = nextUsed;
		else
			((Entry*)(alignedEntries + e->prevEntry))->setNextUsedEntry( nextUsed );
		if (nextUsed != NO_INDEX)
			((Entry*)(alignedEntries + nextUsed))->prevEntry = e->prevEntry;

		e->setNextFreeEntry( firstFreeEntry );
		firstFreeEntry = index;
		++freeEntries;
		return true;
	} else
		return false;
}

template <typename T> void Indexer <T>::reserve( size_t newCapacity ) {
	if (newCapacity <= capacity)
		return;

	char * newEntries = new char[newCapacity * sizeof( AlignedEntry )];

	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * newEntry = new (newEntries + offset) Entry;
		Entry * entry = (Entry*)(entries + offset);
		newEntry->usedAndNextEntry = entry->usedAndNextEntry;
		newEntry->prevEntry = entry->prevEntry;
		if (entry->used()) {
			new (newEntry->t) T( std::move( *((T*)entry->t) ) );
			((T*)entry->t)->~T();
		}
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	if (entries != NULL)
		delete[] entries;
	entries = newEntries;
	capacity = newCapacity;
}

template <typename T> bool Indexer <T>::contains( Index index ) const {
	Entry * e = (Entry*)(alignedEntries + index);
	return (index < count + freeEntries && e->used());
}

template <typename T> void Indexer <T>::clear() {
	size_t offset = 0;
	size_t remaining = count + freeEntries;
	while (remaining > 0) {
		Entry * entry = (Entry*)(entries + offset);
		if (entry->used())
			((T*)entry->t)->~T();
		entry->~Entry();
		offset += sizeof( AlignedEntry );
		--remaining;
	}

	count = 0;
	freeEntries = 0;
	firstUsedEntry = NO_INDEX;
}

template <typename T> size_t Indexer <T>::size() const {
	return count;
}

template <typename T> bool Indexer <T>::empty() const {
	return (count == 0);
}

template <typename T> typename Indexer <T>::Iterator Indexer <T>::getIterator() {
	return Iterator( this );
}

template <typename T> typename Indexer <T>::ConstIterator Indexer <T>::getConstIterator() const {
	return ConstIterator( this );
}

template <typename T> size_t Indexer <T>::memory() const {
	return sizeof( Entry ) * capacity;
}

#endif