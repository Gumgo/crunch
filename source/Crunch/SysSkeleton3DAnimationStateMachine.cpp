#include "SysSkeleton3DAnimationStateMachine.h"

SysSkeleton3DAnimationStateMachine::SysSkeleton3DAnimationStateMachine() {
	initialized = false;
}

bool SysSkeleton3DAnimationStateMachine::init(
	const Skeleton3DAnimationStateMachineResourceReference & animStateMachine,
	const SysSkeleton3DAnimationTree & animTree ) {
	initialized = false;
	if (!animTree.isInitialized())
		return false;

	animationStateMachine = animStateMachine;
	animationTree = &animTree;

	// resolve node names to indices
	commandNodeIndices.clear();
	commandNodeIndices.reserve( animationStateMachine->getCommandCount() );
	for (size_t i = 0; i < animationStateMachine->getCommandCount(); ++i) {
		const Skeleton3DAnimationStateMachineResource::Command & command = animationStateMachine->getCommand( i );
		Skeleton3DAnimationStateMachineResource::Command::Type commandType = command.getType();
		if (commandType == Skeleton3DAnimationStateMachineResource::Command::T_STATE_SET_ACTIVE ||
			commandType == Skeleton3DAnimationStateMachineResource::Command::T_EVENT_TRIGGER) {
			// these commands don't use the node indices - just fill with a meaningless value
			commandNodeIndices.push_back( 0xffffffff );
			continue;
		}

		// get the name of the node that the command is referring to
		const std::string & nodeName = command.getNodeName();
		// resolve that name to an index
		size_t nodeIndex;
		if (!animationTree->getAnimationTree()->getNodeIndex( nodeName, nodeIndex ))
			return false;
		commandNodeIndices.push_back( nodeIndex );

		// make sure that the command type corresponds to the node type
		Skeleton3DAnimationTreeResource::Node::Type nodeType = animationTree->getAnimationTree()->getNode( nodeIndex ).getType();
		switch (commandType) {
		case Skeleton3DAnimationStateMachineResource::Command::T_ANIMATION_PLAY:
		case Skeleton3DAnimationStateMachineResource::Command::T_ANIMATION_SET_TIME:
			if (nodeType != Skeleton3DAnimationTreeResource::Node::T_ANIMATION)
				return false;
			break;
		case Skeleton3DAnimationStateMachineResource::Command::T_SELECT_SET_INDEX:
			if (nodeType != Skeleton3DAnimationTreeResource::Node::T_SELECT)
				return false;
			break;
		case Skeleton3DAnimationStateMachineResource::Command::T_BLEND_TRANSITION_WEIGHT:
		case Skeleton3DAnimationStateMachineResource::Command::T_BLEND_SET_WEIGHT:
			if (nodeType != Skeleton3DAnimationTreeResource::Node::T_BLEND)
				return false;
			break;
		case Skeleton3DAnimationStateMachineResource::Command::T_SEQUENCE_TRANSITION_WEIGHT:
		case Skeleton3DAnimationStateMachineResource::Command::T_SEQUENCE_SET_WEIGHT:
			if (nodeType != Skeleton3DAnimationTreeResource::Node::T_SEQUENCE)
				return false;
			break;
		default:
			assert( false );
		}
	}

	initialized = true;
	return true;
}

bool SysSkeleton3DAnimationStateMachine::isInitialized() const {
	return initialized;
}

const Skeleton3DAnimationStateMachineResourceReference & SysSkeleton3DAnimationStateMachine::getAnimationStateMachine() const {
	return animationStateMachine;
}

const SysSkeleton3DAnimationTree * SysSkeleton3DAnimationStateMachine::getAnimationTree() const {
	return animationTree;
}

size_t SysSkeleton3DAnimationStateMachine::getCommandNodeIndex( size_t commandIndex ) const {
	return commandNodeIndices[commandIndex];
}

SysSkeleton3DAnimationStateMachine::State::State() {
	stateMachine = NULL;
}

SysSkeleton3DAnimationStateMachine::State::State( const SysSkeleton3DAnimationStateMachine & sm )
	: stateMachine( &sm ) {
	// create a tree state
	treeState = stateMachine->getAnimationTree()->getState();

	// initially all states are deactivated
	size_t stateBits = stateMachine->getAnimationStateMachine()->getStateCount();
	activeStates.resize( (stateBits + sizeof( activeStates[0] )*8 - 1) / (sizeof( activeStates[0] )*8), 0 );

	// initialize all variable bindings to NULL
	VariableBinding nullBinding;
	nullBinding.floatBinding = NULL;
	variableBindings.resize( stateMachine->getAnimationStateMachine()->getVariableCount(), nullBinding );
}

const SysSkeleton3DAnimationStateMachine * SysSkeleton3DAnimationStateMachine::State::getAnimationStateMachine() const {
	return stateMachine;
}

SysSkeleton3DAnimationTree::State & SysSkeleton3DAnimationStateMachine::State::getAnimationTreeState() {
	return treeState;
}

const SysSkeleton3DAnimationTree::State & SysSkeleton3DAnimationStateMachine::State::getAnimationTreeState() const {
	return treeState;
}

void SysSkeleton3DAnimationStateMachine::State::bindVariable( size_t index, const float * location ) {
	if (index >= variableBindings.size())
		throw std::runtime_error( "Variable index out of range" );
	if (stateMachine->getAnimationStateMachine()->getVariableType( index ) != Skeleton3DAnimationStateMachineResource::VT_FLOAT)
		throw std::runtime_error( "Variable is not an unsigned int" );
	variableBindings[index].floatBinding = location;
}

void SysSkeleton3DAnimationStateMachine::State::bindVariable( const std::string & name, const float * location ) {
	size_t index;
	if (!stateMachine->getAnimationStateMachine()->getVariableIndex( name, index ))
		throw std::runtime_error( "Invalid variable " + name );
	return bindVariable( index, location );
}

void SysSkeleton3DAnimationStateMachine::State::bindVariable( size_t index, const uint * location ) {
	if (index >= variableBindings.size())
		throw std::runtime_error( "Variable index out of range" );
	if (stateMachine->getAnimationStateMachine()->getVariableType( index ) != Skeleton3DAnimationStateMachineResource::VT_UINT)
		throw std::runtime_error( "Variable is not an unsigned int" );
	variableBindings[index].uintBinding = location;
}

void SysSkeleton3DAnimationStateMachine::State::bindVariable( const std::string & name, const uint * location ) {
	size_t index;
	if (!stateMachine->getAnimationStateMachine()->getVariableIndex( name, index ))
		throw std::runtime_error( "Invalid variable " + name );
	return bindVariable( index, location );
}

void SysSkeleton3DAnimationStateMachine::State::setStateActive( size_t index, bool a ) {
	if (index >= stateMachine->getAnimationStateMachine()->getStateCount())
		throw std::runtime_error( "State index out of range" );
	size_t idx = index / (sizeof( activeStates[0] ) * 8);
	size_t bit = index % (sizeof( activeStates[0] ) * 8);
	if (a)
		activeStates[idx] |= (1 << bit);
	else
		activeStates[idx] &= ~(1 << bit);
}

void SysSkeleton3DAnimationStateMachine::State::setStateActive( const std::string & name, bool a ) {
	size_t index;
	if (!stateMachine->getAnimationStateMachine()->getStateIndex( name, index ))
		throw std::runtime_error( "Invalid state " + name );
	return setStateActive( index, a );
}

bool SysSkeleton3DAnimationStateMachine::State::isStateActive( size_t index ) const {
	if (index >= stateMachine->getAnimationStateMachine()->getStateCount())
		return false;
	else {
		size_t idx = index / (sizeof( activeStates[0] ) * 8);
		size_t bit = index % (sizeof( activeStates[0] ) * 8);
		return (activeStates[idx] & (1 << bit)) != 0;
	}
}

bool SysSkeleton3DAnimationStateMachine::State::isStateActive( const std::string & name ) const {
	size_t index;
	if (!stateMachine->getAnimationStateMachine()->getStateIndex( name, index ))
		throw std::runtime_error( "Invalid state " + name );
	return isStateActive( index );
}

float SysSkeleton3DAnimationStateMachine::State::getValue( const Skeleton3DAnimationStateMachineResource::FloatValue & value ) const {
	switch (value.getType()) {
	case Skeleton3DAnimationStateMachineResource::FloatValue::T_CONSTANT:
		return value.getConstantValue();
	case Skeleton3DAnimationStateMachineResource::FloatValue::T_VARIABLE:
		return *variableBindings[value.getVariableIndex()].floatBinding;
	default:
		assert( false );
		return 0.0f;
	}
}

uint SysSkeleton3DAnimationStateMachine::State::getValue( const Skeleton3DAnimationStateMachineResource::UintValue & value ) const {
	switch (value.getType()) {
	case Skeleton3DAnimationStateMachineResource::FloatValue::T_CONSTANT:
		return value.getConstantValue();
	case Skeleton3DAnimationStateMachineResource::FloatValue::T_VARIABLE:
		return *variableBindings[value.getVariableIndex()].uintBinding;
	default:
		assert( false );
		return 0;
	}
}

void SysSkeleton3DAnimationStateMachine::State::update() {
	for (size_t i = 0; i < activeStates.size(); ++i) {
		// check 32 states at once for activity
		uint bits32 = activeStates[i];
		if (bits32 != 0) {
			// check 8 states at once for activity
			for (size_t t = 0; t < sizeof( bits32 ); ++t) {
				byte bits8 = ((byte*)&bits32)[t];
				if (bits8 != 0) {
					for (size_t b = 0; b < 8; ++b) {
						if ((bits8 & (1 << b)) != 0)
							// update the state
							updateState( (i*sizeof( uint ) + t)*8 + b );
					}
				}
			}
		}
	}
}

void SysSkeleton3DAnimationStateMachine::State::updateState( size_t stateIndex ) {
	const Skeleton3DAnimationStateMachineResource::State & state =
		stateMachine->getAnimationStateMachine()->getState( stateIndex );

	size_t first = state.getFirstCommandIndex();
	size_t last = first + state.getCommandCount();
	for (size_t c = first; c < last; ++c)
		executeCommand( c );
}

void SysSkeleton3DAnimationStateMachine::State::triggerEvent( size_t index ) {
	const Skeleton3DAnimationStateMachineResource::Event & evt =
		stateMachine->getAnimationStateMachine()->getEvent( index );

	size_t first = evt.getFirstCommandIndex();
	size_t last = first + evt.getCommandCount();
	for (size_t c = first; c < last; ++c)
		executeCommand( c );
}

void SysSkeleton3DAnimationStateMachine::State::triggerEvent( const std::string & name ) {
	size_t index;
	if (!stateMachine->getAnimationStateMachine()->getEventIndex( name, index ))
		throw std::runtime_error( "Invalid event " + name );
	return triggerEvent( index );
}

void SysSkeleton3DAnimationStateMachine::State::executeCommand( size_t commandIndex ) {
	// get the command
	const Skeleton3DAnimationStateMachineResource::Command & command =
		stateMachine->getAnimationStateMachine()->getCommand( commandIndex );

	switch (command.getType()) {
	case Skeleton3DAnimationStateMachineResource::Command::T_ANIMATION_PLAY:
		{
			size_t treeNodeIndex = stateMachine->getCommandNodeIndex( commandIndex );
			const Skeleton3DAnimationStateMachineResource::AnimationPlayCommand & cmd = command.getAnimationPlayCommand();
			treeState.setAnimationNodePlaySettings(
				treeNodeIndex,
				getValue( cmd.getSpeed() ),
				cmd.getLoop() );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_ANIMATION_SET_TIME:
		{
			size_t treeNodeIndex = stateMachine->getCommandNodeIndex( commandIndex );
			const Skeleton3DAnimationStateMachineResource::AnimationSetTimeCommand & cmd = command.getAnimationSetTimeCommand();
			treeState.setAnimationNodeTime(
				treeNodeIndex,
				getValue( cmd.getTime() ) );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_SELECT_SET_INDEX:
		{
			size_t treeNodeIndex = stateMachine->getCommandNodeIndex( commandIndex );
			const Skeleton3DAnimationStateMachineResource::SelectSetIndexCommand & cmd = command.getSelectSetIndexCommand();
			treeState.setSelectNodeSelection(
				treeNodeIndex,
				getValue( cmd.getIndex() ) );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_BLEND_TRANSITION_WEIGHT:
		{
			size_t treeNodeIndex = stateMachine->getCommandNodeIndex( commandIndex );
			const Skeleton3DAnimationStateMachineResource::BlendTransitionWeightCommand & cmd = command.getBlendTransitionWeightCommand();
			treeState.setBlendNodeTransitionSettings(
				treeNodeIndex,
				getValue( cmd.getWeight() ),
				getValue( cmd.getSpeed() ) );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_BLEND_SET_WEIGHT:
		{
			size_t treeNodeIndex = stateMachine->getCommandNodeIndex( commandIndex );
			const Skeleton3DAnimationStateMachineResource::BlendSetWeightCommand & cmd = command.getBlendSetWeightCommand();
			treeState.setBlendNodeWeight(
				treeNodeIndex,
				getValue( cmd.getWeight() ) );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_SEQUENCE_TRANSITION_WEIGHT:
		{
			size_t treeNodeIndex = stateMachine->getCommandNodeIndex( commandIndex );
			const Skeleton3DAnimationStateMachineResource::SequenceTransitionWeightCommand & cmd = command.getSequenceTransitionWeightCommand();
			treeState.setSequenceNodeTransitionSettings(
				treeNodeIndex,
				cmd.getIndex(),
				getValue( cmd.getWeight() ),
				getValue( cmd.getSpeed() ) );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_SEQUENCE_SET_WEIGHT:
		{
			size_t treeNodeIndex = stateMachine->getCommandNodeIndex( commandIndex );
			const Skeleton3DAnimationStateMachineResource::SequenceSetWeightCommand & cmd = command.getSequenceSetWeightCommand();
			treeState.setSequenceNodeWeight(
				treeNodeIndex,
				cmd.getIndex(),
				getValue( cmd.getWeight() ) );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_STATE_SET_ACTIVE:
		{
			const Skeleton3DAnimationStateMachineResource::StateSetActiveCommand & cmd = command.getStateSetActiveCommand();
			setStateActive( cmd.getStateIndex(), cmd.getActive() );
		}
		break;
	case Skeleton3DAnimationStateMachineResource::Command::T_EVENT_TRIGGER:
		{
			const Skeleton3DAnimationStateMachineResource::EventTriggerCommand & cmd = command.getEventTriggerCommand();
			triggerEvent( cmd.getEventIndex() );
		}
		break;
	default:
		assert( false );
	}
}

SysSkeleton3DAnimationStateMachine::State SysSkeleton3DAnimationStateMachine::getState() const {
	return State( *this );
}
