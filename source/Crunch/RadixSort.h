/** @file RadixSort.h
 *  @brief Contains radix sorting functions.
 */

#ifndef RADIXSORT_DEFINED
#define RADIXSORT_DEFINED

#include "Common.h"
#include <vector>

/** @brief Used for determining the bucket when sorting by unsigned integer keys.
 *
 *  @tparam K	The key type, which should be an unsigned integer type.
 */
template <typename K> struct GetUintBucket {
	/** brief Returns the appropriate bucket for byte r of the value k.
	 */
	byte operator()( const K & k, size_t r ) const {
		return ((byte*)(&k))[r];
	}
};

/** @brief Used for determining the bucket when sorting by signed integer keys.
 *
 *  @tparam K	The key type, which should be a signed integer type.
 */
template <typename K> struct GetIntBucket {
	/** brief Returns the appropriate bucket for byte r of the value k.
	 */
	byte operator()( const K & k, size_t r ) const {
		// if last byte, flip sign bit
		return (r == sizeof( K ) - 1)
			? (((byte*)(&k))[r]) ^ 0x80;
			: ((byte*)(&k))[r];
	}
};

/** @brief Used for determining the bucket when sorting by floating point keys.
 *
 *  @tparam K	The key type, which should be a floating point type.
 */
template <typename K> struct GetFloatBucket {
	/** brief Returns the appropriate bucket for byte r of the value k.
	 */
	byte operator()( const K & k, size_t r ) const {
		byte val = ((byte*)(&k))[r];

		if (((((byte*)(&k))[sizeof( K ) - 1]) & 0x80) == 0x80)
			// if the key is negative (including -0.0), flip all the bits
			val ^= 0xff;
		else if (r == sizeof( K ) - 1)
			// otherwise, if positive, and the last byte, just flip the last bit
			val ^= 0x80;

		return val;
	}
};

/** @brief A bucket entry used in radix sorting.
 */
template <typename V> struct RadixBucketEntry {
	V value;		/**< The value stored.*/
	size_t next;	/**< The index of the next bucket entry.*/
	RadixBucketEntry( const V & v )	/**< The constructor.*/
		: value( v ) {
	}
};

/** @brief Sorts an array using a radix.
 *
 *  @tparam K			The key type (i.e. the value to sort by).
 *  @tparam V			The value type (i.e. the elements in the array to sort).
 *  @tparam F			A lambda or function object that returns an instance of K given an instance of V.
 *  @tparam B			A lambda or function object that returns the appropriate bucket for byte r of a key k.
 *  @param arr			A pointer to the array to sort.
 *  @param size			The number of elements to sort.
 *  @param getKey		Used to obtain a key corresponding to each value in the array.
 *  @param getBucket	Used to obtain the appropriate bucket for byte r of a key k.
 *  @param buffer		An optional preallocated buffer of bucket entries of type V.
 */
template <typename K, typename V, typename F, typename B> void radixSort( V * arr, size_t size, const F & getKey, const B & getBucket,
	std::vector <RadixBucketEntry <V>> & buffer = std::vector <RadixBucketEntry <V>>() ) {
	if (size <= 0)
		return;

	struct Bucket {
		size_t start;
		size_t end;
	};
	// if start > end, the bucket is empty

	Bucket buckets[256];
	buffer.reserve( size );

	// for each radix (byte)
	for (size_t r = 0; r < sizeof( K ); ++r) {
		for (int i = 0; i < 256; ++i) {
			buckets[i].start = 1;
			buckets[i].end = 0;
		}
		buffer.clear();

		for (size_t i = 0; i < size; ++i) {
			// get the key associated with that value
			K key = getKey( arr[i] );

			byte val = getBucket( key, r );

			// put the value into the appropriate bucket
			if (buckets[val].start > buckets[val].end)
				buckets[val].start = buckets[val].end = buffer.size();
			else {
				buffer[buckets[val].end].next = buffer.size();
				buckets[val].end = buffer.size();
			}
			buffer.push_back( RadixBucketEntry <V>( arr[i] ) );
		}

		size_t place = 0;
		for (int i = 0; i < 256; ++i) {
			// if start > end, empty, so skip
			if (buckets[i].start <= buckets[i].end) {
				// pop the buckets in order
				size_t index = buckets[i].start;
				arr[place] = buffer[index].value;
				++place;
				while (index != buckets[i].end) {
					index = buffer[index].next;
					arr[place] = buffer[index].value;
					++place;
				}
			}
		}
	}
}

/** @brief Sorts an array using a radix sort for unsigned integer types.
 *
 *  @tparam K		The key type (i.e. the value to sort by).
 *  @tparam V		The value type (i.e. the elements in the array to sort).
 *  @tparam F		A lambda or function object that returns an instance of K given an instance of V.
 *  @param arr		A pointer to the array to sort.
 *  @param size		The number of elements to sort.
 *  @param getKey	Used to obtain a key corresponding to each value in the array.
 *  @param buffer	An optional preallocated buffer of bucket entries of type V.
 */
template <typename K, typename V, typename F> void radixSortUint( V * arr, size_t size, const F & getKey,
	std::vector <RadixBucketEntry <V>> & buffer = std::vector <RadixBucketEntry <V>>() ) {
	radixSort <K, V, F, GetUintBucket <K>>( arr, size, getKey, GetUintBucket <K>(), buffer );
}

/** @brief Sorts an array using a radix sort for signed integer types.
 *
 *  @tparam K		The key type (i.e. the value to sort by).
 *  @tparam V		The value type (i.e. the elements in the array to sort).
 *  @tparam F		A lambda or function object that returns an instance of K given an instance of V.
 *  @param arr		A pointer to the array to sort.
 *  @param size		The number of elements to sort.
 *  @param getKey	Used to obtain a key corresponding to each value in the array.
 *  @param buffer	An optional preallocated buffer of bucket entries of type V.
 */
template <typename K, typename V, typename F> void radixSortInt( V * arr, size_t size, const F & getKey,
	std::vector <RadixBucketEntry <V>> & buffer = std::vector <RadixBucketEntry <V>>() ) {
	radixSort <K, V, F, GetIntBucket <K>>( arr, size, getKey, GetIntBucket <K>(), buffer );
}

/** @brief Sorts an array using a radix sort for floating point types.
 *
 *  @tparam K		The key type (i.e. the value to sort by).
 *  @tparam V		The value type (i.e. the elements in the array to sort).
 *  @tparam F		A lambda or function object that returns an instance of K given an instance of V.
 *  @param arr		A pointer to the array to sort.
 *  @param size		The number of elements to sort.
 *  @param getKey	Used to obtain a key corresponding to each value in the array.
 *  @param buffer	An optional preallocated buffer of bucket entries of type V.
 */
template <typename K, typename V, typename F> void radixSortFloat( V * arr, size_t size, const F & getKey,
	std::vector <RadixBucketEntry <V>> & buffer = std::vector <RadixBucketEntry <V>>() ) {
	radixSort <K, V, F, GetFloatBucket <K>>( arr, size, getKey, GetFloatBucket <K>(), buffer );
}

#endif