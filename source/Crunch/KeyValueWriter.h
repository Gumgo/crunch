/** @file KeyValueWriter.h
 *  @brief Provides functionality to read key/value pairs.
 */

#ifndef KEYVALUEWRITER_DEFINED
#define KEYVALUEWRITER_DEFINED

#include "Common.h"
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <map>
#include <vector>
#include <stack>

/** @brief Provides functionality to write key/value pairs.
 *
 *  KeyValueWriter will write out specified key/value
 *  pairs to a file. Each pair is written in the following
 *  format:
 *
 *  key = value
 *
 *  Anonymous (unnamed) values may also be specified which are
 *  written as follows:
 *
 *  *value
 *
 *  The range of possible values includes integers, unsigned
 *  integers, booleans, floats, strings, binary data, and arrays.
 *  Arrays consist of sub-groupings of key/value pairs. Arrays
 *  may be nested.
 */
class KeyValueWriter {
public:
	class Iterator;
	friend class Iterator;

	/** @brief Indicates the value type of a key/value pair.
	 */
	enum ValueType {
		VAL_INT,	/**< A value of type int.*/
		VAL_UINT,	/**< A value of type unsigned int.*/
		VAL_FLOAT,	/**< A value of type float.*/
		VAL_BOOL,	/**< A value of type bool.*/
		VAL_STRING,	/**< A value of type string.*/
		VAL_BINARY, /**< A value consisting of binary data.*/
		VAL_ARRAY	/**< A value consisting of an array of values.*/
	};

private:
	// declare private before public because the Iterator must access these

	// the value associated with a key
	struct Value {
		ValueType type;		// the type of value
		std::string key;	// the key associated with this value

		// a union is used for primitive types
		union {
			int intValue;			// the int value, if an int
			uint uintValue;			// the uint value, if a uint
			float floatValue;		// the float value, if a float
			bool boolValue;			// the bool value, if a bool
		};
		std::string stringValue;		// the string value, if a string
		std::vector <byte> binaryValue;	// binary data, if binary

		std::vector <Value> arrayValue;				// the array of values, if an array
		std::map <std::string, size_t> arrayMap;	// maps keys to indices in the array of values, if an array

		Value();
		Value( Value && other );				// move constructor for efficient resizing of parent vector
		Value & operator=( Value && other );	// move assignment operator for efficient resizing of parent vector
	};

	// the root array of the file
	Value root;

	// throws an exception if a key is invalid
	static void checkKey( const std::string & key );

	// returns an escaped version of the given string
	static std::string getEscapedString( const std::string & str );

public:
	/** @brief The constructor.
	 */
	KeyValueWriter();

	/** @brief Returns an iterator used to write the root level key/value pairs.
	 */
	Iterator getIterator();

	/** @brief Writes the specified key/value pairs to the given file.
	 */
	void writeToFile( const std::string & fname ) const;

	/** @brief Used to write the key/value pairs to the key/value file.
	 */
	class Iterator {
	public:
		Iterator( const Iterator & it );				/**< Copy constructor.*/
		Iterator & operator=( const Iterator & it );	/**< Assignment operator.*/

		void setInt( const std::string & key, int value );	/**< Writes an integer value with the given key.*/
		void setInt( int value );							/**< Writes an anonymous integer value.*/

		void setUint( const std::string & key, uint value );	/**< Writes an unsigned integer value with the given key.*/
		void setUint( uint value );								/**< Writes an anonymous unsigned integer value.*/

		void setFloat( const std::string & key, float value );	/**< Writes an float value with the given key.*/
		void setFloat( float value );							/**< Writes an anonymous float value.*/

		void setBool( const std::string & key, bool value );	/**< Writes an boolean value with the given key.*/
		void setBool( bool value );								/**< Writes an anonymous boolean value.*/

		void setString( const std::string & key, const std::string & value );	/**< Writes an string value with the given key.*/
		void setString( const std::string & value );							/**< Writes an anonymous string value.*/

		void setBinaryData( const std::string & key, const byte * value, size_t length );	/**< Writes the binary data with the given key.*/
		void setBinaryData( const byte * value, size_t length );							/**< Writes anonymous binary data.*/

		Iterator setArray( const std::string & key );	/**< Writes an array with the given key and returns an iterator to it.*/
		Iterator setArray();							/**< Writes an anonymous array and returns an iterator to it.*/

	private:
		friend class KeyValueWriter;

		// a reference to the current value being written
		KeyValueWriter::Value * source;

		// private constructor for creation by KeyValueWriter or Iterator only
		Iterator( KeyValueWriter::Value & src );

		// adds the given key and an empty value, throwing an exception if the given key is invalid or already exists
		void addKeyValue( const std::string & key );
	};
};

#endif