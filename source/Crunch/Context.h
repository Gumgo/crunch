/** @file Context.h
 *  @brief Provides a global context for the game.
 */

#ifndef CONTEXT_DEFINED
#define CONTEXT_DEFINED

#include "Common.h"
#include "HashMap.h"
#include "HashFunctions.h"

#include "ResourceManager.h"
#include "InputManager.h"
#include "Window.h"
#include "GameStateManager.h"
#include "GpuState.h"
#include "Renderer.h"
#include "AudioManager.h"
//#include "NetworkManager.h"
#include "TaskQueue.h"
#include "Timer.h"

#include "Registration.h"

#include "FastDelegate.h"

/** @brief The context of the game.
 */
class Context : private boost::noncopyable {
private:
	// used to initialize context pointer before everything else
	struct ContextInitializer {
		ContextInitializer( Context * context );
	} contextInitializer;

public:
	/** @brief Encapsulates the state to initially create.
	 */
	class StateInitializer {
	public:
		/** @brief Creates a state initializer.
		 */
		template <typename T> static StateInitializer create();

	private:
		friend class Context;
		typedef fastdelegate::FastDelegate1 <Context &> StateCreator;
		StateCreator stateCreator;

		StateInitializer();
		template <typename T> void setInitialState();
		template <typename T> static void createInitialState( Context & context );
	};

	/** @brief The registration function type.
	 */
	typedef fastdelegate::FastDelegate0 <StateInitializer> RegistrationFunction;

	/** @brief Settings for the context.
	 */
	struct Settings {
		bool server;								/**< Whether this is a server instance.*/
		std::string resourcePathFile;				/**< The path of the file containing resource names and paths.*/
		std::vector <std::string> args;				/**< The command line arguments.*/
		RegistrationFunction registrationFunction;	/**< The registration function.*/
		uint taskQueueThreads;						/**< The number of task queue threads.*/
	};

	/** @brief Initializes the context.
	 */
	Context( const Settings & s );
	static Context & get();	/**< Returns a reference to the context.*/

	bool isServer() const;	/**< Returns whether the application is in server mode.*/

	std::string getArgument( size_t i ) const;	/**< Returns a command line argument or an empty string if it wasn't provided.*/

	void createInitialState();	/**< Creates the initial state.*/

	void signalQuit();			/**< Signals to quit the game.*/
	bool getQuitSignal() const;	/**< Returns whether quit has been signaled.*/

	// order is important - window destructor (i.e. OpenGL context) must be called AFTER gpuState and resources
	Timer timer;						/**< The timer.*/
	Window window;						/**< The window.*/
	ResourceManager resourceManager;	/**< The resource manager.*/
	InputManager inputManager;			/**< The input manager.*/
	GpuState gpuState;					/**< The GPU state.*/
	Renderer renderer;					/**< The renderer.*/
	AudioManager audioManager;			/**< The audio manager.*/
	GameStateManager gameStateManager;	/**< The game state manager.*/
	//NetworkManager networkManager;		/**< The network manager.*/
	TaskQueue taskQueue;				/**< The task queue.*/

private:
	static Context * context; // static pointer to the context

	bool server; // whether we should run in server mode (no GUI)
	std::vector <std::string> arguments; // the command line args
	StateInitializer stateInitializer; // used to create initial state
	bool quitSignaled; // whether quit has been signaled
};

template <typename T> Context::StateInitializer Context::StateInitializer::create() {
	StateInitializer stateInitializer;
	stateInitializer.setInitialState <T>();
	return stateInitializer;
}

template <typename T> void Context::StateInitializer::setInitialState() {
	stateCreator = StateCreator( &Context::StateInitializer::createInitialState <T> );
}

template <typename T> void Context::StateInitializer::createInitialState( Context & context ) {
	context.gameStateManager.createGameState <T>();
}

#endif