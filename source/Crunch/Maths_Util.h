/** @file Maths_Util.h
 *  @brief Contains some general-purpose math-related functions.
 */

#ifndef MATHS_UTIL_DEFINED
#define MATHS_UTIL_DEFINED

#include "Maths_Includes.h"

/** @brief Returns val clamped to the range [vmin, vmax].
 */
template <typename T> T clamp( T val, T vmin, T vmax ) {
	if (val > vmax)
		val = vmax;
	if (val < vmin)
		val = vmin;
	return val;
}

/** @brief Returns the sign of val.
 */
template <typename T> T sign( T val ) {
	if (val < 0)
		return -1;
	return (val > 0);
}

/** @brief Interpolates between a and b by amount c using linear interpolation.
 */
template <typename T, typename S> T lerp( T a, T b, S c ) {
	return a + (b-a)*c;
}

/** @brief Converts degrees to radians.
 */
template <typename T> T degToRad( T deg ) {
	return deg * (boost::math::constants::pi <T>() / (T)180);
}

/** @brief Converts radians to degrees.
 */
template <typename T> T radToDeg( T rad ) {
	return rad * ((T)180 / boost::math::constants::pi <T>());
}

/** @brief Returns an epsilon value.
 */
template <typename T> T epsilon() {
	return (T)0.0001;
}

#endif