/** @file Physics2DShape.h
 *  @brief Defines the shape of bodies.
 */
#ifndef PHYSICS2DSHAPE_DEFINED
#define PHYSICS2DSHAPE_DEFINED

#include "Common.h"
#include "Physics2D.h"

namespace Physics2D {

class Simulation;
class Body;
struct ShapeDefinition;
class Contact;

/** @brief Defines the shape of bodies.
 */
class Shape : private boost::noncopyable {
public:
	friend class Body;
	friend class Simulation;
	friend class ContactManager;

	static const Real AABB_EXPANSION;

	/** @brief The type of shape.
	 */
	enum Type {
		T_CIRCLE,			/**< A circle.*/
		T_ELLIPSE,			/**< An ellipse.*/
		T_CAPSULE,			/**< A capsule.*/
		T_CONVEX_POLYGON,	/**< A convex polygon.*/
		T_EDGE,				/**< An edge.*/
		TYPE_COUNT			/**< The number of types.*/
	};

	Shape( const ShapeDefinition & d, Type t, Body * par );	/**< The constructor.*/
	virtual ~Shape();										/**< The destructor.*/

	Type getType() const;		/**< Returns the type of shape.*/
	Body * getParent() const;	/**< Returns the parent body.*/

	/** @brief Returns the bounding box of the transformed shape.
	 */
	virtual Aabb getAabb( const Matrix33ar & transformation ) const = 0;

	/** @brief Computes the mass data of the shape.
	 */
	virtual void computeMassData() = 0;

	Real getArea() const;				/**< Returns the area of this shape.*/			

	void setFriction( Real fr );		/**< Sets the friction of this shape.*/
	Real getFriction() const;			/**< Returns the friction of this shape.*/

	void setRestitution( Real rest );	/**< Sets the coefficent of restitution of this shape.*/
	Real getRestitution() const;		/**< Returns the coefficent of restitution of this shape.*/

	void setDensity( Real d );			/**< Sets the density.*/
	Real getDensity() const;			/**< Returns the density.*/

	void overrideCenterOfMass( const Vector2r & center );	/**< Overrides the center of mass.*/
	void resetCenterOfMass();								/**< Resets the center of mass.*/
	bool isCenterOfMassOverridden() const;					/**< Returns whether the center of mass is overridden.*/
	Vector2r getOverriddenCenterOfMass() const;				/**< Returns the overridden center of mass.*/

	MassData getMassData() const;							/**< Returns the mass data after computing the mass data.*/

	void setTransform( const Vector2r & pos, Real rot );	/**< Sets the shape's transform.*/
	Matrix33ar getTransform() const;						/**< Returns the shape's transform.*/

	void addContact( Contact * c );								/**< Adds a contact to the contact list.*/
	void removeContact( Contact * c );							/**< Removes a contact from the contact list.*/
	Contact * getContacts();									/**< Returns the contact list.*/
	const Contact * getContacts() const;						/**< Returns the contact list.*/
	Contact * getNextContact( Contact * c );					/**< Returns the next contact in the list.*/
	const Contact * getNextContact( const Contact * c ) const;	/**< Returns the next contact in the list.*/

	bool shouldCollide( const Shape * s ) const;	/**< Returns whether the two shapes should collide.*/

	// for computing moments of inertia, use the parallel axis theorem:
	// the moment of inertia about any new axis z is: I_z = I_cm + mr^2
	// where I_cm is the moment of inertia about axis passing through center of mass, and
	// r is the perpendicular distance between the axes I_z and I_cm
	// this requires that I_z and I_cm are parallel

protected:
	void setArea( Real a );						/**< Sets the area of this shape.*/
	void setMassData( const MassData & md );	/**< Sets the computed mass data.*/

private:
	Type type;						// type of shape

	Body * parent;					// parent body
	Real area;						// area of this shape
	Real friction;					// friction coefficient of this shape
	Real restitution;				// coefficient of restitution of this shape
	Real density;					// shape density
	bool centerOfMassOverridden;	// whether the center of mass is overridden
	Vector2r centerOfMass;			// the overridden center of mass
	Matrix33ar transform;			// shape's transform, applied on top of center of mass

	MassData computedMassData;		// the computed mass data

	Contact * contacts;				// the contact list for this shape

	// data for the parent body
	struct BodyData {
		Shape * next;				// next shape in shape list
		size_t spatialHashIndex;	// index in the spatial hash map
	};

	BodyData bodyData;
	BodyData & getBodyData();
	const BodyData & getBodyData() const;
};

/** @brief The base class for shape definitions.
 */
struct ShapeDefinition {
	Real friction;						/**< The friction coefficient.*/
	Real restitution;					/**< The coefficient of restitution.*/

	Real density;						/**< The shape's density.*/
	bool overrideCenterOfMass;			/**< Whether to override the center of mass.*/
	Vector2r overriddenCenterOfMass;	/**< The overridden center of mass.*/

	Vector2r position;					/**< The shape's position.*/
	Real rotation;						/**< The shape's rotation.*/

	ShapeDefinition();										/**< Assigns default values.*/
	virtual Shape * createShape( Body * parent ) const = 0;	/**< Creates the derived shape from the definition.*/
	virtual ~ShapeDefinition();								/**< The virtual destructor.*/
};

}

#endif