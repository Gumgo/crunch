#include "GpuRasterizerState.h"
#include "GpuState.h"

GpuRasterizerState::GpuRasterizerState()
	: GpuObject( NULL ) {
}

void GpuRasterizerState::bind() {
	getGpuState()->bind( *this );
}

void GpuRasterizerState::setPointSize( float size ) {
	pointSize = size;
}

void GpuRasterizerState::setLineWidth( float width ) {
	lineWidth = width;
}

void GpuRasterizerState::setPolygonMode( GLenum mode ) {
	polygonModeFront = mode;
	polygonModeBack = mode;
}

void GpuRasterizerState::setPolygonModes( GLenum modeFront, GLenum modeBack ) {
	polygonModeFront = modeFront;
	polygonModeBack = modeBack;
}

void GpuRasterizerState::setCullingEnabled( bool e ) {
	cullingEnabled = e;
}

void GpuRasterizerState::setCullMode( GLenum mode ) {
	cullMode = mode;
}

void GpuRasterizerState::setFrontFaceOrientation( GLenum orientation ) {
	frontFaceOrientation = orientation;
}

void GpuRasterizerState::setPolygonOffsetFillEnabled( bool e ) {
	polygonOffsetFillEnabled = e;
}

void GpuRasterizerState::setPolygonOffsetLineEnabled( bool e ) {
	polygonOffsetLineEnabled = e;
}

void GpuRasterizerState::setPolygonOffsetPointEnabled( bool e ) {
	polygonOffsetPointEnabled = e;
}

void GpuRasterizerState::setPolygonOffset( float factor, float units ) {
	polygonOffsetFactor = factor;
	polygonOffsetUnits = units;
}

float GpuRasterizerState::getPointSize() const {
	return pointSize;
}

float GpuRasterizerState::getLineWidth() const {
	return lineWidth;
}

GLenum GpuRasterizerState::getPolygonModeFront() const {
	return polygonModeFront;
}

GLenum GpuRasterizerState::getPolygonModeBack() const {
	return polygonModeBack;
}

bool GpuRasterizerState::isCullingEnabled() const {
	return cullingEnabled;
}

GLenum GpuRasterizerState::getCullMode() const {
	return cullMode;
}

GLenum GpuRasterizerState::getFrontFaceOrientation() const {
	return frontFaceOrientation;
}

bool GpuRasterizerState::isPolygonOffsetFillEnabled() const {
	return polygonOffsetFillEnabled;
}

bool GpuRasterizerState::isPolygonOffsetLineEnabled() const {
	return polygonOffsetLineEnabled;
}

bool GpuRasterizerState::isPolygonOffsetPointEnabled() const {
	return polygonOffsetPointEnabled;
}

float GpuRasterizerState::getPolygonOffsetFactor() const {
	return polygonOffsetFactor;
}

float GpuRasterizerState::getPolygonOffsetUnits() const {
	return polygonOffsetUnits;
}

GpuRasterizerState::GpuRasterizerState( GpuState * state )
	: GpuObject( state ) {
	pointSize = 1.0f;
	lineWidth = 1.0f;
	polygonModeFront = GL_FILL;
	polygonModeBack = GL_FILL;

	cullingEnabled = false;
	cullMode = GL_BACK;
	frontFaceOrientation = GL_CCW;

	polygonOffsetFillEnabled = false;
	polygonOffsetLineEnabled = false;
	polygonOffsetPointEnabled = false;
	polygonOffsetFactor = 0.0f;
	polygonOffsetUnits = 0.0f;
}

void GpuRasterizerState::apply( GpuRasterizerState & currentState ) const {
	if (pointSize != currentState.pointSize) {
		glPointSize( pointSize );
		currentState.pointSize = pointSize;
	}

	if (lineWidth != currentState.lineWidth) {
		glLineWidth( lineWidth );
		currentState.lineWidth = lineWidth;
	}

	// BUG: polygon mode only works with GL_FRONT_AND_BACK

	if (polygonModeFront != currentState.polygonModeFront) {
//		glPolygonMode( GL_FRONT, polygonModeFront );
		glPolygonMode( GL_FRONT_AND_BACK, polygonModeFront );
		currentState.polygonModeFront = polygonModeFront;
	}

//	if (polygonModeBack != currentState.polygonModeBack) {
//		glPolygonMode( GL_BACK, polygonModeBack );
//		currentState.polygonModeBack = polygonModeBack;
//	}

	setStateEnabled( cullingEnabled, currentState.cullingEnabled, GL_CULL_FACE );

	if (cullingEnabled) {
		if (cullMode != currentState.cullMode) {
			glCullFace( cullMode );
			currentState.cullMode = cullMode;
		}
	}

	// front faces are used in other things than just culling, so set it even if culling isn't enabled
	if (frontFaceOrientation != currentState.frontFaceOrientation) {
		glFrontFace( frontFaceOrientation );
		currentState.frontFaceOrientation = frontFaceOrientation;
	}

	setStateEnabled( polygonOffsetFillEnabled, currentState.polygonOffsetFillEnabled, GL_POLYGON_OFFSET_FILL );
	setStateEnabled( polygonOffsetLineEnabled, currentState.polygonOffsetLineEnabled, GL_POLYGON_OFFSET_LINE );
	setStateEnabled( polygonOffsetPointEnabled, currentState.polygonOffsetPointEnabled, GL_POLYGON_OFFSET_POINT );

	if (polygonOffsetFillEnabled || polygonOffsetLineEnabled || polygonOffsetPointEnabled) {
		if (polygonOffsetFactor != currentState.polygonOffsetFactor ||
			polygonOffsetUnits != currentState.polygonOffsetUnits) {
			glPolygonOffset( polygonOffsetFactor, polygonOffsetUnits );
			currentState.polygonOffsetFactor = polygonOffsetFactor;
			currentState.polygonOffsetUnits = polygonOffsetUnits;
		}
	}
}

void GpuRasterizerState::getCurrentState() {
	glGetFloatv( GL_POINT_SIZE, &pointSize );
	glGetFloatv( GL_LINE_WIDTH, &lineWidth );
	// BUG: this doesn't work
	// spec says the initial value is GL_FILL though, so we just set that
	//GLint polygonModeResult[2];
	//glGetIntegerv( GL_POLYGON_MODE, polygonModeResult );
	//polygonModeFront = (GLenum)polygonModeResult[0];
	//polygonModeBack = (GLenum)polygonModeResult[1];
	polygonModeFront = GL_FILL;
	polygonModeBack = GL_FILL;

	cullingEnabled = glIsEnabled( GL_CULL_FACE ) != 0;
	glGetIntegerv( GL_CULL_FACE_MODE, (GLint*)&cullMode );
	glGetIntegerv( GL_FRONT_FACE, (GLint*)&frontFaceOrientation );

	polygonOffsetFillEnabled = glIsEnabled( GL_POLYGON_OFFSET_FILL ) != 0;
	polygonOffsetLineEnabled = glIsEnabled( GL_POLYGON_OFFSET_LINE ) != 0;
	polygonOffsetPointEnabled = glIsEnabled( GL_POLYGON_OFFSET_POINT ) != 0;
	glGetFloatv( GL_POLYGON_OFFSET_FACTOR, &polygonOffsetFactor );
	glGetFloatv( GL_POLYGON_OFFSET_UNITS, &polygonOffsetUnits );
}