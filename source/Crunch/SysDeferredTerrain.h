#ifndef SYSDEFERREDTERRAIN_DEFINED
#define SYSDEFERREDTERRAIN_DEFINED

#include "Common.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"
#include "SysTerrain.h"
#include "SysCamera3D.h"
#include "GpuTexture.h"

// draws terrain using deferred lighting renderer
class SysDeferredTerrain : private boost::noncopyable {
public:
	SysDeferredTerrain();
	~SysDeferredTerrain();
	void setCamera( const SysCamera3D * cam );
	void setTerrain( SysTerrain * t );

	void setMaterialTextures( GpuTextureReference & diffMaps, GpuTextureReference & normMaps );
	void setMaterialProperties( size_t m, uint scalePow2, const Vector3f & diffuseColor, const Vector3f & specularColor, float specularPower );

	void registerDrawCalls( const SysCamera3D * cam );

private:
	ProgramResourceReference fillGBufferProgram;
	ProgramResourceReference shadeProgram;

	SysAutoProgram fillGBufferAuto;
	SysAutoProgram shadeAuto;
	struct {
		size_t z;
		size_t normalM0;
		size_t normalM1;
		size_t normalM2;
		size_t xyPatchEnabled;
		size_t materials1;
		size_t materials2;
		size_t materials3;
		size_t materials4;
		size_t materials5;
	} fillGBufferAttribs;
	struct {
		size_t z;
		size_t xyPatchEnabled;
		size_t materials1;
		size_t materials2;
		size_t materials3;
		size_t materials4;
		size_t materials5;
	} shadeAttribs;
	struct {
		Matrix44af modelViewMatrix;
		Matrix44f mvpMatrix;
		float scale;
		Vector2f offset;
		float chunkScale;
		Vector2f center;
		Vector2f morphDistances1;
		Vector2f morphDistances2;
		int diffuseSpecMap;
		Vector2f screenSize;
		int materialsEnabled[SysTerrain::MAX_MATERIALS];
		float uvScales[SysTerrain::MAX_MATERIALS];
		float specularPowers[SysTerrain::MAX_MATERIALS];
		Vector3f diffuseColors[SysTerrain::MAX_MATERIALS];
		Vector3f specularColors[SysTerrain::MAX_MATERIALS];
		int normalMaps;
		int diffuseColorMaps;
	} uniforms;

	const SysCamera3D * camera;
	SysTerrain * terrain;

	GpuTextureReference diffuseColorMaps;
	GpuTextureReference normalMaps;

	void setGBufferProgram( size_t vertexBufferOffset, const SysTerrain::Chunk & chunk, const bool * materialsEnabled );
	void setShadeProgram( size_t vertexBufferOffset, const SysTerrain::Chunk & chunk, const bool * materialsEnabled );
	SysTerrain::ProgramBinder gBufferBinder;
	SysTerrain::ProgramBinder shadeBinder;

	void fillGBuffer();
	void shade();
};

#endif