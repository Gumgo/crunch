#if 0

#include "NetworkManager.h"
#include "Log.h"

NetworkManager::NetworkManager() {
	initialized = false;
	nextNetworkId = 0;
}

NetworkManager::~NetworkManager() {
	idLayer.term();
}

bool NetworkManager::init( ushort port ) {
	if (initialized) {
		Log::warning() << "Network manager already initialized";
		return false;
	}

	initialized = idLayer.init( port );
	return initialized;
}

void NetworkManager::term() {
	if (!initialized)
		return;

	idLayer.term();
	initialized = false;
	nextNetworkId = 0;
	recentlyRemovedNetworkIdSet.clear();
	while (!recentlyRemovedNetworkIdQueue.empty())
		recentlyRemovedNetworkIdQueue.pop();
}

void NetworkManager::enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn ) {
	idLayer.enableAcceptNewConnections( onAcceptNewConnectionFn );
}

void NetworkManager::disableAcceptNewConnections() {
	idLayer.disableAcceptNewConnections();
}

void NetworkManager::setOnConnectionTimeout( const boost::function <void ( size_t )> & onConnectionTimeoutFn ) {
	idLayer.setOnConnectionTimeout( onConnectionTimeoutFn );
}

bool NetworkManager::createConnection( const std::string & address, ushort port, size_t & newConnectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to create connection: network manager not initialized";
		return false;
	}

	return idLayer.createConnection( address, port, newConnectionId );
}

void NetworkManager::destroyConnection( size_t connectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to destroy connection: network manager not initialized";
		return;
	}

	// TODO: send pending for this client only before destroying
	idLayer.destroyConnection( connectionId );
}

void NetworkManager::receiveAndProcess() {
	if (!initialized) {
		Log::warning() << "Failed to receive and process messages: network manager not initialized";
		return;
	}

	idLayer.receiveAndProcess();
}

bool NetworkManager::queueMessage( size_t connectionId, ushort networkId, NetworkWriter & writer ) {
	if (!initialized) {
		Log::warning() << "Failed to queue message: network manager not initialized";
		return false;
	}

	return idLayer.queueMessage( connectionId, networkId, writer );
}

bool NetworkManager::queueGuaranteedMessage( size_t connectionId, ushort networkId, NetworkWriter & writer ) {
	if (!initialized) {
		Log::warning() << "Failed to queue guaranteed message: network manager not initialized";
		return false;
	}

	return idLayer.queueGuaranteedMessage( connectionId, networkId, writer );
}

void NetworkManager::sendQueued() {
	if (!initialized) {
		Log::warning() << "Failed to send queued messages: network manager not initialized";
		return;
	}

	idLayer.sendQueued();
}

bool NetworkManager::addNetworkIdHandler( ushort networkId, const NetworkIdHandler & handler ) {
	if (!initialized) {
		Log::warning() << "Failed to add network ID handler: network manager not initialized";
		return false;
	}

	return idLayer.addNetworkIdHandler( networkId, handler );
}

bool NetworkManager::removeNetworkIdHandler( ushort networkId ) {
	if (!initialized) {
		Log::warning() << "Failed to remove network ID handler: network manager not initialized";
		return false;
	}

	if (idLayer.removeNetworkIdHandler( networkId )) {
		if (recentlyRemovedNetworkIdQueue.size() == MAX_RECENTLY_REMOVED_NETWORK_ID_COUNT) {
			ushort oldId = recentlyRemovedNetworkIdQueue.back();
			recentlyRemovedNetworkIdQueue.pop();
			recentlyRemovedNetworkIdSet.erase( recentlyRemovedNetworkIdSet.find( oldId ) );
		}

		recentlyRemovedNetworkIdQueue.push( networkId );
		recentlyRemovedNetworkIdSet.insert( networkId );
		return true;
	} else
		return false;
}

ushort NetworkManager::getUnusedNetworkId() {
	if (!initialized) {
		Log::warning() << "Failed to get unused network ID: network manager not initialized";
		return 0;
	}

	if (idLayer.networkIdHandlerCount() == (size_t)std::numeric_limits <ushort>::max() - recentlyRemovedNetworkIdQueue.size())
		throw std::runtime_error( "Exceeded maximum number of possible network IDs" );

	ushort id;
	do {
		id = nextNetworkId;
		++nextNetworkId;
		// keep incrementing if it exists or if it was recently removed
	} while (idLayer.networkIdHandlerExists( id ) ||
		recentlyRemovedNetworkIdSet.find( id ) != recentlyRemovedNetworkIdSet.end());

	return id;
}

bool NetworkManager::isInitialized() const {
	return initialized;
}

#endif