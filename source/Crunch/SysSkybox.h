#ifndef SYSSKYBOX_DEFINED
#define SYSSKYBOX_DEFINED

#include "Common.h"
#include "ProgramResource.h"
#include "TextureResource.h"
#include "GpuBuffer.h"
#include "SysCamera3D.h"
#include "SysAutoProgram.h"

class SysSkybox : private boost::noncopyable {
public:
	SysSkybox();
	~SysSkybox();
	void setTexture( TextureResourceReference & tex );
	void setCamera( const SysCamera3D * cam );

	void setColor( const Vector3f & c );
	Vector3f getColor() const;
	void setSkyRotation( const Matrix33f & r );
	const Matrix33f & getSkyRotation() const;

	void draw();

private:
	struct Vertex {
		float position[3];
	};

	ProgramResourceReference skyboxProgram;
	TextureResourceReference texture;
	const SysCamera3D * camera;

	GpuBufferReference vertexBuffer;
	GpuBufferReference indexBuffer;

	SysAutoProgram autoProgram;
	struct {
		Matrix44f mvpMatrix;
		Matrix33f skyRotationMatrix;
		Vector3f color;
		int colorMap;
	} uniforms;
};

#endif