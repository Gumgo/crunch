/** @file Maths_Matrix33Dec.h
 *  @brief Contains an implementation of a \htmlonly3&#0215&#0051\endhtmlonly matrix class.
 */

#ifndef MATHS_MATRIX33DEC_DEFINED
#define MATHS_MATRIX33DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief An class for \htmlonly3&#0215&#0051\endhtmlonly matrices.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Matrix33 {
	T d[9]; // the matrix components

public:
	/** @brief Constructs a \htmlonly3&#0215&#0051\endhtmlonly identity matrix.
	 */
	Matrix33();

	/** @brief Constructs a \htmlonly3&#0215&#0051\endhtmlonly matrix with the given components.
	 */
	Matrix33( T m00, T m01, T m02,
			  T m10, T m11, T m12,
			  T m20, T m21, T m22 );

	/** @brief Constructs a \htmlonly3&#0215&#0051\endhtmlonly matrix by copying the one provided.
	 */
	Matrix33( const Matrix33 <T> & m );

	/** @brief The destructor.
	 */
	~Matrix33();

	// operators

	/** @brief Returns the sum of the current value and the matrix m.
	 */
	Matrix33 <T> operator+( const Matrix33 <T> & m ) const;

	/** @brief Returns the current value unmodified.
	 */
	Matrix33 <T> operator+() const;

	/** @brief Returns the difference between the current value and the matrix m.
	 */
	Matrix33 <T> operator-( const Matrix33 <T> & m ) const;

	/** @brief Returns the current value negated.
	 */
	Matrix33 <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Matrix33 <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Matrix33 <T> operator/( T r ) const;

	/** @brief Returns the product of the current value and the vector v.
	 */
	Vector3 <T> operator*( const Vector3 <T> & v ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix33 <T> operator*( const Matrix33 <T> & m ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix33a <T> operator*( const Matrix33a <T> & m ) const;

	/** @brief Sets the current value to the matrix m.
	 */
	Matrix33 <T> operator=( const Matrix33 <T> & m );

	/** @brief Increments the current value by the matrix m.
	 */
	Matrix33 <T> operator+=( const Matrix33 <T> & m );

	/** @brief Decrements the current value by the matrix m.
	 */
	Matrix33 <T> operator-=( const Matrix33 <T> & m );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Matrix33 <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Matrix33 <T> operator/=( T r );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix33 <T> operator*=( const Matrix33 <T> & m );

	/** @brief Returns whether the current value is equal to the matrix m.
	 */
	bool operator==( const Matrix33 <T> & m ) const;

	/** @brief Returns whether the current value is not equal to the matrix m.
	 */
	bool operator!=( const Matrix33 <T> & m ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Accesses the component of the current value at (row, col).
	 */
	template <typename U> T & operator()( U row, U col );

	/** @brief Returns the component of the current value at (row, col).
	 */
	template <typename U> T operator()( U row, U col ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Matrix33 <U> () const;

	// mutable

	/** @brief Sets the components of the current value to the components provided.
	 */
	void set( T m00, T m01, T m02,
			  T m10, T m11, T m12,
			  T m20, T m21, T m22 );

	/** @brief Transposes the current value.
	 */
	void transpose();

	/** @brief Inverts the current value.
	 */
	bool invert();

	/** @brief Rotates the current value about the x-axis by a radians.
	 */
	void rotateX( T a );

	/** @brief Rotates the current value about the y-axis by a radians.
	 */
	void rotateY( T a );

	/** @brief Rotates the current value about the z-axis by a radians.
	 */
	void rotateZ( T a );

	/** @brief Rotates the current value about the axis/angle ((x, y, z), a).
	 */
	void rotate( T x, T y, T z, T a );

	/** @brief Rotates the current value about the axis/angle aa.
	 */
	void rotate( const AxisAngle <T> & aa );

	/** @brief Rotates the current value by the quaternion q.
	 */
	void rotate( const Quaternion <T> & q );

	/** @brief Scales the current value by the vector (x, y, z).
	 */
	void scale( T x, T y, T z );

	/** @brief Scales the current value by the vector s.
	 */
	void scale( const Vector3 <T> & s );

	/** @brief Sets all components to 0.
	 */
	void zero();

	/** @brief Sets the current value to the identity matrix.
	 */
	void identity();

	// immutable

	/** @brief Returns the transpose of the current value.
	 */
	Matrix33 <T> transposed() const;

	/** @brief Returns the inverse of the current value.
	 */
	Matrix33 <T> inverse() const;

	/** @brief Returns the current value rotated about the x-axis by a radians.
	 */
	Matrix33 <T> rotatedX( T a ) const;

	/** @brief Returns the current value rotated about the y-axis by a radians.
	 */
	Matrix33 <T> rotatedY( T a ) const;

	/** @brief Returns the current value rotated about the z-axis by a radians.
	 */
	Matrix33 <T> rotatedZ( T a ) const;

	/** @brief Rotates the current value about the axis/angle ((x, y, z), a).
	 */
	Matrix33 rotated( T x, T y, T z, T a ) const;

	/** @brief Rotates the current value about the axis/angle aa.
	 */
	Matrix33 rotated( const AxisAngle <T> & aa ) const;

	/** @brief Returns the current value rotated by the quaternion q.
	 */
	Matrix33 <T> rotated( const Quaternion <T> & q ) const;

	/** @brief Returns the current value scaled by the vector (x, y, z).
	 */
	Matrix33 <T> scaled( T x, T y, T z ) const;

	/** @brief Returns the current value scaled by the vector s.
	 */
	Matrix33 <T> scaled( const Vector3 <T> & s ) const;

	/** @brief Returns the determinant of the current value.
	 */
	T determinant() const;

	/** @brief Returns the trace of the current value.
	 */
	T trace() const;

	/** @brief Returns a pointer to the component array.
	 */
	const T * getArray() const;

	/** @brief Returns the ray r transformed by the current value.
	 */
	Ray3 <T> transformRay( const Ray3 <T> & r ) const;

	/** @brief Returns a matrix used to transform normals.
	 *
	 *  The matrix returned is the inverse transpose
	 *  of the current value.
	 */
	Matrix33 <T> getNormalTransformation() const;
};

/** @brief Returns the product of the scalar r and the matrix m.
 */
template <typename T> Matrix33 <T> operator*( T r, const Matrix33 <T> & m );

/** @brief Matrix33 of type float.
 */
typedef Matrix33 <float> Matrix33f;

/** @brief Matrix33 of type double.
 */
typedef Matrix33 <double> Matrix33d;

#endif