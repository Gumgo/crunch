#if 0

#include "Socket.h"

int initializeSockets() {
#ifdef _WIN32
	WSADATA wsaData;
	return WSAStartup( MAKEWORD( 2, 0 ), &wsaData );
#else
	return 0;
#endif
}

int terminateSockets() {
#ifdef _WIN32
	return WSACleanup();
#else
	return 0;
#endif
}

int socketCheckError() {
	#ifdef _WIN32
	return WSAGetLastError();
	#else
	return errno;
	#endif
}

#endif