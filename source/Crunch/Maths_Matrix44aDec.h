/** @file Maths_Matrix44aDec.
 *  @brief Contains an implementation of an affine \htmlonly4&#0215&#0052\endhtmlonly matrix class.
 */

#ifndef MATHS_MATRIX44ADEC_DEFINED
#define MATHS_MATRIX44ADEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief An class for affine \htmlonly4&#0215&#0052\endhtmlonly matrices.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Matrix44a {
	T d[12]; // the matrix components

public:
	/** @brief Constructs a \htmlonly4&#0215&#0052\endhtmlonly identity matrix.
	 */
	Matrix44a();

	/** @brief Constructs a \htmlonly4&#0215&#0052\endhtmlonly matrix with the given components.
	 */
	Matrix44a( T m00, T m01, T m02, T m03,
			   T m10, T m11, T m12, T m13,
			   T m20, T m21, T m22, T m23 );

	/** @brief Constructs a \htmlonly4&#0215&#0052\endhtmlonly matrix by copying the one provided.
	 */
	Matrix44a( const Matrix44a <T> & m );

	/** @brief Constructs a \htmlonly4&#0215&#0052\endhtmlonly matrix by copying the one provided.
	 */
	Matrix44a( const Matrix33 <T> & m );

	/** @brief Constructs a rotation matrix from the given quaternion rotation and vector translation.
	 */
	Matrix44a( const Quaternion <T> & q, const Vector3 <T> & v );

	/** @brief The destructor.
	 */
	~Matrix44a();

	// operators

	/** @brief Returns the sum of the current value and the matrix m.
	 */
	Matrix44a <T> operator+( const Matrix44a <T> & m ) const;

	/** @brief Returns the current value unmodified.
	 */
	Matrix44a <T> operator+() const;

	/** @brief Returns the difference between the current value and the matrix m.
	 */
	Matrix44a <T> operator-( const Matrix44a <T> & m ) const;

	/** @brief Returns the current value negated.
	 */
	Matrix44a <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Matrix44a <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Matrix44a <T> operator/( T r ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix44a <T> operator*( const Matrix44a <T> & m ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix44 <T> operator*( const Matrix44 <T> & m ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix44a <T> operator*( const Matrix33 <T> & m ) const;

	/** @brief Sets the current value to the matrix m.
	 */
	Matrix44a <T> operator=( const Matrix44a <T> & m );

	/** @brief Sets the current value to the matrix m.
	 */
	Matrix44a <T> operator=( const Matrix33 <T> & m );

	/** @brief Increments the current value by the matrix m.
	 */
	Matrix44a <T> operator+=( const Matrix44a <T> & m );

	/** @brief Decrements the current value by the matrix m.
	 */
	Matrix44a <T> operator-=( const Matrix44a <T> & m );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Matrix44a <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Matrix44a <T> operator/=( T r );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix44a <T> operator*=( const Matrix44a <T> & m );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix44a <T> operator*=( const Matrix33 <T> & m );

	/** @brief Returns whether the current value is equal to the matrix m.
	 */
	bool operator==( const Matrix44a <T> & m ) const;

	/** @brief Returns whether the current value is not equal to the matrix m.
	 */
	bool operator!=( const Matrix44a <T> & m ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Accesses the component of the current value at (row, col).
	 */
	template <typename U> T & operator()( U row, U col );

	/** @brief Returns the component of the current value at (row, col).
	 */
	template <typename U> T operator()( U row, U col ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Matrix44a <U> () const;

	// mutable

	/** @brief Sets the components of the current value to the components provided.
	 */
	void set( T m00, T m01, T m02, T m03,
			  T m10, T m11, T m12, T m13,
			  T m20, T m21, T m22, T m23 );

	/** @brief Inverts the current value.
	 */
	bool invert();

	/** @brief Translates the current value by the vector (x, y, z).
	 */
	void translate( T x, T y, T z );

	/** @brief Translates the current value by the vector t.
	 */
	void translate( const Vector3 <T> & t );

	/** @brief Rotates the current value about the x-axis by a radians.
	 */
	void rotateX( T a );

	/** @brief Rotates the current value about the y-axis by a radians.
	 */
	void rotateY( T a );

	/** @brief Rotates the current value about the z-axis by a radians.
	 */
	void rotateZ( T a );

	/** @brief Rotates the current value about the axis/angle ((x, y, z), a).
	 */
	void rotate( T x, T y, T z, T a );

	/** @brief Rotates the current value about the axis/angle aa.
	 */
	void rotate( const AxisAngle <T> & aa );

	/** @brief Rotates the current value by the quaternion q.
	 */
	void rotate( const Quaternion <T> & q );

	/** @brief Scales the current value by the vector (x, y).
	 */
	void scale( T x, T y, T z );

	/** @brief Scales the current value by the vector s.
	 */
	void scale( const Vector3 <T> & s );

	/** @brief Sets the current value to the identity matrix.
	 */
	void identity();

	// immutable

	/** @brief Returns the inverse of the current value.
	 */
	Matrix44a <T> inverse() const;

	/** @brief Returns the current value translated by the vector (x, y).
	 */
	Matrix44a <T> translated( T x, T y, T z ) const;

	/** @brief Returns the current value translated by the vector t.
	 */
	Matrix44a <T> translated( const Vector3 <T> & t ) const;

	/** @brief Returns the current value rotated about the x-axis by a radians.
	 */
	Matrix44a <T> rotatedX( T a ) const;

	/** @brief Returns the current value rotated about the y-axis by a radians.
	 */
	Matrix44a <T> rotatedY( T a ) const;

	/** @brief Returns the current value rotated about the z-axis by a radians.
	 */
	Matrix44a <T> rotatedZ( T a ) const;

	/** @brief Rotates the current value about the axis/angle ((x, y, z), a).
	 */
	Matrix44a rotated( T x, T y, T z, T a ) const;

	/** @brief Rotates the current value about the axis/angle aa.
	 */
	Matrix44a rotated( const AxisAngle <T> & aa ) const;

	/** @brief Returns the current value rotated by the quaternion q.
	 */
	Matrix44a <T> rotated( const Quaternion <T> & q ) const;

	/** @brief Returns the current value scaled by the vector (x, y).
	 */
	Matrix44a <T> scaled( T x, T y, T z ) const;

	/** @brief Returns the current value scaled by the vector s.
	 */
	Matrix44a <T> scaled( const Vector3 <T> & s ) const;

	/** @brief Returns the determinant of the current value.
	 */
	T determinant() const;

	/** @brief Returns the trace of the current value.
	 */
	T trace() const;

	/** @brief Returns a pointer to the component array.
	 */
	const T * getArray() const;

	/** @brief Returns the point p transformed by the current value (where p.w = 1).
	 */
	Vector3 <T> transformPoint( const Vector3 <T> & p ) const;

	/** @brief Returns the vector v transformed by the current value (where v.w = 0).
	 */
	Vector3 <T> transformVector( const Vector3 <T> & v ) const;

	/** @brief Returns the ray r transformed by the current value.
	 */
	Ray3 <T> transformRay( const Ray3 <T> & r ) const;

	/** @brief Returns a matrix used to transform normals.
	 *
	 *  The matrix returned is the inverse transpose
	 *  of the rotation and scaling portion of the
	 *  current value.
	 */
	Matrix33 <T> getNormalTransformation() const;
};

/** @brief Returns the product of the scalar r and the matrix m.
 */
template <typename T> Matrix44a <T> operator*( T r, const Matrix44a <T> & m );

/** @brief Matrix44a of type float.
 */
typedef Matrix44a <float> Matrix44af;

/** @brief Matrix44a of type double.
 */
typedef Matrix44a <double> Matrix44ad;

#endif