/** @file Physics2DEdgeShape.h
 *  @brief Defines edge shapes.
 */
#ifndef PHYSICS2DEDGESHAPE_DEFINED
#define PHYSICS2DEDGESHAPE_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DShape.h"

namespace Physics2D {

class Body;
struct EdgeShapeDefinition;

/** @brief An edge shape.
 */
class EdgeShape : public Shape {
public:
	EdgeShape( const EdgeShapeDefinition & d, Body * par );	/**< The constructor.*/

	/** @brief Returns the bounding box of the transformed shape.
	 */
	Aabb getAabb( const Matrix33ar & transformation ) const;

	/** @brief Computes the mass data of the shape.
	 */
	void computeMassData();

	void setPoints( const Vector2r & p0, const Vector2r & p1 );	/**< Sets the points.*/
	Vector2r getPoint( size_t point ) const;					/**< Returns a point.*/
	Vector2r getNormal() const;									/**< Returns the normal.*/

	/** @brief Sets the edges to which the two points are connected.
	 */
	void setConnectivity( EdgeShape * point0Edge, EdgeShape * point1Edge );

	/** @brief Returns the edge to which the given point is connected.
	 */
	EdgeShape * getConnectivity( size_t point ) const;

private:
	Vector2r points[2];	// the two points in the edge
	Vector2r normal;	// the normal of the edge, computed from clockwise points

	// the edges this edge is connected to
	EdgeShape * connectivity[2];
};

/** @brief Defines edge shapes.
 */
struct EdgeShapeDefinition : public ShapeDefinition {
	Vector2r points[2];							/**< The two points.*/

	EdgeShapeDefinition();						/**< Assigns default values.*/
	Shape * createShape( Body * parent ) const;	/**< Creates an edge shape from the definition.*/
};

}

#endif