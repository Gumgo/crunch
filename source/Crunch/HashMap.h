/** @file HashMap.h
 *  @brief Contains a hash map implementation.
 */

#ifndef HASHMAP_DEFINED
#define HASHMAP_DEFINED

#include "Common.h"
#include "Indexer.h"
#include <vector>
#include <stack>

/** @brief Provides a hash map implementation.
 *
 *  @tparam H	The hash function object.
 *  @tparam K	Type of key used.
 *  @tparam V	Type of value used.
 */
template <typename H, typename K, typename V> class HashMap {
public:
	/** @brief The index type.
	 */
	typedef size_t Index;

	struct Entry;

	/** @brief Provides a way to iterate over all entries.
	 */
	class Iterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		const K & getKey();		/**< Returns the current entry's key.*/
		V & getValue();			/**< Returns the current entry's value.*/
		Index getIndex();		/**< Returns the index of the current entry.*/
		void remove();			/**< Removes the current entry.*/

	private:
		friend class HashMap <H, K, V>;

		HashMap <H, K, V> * base;				// the underlying hash map
		typename Indexer <Entry>::Iterator it;	// the underlying iterator

		Iterator( HashMap <H, K, V> * b );
	};

	/** @brief Provides a way to iterate over all entries.
	 */
	class ConstIterator {
	public:
		bool hasNext() const;	/**< Returns whether a next element exists.*/
		bool next();			/**< Returns whether a next element exists and increments the internal index if it does.*/
		const K & getKey();		/**< Returns the current entry's key.*/
		const V & getValue();	/**< Returns the current entry's value.*/
		Index getIndex();		/**< Returns the index of the current entry.*/

	private:
		friend class HashMap <H, K, V>;

		typename Indexer <Entry>::ConstIterator it; // the underlying iterator

		ConstIterator( const HashMap <H, K, V> * b );
	};

	friend class Iterator;
	friend class ConstIterator;

	/** @brief The constructor.
	 *
	 *  @param defaultCapacity	The capacity of the hash table.
	 *  @param h				The hash object.
	 */
	HashMap( size_t defaultCapacity = 20, const H & h = H(),
		float minLoadFactor = -std::numeric_limits <float>::max(),
		float maxLoadFactor = std::numeric_limits <float>::max() );

	/** @brief The copy constructor.
	 *
	 *  @param other	The HashMap to copy.
	 */
	HashMap( const HashMap <H, K, V> & other );

	/** @brief The move constructor.
	 *
	 *  @param other	The HashMap to move.
	 */
	HashMap( HashMap <H, K, V> && other );

	/** @brief The copy assignment operator.
	 *
	 *  @param other	The HashMap to assign.
	 */
	HashMap <H, K, V> & operator=( const HashMap <H, K, V> & other );

	/** @brief The move assignment operator.
	 *
	 *  @param other	The HashMap to move.
	 */
	HashMap <H, K, V> & operator=( HashMap <H, K, V> && other );

	/** @brief Puts the key/value pair into the hash table.
	 *
	 *  @param keyValue	The key/value pair.
	 *  @return			The index of the newly inserted value.
	 */
	Index put( const std::pair <K, V> & keyValue );

	/** @brief Puts the key/value pair into the hash table.
	 *
	 *  @param key		The key associated with value.
	 *  @param value	The value to store in the table.
	 *  @return			The index of the newly inserted value.
	 */
	Index put( const K & key, const V & value );

	/** @brief Returns the value associated with the given key.
	 *
	 *  @param key	The key for which to find the associated value.
	 *  @return		The value associated with the given key.
	 */
	V & get( const K & key );

	/** @brief Returns the value associated with the given key.
	 *
	 *  @param key	The key for which to find the associated value.
	 *  @return		The value associated with the given key.
	 */
	const V & get( const K & key ) const;

	/** @brief Assigns the value associated with the given key to value if it exists.
	 *
	 *  @param key		The key for which to find the associated value.
	 *  @param value	The destination to store the value.
	 *  @return			Whether the value was found and assigned.
	 */
	bool get( const K & key, V & value ) const;

	/** @brief Returns the value associated with the given index.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @return			The value associated with the given index.
	 */
	V & getByIndex( Index index );

	/** @brief Returns the value associated with the given index.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @return			The value associated with the given index.
	 */
	const V & getByIndex( Index index ) const;

	/** @brief Assigns the value associated with the given index to value if it exists.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @param value	The destination to store the value.
	 *  @return			Whether the value was found and assigned.
	 */
	bool getByIndex( Index index, V & value ) const;

	/** @brief Returns the index associated with the given key.
	 *
	 *  @param key	The key for which to find the associated index.
	 *  @return		The index associated with the given key.
	 */
	Index getIndex( const K & key ) const;

	/** @brief Assigns the index associated with the given key to index if it exists.
	 *
	 *  @param key		The key for which to find the associated index.
	 *  @param index	The destination to store the index.
	 *  @return			Whether the index was found and assigned.
	 */
	bool getIndex( const K & key, Index & index ) const;

	/** @brief Returns the key associated with the given index.
	 *
	 *  @param index	The index for which to find the associated key.
	 *  @return			The key associated with the given index.
	 */
	K getKey( Index index ) const;

	/** @brief Assigns the key associated with the given index to key if it exists.
	 *
	 *  @param index	The index for which to find the associated key.
	 *  @param key		The destination to store the key.
	 *  @return			Whether the key was found and assigned.
	 */
	bool getKey( Index index, K & key ) const;

	/** @brief Removes the value associated with the key from the table.
	 *
	 *  @param key	The key associated with the value to remove.
	 *  @return		Whether the value was found and removed.
	 */
	bool remove( const K & key );

	/** @brief Removes the value associated with the index from the table.
	 *
	 *  @param index	The index associated with the value to remove.
	 *  @return			Whether the value was found and removed.
	 */
	bool removeByIndex( Index index );

	/** @brief Removes and returns the value associated with the given key.
	 *
	 *  @param key	The key for which to find the associated value.
	 *  @return		The value associated with the given key.
	 */
	V extract( const K & key );

	/** @brief Assigns the value associated with the given key to value and removes it from the table if it exists.
	 *
	 *  @param key		The key for which to find the associated value.
	 *  @param value	The destination to store the value.
	 *  @return			Whether the value was found and assigned.
	 */
	bool extract( const K & key, V & value );

	/** @brief Removes and returns the value associated with the given index.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @return			The value associated with the given index.
	 */
	V extractByIndex( Index index );

	/** @brief Assigns the value associated with the given index to value and removes it from the table if it exists.
	 *
	 *  @param index	The index for which to find the associated value.
	 *  @param value	The destination to store the value.
	 *  @return			Whether the value was found and assigned.
	 */
	bool extractByIndex( Index index, V & value );

	/** @brief Returns whether the table contains a value associated with the given key.
	 *
	 *  @param key	The key to check for.
	 *  @return		Whether the table contains a value associated with the given key.
	 */
	bool contains( const K & key ) const;

	/** @brief Returns whether the table contains a value associated with the given index.
	 *
	 *  @param index	The index to check for.
	 *  @return			Whether the table contains a value associated with the given index.
	 */
	bool containsIndex( Index index ) const;

	/** @brief Removes all values.
	 */
	void clear();

	/** @brief Resizes the capacity.
	 *
	 *  @param newCapacity	The new capacity.
	 */
	void resize( size_t newCapacity );

	/** @brief Returns the capacity of the hash table.
	 *
	 *  Because separate chaining is used,
	 *  this capacity can be exceeded, but
	 *  performance will decrease.
	 */
	size_t capacity() const;

	/** @brief Returns the number of values currently stored in the hash table.
	 */
	size_t size() const;

	/** @brief Returns true if the hash table is empty.
	 */
	bool empty() const;

	/** @brief Returns an iterator.
	 */
	Iterator getIterator();

	/** @brief Returns a const iterator.
	 */
	ConstIterator getConstIterator() const;

	/** @brief Returns the amount of memory used.
	 */
	size_t memory() const;

	/** @brief Returns minimum load factor.
	 */
	float getMinLoadFactor() const;

	/** @brief Returns maximum load factor.
	 */
	float getMaxLoadFactor() const;

private:
	H hash; // hash function object
	float minimumLoadFactor;
	float maximumLoadFactor;
	void adjustForMinLoadFactor( size_t newSize );
	void adjustForMaxLoadFactor( size_t newSize );

	// the value representing no entry
	static const size_t NO_INDEX = boost::integer_traits <Index>::const_max;

	// an entry in the table
	struct Entry {
		K key;				// the key associated with the entry
		V value;			// the value associated with the entry
		Index prevEntry;	// the next entry in this table bucket, or the bucket index if the highest bit is 1
		Index nextEntry;	// the previous entry in this table bucket

		Entry( const K & k, const V & v );		// constructs entry with the given key and value
		Entry( K && k, V && v );				// constructs entry with the given key and value
		Entry( const Entry & e );				// constructs entry from another entry
		Entry( Entry && e );					// constructs entry from another entry
		Entry & operator=( const Entry & e );	// assigns the entry from another entry
		Entry & operator=( Entry && e );		// assigns the entry from another entry

		bool isFirstEntry() const;		// returns whether this is the first entry in the table bucket
		Index getTableIndex() const;	// returns the bucket index if this is the first entry in the table bucket
		void setTableIndex( Index b );	// sets the bucket index
	};

	std::vector <Index> table;	// the table of indices pointing to entries
	Indexer <Entry> entries;	// the entries
};

template <typename H, typename K, typename V>
HashMap <H, K, V>::Entry::Entry( const K & k, const V & v )
	: key( k )
	, value( v ) {
}

template <typename H, typename K, typename V>
HashMap <H, K, V>::Entry::Entry( K && k, V && v )
	: key( k )
	, value( v ) {
}

template <typename H, typename K, typename V>
HashMap <H, K, V>::Entry::Entry( const Entry & e )
	: key( e.key )
	, value( e.value ) {
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
}

template <typename H, typename K, typename V>
HashMap <H, K, V>::Entry::Entry( Entry && e )
	: key( std::move( e.key ) )
	, value( std::move( e.value ) ) {
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
}

template <typename H, typename K, typename V>
typename HashMap <H, K, V>::Entry & HashMap <H, K, V>::Entry::operator=( const Entry & e ) {
	key = e.key;
	value = e.value;
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
	return *this;
}

template <typename H, typename K, typename V>
typename HashMap <H, K, V>::Entry & HashMap <H, K, V>::Entry::operator=( Entry && e ) {
	key = std::move( e.key );
	value = std::move( e.value );
	prevEntry = e.prevEntry;
	nextEntry = e.nextEntry;
	return *this;
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::Entry::isFirstEntry() const {
	return (prevEntry & (1 << (sizeof( prevEntry )*8 - 1))) != 0;
}

template <typename H, typename K, typename V>
typename HashMap <H, K, V>::Index HashMap <H, K, V>::Entry::getTableIndex() const {
	return prevEntry & ~(1 << (sizeof( prevEntry )*8 - 1));
}

template <typename H, typename K, typename V>
void HashMap <H, K, V>::Entry::setTableIndex( Index b ) {
	prevEntry = b | (1 << (sizeof( prevEntry )*8 - 1));
}

template <typename H, typename K, typename V> HashMap <H, K, V>::Iterator::Iterator( HashMap <H, K, V> * b )
	: it( b->entries.getIterator() ) {
	base = b;
}

template <typename H, typename K, typename V> bool HashMap <H, K, V>::Iterator::hasNext() const {
	return it.hasNext();
}

template <typename H, typename K, typename V> bool HashMap <H, K, V>::Iterator::next() {
	return it.next();
}

template <typename H, typename K, typename V> const K & HashMap <H, K, V>::Iterator::getKey() {
	return it.get().key;
}

template <typename H, typename K, typename V> V & HashMap <H, K, V>::Iterator::getValue() {
	return it.get().value;
}

template <typename H, typename K, typename V> typename HashMap <H, K, V>::Index HashMap <H, K, V>::Iterator::getIndex() {
	return it.getIndex();
}

template <typename H, typename K, typename V> void HashMap <H, K, V>::Iterator::remove() {
	Index index = it.getIndex();

	Entry & e = base->entries.get( index );
	if (e.isFirstEntry())
		base->table[e.getTableIndex()] = e.nextEntry;
	else
		base->entries.get( e.prevEntry ).nextEntry = e.nextEntry;
	if (e.nextEntry != NO_INDEX)
		base->entries.get( e.nextEntry ).prevEntry = e.prevEntry;

	it.remove();
}

template <typename H, typename K, typename V> HashMap <H, K, V>::ConstIterator::ConstIterator( const HashMap <H, K, V> * b )
	: it( b->entries.getConstIterator() ) {
}

template <typename H, typename K, typename V> bool HashMap <H, K, V>::ConstIterator::hasNext() const {
	return it.hasNext();
}

template <typename H, typename K, typename V> bool HashMap <H, K, V>::ConstIterator::next() {
	return it.next();
}

template <typename H, typename K, typename V> const K & HashMap <H, K, V>::ConstIterator::getKey() {
	return it.get().key;
}

template <typename H, typename K, typename V> const V & HashMap <H, K, V>::ConstIterator::getValue() {
	return it.get().value;
}

template <typename H, typename K, typename V> typename HashMap <H, K, V>::Index HashMap <H, K, V>::ConstIterator::getIndex() {
	return it.getIndex();
}

template <typename H, typename K, typename V>
HashMap <H, K, V>::HashMap( size_t defaultCapacity, const H & h, float minLoadFactor, float maxLoadFactor )
	: hash( h )
	, minimumLoadFactor( minLoadFactor )
	, maximumLoadFactor( maxLoadFactor )
	, entries( defaultCapacity ) {
	// to avoid useless allocations, defaultCapacity can be 0
	// however, attempting to use the data structure in nearly way is undefined (most likely will crash)
	table.resize( defaultCapacity, NO_INDEX );
}

template <typename H, typename K, typename V>
HashMap <H, K, V>::HashMap( const HashMap <H, K, V> & other )
	: hash( other.hash )
	, minimumLoadFactor( other.minimumLoadFactor )
	, maximumLoadFactor( other.maximumLoadFactor )
	, table( other.table )
	, entries( other.entries ) {
}

template <typename H, typename K, typename V>
HashMap <H, K, V>::HashMap( HashMap <H, K, V> && other )
	: hash( other.hash )
	, minimumLoadFactor( other.minimumLoadFactor )
	, maximumLoadFactor( other.maximumLoadFactor )
	, entries( std::move( other.entries ) )
	, table( std::move( other.table ) ) {
}

template <typename H, typename K, typename V>
HashMap <H, K, V> & HashMap <H, K, V>::operator=( const HashMap <H, K, V> & other ) {
	hash = other.hash;
	minimumLoadFactor = other.minimumLoadFactor;
	maximumLoadFactor = other.maximumLoadFactor;
	entries = other.entries;
	table = other.table;
	return *this;
}

template <typename H, typename K, typename V>
HashMap <H, K, V> & HashMap <H, K, V>::operator=( HashMap <H, K, V> && other ) {
	std::swap( hash, other.hash );
	minimumLoadFactor = other.minimumLoadFactor;
	maximumLoadFactor = other.maximumLoadFactor;
	table.swap( other.table );
	entries = std::move( other.entries );
	return *this;
}

template <typename H, typename K, typename V>
typename HashMap <H, K, V>::Index HashMap <H, K, V>::put( const std::pair <K, V> & keyValue ) {
	return put( keyValue.first, keyValue.second );
}

template <typename H, typename K, typename V>
typename HashMap <H, K, V>::Index HashMap <H, K, V>::put( const K & key, const V & value ) {
	adjustForMaxLoadFactor( entries.size() + 1 );

	Index tableIndex = (Index)(hash( key ) % table.size());

	// add to front of list the bucket
	Index index = entries.add( Entry( key, value ) );
	Entry & e = entries.get( index );
	e.setTableIndex( tableIndex );
	e.nextEntry = table[tableIndex];
	table[tableIndex] = index;
	if (e.nextEntry != NO_INDEX)
		entries.get( e.nextEntry ).prevEntry = index;

	return index;
}

template <typename H, typename K, typename V>
V & HashMap <H, K, V>::get( const K & key ) {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		Entry & e = entries.get( index );
		if (key == e.key)
			return e.value;
		index = e.nextEntry;
	}

	throw std::runtime_error( "No value exists with the specified key" );
}

template <typename H, typename K, typename V>
const V & HashMap <H, K, V>::get( const K & key ) const {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		const Entry & e = entries.get( index );
		if (key == e.key)
			return e.value;
		index = e.nextEntry;
	}

	throw std::runtime_error( "No value exists with the specified key" );
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::get( const K & key, V & value ) const {
	Index tableIndex = (size_t)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		const Entry & e = entries.get( index );
		if (key == e.key) {
			value = e.value;
			return true;
		}
		index = e.nextEntry;
	}

	return false;
}

template <typename H, typename K, typename V>
V & HashMap <H, K, V>::getByIndex( Index index ) {
	try {
		return entries.get( index ).value;
	} catch (const std::exception &) {
		throw std::runtime_error( "No value exists with the specified index" );
	}
}

template <typename H, typename K, typename V>
const V & HashMap <H, K, V>::getByIndex( Index index ) const {
	try {
		return entries.get( index ).value;
	} catch (const std::exception &) {
		throw std::runtime_error( "No value exists with the specified index" );
	}
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::getByIndex( Index index, V & value ) const {
	if (entries.contains( index )) {
		value = entries.get( index ).value;
		return true;
	} else
		return false;
}

template <typename H, typename K, typename V>
typename HashMap <H, K, V>::Index HashMap <H, K, V>::getIndex( const K & key ) const {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		const Entry & e = entries.get( index );
		if (key == e.key)
			return index;
		index = e.nextEntry;
	}

	throw std::runtime_error( "No index exists with the specified key" );
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::getIndex( const K & key, Index & index ) const {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index idx = table[tableIndex];
	while (idx != NO_INDEX) {
		const Entry & e = entries.get( idx );
		if (key == e.key) {
			index = idx;
			return true;
		}
		idx = e.nextEntry;
	}

	return false;
}

template <typename H, typename K, typename V>
K HashMap <H, K, V>::getKey( Index index ) const {
	try {
		return entries.get( index ).key;
	} catch (const std::exception &) {
		throw std::runtime_error( "No key exists with the specified index" );
	}
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::getKey( Index index, K & key ) const {
	if (entries.contains( index )) {
		key = entries.get( index ).key;
		return true;
	} else
		return false;
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::remove( const K & key ) {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		Entry & e = entries.get( index );
		if (key == e.key) {
			if (e.isFirstEntry())
				table[tableIndex] = e.nextEntry;
			else
				entries.get( e.prevEntry ).nextEntry = e.nextEntry;
			if (e.nextEntry != NO_INDEX)
				entries.get( e.nextEntry ).prevEntry = e.prevEntry;
			entries.remove( index );
			adjustForMinLoadFactor( entries.size() - 1 );
			return true;
		}
		index = e.nextEntry;
	}

	return false;
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::removeByIndex( Index index ) {
	if (entries.contains( index )) {
		Entry & e = entries.get( index );
		if (e.isFirstEntry())
			table[e.getTableIndex()] = e.nextEntry;
		else
			entries.get( e.prevEntry ).nextEntry = e.nextEntry;
		if (e.nextEntry != NO_INDEX)
			entries.get( e.nextEntry ).prevEntry = e.prevEntry;
		entries.remove( index );
		adjustForMinLoadFactor( entries.size() );
		return true;
	} else
		return false;
}

template <typename H, typename K, typename V>
V HashMap <H, K, V>::extract( const K & key ) {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		Entry & e = entries.get( index );
		if (key == e.key) {
			if (e.isFirstEntry())
				table[tableIndex] = e.nextEntry;
			else
				entries.get( e.prevEntry ).nextEntry = e.nextEntry;
			if (e.nextEntry != NO_INDEX)
				entries.get( e.nextEntry ).prevEntry = e.prevEntry;
			V ret = entries.extract( index ).value;
			adjustForMinLoadFactor( entries.size() );
			return ret;
		}
		index = e.nextEntry;
	}

	throw std::runtime_error( "No value exists with the specified key" );
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::extract( const K & key, V & value ) {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		Entry & e = entries.get( index );
		if (key == e.key) {
			if (e.isFirstEntry())
				table[tableIndex] = e.nextEntry;
			else
				entries.get( e.prevEntry ).nextEntry = e.nextEntry;
			if (e.nextEntry != NO_INDEX)
				entries.get( e.nextEntry ).prevEntry = e.prevEntry;
			value = entries.extract( index ).value;
			adjustForMinLoadFactor( entries.size() );
			return true;
		}
		index = e.nextEntry;
	}

	return false;
}

template <typename H, typename K, typename V>
V HashMap <H, K, V>::extractByIndex( Index index ) {
	try {
		Entry & e = entries.get( index );
		if (e.isFirstEntry())
			table[e.getTableIndex()] = e.nextEntry;
		else
			entries.get( e.prevEntry ).nextEntry = e.nextEntry;
		if (e.nextEntry != NO_INDEX)
			entries.get( e.nextEntry ).prevEntry = e.prevEntry;
		V ret = entries.extract( index ).value;
		adjustForMinLoadFactor( entries.size() );
		return ret;
	} catch (const std::exception & e) {
		throw std::runtime_error( "No value exists with the specified index" );
	}
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::extractByIndex( Index index, V & value ) {
	if (entries.contains( index )) {
		Entry & e = entries.get( index );
		if (e.isFirstEntry())
			table[e.getTableIndex()] = e.nextEntry;
		else
			entries.get( e.prevEntry ).nextEntry = e.nextEntry;
		if (e.nextEntry != NO_INDEX)
			entries.get( e.nextEntry ).prevEntry = e.prevEntry;
		value = entries.extract( index ).value;
		adjustForMinLoadFactor( entries.size() );
		return true;
	} else
		return false;
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::contains( const K & key ) const {
	Index tableIndex = (Index)(hash( key ) % table.size());

	Index index = table[tableIndex];
	while (index != NO_INDEX) {
		const Entry & e = entries.get( index );
		if (key == e.key)
			return true;
		index = e.nextEntry;
	}

	return false;
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::containsIndex( Index index ) const {
	return entries.contains( index );
}

template <typename H, typename K, typename V>
void HashMap <H, K, V>::clear() {
	for (size_t i = 0; i < table.size(); ++i)
		table[i] = NO_INDEX;

	entries.clear();
}

template <typename H, typename K, typename V>
void HashMap <H, K, V>::resize( size_t newCapacity ) {
	if (newCapacity == 0 && table.size() > 0)
		throw std::runtime_error( "Invalid HashMap capacity " + toString( newCapacity ) );

	table.assign( newCapacity, NO_INDEX );

	// rehash
	size_t remaining = entries.size();
	Index index = 0;
	while (remaining > 0) {
		if (entries.contains( index )) {
			Entry & e = entries.get( index );
			Index tableIndex = (Index)(hash( e.key ) % table.size());

			// add to front of list the bucket
			e.setTableIndex( tableIndex );
			e.nextEntry = table[tableIndex];
			table[tableIndex] = index;
			if (e.nextEntry != NO_INDEX)
				entries.get( e.nextEntry ).prevEntry = index;
			--remaining;
		}
		++index;
	}
}

template <typename H, typename K, typename V>
size_t HashMap <H, K, V>::capacity() const {
	return table.size();
}

template <typename H, typename K, typename V>
size_t HashMap <H, K, V>::size() const {
	return entries.size();
}

template <typename H, typename K, typename V>
bool HashMap <H, K, V>::empty() const {
	return entries.size() == 0;
}

template <typename H, typename K, typename V> typename HashMap <H, K, V>::Iterator HashMap <H, K, V>::getIterator() {
	return Iterator( this );
}

template <typename H, typename K, typename V> typename HashMap <H, K, V>::ConstIterator HashMap <H, K, V>::getConstIterator() const {
	return ConstIterator( this );
}

template <typename H, typename K, typename V> size_t HashMap <H, K, V>::memory() const {
	return sizeof( table[0] ) * table.capacity() + entries.memory();
}

template <typename H, typename K, typename V> void HashMap <H, K, V>::adjustForMinLoadFactor( size_t newSize ) {
	size_t newTableSize = table.size();
	while ((float)newSize / (float)newTableSize < minimumLoadFactor && newTableSize > 1)
		newTableSize /= 2; // will never go to 0
	if (newTableSize < table.size())
		resize( newTableSize );
}

template <typename H, typename K, typename V> void HashMap <H, K, V>::adjustForMaxLoadFactor( size_t newSize ) {
	size_t newTableSize = table.size();
	while ((float)newSize / (float)newTableSize > maximumLoadFactor)
		newTableSize *= 2;
	if (newTableSize > table.size())
		resize( newTableSize );
}

#endif