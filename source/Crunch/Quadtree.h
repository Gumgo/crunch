/** @file Quadtree.h
 *  @brief Contains a quadtree implementation.
 */

#ifndef QUADTREE_DEFINED
#define QUADTREE_DEFINED

#include "Common.h"
#include <vector>

/** @brief A quadtree implementation.
 */
template <typename T, typename GetBounds>
class Quadtree {
public:
	/** @brief The constructor.
	 */
	Quadtree( const Vector2f & boundMin, const Vector2f & boundMax, const GetBounds & gb = GetBounds() );

	void insert( const T & t );	/**< Inserts the element into the quadtree.*/
	void remove( const T & t );	/**< Removes the element from the quadtree.*/

	/** @brief Returns a list of elements overlapping the point p.
	 */
	std::vector <T> overlapsPoint( const Vector2f & p ) const;

private:
	// the node structure.
	struct Node {
		static const size_t MAX_NODE_SIZE = 8;	// maximum size of a node before splitting
		static const size_t MIN_NODE_SIZE = 6;	// minimum size of a node before merging
		static const size_t MAX_DEPTH = 4;		// maximum depth nodes can subdivide to

		Quadtree <T, GetBounds> * qt;	// pointer to the outer quadtree instance
		std::vector <Node> children;	// the node's children
		size_t depth;					// the depth of this node
		Vector2f bounds[2];				// the min and max bounds of this node
		Vector2f center;				// the center of this node

		std::vector <T> elements;		// the node's contained elements
		size_t count;					// the number of elements at this node or lower

		// inserts the element into the node
		void insert( const T & t, const Vector2f & boundMin, const Vector2f & boundMax );

		// removes the element from the node
		void remove( const T & t, const Vector2f & boundMin, const Vector2f & boundMax );

		// splits this node into 4 children
		void split();

		// merges the children of this node
		void merge();

		// appends elements overlapping p to the list
		void overlapsPoint( const Vector2f & p, std::vector <T> & elems, const GetBounds & getBounds ) const;
	} root;

	friend struct Node;
	GetBounds getBounds; // function object to get the bounds of an element
};

template <typename T, typename GetBounds>
Quadtree <T, GetBounds>::Quadtree( const Vector2f & boundMin, const Vector2f & boundMax, const GetBounds & gb )
	: getBounds( gb ) {
	root.qt = this;
	root.depth = 0;
	root.bounds[0] = boundMin;
	root.bounds[1] = boundMax;
	root.center = (boundMin + boundMax) * 0.5f;
	root.count = 0;
}

template <typename T, typename GetBounds>
void Quadtree <T, GetBounds>::insert( const T & t ) {
	std::pair <Vector2f, Vector2f> bounds = getBounds( t );
	root.insert( t, bounds.first, bounds.second );
}

template <typename T, typename GetBounds>
void Quadtree <T, GetBounds>::remove( const T & t ) {
	std::pair <Vector2f, Vector2f> bounds = getBounds( t );
	root.remove( t, bounds.first, bounds.second );
}

template <typename T, typename GetBounds>
std::vector <T> Quadtree <T, GetBounds>::overlapsPoint( const Vector2f & p ) const {
	std::vector <T> elems;
	root.overlapsPoint( p, elems, getBounds );
	return elems;
}

template <typename T, typename GetBounds>
void Quadtree <T, GetBounds>::Node::insert( const T & t, const Vector2f & boundMin, const Vector2f & boundMax ) {
	++count;
	if (!children.empty()) {
		// if a node...
		if (boundMin.x <= bounds[0].x && boundMin.y <= bounds[0].y && boundMax.x >= bounds[1].x && boundMax.y >= bounds[1].y)
			// if t covers whole node, don't pass to children
			elements.push_back( t );
		else {
			// pass to children
			if (boundMin.x < center.x && boundMin.y < center.y)
				children[0].insert( t, boundMin, boundMax );
			if ((boundMin.x >= center.x || boundMax.x > center.x) && boundMin.y < center.y)
				children[1].insert( t, boundMin, boundMax );
			if (boundMin.x < center.x && (boundMin.y >= center.y || boundMax.y > center.y))
				children[2].insert( t, boundMin, boundMax );
			if ((boundMin.x >= center.x || boundMax.x > center.x) && (boundMin.y >= center.y || boundMax.y > center.y))
				children[3].insert( t, boundMin, boundMax );
		}
	} else {
		// if a leaf...
		// add t
		elements.push_back( t );
		if (elements.size() >= MAX_NODE_SIZE && depth < MAX_DEPTH)
			// if too many elements, subdivide
			split();
	}
}

template <typename T, typename GetBounds>
void Quadtree <T, GetBounds>::Node::remove( const T & t, const Vector2f & boundMin, const Vector2f & boundMax ) {
	--count;
	if (children.empty() || boundMin.x <= bounds[0].x && boundMin.y <= bounds[0].y && boundMax.x >= bounds[1].x && boundMax.y >= bounds[1].y) {
		// if a leaf or if t covers whole node, try removing from this node
		for (size_t i = 0; i < elements.size(); ++i) {
			if (elements[i] == t) {
				elements.erase( elements.begin() + i );
				break;
			}
		}
	} else {
		// pass to children

		if (boundMin.x < center.x && boundMin.y < center.y)
			children[0].remove( t, boundMin, boundMax );
		if ((boundMin.x >= center.x || boundMax.x > center.x) && boundMin.y < center.y)
			children[1].insert( t, boundMin, boundMax );
		if (boundMin.x < center.x && (boundMin.y >= center.y || boundMax.y > center.y))
			children[2].insert( t, boundMin, boundMax );
		if ((boundMin.x >= center.x || boundMax.x > center.x) && (boundMin.y >= center.y || boundMax.y > center.y))
			children[3].insert( t, boundMin, boundMax );
	}

	if (count < MIN_NODE_SIZE)
		merge();
}

template <typename T, typename GetBounds>
void Quadtree <T, GetBounds>::Node::split() {
	std::vector <T> childElements[4];
	std::vector <T> thisElements;
	for (size_t i = 0; i < elements.size(); ++i) {
		// for each element, get the bounds
		const T & elem = elements[i];
		std::pair <Vector2f, Vector2f> tBounds = qt->getBounds( elem );
		const Vector2f & bMin = tBounds.first;
		const Vector2f & bMax = tBounds.second;
		// insert into appropriate child list (or list for this node)
		if (bMin.x <= bounds[0].x && bMin.y <= bounds[0].y && bMax.x >= bounds[1].x && bMax.y >= bounds[1].y)
			thisElements.push_back( elem );
		else {
			if (bMin.x < center.x && bMin.y < center.y)
				childElements[0].push_back( elem );
			if ((bMin.x >= center.x || bMax.x > center.x) && bMin.y < center.y)
				childElements[1].push_back( elem );
			if (bMin.x < center.x && (bMin.y >= center.y || bMax.y > center.y))
				childElements[2].push_back( elem );
			if ((bMin.x >= center.x || bMax.x > center.x) && (bMin.y >= center.y || bMax.y > center.y))
				childElements[3].push_back( elem );
		}
	}
	// make sure all the children aren't being inserted into the same node(s)
	bool allInvalid = true;
	for (size_t j = 0; j < 4; ++j) {
		if (childElements[j].size() != 0 && childElements[j].size() != elements.size()) {
			allInvalid = false;
			break;
		}
	}

	children.resize( 4 );
	children[0].bounds[0] = bounds[0];
	children[0].bounds[1] = center;
	children[1].bounds[0] = Vector2f( center.x, bounds[0].y );
	children[1].bounds[1] = Vector2f( bounds[1].x, center.y );
	children[2].bounds[0] = Vector2f( bounds[0].x, center.y );
	children[2].bounds[1] = Vector2f( center.x, bounds[1].y );
	children[3].bounds[0] = center;
	children[3].bounds[1] = bounds[1];
	for (size_t i = 0; i < 4; ++i) {
		children[i].qt = qt;
		children[i].depth = depth + 1;
		children[i].center = (children[i].bounds[0] + children[i].bounds[1]) * 0.5f;
		children[i].elements.swap( childElements[i] );
	}
	elements.swap( thisElements );
}

template <typename T, typename GetBounds>
void Quadtree <T, GetBounds>::Node::merge() {
	for (size_t i = 0; i < children.size(); ++i) {
		children[i].merge();
		for (size_t t = 0; t < children[i].elements.size(); ++t)
			elements.push_back( children[i].elements[t] );
	}

	children.clear();
}

template <typename T, typename GetBounds>
void Quadtree <T, GetBounds>::Node::overlapsPoint( const Vector2f & p, std::vector <T> & elems, const GetBounds & getBounds ) const {
	for (size_t i = 0; i < elements.size(); ++i) {
		std::pair <Vector2f, Vector2f> elemBounds = getBounds( elements[i] );
		if (p.x >= elemBounds.first.x && p.y >= elemBounds.first.y &&
			p.x < elemBounds.second.x && p.y < elemBounds.second.y) {
			bool found = false;
			for (size_t t = 0; t < elems.size(); ++t) {
				if (elems[t] == elements[i]) {
					found = true;
					break;
				}
			}
			if (!found)
				elems.push_back( elements[i] );
		}
	}
	if (!children.empty()) {
		int c = 0;
		if (p.x >= center.x)
			++c;
		if (p.y >= center.y)
			c += 2;
		children[c].overlapsPoint( p, elems, getBounds );
	}
}

#endif