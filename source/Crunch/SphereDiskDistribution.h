#ifndef SPHEREDISKDISTRIBUTION_DEFINED
#define SPHEREDISKDISTRIBUTION_DEFINED

#include "Common.h"
#include <vector>

// generates evenly distributed disks on a unit sphere
class SphereDiskDistribution {
public:
	struct Disk {
		Vector3f position;
		float radius;
	};

	SphereDiskDistribution( size_t diskCount, float areaVariation, size_t iterations, uint seed );
	const std::vector <Disk> & getDisks() const;

private:
	std::vector <Disk> disks;
	std::vector <float/*???*/> forces;

	void iterate();
};

#endif