/** @file Registration.h
 *  @brief Registers core resources.
 */

#ifndef REGISTRATION_DEFINED
#define REGISTRATION_DEFINED

#include "Common.h"
#include "FastDelegate.h"

/** @brief Registers core engine components.
 */
void registerCoreComponents();

#endif