#include "Skeleton3DAnimationTreeResource.h"
#include "Log.h"

Skeleton3DAnimationTreeResource::Skeleton3DAnimationTreeResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m )
	, nodeNameMap( 1 ) {
}

bool Skeleton3DAnimationTreeResource::loadDataAsync() {
	try {
		// open and load the file
		KeyValueReader in( getPath() );

		KeyValueReader::Iterator it = in.getIterator();

		KeyValueReader::Iterator treeIt = it.getArray( "tree" );
		if (treeIt.getCount() != 1)
			throw std::runtime_error( "Tree must contain a single root node" );

		std::vector <std::string> names;
		// queue of iterators to process
		std::queue <KeyValueReader::Iterator> nodeIts;
		std::queue <std::string> nodeNames;
		nodeIts.push( treeIt.getArray( 0 ) );
		nodeNames.push( treeIt.getKey( 0 ) );
		size_t nextNodeIndex = 1;
		while (!nodeIts.empty()) {
			KeyValueReader::Iterator nodeIt = nodeIts.front();
			nodeIts.pop();
			std::string name = nodeNames.front();
			nodeNames.pop();
			std::string type = nodeIt.getString( "type" );
			for (size_t i = 0; i < names.size(); ++i) {
				if (names[i] == name)
					throw std::runtime_error( "Duplicate node name " + name + " specified" );
			}
			names.push_back( name );
			if (type == "ANIMATION") {
				std::string animation = nodeIt.getString( "animation" );
				// add the animation if it doesn't already exist
				size_t animationIndex;
				for (animationIndex = 0; animationIndex < animations.size(); ++animationIndex) {
					if (animations[animationIndex] == animation)
						break;
				}
				if (animationIndex == animations.size())
					animations.push_back( animation );

				AnimationNode node;
				node.animationIndex = animationIndex;
				nodes.push_back( Node( node ) );

				if (nodeIt.contains( "children" )) {
					if (nodeIt.getArray( "children" ).getCount() > 0)
						throw std::runtime_error( "Animation node can have no children" );
				}
			} else if (type == "SELECT") {
				KeyValueReader::Iterator childrenIt = nodeIt.getArray( "children" );
				if (childrenIt.getCount() == 0)
					throw std::runtime_error( "Select node must have at least one child" );

				SelectNode node;
				node.firstChild = nextNodeIndex;
				node.childCount = childrenIt.getCount();
				nextNodeIndex += node.childCount;
				nodes.push_back( Node( node ) );

				for (size_t i = 0; i < childrenIt.getCount(); ++i) {
					nodeIts.push( childrenIt.getArray( i ) );
					nodeNames.push( childrenIt.getKey( i ) );
				}
			} else if (type == "BLEND") {
				KeyValueReader::Iterator childrenIt = nodeIt.getArray( "children" );
				if (childrenIt.getCount() == 0)
					throw std::runtime_error( "Blend node must have at least one child" );

				BlendNode node;
				node.wrap = nodeIt.getBool( "wrap" );
				node.firstChild = nextNodeIndex;
				node.childCount = childrenIt.getCount();
				nextNodeIndex += node.childCount;
				nodes.push_back( Node( node ) );

				for (size_t i = 0; i < childrenIt.getCount(); ++i) {
					nodeIts.push( childrenIt.getArray( i ) );
					nodeNames.push( childrenIt.getKey( i ) );
				}
			} else if (type == "SEQUENCE") {
				KeyValueReader::Iterator childrenIt = nodeIt.getArray( "children" );
				if (childrenIt.getCount() == 0)
					throw std::runtime_error( "Sequence node must have at least one child" );

				SequenceNode node;
				node.firstChild = nextNodeIndex;
				node.childCount = childrenIt.getCount();
				nextNodeIndex += node.childCount;
				nodes.push_back( node );

				for (size_t i = 0; i < childrenIt.getCount(); ++i) {
					nodeIts.push( childrenIt.getArray( i ) );
					nodeNames.push( childrenIt.getKey( i ) );
				}
			} else
				throw std::runtime_error( "Invalid node type " + type );
		}

		nodeNameMap.resize( names.size() * 2 + 1 );
		for (size_t i = 0; i < names.size(); ++i)
			nodeNameMap.put( names[i], i );

		return true;
	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}
}

bool Skeleton3DAnimationTreeResource::loadDataSync() {
	return true;
}

void Skeleton3DAnimationTreeResource::freeData() {
}

size_t Skeleton3DAnimationTreeResource::AnimationNode::getAnimationIndex() const {
	return animationIndex;
}

size_t Skeleton3DAnimationTreeResource::SelectNode::getChildCount() const {
	return childCount;
}

size_t Skeleton3DAnimationTreeResource::SelectNode::getChildIndex( size_t i ) const {
	return firstChild + i;
}

bool Skeleton3DAnimationTreeResource::BlendNode::getWrap() const {
	return wrap;
}

size_t Skeleton3DAnimationTreeResource::BlendNode::getChildCount() const {
	return childCount;
}

size_t Skeleton3DAnimationTreeResource::BlendNode::getChildIndex( size_t i ) const {
	return firstChild + i;
}

size_t Skeleton3DAnimationTreeResource::SequenceNode::getChildCount() const {
	return childCount;
}

size_t Skeleton3DAnimationTreeResource::SequenceNode::getChildIndex( size_t i ) const {
	return firstChild + i;
}

Skeleton3DAnimationTreeResource::Node::Type Skeleton3DAnimationTreeResource::Node::getType() const {
	return type;
}

const Skeleton3DAnimationTreeResource::AnimationNode & Skeleton3DAnimationTreeResource::Node::getAnimationNode() const {
	if (type != T_ANIMATION)
		throw std::runtime_error( "Node is not an animation node" );
	return animationNode;
}

const Skeleton3DAnimationTreeResource::SelectNode & Skeleton3DAnimationTreeResource::Node::getSelectNode() const {
	if (type != T_SELECT)
		throw std::runtime_error( "Node is not an select node" );
	return selectNode;
}

const Skeleton3DAnimationTreeResource::BlendNode & Skeleton3DAnimationTreeResource::Node::getBlendNode() const {
	if (type != T_BLEND)
		throw std::runtime_error( "Node is not an blend node" );
	return blendNode;
}

const Skeleton3DAnimationTreeResource::SequenceNode & Skeleton3DAnimationTreeResource::Node::getSequenceNode() const {
	if (type != T_SEQUENCE)
		throw std::runtime_error( "Node is not an sequence node" );
	return sequenceNode;
}

Skeleton3DAnimationTreeResource::Node::Node( const AnimationNode & n ) {
	type = T_ANIMATION;
	animationNode = n;
}

Skeleton3DAnimationTreeResource::Node::Node( const SelectNode & n ) {
	type = T_SELECT;
	selectNode = n;
}

Skeleton3DAnimationTreeResource::Node::Node( const BlendNode & n ) {
	type = T_BLEND;
	blendNode = n;
}

Skeleton3DAnimationTreeResource::Node::Node( const SequenceNode & n ) {
	type = T_SEQUENCE;
	sequenceNode = n;
}

size_t Skeleton3DAnimationTreeResource::getNodeCount() const {
	return nodes.size();
}

const Skeleton3DAnimationTreeResource::Node & Skeleton3DAnimationTreeResource::getNode( size_t i ) const {
	return nodes[i];
}

bool Skeleton3DAnimationTreeResource::getNodeIndex( const std::string & name, size_t & index ) const {
	return nodeNameMap.get( name, index );
}

size_t Skeleton3DAnimationTreeResource::getAnimationCount() const {
	return animations.size();
}

const std::string & Skeleton3DAnimationTreeResource::getAnimation( size_t i ) const {
	return animations[i];
}