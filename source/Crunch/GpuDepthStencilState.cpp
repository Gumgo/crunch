#include "GpuDepthStencilState.h"
#include "GpuState.h"

GpuDepthStencilState::GpuDepthStencilState()
	: GpuObject( NULL ) {
}

void GpuDepthStencilState::bind() {
	getGpuState()->bind( *this );
}

void GpuDepthStencilState::setDepthTestEnabled( bool e ) {
	depthTestEnabled = e;
}

void GpuDepthStencilState::setDepthFunction( GLenum function ) {
	depthFunction = function;
}

void GpuDepthStencilState::setDepthWriteEnabled( bool e ) {
	depthWriteEnabled = e;
}

void GpuDepthStencilState::setStencilTestEnabled( bool e ) {
	stencilTestEnabled = e;
}

void GpuDepthStencilState::setStencilFunction( GLenum function ) {
	stencilFunctionFront = function;
	stencilFunctionBack = function;
}

void GpuDepthStencilState::setStencilFunctions( GLenum functionFront, GLenum functionBack ) {
	stencilFunctionFront = functionFront;
	stencilFunctionBack = functionBack;
}

void GpuDepthStencilState::setStencilReference( int reference ) {
	stencilReferenceFront = reference;
	stencilReferenceBack = reference;
}

void GpuDepthStencilState::setStencilReferences( int referenceFront, int referenceBack ) {
	stencilReferenceFront = referenceFront;
	stencilReferenceBack = referenceBack;
}

void GpuDepthStencilState::setStencilMask( uint mask ) {
	stencilMaskFront = mask;
	stencilMaskBack = mask;
}

void GpuDepthStencilState::setStencilMasks( uint maskFront, uint maskBack ) {
	stencilMaskFront = maskFront;
	stencilMaskBack = maskBack;
}

void GpuDepthStencilState::setStencilOperation(
	GLenum operationStencilFail, GLenum operationDepthFail, GLenum operationDepthPass ) {
	stencilOperationStencilFailFront = operationStencilFail;
	stencilOperationStencilFailBack = operationStencilFail;
	stencilOperationDepthFailFront = operationDepthFail;
	stencilOperationDepthFailBack = operationDepthFail;
	stencilOperationDepthPassFront = operationDepthPass;
	stencilOperationDepthPassBack = operationDepthPass;
}

void GpuDepthStencilState::setStencilOperations(
	GLenum operationStencilFailFront, GLenum operationDepthFailFront, GLenum operationDepthPassFront,
	GLenum operationStencilFailBack, GLenum operationDepthFailBack, GLenum operationDepthPassBack ) {
	stencilOperationStencilFailFront = operationStencilFailFront;
	stencilOperationStencilFailBack = operationStencilFailBack;
	stencilOperationDepthFailFront = operationDepthFailFront;
	stencilOperationDepthFailBack = operationDepthFailBack;
	stencilOperationDepthPassFront = operationDepthPassFront;
	stencilOperationDepthPassBack = operationDepthPassBack;
}

void GpuDepthStencilState::setStencilWriteMask( uint mask ) {
	stencilWriteMaskFront = mask;
	stencilWriteMaskBack = mask;
}

void GpuDepthStencilState::setStencilWriteMasks( uint maskFront, uint maskBack ) {
	stencilWriteMaskFront = maskFront;
	stencilWriteMaskBack = maskBack;
}

bool GpuDepthStencilState::isDepthTestEnabled() const {
	return depthTestEnabled;
}

GLenum GpuDepthStencilState::getDepthFunction() const {
	return depthFunction;
}

bool GpuDepthStencilState::isDepthWriteEnabled() const {
	return depthWriteEnabled;
}

bool GpuDepthStencilState::isStencilTestEnabled() const {
	return stencilTestEnabled;
}

GLenum GpuDepthStencilState::getStencilFunctionFront() const {
	return stencilFunctionFront;
}

GLenum GpuDepthStencilState::getStencilFunctionBack() const {
	return stencilFunctionBack;
}

GLenum GpuDepthStencilState::getStencilReferenceFront() const {
	return stencilReferenceFront;
}

GLenum GpuDepthStencilState::getStencilReferenceBack() const {
	return stencilReferenceBack;
}

uint GpuDepthStencilState::getStencilMaskFront() const {
	return stencilMaskFront;
}

uint GpuDepthStencilState::getStencilMaskBack() const {
	return stencilMaskBack;
}

GLenum GpuDepthStencilState::getStencilOperationStencilFailFront() const {
	return stencilOperationStencilFailFront;
}

GLenum GpuDepthStencilState::getStencilOperationDepthFailFront() const {
	return stencilOperationDepthFailFront;
}

GLenum GpuDepthStencilState::getStencilOperationDepthPassFront() const {
	return stencilOperationDepthPassFront;
}

GLenum GpuDepthStencilState::getStencilOperationStencilFailBack() const {
	return stencilOperationStencilFailBack;
}

GLenum GpuDepthStencilState::getStencilOperationDepthFailBack() const {
	return stencilOperationDepthFailBack;
}

GLenum GpuDepthStencilState::getStencilOperationDepthPassBack() const {
	return stencilOperationDepthPassBack;
}

uint GpuDepthStencilState::getStencilWriteMaskFront() const {
	return stencilWriteMaskFront;
}

uint GpuDepthStencilState::getStencilWriteMaskBack() const {
	return stencilWriteMaskBack;
}

GpuDepthStencilState::GpuDepthStencilState( GpuState * state )
	: GpuObject( state ) {
	depthTestEnabled = false;
	depthFunction = GL_LEQUAL; // note: OpenGL's default is GL_LESS
	depthWriteEnabled = true;

	stencilTestEnabled = false;
	stencilFunctionFront = GL_ALWAYS;
	stencilFunctionBack = GL_ALWAYS;
	stencilReferenceFront = 0;
	stencilReferenceBack = 0;
	stencilMaskFront = 0xffffffff;
	stencilMaskBack = 0xffffffff;
	stencilOperationStencilFailFront = GL_KEEP;
	stencilOperationStencilFailBack = GL_KEEP;
	stencilOperationDepthFailFront = GL_KEEP;
	stencilOperationDepthFailBack = GL_KEEP;
	stencilOperationDepthPassFront = GL_KEEP;
	stencilOperationDepthPassBack = GL_KEEP;
	stencilWriteMaskFront = 0xffffffff;
	stencilWriteMaskBack = 0xffffffff;
}

void GpuDepthStencilState::apply( GpuDepthStencilState & currentState ) const {
	setStateEnabled( depthTestEnabled, currentState.depthTestEnabled, GL_DEPTH_TEST );

	if (depthTestEnabled) {
		if (depthFunction != currentState.depthFunction) {
			glDepthFunc( depthFunction );
			currentState.depthFunction = depthFunction;
		}
	}

	// depth write mask isn't changed using glEnable/glDisable
	if (depthWriteEnabled != currentState.depthWriteEnabled) {
		glDepthMask( depthWriteEnabled );
		currentState.depthWriteEnabled = depthWriteEnabled;
	}

	setStateEnabled( stencilTestEnabled, currentState.stencilTestEnabled, GL_STENCIL_TEST );

	if (stencilTestEnabled) {
		if (stencilFunctionFront != currentState.stencilFunctionFront ||
			stencilReferenceFront != currentState.stencilReferenceFront ||
			stencilMaskFront != currentState.stencilMaskFront) {
			glStencilFuncSeparate( GL_FRONT, stencilFunctionFront, stencilReferenceFront, stencilMaskFront );
			currentState.stencilFunctionFront = stencilFunctionFront;
			currentState.stencilReferenceFront = stencilReferenceFront;
			currentState.stencilMaskFront = stencilMaskFront;
		}

		if (stencilFunctionBack != currentState.stencilFunctionBack ||
			stencilReferenceBack != currentState.stencilReferenceBack ||
			stencilMaskBack != currentState.stencilMaskBack) {
			glStencilFuncSeparate( GL_BACK, stencilFunctionBack, stencilReferenceBack, stencilMaskBack );
			currentState.stencilFunctionBack = stencilFunctionBack;
			currentState.stencilReferenceBack = stencilReferenceBack;
			currentState.stencilMaskBack = stencilMaskBack;
		}
	}

	if (stencilWriteMaskFront != currentState.stencilWriteMaskFront) {
		glStencilMaskSeparate( GL_FRONT, stencilWriteMaskFront );
		currentState.stencilWriteMaskFront = stencilWriteMaskFront;
	}

	if (stencilWriteMaskBack != currentState.stencilWriteMaskBack) {
		glStencilMaskSeparate( GL_BACK, stencilWriteMaskBack );
		currentState.stencilWriteMaskBack = stencilWriteMaskBack;
	}
}

void GpuDepthStencilState::getCurrentState() {
	depthTestEnabled = glIsEnabled( GL_DEPTH_TEST ) != 0;
	glGetIntegerv( GL_DEPTH_FUNC, (GLint*)&depthFunction );
	GLboolean depthWriteEnabledResult;
	glGetBooleanv( GL_DEPTH_WRITEMASK, &depthWriteEnabledResult );
	depthWriteEnabled = (depthWriteEnabledResult != 0);

	stencilTestEnabled = glIsEnabled( GL_STENCIL_TEST ) != 0;
	glGetIntegerv( GL_STENCIL_FUNC, (GLint*)&stencilFunctionFront );
	glGetIntegerv( GL_STENCIL_BACK_FUNC, (GLint*)&stencilFunctionBack );
	glGetIntegerv( GL_STENCIL_REF, &stencilReferenceFront );
	glGetIntegerv( GL_STENCIL_BACK_REF, &stencilReferenceBack );
	glGetIntegerv( GL_STENCIL_FAIL, (GLint*)&stencilOperationStencilFailFront );
	glGetIntegerv( GL_STENCIL_BACK_FAIL, (GLint*)&stencilOperationStencilFailBack );
	glGetIntegerv( GL_STENCIL_PASS_DEPTH_FAIL, (GLint*)&stencilOperationDepthFailFront );
	glGetIntegerv( GL_STENCIL_BACK_PASS_DEPTH_FAIL, (GLint*)&stencilOperationDepthFailBack );
	glGetIntegerv( GL_STENCIL_PASS_DEPTH_PASS, (GLint*)&stencilOperationDepthPassFront );
	glGetIntegerv( GL_STENCIL_BACK_PASS_DEPTH_PASS, (GLint*)&stencilOperationDepthPassFront );
	glGetIntegerv( GL_STENCIL_WRITEMASK, (GLint*)&stencilWriteMaskFront );
	glGetIntegerv( GL_STENCIL_BACK_WRITEMASK, (GLint*)&stencilWriteMaskBack );
}