#include "Cone.h"

Cone::Cone() {
}

void Cone::generate( size_t segments ) {
	vertices.clear();
	indices.clear();

	segments = std::max <size_t>( 3, segments ); // must have at least 3 segments

	// cone is positioned pointing UP the z-axis 1 unit and with a radius of 1
	//     z
	//   __|__
	//  |__|__|
	//   \ | /
	//    \|/______ y
	//    /
	//   /
	//  x

	size_t vertexCount = segments + 1; // 1 extra vertex for cone point
	size_t triangleCount = segments*2 - 2; // "segments" triangles on cone, "segments-2" triangles on cap

	vertices.reserve( vertexCount );
	indices.reserve( triangleCount*3 );

	// cone vertex is the origin
	vertices.push_back( Vector3f() ); // 0
	for (size_t i = 0; i < segments; ++i) {
		float theta = 2.0f * boost::math::constants::pi <float>() * (float)i / (float)segments;
		vertices.push_back( Vector3f( cos( theta ), sin( theta ), 1.0f ) );
	}
	// cone triangles
	for (size_t i = 0; i < segments; ++i) {
		indices.push_back( 0 );
		indices.push_back( (i+1) % segments + 1 );
		indices.push_back( i+1 );
	}
	// cap triangles
	for (size_t i = 0; i < segments-2; ++i) {
		indices.push_back( 1 );
		indices.push_back( i+2 );
		indices.push_back( i+3 );
	}

	inscriptionRadiusMultiplier = 1.0f / (0.5f * (vertices[1].xy() + vertices[2].xy())).magnitude();
}

const std::vector <Vector3f> & Cone::getVertices() const {
	return vertices;
}

const std::vector <ushort> & Cone::getIndices() const {
	return indices;
}

float Cone::getInscriptionRadiusMultiplier() const {
	return inscriptionRadiusMultiplier;
}