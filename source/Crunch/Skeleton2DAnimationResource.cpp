#include "Skeleton2DAnimationResource.h"
#include "Log.h"

Skeleton2DAnimationResource::Skeleton2DAnimationResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m ) {
}

bool Skeleton2DAnimationResource::loadDataAsync() {
	try {
		// open and load the file
		DataReader in( getPath() );
		if (in.fail())
			throw std::runtime_error( "Failed to open " + getPath() );

		uint partCount;
		if (!in.readElements( &partCount, 1 ))
			throw std::runtime_error( "Failed to read part count" );

		for (uint i = 0; i < partCount; ++i) {
			uint partId;
			if (!in.readElements( &partId, 1 ))
				throw std::runtime_error( "Failed to read part ID" );
			partIds.push_back( (size_t)partId );
		}

		if (!in.readElements( &length, 1 ))
			throw std::runtime_error( "Failed to read length" );

		uint keyframeCount;
		if (!in.readElements( &keyframeCount, 1 ))
			throw std::runtime_error( "Failed to read keyframe count" );
		if (keyframeCount == 0)
			throw std::runtime_error( "No keyframes specified" );

		keyframes.reserve( (size_t)keyframeCount );
		for (uint i = 0; i < keyframeCount; ++i) {
			keyframes.push_back( KeyframeData() );
			KeyframeData & keyframeData = keyframes.back();

			if (!in.readElements( &keyframeData.keyframeTime, 1 ))
				throw std::runtime_error( "Failed to read keyframe time" );

			keyframeData.parts.reserve( (size_t)partCount );
			for (uint t = 0; t < partCount; ++t) {
				keyframeData.parts.push_back( AnimatedPartData() );
				if (!in.readElements( &keyframeData.parts.back(), 1 ))
					throw std::runtime_error( "Failed to read animated part data" );
			}
		}

		struct SortKeyframes {
			bool operator()( const KeyframeData & k1, const KeyframeData & k2 ) const {
				return k1.keyframeTime < k2.keyframeTime;
			}
		};

		std::sort( keyframes.begin(), keyframes.end(), SortKeyframes() );

		// verify
		for (size_t i = 1; i < keyframes.size(); ++i) {
			if (keyframes[i-1].keyframeTime == keyframes[i].keyframeTime)
				throw std::runtime_error( "Duplicate keyframe time specified" );
		}

	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}

	return true;
}

bool Skeleton2DAnimationResource::loadDataSync() {
	return true;
}

void Skeleton2DAnimationResource::freeData() {
}

size_t Skeleton2DAnimationResource::getPartCount() const {
	return partIds.size();
}

size_t Skeleton2DAnimationResource::getPartId( size_t i ) const {
	return partIds[i];
}

uint Skeleton2DAnimationResource::getLength() const {
	return length;
}

size_t Skeleton2DAnimationResource::getKeyframeCount() const {
	return keyframes.size();
}

const Skeleton2DAnimationResource::KeyframeData & Skeleton2DAnimationResource::getKeyframeData( size_t i ) const {
	return keyframes[i];
}