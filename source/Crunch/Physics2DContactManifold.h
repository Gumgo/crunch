/** @file Physics2DContactManifold.h
 *  @brief Functions to determine the contact manifold between shapes.
 */

#ifndef PHYSICS2DCONTACTMANIFOLD_DEFINED
#define PHYSICS2DCONTACTMANIFOLD_DEFINED

#include "Physics2D.h"
#include "Physics2DCapsuleShape.h"
#include "Physics2DCircleShape.h"
#include "Physics2DConvexPolygonShape.h"
#include "Physics2DEdgeShape.h"
#include "Physics2DEllipseShape.h"

namespace Physics2D {

// shapes: capsule, circle, convex polygon, edge, ellipse
// the convention is for the normal to point from s0 to s1

// capsule

/** @brief Computes the contact manifold between a capsule and a capsule.
 */
void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const CapsuleShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a capsule and a circle.
 */
void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const CircleShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a capsule and a convex polygon.
 */
void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const ConvexPolygonShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a capsule and an edge.
 */
void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a capsule and an ellipse.
 */
void getContactManifold( Manifold * manifold, const CapsuleShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 );

// circle

/** @brief Computes the contact manifold between a circle and a circle.
 */
void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const CircleShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a circle and a convex polygon.
 */
void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const ConvexPolygonShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a circle and an edge.
 */
void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a circle and an ellipse.
 */
void getContactManifold( Manifold * manifold, const CircleShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 );

// convex polygon

/** @brief Computes the contact manifold between a convex polygon and a convex polygon.
 */
void getContactManifold( Manifold * manifold, const ConvexPolygonShape * s0, const Matrix33af & tf0, const ConvexPolygonShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a convex polygon and an edge.
 */
void getContactManifold( Manifold * manifold, const ConvexPolygonShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between a convex polygon and an ellipse.
 */
void getContactManifold( Manifold * manifold, const ConvexPolygonShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 );

// edge

/** @brief Computes the contact manifold between an edge and an edge.
 */
void getContactManifold( Manifold * manifold, const EdgeShape * s0, const Matrix33af & tf0, const EdgeShape * s1, const Matrix33af & tf1 );

/** @brief Computes the contact manifold between an edge and an ellipse.
 */
void getContactManifold( Manifold * manifold, const EdgeShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 );

// ellipse

/** @brief Computes the contact manifold between an ellipse and an ellipse.
 */
void getContactManifold( Manifold * manifold, const EllipseShape * s0, const Matrix33af & tf0, const EllipseShape * s1, const Matrix33af & tf1 );

}

#endif