/** @file Semaphore.h
 *  @brief A semaphore class.
 */

#ifndef SEMAPHORE_DEFINED
#define SEMAPHORE_DEFINED

#include "Common.h"
#include <boost/thread.hpp>

/** @brief A semaphore class.
 */
class Semaphore {
public:
	Semaphore();	/**< Initializes count to 0.*/
	void signal();	/**< Increments count.*/
	void wait();	/**< Waits until count is greater than 0 and decrements count.*/

private:
	boost::mutex mutex;						// the mutex
	boost::condition_variable condition;	// the condition variable
	uint count;								// the count
};

#endif