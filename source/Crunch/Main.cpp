#include "Main.h"

int crunchMain( int argc, char ** argv, Context::RegistrationFunction registrationFunction ) {

	memDbgInit();

	{
		Log::createStreamList();
		Log::addStream( std::cout );

		try {
			std::vector <std::string> args;
			for (int i = 1; i < argc; ++i)
				args.push_back( std::string( argv[i] ) );

			Runner runner( args, registrationFunction );
		} catch (const std::exception &) {
		}

		Log::freeStreamList();
	}

	memDbgTerm();

	return 0;
}