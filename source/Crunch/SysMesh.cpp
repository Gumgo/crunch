#include "SysMesh.h"
#include "Context.h"

SysMesh::SysMesh() {
}

SysMesh::~SysMesh() {
}

void SysMesh::setMesh( MeshResourceReference & m ) {
	mesh = m;
}

MeshResourceReference SysMesh::getMesh() const {
	return mesh;
}

void SysMesh::draw( SysAutoProgram & ap ) {
	if (!mesh || mesh->getStatus() != Resource::S_LOADED || mesh->getVertexCount() == 0)
		return;

	mesh->getVertexBuffer()->bind();
	mesh->getIndexBuffer()->bind();

	ap.bind(); // bind shader attributes and uniforms

	GLenum indexType;
	switch (mesh->getIndexSize()) {
	case 1:
		indexType = GL_UNSIGNED_BYTE;
		break;
	case 2:
		indexType = GL_UNSIGNED_SHORT;
		break;
	case 4:
		indexType = GL_UNSIGNED_INT;
		break;
	default:
		assert( false );
	}
	GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
	drawCall.setParameters( GL_TRIANGLES, 0, mesh->getVertexCount()-1, mesh->getIndexCount(), indexType, bufferOffset( 0 ) );
	drawCall.draw();
}