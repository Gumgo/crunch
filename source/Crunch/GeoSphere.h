#ifndef GEOSPHERE_DEFINED
#define GEOSPHERE_DEFINED

#include "Common.h"
#include <vector>

// provides geometry for a geosphere
class GeoSphere {
public:
	GeoSphere();
	void generate( size_t subdivisions );
	const std::vector <Vector3f> & getVertices() const;
	const std::vector <ushort> & getIndices() const;
	float getInscriptionRadiusMultiplier() const;

private:
	struct Edge {
		bool split; // whether this edge has been split
		Vector2 <size_t> vertices; // the two endpoint vertex indices
		size_t splitVertex; // the split vertex index
		Vector2 <size_t> splitEdges; // the two edges after splitting - they correspond to the "vertices" vector

		Edge( size_t v0, size_t v1 );

		// returns the sub edge containing the endpoint vertex provided
		size_t getSplitEdge( size_t vertex );
	};

	struct Triangle {
		Vector3 <size_t> vertices; // the vertex indices
		Vector3 <size_t> edges; // the edge indices

		Triangle( size_t v0, size_t v1, size_t v2, size_t e0, size_t e1, size_t e2 );
	};

	std::vector <Vector3f> vertices;
	std::vector <Edge> edges;
	std::vector <Edge> newEdges;
	std::vector <Triangle> triangles;
	std::vector <Triangle> newTriangles;

	std::vector <ushort> indices;
	float inscriptionRadiusMultiplier;

	void splitEdge( Edge & e ); // splits an edge
	void subdivideTriangle( const Triangle & t ); // subdivides a triangle

	void subdivide(); // makes a subdivision pass
	float computeInscriptionRadiusMultiplier() const;
};

#endif