/** @file BinarySearch.h
 *  @brief Provides a function to perform a binary search on vector of key/value pairs.
 */

#ifndef BINARYSEARCH_DEFINED
#define BINARYSEARCH_DEFINED

#include "Common.h"

template <typename V> class ArrayValueComparator {
private:
	const V * a;	// the array to search
	const V & r;	// the reference value

public:
	/** @brief Constructor takes the array and reference value.
	 */
	ArrayValueComparator( const V * arr, const V & ref );

	/** @brief Returns -1 if arr[index] < ref, 0 if arr[index] == ref, or 1 if arr[index] > ref.
	 */
	int operator()( size_t index ) const {
		return (arr[index] > ref) - (arr[index] < ref);
	}
};

/** @brief Performs a binary search on the given range of indices using the given comparator.
 *
 *  If the value exists, its index is stored in index. If it does not exist,
 *  the index where it would exist is stored in index.
 *  The comparator is responsible for providing the reference value.
 *  The comparator should implement int operator()( size_t ) const, with the following properties:
 *  - A negative value should be returned if the value at the given index is less than the reference value.
 *  - Zero should be returned if value at the given index is equal to the reference value.
 *  - A positive value should be returned if the value at the given index is greater than the reference value.
 */
template <typename C> bool binarySearch( size_t first, size_t last, size_t & index, const C & comparator ) {
	size_t b = first, e = last;
	while (b < e) {
		size_t mid = (b + e) / 2; // optimized to >> 1
		int cmp = comparator( mid );
		if (cmp > 0)
			// v[mid] > ref
			e = mid;
		else if (cmp < 0)
			// v[mid] < ref
			b = mid + 1;
		else {
			// v[mid] == ref
			index = mid;
			return true;
		}
	}

	if (b == last || comparator( b ) >= 0)
		// insertion index is past the end, or v[b] >= ref
		index = b;
	else
		// v[b] < ref; 
		index = b + 1;
	return false;
}

#endif