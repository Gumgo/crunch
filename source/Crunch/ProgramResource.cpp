#include "ProgramResource.h"
#include "Context.h"
#include "Log.h"

ProgramResource::ProgramResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m ) 
	: Resource( rm, n, p, m ) {
}

void processLine( std::string & out, const std::string & currentPath, const std::string & line, const std::string & trimmed,
	const char * defineString ) {

	static const char * INCLUDE_STRING = "#include";
	static const size_t INCLUDE_LENGTH = strlen( INCLUDE_STRING );
	static const char * VERSION_STRING = "#version";
	static const size_t VERSION_LENGTH = strlen( VERSION_STRING );

	bool skip = false;
	// check for #include and #version
	if (trimmed.length() >= INCLUDE_LENGTH+1 && trimmed.substr( 0, INCLUDE_LENGTH ) == INCLUDE_STRING &&
		(trimmed[INCLUDE_LENGTH] == ' ' || trimmed[INCLUDE_LENGTH] == '\t')) {
		// find next non-whitespace char
		size_t i;
		for (i = 9; i < trimmed.length(); ++i) {
			if (trimmed[i] != ' ' && trimmed[i] != '\t')
				break;
		}
		if (i < trimmed.length()) {
			std::string fnameQuotes = trimmed.substr( i );
			if (fnameQuotes.length() >= 2 && fnameQuotes[0] == '"' && fnameQuotes[fnameQuotes.size()-1] == '"') {
				skip = true;
				std::string newPath = fnameQuotes.substr( 1, fnameQuotes.length() - 2 );
				if (newPath.empty())
					throw std::runtime_error( "Empty #include directive in " + currentPath );

				if (newPath[0] != '/' && newPath[0] != '\\') {
					size_t s;
					for (s = currentPath.size(); s > 0; --s) {
						char ch = currentPath[s-1];
						if (ch == '/' || ch == '\\')
							break;
					}
					std::string currentLocalPath = currentPath.substr( 0, s );
					newPath = currentLocalPath + newPath;
				}

				std::ifstream in( newPath );
				if (!in.is_open())
					throw std::runtime_error( "Failed to open " + newPath );

				std::string line, sub;
				while (!in.eof()) {
					std::getline( in, line );
					// trim whitespace
					size_t start = 0, end = line.length();
					while (start+1 < line.length() && (line[start+1] == ' ' || line[start+1] == '\t'))
						++start;
					while (end > 0 && end-1 > start && (line[end-1] == ' ' || line[end-1] == '\t'))
						--end;
					sub = line.substr( start, end-start );
					processLine( out, newPath, line, sub, defineString );
				}

			}
		}
	} else if (trimmed.length() >= VERSION_LENGTH+1 && trimmed.substr( 0, VERSION_LENGTH ) == VERSION_STRING &&
		(trimmed[VERSION_LENGTH] == ' ' || trimmed[VERSION_LENGTH] == '\t')) {
		// append a #define after we encounter #version
		skip = true;
		out += line + '\n' + defineString + '\n';
	}

	if (!skip)
		out += line + '\n';
}

bool ProgramResource::loadDataAsync() {
	try {
		std::ifstream in( getPath() );
		if (!in.is_open())
			throw std::runtime_error( "Failed to open " + getPath() );

		static const char * defines[SHADER_TYPE_COUNT] = { "#define vertex_shader", "#define geometry_shader", "#define fragment_shader" };
		static const char * shaderNames[SHADER_TYPE_COUNT] = { "Vertex", "Geometry", "Fragment" };
		static const char * shaderDeclarations[SHADER_TYPE_COUNT] = { "#vertex_shader", "#geometry_shader", "#fragment_shader" };
		bool declared[SHADER_TYPE_COUNT];
		for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i)
			declared[i] = false;

		std::string line, sub;
		size_t currentShader = SHADER_TYPE_COUNT + 1;
		while (!in.eof()) {
			std::getline( in, line );
			// trim whitespace
			size_t start = 0, end = line.length();
			while (start+1 < line.length() && (line[start+1] == ' ' || line[start+1] == '\t'))
				++start;
			while (end > 0 && end-1 > start && (line[end-1] == ' ' || line[end-1] == '\t'))
				--end;
			sub = line.substr( start, end-start );
			bool declarationFound = false;
			for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i) {
				if (sub == shaderDeclarations[i]) {
					if (declared[i])
						throw std::runtime_error( std::string( shaderNames[i] ) + " declared multiple times" );
					declared[i] = true;
					currentShader = i;
					declarationFound = true;
					break;
				}
			}
			if (!declarationFound && currentShader < SHADER_TYPE_COUNT)
				processLine( source[currentShader], getPath(), line, sub, defines[currentShader] );
		}
		for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i) {
			if (declared[i] && source[i].empty())
				throw std::runtime_error( std::string( shaderNames[i] ) + " declared but no source code found" );
		}
	} catch (const std::exception & e) {
		Log::error() << e.what();
		for (size_t i = 0; i < arraySize( source ); ++i) {
			source[i].clear();
			source[i].shrink_to_fit();
		}
		return false;
	}

	return true;
}

bool ProgramResource::loadDataSync() {
	try {
		GpuProgram::ProgramDescription desc;
		const char * sourcePtrs[SHADER_TYPE_COUNT];
		for (size_t i = 0; i < SHADER_TYPE_COUNT; ++i)
			sourcePtrs[i] = source[i].c_str();
		if (!source[0].empty())
			desc.vertexShaderSource = &sourcePtrs[0];
		if (!source[1].empty())
			desc.geometryShaderSource = &sourcePtrs[1];
		if (!source[2].empty())
			desc.fragmentShaderSource = &sourcePtrs[2];

		program = Context::get().gpuState.createProgram( desc );

		// no longer need the source
		for (size_t i = 0; i < arraySize( source ); ++i) {
			source[i].clear();
			source[i].shrink_to_fit();
		}
	} catch (const std::exception & e) {
		Log::error() << e.what();
		for (size_t i = 0; i < arraySize( source ); ++i) {
			source[i].clear();
			source[i].shrink_to_fit();
		}
		return false;
	}

	return true;
}

void ProgramResource::freeData() {
}

GpuProgramReference ProgramResource::getProgram() {
	return program;
}

GpuProgramConstReference ProgramResource::getProgram() const {
	return program;
}