#include "SysCamera2D.h"

void SysCamera2D::computeViewProjectionMatrix() {
	// viewProjectionMatrix = projectionMatrix * viewMatrix
	viewProjectionMatrix.set(
		projectionMatrix[ 0]*viewMatrix[0] + projectionMatrix[ 1]*viewMatrix[3],
		projectionMatrix[ 0]*viewMatrix[1] + projectionMatrix[ 1]*viewMatrix[4],
		projectionMatrix[ 2],
		projectionMatrix[ 0]*viewMatrix[2] + projectionMatrix[ 1]*viewMatrix[5] + projectionMatrix[ 3],
		projectionMatrix[ 4]*viewMatrix[0] + projectionMatrix[ 5]*viewMatrix[3],
		projectionMatrix[ 4]*viewMatrix[1] + projectionMatrix[ 5]*viewMatrix[4],
		projectionMatrix[ 6],
		projectionMatrix[ 4]*viewMatrix[2] + projectionMatrix[ 5]*viewMatrix[5] + projectionMatrix[ 7],
		projectionMatrix[ 8]*viewMatrix[0] + projectionMatrix[ 9]*viewMatrix[3],
		projectionMatrix[ 8]*viewMatrix[1] + projectionMatrix[ 9]*viewMatrix[4],
		projectionMatrix[10],
		projectionMatrix[ 8]*viewMatrix[2] + projectionMatrix[ 9]*viewMatrix[5] + projectionMatrix[11],
		projectionMatrix[12]*viewMatrix[0] + projectionMatrix[13]*viewMatrix[3],
		projectionMatrix[12]*viewMatrix[1] + projectionMatrix[13]*viewMatrix[4],
		projectionMatrix[14],
		projectionMatrix[12]*viewMatrix[2] + projectionMatrix[13]*viewMatrix[5] + projectionMatrix[15] );
}

void SysCamera2D::setProjection( float pLeft, float pRight, float pBottom, float pTop ) {
	setProjection( pLeft, pRight, pBottom, pTop, -1.0f, 1.0f );
}

void SysCamera2D::setProjection( float pLeft, float pRight, float pBottom, float pTop, float pNear, float pFar ) {
	projLeft = pLeft;
	projRight = pRight;
	projBottom = pBottom;
	projTop = pTop;
	projNear = pNear;
	projFar = pFar;

	projectionMatrix.set(
		2.0f/(pRight-pLeft),	0.0f,					0.0f,				(pRight+pLeft)/(pLeft-pRight),
		0.0f,					2.0f/(pTop-pBottom),	0.0f,				(pTop+pBottom)/(pBottom-pTop),
		0.0f,					0.0f,					2.0f/(pNear-pFar),	(pFar+pNear)/(pNear-pFar),
		0.0f,					0.0f,					0.0f,				1.0f );

	computeViewProjectionMatrix();
}

void SysCamera2D::setCamera( const Vector2f & position ) {
	viewMatrix.identity();
	viewMatrix[2] = -position.x;
	viewMatrix[5] = -position.y;
	cameraMatrix.identity();
	cameraMatrix[2] = position.x;
	cameraMatrix[5] = position.y;
	computeViewProjectionMatrix();
}

void SysCamera2D::setCameraCenter( const Vector2f & centerPosition ) {
	viewMatrix.identity();
	viewMatrix[2] = -centerPosition.x + (projRight-projLeft);
	viewMatrix[5] = -centerPosition.y + (projTop-projBottom);
	computeViewProjectionMatrix();
}

void SysCamera2D::setViewMatrix( const Matrix33af & matrix ) {
	viewMatrix = matrix;
	cameraMatrix = matrix.inverse();
	computeViewProjectionMatrix();
}

void SysCamera2D::setCameraMatrix( const Matrix33af & matrix ) {
	viewMatrix = matrix.inverse();
	cameraMatrix = matrix;
	computeViewProjectionMatrix();
}

float SysCamera2D::getLeft() const {
	return projLeft;
}

float SysCamera2D::getRight() const {
	return projRight;
}

float SysCamera2D::getBottom() const {
	return projBottom;
}

float SysCamera2D::getTop() const {
	return projTop;
}

float SysCamera2D::getNear() const {
	return projNear;
}

float SysCamera2D::getFar() const {
	return projFar;
}

const Matrix44f & SysCamera2D::getProjectionMatrix() const {
	return projectionMatrix;
}

const Matrix33af & SysCamera2D::getViewMatrix() const {
	return viewMatrix;
}

const Matrix33af & SysCamera2D::getCameraMatrix() const {
	return cameraMatrix;
}

const Matrix44f & SysCamera2D::getViewProjectionMatrix() const {
	return viewProjectionMatrix;
}

Matrix33af SysCamera2D::getModelViewMatrix( const Matrix22f & modelMatrix ) const {
	return viewMatrix * modelMatrix;
}

Matrix33af SysCamera2D::getModelViewMatrix( const Matrix33af & modelMatrix ) const {
	return viewMatrix * modelMatrix;
}

Matrix44f SysCamera2D::getModelViewProjectionMatrix( const Matrix22f & modelMatrix ) const {
	// viewProjectionMatrix * modelMatrix
	return Matrix44f(
		viewProjectionMatrix[ 0]*modelMatrix[0] + viewProjectionMatrix[ 1]*modelMatrix[2],
		viewProjectionMatrix[ 0]*modelMatrix[1] + viewProjectionMatrix[ 1]*modelMatrix[3],
		viewProjectionMatrix[ 2],
		viewProjectionMatrix[ 3],
		viewProjectionMatrix[ 4]*modelMatrix[0] + viewProjectionMatrix[ 5]*modelMatrix[2],
		viewProjectionMatrix[ 4]*modelMatrix[1] + viewProjectionMatrix[ 5]*modelMatrix[3],
		viewProjectionMatrix[ 6],
		viewProjectionMatrix[ 7],
		viewProjectionMatrix[ 8]*modelMatrix[0] + viewProjectionMatrix[ 9]*modelMatrix[2],
		viewProjectionMatrix[ 8]*modelMatrix[1] + viewProjectionMatrix[ 9]*modelMatrix[3],
		viewProjectionMatrix[10],
		viewProjectionMatrix[11],
		viewProjectionMatrix[12]*modelMatrix[0] + viewProjectionMatrix[13]*modelMatrix[2],
		viewProjectionMatrix[12]*modelMatrix[1] + viewProjectionMatrix[13]*modelMatrix[3],
		viewProjectionMatrix[14],
		viewProjectionMatrix[15] );
}

Matrix44f SysCamera2D::getModelViewProjectionMatrix( const Matrix33af & modelMatrix ) const {
	// viewProjectionMatrix * modelMatrix
	return Matrix44f(
		viewProjectionMatrix[ 0]*modelMatrix[0] + viewProjectionMatrix[ 1]*modelMatrix[3],
		viewProjectionMatrix[ 0]*modelMatrix[1] + viewProjectionMatrix[ 1]*modelMatrix[4],
		viewProjectionMatrix[ 2],
		viewProjectionMatrix[ 0]*modelMatrix[2] + viewProjectionMatrix[ 1]*modelMatrix[5] + viewProjectionMatrix[ 3],
		viewProjectionMatrix[ 4]*modelMatrix[0] + viewProjectionMatrix[ 5]*modelMatrix[3],
		viewProjectionMatrix[ 4]*modelMatrix[1] + viewProjectionMatrix[ 5]*modelMatrix[4],
		viewProjectionMatrix[ 6],
		viewProjectionMatrix[ 4]*modelMatrix[2] + viewProjectionMatrix[ 5]*modelMatrix[5] + viewProjectionMatrix[ 7],
		viewProjectionMatrix[ 8]*modelMatrix[0] + viewProjectionMatrix[ 9]*modelMatrix[3],
		viewProjectionMatrix[ 8]*modelMatrix[1] + viewProjectionMatrix[ 9]*modelMatrix[4],
		viewProjectionMatrix[10],
		viewProjectionMatrix[ 8]*modelMatrix[2] + viewProjectionMatrix[ 9]*modelMatrix[5] + viewProjectionMatrix[11],
		viewProjectionMatrix[12]*modelMatrix[0] + viewProjectionMatrix[13]*modelMatrix[3],
		viewProjectionMatrix[12]*modelMatrix[1] + viewProjectionMatrix[13]*modelMatrix[4],
		viewProjectionMatrix[14],
		viewProjectionMatrix[12]*modelMatrix[2] + viewProjectionMatrix[13]*modelMatrix[5] + viewProjectionMatrix[15] );
}

Vector2f SysCamera2D::screenToWorld( const Vector2f & screen ) const {
	return cameraMatrix.transformPoint(
		lerp( Vector2f( (float)projLeft, (float)projTop ), Vector2f( (float)projRight, (float)projBottom ), screen ) );
}