#include "SysGui.h"

void SysGui::onCreate() {
	camera = NULL;
	deactivate();
	hide();
}

void SysGui::setCamera( const SysCamera2D * cam ) {
	camera = cam;
}

void SysGui::setFont( FontResourceReference & f ) {
	font = f;
}

FontResourceReference SysGui::getFont() {
	return font;
}

FontResourceConstReference SysGui::getFont() const {
	return font;
}

void SysGui::onActivate() {
	deactivate();
}