#include "Skeleton3DAnimationResource.h"
#include "Log.h"

Skeleton3DAnimationResource::Skeleton3DAnimationResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m ) {
}

bool Skeleton3DAnimationResource::loadDataAsync() {
	try {
		// open and load the file
		KeyValueReader in( getPath() );

		KeyValueReader::Iterator it = in.getIterator();

		length = it.getFloat( "length" );
		fadeInTime = it.getFloat( "fadeInTime" );
		fadeOutTime = it.getFloat( "fadeOutTime" );

		KeyValueReader::Iterator animatedBonesIt = it.getArray( "animatedBones" );
		animatedBones.reserve( animatedBonesIt.getCount() );
		for (size_t i = 0; i < animatedBonesIt.getCount(); ++i) {
			KeyValueReader::Iterator animatedBoneIt = animatedBonesIt.getArray( i );
			animatedBones.push_back( AnimatedBoneData() );
			AnimatedBoneData & animatedBone = animatedBones.back();

			animatedBone.boneId = (size_t)animatedBoneIt.getUint( "boneId" );
			// check to see if this bone ID is duplicated; naive check should be enough, since there won't be many bones
			if (i > 0) {
				// make sure to do this check, since i-1 is a big value if i = 0!
				for (size_t t = 0; t < i-1; ++t) {
					if (animatedBones[t].boneId == animatedBone.boneId)
						throw std::runtime_error( "Duplicate bone specified" );
				}
			}

			// iterate over keyframes
			KeyValueReader::Iterator keyframesIt = animatedBoneIt.getArray( "keyframes" );
			if (keyframesIt.getCount() == 0)
				throw std::runtime_error( "No keyframes specified" );
			animatedBone.keyframes.reserve( keyframesIt.getCount() );
			for (size_t kf = 0; kf < keyframesIt.getCount(); ++kf) {
				KeyValueReader::Iterator keyframeIt = keyframesIt.getArray( kf );
				animatedBone.keyframes.push_back( KeyframeData() );
				KeyframeData & keyframe = animatedBone.keyframes.back();

				keyframe.time = keyframeIt.getFloat( "time" );
				// make sure keyframes are ordered
				if (kf > 0 && keyframe.time <= animatedBone.keyframes[kf-1].time)
					throw std::runtime_error( "Invalid keyframe ordering" );

				if (keyframeIt.getBinaryDataLength( "translation" ) != sizeof( keyframe.translation ) ||
					keyframeIt.getBinaryDataLength( "rotation" ) != sizeof( keyframe.rotation ))
					throw std::runtime_error( "Invalid keyframe transformation data" );

				// copy transformation data
				memcpy( &keyframe.translation, keyframeIt.getBinaryData( "translation" ), sizeof( keyframe.translation ) );
				memcpy( &keyframe.rotation, keyframeIt.getBinaryData( "rotation" ), sizeof( keyframe.rotation ) );
			}
		}

	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}

	return true;
}

bool Skeleton3DAnimationResource::loadDataSync() {
	return true;
}

void Skeleton3DAnimationResource::freeData() {
}

size_t Skeleton3DAnimationResource::getAnimatedBoneCount() const {
	return animatedBones.size();
}

const Skeleton3DAnimationResource::AnimatedBoneData & Skeleton3DAnimationResource::getAnimatedBoneData( size_t i ) const {
	return animatedBones[i];
}

float Skeleton3DAnimationResource::getLength() const {
	return length;
}

float Skeleton3DAnimationResource::getFadeInTime() const {
	return fadeInTime;
}

float Skeleton3DAnimationResource::getFadeOutTime() const {
	return fadeOutTime;
}