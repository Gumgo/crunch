#ifndef TASKQUEUE_DEFINED
#define TASKQUEUE_DEFINED

#include "Common.h"
#include "Semaphore.h"
#include "Indexer.h"
#include "FastDelegate.h"
#include <list>
#include <boost/thread.hpp>
#include <boost/interprocess/detail/atomic.hpp>

/** @brief Performs tasks in the background.
 */
class TaskQueue {
public:
	/** @brief Describes the status of a task.
	 */
	enum TaskStatus {
		TS_COMPLETE,	/**< A completed task.*/
		TS_PENDING,		/**< A pending task.*/ 
		TS_EXECUTING,	/**< An executing task.*/
		TS_INVALID_TASK	/**< An invalid task ID was specified.*/
	};

	typedef size_t TaskId;											/**< The ID of a task.*/
	typedef fastdelegate::FastDelegate1 <void*, void> TaskFunction;	/**< The function to execute in a task.*/

	/** @brief Initializes the task threads.
	 */
	TaskQueue( size_t threadCount );

	/** @brief Flushes all active tasks and terminates the task threads.
	 */
	~TaskQueue();

	/** @brief Adds the task to the task queue to be executed.
	 */
	TaskId addTask( TaskFunction function, void * data );

	/** @brief Attempts to cancel the task with the given ID.
	 *
	 *  If the task has not yet been started, the task is canceled
	 *  and true is returned. Otherwise, if the task is running or
	 *  has already completed, false is returned. If the task ID is
	 *  invalid, false is returned.
	 */
	bool cancelTask( TaskId id );

	/** @brief Returns the status of the task with the given ID.
	 *
	 *  If the task has completed, TS_COMPLETE is returned. If
	 *  the task is pending, TS_PENDING is returned. If the task
	 *  is currently executing, TS_EXECUTING is returned. If
	 *  releaseTaskId is true and the task is complete, the task's
	 *  ID is released for reuse. If the task ID is invalid,
	 *  TS_INVALID_TASK is returned.
	 */
	TaskStatus getTaskStatus( TaskId id, bool releaseTaskId = true );

	/** @brief Blocks until all pending/running tasks are complete.
	 */
	void flushTasks();

private:
	// a pending task
	struct PendingTask {
		// the type of task
		enum TaskType {
			TT_NORMAL,		// a normal task
			TT_TERMINATE,	// this task is a signal to terminate the thread
			TT_FLUSH		// this task is a signal that the task queue is being flushed
		};
		TaskType type;			// the type of this task
		TaskId id;				// the ID of the task
		TaskFunction function;	// the function to execute
		void * data;			// the data to be passed to the function
	};

	// a task
	struct Task {
		volatile boost::uint32_t status;				// the status of this task
		std::list <PendingTask>::iterator pendingTask;	// the location of the pending task
	};

	std::vector <boost::thread> threads;	// the task threads

	boost::mutex taskMutex;					// the mutex used for task-related data structures

	Indexer <Task> tasks;					// the set of tasks
	std::list <PendingTask> pendingTasks;	// the list of pending tasks
	Semaphore tasksPending;					// used to signal when there are pending tasks

	size_t flushedCount;					// the number of threads that have been flushed
	boost::mutex flushMutex;				// the mutex used to adjust the flushed count
	Semaphore mainFlushSemaphore;			// the main thread waits on this semaphore when flushing
	Semaphore taskFlushSemaphore;			// the task threads wait on this semaphore when flushing

	// the main function for the task threads
	void taskThreadMain();
};

#endif