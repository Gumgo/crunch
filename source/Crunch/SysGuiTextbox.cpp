#include "SysGuiTextbox.h"
#include "Context.h"
#include "SysRenderer2D.h"

bool SysGuiTextbox::pointInside( const Vector2f & point ) const {
	return (point.x >= (float)position.x && point.y >= (float)position.y &&
			point.x < (float)(position.x + width) && point.y < (float)(position.y + 16));
}

void SysGuiTextbox::onCreate() {
	SysGui::onCreate();
	selected = false;
	width = 128;
	horiz = TAH_LEFT;
	mode = TM_STRING;
}

void SysGuiTextbox::recomputeDisplayedText() {
	displayedText.clear();
	if (!selected) {
		horiz = TAH_LEFT;
		int maxWidth = width - 4;
		int currentWidth = 0;
		for (size_t i = 0; i < text.length(); ++i) {
			currentWidth += font->getGlyph( font->getGlyphIndex( text[i] ) ).advance;
			if (currentWidth <= maxWidth)
				displayedText += text[i];
			else
				break;
		}
	} else {
		horiz = TAH_LEFT;
		int maxWidth = width - 4;
		int currentWidth = 0;
		for (size_t i = 0; i <= text.length(); ++i) {
			if (i < text.length())
				currentWidth += font->getGlyph( font->getGlyphIndex( text[i] ) ).advance;
			else
				currentWidth += font->getGlyph( font->getGlyphIndex( '|' ) ).advance;
			if (currentWidth <= maxWidth) {
				if (i < text.length())
					displayedText += text[i];
				else
					displayedText += '|';
			} else {
				displayedText.clear();
				horiz = TAH_RIGHT;
				currentWidth = font->getGlyph( font->getGlyphIndex( '|' ) ).advance;
				int i;
				for (i = (int)text.length() - 1; i >= 0; --i) {
					currentWidth += font->getGlyph( font->getGlyphIndex( text[i] ) ).advance;
					if (currentWidth > maxWidth) {
						++i;
						break;
					}
				}
				for (size_t t = (size_t)i; t < text.length(); ++t)
					displayedText += text[t];
				displayedText += '|';
			}
		}
	}
}

void SysGuiTextbox::move( const Vector2i & pos ) {
	position = pos;
}

void SysGuiTextbox::setWidth( int w ) {
	if (w < 16)
		w = 16;
	width = w;
	recomputeDisplayedText();
}

void SysGuiTextbox::setText( const std::string & t, bool callback ) {
	text = t;
	recomputeDisplayedText();
	if (callback) {
		if (onChangeText)
			onChangeText( text );
		else if (onChangeText2)
			onChangeText2( text, selectedText );
	}
}

std::string SysGuiTextbox::getText() const {
	return text;
}

void SysGuiTextbox::setTextMode( TextMode tm ) {
	mode = tm;
}

void SysGuiTextbox::select() {
	if (!selected) {
		selectedText = text;
		selected = true;
		recomputeDisplayedText();
	}
}

void SysGuiTextbox::deselect( bool callback ) {
	if (selected) {
		if (selectedText != text && callback) {
			if (onChangeText)
				onChangeText( text );
			else if (onChangeText2)
				onChangeText2( text, selectedText );
		}
		selected = false;
		recomputeDisplayedText();
	}
}

void SysGuiTextbox::setOnChangeText( const boost::function <void ( const std::string & )> & callback ) {
	onChangeText = callback;
	onChangeText2.clear();
}

void SysGuiTextbox::setOnChangeText( const boost::function <void ( const std::string &, const std::string & )> & callback ) {
	onChangeText.clear();
	onChangeText2 = callback;
}

void SysGuiTextbox::receiveEvent( const InputEvent & ev ) {
	bool recomp = false;
	if (ev.type == IET_MOUSE_BUTTON && ev.mouseButtonEvent.button == MBC_LEFT && ev.mouseButtonEvent.type == MBET_PRESSED) {
		Vector2i mouseWindow = Context::get().inputManager.mousePosition();
		Vector2f mouseWorld = camera->screenToWorld( Vector2f(
			(float)mouseWindow.x / (float)Context::get().window.getWidth(),
			(float)mouseWindow.y / (float)Context::get().window.getHeight() ) );

		if (pointInside( mouseWorld )) {
			if (!selected) {
				recomp = true;
				selectedText = text;
			}
			selected = true;
		} else {
			if (selected) {
				recomp = true;
				if (selectedText != text) {
					if (onChangeText)
						onChangeText( text );
					else if (onChangeText2)
						onChangeText2( text, selectedText );
				}
			}
			selected = false;
		}
	} else if (selected && ev.type == IET_CHARACTER) {
		if (ev.characterEvent.characterCode == KC_BACKSPACE) {
			if (!text.empty())
				text.pop_back();
		} else if (ev.characterEvent.characterCode == KC_RETURN) {
			selected = false;
			if (selectedText != text) {
				if (onChangeText)
					onChangeText( text );
				else if (onChangeText2)
					onChangeText2( text, selectedText );
			}
		} else if (font && font->containsGlyph( ev.characterEvent.characterCode )) {
			if (mode == TM_STRING ||
				(mode == TM_INT &&
					((ev.characterEvent.characterCode >= '0' && ev.characterEvent.characterCode <= '9') ||
						(ev.characterEvent.characterCode == '-' && text.empty()))) ||
				(mode == TM_UINT &&
					(ev.characterEvent.characterCode >= '0' && ev.characterEvent.characterCode <= '9')) ||
				(mode == TM_REAL &&
					((ev.characterEvent.characterCode >= '0' && ev.characterEvent.characterCode <= '9') ||
						(ev.characterEvent.characterCode == '.' && text.find( '.' ) == std::string::npos) ||
						(ev.characterEvent.characterCode == '-' && text.empty()))))
			text += (char)ev.characterEvent.characterCode;
		}
		recomp = true;
	}

	if (recomp)
		recomputeDisplayedText();
}

void SysGuiTextbox::onDismissParent() {
	if (selected) {
		selected = false;
		if (selectedText != text) {
			if (onChangeText)
				onChangeText( text );
			else if (onChangeText2)
				onChangeText2( text, selectedText );
		}
	}
}

void SysGuiTextbox::draw() {
	if (camera != NULL) {
		Matrix33af modelMatrix;
		modelMatrix.translate( (float)position.x, (float)position.y );
		Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

		GpuBlendState blendState = Context::get().gpuState.createBlendState();
		blendState.setBlendFunction( GpuBlendState::BM_NONE );
		blendState.bind();

		SysRenderer2D::get().drawGradientVert( mvpMatrix,
			Vector2f( 0.0f, 0.0f ), Vector2f( (float)width, 16.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );

		SysRenderer2D::get().drawGradientVert( mvpMatrix,
			Vector2f( 1.0f, 1.0f ), Vector2f( (float)width - 2.0f, 14.0f ),
			Vector4f( 0.75f, 0.75f, 0.75f, 1.0f ),
			Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ) );

		modelMatrix.translate( 2.0f, 8.0f );
		if (horiz == TAH_RIGHT)
			modelMatrix.translate( (float)width - 4.0f, 0.0f );
		mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

		SysRenderer2D::get().drawText( displayedText, *font, mvpMatrix, horiz, TAV_MIDDLE );
	}
}