#include "Physics2DShape.h"
#include "Physics2DBody.h"
#include "Physics2DContact.h"

namespace Physics2D {

const Real Shape::AABB_EXPANSION = (Real)0.01;

ShapeDefinition::ShapeDefinition() {
	friction = (Real)1;
	restitution = (Real)0;

	density = (Real)1;
	overrideCenterOfMass = false;

	rotation = (Real)0;
}

ShapeDefinition::~ShapeDefinition() {
}

Shape::Shape( const ShapeDefinition & d, Type t, Body * par ) {
	type = t;
	parent = par;
	area = (Real)0;
	friction = d.friction;
	restitution = d.restitution;
	density = d.density;
	centerOfMassOverridden = d.overrideCenterOfMass;
	if (d.overrideCenterOfMass)
		centerOfMass = d.overriddenCenterOfMass;

	setTransform( d.position, d.rotation );

	contacts = NULL;
}

Shape::~Shape() {
}

Shape::Type Shape::getType() const {
	return type;
}

Body * Shape::getParent() const {
	return parent;
}

void Shape::setArea( Real a ) {
	area = a;
}

void Shape::setMassData( const MassData & md ) {
	computedMassData = md;
}

Real Shape::getArea() const {
	return area;
}

void Shape::setFriction( Real fr ) {
	friction = fr;
}

Real Shape::getFriction() const {
	return friction;
}

void Shape::setRestitution( Real rest ) {
	restitution = rest;
}

Real Shape::getRestitution() const {
	return restitution;
}

void Shape::setDensity( Real d ) {
	density = d;
}

Real Shape::getDensity() const {
	return density;
}

void Shape::overrideCenterOfMass( const Vector2r & center ) {
	centerOfMass = center;
	centerOfMassOverridden = true;
}

void Shape::resetCenterOfMass() {
	centerOfMassOverridden = false;
}

bool Shape::isCenterOfMassOverridden() const {
	return centerOfMassOverridden;
}

Vector2r Shape::getOverriddenCenterOfMass() const {
	return centerOfMass;
}

MassData Shape::getMassData() const {
	return computedMassData;
}

void Shape::setTransform( const Vector2r & pos, Real rot ) {
	transform.identity();
	transform.rotate( rot );
	transform.translate( pos );
}

Matrix33ar Shape::getTransform() const {
	return transform;
}

void Shape::addContact( Contact * c ) {
	size_t iNew = c->getShapeIndex( this );
	if (iNew == Contact::NO_INDEX)
		return;

	if (contacts != NULL) {
		size_t iOld = contacts->getShapeIndex( this );
		contacts->getSimulationData().shapePrev[iOld] = c;
	}
	c->getSimulationData().shapeNext[iNew] = contacts;
	c->getSimulationData().shapePrev[iNew] = NULL;
	contacts = c;
}

void Shape::removeContact( Contact * c ) {
	size_t iRem = c->getShapeIndex( this );
	if (iRem == Contact::NO_INDEX)
		return;

	Contact * prev = c->getSimulationData().shapePrev[iRem];
	Contact * next = c->getSimulationData().shapeNext[iRem];
	if (prev == NULL)
		contacts = next;
	else {
		size_t iPrev = prev->getShapeIndex( this );
		prev->getSimulationData().shapeNext[iPrev] = next;
	}
	if (next != NULL) {
		size_t iNext = next->getShapeIndex( this );
		next->getSimulationData().shapePrev[iNext] = prev;
	}

	// the contact is now unlinked for this shape
	c->getSimulationData().shapePrev[iRem] = c->getSimulationData().shapeNext[iRem] = NULL;
}

Contact * Shape::getContacts() {
	return contacts;
}

const Contact * Shape::getContacts() const {
	return contacts;
}

Contact * Shape::getNextContact( Contact * c ) {
	return c->getSimulationData().shapeNext[c->getShapeIndex( this )];
}

const Contact * Shape::getNextContact( const Contact * c ) const {
	return c->getSimulationData().shapeNext[c->getShapeIndex( this )];
}

bool Shape::shouldCollide( const Shape * s ) const {
	// TODO: make this take custom filtering into account
	// should not collide if both shapes belong to same body
	if (getParent() == s->getParent())
		return false;
	// should not collide if neither is dynamic
	if (getParent()->getType() != Body::T_DYNAMIC && s->getParent()->getType() != Body::T_DYNAMIC)
		return false;

	return true;
}

Shape::BodyData & Shape::getBodyData() {
	return bodyData;
}

const Shape::BodyData & Shape::getBodyData() const {
	return bodyData;
}

}