/** @file Common.h
 *  @brief Includes files and defines functions common to most parts of the project.
 */

#ifndef COMMON_DEFINED
#define COMMON_DEFINED

#ifdef _WIN32
/** @brief Windows platform.
 */
#define CRUNCH_PLATFORM_WINDOWS
#elif defined linux
/** @brief Linux platform.
 */
#define CRUNCH_PLATFORM_LINUX
#elif defined __APPLE__
/** @brief Apple platform.
 */
#define CRUNCH_PLATFORM_APPLE
#else
#error Unsupported platform!
#endif
// todo: add iOS and Android platforms

#pragma comment( lib, "glew32.lib" )	// library for OpenGL Extension Wrangler
#pragma comment( lib, "opengl32.lib" )	// library for OpenGL
#pragma comment( lib, "glu32.lib" )		// library for OpenGL Utility Library
#pragma comment( lib, "FreeImage.lib" )	// library for loading images
#pragma comment( lib, "winmm.lib" )		// library for timing functions
#pragma comment( lib, "Ws2_32.lib" )	// library for WinSock2
#pragma comment( lib, "fmodex_vc.lib" )	// library for FMOD

#if defined CRUNCH_PLATFORM_WINDOWS
#ifndef NOMINMAX
/** @brief Defined so that the windows headers don't declare min and max macros.
 */
#define NOMINMAX
#endif

#ifndef WIN32_LEAN_AND_MEAN
/** @brief Defined to avoid include order issues.
 */
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef _SCL_SECURE_NO_WARNINGS
/** @brief Defined to avoid warnings from functions like std::copy().
 */
#define _SCL_SECURE_NO_WARNINGS
#endif
#endif

#include "Types.h"
#include "Maths.h"
#include "Exception.h"
#include "ReferenceCountedPointer.h"

#if defined CRUNCH_PLATFORM_WINDOWS
#include <Windows.h>
#include <WindowsX.h>
#include <WinDef.h>
#elif defined CRUNCH_PLATFORM_LINUX
#error Linux not yet supported!
#elif defined CRUNCH_PLATFORM_APPLE
#include <Cocoa/Cocoa.h>
#include <Carbon/Carbon.h>
#endif

#include <string>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cassert>
#include <memory>
#include <boost/noncopyable.hpp>

/** @brief A safer version of delete which sets the pointer to NULL.
 */
template <typename T> void safeDelete( T * &ptr ) {
	delete ptr;
	ptr = NULL;
}

/** @brief A safer version of delete[] which sets the pointer to NULL.
 */
template <typename T> void safeDeleteArray( T * &ptr ) {
	delete[] ptr;
	ptr = NULL;
}

/** @brief Converts an instance of type T to a string.
 */
template <typename T> std::string toString( T val ) {
	std::ostringstream oss;
	oss << val;
	return oss.str();
}

/** @brief Extracts an instance of type T from a string and stores it in val if successful.
 */
template <typename T> bool fromString( const std::string & str, T & val ) {
	std::stringstream ss( str );
	ss >> val;
	if (ss.fail())
		return false;
	return true;
}

/** @brief Returns the length of a static array.
 */
template <typename T, size_t i> inline size_t arraySize( const T (&arr)[i] ) {
	return sizeof( arr ) / sizeof( arr[0] );
}

/** @brief Macro version of arraySize for use in static assertions.
 */
#define ARRAY_SIZE( arr ) (sizeof( arr ) / sizeof( arr[0] ))

/** @brief Converts all characters in a string to upper case.
 */
std::string convertToUpper( const std::string & str );

/** @brief Converts all characters in a string to lower case.
 */
std::string convertToLower( const std::string & str );

#endif