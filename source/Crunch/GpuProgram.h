/** @file GpuProgram.h
 *  @brief Encapsulates a program on the GPU.
 */

#ifndef GPUPROGRAM_DEFINED
#define GPUPROGRAM_DEFINED

#include "Common.h"
#include "GpuObject.h"
#include "HashMap.h"
#include "HashFunctions.h"
#include "GpuBuffer.h"
#include <vector>

/** @brief Encapsulates a program on the GPU.
 */
class GpuProgram : public ReferenceCountedGpuObject {
public:
	/** @brief Description of program settings.
	 */
	struct ProgramDescription {
		const char ** vertexShaderSource;	/**< Source code for the vertex shader.*/
		size_t vertexShaderSourceCount;		/**< Number of strings in the vertex shader source.*/
		const char ** geometryShaderSource;	/**< Source code for the geometry shader.*/
		size_t geometryShaderSourceCount;	/**< Number of strings in the geometry shader source.*/
		const char ** fragmentShaderSource;	/**< Source code for the fragment shader.*/
		size_t fragmentShaderSourceCount;	/**< Number of strings in the fragment shader source.*/

		ProgramDescription();	/**< Sets default values.*/
	};

	/** @brief Description of a uniform in a program.
	 */
	struct UniformDescription {
		std::string name;	/**< The name of the uniform.*/
		GLenum type;		/**< The GLSL type.*/
		size_t arrayCount;	/**< The size if an array (1 otherwise).*/
	};

	/** @brief Description of an attribute in a program.
	 */
	struct AttributeDescription {
		std::string name;	/**< The name of the attribute.*/
		GLenum type;		/**< The GLSL type.*/
		size_t arrayCount;	/**< The size if an array (1 otherwise).*/
	};

	GLuint getVertexShaderId() const;	/**< Returns the vertex shader ID.*/
	GLuint getGeometryShaderId() const;	/**< Returns the geometry shader ID.*/
	GLuint getFragmentShaderId() const;	/**< Returns the fragment shader ID.*/
	GLuint getProgramId() const;		/**< Returns the program ID.*/

	/** @brief Returns the number of uniforms.
	 */
	size_t getUniformCount() const;

	/** @brief Returns the description of the uniform with the given index.
	 */
	const UniformDescription & getUniformDescription( size_t index ) const;

	/** @brief Returns the location of the uniform with the given index, or -1 if it does not exist.
	 */
	GLint getUniformLocation( size_t index, size_t arrayOffset = 0 ) const;

	/** @brief Returns the location of the uniform with the given name, or -1 if it does not exist.
	 */
	GLint getUniformLocation( const std::string & name, size_t arrayOffset = 0 ) const;

	/** @brief Returns whether a uniform with the given name exists and stores its index in index if it does.
	 */
	bool getUniformIndex( const std::string & name, size_t & index ) const;

	/** @brief Sets the value of the uniform with the given index.
	 *
	 *  The provided data is interpreted based on the uniform type.
	 *  The return value indicates whether the uniform was successfully set.
	 */
	bool setUniform( size_t index, GLenum type, const void * data, size_t arrayOffset = 0, size_t arrayCount = 1 );

	/** @brief Sets the value of the uniform with the given name.
	 */
	bool setUniform( const std::string & name, GLenum type, const void * data, size_t arrayOffset = 0, size_t arrayCount = 1 );

	/** @brief Returns the number of attributes.
	 */
	size_t getAttributeCount() const;

	/** @brief Returns the description of the attribute with the given index.
	 */
	const AttributeDescription & getAttributeDescription( size_t index ) const;

	/** @brief Returns the location of the attribute with the given index, or -1 if it does not exist.
	 */
	GLint getAttributeLocation( size_t index ) const;

	/** @brief Returns the location of the attribute with the given name, or -1 if it does not exist.
	 */
	GLint getAttributeLocation( const std::string & name ) const;

	/** @brief Returns whether an attribute with the given name exists and stores its index in index if it does.
	 */
	bool getAttributeIndex( const std::string & name, size_t & index ) const;

	/** @brief Enables and sets the value of the attribute with the given name.
	 */
	bool setAttribute( size_t index, uint size, GLenum type, bool normalized, bool integer, uint stride,
		const void * data, size_t arrayOffset = 0, size_t arrayCount = 1 );

	/** @brief Enables and sets the value of the uniform with the given name.
	 */
	bool setAttribute( const std::string & name, uint size, GLenum type, bool normalized, bool integer, uint stride,
		const void * data, size_t arrayOffset = 0, size_t arrayCount = 1 );

	/** @brief Enables and sets the value of the attribute with the given name.
	 */
	bool setAttribute( size_t index, GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer, uint stride,
		const void * data, size_t arrayOffset = 0, size_t arrayCount = 1 );

	/** @brief Enables and sets the value of the uniform with the given name.
	 */
	bool setAttribute( const std::string & name, GpuBufferReference & buffer,uint size, GLenum type, bool normalized, bool integer, uint stride,
		const void * data, size_t arrayOffset = 0, size_t arrayCount = 1 );

	/** @brief Binds the program.
	 */
	void bind();

	/** @brief Describes a GLSL uniform type.
	 */
	struct GlslUniformTypeDescription {
		GLenum baseType;				/**< The base type.*/
		size_t elements;				/**< The number of elements.*/
		GlslUniformTypeDescription();	/**< Default constructor.*/
		GlslUniformTypeDescription( GLenum base, size_t elem );	/**< Constructor assigning values.*/
	};

	/** @brief Returns a type description of a GLSL uniform type.
	 */
	static GlslUniformTypeDescription getGlslUniformTypeDescription( GLenum type );

	/** @brief Returns the size in bytes of a GLSL uniform base type (e.g. GL_FLOAT, but not GL_FLOAT_VEC2).
	 */
	static size_t getGlslUniformBaseTypeSize( GLenum type );

	/** @brief Describes a GLSL attribute type.
	 */
	struct GlslAttributeTypeDescription {
		GLenum baseType;	/**< The base type.*/
		size_t elements;	/**< The number of elements.*/
		size_t locations;	/**< The number of locations used (e.g. GL_FLOAT_MAT4 uses 4).*/
		GlslAttributeTypeDescription();	/**< Default constructor.*/
		GlslAttributeTypeDescription( GLenum base, size_t elem, size_t locs );	/**< Constructor assigning values.*/
	};

	/** @brief Returns a type description of a GLSL attribute type.
	 */
	static GlslAttributeTypeDescription getGlslAttributeTypeDescription( GLenum type );

	/** @brief Returns the size in bytes of a GLSL attribute base type (e.g. GL_FLOAT, but not GL_FLOAT_VEC2).
	 */
	static size_t getOpenGLAttributeTypeSize( GLenum type, size_t elements );

private:
	friend class GpuState;
	GpuProgram( GpuState * gpuState, const ProgramDescription & desc );

	~GpuProgram();
	void cleanup();

	void onRelease() const;
	void onUnbind();

	enum ShaderType {
		ST_VERTEX_SHADER,
		ST_GEOMETRY_SHADER,
		ST_FRAGMENT_SHADER,
		SHADER_TYPE_COUNT
	};
	static ShaderType getShaderType( GLenum shader );

	GLuint programId;
	GLuint shaderIds[SHADER_TYPE_COUNT];

	// uniforms
	struct UniformData {
		UniformDescription desc;
		GlslUniformTypeDescription typeDescription;
		size_t locationIndex; // index of location of first element in the uniformLocations array
		size_t cacheOffset;
	};
	// list of uniforms
	std::vector <UniformData> uniformData;
	// lists of uniform locations for each uniform or uniform array
	std::vector <GLint> uniformLocations;
	// map of uniform names
	HashMap <Hash <std::string>, std::string, size_t> uniformMap;
	// the uniform cache - type is uint to ensure 4-byte alignment
	std::vector <uint> uniformCache;

	// attributes
	struct AttributeData {
		AttributeDescription desc;
		GlslAttributeTypeDescription typeDescription;
		GLint location;
	};
	// list of attributes
	std::vector <AttributeData> attributeData;
	// map of attribute names
	HashMap <Hash <std::string>, std::string, size_t> attributeMap;
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <GpuProgram> GpuProgramReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <GpuProgram> GpuProgramConstReference;

#endif