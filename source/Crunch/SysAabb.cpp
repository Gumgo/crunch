#include "SysAabb.h"

const SysAabb SysAabb::EMPTY(
	Vector3f( std::numeric_limits <float>::max(), std::numeric_limits <float>::max(), std::numeric_limits <float>::max() ),
	Vector3f( -std::numeric_limits <float>::max(), -std::numeric_limits <float>::max(), -std::numeric_limits <float>::max() ) );

SysAabb::SysAabb() {
}

SysAabb::SysAabb( const Vector3f & minBnd, const Vector3f & maxBnd )
	: minBound( minBnd )
	, maxBound( maxBnd ) {
}

SysAabb::SysAabb( const Vector3f & minBnd, const Vector3f & maxBnd, const Matrix44af & tf )
	: minBound( std::numeric_limits <float>::max(), std::numeric_limits <float>::max(), std::numeric_limits <float>::max() )
	, maxBound( -std::numeric_limits <float>::max(), -std::numeric_limits <float>::max(), -std::numeric_limits <float>::max() ){
	const Vector3f * bnds[2] = { &minBnd, &maxBnd };
	for (size_t i = 0; i < 8; ++i) {
		// construct each corner point
		Vector3f corner(
			bnds[(i     ) & 1]->x,
			bnds[(i >> 1) & 1]->y,
			bnds[(i >> 2) & 1]->z );
		corner = tf.transformPoint( corner );
		minBound = vecMin( corner, minBound );
		maxBound = vecMax( corner, maxBound );
	}
}

SysAabb::SysAabb( const Vector3f & minBnd, const Vector3f & maxBnd, const Matrix33f & tf )
	: minBound( std::numeric_limits <float>::max(), std::numeric_limits <float>::max(), std::numeric_limits <float>::max() )
	, maxBound( -std::numeric_limits <float>::max(), -std::numeric_limits <float>::max(), -std::numeric_limits <float>::max() ){
	const Vector3f * bnds[2] = { &minBnd, &maxBnd };
	for (size_t i = 0; i < 8; ++i) {
		// construct each corner point
		Vector3f corner(
			bnds[(i     ) & 1]->x,
			bnds[(i >> 1) & 1]->y,
			bnds[(i >> 2) & 1]->z );
		corner = tf * corner;
		minBound = vecMin( corner, minBound );
		maxBound = vecMax( corner, maxBound );
	}
}

SysAabb::SysAabb( const SysAabb & aabb1, const SysAabb & aabb2 )
	: minBound( vecMin( aabb1.minBound, aabb2.minBound ) )
	, maxBound( vecMax( aabb1.maxBound, aabb2.maxBound ) ) {
}