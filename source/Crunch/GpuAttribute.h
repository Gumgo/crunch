 /** @file GpuAttribute.h
 *  @brief Encapsulates an attribute on the GPU.
 */

#ifndef GPUATTRIBUTE_DEFINED
#define GPUATTRIBUTE_DEFINED

#include "Common.h"
#include "GpuObject.h"
#include "GpuBuffer.h"

/** @brief Encapsulates an attribute on the GPU.
 */
class GpuAttribute : public ReferenceCountedGpuObject {
public:
	/** @brief Sets whether this attribute is enabled.
	 */
	void setEnabled( bool e );

	/** @brief Returns whether this attribute is enabled.
	 */
	bool isEnabled() const;

	/** @brief Sets the attribute pointer using the currently bounding array buffer.
	 *
	 *  Returns true on success. This method will fail if no array buffer is bound.
	 */
	bool setPointer( uint size, GLenum type, bool normalized, bool integer, uint stride, const void * pointer );

	/** @brief Sets the attribute pointer using the given array buffer.
	 */
	bool setPointer( const GpuBufferReference & buffer, uint size, GLenum type, bool normalized, bool integer, uint stride, const void * pointer );

	/** @brief Clears the attribute pointer.
	 */
	void clearPointer();

private:
	friend class GpuState;
	GpuAttribute( GpuState * gpuState, size_t i );

	~GpuAttribute();

	void onRelease() const;

	size_t index;
	bool attributeEnabled;
	GpuBufferReference attributeBuffer;
	uint attributeSize;
	GLenum attributeType;
	bool attributeNormalized;
	bool attributeInteger;
	uint attributeStride;
	const void * attributePointer;
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <GpuAttribute> GpuAttributeReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <GpuAttribute> GpuAttributeConstReference;

#endif