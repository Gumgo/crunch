#ifndef SYSGUICHECKBOX_DEFINED
#define SYSGUICHECKBOX_DEFINED

#include "Common.h"
#include "Actor.h"
#include "SysGui.h"

#include <boost/function.hpp>

DEFINE_DERIVED_ACTOR( SysGuiCheckbox, SysGui, 0, 0 )

	Vector2i position;
	std::string text;
	bool checked;

	boost::function <void ( bool )> onCheck;

	bool pointInside( const Vector2f & point ) const;

public:
	void onCreate();

	void move( const Vector2i & pos );
	void setText( const std::string & t );
	void setChecked( bool c, bool callback );
	bool isChecked() const;

	void setOnCheck( const boost::function <void ( bool )> & callback );

	void receiveEvent( const InputEvent & ev );
	void onDismissParent();
	void draw();
};

#endif