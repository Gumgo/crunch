#ifndef MATHS_AXISANGLEDEF_DEFINED
#define MATHS_AXISANGLEDEF_DEFINED

template <typename T> AxisAngle <T>::AxisAngle()
	: axis( 0, 0, 1 )
	, angle( 0 ) {
}

template <typename T> AxisAngle <T>::AxisAngle( const AxisAngle <T> & aa )
	: axis( aa.axis )
	, angle( aa.angle ) {
}

template <typename T> AxisAngle <T>::AxisAngle( T x, T y, T z, T a )
	: axis( x, y, z )
	, angle( a ) {
}

template <typename T> AxisAngle <T>::AxisAngle( const Vector3 <T> & xyz, T a )
	: axis( xyz )
	, angle( a ) {
}

template <typename T> AxisAngle <T>::~AxisAngle() {
}

template <typename T> AxisAngle <T> AxisAngle <T>::operator=( const AxisAngle <T> & aa ) {
	axis = aa.axis;
	angle = aa.angle;
	return *this;
}

template <typename T> bool AxisAngle <T>::operator==( const AxisAngle <T> & aa ) const {
	return (axis == aa.axis && angle == aa.angle);
}

template <typename T> bool AxisAngle <T>::operator!=( const AxisAngle <T> & aa ) const {
	return (axis != aa.axis || angle != aa.angle);
}

template <typename T> template <typename U> AxisAngle <T>::operator AxisAngle <U> () const {
	return AxisAngle <U>( (Vector3 <U>)axis, (U)angle );
}

template <typename T> void AxisAngle <T>::set( T x, T y, T z, T a ) {
	axis.set( x, y, z );
	angle = a;
}

template <typename T> void AxisAngle <T>::set( const Vector3 <T> & xyz, T a ) {
	axis = xyz;
	angle = a;
}

template <typename T> void AxisAngle <T>::normalize() {
	axis.normalize();
}

template <typename T> void AxisAngle <T>::invert() {
	angle = -angle;
}

template <typename T> AxisAngle <T> AxisAngle <T>::normalized() const {
	return AxisAngle <T>( axis.normalized(), angle );
}

template <typename T> AxisAngle <T> AxisAngle <T>::inverse() const {
	return AxisAngle <T>( axis, -angle );
}

template <typename T> Vector3 <T> AxisAngle <T>::apply( const Vector3 <T> & v ) const {
	T cosA = cos( angle );
	T sinA = sin( angle );

	return v*cosA + (axis.cross( v ))*sinA + axis*(axis.dot( v ))*(1 - cosA);
}

#endif