#include "Window.h"
#include "Context.h"
#include "Log.h"

#if defined CRUNCH_PLATFORM_WINDOWS
// the win32 implementation of the window class

Window::Window() {
	context = NULL;
	deviceContext = NULL;
	window = NULL;

	active = true;
	fullscreen = false;

	created = false;
	quit = false;

	width = 0;
	height = 0;
	bitsPerPixel = 0;
}

Window::~Window() {
	destroy();
}

LRESULT CALLBACK WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam ) {
	Window * win = (Window*)GetWindowLongPtr( hWnd, GWLP_USERDATA );
	InputEvent ev;
	switch ( uMsg ) {
	case WM_ACTIVATE:
		ev.type = IET_WINDOW;
		if (LOWORD( wParam ) == WA_INACTIVE) {
			win->active = false;
			ev.windowEvent.type = WET_LOST_FOCUS;
		} else {
			win->active = true;
			ev.windowEvent.type = WET_GAINED_FOCUS;
		}
		win->eventData.push_back( ev );
		return 0;
	case WM_SYSCOMMAND:
		switch (wParam) {
		case SC_SCREENSAVE:
		case SC_MONITORPOWER:
			return 0;
		}
		break;
	case WM_CLOSE:
		ev.type = IET_WINDOW;
		ev.windowEvent.type = WET_CLOSE;
		win->eventData.push_back( ev );
		win->quit = true;
		return 0;
	case WM_CHAR:
		ev.type = IET_CHARACTER;
		ev.characterEvent.characterCode = (int)wParam;
		win->eventData.push_back( ev );
		break;
	case WM_KEYDOWN:
		ev.type = IET_KEY;
		ev.keyEvent.type = KET_PRESSED;
		ev.keyEvent.keyCode = (int)wParam;
		win->eventData.push_back( ev );
		return 0;
	case WM_KEYUP:
		ev.type = IET_KEY;
		ev.keyEvent.type = KET_RELEASED;
		ev.keyEvent.keyCode = (int)wParam;
		win->eventData.push_back( ev );
		return 0;
	case WM_LBUTTONDOWN:
		ev.type = IET_MOUSE_BUTTON;
		ev.mouseButtonEvent.type = MBET_PRESSED;
		ev.mouseButtonEvent.button = (int)MBC_LEFT;
		ev.mouseButtonEvent.x = GET_X_LPARAM( lParam );
		ev.mouseButtonEvent.y = GET_Y_LPARAM( lParam );
		win->eventData.push_back( ev );
		return 0;
	case WM_LBUTTONUP:
		ev.type = IET_MOUSE_BUTTON;
		ev.mouseButtonEvent.type = MBET_RELEASED;
		ev.mouseButtonEvent.button = (int)MBC_LEFT;
		ev.mouseButtonEvent.x = GET_X_LPARAM( lParam );
		ev.mouseButtonEvent.y = GET_Y_LPARAM( lParam );
		win->eventData.push_back( ev );
		return 0;
	case WM_RBUTTONDOWN:
		ev.type = IET_MOUSE_BUTTON;
		ev.mouseButtonEvent.type = MBET_PRESSED;
		ev.mouseButtonEvent.button = (int)MBC_RIGHT;
		ev.mouseButtonEvent.x = GET_X_LPARAM( lParam );
		ev.mouseButtonEvent.y = GET_Y_LPARAM( lParam );
		win->eventData.push_back( ev );
		return 0;
	case WM_RBUTTONUP:
		ev.type = IET_MOUSE_BUTTON;
		ev.mouseButtonEvent.type = MBET_RELEASED;
		ev.mouseButtonEvent.button = (int)MBC_RIGHT;
		ev.mouseButtonEvent.x = GET_X_LPARAM( lParam );
		ev.mouseButtonEvent.y = GET_Y_LPARAM( lParam );
		win->eventData.push_back( ev );
		return 0;
	case WM_MBUTTONDOWN:
		ev.type = IET_MOUSE_BUTTON;
		ev.mouseButtonEvent.type = MBET_PRESSED;
		ev.mouseButtonEvent.button = (int)MBC_MIDDLE;
		ev.mouseButtonEvent.x = GET_X_LPARAM( lParam );
		ev.mouseButtonEvent.y = GET_Y_LPARAM( lParam );
		win->eventData.push_back( ev );
		return 0;
	case WM_MBUTTONUP:
		ev.type = IET_MOUSE_BUTTON;
		ev.mouseButtonEvent.type = MBET_RELEASED;
		ev.mouseButtonEvent.button = (int)MBC_MIDDLE;
		ev.mouseButtonEvent.x = GET_X_LPARAM( lParam );
		ev.mouseButtonEvent.y = GET_Y_LPARAM( lParam );
		win->eventData.push_back( ev );
		return 0;
	case WM_MOUSEWHEEL:
		ev.type = IET_MOUSE_WHEEL;
		ev.mouseWheelEvent.scroll = (float)((short)HIWORD( wParam )) / (float)WHEEL_DELTA;
		ev.mouseWheelEvent.x = GET_X_LPARAM( lParam );
		ev.mouseWheelEvent.y = GET_Y_LPARAM( lParam );
		win->eventData.push_back( ev );
		return 0;
	case WM_INPUT:
		{
			UINT DW_SIZE = 40;
			BYTE lpb[40];
			GetRawInputData( (HRAWINPUT)lParam, RID_INPUT, lpb, &DW_SIZE, sizeof( RAWINPUTHEADER ) );
			RAWINPUT * raw = (RAWINPUT*)lpb;
			if (raw->header.dwType == RIM_TYPEMOUSE) {
				// if the event on top is also a raw mouse event, just combine them
				// if we don't, we get a huge amount of messages
				if (!win->eventData.empty() && win->eventData.back().type == IET_MOUSE_RAW) {
					win->eventData.back().mouseRawEvent.dx += raw->data.mouse.lLastX;
					win->eventData.back().mouseRawEvent.dy += raw->data.mouse.lLastY;
				} else {
					ev.type = IET_MOUSE_RAW;
					ev.mouseRawEvent.dx = raw->data.mouse.lLastX;
					ev.mouseRawEvent.dy = raw->data.mouse.lLastY;
					win->eventData.push_back( ev );
				}
				return 0;
			}
		}
		break;
	case WM_SIZE:
		return 0;
	}
	return DefWindowProc( hWnd, uMsg, wParam, lParam );
}

void Window::create( const std::string & text, int w, int h, int bits, bool full ) {
	if (created)
		return;

	int pixelFormat;
	WNDCLASS wc;
	DWORD dwExStyle, dwStyle;

	width = w;
	height = h;
	bitsPerPixel = bits;
	title = text;

	RECT windowRect;
	windowRect.left = (long)0;
	windowRect.right = (long)w;
	windowRect.top = (long)0;
	windowRect.bottom = (long)h;

	fullscreen = full;

	// some window settings - mostly just standard procedure
	instance = GetModuleHandle( NULL );
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = instance;
	wc.hIcon = LoadIcon( NULL, IDI_WINLOGO );
	wc.hCursor = LoadCursor( NULL, IDC_ARROW );
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "OpenGL";

	if (!RegisterClass( &wc ))
		throw std::runtime_error( "Failed to create window: unable to register class" );

	if (fullscreen) {
		DEVMODE dmScreenSettings;
		memset( &dmScreenSettings, 0, sizeof( dmScreenSettings ) );
		dmScreenSettings.dmSize = sizeof( dmScreenSettings );
		dmScreenSettings.dmPelsWidth = w;
		dmScreenSettings.dmPelsHeight = h;
		dmScreenSettings.dmBitsPerPel = bits;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		if (ChangeDisplaySettings( &dmScreenSettings, CDS_FULLSCREEN ) != DISP_CHANGE_SUCCESSFUL) {
			Log::warning() << "Failed to initialize window in fullscreen mode, switching to windowed mode";
			fullscreen = false;
		}
	}

	//ShowCursor( false );

	// window frame settings here
	if (fullscreen) {
		dwExStyle = WS_EX_APPWINDOW;
		dwStyle = WS_POPUP;
	} else {
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		//dwStyle = WS_OVERLAPPEDWINDOW;
		dwStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU/* | WS_THICKFRAME*/ | WS_MINIMIZEBOX/* | WS_MAXIMIZEBOX*/;
	}

	AdjustWindowRectEx( &windowRect, dwStyle, false, dwExStyle );

	// first create a dummy window to get a dummy OpenGL context
	window = CreateWindowEx(
		dwExStyle,
		"OpenGL",
		title.c_str(),
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle,
		0, // x
		0, // y
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		NULL,
		NULL,
		instance,
		NULL );

	if (window == NULL) {
		destroy();
		throw std::runtime_error( "Failed to create window: invalid window handle obtained" );
	}

	// register raw input mouse events
	RAWINPUTDEVICE rid;
	rid.usUsagePage = (USHORT)0x01;	// code for HID_USAGE_PAGE_GENERIC
	rid.usUsage = (USHORT)0x02;		// code for HID_USAGE_GENERIC_MOUSE
	rid.dwFlags = RIDEV_INPUTSINK;
	rid.hwndTarget = window;
	RegisterRawInputDevices( &rid, 1, sizeof( rid ) );

	deviceContext = GetDC( window );
	if (deviceContext == NULL) {
		destroy();
		throw std::runtime_error( "Failed to create window: unable to get device context" );
	}

	// create the temp context
	try {
		HWND tempWindow = CreateWindowEx(
			dwExStyle,
			"OpenGL",
			title.c_str(),
			WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle,
			0, // x
			0, // y
			windowRect.right - windowRect.left,
			windowRect.bottom - windowRect.top,
			NULL,
			NULL,
			instance,
			NULL );

		if (tempWindow == NULL) {
			DestroyWindow( tempWindow );
			throw std::runtime_error( "Failed to create window: invalid window handle obtained" );
		}

		HDC tempDeviceContext = GetDC( tempWindow );
		if (tempDeviceContext == NULL) {
			DestroyWindow( tempWindow );
			throw std::runtime_error( "Failed to create window: unable to get device context" );
		}

		static const PIXELFORMATDESCRIPTOR pfd = {
			sizeof( PIXELFORMATDESCRIPTOR ),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
			PFD_TYPE_RGBA,
			bits,
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0,
			16, 0,
			0,
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
		};

		int tempPixelFormat = ChoosePixelFormat( tempDeviceContext, &pfd );
		if (tempPixelFormat == 0) {
			ReleaseDC( tempWindow, tempDeviceContext );
			DestroyWindow( tempWindow );
			destroy();
			throw std::runtime_error( "Failed to create window: unable to choose pixel format" );
		}

		if (!SetPixelFormat( tempDeviceContext, tempPixelFormat, &pfd )) {
			ReleaseDC( tempWindow, tempDeviceContext );
			DestroyWindow( tempWindow );
			destroy();
			throw std::runtime_error( "Failed to create window: unable to set pixel format" );
		}
		
		// create the dummy context
		HGLRC tempContext = wglCreateContext( tempDeviceContext );
		if (tempContext == NULL) {
			ReleaseDC( tempWindow, tempDeviceContext );
			DestroyWindow( tempWindow );
			throw std::runtime_error( "Failed to create window: unable to create context" );
		}

		if (!wglMakeCurrent( tempDeviceContext, tempContext )) {
			wglDeleteContext( tempContext );
			ReleaseDC( tempWindow, tempDeviceContext );
			DestroyWindow( tempWindow );
			throw std::runtime_error( "Failed to create window: wglMakeCurrent failed" );
		}

		// get the necessary functions, then destroy the temp context
		wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress( "wglCreateContextAttribsARB" );
		wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress( "wglChoosePixelFormatARB" );
		wglGetPixelFormatAttribivARB = (PFNWGLGETPIXELFORMATATTRIBIVARBPROC)wglGetProcAddress( "wglGetPixelFormatAttribivARB" );

		wglDeleteContext( tempContext );
		ReleaseDC( tempWindow, tempDeviceContext );
		DestroyWindow( tempWindow );
	} catch (const std::exception &) {
		destroy();
		throw;
	}

	// now that we've gotten our necessary OpenGL functions, we set up the real context
	static const int pixelFormatAttributes[] = {
		WGL_DRAW_TO_WINDOW_ARB,				GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB,				GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB,				GL_TRUE,
		WGL_PIXEL_TYPE_ARB,					WGL_TYPE_RGBA_ARB,
		WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB,	GL_TRUE,
		WGL_COLOR_BITS_ARB,					32,
		WGL_DEPTH_BITS_ARB,					24,
		WGL_STENCIL_BITS_ARB,				8,
		0
	};

	UINT formatCount;
	if (!wglChoosePixelFormatARB( deviceContext, pixelFormatAttributes, NULL, 1, &pixelFormat, &formatCount )) {
		destroy();
		throw std::runtime_error( "Failed to create window: unable to choose pixel format" );
	}

	if (!SetPixelFormat( deviceContext, pixelFormat, NULL )) {
		destroy();
		throw std::runtime_error( "Failed to create window: unable to set pixel format" );
	}

	static const int CONTEXT_ATTRIBS[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB,		3,
		WGL_CONTEXT_MINOR_VERSION_ARB,		3,
		WGL_CONTEXT_FLAGS_ARB,				WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};

	context = wglCreateContextAttribsARB( deviceContext, NULL, CONTEXT_ATTRIBS );
	if (context == NULL) {
		destroy();
		throw std::runtime_error( "Failed to create window: unable to create context" );
	}

	if (!wglMakeCurrent( deviceContext, context )) {
		destroy();
		throw std::runtime_error( "Failed to create window: wglMakeCurrent failed" );
	}

	glewInit();

	// center the window
	if (!fullscreen) {
		RECT windowPos;
		GetWindowRect( window, &windowPos );

		Vector2 <ULONG> windowSize( windowPos.right - windowPos.left, windowPos.bottom - windowPos.top );
		Vector2 <ULONG> screenSize( GetSystemMetrics( SM_CXSCREEN ), GetSystemMetrics( SM_CYSCREEN ) );
		Vector2 <ULONG> newPosition = screenSize / 2 - windowSize / 2;

		MoveWindow( window, newPosition.x, newPosition.y, windowSize.x, windowSize.y, true );
	}

	SetWindowLongPtr( window, GWLP_USERDATA, (long)this ); // for use in wndProc
	ShowWindow( window, SW_SHOW );
	SetForegroundWindow( window );
	SetFocus( window );

	quit = false;
	created = true;
	Log::info() << "Window initialized";
}

void Window::destroy() {
	if (fullscreen) {
		ChangeDisplaySettings( NULL, 0 );
		ShowCursor( true );
	}

	if (context != NULL) {
		wglMakeCurrent( NULL, NULL );
		wglDeleteContext( context );
		context = NULL;
	}

	if (window != NULL) {
		if (deviceContext != NULL)
			ReleaseDC( window, deviceContext );
		DestroyWindow( window );
		window = NULL;
		deviceContext = NULL;
	}

	UnregisterClass( "OpenGL", instance );
	instance = NULL;

	created = false;
}

int Window::getWidth() const {
	if (!created)
		throw std::runtime_error( "Unable to get width: window not initialized" );
	return width;
}

int Window::getHeight() const {
	if (!created)
		throw std::runtime_error( "Unable to get height: window not initialized" );
	return height;
}

Vector2i Window::getSize() const {
	if (!created)
		throw std::runtime_error( "Unable to get size: window not initialized" );
	return Vector2i( width, height );
}

std::string Window::getTitle() const {
	if (!created)
		throw std::runtime_error( "Unable to get title: window not initialized" );
	return title;
}

void Window::setTitle( std::string text ) {
	if (!created) {
		Log::warning() << "Unable to set title: window not initialized";
		return;
	}
	SetWindowText( window, text.c_str() );
}

void Window::setMousePosition( int x, int y ) {
	if (!created) {
		Log::warning() << "Unable to set mouse position: window not initialized";
		return;
	}
	POINT cursor;
	cursor.x = x;
	cursor.y = y;
	ClientToScreen( window, &cursor );
	SetCursorPos( (int)cursor.x, (int)cursor.y );
}

bool Window::getActive() const {
	if (!created)
		throw std::runtime_error( "Unable to get active status: window not initialized" );
	return active;
}

bool Window::getFullscreen() const {
	if (!created)
		throw std::runtime_error( "Unable to get fullscreen status: window not initialized" );
	return fullscreen;
}

bool Window::setFullscreen( bool flag ) {
	if (!created) {
		Log::warning() << "Unable to set fullscreen status: window not initialized";
		return false;
	}
	if (fullscreen == flag)
		return true;
	else {
		destroy();
		fullscreen = !fullscreen;
		try {
			create( title, width, height, bitsPerPixel, fullscreen );
		} catch (const std::exception & e) {
			Log::error() << e.what();
			Log::warning() << "Failed to switch to fullscreen mode";
			fullscreen = !fullscreen;
			try {
				create( title, width, height, bitsPerPixel, fullscreen );
			} catch (const std::exception & e) {
				Log::error() << e.what();
				throw std::runtime_error( "Failed to restore previous window settings after failing to switch to fullscreen mode" );
			}
			return false;
		}
		if (fullscreen)
			Log::info() << "Entered fullscreen mode";
		else
			Log::info() << "Entered windowed mode";
		return true;
	}
}

bool Window::resize( int newW, int newH ) {
	if (!created) {
		Log::warning() << "Unable to resize window: window not initialized";
		return false;
	}

	if (!fullscreen) {
		RECT windowPos;
		GetWindowRect( window, &windowPos );

		Vector2 <ULONG> windowSize( windowPos.right - windowPos.left, windowPos.bottom - windowPos.top );
		Vector2 <ULONG> screenSize( GetSystemMetrics( SM_CXSCREEN ), GetSystemMetrics( SM_CYSCREEN ) );
		Vector2 <ULONG> newPosition = screenSize / 2 - windowSize / 2;

		if (MoveWindow( window, newPosition.x, newPosition.y, windowSize.x, windowSize.y, true )) {
			width = newW;
			height = newH;
			return true;
		} else {
			Log::warning() << "Failed to resize window";
			return false;
		}
	} else {
		destroy();
		try {
			create( title, newW, newH, bitsPerPixel, fullscreen );
		} catch (const std::exception & e) {
			Log::error() << e.what();
			Log::warning() << "Failed to change resolution";
			try {
				create( title, width, height, bitsPerPixel, fullscreen );
			} catch (const std::exception & e) {
				Log::error() << e.what();
				throw std::runtime_error( "Failed to restore previous resolution after failing to change resolution" );
			}
			return false;
		}
		width = newW;
		height = newH;
		Log::info() << "Changed resolution to " << width << "x" << height;
		return true;
	}
}

bool Window::getQuit() const {
	return quit;
}

void Window::update() {
	if (!created)
		throw std::runtime_error( "Unable to update: window not initialized" );

	quit = false;
	eventData.clear();

	// update all the key/mouse inputs, etc.

	// set all pressed/released flags to false initially
	// we will detect these by comparing to the previous state

	POINT cursor;
	GetCursorPos( &cursor );
	ScreenToClient( window, &cursor );
	InputEvent ev;
	ev.type = IET_MOUSE_POSITION;
	ev.mousePositionEvent.x = (int)cursor.x;
	ev.mousePositionEvent.y = (int)cursor.y;
	eventData.push_back( ev );

	// handle window messages
	MSG msg;
	while (PeekMessage( &msg, NULL, 0, 0, PM_REMOVE )) {
		if (msg.message != WM_QUIT) {
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
	}
}

void Window::updateDisplay() {
	// put the rendered image onto the screen
	SwapBuffers( deviceContext );
}

const std::vector <InputEvent> & Window::getEventData() const {
	return eventData;
}

#endif