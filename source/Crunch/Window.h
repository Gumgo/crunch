/** @file Window.h
 *  @brief Contains functionality for managing the window.
 */

#ifndef WINDOW_DEFINED
#define WINDOW_DEFINED

#include "Common.h"
#include "OpenGL.h"
#include "InputEvent.h"
#include <vector>
#if defined CRUNCH_PLATFORM_APPLE
#import <Cocoa/Cocoa.h>
#endif

#if defined CRUNCH_PLATFORM_APPLE
class Window;

/** @brief Allows Objective-C interfaces to access the Window instance.
 */
class WindowWrapper {
public:
	Window * window;					/**< A pointer to the Window instance.*/
	void setActive( bool a );			/**< Sets the window's active status.*/
	void setFullscreen( bool f );		/**< Sets the window's fullscreen status.*/
	void setQuit( bool q );				/**< Sets the window's quit status.*/
	void pushEvent( InputEvent ie );	/**< Pushes an event to the window.*/
};

/** @interface InternalWindow
 *  @brief The internal window interface on Mac OS X.
 */
@interface InternalWindow : NSWindow <NSWindowDelegate> {
	WindowWrapper window;	/**< Provides access to the Window instance.*/
}

- (void)setWindow:(Window *)win;			/**< Sets the Window instance.*/
- (void)mouseDown:(NSEvent *)theEvent;		/**< Called when the mouse button is pressed.*/
- (void)mouseUp:(NSEvent *)theEvent;		/**< Called when the mouse button is released.*/
- (void)rightMouseDown:(NSEvent *)theEvent;	/**< Called when the right mouse button is pressed.*/
- (void)rightMouseUp:(NSEvent *)theEvent;	/**< Called when the right mouse button is released.*/
- (void)scrollWheel:(NSEvent *)theEvent;	/**< Called when the mouse wheel is scrolled.*/

- (void)keyDown:(NSEvent *)theEvent;		/**< Called when a key is pressed.*/
- (void)keyUp:(NSEvent *)theEvent;			/**< Called when a key is released.*/

- (void)windowShouldClose:(id)sender;						/**< Called when the user attempts to close the window.*/
- (void)windowDidBecomeKey:(NSNotification *)notification;	/**< Called when the window becomes the key window.*/
- (void)windowDidResignKey:(NSNotification *)notification;	/**< Called when the window loses its key status.*/

@end
#endif

/** @brief Manages the game window.
 */
class Window : private boost::noncopyable {
public:
	/** @brief The constructor.
	 */
	Window();

	/** @brief The destructor destroys the window.
	 */
	~Window();

	/** @brief Creates a window and returns whether successful.
	 *
	 *  @param text	The title text of the window.
	 *  @param w	The width of the window.
	 *  @param h	The height of the window.
	 *  @param bits	The number of bits per pixel.
	 *  @param full	Whether the window should be fullscreen.
	 */
	void create( const std::string & text, int w, int h, int bits, bool full );

	/** @brief Destroys the window.
	 */
	void destroy();

	int getWidth() const;					/**< Returns the width of the client part of the window in pixels.*/
	int getHeight() const;					/**< Returns the height of the client part of the window in pixels.*/
	Vector2i getSize() const;				/**< Returns the width and height of the client part of the window in pixels.*/
	std::string getTitle() const;			/**< Returns the title text of the window.*/
	void setTitle( std::string text );		/**< Sets the title text of the window.*/

	void setMousePosition( int x, int y );	/**< Sets the mouse position on the client part of the window in pixels.*/

	bool getActive() const;					/**< Returns whether the window is active.*/
	bool getFullscreen() const;				/**< Returns whether the window is fullscreen.*/
	bool setFullscreen( bool flag );		/**< Enables or disables fullscreen mode.*/
	bool resize( int newW, int newH );		/**< Resizes the window.*/

	bool getQuit() const;					/**< Returns whether the close button has been pressed.*/

	void update();							/**< Handles incoming messages and updates the InputState instance.*/
	void updateDisplay();					/**< Updates the display by swapping buffers.*/

	/** @brief Returns the vector containing input event data generated on the last update.
	 */
	const std::vector <InputEvent> & getEventData() const;

private:
#if defined CRUNCH_PLATFORM_WINDOWS
	HGLRC context;		// the context
	HDC deviceContext;	// the device context
	HWND window;		// the window
	HINSTANCE instance;	// the module

	/// the function which handles messages on Windows
	friend LRESULT CALLBACK WndProc( HWND, UINT, WPARAM, LPARAM );
#elif defined CRUNCH_PLATFORM_APPLE
	InternalWindow * window; // the internal window instance
	friend class WindowWrapper;
#elif defined CRUNCH_PLATFORM_LINUX
#error Linux not yet supported!
#else
#error Unsupported platform!
#endif

	bool active;		// whether the window is active
	bool fullscreen;	// whether the window is fullscreen

	bool created;		// whether the window has been created
	bool quit;			// whether the close button has been pressed

	int width;			// the width of the client part of the window in pixels
	int height;			// the height of the client part of the window in pixels
	int bitsPerPixel;	// the number of bits per pixel
	std::string title;	// the window title text

	// the ordered event data for a frame
	std::vector <InputEvent> eventData;
};

#endif