/** @file ResourceManager.h
 *  @brief Provides functionality to manage the loading, freeing, and accessing of resources.
 */

#ifndef RESOURCEMANAGER_DEFINED
#define RESOURCEMANAGER_DEFINED

#include "Common.h"
#include "Resource.h"
#include "HashMap.h"
#include "HashFunctions.h"
#include "Log.h"
#include "Sfinae.h"
#include "FastDelegate.h"
#include <list>

/** @brief Manages the loading, freeing, and accessing of resources.
 */
class ResourceManager : private boost::noncopyable {
public:
	/** @brief Initializes the resource loading thread.
	 */
	ResourceManager();

	/** @brief Frees all resources and terminates the resource loading thread.
	 */
	~ResourceManager();

	/** @brief Registers a resource class.
	 */
	template <typename R> void registerResourceClass();

	/** @brief Registers resources and metadata provided by the given root iterator to the resources file.
	 */
	void registerResources( KeyValueReader::Iterator root );

	/** @brief Acquires a resource by name.
	 *
	 *  The reference count of the resource is not incremented directly.
	 *  Instead, a ResourceReference object of the resource type is returned
	 *  which implicitly causes the reference count to increment. If the resource
	 *  has not yet been loaded, it will be queued in the resource loading thread.
	 *
	 *  @param name		The name of the resource to acquire.
	 *  @param block	If true, the thread blocks until the resource is ready or has failed to load.
	 *
	 *  @tparam T		The resource type.
	 */
	template <typename T> ReferenceCountedPointer <T> acquire( const std::string & name, bool block = false );

	/** @brief Releases a resource.
	 *
	 *  @param resource	The resource to release.
	 *  @param force	If true, the reference count will always be set to 0 and the resource will be unloaded.
	 */
	void release( Resource * resource, bool force = false );

	/** @brief Blocks until the given resource is ready or has failed to load.
	 *
	 *  Blocks until the given resource is ready or has failed to load.
	 *  If the resource is still in the loading queue, it is immediately loaded.
	 */
	void blockUntilLoaded( Resource * resource );

	/** @brief Blocks until all resources are ready or have failed to load.
	 */
	void flush();

	/** @brief Unloads all resources.
	 */
	void clear();

	/** @brief Completes synchronous loading of resources which have been asynchronously loaded.
	 */
	void update();

private:
	// associates a resource name with a path and metadata
	void registerResource( size_t resourceClassId, const std::string & name, const std::string & path, uint metadata );

	// data about a class of resources
	struct ResourceClassData {
		typedef fastdelegate::FastDelegate0 <const char*> GetResourceClassName;
		typedef fastdelegate::FastDelegate0 <const char*> GetResourceCategoryName;
		typedef fastdelegate::FastDelegate2 <uint &, KeyValueReader::Iterator, bool> ReadMetadata;
		typedef fastdelegate::FastDelegate0 <uint> GetDefaultMetadata;
		GetResourceClassName getResourceClassName;
		GetResourceCategoryName getResourceCategoryName;
		ReadMetadata readMetadata;
		GetDefaultMetadata getDefaultMetadata;
	};

	CLASS_HAS_FUNCTION( HasMetadata, readMetadata )

	// we need to partially specialize this template, but it requires a class, not a function
	template <typename R, bool V> class GetMetadataFunctions {
	public:
		static void get(
			ResourceClassData::ReadMetadata & readMetadata,
			ResourceClassData::GetDefaultMetadata & getDefaultMetadata );
	};

	template <typename R> class GetMetadataFunctions <R, true> {
	public:
		static void get(
			ResourceClassData::ReadMetadata & readMetadata,
			ResourceClassData::GetDefaultMetadata & getDefaultMetadata );
	};

	// contains data about each resource class, ordered corresponding to resource class ID
	std::vector <ResourceClassData> resourceClassData;

	// maps resource class name to resource class ID
	static const size_t RESOURCE_CLASS_CAPACITY = 20;
	HashMap <Hash <std::string>, std::string, size_t> resourceClassNameMap;

	// responsible for actually loading the resource
	void acquire( Resource * resource, bool block );

	// the key to a resource in the resource table
	struct ResourceKey {
		// combination of resource class ID and name
		std::string resourceClassIdName;
		ResourceKey( size_t resourceClassId, const std::string & name );
		bool operator==( const ResourceKey & o ) const;
		size_t getResourceClassId() const;
	};

	// the hash function for resource keys
	struct HashResourceKey {
		size_t operator()( const ResourceKey & key ) const;
	};

	static const size_t RESOURCE_CAPACITY = 500; // the resource table capacity

	// an indexed hash table of Resources
	HashMap <HashResourceKey, ResourceKey, Resource*> resources;

	// maps resource names to paths and metadata
	HashMap <HashResourceKey, ResourceKey, std::pair <std::string, uint>> paths;

	// contains data related to the resource loading thread
	struct ResourceLoadingThread {
		// a resource in the queue to be asynchronously loaded
		struct Task {
			// the type of task
			enum Type {
				T_RESOURCE,	// indicates that this task contains a resource to be loaded
				T_QUIT,		// indicates that this task should terminate the thread
				T_FLUSH		// indicates that this task should signal the main thread once it completes
			};
			Type type;				// the task type
			Resource * resource;	// pointer to the resource
			bool deleteFlag;		// whether this resource should be deleted once asynchronous loading completes
		};

		// the resource loading thread
		boost::thread thread;

		// queue of pending tasks
		std::list <Task> pendingTasks;

		// queue of completed tasks
		std::list <Task> completedTasks;

		// maps resources to their position in the pending or completed task queues (each resource will only appear in one)
		HashMap <Hash <Resource*>, Resource*, std::list <Task>::iterator> resourceToQueueMap;

		// the semaphore for alerting the thread that new tasks have been queued
		Semaphore tasksPending;

		// the mutex for the resource loading thread data structures
		boost::mutex mutex;

		// signaled when asynchronous loading completes if the main thread is waiting for asynchronous loading to complete, or when the loading thread is being flushed
		Semaphore mainThreadSignaler;

		// whether to signal the main thread once asynchronous loading completes
		bool signalFlag;

		// whether to set the delete flag on the currently loading resource
		bool deleteFlag;

		// the main function to run on the resource loading thread
		void main();

		// the constructor
		ResourceLoadingThread();
	} rlt;

	// completed tasks are quickly copied into this vector to avoid holding the loading thread mutex
	std::vector <ResourceLoadingThread::Task> completedTasks;
};

template <typename R> void ResourceManager::registerResourceClass() {
	// make sure the resource is derived from Resource
	BOOST_STATIC_ASSERT( (boost::is_base_of <Resource, R>::value) );

	struct DefaultMetadataFunctions {
		static bool readMetadata( uint & m, KeyValueReader::Iterator it ) {
			m = 0;
			return true;
		}

		static uint getDefaultMetadata() {
			return 0;
		}
	};

	// detect if the class contains a readMetadata function
	ResourceClassData rcd;
	rcd.getResourceClassName = ResourceClassData::GetResourceClassName( &R::getStaticResourceClassName );
	rcd.getResourceCategoryName = ResourceClassData::GetResourceCategoryName( &R::getStaticResourceCategoryName );
	rcd.readMetadata = ResourceClassData::ReadMetadata( &DefaultMetadataFunctions::readMetadata );
	rcd.getDefaultMetadata = ResourceClassData::GetDefaultMetadata( &DefaultMetadataFunctions::getDefaultMetadata );
	GetMetadataFunctions <R, HasMetadata <R, bool (*)( uint &, KeyValueReader::Iterator )>::value>::get(
		rcd.readMetadata, rcd.getDefaultMetadata );

	size_t resourceClassId = resourceClassData.size();
	resourceClassData.push_back( rcd );
	resourceClassNameMap.put( R::getStaticResourceClassName(), resourceClassId );
}

template <typename R, bool V> void ResourceManager::GetMetadataFunctions <R, V>::get(
	ResourceClassData::ReadMetadata & readMetadata,
	ResourceClassData::GetDefaultMetadata & getDefaultMetadata ) {
}

template <typename R> void ResourceManager::GetMetadataFunctions <R, true>::get(
	ResourceClassData::ReadMetadata & readMetadata,
	ResourceClassData::GetDefaultMetadata & getDefaultMetadata ) {
	readMetadata = ResourceClassData::ReadMetadata( &R::readMetadata );
	getDefaultMetadata = ResourceClassData::GetDefaultMetadata( &R::getDefaultMetadata );
}

template <typename T> ReferenceCountedPointer <T> ResourceManager::acquire( const std::string & name, bool block ) {
	size_t resourceClassId = resourceClassNameMap.get( T::getStaticResourceClassName() );
	ResourceKey key( resourceClassId, name );

	Resource * resource;
	std::pair <std::string, uint> pathMetadata;

	if (!paths.get( key, pathMetadata )) {
		// if the name has not been registered, return NULL
		Log::warning() << "Attempted to acquire an unregistered " << resourceClassData[resourceClassId].getResourceClassName() << " resource " << name;
		return ReferenceCountedPointer <T>();
	}

	if (!resources.get( key, resource )) {
		// the resource doesn't exist yet - create it
		resource = new T( this, name, pathMetadata.first, pathMetadata.second );
		resources.put( key, resource );
	}

	// acquire the resource
	acquire( resource, block );
	return ReferenceCountedPointer <T>( (T*)resource );
}

#endif