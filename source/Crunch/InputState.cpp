#include "InputState.h"

InputState::InputState() {
	closePressed = false;
	focus = true;
	gainedFocus = false;
	lostFocus = false;

	std::fill( mouseButtons, mouseButtons + MOUSE_BUTTON_CODE_COUNT, false );
	std::fill( mouseButtonsPressed, mouseButtonsPressed + MOUSE_BUTTON_CODE_COUNT, false );
	std::fill( mouseButtonsReleased, mouseButtonsReleased + MOUSE_BUTTON_CODE_COUNT, false );

	mouseScrollWheel = 0.0f;

	mousePosition[0] = 0;
	mousePosition[1] = 0;

	std::fill( keys, keys + 256, false );
	std::fill( keysPressed, keysPressed + 256, false );
	std::fill( keysReleased, keysReleased + 256, false );
}