/** @file GpuScissorState.h
 *  @brief Encapsulates the state of the scissor test on the GPU.
 */

#ifndef GPUSCISSORSTATE_DEFINED
#define GPUSCISSORSTATE_DEFINED

#include "Common.h"
#include "GpuObject.h"

/** @brief Encapsulates the state of the scissor test on the GPU.
 */
class GpuScissorState : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuScissorState();

	/** @brief Sets the state of the GPU to the state represented by this object.
	 */
	void bind();

	/** @brief Sets whether the scissor test is enabled.
	 */
	void setScissorTestEnabled( bool e );

	/** @brief Sets the scissor box position and size.
	 */
	void setScissorBox( const Vector2i & position, const Vector2i & size );

	/** @brief Returns whether the scissor test is enabled.
	 */
	bool isScissorTestEnabled() const;

	/** @brief Returns the scissor box position.
	 */
	Vector2i getScissorBoxPosition() const;

	/** @brief Returns the scissor box size.
	 */
	Vector2i getScissorBoxSize() const;

private:
	friend class GpuState;
	GpuScissorState( GpuState * state );

	bool scissorTestEnabled;		// whether the scissor test is enabled
	Vector2i scissorBoxPosition;	// position of scissor box
	Vector2i scissorBoxSize;		// size of scissor box

	// compares changes against current state and apply differences
	void apply( GpuScissorState & currentState ) const;
	// sets values to the current OpenGL state
	void getCurrentState();
};

#endif