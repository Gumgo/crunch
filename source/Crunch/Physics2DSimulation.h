/** @file Physics2DSimulation.h
 *  @brief Contains the physics simulation object.
 */

#ifndef PHYSICS2DSIMULATION_DEFINED
#define PHYSICS2DSIMULATION_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DBody.h"
#include "Physics2DSpatialHashMap.h"
#include "Physics2DContactManager.h"
#include "Indexer.h"

namespace Physics2D {

/** @brief A physics simulation.
 */
class Simulation : private boost::noncopyable {
public:
	Simulation( Real binDimension );	/**< The constructor.*/
	~Simulation();						/**< The destructor.*/

	/** @brief Updates the simulation with the given timestep.
	 */
	void update( TimeUnit dt );

	/** @brief Returns whether the simulation is locked.
	 *
	 *  This is used to prevent user updates from callbacks while the simulation is updating.
	 */
	bool isLocked() const;

	/** @brief Throws an exception if the simulation is locked.
	 */
	void throwIfLocked();

	Body * createBody( const BodyDefinition & def );	/**< Creates a body from the given definition.*/
	void destroyBody( Body * b );						/**< Destroys the given body.*/
	Body * getBodies();									/**< Returns the list of bodies.*/
	const Body * getBodies() const;						/**< Returns the list of bodies.*/
	Body * getNextBody( Body * b );						/**< Returns the next body given a body.*/
	const Body * getNextBody( const Body * b ) const;	/**< Returns the next body given a body.*/
	size_t getBodyCount() const;						/**< Returns the number of bodies.*/

private:
	friend class Body;

	bool locked; // whether the simulation is locked

	SpatialHashMap spatialHashMap;	// the spatial hash map holding shapes in the simulation
	ContactManager contactManager;	// manages contacts in the simulation

	Body * bodies;		// list of bodies
	size_t bodyCount;	// number of bodies

	// solves the physics simulation
	void solve( const TimeStep & timeStep );
};

}

#endif