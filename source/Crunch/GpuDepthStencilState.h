/** @file GpuDepthStencilState.h
 *  @brief Encapsulates the state of the depth and stencil functionality on the GPU.
 */

#ifndef GPUDEPTHSTENCILSTATE_DEFINED
#define GPUDEPTHSTENCILSTATE_DEFINED

#include "Common.h"
#include "GpuObject.h"

/** @brief Encapsulates the state of the depth and stencil functionality on the GPU.
 */
class GpuDepthStencilState : public GpuObject {
public:
	/** @brief Default constructor initializes GPU state pointer to NULL.
	 */
	GpuDepthStencilState();

	/** @brief Sets the state of the GPU to the state represented by this object.
	 */
	void bind();

	/** @brief Sets whether the depth test is enabled.
	 */
	void setDepthTestEnabled( bool e );

	/** @brief Sets the depth function.
	 */
	void setDepthFunction( GLenum function );

	/** @brief Sets whether depth writing is enabled.
	 */
	void setDepthWriteEnabled( bool e );

	/** @brief Sets whether the stencil test is enabled.
	 */
	void setStencilTestEnabled( bool e );

	/** @brief Sets the stencil function.
	 */
	void setStencilFunction( GLenum function );

	/** @brief Sets the stencil function for front and back faces separately.
	 */
	void setStencilFunctions( GLenum functionFront, GLenum functionBack );

	/** @brief Sets the stencil reference value.
	 */
	void setStencilReference( int reference );

	/** @brief Sets the stencil reference value for front and back faces separately.
	 */
	void setStencilReferences( int referenceFront, int referenceBack );

	/** @brief Sets the stencil mask.
	 */
	void setStencilMask( uint mask );

	/** @brief Sets the stencil mask for front and back faces separately.
	 */
	void setStencilMasks( uint maskFront, uint maskBack );

	/** @brief Sets the stencil operation.
	 */
	void setStencilOperation(
		GLenum operationStencilFail, GLenum operationDepthFail, GLenum operationDepthPass );

	/** @brief Sets the stencil operation for front and back faces separately.
	 */
	void setStencilOperations(
		GLenum operationStencilFailFront, GLenum operationDepthFailFront, GLenum operationDepthPassFront,
		GLenum operationStencilFailBack, GLenum operationDepthFailBack, GLenum operationDepthPassBack );

	/** @brief Sets the stencil write mask.
	 */
	void setStencilWriteMask( uint mask );

	/** @brief Sets the stencil write mask for front and back faces separately.
	 */
	void setStencilWriteMasks( uint maskFront, uint maskBack );

	/** @brief Returns whether the depth test is enabled.
	 */
	bool isDepthTestEnabled() const;

	/** @brief Returns the depth function.
	 */
	GLenum getDepthFunction() const;

	/** @brief Returns whether depth writing is enabled.
	 */
	bool isDepthWriteEnabled() const;

	/** @brief Returns whether the stencil test is enabled.
	 */
	bool isStencilTestEnabled() const;

	/** @brief Returns the stencil function for front faces.
	 */
	GLenum getStencilFunctionFront() const;

	/** @brief Returns the stencil function for back faces.
	 */
	GLenum getStencilFunctionBack() const;

	/** @brief Returns the stencil reference value for front faces.
	 */
	GLenum getStencilReferenceFront() const;

	/** @brief Returns the stencil reference value for back faces.
	 */
	GLenum getStencilReferenceBack() const;

	/** @brief Returns the stencil mask for front faces.
	 */
	uint getStencilMaskFront() const;

	/** @brief Returns the stencil mask for back faces.
	 */
	uint getStencilMaskBack() const;

	/** @brief Returns the stencil operation "stencil fail" for front faces.
	 */
	GLenum getStencilOperationStencilFailFront() const;

	/** @brief Returns the stencil operation "depth fail" for front faces.
	 */
	GLenum getStencilOperationDepthFailFront() const;

	/** @brief Returns the stencil operation "depth pass" for front faces.
	 */
	GLenum getStencilOperationDepthPassFront() const;

	/** @brief Returns the stencil operation "stencil fail" for back faces.
	 */
	GLenum getStencilOperationStencilFailBack() const;

	/** @brief Returns the stencil operation "depth fail" for back faces.
	 */
	GLenum getStencilOperationDepthFailBack() const;

	/** @brief Returns the stencil operation "depth pass" for back faces.
	 */
	GLenum getStencilOperationDepthPassBack() const;

	/** @brief Returns the stencil write mask for front faces.
	 */
	uint getStencilWriteMaskFront() const;

	/** @brief Returns the stencil write mask for back faces.
	 */
	uint getStencilWriteMaskBack() const;

private:
	friend class GpuState;
	GpuDepthStencilState( GpuState * state );

	bool depthTestEnabled;		// whether the depth test is enabled
	GLenum depthFunction;		// function used in depth comparisons
	bool depthWriteEnabled;		// whether depth writing is enabled

	// whether the stencil test is enabled
	bool stencilTestEnabled;
	// function used in stencil comparisons for front/back facing polygons
	GLenum stencilFunctionFront;
	GLenum stencilFunctionBack;
	// the reference value used in stencil comparisons for front/back facing polygons
	int stencilReferenceFront;
	int stencilReferenceBack;
	// the mask used in stencil comparisons for front/back facing polygons
	uint stencilMaskFront;
	uint stencilMaskBack;
	// stencil operations for front/back facing polygons
	GLenum stencilOperationStencilFailFront;
	GLenum stencilOperationStencilFailBack;
	GLenum stencilOperationDepthFailFront;
	GLenum stencilOperationDepthFailBack;
	GLenum stencilOperationDepthPassFront;
	GLenum stencilOperationDepthPassBack;
	// stencil write mask for front facing polygons
	uint stencilWriteMaskFront;
	uint stencilWriteMaskBack;

	// compares changes against current state and apply differences
	void apply( GpuDepthStencilState & currentState ) const;
	// sets values to the current OpenGL state
	void getCurrentState();
};

#endif