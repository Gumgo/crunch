#include "Skeleton3DResource.h"
#include "Log.h"

Skeleton3DResource::Skeleton3DResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m )
	, boneNames( 1 ) {
}

bool Skeleton3DResource::loadDataAsync() {
	try {
		KeyValueReader in( getPath() );

		KeyValueReader::Iterator it = in.getIterator();

		KeyValueReader::Iterator bonesIt = it.getArray( "bones" );
		bones.resize( bonesIt.getCount() );
		boneNames.resize( bonesIt.getCount() * 2 + 1 );

		for (size_t i = 0; i < bonesIt.getCount(); ++i) {
			KeyValueReader::Iterator boneIt = bonesIt.getArray( i );
			BoneData & bone = bones[i];

			std::string name = boneIt.getString( "name" );
			if (boneNames.contains( "name" ))
				throw std::runtime_error( "Duplicate bone name specified" );
			boneNames.put( name, i );

			if (boneIt.getBinaryDataLength( "inverseBind" ) != sizeof( bone.inverseBind ))
				throw std::runtime_error( "Invalid inverse bind matrix" );
			memcpy( &bone.inverseBind, boneIt.getBinaryData( "inverseBind" ), sizeof( bone.inverseBind ) );

			if (boneIt.contains( "parent" )) {
				size_t parent = (size_t)boneIt.getUint( "parent" );
				if (parent >= bonesIt.getCount())
					throw std::runtime_error( "Invalid bone parent" );
				bones[parent].children.push_back( i );
			} else
				// no parent - this is a root bone
				rootBones.push_back( i );
		}

		// verify the bone structure - make sure each bone is touched exactly once
		std::vector <bool> touched( bones.size(), false );
		std::vector <size_t> boneStack;
		for (size_t r = 0; r < rootBones.size(); ++r) {
			boneStack.push_back( rootBones[r] );
			while (!boneStack.empty()) {
				size_t boneId = boneStack.back();
				boneStack.pop_back();

				if (touched[boneId])
					// we should touch each bone exactly once
					throw std::runtime_error( "Invalid bone hierarchy specified" );
				touched[boneId] = true;
				for (size_t c = 0; c < bones[boneId].children.size(); ++c)
					boneStack.push_back( bones[boneId].children[c] );
			}
		}

		// now make sure all bones actually were touched
		for (size_t i = 0; i < touched.size(); ++i) {
			if (!touched[i])
				throw std::runtime_error( "Isolated bone in hierarchy specified" );
		}

	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}

	return true;
}

bool Skeleton3DResource::loadDataSync() {
	return true;
}

void Skeleton3DResource::freeData() {
}

size_t Skeleton3DResource::getBoneCount() const {
	return bones.size();
}

const Skeleton3DResource::BoneData & Skeleton3DResource::getBoneData( size_t i ) const {
	return bones[i];
}

bool Skeleton3DResource::getBoneIndex( const std::string & name, size_t & index ) const {
	return boneNames.get( name, index );
}

size_t Skeleton3DResource::getRootBoneCount() const {
	return rootBones.size();
}

size_t Skeleton3DResource::getRootBoneIndex( size_t i ) const {
	return rootBones[i];
}
