/** @file Maths_AxisAngleDec.h
 *  @brief Contains an implementation of an axis/angle rotation class.
 */

#ifndef MATHS_AXISANGLEDEC_DEFINED
#define MATHS_AXISANGLEDEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for axis/angle rotations.
 *
 *  @tparam T	The component type.
 */
template <typename T> class AxisAngle {
public:
	Vector3 <T> axis;	/**< The axis.*/
	T angle;			/**< The angle.*/

	/** @brief Constructs an axis/angle with default values.
	 */
	AxisAngle();

	/** @brief Constructs an axis/angle by copying the one provided.
	 */
	AxisAngle( const AxisAngle <T> & aa );

	/** @brief Constructs an axis/angle from the given axis and angle.
	 */
	AxisAngle( T x, T y, T z, T a );

	/** @brief Constructs an axis/angle from the given axis and angle.
	 */
	AxisAngle( const Vector3 <T> & xyz, T a );

	/** @brief The destructor.
	 */
	~AxisAngle();

	// operators

	/** @brief Sets the current value to the axis/angle aa.
	 */
	AxisAngle <T> operator=( const AxisAngle <T> & aa );

	/** @brief Returns whether the current value is equal to the axis/angle aa.
	 */
	bool operator==( const AxisAngle <T> & aa ) const;

	/** @brief Returns whether the current value is not equal to the axis/angle aa.
	 */
	bool operator!=( const AxisAngle <T> & aa ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator AxisAngle <U> () const;

	// mutable

	/** @brief Sets the current value's components to ((x, y, z), a).
	 */
	void set( T x, T y, T z, T a );

	/** @brief Sets the current value's components to (xyz, a).
	 */
	void set( const Vector3 <T> & xyz, T a );

	/** @brief Normalizes the current value.
	 */
	void normalize();

	/** @brief Inverts the current value.
	 */
	void invert();

	// immutable

	/** @brief Returns the current value normalized.
	 */
	AxisAngle <T> normalized() const;

	/** @brief Returns the inverse of the current.
	 */
	AxisAngle <T> inverse() const;

	/** @brief Applies the rotation to the vector v.
	 */
	Vector3 <T> apply( const Vector3 <T> & v ) const;
};

/** @brief AxisAngle of type float.
 */
typedef AxisAngle <float> AxisAnglef;

/** @brief AxisAngle of type float.
 */
typedef AxisAngle <double> AxisAngled;

#endif