#if 0

#include "NetworkDataLayer.h"
#include "Log.h"

#define IGNORE_ECONNRESET

NetworkDataLayer::AddressKey::AddressKey( const sockaddr_storage & addr )
	: address( addr ) {
}

bool NetworkDataLayer::AddressKey::operator==( const AddressKey & other ) const {
	return (memcmp( &address, &other.address, sizeof( address ) ) == 0);
}

NetworkDataLayer::NetworkDataLayer()
	: receiveBuffer( RECEIVE_BUFFER_SIZE )
	, connectionsMap( 20 ) {
	initialized = false;
	acceptNewConnections = false;
	initializeSockets();
}

NetworkDataLayer::~NetworkDataLayer() {
	term();
	terminateSockets();
}

bool NetworkDataLayer::init( ushort port ) {
	if (initialized) {
		Log::warning() << "Network data layer already initialized";
		return false;
	}

	socketId = INVALID_SOCKET_HANDLE;
	addrinfo socketHints;
	addrinfo * socketResults = NULL;

	try {
		// clear the socket hints struct
		memset( &socketHints, 0, sizeof( socketHints ) );
		socketHints.ai_family	= AF_INET;		// listen on IPv4
		socketHints.ai_socktype	= SOCK_DGRAM;	// we are using UDP, so use datagram
		socketHints.ai_flags	= AI_PASSIVE;	// use this IP address

		std::string portString = toString( port );
		if (getaddrinfo( NULL, portString.c_str(), &socketHints, &socketResults ) != 0)
			throw std::runtime_error( "Failed to initialize socket info" );

		// loop through the results and try to bind to one
		for (addrinfo * i = socketResults; i != NULL; i = i->ai_next) {
			socketId = socket( i->ai_family, i->ai_socktype, i->ai_protocol );
			if (socketId != INVALID_SOCKET_HANDLE) {
#ifdef _WIN32
				ULONG nonzero = 1;
				if (ioctlsocket( socketId, FIONBIO, &nonzero ) != 0 ||
#else
				if (fcntl( socketId, F_SETFL, O_NONBLOCK ) != 0 ||
#endif
					bind( socketId, i->ai_addr, i->ai_addrlen ) != 0) {
					closesocket( socketId );
					socketId = INVALID_SOCKET_HANDLE;
				} else
					// we found one that works
					break;
			}
		}

		if (socketId == INVALID_SOCKET_HANDLE)
			throw std::runtime_error( "Failed to initialize socket" );

		freeaddrinfo( socketResults );
		socketResults = NULL;

		initialized = true;
		Log::info() << "Network data layer initialized";
		return true;

	} catch (const std::exception & e) {
		Log::error() << e.what();
		if (socketResults != NULL)
			freeaddrinfo( socketResults );
		if (socketId != INVALID_SOCKET_HANDLE)
			closesocket( socketId );
		return false;
	}
}

void NetworkDataLayer::term() {
	if (!initialized)
		return;

	size_t index = 0;
	size_t count = connections.size();
	while (count > 0) {
		if (connections.contains( index )) {
			--count;
			destroyConnection( index );
		}
		++index;
	}
	connections.clear();

	closesocket( socketId );
	socketId = INVALID_SOCKET_HANDLE;
	Log::info() << "Network data layer terminated";
	initialized = false;
}

void NetworkDataLayer::enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn ) {
	acceptNewConnections = true;
	onAcceptNewConnection = onAcceptNewConnectionFn;
}

void NetworkDataLayer::disableAcceptNewConnections() {
	acceptNewConnections = false;
}

bool NetworkDataLayer::createConnection( const std::string & address, ushort port, size_t & newConnectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to create data layer connection: network data layer not initialized";
		return false;
	}

	Connection c;
	c.queueStarted = false;
	c.currentQueueSize = 0;

	memset( &c.address, 0, sizeof( c.address ) );
	// first try interpreting as IPv4
	// if that fails try IPv6
	// if that fails, something is wrong
	sockaddr_in * address4 = (sockaddr_in*)&c.address;
	sockaddr_in6 * address6 = (sockaddr_in6*)&c.address;
	if (inet_pton( AF_INET, address.c_str(), &address4->sin_addr ) == 1) {
		address4->sin_family = AF_INET;
		address4->sin_port = htons( port );
	} else if (inet_pton( AF_INET6, address.c_str(), &address6->sin6_addr ) == 1) {
		address6->sin6_family = AF_INET6;
		address6->sin6_port = htons( port );
	} else {
		Log::warning() << "Failed to create data layer connection: unknown IP address format";
		return false;
	}

	newConnectionId = connections.add( c );
	connections.get( newConnectionId ).dataQueue.reserve( Connection::DATA_QUEUE_SIZE );
	connectionsMap.put( c.address, newConnectionId );
	Log::info() << "Connection " << newConnectionId << " created";
	return true;
}

void NetworkDataLayer::destroyConnection( size_t connectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to destroy data layer connection: network data layer not initialized";
		return;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to destroy a nonexistent data layer connection";
		return;
	}

	Connection & c = connections.get( connectionId );
	// if pending messages, remove them from the pending list
	if (!c.dataQueue.empty()) {
		for (size_t i = 0; i < sendPendingConnections.size(); ++i) {
			if (sendPendingConnections[i] == connectionId) {
				sendPendingConnections.erase( sendPendingConnections.begin() + i );
				break;
			}
		}
	}

	// send all data that might be queued (unreliable for guaranteed messages!)
	sendQueued( connectionId );

	connectionsMap.remove( c.address );
	connections.remove( connectionId );
	Log::info() << "Data layer connection " << connectionId << " destroyed";
}

void NetworkDataLayer::setOnReceiveData( const boost::function <void ( size_t, const byte*, size_t )> & onReceiveDataFn ) {
	onReceiveData = onReceiveDataFn;
}

void NetworkDataLayer::receiveAndProcess() {
	if (!initialized) {
		Log::warning() << "Failed to receive and process data: network data layer not initialized";
		return;
	}

	while (true) {
		sockaddr_storage fromInfo;
		memset( &fromInfo, 0, sizeof( fromInfo ) );
		socklen_t fromInfoLength = (socklen_t)sizeof( fromInfo );
		// receive the data
		int bytesPending = (int)recvfrom( socketId, (char*)&receiveBuffer[0], (int)receiveBuffer.size(), 0, (sockaddr*)&fromInfo, &fromInfoLength );
		if (bytesPending == -1) {
			// either there's no more data or an error occurred - either case, stop trying to read
#ifdef IGNORE_ECONNRESET
			if (socketCheckError() != socketErrorCode( ECONNRESET )) {
#endif
			if (socketCheckError() != socketErrorCode( EWOULDBLOCK ))
				Log::warning() << "Failed to read data from socket";
#ifdef IGNORE_ECONNRESET
			}
#endif
			break;
		}

		bool processData = true;
		size_t connectionId;
		if (!connectionsMap.get( fromInfo, connectionId )) {
			if (acceptNewConnections) {
				// create a new connection
				char address[INET6_ADDRSTRLEN];
				address[0] = '\0';
				ushort port = 0;
				if (fromInfo.ss_family == AF_INET) {
					// if IPv4, extract port and IP
					sockaddr_in * from = (sockaddr_in*)&fromInfo;
					port = ntohs( from->sin_port );
					inet_ntop( from->sin_family, &from->sin_addr, address, sizeof( address ) );
				} else if (fromInfo.ss_family == AF_INET6) {
					// if IPv6, extract port and IP
					sockaddr_in6 * from = (sockaddr_in6*)&fromInfo;
					port = ntohs( from->sin6_port );
					inet_ntop( from->sin6_family, &from->sin6_addr, address, sizeof( address ) );
				}
				// try to create the connection
				if (createConnection( address, port, connectionId )) {
					if (onAcceptNewConnection)
						onAcceptNewConnection( connectionId );
				} else
					// this shouldn't be failing, but just in case it does
					processData = false;
			} else
				processData = false;
		}

		if (processData) {
			size_t dataPointer = 0;
			bool error = false;
			while (dataPointer < (size_t)bytesPending) {
				if ((size_t)bytesPending - dataPointer < 2) {
					error = true;
					break;
				}
				// read the data size
				ushort dataSize = *((ushort*)&receiveBuffer[dataPointer]);
				// convert it to the right format
				dataSize = ntohs( dataSize );
				dataPointer += 2;
				if ((size_t)bytesPending - dataPointer < (size_t)dataSize) {
					error = true;
					break;
				}
				// call the function to process the data
				if (onReceiveData)
					onReceiveData( connectionId, &receiveBuffer[dataPointer], (size_t)dataSize );
				dataPointer += (size_t)dataSize;
				// if the call to onReceiveData disconnected, don't process any more data
				if (!connectionsMap.contains( fromInfo ))
					break;
			}
			if (error)
				Log::warning() << "Invalid data received from socket";
		}
	}
}

bool NetworkDataLayer::startQueueData( size_t connectionId ) {
	if (!initialized) {
		Log::warning() << "Failed to start queue data: network data layer not initialized";
		return false;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to start queue data on a nonexistent connection";
		return false;
	}

	Connection & c = connections.get( connectionId );
	c.queueStarted = true;
	c.currentQueueSize = 0;

	// add to pending list if not already
	if (c.dataQueue.empty())
		sendPendingConnections.push_back( connectionId );

	ushort currentSize = 0;
	c.dataQueue.resize( c.dataQueue.size() + 2 );
	*(ushort*)(&c.dataQueue[c.dataQueue.size() - 2]) = htons( currentSize );

	return true;
}

bool NetworkDataLayer::queueData( size_t connectionId, const byte * data, size_t dataSize ) {
	if (!initialized) {
		Log::warning() << "Failed to queue data: network data layer not initialized";
		return false;
	}

	if (!connections.contains( connectionId )) {
		Log::warning() << "Attempted to queue data on a nonexistent connection";
		return false;
	}

	Connection & c = connections.get( connectionId );

	if (!c.queueStarted) {
		Log::warning() << "Attempted to queue data on an unstarted queue";
		return false;
	}

	if (c.currentQueueSize + dataSize > std::numeric_limits <ushort>::max()) {
		Log::warning() << "Attempted to queue data of length greater than 65,535 bytes";
		return false;
	}

	// resize the data queue
	size_t dataPointer = c.dataQueue.size();
	c.dataQueue.resize( c.dataQueue.size() + dataSize );
	// copy the data
	memcpy( &c.dataQueue[dataPointer], data, dataSize );

	c.currentQueueSize += dataSize;

	// write the new data size
	ushort currentSize = (ushort)c.currentQueueSize;
	*((ushort*)&c.dataQueue[c.dataQueue.size()-c.currentQueueSize-2]) = htons( currentSize );

	return true;
}

void NetworkDataLayer::sendQueued( size_t i ) {
	Connection & c = connections.get( i );
	if (!c.queueStarted)
		return;

	int result = (int)sendto( socketId, (char*)&c.dataQueue[0], (int)c.dataQueue.size(), 0, (sockaddr*)&c.address, (int)sizeof( c.address ) );
	if (result == -1)
		Log::error() << "Failed to send queued data";

	c.dataQueue.clear();
	c.queueStarted = false;
	c.currentQueueSize = 0;
}

void NetworkDataLayer::sendQueued() {
	if (!initialized) {
		Log::warning() << "Failed to send queued data: network data layer not initialized";
		return;
	}

	for (size_t i = 0; i < sendPendingConnections.size(); ++i)
		sendQueued( sendPendingConnections[i] );

	sendPendingConnections.clear();
}

#endif