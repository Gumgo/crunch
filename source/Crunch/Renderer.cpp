#include "Renderer.h"

Renderer::Renderer() {
	calls.reserve( 1000 );
	sortBuffer.reserve( 1000 );
}

void Renderer::submit( Renderer::Call call, Renderer::Key key ) {
	calls.push_back( std::make_pair( call, key ) );
}

void Renderer::execute() {
	struct GetKey {
		Key operator()( const std::pair <Call, Key> & i ) const {
			return i.second;
		}
	};

	if (!calls.empty())
		radixSortUint <Key, std::pair <Call, Key>, GetKey>( &calls[0], calls.size(), GetKey(), sortBuffer );
	for (size_t i = 0; i < calls.size(); ++i)
		calls[i].first();
	calls.clear();
}

Renderer::Key Renderer::makeKey( byte pass, int order ) {
	Key k;
	k.order[0] = ((byte*)&order)[0];
	k.order[1] = ((byte*)&order)[1];
	k.order[2] = ((byte*)&order)[2];
	k.order[3] = ((byte*)&order)[3] ^ 0x80; // flip the most significant bit
	k.order[4] = pass;
	return k;
}

Renderer::Key Renderer::makeKey( byte pass, uint order ) {
	Key k;
	k.order[0] = ((byte*)&order)[0];
	k.order[1] = ((byte*)&order)[1];
	k.order[2] = ((byte*)&order)[2];
	k.order[3] = ((byte*)&order)[3];
	k.order[4] = pass;
	return k;
}

Renderer::Key Renderer::makeKey( byte pass, float order ) {
	uint iOrder = *(uint*)&order;
	if (iOrder & 0x80000000)
		// if negative, flip all the bits (including sign)
		iOrder = ~iOrder;
	else
		// otherwise, just flip the sign bit
		iOrder ^= 0x80000000;
	Key k;
	k.order[0] = ((byte*)&iOrder)[0];
	k.order[1] = ((byte*)&iOrder)[1];
	k.order[2] = ((byte*)&iOrder)[2];
	k.order[3] = ((byte*)&iOrder)[3];
	k.order[4] = pass;
	return k;
}

Renderer::Key Renderer::makeKeyFirst( byte pass ) {
	Key k = { 0, 0, 0, 0, pass };
	return k;
}

Renderer::Key Renderer::makeKeyLast( byte pass ) {
	Key k = { 0xFF, 0xFF, 0xFF, 0xFF, pass };
	return k;
}