/** @file Physics2DCapsuleShape.h
 *  @brief Defines capsule shapes.
 */
#ifndef PHYSICS2DCAPSULESHAPE_DEFINED
#define PHYSICS2DCAPSULESHAPE_DEFINED

#include "Common.h"
#include "Physics2D.h"
#include "Physics2DShape.h"

namespace Physics2D {

class Body;
struct CapsuleShapeDefinition;

/** @brief A capsule shape.
 */
class CapsuleShape : public Shape {
public:
	CapsuleShape( const CapsuleShapeDefinition & d, Body * par );	/**< The constructor.*/

	/** @brief Returns the bounding box of the transformed shape.
	 */
	Aabb getAabb( const Matrix33ar & transformation ) const;

	/** @brief Computes the mass data of the shape.
	 */
	void computeMassData();

	void setVertices( const Vector2r & v0, const Vector2r & v1 );	/**< Sets the vertices.*/
	Vector2r getVertex( size_t vertex ) const;						/**< Returns a vertex.*/
	void setRadius( Real rad );										/**< Sets the radius.*/
	Real getRadius() const;											/**< Returns the radius.*/

private:
	Vector2r vertices[2];	// the vertices
	Real radius;			// the radius

	Real computeArea() const;
};

/** @brief Defines capsule shapes.
 */
struct CapsuleShapeDefinition : public ShapeDefinition {
	Vector2r vertices[2];		/**< The vertices.*/
	Real radius;				/**< The radius.*/

	CapsuleShapeDefinition();					/**< Assigns default values.*/
	Shape * createShape( Body * parent )const;	/**< Creates a capsule shape from the definition.*/
};

}

#endif