/** @file GpuTexture.h
 *  @brief Encapsulates a texture on the GPU.
 */

#ifndef GPUTEXTURE_DEFINED
#define GPUTEXTURE_DEFINED

#include "Common.h"
#include "GpuObject.h"
#include "GpuBuffer.h"

class GpuState;

/** @brief Encapsulates a texture on the GPU.
 */
class GpuTexture : public ReferenceCountedGpuObject {
public:
	// notes about OpenGL textures:
	// if mipmapping is enabled in the texture filtering mode, the texture must be mipmap complete
	// the base mipmap level is specified as B
	// if d[i] is a dimension of the ith entry in the mipmap array, then the following must hold:
	//   d[i+1] = floor( d[i] / 2 )
	// the mipmap array stops after an entry where each dimension is 1
	// let M = the maximum dimension of the base level
	// this means that a complete mipmap array contains floor( log_2( M ) ) + 1 entries

	/** @brief Description of 1D data.
	 */
	struct Data1DDescription {
		uint width;			/**< The width of the data.*/
		GLenum format;		/**< The format of the data.*/
		GLenum type;		/**< The type of the data.*/
		const void * data;	/**< A pointer to the data.*/

		Data1DDescription();	/**< Sets default values.*/
	};

	/** @brief Description of 2D data.
	 */
	struct Data2DDescription {
		uint width;			/**< The width of the data.*/
		uint height;		/**< The height of the data, or the number of layers in the array.*/
		GLenum format;		/**< The format of the data.*/
		GLenum type;		/**< The type of the data.*/
		const void * data;	/**< A pointer to the data.*/

		Data2DDescription();	/**< Sets default values.*/
	};

	/** @brief Description of image settings.
	 */
	struct ImageDescription {
		GLenum internalFormat;	/**< The texture's internal format.*/
		GLenum minFilter;		/**< The min filter.*/
		GLenum magFilter;		/**< The mag filter.*/
		Vector4f borderColor;	/**< The border color.*/

		bool depthCompareModeEnabled;	/**< Whether depth compare mode is enabled.*/
		GLenum depthCompareFunction;	/**< The depth compare function.*/

		ImageDescription();	/**< Sets default values.*/
	};

	/** @brief Description of 3D data.
	 */
	struct Data3DDescription {
		uint width;			/**< The width of the data.*/
		uint height;		/**< The height of the data.*/
		uint depth;			/**< The depth of the data, or the number of layers in the array.*/
		GLenum format;		/**< The format of the data.*/
		GLenum type;		/**< The type of the data.*/
		const void * data;	/**< A pointer to the data.*/

		Data3DDescription();	/**< Sets default values.*/
	};

	/** @brief Description of mipmap settings.
	 */
	struct MipmapDescription {
		int baseLevel;			/**< The base mipmap level.*/
		int maxLevel;			/**< The maximum mipmap level.*/
		float minLod;			/**< The minimum LOD level.*/
		float maxLod;			/**< The maximum LOD level.*/
		float lodBias;			/**< The LOD bias.*/
		size_t levelCount;		/**< The number of mipmaps provided.*/
		bool generateMipmaps;	/**< Whether mipmaps should be generated.*/

		MipmapDescription();	/**< Sets default values.*/
	};

	/** @brief Description of multisample settings.
	 */
	struct MultisampleDescription {
		uint samples;				/**< The number of samples per pixel.*/
		bool fixedSampleLocations;	/**< Whether sample locations are fixed.*/

		MultisampleDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a 1D texture.
	 */
	struct Texture1DDescription {
		ImageDescription imageDescription;		/**< The image settings.*/
		MipmapDescription mipmapDescription;	/**< The mipmap settings.*/
		const Data1DDescription * mipmapLevels;	/**< The array of mipmap level data.*/
		GLenum wrapS;							/**< The S wrapping mode.*/

		Texture1DDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a 1D array texture.
	 */
	struct Texture1DArrayDescription {
		ImageDescription imageDescription;		/**< The image settings.*/
		MipmapDescription mipmapDescription;	/**< The mipmap settings.*/
		const Data2DDescription * mipmapLevels;	/**< The array of mipmap level data.*/
		GLenum wrapS;							/**< The S wrapping mode.*/

		Texture1DArrayDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a 2D texture.
	 */
	struct Texture2DDescription {
		ImageDescription imageDescription;		/**< The image settings.*/
		MipmapDescription mipmapDescription;	/**< The mipmap settings.*/
		const Data2DDescription * mipmapLevels;	/**< The array of mipmap level data.*/
		GLenum wrapS;							/**< The S wrapping mode.*/
		GLenum wrapT;							/**< The T wrapping mode.*/

		Texture2DDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a 2D array texture.
	 */
	struct Texture2DArrayDescription {
		ImageDescription imageDescription;		/**< The image settings.*/
		MipmapDescription mipmapDescription;	/**< The mipmap settings.*/
		const Data3DDescription * mipmapLevels;	/**< The array of mipmap level data.*/
		GLenum wrapS;							/**< The S wrapping mode.*/
		GLenum wrapT;							/**< The T wrapping mode.*/

		Texture2DArrayDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a 2D multisample texture.
	 */
	struct Texture2DMultisampleDescription {
		ImageDescription imageDescription;				/**< The image settings.*/
		MultisampleDescription multisampleDescription;	/**< The multisample settings.*/
		uint width;										/**< The width of the texture.*/
		uint height;									/**< The height of the texture.*/
		GLenum wrapS;									/**< The S wrapping mode.*/
		GLenum wrapT;									/**< The T wrapping mode.*/

		Texture2DMultisampleDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a 2D multisample array texture.
	 */
	struct Texture2DMultisampleArrayDescription {
		ImageDescription imageDescription;				/**< The image settings.*/
		MultisampleDescription multisampleDescription;	/**< The multisample settings.*/
		uint width;										/**< The width of the texture.*/
		uint height;									/**< The height of the texture.*/
		uint depth;										/**< The number of layers in the array.*/
		GLenum wrapS;									/**< The S wrapping mode.*/
		GLenum wrapT;									/**< The T wrapping mode.*/

		Texture2DMultisampleArrayDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a 2D texture.
	 */
	struct Texture3DDescription {
		ImageDescription imageDescription;		/**< The image settings.*/
		MipmapDescription mipmapDescription;	/**< The mipmap settings.*/
		const Data3DDescription * mipmapLevels;	/**< The array of mipmap level data.*/
		GLenum wrapS;							/**< The S wrapping mode.*/
		GLenum wrapT;							/**< The T wrapping mode.*/
		GLenum wrapR;							/**< The R wrapping mode.*/

		Texture3DDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a buffer texture.
	 */
	struct TextureBufferDescription {
		GLenum internalFormat;		/**< The texture's internal format.*/
		GpuBufferReference buffer;	/**< The underlying buffer.*/

		TextureBufferDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a cube map texture.
	 */
	struct TextureCubeMapDescription {
		ImageDescription imageDescription;			/**< The image settings.*/
		MipmapDescription mipmapDescription;		/**< The mipmap settings, which apply to all faces.*/
		const Data2DDescription * mipmapLevels[6];	/**< The array of mipmap level data for each face.*/
		GLenum wrapS;								/**< The S wrapping mode.*/
		GLenum wrapT;								/**< The T wrapping mode.*/

		TextureCubeMapDescription();	/**< Sets default values.*/
	};

	/** @brief Description used to initialize a rectangle texture.
	 */
	struct TextureRectangleDescription {
		ImageDescription imageDescription;	/**< The image settings.*/
		Data2DDescription mipmapLevel;		/**< The single mipmap level data.*/
		GLenum wrapS;						/**< The S wrapping mode.*/
		GLenum wrapT;						/**< The T wrapping mode.*/

		TextureRectangleDescription();	/**< Sets default values.*/
	};

	GLuint getId() const;				/**< Returns the underlying texture ID.*/
	GLenum getType() const;				/**< Returns the texture's type.*/
	GLenum getInternalFormat() const;	/**< Returns the texture's internal format.*/

	void setImageFilters( GLenum minFilter, GLenum magFilter );	/**< Sets the image filters.*/
	GLenum getImageMinFilter() const;							/**< Returns the image min filter.*/
	GLenum getImageMagFilter() const;							/**< Returns the image mag filter.*/
	void setImageBorderColor( const Vector4f & borderColor );	/**< Sets the image border color.*/
	Vector4f getImageBorderColor() const;						/**< Returns the image border color.*/

	void setImageDepthCompareModeEnabled( bool e );							/**< Sets whether depth compare mode is enabled.*/
	bool isImageDepthCompareModeEnabled() const;							/**< Returns whether depth compare mode is enabled.*/
	void setImageDepthCompareFunction( GLenum depthCompareFunction );		/**< Sets the depth compare function.*/
	GLenum getImageDepthCompareFunction() const;							/**< Returns the depth compare function.*/

	size_t getMipmapLevelCount() const;								/**< Returns the mipmap level count.*/
	void setMipmapLevels( int baseLevel, int maxLevel );			/**< Sets the mipmap levels.*/
	int getMipmapBaseLevel() const;									/**< Returns the mipmap base level.*/
	int getMipmapMaxLevel() const;									/**< Returns the mipmap max level.*/
	void setMipmapLod( float minLod, float maxLod, float lodBias );	/**< Sets the mipmap LOD values.*/
	float getMipmapMinLod() const;									/**< Returns the mipmap minimum LOD.*/
	float getMipmapMaxLod() const;									/**< Returns the mipmap maximum LOD.*/
	float getMipmapLodBias() const;									/**< Returns the mipmap LOD bias.*/

	uint getMultisampleSamples() const;					/**< Returns the number of multisample samples.*/
	bool areMultisampleSampleLocationsFixed() const;	/**< Returns whether multisample sample locations are fixed.*/

	uint getSize( size_t dimension ) const;	/**< Returns the texture size in the given dimension, or the number of layers in the array.*/
	uint getWidth() const;	/**< Returns the texture width.*/
	uint getHeight() const;	/**< Returns the texture height, or the number of layers in the array.*/
	uint getDepth() const;	/**< Returns the texture depth, or the number of layers in the array.*/

	void setWrapS( GLenum s );	/**< Sets the S wrap mode.*/
	GLenum getWrapS() const;	/**< Returns the S wrap mode.*/
	void setWrapT( GLenum t );	/**< Sets the T wrap mode.*/
	GLenum getWrapT() const;	/**< Returns the T wrap mode.*/
	void setWrapR( GLenum r );	/**< Sets the R wrap mode.*/
	GLenum getWrapR() const;	/**< Returns the R wrap mode.*/

	GpuBufferReference getBuffer();				/**< Returns the underlying buffer for a buffer texture.*/
	GpuBufferConstReference getBuffer() const;	/**< Returns the underlying buffer for a buffer texture.*/

	void bind();							/**< Binds the texture to the currently active texture image unit.*/
	void bind( size_t textureImageUnit );	/**< Binds the texture to the given texture image unit.*/

private:
	friend class GpuState;
	GpuTexture( GpuState * gpuState, const Texture1DDescription & desc );
	GpuTexture( GpuState * gpuState, const Texture1DArrayDescription & desc );
	GpuTexture( GpuState * gpuState, const Texture2DDescription & desc );
	GpuTexture( GpuState * gpuState, const Texture2DArrayDescription & desc );
	GpuTexture( GpuState * gpuState, const Texture2DMultisampleDescription & desc );
	GpuTexture( GpuState * gpuState, const Texture2DMultisampleArrayDescription & desc );
	GpuTexture( GpuState * gpuState, const Texture3DDescription & desc );
	GpuTexture( GpuState * gpuState, const TextureBufferDescription & desc );
	GpuTexture( GpuState * gpuState, const TextureCubeMapDescription & desc );
	GpuTexture( GpuState * gpuState, const TextureRectangleDescription & desc );

	~GpuTexture();

	void onRelease() const;

	// initializes values
	void init(
		GLenum textureType,
		const ImageDescription * imageDesc,
		const MipmapDescription * mipmapDesc,
		const MultisampleDescription * multisampleDesc,
		bool setWrap );

	GLuint id;				// texture ID
	GLenum type;			// texture type
	GLenum internalFormat;	// internal texture format

	GLenum imageMinFilter;		// the image min filter
	GLenum imageMagFilter;		// the image mag filter
	Vector4f imageBorderColor;	// the image border color

	bool imageDepthCompareModeEnabled;		// whether depth stencil compare mode is enabled
	GLenum imageDepthCompareFunction;		// the image depth compare function

	size_t mipmapLevelCount;	// number of mipmap levels
	int mipmapBaseLevel;		// mipmap base level
	int mipmapMaxLevel;			// mipmap max level
	float mipmapMinLod;			// mipmap min LOD
	float mipmapMaxLod;			// mipmap max LOD
	float mipmapLodBias;		// mipmap LOD bias

	uint multisampleSamples;				// number of multisample samples
	bool multisampleFixedSampleLocations;	// whether multisample sample locations are fixed

	uint size[3];	// texture size in each dimension, or number of array layers

	GLenum wrapS;	// S wrap mode
	GLenum wrapT;	// T wrap mode
	GLenum wrapR;	// R wrap mode

	GpuBufferReference buffer;	// underlying buffer for buffer textures
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <GpuTexture> GpuTextureReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <GpuTexture> GpuTextureConstReference;

#endif