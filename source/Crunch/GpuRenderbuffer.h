/** @file GpuRenderbuffer.h
 *  @brief Encapsulates a renderbuffer on the GPU.
 */

#ifndef GPURENDERBUFFER_DEFINED
#define GPURENDERBUFFER_DEFINED

#include "Common.h"
#include "GpuObject.h"
#include <vector>

/** @brief Encapsulates a renderbuffer on the GPU.
 */
class GpuRenderbuffer : public ReferenceCountedGpuObject {
public:
	/** @brief Description used to initialize a renderbuffer.
	 */
	struct RenderbufferDescription {
		GLenum internalFormat;	/**< The renderbuffer's internal format.*/
		uint samples;			/**< The number of samples per pixel.*/
		uint width;				/**< The width of the renderbuffer.*/
		uint height;			/**< The height of the renderbuffer.*/

		RenderbufferDescription();	/**< Sets default values.*/
	};

	GLuint getId() const;					/**< Returns the underlying renderbuffer ID.*/
	GLenum getInternalFormat() const;		/**< Returns the renderbuffer's internal format.*/
	uint getSamples() const;				/**< Returns the number of samples in the renderbuffer.*/
	uint getSize( size_t dimension ) const;	/**< Returns the renderbuffer size in the given dimension.*/
	uint getWidth() const;					/**< Returns the renderbuffer width.*/
	uint getHeight() const;					/**< Returns the renderbuffer height.*/

	void bind();	/**< Binds the renderbuffer.*/

private:
	friend class GpuState;
	GpuRenderbuffer( GpuState * gpuState, const RenderbufferDescription & desc );

	~GpuRenderbuffer();

	void onRelease() const;

	GLuint id;				// internal ID
	GLenum internalFormat;	// internal format
	uint samples;			// number of samples
	uint size[2];			// size
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <GpuRenderbuffer> GpuRenderbufferReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <GpuRenderbuffer> GpuRenderbufferConstReference;

#endif