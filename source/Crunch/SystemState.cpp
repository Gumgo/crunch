#include "SystemState.h"
#include "Context.h"

SystemState * SystemState::systemState = NULL;

SystemState & SystemState::get() {
	return *systemState;
}

void SystemState::onCreate() {
	systemState = this;
	hide();

	try {
		KeyValueReader sysResIn( "./SystemResources.txt" );
		Context::get().resourceManager.registerResources( sysResIn.getIterator() );
	} catch (const std::exception & e) {
		Log::error() << e.what();
		throw std::runtime_error( "Failed to read system resources" );
	}

	counterStarted = false;
}

void SystemState::onUpdate() {
	if (!counterStarted) {
		counter = 0;
		counterStarted = true;
	} else
		++counter;
}

void SystemState::onDestroy() {
	systemState = NULL;
}

uint SystemState::getCounter() const {
	return counter;
}