#include "Runner.h"
#include "SystemState.h"
#include "Log.h"

Runner::Runner( const std::vector <std::string> & arguments, Context::RegistrationFunction registrationFunction ) {

	// load the initialization file
	std::string initPath;
	if (arguments.empty())
		initPath = "Init.txt";
	else
		initPath = arguments[0];

	KeyValueReader init( initPath );
	KeyValueReader::Iterator it = init.getIterator();

	Log::info() << "Loaded " << initPath;

	////////////////////////////////////////////////
	// READ PREFERENCES

	bool server						= it.getBool( "server" );		// whether to launch in server mode

	int width = 0;
	int height = 0;
	bool fullscreen = false;
	std::string title;

	if (!server) {
		width						= it.getInt( "width" );			// the window width
		height						= it.getInt( "height" );		// the window height
		fullscreen					= it.getBool( "fullscreen" );	// the window fullscreen flag
		title						= it.getString( "title" );		// the window title
	}

	// the frame rate
	uint framesPerSecond			= it.getUint( "framesPerSecond" );

	// the number of frames behind the CPU can fall before slowing down
	uint maxLaggedFrames			= it.getUint( "maxLaggedFrames" );

	std::string resourcePathFile	= it.getString( "resourcePathFile" );

	uint taskQueueThreads			= it.getUint( "taskQueueThreads" );

	// logging
	bool addLog						= it.contains( "log" );
	std::string log;
	if (addLog)
		log							= it.getString( "log" );

	// END READ PREFERENCES
	////////////////////////////////////////////////

	if (!server && (width <= 0 || height <= 0))
		throw std::runtime_error( "Invalid window dimensions" );
	if (framesPerSecond == 0)
		throw std::runtime_error( "Invalid framesPerSecond" );
	if (maxLaggedFrames == 0)
		throw std::runtime_error( "Invalid maxLaggedFrames" );
	if (taskQueueThreads == 0)
		throw std::runtime_error( "Invalid taskQueueThreads" );

	std::ofstream logOut;
	if (addLog) {
		logOut.open( log );
		Log::addStream( logOut );
	}

	try {
#if defined CRUNCH_PLATFORM_APPLE
		NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
#endif

		Context::Settings s;
		s.server = server;
		s.resourcePathFile = resourcePathFile;
		for (size_t i = 1; i < arguments.size(); ++i)
			s.args.push_back( arguments[i] );
		s.registrationFunction = registrationFunction;
		s.taskQueueThreads = taskQueueThreads;
		Context context( s );

		context.timer.start( framesPerSecond, maxLaggedFrames );
		if (!context.isServer()) {
			context.window.create( title, width, height, 16, fullscreen );
			context.gpuState.init();
		}

		context.gameStateManager.createGameState <SystemState>();
		context.createInitialState();

		while (!context.getQuitSignal()) {

#if defined CRUNCH_PLATFORM_APPLE
			[pool release];
			pool = [[NSAutoreleasePool alloc] init];
#endif

			if (!context.isServer()) {
				context.window.update();
				context.inputManager.update( context.window.getEventData() );
			}

			// run logic
			size_t updateTimerId = context.timer.beginTimingQuery( "update", Timer::TQ_CPU );
			context.resourceManager.update();
			context.gameStateManager.update();
			context.timer.endTimingQuery( updateTimerId );

			if (!context.isServer()) {
				// update audio
				context.audioManager.update();

				// draw the frame
				size_t preRenderTimerId = context.timer.beginTimingQuery( "pre-render", Timer::TQ_CPU );
				context.gameStateManager.render();
				context.timer.endTimingQuery( preRenderTimerId );

				size_t renderTimerCpuId = context.timer.beginTimingQuery( "render-cpu", Timer::TQ_CPU );
				size_t renderTimerGpuId = context.timer.beginTimingQuery( "render-gpu", Timer::TQ_GPU );
				context.renderer.execute();
				context.timer.endTimingQuery( renderTimerGpuId );
				context.timer.endTimingQuery( renderTimerCpuId );

				// update the display and frame time
				context.window.updateDisplay();
			}

			context.timer.update();
		}

		context.gameStateManager.clear();

		// don't destroy window here
		// it will be automatically destroyed by Context's destructor
		// since ResourceManager has some OpenGL objects, it is important to destroy the OpenGL context LAST
		context.timer.stop();

#if defined CRUNCH_PLATFORM_APPLE
		[pool release];
#endif

	} catch (const std::exception & e) {
		Log::error() << e.what();
		Log::info() << "Terminating due to unrecoverable error";
		if (addLog)
			Log::removeStream( logOut );
		throw;
	}

	if (addLog)
		Log::removeStream( logOut );
}