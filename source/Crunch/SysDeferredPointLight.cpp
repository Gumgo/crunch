#include "SysDeferredPointLight.h"
#include "Context.h"
#include "SysRendererDeferredLighting.h"
#include "SysRenderer2D.h"
#include "Log.h"

const float SysDeferredPointLight::FAR_RANGE_MULTIPLIER = 1.25f;

SysDeferredPointLight::SysDeferredPointLight()
	: SysDeferredLight( LT_POINT ) {
	setupMatrices( uniforms.faceMatrices, true, &projectionMatrix, false );

	sphereProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_point_sphere", true );
	sphereShadowProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_point_sphere_shadow", true );
	quadProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_point_quad", true );
	quadShadowProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_point_quad_shadow", true );
	geomProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___light_deferred_point_geometry", true );

	autoSphereProgram.setProgram( sphereProgram->getProgram() );
	autoQuadProgram.setProgram( quadProgram->getProgram() );
	autoGeomProgram.setProgram( geomProgram->getProgram() );
	GpuBufferReference sphereBuffer = SysRendererDeferredLighting::get().getLightVolumeVertexBuffer();
	GpuBufferReference quadBuffer = SysRenderer2D::get().getQuadVertexBuffer();
	autoSphereProgram.setAttribute( "position", sphereBuffer, 3, GL_FLOAT, false, false, sizeof( Vector3f ),
		bufferOffset( SysRendererDeferredLighting::get().getGeoSphereVertexOffset() ) );
	autoQuadProgram.setAttribute( "corner", quadBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	autoGeomProgram.setAttribute( "position", sphereBuffer, 3, GL_FLOAT, false, false, sizeof( Vector3f ),
		bufferOffset( SysRendererDeferredLighting::get().getGeoSphereVertexOffset() ) );
	autoSphereProgram.setUniform( "modelViewMatrix", GL_FLOAT_MAT3x4, &uniforms.modelViewMatrix );
	autoSphereProgram.setUniform( "projectionMatrix", GL_FLOAT_MAT4, &uniforms.projectionMatrix );
	autoSphereProgram.setUniform( "screenSize", GL_FLOAT_VEC2, &uniforms.screenSize );
	autoSphereProgram.setUniform( "radius", GL_FLOAT, &uniforms.radius );
	autoQuadProgram.setUniform( "imagePlaneHalfSize", GL_FLOAT_VEC2, &uniforms.imagePlaneHalfSize );
	autoGeomProgram.setUniform( "mvpMatrix", GL_FLOAT_MAT4, &uniforms.projectionMatrix ); // reuse proj mat
	autoGeomProgram.setUniform( "radius", GL_FLOAT, &uniforms.radius );
	autoGeomProgram.setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
	SysAutoProgram * progs[2] = { &autoSphereProgram, &autoQuadProgram };
	for (size_t i = 0; i < arraySize( progs ); ++i) {
		progs[i]->setUniform( "normalSpecMap", GL_SAMPLER_2D, &uniforms.normalSpecMap );
		progs[i]->setUniform( "depthMap", GL_SAMPLER_2D, &uniforms.depthMap );
		progs[i]->setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
		progs[i]->setUniform( "quadraticAttenuation", GL_BOOL, &uniforms.quadraticAttenuation );
		progs[i]->setUniform( "attenuation", GL_FLOAT_VEC3, &uniforms.attenuation );
		progs[i]->setUniform( "viewSpacePosition", GL_FLOAT_VEC3, &uniforms.viewSpacePosition );
	}
	uniforms.color.set( 1.0f, 1.0f, 1.0f );
	setAttenuation( AM_QUADRATIC, Vector3f( 0.0f, 0.0f, 1.0f ) );
	uniforms.normalSpecMap = 0;
	uniforms.depthMap = 2;
	setPosition( Vector3f() );
}

SysDeferredPointLight::~SysDeferredPointLight() {
}

Matrix44af SysDeferredPointLight::computeFaceMatrix( size_t face ) const {
	// +x, -x, +y, -y, +z, -z
	static const Vector3f fwdUp[6][2] = {
		{ Vector3f(  1.0f,  0.0f,  0.0f ), Vector3f(  0.0f, -1.0f,  0.0f ) },
		{ Vector3f( -1.0f,  0.0f,  0.0f ), Vector3f(  0.0f, -1.0f,  0.0f ) },
		{ Vector3f(  0.0f,  1.0f,  0.0f ), Vector3f(  0.0f,  0.0f,  1.0f ) },
		{ Vector3f(  0.0f, -1.0f,  0.0f ), Vector3f(  0.0f,  0.0f, -1.0f ) },
		{ Vector3f(  0.0f,  0.0f,  1.0f ), Vector3f(  0.0f, -1.0f,  0.0f ) },
		{ Vector3f(  0.0f,  0.0f, -1.0f ), Vector3f(  0.0f, -1.0f,  0.0f ) }
	};
	// todo: optimize this a bit (not a big deal probably)
	Vector3f b = -fwdUp[face][0];
	Vector3f u = fwdUp[face][1];
	Vector3f r = u.cross( b );
	return Matrix44af(
		r.x, r.y, r.z, -r.dot( position ),
		u.x, u.y, u.z, -u.dot( position ),
		b.x, b.y, b.z, -b.dot( position ) );
}

Matrix44f SysDeferredPointLight::computeProjectionMatrix() const {
	float n = uniforms.lightNearFar.x;
	float f = uniforms.lightNearFar.y;
	return Matrix44f(
		1.0f,	0.0f,	0.0f,			0.0f,
		0.0f,	1.0f,	0.0f,			0.0f,
		0.0f,	0.0f,	(f+n)/(n-f),	(2.0f*f*n)/(n-f),
		0.0f,	0.0f,	-1.0f,			0.0f );
}

void SysDeferredPointLight::enableShadow( size_t size, float minNear, float maxFar, float constantBias, float maxMultiplierBias ) {
	shadowsEnabled = true;

	GpuTexture::TextureCubeMapDescription desc;
	desc.imageDescription.internalFormat = GL_DEPTH_COMPONENT;
	desc.imageDescription.minFilter = GL_NEAREST;
	desc.imageDescription.magFilter = GL_NEAREST;
	desc.wrapS = GL_CLAMP_TO_EDGE;
	desc.wrapT = GL_CLAMP_TO_EDGE;
	GpuTexture::Data2DDescription mip;
	mip.width = (uint)size;
	mip.height = (uint)size;
	mip.format = GL_DEPTH_COMPONENT;
	mip.type = GL_FLOAT;
	std::fill( desc.mipmapLevels, desc.mipmapLevels + 6, &mip );
	shadowMap = Context::get().gpuState.createTexture( desc );
	shadowMinNear = minNear;
	shadowMaxFar = maxFar;

	autoSphereProgram.setProgram( sphereShadowProgram->getProgram() );
	autoQuadProgram.setProgram( quadShadowProgram->getProgram() );
	GpuBufferReference sphereBuffer = SysRendererDeferredLighting::get().getLightVolumeVertexBuffer();
	GpuBufferReference quadBuffer = SysRenderer2D::get().getQuadVertexBuffer();
	autoSphereProgram.setAttribute( "position", sphereBuffer, 3, GL_FLOAT, false, false, sizeof( Vector3f ),
		bufferOffset( SysRendererDeferredLighting::get().getGeoSphereVertexOffset() ) );
	autoQuadProgram.setAttribute( "corner", quadBuffer, 2, GL_FLOAT, false, false, sizeof( SysRenderer2D::QuadVertex ),
		bufferOffset( offsetof( SysRenderer2D::QuadVertex, corner ) ) );
	autoSphereProgram.setUniform( "modelViewMatrix", GL_FLOAT_MAT3x4, &uniforms.modelViewMatrix );
	autoSphereProgram.setUniform( "projectionMatrix", GL_FLOAT_MAT4, &uniforms.projectionMatrix );
	autoSphereProgram.setUniform( "screenSize", GL_FLOAT_VEC2, &uniforms.screenSize );
	autoSphereProgram.setUniform( "radius", GL_FLOAT, &uniforms.radius );
	autoQuadProgram.setUniform( "imagePlaneHalfSize", GL_FLOAT_VEC2, &uniforms.imagePlaneHalfSize );
	SysAutoProgram * progs[2] = { &autoSphereProgram, &autoQuadProgram };
	for (size_t i = 0; i < arraySize( progs ); ++i) {
		progs[i]->setUniform( "normalSpecMap", GL_SAMPLER_2D, &uniforms.normalSpecMap );
		progs[i]->setUniform( "depthMap", GL_SAMPLER_2D, &uniforms.depthMap );
		progs[i]->setUniform( "color", GL_FLOAT_VEC3, &uniforms.color );
		progs[i]->setUniform( "quadraticAttenuation", GL_BOOL, &uniforms.quadraticAttenuation );
		progs[i]->setUniform( "attenuation", GL_FLOAT_VEC3, &uniforms.attenuation );
		progs[i]->setUniform( "viewSpacePosition", GL_FLOAT_VEC3, &uniforms.viewSpacePosition );

		progs[i]->setUniform( "faceNormalMap", GL_SAMPLER_2D, &uniforms.faceNormalMap );
		progs[i]->setUniform( "shadowMap", GL_SAMPLER_CUBE, &uniforms.shadowMap );
		progs[i]->setUniform( "viewToLightMatrix", GL_FLOAT_MAT3x4, &uniforms.viewToLightMatrix );
		progs[i]->setUniform( "halfResolution", GL_FLOAT, &uniforms.halfResolution );
		progs[i]->setUniform( "constantBias", GL_FLOAT, &uniforms.constantBias );
		progs[i]->setUniform( "maxMultiplierBias", GL_FLOAT, &uniforms.maxMultiplierBias );
		progs[i]->setUniform( "faceMatrices", GL_FLOAT_MAT3x4, uniforms.faceMatrices, 0, 6 );
		progs[i]->setUniform( "lightNearFar", GL_FLOAT_VEC2, &uniforms.lightNearFar );
	}
	uniforms.faceNormalMap = 1;
	uniforms.shadowMap = 4;
	uniforms.halfResolution = 0.5f * (float)size;
	uniforms.constantBias = constantBias;
	uniforms.maxMultiplierBias = maxMultiplierBias;
	// set the near/far uniforms only when computing shadow z range
}

void SysDeferredPointLight::setColor( const Vector3f & c ) {
	uniforms.color = c;
	if (attenuationMode == AM_QUADRATIC)
		range = computeQuadraticAttenuationRange( uniforms.color, uniforms.attenuation );
}

Vector3f SysDeferredPointLight::getColor() const {
	return uniforms.color;
}

void SysDeferredPointLight::setPosition( const Vector3f & p ) {
	position = p;
	for (size_t i = 0; i < 6; ++i)
		uniforms.faceMatrices[i] = computeFaceMatrix( i );
}

Vector3f SysDeferredPointLight::getPosition() const {
	return position;
}

void SysDeferredPointLight::setAttenuation( SysDeferredLight::AttenuationMode mode, const Vector3f & params ) {
	if (mode == AM_QUADRATIC) {
		if ((params[0] == 0.0f && params[1] == 0.0f && params[2] == 0.0f) || params[0] < 0.0f || params[1] < 0.0f || params[2] < 0.0f) {
			Log::warning() << "Invalid point light quadratic attenuation parameters [" <<
				params[0] << ", " << params[1] << ", " << params[2] << "]";
			return;
		}
		uniforms.attenuation = params;
		range = computeQuadraticAttenuationRange( uniforms.color, uniforms.attenuation );
		uniforms.quadraticAttenuation = 1;
	} else if (mode == AM_RADIAL) {
		if (params[0] < 0.0f || params[1] <= params[0]) {
			Log::warning() << "Invalid point light radial attenuation parameters [" <<
				params[0] << ", " << params[1] << "]";
			return;
		}
		uniforms.attenuation = params;
		range = params[1];
		uniforms.quadraticAttenuation = 0;
	} else
		assert( false );
	attenuationMode = mode;
}

SysDeferredPointLight::AttenuationMode SysDeferredPointLight::getAttenuationMode() const {
	return attenuationMode;
}

Vector3f SysDeferredPointLight::getAttenuationParameters() const {
	return uniforms.attenuation;
}

void SysDeferredPointLight::computeShadowMapRange( fastdelegate::FastDelegate0 <const SysAabb*> nextAabb ) {
	float aabbNear = std::numeric_limits <float>::max();
	float aabbFar = 0.0f;
	float range2 = range*range;
	const SysAabb * aabb;
	while ((aabb = nextAabb()) != NULL) {
		// find the closest point on/in the box
		Vector3f closest = vecMin( position, aabb->maxBound );
		closest = vecMax( closest, aabb->minBound );
		const Vector3f * bounds[2] = { &aabb->minBound, &aabb->maxBound };
		float aabbNearDist2 = (position - closest).magnitudeSquared();

		Vector3f middle = (aabb->minBound + aabb->maxBound)*0.5f;
		// find the furthest point on the box
		Vector3f farthest(
			bounds[position.x < middle.x]->x,
			bounds[position.y < middle.y]->y,
			bounds[position.z < middle.z]->z );
		float aabbFarDist2 = (position - farthest).magnitudeSquared();

		// if the closest point is further than the range, don't expand far bound
		if (aabbNearDist2 <= range2)
			aabbFar = std::max( aabbFar, aabbFarDist2 );
		aabbNear = std::min( aabbNear, aabbNearDist2 );
	}
	if (aabbNear > aabbFar)
		// no objects in range; set default near/far
		uniforms.lightNearFar.set( shadowMinNear, range );
	else
		// far range is whichever is smaller: light radius or furthest object
		uniforms.lightNearFar.set( sqrt( aabbNear ), std::min( range, sqrt( aabbFar ) ) );
	uniforms.lightNearFar[1] *= FAR_RANGE_MULTIPLIER;
	// clamp min/max
	uniforms.lightNearFar[0] = std::max( uniforms.lightNearFar[0], shadowMinNear );
	uniforms.lightNearFar[1] = std::min( uniforms.lightNearFar[1], shadowMaxFar );
	projectionMatrix = computeProjectionMatrix();
}

void SysDeferredPointLight::drawShadowMap(
	fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
	fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject ) {
	if (shadowMap.get() == NULL)
		return;

	GpuViewportState vpState = Context::get().gpuState.createViewportState();
	vpState.setViewportSize( Vector2i( (int)shadowMap.get()->getWidth(), (int)shadowMap.get()->getHeight() ) );
	vpState.bind();

	GpuFramebufferReference fb = Context::get().gpuState.getBoundFramebuffer( GL_DRAW_FRAMEBUFFER );
	fb->detatch( GL_COLOR_ATTACHMENT0 );
	fb->detatch( GL_COLOR_ATTACHMENT1 );
	fb->detatch( GL_COLOR_ATTACHMENT2 );
	fb->detatch( GL_COLOR_ATTACHMENT3 );

	GpuDrawBufferState dbState = Context::get().gpuState.createDrawBufferState();
	dbState.setDrawBuffers( 0, NULL );
	dbState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthWriteEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.bind();

	GpuClearCall clearCall = Context::get().gpuState.createClearCall();
	clearCall.setClearDepth( 1.0f );
	clearCall.setClearBuffers( GL_DEPTH_BUFFER_BIT );

	for (uint f = 0; f < 6; ++f) {
		fb->attachLayer( GL_DEPTH_ATTACHMENT, shadowMap, f );
		clearCall.clear();

		ShadowMapRenderer r;
		const SysAabb * aabb;
		while (r = nextObject(), (aabb = nextAabb()) != NULL)
			// TODO: cull per-face eventually
			r( this, f );
	}
}

void SysDeferredPointLight::draw() {
	if (camera == NULL)
		return;

	uniforms.radius = range * SysRendererDeferredLighting::get().getGeoSphere().getInscriptionRadiusMultiplier();
	uniforms.viewSpacePosition = camera->getViewMatrix().transformPoint( position );
	float depth = -uniforms.viewSpacePosition.z;
	if (depth - uniforms.radius > camera->getFar() || depth + uniforms.radius < camera->getNear())
		return;

	// if the light intersects the far plane, we must draw the front faces
	// if the light intersects the near plane, we must draw the back faces
	// otherwise we can do either (or both using stencil test, but that is probably inefficient)
	bool frontFaces = (depth + uniforms.radius) > camera->getFar();
	// if intersects BOTH planes (e.g. no attenuation) then we need to draw using a fullscreen quad
	bool quad = (frontFaces && depth - uniforms.radius < camera->getNear());

	// renderer has already bound textures

	if (shadowMap.get() != NULL) {
		if (quad && quadShadowProgram->getStatus() != Resource::S_LOADED || sphereShadowProgram->getStatus() != Resource::S_LOADED)
			return;
		shadowMap->bind( (size_t)uniforms.shadowMap );
		uniforms.viewToLightMatrix = Matrix44af().translated( -position ) * camera->getCameraMatrix();
	} else {
		if (quad && quadProgram->getStatus() != Resource::S_LOADED || sphereProgram->getStatus() != Resource::S_LOADED)
			return;
	}

	if (quad) {
		uniforms.imagePlaneHalfSize.y = tan( degToRad( camera->getFov() ) * 0.5f );
		uniforms.imagePlaneHalfSize.x = uniforms.imagePlaneHalfSize.y * camera->getAspectRatio();

		GpuRasterizerState rState = Context::get().gpuState.getCurrentRasterizerState();
		rState.setCullingEnabled( false );
		rState.bind();

		GpuDepthStencilState dsState = Context::get().gpuState.getCurrentDepthStencilState();
		dsState.setDepthTestEnabled( false );
		dsState.bind();

		SysRenderer2D::get().getQuadVertexBuffer()->bind();
		autoQuadProgram.bind();

		SysRenderer2D::get().drawSingleQuad();
	} else {
		uniforms.modelViewMatrix = camera->getModelViewMatrix( Matrix44af().translated( position ) );
		uniforms.projectionMatrix = camera->getProjectionMatrix();
		uniforms.screenSize = (Vector2f)Context::get().gpuState.getCurrentViewportState().getViewportSize();

		GpuRasterizerState rState = Context::get().gpuState.getCurrentRasterizerState();
		rState.setCullingEnabled( true );

		GpuDepthStencilState dsState = Context::get().gpuState.getCurrentDepthStencilState();
		dsState.setDepthTestEnabled( true );

		if (frontFaces) {
			rState.setCullMode( GL_BACK );
			dsState.setDepthFunction( GL_LEQUAL );
		} else {
			rState.setCullMode( GL_FRONT );
			dsState.setDepthFunction( GL_GEQUAL );
		}

		rState.bind();
		dsState.bind();

		SysRendererDeferredLighting::get().getLightVolumeVertexBuffer()->bind();
		SysRendererDeferredLighting::get().getLightVolumeIndexBuffer()->bind();

		autoSphereProgram.bind();

		GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
		size_t vertexCount = SysRendererDeferredLighting::get().getGeoSphere().getVertices().size();
		size_t indexCount = SysRendererDeferredLighting::get().getGeoSphere().getIndices().size();
		size_t indexOffset = SysRendererDeferredLighting::get().getGeoSphereIndexOffset();
		drawCall.setParameters(
			GL_TRIANGLES, 0, vertexCount-1, indexCount, GL_UNSIGNED_SHORT, bufferOffset( indexOffset ) );
		drawCall.draw();
	}
}

void SysDeferredPointLight::drawGeometry() {
	if (geomProgram->getStatus() != Resource::S_LOADED || camera == NULL)
		return;

	uniforms.radius = range * SysRendererDeferredLighting::get().getGeoSphere().getInscriptionRadiusMultiplier();
	uniforms.viewSpacePosition = camera->getViewMatrix().transformPoint( position );
	float depth = -uniforms.viewSpacePosition.z;
	if (depth - uniforms.radius > camera->getFar() || depth + uniforms.radius < camera->getNear())
		return;

	// if the light intersects the far plane, we must draw the front faces
	// if the light intersects the near plane, we must draw the back faces
	// otherwise we can do either (or both using stencil test, but that is probably inefficient)
	bool frontFaces = (depth + uniforms.radius) > camera->getFar();
	// if intersects BOTH planes (e.g. no attenuation) then we need to draw using a fullscreen quad
	bool quad = (frontFaces && depth - uniforms.radius < camera->getNear());

	if (quad)
		return;

	// just reuse this for mvp
	uniforms.projectionMatrix = camera->getModelViewProjectionMatrix( Matrix44af().translated( position ) );

	GpuRasterizerState rState = Context::get().gpuState.createRasterizerState();
	rState.setCullingEnabled( false );
	rState.setPolygonMode( GL_LINE );
	rState.bind();

	GpuDepthStencilState dsState = Context::get().gpuState.createDepthStencilState();
	dsState.setDepthTestEnabled( true );
	dsState.setDepthFunction( GL_LEQUAL );
	dsState.bind();

	SysRendererDeferredLighting::get().getLightVolumeVertexBuffer()->bind();
	SysRendererDeferredLighting::get().getLightVolumeIndexBuffer()->bind();

	autoGeomProgram.bind();

	GpuIndexedRangedDrawCall drawCall = Context::get().gpuState.createIndexedRangedDrawCall();
	size_t vertexCount = SysRendererDeferredLighting::get().getGeoSphere().getVertices().size();
	size_t indexCount = SysRendererDeferredLighting::get().getGeoSphere().getIndices().size();
	size_t indexOffset = SysRendererDeferredLighting::get().getGeoSphereIndexOffset();
	drawCall.setParameters(
		GL_TRIANGLES, 0, vertexCount-1, indexCount, GL_UNSIGNED_SHORT, bufferOffset( indexOffset ) );
	drawCall.draw();

	rState.setPolygonMode( GL_FILL );
	rState.bind();
}