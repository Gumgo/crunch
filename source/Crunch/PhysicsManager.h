#if 0

/** @file PhysicsManager.h
 *  @brief Manages the game's physics.
 */

#ifndef PHYSICSMANAGER_DEFINED
#define PHYSICSMANAGER_DEFINED

#include "Common.h"
#include "RoomData.h"
#include "PhysicsObjects.h"
#include "PhysicsFunctions.h"
#include "Indexer.h"
#include "HashMap.h"
#include "HashFunctions.h"
#ifdef _DEBUG
#include "ProgramData.h"
#include "GpuBuffer.h"
#include "SysCamera2D.h"
#endif
#include <vector>
#include <map>
#include <set>
#include <boost/function.hpp>

// TODO: "bounce" mode vs walking mode
// testing capsule casts manually
// testing raycasts manually

/** @brief Stores parameters for a dynamic actor.
 */
struct DynamicActorParameters {
	float gravity;				/**< The gravity.*/
	float maxGravitySpeed;		/**< The maximum vertical speed.*/
	float maxSlopeAngle;		/**< The maximum slope angle before sliding, in degrees.*/
	float walkSpeed;			/**< The walking speed.*/
	float walkAccel;			/**< The walking acceleration.*/
	float airAccel;				/**< The air acceleration.*/
	float jumpSpeed;			/**< The jumping speed.*/
	bool hitsColliders;			/**< Whether the dynamic actor reacts to hitting colliders.*/
	bool hitsActors;			/**< Whether the dynamic actor reacts to hitting actors.*/
	size_t type;				/**< The type used to determine the callback upon collision.*/
	void * data;				/**< The data passed into the callback upon collision.*/

	DynamicActorParameters();	/**< Assigns default values to the parameters.*/
};

/** @brief Stores parameters for a raycast.
 */
struct RaycastParameters {
	Vector2f origin;			/**< The origin of the ray.*/
	Vector2f direction;			/**< The direction of the ray.*/
	float maxDistance;			/**< The maximum distance the ray is allowed to travel.*/
	bool stopsAtColliders;		/**< Whether the ray stops at the first collider it hits.*/
	bool stopsAtDynamicActors;	/**< Whether the ray stops at the first dynamic actor it hits.*/
	size_t type;				/**< The type used to determine the callback upon collision.*/
	void * data;				/**< The data passed into callbacks upon collision.*/

	RaycastParameters();		/**< Assigns default values to the parameters.*/
};

/** @brief The result of a raycast.
 */
struct RaycastResult {
	bool hitCollider;		/**< Whether the ray hit a collider.*/
	size_t colliderId;		/**< The ID of the collider hit, if the ray hit a collider.*/
	bool hitDynamicActor;	/**< Whether the ray hit at least one dynamic actor.*/
	float distance;			/**< The distance the ray traveled before impact.*/
};

/** @brief Manages the game's physics.
 */
class PhysicsManager : private boost::noncopyable {
	/** @brief A static line segment in the physics manager.
	 */
	struct StaticCollider {
		size_t id;					/**< The ID of this line segment in the room.*/
		LineSegment lineSegment;	/**< The line segment.*/
		StaticCollider( const Vector2f * pts, size_t segId );	/**< Initializes a static collider.*/
	};

	/** @brief A set of dynamic line segments in the physics manager.
	 */
	struct DynamicCollider {
		// TODO
	};

	/** @brief A dynamic actor in the physics manager.
	 */
	struct DynamicActor {
		Capsule capsule;			/**< The capsule.*/
		Vector2f velocity;			/**< The velocity.*/
		DynamicActorParameters p;	/**< The dynamic actor's parameters.*/
		float maxSlopeAngleTan;		/**< The sine of maxSlopeAngle.*/
		float maxSlopeNormalY;		/**< The maximum normal of a slope corresponding to maxSlopeAngle.*/
		int updatePriority;			/**< The update priority.*/

		int walking;				/**< Whether the actor is standing still (0), walking left (-1), or walking right (1).*/
		bool jump;					/**< Whether to attempt to jump next update.*/

		/** @brief Position in the update list.
		 */
		std::multimap <int, size_t>::iterator updatePosition;
	};

	/** @brief The list of static colliders.
	 */
	std::vector <StaticCollider> staticColliders;

	/** @brief The size of the uniform grid used.
	 */
	static const float GRID_SIZE;

	/** @brief Each cell contains a list of static collider IDs.
	 */
	struct GridCell {
		std::vector <size_t> staticColliderIds;	/**< List of static collider IDs contained in this grid cell.*/
		std::vector <size_t> dynamicActorIds;	/**< List of dynamic actor IDs contained in this grid cell.*/
	};

	Vector2i gridMin;	/**< The first grid cell.*/
	Vector2i gridMax;	/**< One past the last grid cell.*/
	std::vector <GridCell> gridCells;			/**< The cells in the grid.*/
	size_t gridCellId( int x, int y ) const;	/**< Returns the ID of a grid cell.*/

	/** @brief Contains the indexed dynamic actors in the physics manager.
	 */
	Indexer <DynamicActor> dynamicActors;

	/** @brief The ordered update list of dynamic actors.
	 */
	std::multimap <int, size_t> dynamicActorUpdateList;

	/** @brief A callback for two colliding dynamic actors.
	 */
	struct CollisionCallback {
		boost::function <void ( void*, void* )> callback;	/**< The function to call.*/
		bool flipData;										/**< Whether to pass (actor0, actor1) or (actor1, actor0).*/
	};

	/** @brief Table of collision callbacks.
	 */
	HashMap <Hash <std::pair <size_t, size_t>>, std::pair <size_t, size_t>, CollisionCallback> collisionCallbacks;

	/** @brief The capacity for the collision callback table.
	 */
	static const size_t COLLISION_CALLBACKS_CAPACITY = 50;

	/** @brief Convenience method to throw an exception if an invalid index is specified.
	 */
	void checkDynamicActorIndex( size_t index ) const;

	// ---- helper data structures in physics update loop to reduce per-frame allocation ---- //
	std::vector <size_t> potentialColliders;	/**< List of colliders to check for collision.*/
	std::vector <bool> colliderAdded;			/**< Whether each collider has been added as a potential collider.*/
	std::set <size_t> potentialActors;			/**< List of dynamic actors to check for collision.*/
	std::multimap <int, size_t>::iterator updateIteratorOuter;	/**< The current dynamic actor being updated in the outer loop.*/
	std::set <size_t>::iterator updateIteratorInner;			/**< The current dynamic actor being updated in the inner loop.*/

#ifdef _DEBUG
	const ProgramData * linesProgram;	/**< The program used to draw lines for debugging.*/
	const ProgramData * capsuleProgram;	/**< The program used to draw capsules for debugging.*/

	/** @brief The capsule vertex structure.
	 */
	struct CapsuleVertex {
		float corner[2];	/**< The vertex corner.*/
		float padding[2];	/**< Padding.*/
	};

	/** @brief The line vertex structure.
	 */
	struct LineVertex {
		float pos[2];		/**< The vertex position.*/
		float col[4];		/**< The vertex color.*/
		float padding[2];	/**< The vertex padding.*/
	};

	std::auto_ptr <GpuBuffer> vertexBuffer;		/**< The vertex buffer to draw capsules and lines when debugging.*/
	std::auto_ptr <GpuBuffer> indexBuffer;		/**< The index buffer to draw capsules when debugging.*/
	size_t lineVertexBufferOffset;				/**< The offset of the start of the lines in the vertex buffer.*/
#endif

	// PHYSICS UPDATE FUNCTIONS:

	void getPotentialColliders( const Box & bounds );		/**< Creates the list of potential colliders based on bounds.*/
	void getPotentialColliders( const LineSegment & ls );	/**< Creates the list of potential colliders based on ls.*/
	void clearPotentialColliders();							/**< Clears the list of potential colliders.*/
	void getPotentialActors( const Box & bounds );			/**< Creates the list of potential actors based on bounds.*/
	void getPotentialActors( const LineSegment & ls );		/**< Creates the list of potential actors based on ls.*/

	/** @brief Removes a dynamic actor from the cells it is in.
	 */
	void removeFromCells( size_t id );

	/** @brief Adds a dynamic actor to its appropriate cells.
	 */
	void addToCells( size_t id );

	/** @brief Checks whether the actor is on the ground and if so sets the appropriate output parameters.
	 */
	bool isOnGround( DynamicActor & a, Vector2f & groundNormal, size_t & groundId );

	/** @brief Applies gravity, walking, and jumping forces to an actor.
	 */
	void applyForces( DynamicActor & a, bool onGround, const Vector2f & groundNormal, size_t groundId );

	/** @brief Performs an iteration of the physics loop.
	 */
	void iteratePhysics( DynamicActor & a, float & velocityRemaining );

	/** @brief Moves an actor so that it is not colliding with the scene.
	 */
	void moveOut( DynamicActor & a );

	/** @brief Returns whether a collision will occur and fills in appropriate output parameters if so.
	 */
	bool collide( DynamicActor & a, const Vector2f & velocity, float & sweepTime, Vector2f & isectNormal, Vector2f & isectPoint, size_t & collisionId );

	/** @brief Responds to a collision by moving the actor and adjusting the actor's velocity.
	 */
	void respond( DynamicActor & a, const Vector2f & velocity, float sweepTime, const Vector2f & isectNormal, const Vector2f & isectPoint, float & velocityRemaining );

	/** @brief Moves the actor to the floor if it is within range.
	 */
	void moveToGround( DynamicActor & a, const Vector2f & prevPosition );

public:
	/** @brief The constructor.
	 */
	PhysicsManager();

	/** @brief Initializes the static line segments using room data.
	 */
	void initRoom( const RoomData * room );

	/** @brief Updates the physics simulation.
	 */
	void update();

#ifdef _DEBUG
	/** @brief Draws colliders for debugging.
	 */
	void draw( const SysCamera2D * camera );
#endif

	/** @brief Creates a dynamic actor and returns its index.
	 */
	size_t createDynamicActor( int updatePriority, const Vector2f & position, float height, float radius, const DynamicActorParameters & params );

	/** @brief Destroys the dynamic actor with the given index.
	 */
	void destroyDynamicActor( size_t index );

	Vector2f getDynamicActorPosition( size_t index ) const;							/**< Sets the dynamic actor's position.*/
	void setDynamicActorPosition( size_t index, const Vector2f & position );		/**< Returns the dynamic actor's position.*/

	Vector2f getDynamicActorVelocity( size_t index ) const;							/**< Sets the dynamic actor's velocity.*/
	void setDynamicActorVelocity( size_t index, const Vector2f & velocity );		/**< Returns the dynamic actor's velocity.*/

	float getDynamicActorHeight( size_t index ) const;								/**< Sets the dynamic actor's height.*/
	void setDynamicActorHeight( size_t index, float height );						/**< Returns the dynamic actor's height.*/

	float getDynamicActorRadius( size_t index ) const;								/**< Sets the dynamic actor's radius.*/
	void setDynamicActorRadius( size_t index, float radius );						/**< Returns the dynamic actor's radius.*/

	float getDynamicActorGravity( size_t index ) const;								/**< Sets the dynamic actor's gravity.*/
	void setDynamicActorGravity( size_t index, float gravity );						/**< Returns the dynamic actor's gravity.*/

	float getDynamicActorMaxGravitySpeed( size_t index ) const;						/**< Sets the dynamic actor's maximum gravity speed.*/
	void setDynamicActorMaxGravitySpeed( size_t index, float maxGravitySpeed );		/**< Returns the dynamic actor's maximum gravity speed.*/

	float getDynamicActorMaxSlopeAngle( size_t index ) const;						/**< Sets the dynamic actor's maximum slope angle.*/
	void setDynamicActorMaxSlopeAngle( size_t index, float maxSlopeAngle );			/**< Returns the dynamic actor's maximum slope angle.*/

	float getDynamicActorWalkSpeed( size_t index ) const;							/**< Sets the dynamic actor's walking speed.*/
	void setDynamicActorWalkSpeed( size_t index, float walkSpeed );					/**< Returns the dynamic actor's walking speed.*/

	float getDynamicActorWalkAccel( size_t index ) const;							/**< Sets the dynamic actor's walking acceleration.*/
	void setDynamicActorWalkAccel( size_t index, float walkAccel );					/**< Returns the dynamic actor's walking acceleration.*/

	float getDynamicActorAirAccel( size_t index ) const;							/**< Sets the dynamic actor's air acceleration.*/
	void setDynamicActorAirAccel( size_t index, float airAccel );					/**< Returns the dynamic actor's air acceleration.*/

	float getDynamicActorJumpSpeed( size_t index ) const;							/**< Sets the dynamic actor's jumping speed.*/
	void setDynamicActorJumpSpeed( size_t index, float jumpSpeed );					/**< Returns the dynamic actor's jumping speed.*/

	int getDynamicActorUpdatePriority( size_t index ) const;						/**< Sets the dynamic actor's update priority.*/
	void setDynamicActorUpdatePriority( size_t index, int updatePriority );			/**< Returns the dynamic actor's update priority.*/

	bool isDynamicActorActive( size_t index ) const;								/**< Returns whether the dynamic actor is active.*/
	void activateDynamicActor( size_t index );										/**< Activates the dynamic actor.*/
	void deactivateDynamicActor( size_t index );									/**< Deactivates the dynamic actor.*/

	void dynamicActorWalk( size_t index, bool left, bool right );					/**< Sets whether the dynamic actor should be walking.*/
	void dynamicActorJump( size_t index );											/**< Causes the dynamic actor to attempt to jump the next update.*/

	/** @brief Adds a callback to be called when dynamic actors of types type1 and type2 collide.
	 *
	 *  The order type1 and type2 are listed determines the
	 *  order in which data is passed into the callback,
	 *  where the actor of type1 provides the first parameter
	 *  and the actor of type2 provides the second. Only one
	 *  ordering may exist at a time. If a callback exists
	 *  taking (type1, type2), adding one taking (type2, type1)
	 *  will replace it.
	 */
	void addCollisionCallback( size_t type1, size_t type2, const boost::function <void ( void*, void* )> & callback );

	/** @brief Removes the callback called when dynamic actors of types type1 and type2 collide.
	 *
	 *  The order type1 and type2 are listed does not
	 *  matter; both (type1, type2) and (type2, type1)
	 *  callbacks will be removed.
	 */
	void removeCollisionCallback( size_t type2, size_t type1 );

	/** @brief Casts a ray and returns the result.
	 */
	RaycastResult raycast( const RaycastParameters & params );

	/** @brief Returns whether placing the given capsule or sweeping it along the given vector will result in a collision with a collider.
	 */
	bool isPathFree( const Capsule & c, const Vector2f & sweepVec = Vector2f( 0.0f, 0.0f ) );
};

#endif

#endif