#ifndef SYSDEFERREDDIRECTIONALLIGHT_DEFINED
#define SYSDEFERREDDIRECTIONALLIGHT_DEFINED

#include "Common.h"
#include "SysDeferredLight.h"
#include "ProgramResource.h"
#include "SysAutoProgram.h"
#include "GpuTexture.h"

class SysDeferredDirectionalLight : public SysDeferredLight {
public:
	SysDeferredDirectionalLight();
	~SysDeferredDirectionalLight();

	void enableShadow(
		size_t size, size_t slices,
		float constantBias = 0.1f,
		float maxSlopeBiasAngle = DEFAULT_MAX_SLOPE_BIAS_ANGLE );

	void setColor( const Vector3f & c );
	Vector3f getColor() const;
	void setDirection( const Vector3f & d ); // normalizes upon setting
	Vector3f getDirection() const;

	Matrix44af getLightMatrix() const;
	Matrix44f getProjectionMatrix() const;

	void computeShadowMapRange(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb );
	void drawShadowMap(
		fastdelegate::FastDelegate0 <const SysAabb*> nextAabb,
		fastdelegate::FastDelegate0 <ShadowMapRenderer> nextObject );

	void draw();

private:
	ProgramResourceReference program;
	ProgramResourceReference shadowProgram;

	static const size_t MAX_SLICES = 6;
	GpuTextureReference shadowMap;
	std::vector <fastdelegate::FastDelegate1 <SysDeferredDirectionalLight*, void>> shadowMapObjects;
	size_t shadowMapSlices;
	float slicePositions[MAX_SLICES+1];
	float maxSlopeBiasAngleTan;

	Matrix44af lightMatrix;
	Matrix44f projectionMatrices[MAX_SLICES];

	SysAutoProgram autoProgram;
	struct {
		Vector2f imagePlaneHalfSize;
		int normalSpecMap;
		int depthMap;
		Vector3f color;
		Vector3f viewSpaceDirection;

		int faceNormalMap;
		int shadowMap;
		Matrix44af viewToLightMatrix;
		float halfResolution;
		float constantBias;
		float maxSlopeBias[MAX_SLICES];
		int slices;
		float zSplits[MAX_SLICES-1];
		Vector2f clipSpaceHalfSize[MAX_SLICES];
		Vector2f clipSpaceCenter[MAX_SLICES];
		Vector2f lightNearFar[MAX_SLICES];
	} uniforms;

	Vector3f direction;

	Matrix44af computeLightMatrix() const;
	Matrix44f computeProjectionMatrix( size_t i ) const;
	void computeSlices();

	// for some reason I can't make a default parameter use boost::constants::math::pi <float>()
	static const float DEFAULT_MAX_SLOPE_BIAS_ANGLE;
};

#endif