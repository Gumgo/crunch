#include "GameStateManager.h"
#include "Context.h"
#include "Log.h"

GameStateManager::GameStateManager() {
}

GameStateManager::~GameStateManager() {
	clear();
}

void GameStateManager::update() {
	// game state updates are deferred to the beginning of each frame
	executePreFrameCommands();

	// hide/show game states
	Indexer <GameStateData>::Iterator it = gameStates.getIterator();
	while (it.next()) {
		GameStateData & gsd = it.get();
		if (gsd.gameState->isVisible() && !gsd.shouldBeVisible) {
			// hide
			renderList.erase( gsd.renderPosition );
			gsd.renderPosition = renderList.end();
		} else if (!gsd.gameState->isVisible() && gsd.shouldBeVisible)
			// show
			gsd.renderPosition = renderList.insert( std::make_pair( gsd.gameState->getRenderPriority(), it.getIndex() ) );
	}

	// update game states
	std::multimap <int, size_t>::iterator updateIterator = updateList.begin();
	while (updateIterator != updateList.end()) {
		size_t index = updateIterator->second;
		++updateIterator;

		GameState * gameState = gameStates.get( index ).gameState;
		gameState->onUpdate();
		gameState->update();
		gameState->onPostUpdate();
	}
}

void GameStateManager::render() {
	std::multimap <int, size_t>::iterator renderIterator = renderList.begin();
	while (renderIterator != renderList.end()) {
		size_t index = renderIterator->second;
		++renderIterator;

		GameState * gameState = gameStates.get( index ).gameState;
		gameState->onDraw();
		gameState->render();
	}
}

void GameStateManager::clear() {
	// perform ordered cleanup before simply destroying all game states
	executePreFrameCommands();

	// clear all game states
	std::vector <size_t> indicesToDestroy;
	Indexer <GameStateData>::Iterator it = gameStates.getIterator();
	// first accumulate a list of game states to call onDestroy() for
	// the reason we don't call it here is that onDestroy() might add to gameStates if a new game state is created
	// this could invalidate the iterator
	while (it.next()) {
		GameStateData & gsd = it.get();
		if (gsd.initialized)
			indicesToDestroy.push_back( it.getIndex() );
	}
	// call onDestroy() on initialized game states
	for (size_t i = 0; i < indicesToDestroy.size(); ++i) {
		GameStateData & gsd = gameStates.get( indicesToDestroy[i] );
		gsd.gameState->removeActors();
		gsd.gameState->onDestroy();
		if (gsd.updatePosition != updateList.end())
			updateList.erase( gsd.updatePosition );
		if (gsd.renderPosition != renderList.end())
			renderList.erase( gsd.renderPosition );
		delete gsd.gameState;
		gameStates.remove( indicesToDestroy[i] );
	}
	// make a pass through the new list and delete those game states, but do not call onDestroy()
	for (size_t i = 0; i < preFrameCommands.size(); ++i) {
		if (preFrameCommands[i].commandType != PreFrameCommand::CT_CREATE)
			continue;
		GameStateData & gsd = gameStates.get( preFrameCommands[i].gameState );
		if (gsd.updatePosition != updateList.end())
			updateList.erase( gsd.updatePosition );
		if (gsd.renderPosition != renderList.end())
			renderList.erase( gsd.renderPosition );
		delete gsd.gameState;
		gameStates.remove( preFrameCommands[i].gameState );
	}
	preFrameCommands.clear();
	// now all game states have been deleted
}

GameState * GameStateManager::getGameState( size_t index ) {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to access game state with invalid index";
		return NULL;
	} else
		return gameStates.get( index ).gameState;
}

void GameStateManager::destroyGameState( size_t index ) {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to destroy game state with invalid index";
		return;
	}

	GameStateData & gsd = gameStates.get( index );
	if (gsd.shouldBeDestroyed)
		return;
	gsd.shouldBeDestroyed = true;
	PreFrameCommand cmd;
	cmd.commandType = PreFrameCommand::CT_DESTROY;
	cmd.gameState = index;
	preFrameCommands.push_back( cmd );
}

void GameStateManager::activateGameState( size_t index ) {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to activate game state with invalid index";
		return;
	}

	GameStateData & gsd = gameStates.get( index );
	if (gsd.shouldBeDestroyed || gsd.shouldBeActive)
		return;
	gsd.shouldBeActive = true;
	if (gsd.initialized) {
		// only add a command if we're already initialized
		// otherwise, the active status is simply set upon creation, but the event isn't called
		PreFrameCommand cmd;
		cmd.commandType = PreFrameCommand::CT_ACTIVATE;
		cmd.gameState = index;
		preFrameCommands.push_back( cmd );
	}
}

void GameStateManager::deactivateGameState( size_t index ) {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to deactivate game state with invalid index";
		return;
	}

	GameStateData & gsd = gameStates.get( index );
	if (gsd.shouldBeDestroyed || !gsd.shouldBeActive)
		return;
	gsd.shouldBeActive = false;
	if (gsd.initialized) {
		// only add a command if we're already initialized
		// otherwise, the active status is simply set upon creation, but the event isn't called
		PreFrameCommand cmd;
		cmd.commandType = PreFrameCommand::CT_DEACTIVATE;
		cmd.gameState = index;
		preFrameCommands.push_back( cmd );
	}
}

void GameStateManager::hideGameState( size_t index ) {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to deactivate game state with invalid index";
		return;
	}

	GameStateData & gsd = gameStates.get( index );
	gsd.shouldBeVisible = false;
}

void GameStateManager::showGameState( size_t index ) {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to show game state with invalid index";
		return;
	}

	GameStateData & gsd = gameStates.get( index );
	gsd.shouldBeVisible = true;
}

bool GameStateManager::isGameStateActive( size_t index ) const {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to query activity of game state with invalid index";
		return false;
	}

	return gameStates.get( index ).updatePosition != updateList.end();
}

bool GameStateManager::isGameStateVisible( size_t index ) const {
	if (!gameStates.contains( index )) {
		Log::warning() << "Attempted to query visibility of game state with invalid index";
		return false;
	}

	return gameStates.get( index ).renderPosition != renderList.end();
}

void GameStateManager::executePreFrameCommands() {
	// it is fine to continue to append commands as they are being executed
	// e.g. creating one game state spawns several more
	for (size_t i = 0; i < preFrameCommands.size(); ++i) {
		size_t gameStateIndex = preFrameCommands[i].gameState;
		GameStateData & gsd = gameStates.get( gameStateIndex );
		switch (preFrameCommands[i].commandType) {
		case PreFrameCommand::CT_CREATE:
			// if active and visible, add to lists
			if (gsd.shouldBeActive) {
				int updatePriority = gsd.gameState->getUpdatePriority();
				gsd.updatePosition = updateList.insert( std::make_pair( updatePriority, gameStateIndex ) );
			}
			if (gsd.shouldBeVisible) {
				int renderPriority = gsd.gameState->getRenderPriority();
				gsd.renderPosition = renderList.insert( std::make_pair( renderPriority, gameStateIndex ) );
			}
			gsd.initialized = true;
			gsd.gameState->onCreate();
			break;
		case PreFrameCommand::CT_DESTROY:
			gsd.gameState->removeActors();
			gsd.gameState->onDestroy();
			if (gsd.updatePosition != updateList.end())
				updateList.erase( gsd.updatePosition );
			if (gsd.renderPosition != renderList.end())
				renderList.erase( gsd.renderPosition );
			delete gsd.gameState;
			gameStates.remove( gameStateIndex );
			break;
		case PreFrameCommand::CT_ACTIVATE:
			// activate
			gsd.updatePosition = updateList.insert( std::make_pair( gsd.gameState->getUpdatePriority(), gameStateIndex ) );
			gsd.gameState->onActivate();
			break;
		case PreFrameCommand::CT_DEACTIVATE:
			// deactivate
			updateList.erase( gsd.updatePosition );
			gsd.updatePosition = updateList.end();
			gsd.gameState->onDeactivate();
			break;
		}
	}

	preFrameCommands.clear();
}