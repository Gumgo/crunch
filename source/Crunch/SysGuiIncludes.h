#ifndef SYSGUIINCLUDES_DEFINED
#define SYSGUIINCLUDES_DEFINED

#include "SysGuiButton.h"
#include "SysGuiCheckbox.h"
#include "SysGuiLabel.h"
#include "SysGuiListbox.h"
#include "SysGuiModal.h"
#include "SysGuiTextbox.h"

#endif