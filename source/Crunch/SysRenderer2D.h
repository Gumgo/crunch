#ifndef SYSRENDERER2D_DEFINED
#define SYSRENDERER2D_DEFINED

#include "Common.h"
#include "Actor.h"
#include "Renderer.h"
#include "ProgramResource.h"
#include "TextureResource.h"
#include "FontResource.h"
#include "GpuState.h"
#include "SysText.h"
#include "TextUtil.h"

DEFINE_ACTOR( SysRenderer2D, 0, 0 )
public:
	// public so that others can use the vertex buffer for quad drawing
	struct QuadVertex {
		float corner[2];
		float padding[2];
	};

	static SysRenderer2D & get();
	void onCreate();
	void onDestroy();

	static const byte PASS = 200;	// the 2D pass
	void onDraw();					// registers begin2DPass()

	// either directly set a blit texture, or set a function to retrieve the blit texture
	// the second case is useful when using ping-pong buffers for postprocessing,
	// and the final texture isn't necessarily always the same
	void setBlitTexture( GpuTextureReference & t );
	void setBlitTexture( fastdelegate::FastDelegate0 <GpuTextureReference> f );

	GpuBufferReference getQuadVertexBuffer();	// return vertex buffer for drawing quads
	GpuBufferReference getQuadIndexBuffer();	// return index buffer for drawing quads

	// requires that the vertex buffer is bound (but not the index buffer)
	void drawSingleQuad();

	// draws a sprite - GpuTexture
	void drawSprite(
		GpuTextureReference & texture,
		const Matrix44f & mvpMatrix,
		const Vector4f & color,
		const Vector2f & origin,
		const Vector2f & pixelMin,
		const Vector2f & pixelMax );

	// draws a sprite - TextureResource - this is probably more commonly used
	void drawSprite(
		TextureResource & texture,
		const Matrix44f & mvpMatrix,
		const Vector4f & color,
		const Vector2f & origin,
		const Vector2f & pixelMin,
		const Vector2f & pixelMax );

	// shortened versions for convenience
	void drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix );
	void drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector4f & color );
	void drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector2f & origin );
	void drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector4f & color, const Vector2f & origin );
	void drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector2f & pixelMin, const Vector2f & pixelMax );
	void drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector4f & color, const Vector2f & pixelMin, const Vector2f & pixelMax );
	void drawSprite( TextureResource & texture, const Matrix44f & mvpMatrix, const Vector2f & origin, const Vector2f & pixelMin, const Vector2f & pixelMax );

	// draws a multiSprite - an array of the same sprite with different parameters
	// vpMatrix is viewProjectionMatrix, modelMatrices are per-sprite transforms
	void drawMultiSprite(
		TextureResource & texture,
		const Matrix44f & vpMatrix,
		const Vector4f & color,
		size_t count,
		const Vector2f * origins,
		const Vector2f * pixelMins,
		const Vector2f * pixelMaxes,
		const Matrix33af * modelMatrices );

	void drawRect(
		const Matrix44f & mvpMatrix,
		const Vector2f & position,
		const Vector2f & size,
		const Vector4f & color );

	void drawGradient(
		const Matrix44f & mvpMatrix,
		const Vector2f & position,
		const Vector2f & size,
		const Vector4f & topLeftColor,
		const Vector4f & topRightColor,
		const Vector4f & bottomLeftColor,
		const Vector4f & bottomRightColor );

	void drawGradientHoriz(
		const Matrix44f & mvpMatrix,
		const Vector2f & position,
		const Vector2f & size,
		const Vector4f & leftColor,
		const Vector4f & rightColor );

	void drawGradientVert(
		const Matrix44f & mvpMatrix,
		const Vector2f & position,
		const Vector2f & size,
		const Vector4f & topColor,
		const Vector4f & bottomColor );

	// TODO: this version currently DOES NOTHING!!!!
	// it is only left in to keep the old GUI code from crashing
	// eventually update the GUI code to use the newer version of this function
	void drawText(
		const std::string & text,
		FontResource & font,
		const Matrix44f & mvpMatrix,
		TextAlignHoriz horiz,
		TextAlignVert vert,
		const Vector4f & color = Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );

	void drawText( SysText & text, const Matrix44f & mvpMatrix, const Vector4f & color = Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );

	// todo: this should really be a separate object (SysWarp), not a "core" feature
	void drawWarp(
		GpuTextureReference & secondaryRenderTarget,
		const Matrix33af & viewMatrix,
		const Matrix44f & projectionMatrix,
		const Vector2f & center,
		float radius,
		float power );

private:
	static SysRenderer2D * sysRenderer2D;

	static const size_t MAX_MULTISPRITES = 20;	// max multisprites drawn in one call
	static const size_t MAX_CHARACTERS = 20;	// max text characters drawn in one call - TODO change to use vertex buffer
	ProgramResourceReference
		blitProgram,
		spriteProgram,
		multiSpriteProgram,
		rectProgram,
		gradientProgram,
		textProgram,
		tilesProgram,
		warpProgram;
#ifdef _DEBUG
	ProgramResourceReference
		linesProgram,
		capsuleProgram;
#endif

	GpuRasterizerState rasterizerState;
	GpuDepthStencilState depthStencilState;

	GpuBufferReference vertexBuffer;
	GpuBufferReference indexBuffer;

	void begin2DPass(); // called at the beginning of 2D pass
	Renderer::Call begin2DPassCall;
	Renderer::Key begin2DPassKey;

	GpuTextureReference blitTexture;
	fastdelegate::FastDelegate0 <GpuTextureReference> getBlitTexture;
	void blit(); // called at VERY end
	Renderer::Call blitCall;
	Renderer::Key blitKey;
};

#endif