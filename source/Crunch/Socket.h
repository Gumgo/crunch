// Rewriting this system; for now, don't use
#if 0

/** @file Socket.h
 *  @brief Provides fixes for compatibility differences with BSD sockets and WinSock.
 */

#ifndef SOCKET_DEFINED
#define SOCKET_DEFINED

#ifdef _WIN32
#include <WinSock2.h>
#include <WS2tcpip.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#endif

/** @brief Initializes sockets; always returns 0 when not on Windows.
 */
int initializeSockets();

/** @brief Terminates sockets; always returns 0 when not on Windows.
 */
int terminateSockets();

#ifdef _WIN32
/** @brief The type for a socket handle.
 */
typedef SOCKET SocketHandle;

/** @brief An invalid socket handle.
 */
static const SocketHandle INVALID_SOCKET_HANDLE = INVALID_SOCKET;
#else
typedef int SocketHandle;
static const SocketHandle INVALID_SOCKET_HANDLE = -1;
#endif

/** @brief Returns the last error code.
 */
int socketCheckError();

#ifdef _WIN32
/** @brief Converts an error code the form for the appropriate platform.
 */
#define socketErrorCode( code ) WSA ## code
#else
#define socketErrorCode( code ) code
#endif

#ifndef _WIN32
/** @brief Defines closesocket if using BSD sockets.
 */
#define closesocket close
#endif

// TODO : ioctl

#endif

#endif