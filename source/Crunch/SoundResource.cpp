#include "SoundResource.h"
#include "Context.h"
#include "Log.h"

SoundResource::SoundResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m ) {
}

bool SoundResource::loadDataAsync() {
	// TODO: somehow get async loading working?
	// perhaps do this by loading the sound from disk in background, then calling the regular sound load function
	// except load from memory
	return true;
}

bool SoundResource::loadDataSync() {
	try {
		sound = NULL;
		FMOD_MODE mode = FMOD_DEFAULT;
		if ((getMetadata() & LOOP) != 0)
			mode |= FMOD_LOOP_NORMAL;
		if ((getMetadata() & IS_3D) != 0)
			mode |= FMOD_3D;
		FMOD_RESULT result = Context::get().audioManager.getSystem()->createSound( getPath().c_str(), mode, 0, &sound );
		if (result != FMOD_OK)
			throw std::runtime_error( "Failed to create sound" );
	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}

	return true;
}

void SoundResource::freeData() {
	if (sound->release() != FMOD_OK)
		Log::warning() << "Failed to free sound";
}

bool SoundResource::readMetadata( uint & metadata, KeyValueReader::Iterator it ) {
	metadata = 0;
	if (it.contains( "loop" ))
		metadata = (it.getBool( "loop" )) ? (metadata | LOOP) : (metadata & ~LOOP);
	if (it.contains( "is3d" ))
		metadata = (it.getBool( "is3d" )) ? (metadata | IS_3D) : (metadata & ~IS_3D);

	return true;
}

uint SoundResource::getDefaultMetadata() {
	return 0;
}

FMOD::Sound * SoundResource::getSound() {
	return sound;
}

const FMOD::Sound * SoundResource::getSound() const {
	return sound;
}
