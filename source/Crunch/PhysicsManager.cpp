#if 0

#include "PhysicsManager.h"
#ifdef _DEBUG
#include "Context.h"
#include "SystemState.h"
#endif

const float PhysicsManager::GRID_SIZE = 256.0f;

DynamicActorParameters::DynamicActorParameters() {
	gravity			= 0.125f;
	maxGravitySpeed	= 6.0f;
	maxSlopeAngle	= 50.0f;
	walkSpeed		= 2.5f;
	walkAccel		= 0.75f;
	airAccel		= (0.75f / 16.0f);
	jumpSpeed		= 8.0f;
	hitsColliders	= true;
	hitsActors		= true;
	type			= std::numeric_limits <size_t>::max();
	data			= NULL;
}

RaycastParameters::RaycastParameters() {
	maxDistance				= std::numeric_limits <float>::max();
	stopsAtColliders		= true;
	stopsAtDynamicActors	= true;
	type					= std::numeric_limits <size_t>::max();
	data					= NULL;
}

PhysicsManager::StaticCollider::StaticCollider( const Vector2f * pts, size_t segId )
	: lineSegment( LineSegment( pts[0], pts[1] ) )
	, id( segId ) {
	// make sure the line is normalized
	lineSegment.line.normalize();
}

PhysicsManager::PhysicsManager()
	: collisionCallbacks( COLLISION_CALLBACKS_CAPACITY ) {
	updateIteratorOuter = dynamicActorUpdateList.end();
	updateIteratorInner = potentialActors.end();
}

size_t PhysicsManager::gridCellId( int x, int y ) const {
	return (y - gridMin.y)*(gridMax.x - gridMin.x) + (x - gridMin.x);
}

void PhysicsManager::initRoom( const RoomData * room ) {
	staticColliders.clear();
	for (size_t i = 0; i < room->lineSegments.size(); ++i)
		staticColliders.push_back( StaticCollider( room->lineSegments[i].points, i ) );
	gridCells.clear();

	Vector2f boundMin;
	Vector2f boundMax(
		(float)(room->screenDimensions[0] * room->screenSize.x),
		(float)(room->screenDimensions[1] * room->screenSize.y) );

	for (size_t i = 0; i < 2; ++i) {
		gridMin[i] = (int)floor( boundMin[i] / GRID_SIZE );
		gridMax[i] = (int)ceil( boundMax[i] / GRID_SIZE );
	}

	size_t cellCount = (gridMax.x - gridMin.x) * (gridMax.y - gridMin.y);
	gridCells.resize( cellCount );

	// insert each line into proper grid cells
	for (size_t i = 0; i < staticColliders.size(); ++i) {
		StaticCollider & ls = staticColliders[i];
		Vector2f lineMin = vecMin( ls.lineSegment.points[0], ls.lineSegment.points[1] );
		Vector2f lineMax = vecMax( ls.lineSegment.points[0], ls.lineSegment.points[1] );
		lineMin.x -= epsilon <float>();
		lineMin.y -= epsilon <float>();
		lineMax.x += epsilon <float>();
		lineMax.y += epsilon <float>();

		Vector2i lineGridMin;
		Vector2i lineGridMax;
		for (size_t t = 0; t < 2; ++t) {
			// this will work because line is guaranteed not to be "infinitely thin"
			// if it wasn't, we'd have to have a case for that
			lineGridMin[t] = (int)floor( lineMin[t] / GRID_SIZE );
			lineGridMax[t] = (int)ceil( lineMax[t] / GRID_SIZE );
			lineGridMin[t] = std::max( 0, std::min( (int)gridMax[t], lineGridMin[t] ) );
			lineGridMax[t] = std::max( 0, std::min( (int)gridMax[t], lineGridMax[t] ) );
		}

		for (int y = lineGridMin.y; y < lineGridMax.y; ++y) {
			for (int x = lineGridMin.x; x < lineGridMax.x; ++x) {
				Box b;
				b.bounds[0].set( (float)x * GRID_SIZE, (float)y * GRID_SIZE );
				b.bounds[1] = b.bounds[0] + Vector2f( GRID_SIZE, GRID_SIZE );
				b.bounds[0] -= Vector2f( epsilon <float>(), epsilon <float>() );
				b.bounds[1] += Vector2f( epsilon <float>(), epsilon <float>() );
				if (intersect( ls.lineSegment, b ))
					gridCells[gridCellId( x, y )].staticColliderIds.push_back( i );
			}
		}
	}

	potentialColliders.reserve( staticColliders.size() / 2 );
	colliderAdded.resize( staticColliders.size(), false );

#ifdef _DEBUG
	linesProgram = Context::get().resources.programManager.get( "sys___lines" )->getData();
	capsuleProgram = Context::get().resources.programManager.get( "sys___capsule" )->getData();

	CapsuleVertex capsuleVertices[] = {
		{ { 0.0f, 0.0f } },
		{ { 1.0f, 0.0f } },
		{ { 1.0f, 1.0f } },
		{ { 0.0f, 1.0f } }
	};
	ushort capsuleIndices[] = {
		0, 1, 3, 1, 3, 2
	};
	Vector4f lineColors[] = {
		Vector4f( 1.0f, 0.0f, 0.0f, 1.0f ),
		Vector4f( 1.0f, 1.0f, 0.0f, 1.0f ),
		Vector4f( 0.0f, 1.0f, 0.0f, 1.0f ),
		Vector4f( 0.0f, 1.0f, 1.0f, 1.0f ),
		Vector4f( 0.0f, 0.0f, 1.0f, 1.0f ),
		Vector4f( 1.0f, 0.0f, 1.0f, 1.0f )
	};
	std::vector <LineVertex> lineVertices( staticColliders.size() * 4 );
	for (size_t i = 0; i < staticColliders.size(); ++i) {
		size_t idx = i*4;
		memcpy( lineVertices[idx  ].pos, &staticColliders[i].lineSegment.points[0], sizeof( lineVertices[idx  ].pos ) );
		memcpy( lineVertices[idx+1].pos, &staticColliders[i].lineSegment.points[1], sizeof( lineVertices[idx+1].pos ) );
		Vector2f centerPoint = (staticColliders[i].lineSegment.points[0] + staticColliders[i].lineSegment.points[1]) * 0.5f;
		Vector2f normalPoint = centerPoint + staticColliders[i].lineSegment.line.normal * 12.0f;
		memcpy( lineVertices[idx+2].pos, &centerPoint, sizeof( lineVertices[idx+2].pos ) );
		memcpy( lineVertices[idx+3].pos, &normalPoint, sizeof( lineVertices[idx+3].pos ) );
		for (size_t t = 0; t < 4; ++t)
			memcpy( lineVertices[idx+t].col, &lineColors[i % arraySize( lineColors )], sizeof( lineVertices[idx+t].col ) );
	}
	vertexBuffer.reset( new GpuBuffer( GL_ARRAY_BUFFER, sizeof( capsuleVertices ) + sizeof( lineVertices[0] )*lineVertices.size(), GL_STATIC_DRAW, NULL ) );
	vertexBuffer.get()->write( 0, sizeof( capsuleVertices ), capsuleVertices );
	lineVertexBufferOffset = sizeof( capsuleVertices );
	if (!lineVertices.empty())
		vertexBuffer.get()->write( lineVertexBufferOffset, sizeof( lineVertices[0] )*lineVertices.size(), &lineVertices[0] );
	indexBuffer.reset( new GpuBuffer( GL_ELEMENT_ARRAY_BUFFER, sizeof( capsuleIndices ), GL_STATIC_DRAW, capsuleIndices ) );
#endif
}

void PhysicsManager::getPotentialColliders( const Box & bounds ) {
	// note: bounds MUST have some area - it can't just be a line
	// therefore, make sure to add an epsilon of padding
	Vector2i minGridCell(
		(int)floor( bounds.bounds[0].x / GRID_SIZE ),
		(int)floor( bounds.bounds[0].y / GRID_SIZE ) );
	Vector2i maxGridCell(
		(int)ceil( bounds.bounds[1].x / GRID_SIZE ),
		(int)ceil( bounds.bounds[1].y / GRID_SIZE ) );

	// loop through each grid cell in range and find all the potential colliders for this actor
	for (int y = std::max( gridMin.y, minGridCell.y ); y < std::min( gridMax.y, maxGridCell.y ); ++y) {
		for (int x = std::max( gridMin.x, minGridCell.x ); x < std::min( gridMax.x, maxGridCell.x ); ++x) {
			size_t cellId = gridCellId( x, y );
			const GridCell & c = gridCells[cellId];
			for (size_t i = 0; i < c.staticColliderIds.size(); ++i) {
				if (!colliderAdded[c.staticColliderIds[i]]) {
					potentialColliders.push_back( c.staticColliderIds[i] );
					colliderAdded[c.staticColliderIds[i]] = true;
				}
			}
		}
	}
}

void PhysicsManager::getPotentialColliders( const LineSegment & ls ) {
	Box bounds;
	bounds.bounds[0] = vecMin( ls.points[0], ls.points[1] );
	bounds.bounds[1] = vecMax( ls.points[0], ls.points[1] );
	bounds.bounds[0].x -= epsilon <float>();
	bounds.bounds[0].y -= epsilon <float>();
	bounds.bounds[1].x += epsilon <float>();
	bounds.bounds[1].y += epsilon <float>();

	// note: bounds MUST have some area - it can't just be a line
	// therefore, make sure to add an epsilon of padding
	Vector2i minGridCell(
		(int)floor( bounds.bounds[0].x / GRID_SIZE ),
		(int)floor( bounds.bounds[0].y / GRID_SIZE ) );
	Vector2i maxGridCell(
		(int)ceil( bounds.bounds[1].x / GRID_SIZE ),
		(int)ceil( bounds.bounds[1].y / GRID_SIZE ) );

	// loop through each grid cell in range and find all the potential colliders for this actor
	for (int y = std::max( gridMin.y, minGridCell.y ); y < std::min( gridMax.y, maxGridCell.y ); ++y) {
		for (int x = std::max( gridMin.x, minGridCell.x ); x < std::min( gridMax.x, maxGridCell.x ); ++x) {
			size_t cellId = gridCellId( x, y );
			const GridCell & c = gridCells[cellId];
			Box cellBounds;
			Vector2f xy( (float)x, (float)y );
			cellBounds.bounds[0] = xy * GRID_SIZE;
			cellBounds.bounds[1] = (xy + Vector2f( 1.0f, 1.0f )) * GRID_SIZE;
			cellBounds.bounds[0].x -= epsilon <float>();
			cellBounds.bounds[0].y -= epsilon <float>();
			cellBounds.bounds[1].x += epsilon <float>();
			cellBounds.bounds[1].y += epsilon <float>();
			if (intersect( ls, cellBounds )) {
				for (size_t i = 0; i < c.staticColliderIds.size(); ++i) {
					if (!colliderAdded[c.staticColliderIds[i]]) {
						potentialColliders.push_back( c.staticColliderIds[i] );
						colliderAdded[c.staticColliderIds[i]] = true;
					}
				}
			}
		}
	}
}

void PhysicsManager::clearPotentialColliders() {
	for (size_t i = 0; i < potentialColliders.size(); ++i)
		colliderAdded[potentialColliders[i]] = false;
	potentialColliders.clear();
}

void PhysicsManager::getPotentialActors( const Box & bounds ) {
	// note: bounds MUST have some area - it can't just be a line
	// therefore, make sure to add an epsilon of padding
	Vector2i minGridCell(
		(int)floor( bounds.bounds[0].x / GRID_SIZE ),
		(int)floor( bounds.bounds[0].y / GRID_SIZE ) );
	Vector2i maxGridCell(
		(int)ceil( bounds.bounds[1].x / GRID_SIZE ),
		(int)ceil( bounds.bounds[1].y / GRID_SIZE ) );

	// loop through each grid cell in range and find all the potential colliders for this actor
	for (int y = std::max( gridMin.y, minGridCell.y ); y < std::min( gridMax.y, maxGridCell.y ); ++y) {
		for (int x = std::max( gridMin.x, minGridCell.x ); x < std::min( gridMax.x, maxGridCell.x ); ++x) {
			size_t cellId = gridCellId( x, y );
			const GridCell & c = gridCells[cellId];
			for (size_t i = 0; i < c.dynamicActorIds.size(); ++i) {
				if (dynamicActors.get( c.dynamicActorIds[i] ).p.hitsActors)
					potentialActors.insert( c.dynamicActorIds[i] );
			}
		}
	}
}

void PhysicsManager::getPotentialActors( const LineSegment & ls ) {
	Box bounds;
	bounds.bounds[0] = vecMin( ls.points[0], ls.points[1] );
	bounds.bounds[1] = vecMax( ls.points[0], ls.points[1] );
	bounds.bounds[0].x -= epsilon <float>();
	bounds.bounds[0].y -= epsilon <float>();
	bounds.bounds[1].x += epsilon <float>();
	bounds.bounds[1].y += epsilon <float>();

	// note: bounds MUST have some area - it can't just be a line
	// therefore, make sure to add an epsilon of padding
	Vector2i minGridCell(
		(int)floor( bounds.bounds[0].x / GRID_SIZE ),
		(int)floor( bounds.bounds[0].y / GRID_SIZE ) );
	Vector2i maxGridCell(
		(int)ceil( bounds.bounds[1].x / GRID_SIZE ),
		(int)ceil( bounds.bounds[1].y / GRID_SIZE ) );

	// loop through each grid cell in range and find all the potential colliders for this actor
	for (int y = std::max( gridMin.y, minGridCell.y ); y < std::min( gridMax.y, maxGridCell.y ); ++y) {
		for (int x = std::max( gridMin.x, minGridCell.x ); x < std::min( gridMax.x, maxGridCell.x ); ++x) {
			size_t cellId = gridCellId( x, y );
			const GridCell & c = gridCells[cellId];
			Box cellBounds;
			Vector2f xy( (float)x, (float)y );
			cellBounds.bounds[0] = xy * GRID_SIZE;
			cellBounds.bounds[1] = (xy + Vector2f( 1.0f, 1.0f )) * GRID_SIZE;
			cellBounds.bounds[0].x -= epsilon <float>();
			cellBounds.bounds[0].y -= epsilon <float>();
			cellBounds.bounds[1].x += epsilon <float>();
			cellBounds.bounds[1].y += epsilon <float>();
			if (intersect( ls, cellBounds )) {
				for (size_t i = 0; i < c.dynamicActorIds.size(); ++i) {
					if (dynamicActors.get( c.dynamicActorIds[i] ).p.hitsActors)
						potentialActors.insert( c.dynamicActorIds[i] );
				}
			}
		}
	}
}

void PhysicsManager::removeFromCells( size_t id ) {
	DynamicActor & a = dynamicActors.get( id );
	Box bounds = getBounds( a.capsule, epsilon <float>() );

	Vector2i minGridCell(
		(int)floor( bounds.bounds[0].x / GRID_SIZE ),
		(int)floor( bounds.bounds[0].y / GRID_SIZE ) );
	Vector2i maxGridCell(
		(int)ceil( bounds.bounds[1].x / GRID_SIZE ),
		(int)ceil( bounds.bounds[1].y / GRID_SIZE ) );

	// loop through each grid cell in range and find all the potential colliders for this actor
	for (int y = std::max( gridMin.y, minGridCell.y ); y < std::min( gridMax.y, maxGridCell.y ); ++y) {
		for (int x = std::max( gridMin.x, minGridCell.x ); x < std::min( gridMax.x, maxGridCell.x ); ++x) {
			size_t cellId = gridCellId( x, y );
			GridCell & c = gridCells[cellId];
			for (size_t i = 0; i < c.dynamicActorIds.size(); ++i) {
				if (c.dynamicActorIds[i] == id) {
					c.dynamicActorIds[i] = c.dynamicActorIds.back();
					c.dynamicActorIds.pop_back();
					break;
				}
			}
		}
	}
}

void PhysicsManager::addToCells( size_t id ) {
	DynamicActor & a = dynamicActors.get( id );
	Box bounds = getBounds( a.capsule, epsilon <float>() );

	Vector2i minGridCell(
		(int)floor( bounds.bounds[0].x / GRID_SIZE ),
		(int)floor( bounds.bounds[0].y / GRID_SIZE ) );
	Vector2i maxGridCell(
		(int)ceil( bounds.bounds[1].x / GRID_SIZE ),
		(int)ceil( bounds.bounds[1].y / GRID_SIZE ) );

	// loop through each grid cell in range and find all the potential colliders for this actor
	for (int y = std::max( gridMin.y, minGridCell.y ); y < std::min( gridMax.y, maxGridCell.y ); ++y) {
		for (int x = std::max( gridMin.x, minGridCell.x ); x < std::min( gridMax.x, maxGridCell.x ); ++x) {
			size_t cellId = gridCellId( x, y );
			GridCell & c = gridCells[cellId];
			c.dynamicActorIds.push_back( id );
		}
	}
}

bool PhysicsManager::isOnGround( DynamicActor & a, Vector2f & groundNormal, size_t & groundId ) {
	bool onGround = false;

	// get bounds with a bit of spacing
	Box bounds = getBounds( a.capsule, 4.0f * epsilon <float>() );
	getPotentialColliders( bounds );

	float minAllowedDist2 = epsilon <float>()*3.0f;
	minAllowedDist2 *= minAllowedDist2;

	// loop through each potential collider and see which one is closest
	float minDistance2 = std::numeric_limits <float>::max();
	for (size_t i = 0; i < potentialColliders.size(); ++i) {
		// get the line segment
		LineSegment & ls = staticColliders[potentialColliders[i]].lineSegment;
		// check if our bounds intersect it - if not, early out
		if (!intersect( ls, bounds ))
			continue;
		// find the distance and normal vector of intersection at closest point
		Vector2f normal;
		float dist2 = distance2( a.capsule, ls, normal );
		// make sure the distance is within range
		if (dist2 < minAllowedDist2 && dist2 < minDistance2) {
			normal.normalize();
			// make sure the slope is valid (walkable)
			if ((a.p.gravity < 0.0f && normal.y > a.maxSlopeNormalY) ||
				(a.p.gravity > 0.0f && -normal.y > a.maxSlopeNormalY)) {
				minDistance2 = dist2;
				groundNormal = normal;
				groundId = potentialColliders[i];
				onGround = true;
			}
		}
	}

	clearPotentialColliders();
	return onGround;
}

void PhysicsManager::applyForces( DynamicActor & a, bool onGround, const Vector2f & groundNormal, size_t groundId ) {
	if (onGround) {
		// if on the ground, apply walking
		Vector2f rightVec = groundNormal.orthogonal();
		if (rightVec.x < 0.0f)
			rightVec = -rightVec;

		float upVelocity = groundNormal.dot( a.velocity );
		float rightVelocity = rightVec.dot( a.velocity );
		if (a.walking == 1) {
			if (rightVelocity < a.p.walkSpeed)
				rightVelocity = std::min( a.p.walkSpeed, rightVelocity + a.p.walkAccel );
			else if (rightVelocity > a.p.walkSpeed)
				rightVelocity = std::max( a.p.walkSpeed, rightVelocity - a.p.walkAccel );
		} else if (a.walking == -1) {
			if (rightVelocity > -a.p.walkSpeed)
				rightVelocity = std::max( -a.p.walkSpeed, rightVelocity - a.p.walkAccel );
			else if (rightVelocity < -a.p.walkSpeed)
				rightVelocity = std::min( -a.p.walkSpeed, rightVelocity + a.p.walkAccel );
		} else {
			if (rightVelocity > 0.0f)
				rightVelocity = std::max( 0.0f, rightVelocity - a.p.walkAccel );
			else if (rightVelocity < 0.0f)
				rightVelocity = std::min( 0.0f, rightVelocity + a.p.walkAccel );
		}

		a.velocity = upVelocity*groundNormal + rightVelocity*rightVec;
		if (a.jump)
			a.velocity.y -= a.p.jumpSpeed;
	} else {
		// apply "air" walking
		float xVelocity = a.velocity.x;

		if (a.walking == 1) {
			if (xVelocity < a.p.walkSpeed)
				xVelocity = std::min( a.p.walkSpeed, xVelocity + a.p.airAccel );
		} else if (a.walking == -1) {
			if (xVelocity > -a.p.walkSpeed)
				xVelocity = std::max( -a.p.walkSpeed, xVelocity - a.p.airAccel );
		}
		a.velocity.x = xVelocity;

		// add gravity to the actor
		if (a.p.gravity > 0.0f) {
			if (a.velocity.y < a.p.maxGravitySpeed)
				a.velocity.y = std::min( a.p.maxGravitySpeed, a.velocity.y + a.p.gravity );
		} else if (a.p.gravity < 0.0f) {
			if (a.velocity.y > -a.p.maxGravitySpeed)
				a.velocity.y = std::max( -a.p.maxGravitySpeed, a.velocity.y + a.p.gravity );
		}
	}

	a.jump = false;
}

void PhysicsManager::iteratePhysics( DynamicActor & a, float & velocityRemaining ) {
	// make sure the actor isn't embedded in any objects
	moveOut( a );

	// we will use only the remaining portion of velocity
	Vector2f velocity = a.velocity * velocityRemaining;
	// get the magnitude of the velocity
	float velocityMagnitude = velocity.magnitude();
	float iVelocityMagnitude = 1.0f / velocityMagnitude;
	Vector2f velocityNormalized = velocity * iVelocityMagnitude;
	// when we do the sweep test, we actually use a slightly larger velocity
	// this is so we detect collisions that are at the very end of the vector,
	// so we don't accidentally move right up next to any objects
	Vector2f largerVelocity = velocity + velocityNormalized * epsilon <float>();
	float largerVelocityMagnitude = velocityMagnitude + epsilon <float>();

	// collide with the scene using velocity + epsilon
	float minSweepTime = 1.0f;
	Vector2f minIsectNormal;
	Vector2f minIsectPoint;
	size_t minCollisionId;
	bool collision = collide( a, largerVelocity, minSweepTime, minIsectNormal, minIsectPoint, minCollisionId );

	if (!collision) {
		// if there was no collision, we just move forward by the original velocity (remaining)
		velocityRemaining = 0.0f;
		a.capsule.position += velocity;
	} else {
		// the sweep time is currently in terms of largerVelocity but we want it in terms of velocity
		// we convert it here
		float actualMinSweepTime = minSweepTime * (largerVelocityMagnitude * iVelocityMagnitude);

		// respond to the collision
		respond( a, velocity, actualMinSweepTime, minIsectNormal, minIsectPoint, velocityRemaining );
	}
}

void PhysicsManager::moveOut( DynamicActor & a ) {
	static const size_t MAX_ATTEMPTS = 4;
	for (size_t t = 0; t < MAX_ATTEMPTS; ++t) {
		Box bounds = getBounds( a.capsule, epsilon <float>() );
		getPotentialColliders( bounds );

		// go through the list of potential colliders and trim it down to only ones we do collide with
		for (size_t i = 0; i < potentialColliders.size(); ++i) {
			Vector2f norm;
			if (distance2( a.capsule, staticColliders[potentialColliders[i]].lineSegment, norm ) >= epsilon <float>()*epsilon <float>()) {
				colliderAdded[potentialColliders[i]] = false;
				potentialColliders[i] = potentialColliders.back();
				potentialColliders.pop_back();
				--i;
			}
		}

		if (potentialColliders.empty())
			// we're free!
			break;
		else if (potentialColliders.size() == 1) {
 			Vector2f moveVec;
			::moveOut( a.capsule, staticColliders[potentialColliders[0]].lineSegment, epsilon <float>(), moveVec );
			a.capsule.position += moveVec;
		} else {
			Vector2f moveVec;
			// not a particularly great way to do it but... hopefully this won't come up much
			for (size_t i = 1; i < potentialColliders.size(); i += 2) {
				if (::moveOut( a.capsule,
					staticColliders[potentialColliders[i-1]].lineSegment,
					staticColliders[potentialColliders[i]].lineSegment,
					epsilon <float>(), moveVec ))
					a.capsule.position += moveVec;
			}

			// if one left over
			if ((potentialColliders.size() & 1) != 0) {
				::moveOut( a.capsule, staticColliders.back().lineSegment, epsilon <float>(), moveVec );
				a.capsule.position += moveVec;
			}
		}

		clearPotentialColliders();
	}
}

bool PhysicsManager::collide( DynamicActor & a, const Vector2f & velocity, float & sweepTime, Vector2f & isectNormal, Vector2f & isectPoint, size_t & collisionId ) {
	// find the bounds of our entire potential velocity
	// since we're moving using the velocity plus epsilon, we use that
	// we also extend the bounds by epsilon just so we don't miss corner cases
	// this also avoids the problem of moving entirely vertical or horizontal
	Box boundsStart = getBounds( a.capsule, epsilon <float>() );
	Box boundsEnd = boundsStart;
	boundsEnd.bounds[0] += velocity;
	boundsEnd.bounds[1] += velocity;

	// since our motion is linear, it is contained by the bounds containing our start and end positions
	Box aBounds;
	aBounds.bounds[0] = vecMin( boundsStart.bounds[0], boundsEnd.bounds[0] );
	aBounds.bounds[1] = vecMax( boundsStart.bounds[1], boundsEnd.bounds[1] );

	// create the list of potential colliders with these bounds
	getPotentialColliders( aBounds );

	// loop through each potential collider and see which one we hit first
	// store the minimum sweep time in sweepTime and the ID of what we hit in collisionId
	// store the normal vector of the intersection in isectNormal and the point of intersection in isectPoint
	sweepTime = 1.0f;
	bool collision = false;
	Vector2f isectNormalTemp;
	Vector2f isectPointTemp;
	for (size_t i = 0; i < potentialColliders.size(); ++i) {
		// get the line segment
		LineSegment & ls = staticColliders[potentialColliders[i]].lineSegment;
		// check if our bounds intersect it - if not, early out
		if (!intersect( ls, aBounds ))
			continue;
		// perform the sweep test using velocity plus epsilon
		float sweepTimeTemp = sweep( a.capsule, velocity, ls, isectNormalTemp, isectPointTemp );
		// if this is the smallest time, store this potential collider
		if (sweepTimeTemp < sweepTime) {
			sweepTime = sweepTimeTemp;
			isectNormal = isectNormalTemp;
			isectPoint = isectPointTemp;
			collisionId = potentialColliders[i];
			collision = true;
		}
	}

	clearPotentialColliders();

	return collision;
}

void PhysicsManager::respond( DynamicActor & a, const Vector2f & velocity, float sweepTime, const Vector2f & isectNormal, const Vector2f & isectPoint, float & velocityRemaining ) {
	float velocityMagnitude = velocity.magnitude();
	float iVelocityMagnitude = 1.0f / velocityMagnitude;
	Vector2f velocityNormalized = velocity * iVelocityMagnitude;

	// we move to the point of collision
	float moveMagnitude = velocityMagnitude * sweepTime;
	// however, we actually want to move to a point where we are epsilon units away, so we solve for that:
	// position of intersection on the capsule; we can find this with a little trickery
	Vector2f isectOnCapsule = isectPoint - sweepTime*velocity;
	// n = collision normal, i = collision point, p = position, v = velocity, t = time, d = distance
	// d = n . ((p + vt) - i)
	// d = n.(p + vt) - n.i
	// d = n.p + n.vt - n.i
	// d = n.vt + n.p - n.i
	// d = n.vt + n.(p-i)
	// d - n.(p-i) = n.vt
	// t = (d + n.(i-p)) / n.v
	// we want to solve for when d = epsilon
	float denom = isectNormal.dot( velocity );
	if (denom != 0.0f) {
		float t = (epsilon <float>() + isectNormal.dot( isectPoint - isectOnCapsule )) / denom;
		float maxAllowedMoveMagnitude = velocityMagnitude * t;
		// if our move magnitude would bring us past the point closer than epsilon, don't move there
		if (maxAllowedMoveMagnitude < moveMagnitude)
			moveMagnitude = maxAllowedMoveMagnitude;
	}

	// make sure we're not moving further than the original velocity or too far backward
	moveMagnitude = clamp( moveMagnitude, 0.0f, velocityMagnitude );
	// adjust the remaining velocity based on the percent moveMagnitude is of the original magnitude
	velocityRemaining *= (1.0f - moveMagnitude*iVelocityMagnitude);
	// perform the move by moveMagnitude
	Vector2f move = velocityNormalized * moveMagnitude;
	a.capsule.position += move;
	isectOnCapsule += move;

	// next is the response:
	// we need to figure out the next velocity based on the angle of the collider
	// however, we risk floating point precision madness if we just move in that direction
	// therefore, we add a tiny bit of the collider's normal to the new velocity so we move a bit away from it
	// find the vector to slide along
	Vector2f slideVector = isectNormal.orthogonal();
	// the new velocity is the component of the velocity in the direction of slideVector,
	// since the collider absorbed the perpendicular force
	// find the destination point on the slope
	Vector2f destination = isectPoint + slideVector * slideVector.dot( a.velocity );
	// add a tiny bit of the normal to ensure better floating point behavior the next iteration
	destination += isectNormal * epsilon <float>();
	a.velocity = destination - isectOnCapsule;
}

void PhysicsManager::moveToGround( DynamicActor & a, const Vector2f & prevPosition ) {
	// find how much the actor moved
	Vector2f moveAmount = a.capsule.position - prevPosition;
	// figure out how much down we have to check - the slope amount downwards scaled by the amount forward
	// bias a bit - allow us to always check at least a little bit downward
	float downCastAmount = std::max <float>( 1.0f, abs( moveAmount.x ) )*a.maxSlopeAngleTan - moveAmount.y;
	float downCastAmountEps = downCastAmount + epsilon <float>();
	// if we don't need to test, return early
	if (downCastAmount <= 0.0f)
		return;

	Box bounds = getBounds( a.capsule, epsilon <float>() );
	bounds.bounds[1].y += downCastAmountEps;

	// create the list of potential colliders with these bounds
	getPotentialColliders( bounds );

	// find the first thing we hit (see collide function for details)
	float sweepTime = 1.0f;
	bool collision = false;
	Vector2f isectNormalTemp, isectNormal;
	Vector2f isectPointTemp, isectPoint;
	size_t collisionId;
	for (size_t i = 0; i < potentialColliders.size(); ++i) {
		LineSegment & ls = staticColliders[potentialColliders[i]].lineSegment;
		if (!intersect( ls, bounds ))
			continue;
		float sweepTimeTemp = sweep( a.capsule, Vector2f( 0.0f, downCastAmountEps ), ls, isectNormalTemp, isectPointTemp );
		if (sweepTimeTemp < sweepTime) {
			sweepTime = sweepTimeTemp;
			isectNormal = isectNormalTemp;
			isectPoint = isectPointTemp;
			collisionId = potentialColliders[i];
			collision = true;
		}
	}

	clearPotentialColliders();

	// if no collision, return early - don't move down
	if (!collision)
		return;

	// make sure the slope below isn't too steep
	if ((a.p.gravity < 0.0f && isectNormal.y >= a.maxSlopeNormalY) ||
		(a.p.gravity > 0.0f && -isectNormal.y <= a.maxSlopeNormalY))
		return;

	// convert the sweep time into the "real" amount (not the slightly larger version)
	sweepTime *= (downCastAmountEps / downCastAmount);

	// see respond function for details - basically the same, just moving only downward
	float moveMagnitude = downCastAmount * sweepTime;
	Vector2f isectOnCapsule = isectPoint;
	isectOnCapsule.y -= sweepTime*downCastAmount;
	float denom = isectNormal.y * downCastAmount;
	if (denom != 0.0f) {
		float t = (epsilon <float>() + isectNormal.dot( isectPoint - isectOnCapsule )) / denom;
		float maxAllowedMoveMagnitude = downCastAmount * t;
		if (maxAllowedMoveMagnitude < moveMagnitude)
			moveMagnitude = maxAllowedMoveMagnitude;
	}

	moveMagnitude = clamp( moveMagnitude, 0.0f, downCastAmount );
	a.capsule.position.y += moveMagnitude;
	isectOnCapsule.y += moveMagnitude;

	Vector2f slideVector = isectNormal.orthogonal();
	float velocityMag = a.velocity.magnitude();
	Vector2f destination = isectPoint;
	if (sign( slideVector.x ) == sign( a.velocity.x ))
		destination += slideVector * velocityMag;
	else
		destination -= slideVector * velocityMag;
	destination += isectNormal * epsilon <float>();
	a.velocity = destination - isectOnCapsule;
}

void PhysicsManager::update() {
	// first loop through all active dynamic actors/static colliders and update them
	for (std::multimap <int, size_t>::iterator it = dynamicActorUpdateList.begin(); it != dynamicActorUpdateList.end(); ++it) {
		DynamicActor & a = dynamicActors.get( it->second );

		removeFromCells( it->second );

		// if doesn't collide with dynamic actors, just move by velocity
		if (!a.p.hitsColliders) {
			a.capsule.position += a.velocity;
			addToCells( it->second );
			continue;
		}

		bool tryJump = a.jump;

		// first check if the dynamic actor is on the ground
		Vector2f groundNormal;
		size_t groundId;
		bool onGround = isOnGround( a, groundNormal, groundId );

		applyForces( a, onGround, groundNormal, groundId );

		Vector2f prevPosition = a.capsule.position;

		// we start out with 100% of the original velocity
		// each iteration, we subtract the percentage of the velocity we move until we reach 0
		float velocityRemaining = 1.0f;

		static const size_t MAX_ITERATIONS = 5;
		for (size_t r = 0; r < MAX_ITERATIONS && velocityRemaining > epsilon <float>() && !a.velocity.isZero(); ++r)
			iteratePhysics( a, velocityRemaining );

		if (onGround && !tryJump)
			moveToGround( a, prevPosition );

		addToCells( it->second );
	}

	// loop through dynamic actor/dynamic actor and update
	// we have to be careful here since there can be callbacks that could remove dynamic actors while updating
	// the technique used to fix this is similar to that with actors
	for (updateIteratorOuter = dynamicActorUpdateList.begin(); updateIteratorOuter != dynamicActorUpdateList.end(); ++updateIteratorOuter) {
		DynamicActor & a = dynamicActors.get( updateIteratorOuter->second );
		if (!a.p.hitsActors)
			continue;

		std::multimap <int, size_t>::iterator currentIterator = updateIteratorOuter;

		Box bounds = getBounds( a.capsule, epsilon <float>() );

		// get the potential actors
		getPotentialActors( bounds );

		for (updateIteratorInner = potentialActors.begin(); updateIteratorInner != potentialActors.end(); ++updateIteratorInner) {
			// since we're executing callbacks, it's possible that this object was deleted
			// check to make sure it wasn't - if it was, updateIteratorOuter will have been incremented
			if (currentIterator != updateIteratorOuter)
				break;

			// avoid duplicates and self:
			if (*updateIteratorInner <= updateIteratorOuter->second)
				continue;

			DynamicActor & b = dynamicActors.get( *updateIteratorInner );

			float aHalf = a.capsule.height * 0.5f;
			float bHalf = b.capsule.height * 0.5f;

			LineSegment lsA(
				Vector2f( a.capsule.position.x, a.capsule.position.y - aHalf ),
				Vector2f( a.capsule.position.x, a.capsule.position.y + aHalf ) );
			LineSegment lsB(
				Vector2f( a.capsule.position.x, a.capsule.position.y - bHalf ),
				Vector2f( a.capsule.position.x, a.capsule.position.y + bHalf ) );

			std::pair <size_t, size_t> key( a.p.type, b.p.type );
			std::pair <void*, void*> data( a.p.data, b.p.data );
			if (key.second < key.first) {
				std::swap( key.first, key.second );
				std::swap( data.first, data.second );
			}

			size_t cbIndex;
			if (!collisionCallbacks.getIndex( key, cbIndex ))
				continue;

			float rad = a.capsule.radius + b.capsule.radius;
			Vector2f normHolder;
			// check if the capsules are close enough
			if (distance2( lsA, lsB, normHolder ) <= rad*rad) {
				// execute the appropriate callback
				CollisionCallback & cb = collisionCallbacks.getByIndex( cbIndex );
				if (cb.callback) {
					if (cb.flipData)
						cb.callback( data.second, data.first );
					else
						cb.callback( data.first, data.second );
				}
			}
		}

		potentialActors.clear();
	}
}

#ifdef _DEBUG
void PhysicsManager::draw( const SysCamera2D * camera ) {
/*	float matrix[16];
	camera->getViewMatrix( matrix );

	{
		Context::get().renderState.blendState.setBlendMode( BM_NONE );

		// draw lines
		Context::get().renderState.gpuBufferState.bind( vertexBuffer.get() );
		Context::get().renderState.shaderState.setProgram( linesProgram );
		static LineVertex vertex;
		static size_t posOffset = (size_t)&vertex.pos - (size_t)&vertex;
		static size_t colOffset = (size_t)&vertex.col - (size_t)&vertex;
		Context::get().renderState.shaderState.enableVertexAttribute( "pos", sizeof( vertex ), lineVertexBufferOffset, posOffset );
		Context::get().renderState.shaderState.enableVertexAttribute( "col", sizeof( vertex ), lineVertexBufferOffset, colOffset );

		Context::get().renderState.shaderState.setUniform( "mvpMatrix", GL_FLOAT_MAT4, matrix );

		Context::get().renderState.drawCallState.setPrimitiveMode( GL_LINES );
		Context::get().renderState.drawCallState.draw( 0, staticColliders.size() * 4 );
	}

	if (!dynamicActors.empty()) {
		Context::get().renderState.blendState.setBlendMode( BM_ALPHA );

		// draw capsules
		Context::get().renderState.gpuBufferState.bind( vertexBuffer.get() );
		Context::get().renderState.gpuBufferState.bind( indexBuffer.get() );
		Context::get().renderState.shaderState.setProgram( capsuleProgram );
		static CapsuleVertex vertex;
		static size_t cornerOffset = (size_t)&vertex.corner - (size_t)&vertex;
		Context::get().renderState.shaderState.enableVertexAttribute( "corner", sizeof( vertex ), 0, cornerOffset );

		Context::get().renderState.shaderState.setUniform( "mvpMatrix", GL_FLOAT_MAT4, matrix );
		Vector4f color( 1.0f, 0.0f, 0.0f, 1.0f );
		Context::get().renderState.shaderState.setUniform( "color", GL_FLOAT_VEC4, &color.x );

		size_t index = 0;
		size_t count = dynamicActors.size();
		while (count > 0) {
			if (dynamicActors.contains( index )) {
				--count;
				const DynamicActor & a = dynamicActors.get( index );
				Vector4f originSize(
					a.capsule.position - Vector2f( a.capsule.radius, a.capsule.radius + a.capsule.height*0.5f ),
					Vector2f( 2.0f * a.capsule.radius, 2.0f * a.capsule.radius + a.capsule.height )
				);
				Context::get().renderState.shaderState.setUniform( "originSize", GL_FLOAT_VEC4, &originSize.x );
				Vector2f point0 = a.capsule.position;
				Vector2f point1 = a.capsule.position;
				point0.y -= a.capsule.height * 0.5f;
				point1.y += a.capsule.height * 0.5f;
				Context::get().renderState.shaderState.setUniform( "point0", GL_FLOAT_VEC2, &point0.x );
				Context::get().renderState.shaderState.setUniform( "point1", GL_FLOAT_VEC2, &point1.x );
				Vector2f lineVec( 0.0f, 1.0f );
				Context::get().renderState.shaderState.setUniform( "lineVec", GL_FLOAT_VEC2, &lineVec.x );
				float proj0 = point0.y, proj1 = point1.y;
				Context::get().renderState.shaderState.setUniform( "proj0", GL_FLOAT, &proj0 );
				Context::get().renderState.shaderState.setUniform( "proj1", GL_FLOAT, &proj1 );
				Context::get().renderState.shaderState.setUniform( "radius", GL_FLOAT, &a.capsule.radius );

				Context::get().renderState.drawCallState.setPrimitiveMode( GL_TRIANGLES );
				Context::get().renderState.drawCallState.setIndexType( GL_UNSIGNED_SHORT );
				Context::get().renderState.drawCallState.drawIndexedRange( 0, 6, 0, 3 );
			}
			++index;
		}
	}*/
}
#endif

void PhysicsManager::checkDynamicActorIndex( size_t index ) const {
	if (!dynamicActors.contains( index ))
		throw std::runtime_error( "Invalid dynamic capsule index specified" );
}

size_t PhysicsManager::createDynamicActor( int updatePriority, const Vector2f & position, float height, float radius, const DynamicActorParameters & params ) {

	size_t index = dynamicActors.add( DynamicActor() );
	DynamicActor & a	= dynamicActors.get( index );
	a.updatePriority	= updatePriority;
	a.capsule.position	= position;
	a.capsule.height	= height;
	a.capsule.radius	= radius;
	a.p					= params;
	a.maxSlopeNormalY	= sin( degToRad( a.p.maxSlopeAngle + 90.0f ) );
	a.maxSlopeAngleTan	= tan( degToRad( a.p.maxSlopeAngle ) );
	a.walking			= 0;
	a.jump				= false;

	a.updatePosition = dynamicActorUpdateList.insert( std::make_pair( updatePriority, index ) );

	addToCells( index );

	return index;
}

void PhysicsManager::destroyDynamicActor( size_t index ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );

	removeFromCells( index );

	if (a.updatePosition != dynamicActorUpdateList.end()) {
		if (updateIteratorOuter == a.updatePosition)
			++updateIteratorOuter;
		if (updateIteratorInner != potentialActors.end() && *updateIteratorInner != index)
			++updateIteratorInner;
		dynamicActorUpdateList.erase( a.updatePosition );
	}
	dynamicActors.remove( index );
}

Vector2f PhysicsManager::getDynamicActorPosition( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).capsule.position;
}

void PhysicsManager::setDynamicActorPosition( size_t index, const Vector2f & position ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	removeFromCells( index );
	a.capsule.position = position;
	addToCells( index );
}

Vector2f PhysicsManager::getDynamicActorVelocity( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).velocity;
}

void PhysicsManager::setDynamicActorVelocity( size_t index, const Vector2f & velocity ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).velocity = velocity;
}

float PhysicsManager::getDynamicActorHeight( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).capsule.height;
}

void PhysicsManager::setDynamicActorHeight( size_t index, float height ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	removeFromCells( index );
	a.capsule.height = height;
	addToCells( index );
}

float PhysicsManager::getDynamicActorRadius( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).capsule.radius;
}

void PhysicsManager::setDynamicActorRadius( size_t index, float radius ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	removeFromCells( index );
	a.capsule.radius = radius;
	addToCells( index );
}

float PhysicsManager::getDynamicActorGravity( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).p.gravity;
}

void PhysicsManager::setDynamicActorGravity( size_t index, float gravity ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).p.gravity = gravity;
}

float PhysicsManager::getDynamicActorMaxGravitySpeed( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).p.maxGravitySpeed;
}

void PhysicsManager::setDynamicActorMaxGravitySpeed( size_t index, float maxGravitySpeed ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).p.maxGravitySpeed = maxGravitySpeed;
}

float PhysicsManager::getDynamicActorMaxSlopeAngle( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).p.maxSlopeAngle;
}

void PhysicsManager::setDynamicActorMaxSlopeAngle( size_t index, float maxSlopeAngle ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).p.maxSlopeAngle = maxSlopeAngle;
	dynamicActors.get( index ).maxSlopeNormalY = sin( degToRad( maxSlopeAngle + 90.0f ) );
	dynamicActors.get( index ).maxSlopeAngleTan = tan( degToRad( maxSlopeAngle ) );
}

float PhysicsManager::getDynamicActorWalkSpeed( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).p.walkSpeed;
}

void PhysicsManager::setDynamicActorWalkSpeed( size_t index, float walkSpeed ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).p.walkSpeed = walkSpeed;
}

float PhysicsManager::getDynamicActorWalkAccel( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).p.walkAccel;
}

void PhysicsManager::setDynamicActorWalkAccel( size_t index, float walkAccel ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).p.walkAccel = walkAccel;
}

float PhysicsManager::getDynamicActorAirAccel( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).p.airAccel;
}

void PhysicsManager::setDynamicActorAirAccel( size_t index, float airAccel ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).p.airAccel = airAccel;
}

float PhysicsManager::getDynamicActorJumpSpeed( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).p.jumpSpeed;
}

void PhysicsManager::setDynamicActorJumpSpeed( size_t index, float jumpSpeed ) {
	checkDynamicActorIndex( index );
	dynamicActors.get( index ).p.jumpSpeed = jumpSpeed;
}

int PhysicsManager::getDynamicActorUpdatePriority( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).updatePriority;
}

void PhysicsManager::setDynamicActorUpdatePriority( size_t index, int updatePriority ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	if (a.updatePriority != updatePriority) {
		a.updatePriority = updatePriority;
		if (a.updatePosition != dynamicActorUpdateList.end()) {
			dynamicActorUpdateList.erase( a.updatePosition );
			a.updatePosition = dynamicActorUpdateList.insert( std::make_pair( updatePriority, index ) );
		}
	}
}

bool PhysicsManager::isDynamicActorActive( size_t index ) const {
	checkDynamicActorIndex( index );
	return dynamicActors.get( index ).updatePosition != dynamicActorUpdateList.end();
}

void PhysicsManager::activateDynamicActor( size_t index ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	if (a.updatePosition == dynamicActorUpdateList.end())
		a.updatePosition = dynamicActorUpdateList.insert( std::make_pair( a.updatePriority, index ) );
}

void PhysicsManager::deactivateDynamicActor( size_t index ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	if (a.updatePosition != dynamicActorUpdateList.end()) {
		if (updateIteratorOuter == a.updatePosition)
			++updateIteratorOuter;
		if (updateIteratorInner != potentialActors.end() && *updateIteratorInner != index)
			++updateIteratorInner;
		dynamicActorUpdateList.erase( a.updatePosition );
		a.updatePosition = dynamicActorUpdateList.end();
	}
}

void PhysicsManager::dynamicActorWalk( size_t index, bool left, bool right ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	a.walking = 0;
	if (left)
		a.walking -= 1;
	if (right)
		a.walking += 1;
}

void PhysicsManager::dynamicActorJump( size_t index ) {
	checkDynamicActorIndex( index );
	DynamicActor & a = dynamicActors.get( index );
	a.jump = true;
}

void PhysicsManager::addCollisionCallback( size_t type1, size_t type2, const boost::function <void ( void*, void* )> & callback ) {
	std::pair <size_t, size_t> key( type1, type2 );
	bool flip = key.second < key.first;
	if (flip)
		std::swap( key.first, key.second );
	collisionCallbacks.remove( key );
	size_t index = collisionCallbacks.put( key, CollisionCallback() );
	CollisionCallback & cb = collisionCallbacks.getByIndex( index );
	cb.callback = callback;
	cb.flipData = flip;
}

void PhysicsManager::removeCollisionCallback( size_t type1, size_t type2 ) {
	std::pair <size_t, size_t> key( type1, type2 );
	bool flip = key.second < key.first;
	if (flip)
		std::swap( key.first, key.second );
	collisionCallbacks.remove( key );
}

RaycastResult PhysicsManager::raycast( const RaycastParameters & params ) {
	RaycastResult res;

	LineSegment ls( params.origin, params.origin + params.direction * params.maxDistance );
	ls.line.normalize();

	float maxDistance = params.maxDistance;

	bool hit = false;
	float best = std::numeric_limits <float>::max();
	bool bestIsCollider = false;
	size_t bestId; // either collider or actor

	if (params.stopsAtColliders) {
		getPotentialColliders( ls );

		for (size_t i = 0; i < potentialColliders.size(); ++i) {
			StaticCollider & c = staticColliders[potentialColliders[i]];
			float t;
			if (::raycast( params.origin, params.direction, c.lineSegment, t ) &&
				t <= maxDistance && t < best) {
				hit = true;
				best = t;
				bestIsCollider = true;
				bestId = c.id;
			}
		}

		clearPotentialColliders();
	}

	if (hit) {
		maxDistance = best;
		res.colliderId = bestId;
		res.distance = best;
		res.hitCollider = true;
	}

	getPotentialActors( ls );

	for (updateIteratorInner = potentialActors.begin(); updateIteratorInner != potentialActors.end(); ++updateIteratorInner) {
		DynamicActor & a = dynamicActors.get( *updateIteratorInner );

		std::pair <size_t, size_t> key( a.p.type, params.type );
		std::pair <void*, void*> data( a.p.data, params.data );
		if (key.second < key.first) {
			std::swap( key.first, key.second );
			std::swap( data.first, data.second );
		}

		// if there's no callback, ignore this collision
		size_t cbIndex;
		if (!collisionCallbacks.getIndex( key, cbIndex ))
			continue;

		float t;
		if (::raycast( params.origin, params.direction, a.capsule, t )) {
			if (t <= maxDistance) {
				res.hitDynamicActor = true;
				if (params.stopsAtDynamicActors) {
					// if we stop at the first actor, we need to first find the closest one
					if (t < best) {
						hit = true;
						best = t;
						bestIsCollider = false;
						bestId = *updateIteratorInner;
					}
				} else {
					// if we don't stop at first actor, execute this callback
					// execute the appropriate callback
					CollisionCallback & cb = collisionCallbacks.getByIndex( cbIndex );
					if (cb.callback) {
						if (cb.flipData)
							cb.callback( data.second, data.first );
						else
							cb.callback( data.first, data.second );
					}
				}
			}
		}
	}

	if (hit && params.stopsAtDynamicActors && !bestIsCollider) {
		res.distance = best;
		DynamicActor & a = dynamicActors.get( bestId );

		// we stop at dynamic actors and we most recently hit one, so execute its callback
		std::pair <size_t, size_t> key( a.p.type, params.type );
		std::pair <void*, void*> data( a.p.data, params.data );
		if (key.second < key.first) {
			std::swap( key.first, key.second );
			std::swap( data.first, data.second );
		}

		// if there's no callback, ignore this collision
		size_t cbIndex = collisionCallbacks.getIndex( key );

		CollisionCallback & cb = collisionCallbacks.getByIndex( cbIndex );
		if (cb.callback) {
			if (cb.flipData)
				cb.callback( data.second, data.first );
			else
				cb.callback( data.first, data.second );
		}
	}

	potentialActors.clear();

	return res;
}

bool PhysicsManager::isPathFree( const Capsule & c, const Vector2f & sweepVec ) {
	bool sweepZero = sweepVec.isZero();
	Box aBounds;
	if (sweepZero)
		aBounds = getBounds( c, epsilon <float>() );
	else {
		Box boundsStart = getBounds( c, epsilon <float>() );
		Box boundsEnd = boundsStart;
		boundsEnd.bounds[0] += sweepVec;
		boundsEnd.bounds[1] += sweepVec;

		aBounds.bounds[0] = vecMin( boundsStart.bounds[0], boundsEnd.bounds[0] );
		aBounds.bounds[1] = vecMax( boundsStart.bounds[1], boundsEnd.bounds[1] );
	}

	getPotentialColliders( aBounds );

	Vector2f isectNormalTemp;
	Vector2f isectPointTemp;
	for (size_t i = 0; i < potentialColliders.size(); ++i) {
		LineSegment & ls = staticColliders[potentialColliders[i]].lineSegment;
		if (!intersect( ls, aBounds ))
			continue;
		if (sweepZero) {
			if (distance2( c, ls, isectNormalTemp ) == 0.0f)
				return false;
		} else {
			if (sweep( c, sweepVec, ls, isectNormalTemp, isectPointTemp ) < 1.0f)
				return false;
		}
	}

	clearPotentialColliders();

	return true;
}

#endif