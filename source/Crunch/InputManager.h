/** @file InputManager.h
 *  @brief Provides functionality for querying input.
 */

#ifndef INPUTMANAGER_DEFINED
#define INPUTMANAGER_DEFINED

#include "Common.h"
#include "InputState.h"
#include "InputEvent.h"
#include <vector>

/** @brief Provides functions to query user input.
 */
class InputManager {
public:
	/** @brief The constructor links the instance to an InputState instance.
	 */
	InputManager();

	/** @brief Updates the state of the input, detecting presses and releases.
	 */
	void update( const std::vector <InputEvent> & eventData );

	bool mouseButton( MouseButtonCode mbc ) const;			/**< Returns whether the given mouse button is down.*/
	bool mouseButtonPressed( MouseButtonCode mbc ) const;	/**< Returns whether the given mouse button was pressed.*/
	bool mouseButtonReleased( MouseButtonCode mbc ) const;	/**< Returns whether the given mouse button was released.*/

	float mouseScrollWheel() const;							/**< Returns the direction the mouse wheel was scrolled.*/

	/** @brief Returns the coordinate of the mouse in pixels relative to the top-left corner of the window.
	 */
	Vector2i mousePosition() const;

	/** @brief Returns the raw mouse delta.
	 */
	Vector2i mouseDelta() const;

	bool key( byte code ) const;			/**< Returns whether the key with the given code is down.*/
	bool keyPressed( byte code ) const;		/**< Returns whether the key with the given code was just pressed.*/
	bool keyReleased( byte code ) const;	/**< Returns whether the key with the given code was just released.*/

	/** @brief Returns the sequence of events since the last update.
	 */
	const std::vector <InputEvent> & getEventStream() const;

private:
	InputState inputState;	// state of input
};

#endif