/** @file Log.h
 *  @brief Contains functionality to keep a log of events/warnings/errors.
 */

#ifndef LOG_DEFINED
#define LOG_DEFINED

#include "Common.h"
#include <ostream>
#include <vector>
#include <boost/thread.hpp>

class Log;

/** @brief A single thread safe message written to the log.
 */
class LogMessage {
public:
	/** @brief Releases the log's lock.
	 */
	~LogMessage();

	/** @brief Writes to the log.
	 */
	template <typename T> LogMessage & operator<<( const T & t );

private:
	friend class Log;	

	// obtains the log's lock
	LogMessage( const std::string & header );
};

/** @brief Contains functions to write to streams.
 *
 *  The log class does not have to be
 *  instantiated because all functions and
 *  members are static. However, before using
 *  it, it must be initialized and output streams
 *  must be specified.
 */
class Log {
public:
	/** @brief Initializes the output stream list. Call this function first.
	 */
	static void createStreamList();

	/** @brief Adds a stream to the output stream list.
	 */
	static void addStream( std::ostream & stream );

	/** @brief Removes a stream from the output stream list.
	 */
	static void removeStream( std::ostream & stream );

	/** @brief Returns a stream which can be used to write info to the log.
	 */
	static LogMessage info();

	/** @brief Returns a stream which can be used to write an error to the log.
	 */
	static LogMessage error();

	/** @brief Returns a stream which can be used to write a warning to the log.
	 */
	static LogMessage warning();

	/** @brief Returns a stream which can be used to write debug info to the log.
	 */
	static LogMessage debug();

	/** @brief Destroys the output stream list. Call this function last.
	 */
	static void freeStreamList();

private:
	friend class LogMessage;

	Log(); // the private undefined constructor prevents instantiation

	static std::vector <std::ostream*> * outputs;	// the output stream list
	static boost::mutex logMutex;					// ensures the log's functionality from multiple threads
};

template <typename T> LogMessage & LogMessage::operator<<( const T & t ) {
	if (Log::outputs != NULL) {
		for (size_t i = 0; i < Log::outputs->size(); ++i)
			*(*Log::outputs)[i] << t;
	}
	return *this;
}

#endif