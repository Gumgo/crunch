/** @file Maths_Matrix22Dec.h
 *  @brief Contains an implementation of a \htmlonly2&#0215&#0050\endhtmlonly matrix class.
 */

#ifndef MATHS_MATRIX22DEC_DEFINED
#define MATHS_MATRIX22DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief An class for \htmlonly2&#0215&#0050\endhtmlonly matrices.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Matrix22 {
	T d[4]; // the matrix components

public:
	/** @brief Constructs a \htmlonly2&#0215&#0050\endhtmlonly identity matrix.
	 */
	Matrix22();

	/** @brief Constructs a \htmlonly2&#0215&#0050\endhtmlonly matrix with the given components.
	 */
	Matrix22( T m00, T m01,
			  T m10, T m11 );

	/** @brief Constructs a \htmlonly2&#0215&#0050\endhtmlonly matrix by copying the one provided.
	 */
	Matrix22( const Matrix22 <T> & m );

	/** @brief The destructor.
	 */
	~Matrix22();

	// operators

	/** @brief Returns the sum of the current value and the matrix m.
	 */
	Matrix22 <T> operator+( const Matrix22 <T> & m ) const;

	/** @brief Returns the current value unmodified.
	 */
	Matrix22 <T> operator+() const;

	/** @brief Returns the difference between the current value and the matrix m.
	 */
	Matrix22 <T> operator-( const Matrix22 <T> & m ) const;

	/** @brief Returns the current value negated.
	 */
	Matrix22 <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Matrix22 <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Matrix22 <T> operator/( T r ) const;

	/** @brief Returns the product of the current value and the vector v.
	 */
	Vector2 <T> operator*( const Vector2 <T> & v ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix22 <T> operator*( const Matrix22 <T> & m ) const;

	/** @brief Returns the product of the current value and the matrix m.
	 */
	Matrix33a <T> operator*( const Matrix33a <T> & m ) const;

	/** @brief Sets the current value to the matrix m.
	 */
	Matrix22 <T> operator=( const Matrix22 <T> & m );

	/** @brief Increments the current value by the matrix m.
	 */
	Matrix22 <T> operator+=( const Matrix22 <T> & m );

	/** @brief Decrements the current value by the matrix m.
	 */
	Matrix22 <T> operator-=( const Matrix22 <T> & m );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Matrix22 <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Matrix22 <T> operator/=( T r );

	/** @brief Multiplies the current value by the matrix m.
	 */
	Matrix22 <T> operator*=( const Matrix22 <T> & m );

	/** @brief Returns whether the current value is equal to the matrix m.
	 */
	bool operator==( const Matrix22 <T> & m ) const;

	/** @brief Returns whether the current value is not equal to the matrix m.
	 */
	bool operator!=( const Matrix22 <T> & m ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Accesses the component of the current value at (row, col).
	 */
	template <typename U> T & operator()( U row, U col );

	/** @brief Returns the component of the current value at (row, col).
	 */
	template <typename U> T operator()( U row, U col ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Matrix22 <U> () const;

	// mutable

	/** @brief Sets the components of the current value to the components provided.
	 */
	void set( T m00, T m01,
			  T m10, T m11 );

	/** @brief Transposes the current value.
	 */
	void transpose();

	/** @brief Inverts the current value.
	 */
	bool invert();

	/** @brief Rotates the current value by a radians.
	 */
	void rotate( T a );

	/** @brief Scales the current value by the vector (x, y).
	 */
	void scale( T x, T y );

	/** @brief Scales the current value by the vector s.
	 */
	void scale( const Vector2 <T> & s );

	/** @brief Sets all components to 0.
	 */
	void zero();

	/** @brief Sets the current value to the identity matrix.
	 */
	void identity();

	// immutable

	/** @brief Returns the transpose of the current value.
	 */
	Matrix22 <T> transposed() const;

	/** @brief Returns the inverse of the current value.
	 */
	Matrix22 <T> inverse() const;

	/** @brief Returns the current value rotated by a radians.
	 */
	Matrix22 <T> rotated( T a ) const;

	/** @brief Returns the current value scaled by the vector (x, y).
	 */
	Matrix22 <T> scaled( T x, T y ) const;

	/** @brief Returns the current value scaled by the vector s.
	 */
	Matrix22 <T> scaled( const Vector2 <T> & s ) const;

	/** @brief Returns the determinant of the current value.
	 */
	T determinant() const;

	/** @brief Returns the trace of the current value.
	 */
	T trace() const;

	/** @brief Returns a pointer to the component array.
	 */
	const T * getArray() const;

	/** @brief Returns the ray r transformed by the current value.
	 */
	Ray2 <T> transformRay( const Ray2 <T> & r ) const;

	/** @brief Returns a matrix used to transform normals.
	 *
	 *  The matrix returned is the inverse transpose
	 *  of the current value.
	 */
	Matrix22 <T> getNormalTransformation() const;
};

/** @brief Returns the product of the scalar r and the matrix m.
 */
template <typename T> Matrix22 <T> operator*( T r, const Matrix22 <T> & m );

/** @brief Matrix22 of type float.
 */
typedef Matrix22 <float> Matrix22f;

/** @brief Matrix22 of type double.
 */
typedef Matrix22 <double> Matrix22d;

#endif