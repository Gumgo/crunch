#if 0

/** @file NetworkMessageLayer.h
 *  @brief The mid level networking layer.
 */

#ifndef NETWORKMESSAGELAYER_DEFINED
#define NETWORKMESSAGELAYER_DEFINED

#include "Common.h"
#include "NetworkDataLayer.h"
#include "Indexer.h"
#include "HashMap.h"
#include "Timer.h"
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <vector>

/** @brief The mid level networking layer.
 *
 *  The message layer provides the functionality of sending
 *  and receiving guaranteed and non guaranteed messages.
 */
class NetworkMessageLayer : private boost::noncopyable {
	static const byte FLAG_NORMAL		= 0;	/**< Message flag for normal messages.*/
	static const byte FLAG_GUARANTEED	= 1;	/**< Message flag for guaranteed message.*/
	static const byte FLAG_ACK			= 2;	/**< Message flag for acknowledgement of guaranteed message received.*/
	static const byte FLAG_PING			= 3;	/**< Message flag for ping.*/

	NetworkDataLayer dataLayer;	/**< The data layer.*/

	bool initialized;			/**< Whether the message layer is initialized.*/

	/** @brief Used to back up guaranteed messages.
	 */
	class MessageBackup {
		/** @brief A data block in a message.
		 */
		struct DataBlock {
			static const size_t DATA_BLOCK_SIZE = 64;	/**< Size of each data block.*/
			byte data[DATA_BLOCK_SIZE];					/**< The backed up data.*/
			size_t dataSize;							/**< The size of the backed up data.*/
			size_t nextBlockIndex;						/**< The index of the next data block, or the maximum value if this is the last.*/
		};

		static const size_t DEFAULT_DATA_BLOCK_COUNT = 40;	/**< The default number of data blocks.*/
		Indexer <DataBlock> dataBlocks;						/**< The data blocks.*/

	public:
		MessageBackup();									/**< The constructor.*/
		size_t add( const byte * data, size_t dataSize );	/**< Backs up data and returns its ID.*/
		void remove( size_t backupId );						/**< Removes the backed up data with the given ID.*/

		/** @brief Returns backed up data with the given index.
		 *
		 *  A pointer to the backed up data is stored in data.
		 *  The size of the backed up data is stored in dataSize.
		 *  If more backed up data follows, this function returns
		 *  true and the index of the next chunk of backed up
		 *  data is stored in next.
		 */
		bool get( size_t backupId, const byte *& data, size_t & dataSize, size_t & next ) const;

		/** @brief Returns the size of a backed up message.
		 */
		size_t size( size_t backupId ) const;
	} messageBackup;	/**< The instance of the message backup system.*/

	/** @brief Holds info about messages being sent to and from data layer connections.
	 */
	struct Connection {
		size_t dataLayerId;								/**< The ID of this connection in the data layer.*/
		uint ping;										/**< The ping time of this connection.*/
		static const uint DEFAULT_PING = 200;			/**< The default/initial assumed ping time.*/
		static const uint PING_SEND_FREQUENCY = 5000;	/**< How often to send pings.*/
		static const uint TIMEOUT_TIME = 30000;			/**< The amount of time before a connection times out.*/

		Timer::TimerValue lastMessageReceivedTime;		/**< The time the last message was received.*/

		// SENDING GUARANTEED MESSAGES:
		ushort nextGuaranteedSendId;	/**< The ID of the next guaranteed message to send.*/

		/** @brief Holds data about guaranteed messages that have not yet been acknowledged.
		 */
		struct PendingAck {
			ushort guaranteedId;			/**< The ID of the guaranteed message.*/
			size_t messageBackupId;			/**< The ID of the backed up message.*/
			Timer::TimerValue lastSentTime;	/**< The time in milliseconds this message was last sent.*/
		};
		std::vector <PendingAck> pendingAcks;	/**< The list of guaranteed messages that have not yet been acknowledged.*/

		// RECEIVING GUARANTEED MESSAGES:
		ushort nextGuaranteedReceiveId;	/**< The ID of the next guaranteed message we should receive/process.*/

		/** @brief Holds data about guaranteed messages that cannot yet be processed.
		 */
		struct PendingGuaranteed {
			ushort guaranteedId;	/**< The ID of the guaranteed message.*/
			size_t messageBackupId;	/**< The ID of the backed up message.*/
		};
		std::vector <PendingGuaranteed> pendingGuaranteed;	/**< The list of guaranteed messages that cannot yet be processed.*/

		// PINGS
		ushort nextPingId;					/**< The ID of the next ping to send.*/
		Timer::TimerValue lastSentPingTime;	/**< The time the most recent ping was sent.*/

		/** @brief Holds data about a ping that has not yet returned.
		 */
		struct PendingPing {
			ushort pingId;				/**< The ID of this ping.*/
			Timer::TimerValue timeSent;	/**< The time this ping was sent.*/
		};
		std::vector <PendingPing> pendingPings;	/**< The list of pending pings that have not yet returned.*/
	};

	std::vector <size_t> connectionsWithPendingAcks;	/**< List of connections with acks pending.*/

	static const size_t GUARANTEED_PROCESSING_BUFFER_SIZE = 256;	/**< Default size of the guaranteed processing buffer.*/
	std::vector <byte> guaranteedProcessingBuffer;				/**< Used when delaying the processing of messages.*/

	Indexer <Connection> connections;							/**< The list of current connections.*/
	HashMap <Hash <size_t>, size_t, size_t> connectionsMap;		/**< Maps data layer connection IDs to message layer connection IDs.*/
	void acceptNewConnection( size_t dataLayerId );				/**< Called when the data layer accepts a new connection.*/
	boost::function <void ( size_t )> onAcceptNewConnection;	/**< Called when a new connection is accepted if new connection acceptance is enabled.*/
	boost::function <void ( size_t )> onConnectionTimeout;		/**< Called when a connection times out.*/

	/** @brief Called when a message is received.
	 */
	void receiveMessage( size_t dataLayerId, const byte * data, size_t dataSize );

	/** @brief Called when a message is received.
	 */
	boost::function <void ( size_t, const byte*, size_t )> onReceiveMessage;

public:
	NetworkMessageLayer();		/**< The constructor.*/
	~NetworkMessageLayer();		/**< The destructor.*/

	bool init( ushort port );	/**< Initializes the message and data layers.*/
	void term();				/**< Terminates the message and data layers.*/

	/** @brief Enables acceptance of new connections.
	 *  @param onAcceptNewConnectionFn	Called when a new connection is accepted, passing the new connection's ID.
	 */
	void enableAcceptNewConnections( const boost::function <void ( size_t )> & onAcceptNewConnectionFn );

	/** @brief Disables acceptance of new connections.
	 */
	void disableAcceptNewConnections();

	/** @brief Sets the function to be called when a connection times out.
	 */
	void setOnConnectionTimeout( const boost::function <void ( size_t )> & onConnectionTimeoutFn );

	/** @brief Creates a new connection.
	 */
	bool createConnection( const std::string & address, ushort port, size_t & newConnectionId );

	/** @brief Destroys a connection.
	 */
	void destroyConnection( size_t connectionId );

	/** @brief Sets the function to be called when a message is received.
	 *
	 *  The function is passed the connection ID, the data pointer, and the data length.
	 */
	void setOnReceiveMessage( const boost::function <void ( size_t, const byte*, size_t )> & onReceiveMessageFn );

	/** @brief Receives and processes incoming data.
	 */
	void receiveAndProcess();

	/** @brief Queues a non guaranteed message to be sent.
	 */
	bool queueMessage( size_t connectionId, const byte * data, size_t dataSize );

	/** @brief Queues a guaranteed message to be sent.
	 */
	bool queueGuaranteedMessage( size_t connectionId, const byte * data, size_t dataSize );

	/** @brief Queues a ping to be sent.
	 */
	bool queuePing( size_t connectionId );

	/** @brief Sends all queued messages.
	 */
	void sendQueued();
};

#endif

#endif