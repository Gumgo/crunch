/** @file Maths_Includes.h
 *  @brief Includes \<cmath\> and defines some additional mathematical constants.
 */

#ifndef MATHS_INCLUDES_DEFINED
#define MATHS_INCLUDES_DEFINED

#include <cmath>
#pragma warning( push )
#pragma warning( disable : 4800 )
#include <boost/math/constants/constants.hpp>
#pragma warning( pop )

#endif