/** @file Maths_Vector4Dec.h
 *  @brief Contains an implementation of a 4D vector class.
 */

#ifndef MATHS_VECTOR4DEC_DEFINED
#define MATHS_VECTOR4DEC_DEFINED

#include "Maths_Includes.h"
#include "Maths_ForwardDec.h"

/** @brief A class for 4D vectors.
 *
 *  @tparam T	The component type.
 */
template <typename T> class Vector4 {
public:
	union {
		struct {
			T x;	/**< The x component.*/
			T y;	/**< The y component.*/
			T z;	/**< The z component.*/
			T w;	/**< The w component.*/
		};
		struct {
			T r;	/**< The r component.*/
			T g;	/**< The g component.*/
			T b;	/**< The b component.*/
			T a;	/**< The a component.*/
		};
	};

	/** @brief Constructs a 4D vector with all components set to 0.
	 */
	Vector4();

	/** @brief Constructs a 4D vector with the provided components.
	 */
	Vector4( T px, T py, T pz, T pw );

	/** @brief Constructs a 4D vector by copying the one provided.
	 */
	Vector4( const Vector4 <T> & v );

	/** @brief Constructs a 4D vector from a 3D vector and a scalar.
	 */
	Vector4( const Vector3 <T> & pxyz, T pw );

	/** @brief Constructs a 4D vector from a scalar and a 3D vector.
	 */
	Vector4( T px, const Vector3 <T> & pyzw );

	/** @brief Constructs a 4D vector from two 2D vectors.
	 */
	Vector4( const Vector2 <T> & pxy, const Vector2 <T> & pzw );

	/** @brief Constructs a 4D vector from a 2D vector and two scalars.
	 */
	Vector4( const Vector2 <T> & pxy, T pz, T pw );

	/** @brief Constructs a 4D vector from a scalar, a 2D vector, and a scalar.
	 */
	Vector4( T px, const Vector2 <T> & pyz, T pw );

	/** @brief Constructs a 4D vector from two scalars and a 2D vector.
	 */
	Vector4( T px, T py, const Vector2 <T> & pzw );

	/** @brief The destructor.
	 */
	~Vector4();

	// operators

	/** @brief Returns the sum of the current value and the vector v.
	 */
	Vector4 <T> operator+( const Vector4 <T> & v ) const;

	/** @brief Returns the current value unmodified.
	 */
	Vector4 <T> operator+() const;

	/** @brief Returns the difference between the current value and the vector v.
	 */
	Vector4 <T> operator-( const Vector4 <T> & v ) const;

	/** @brief Returns the current value negated.
	 */
	Vector4 <T> operator-() const;

	/** @brief Returns the product of the current value and the scalar r.
	 */
	Vector4 <T> operator*( T r ) const;

	/** @brief Returns the quotient of the current value and the scalar r.
	 */
	Vector4 <T> operator/( T r ) const;

	/** @brief Returns a vector containing the product of each component
	 *  in the current value and the corresponding component in the vector v.
	 */
	Vector4 <T> operator*( const Vector4 <T> & v ) const;

	/** @brief Returns a vector containing the quotient of each component
	 *  in the current value and the corresponding component in the vector v.
	 */
	Vector4 <T> operator/( const Vector4 <T> & v ) const;

	/** @brief Sets the current value to the vector v.
	 */
	Vector4 <T> operator=( const Vector4 <T> & v );

	/** @brief Increments the current value by the vector v.
	 */
	Vector4 <T> operator+=( const Vector4 <T> & v );

	/** @brief Decrements the current value by the vector v.
	 */
	Vector4 <T> operator-=( const Vector4 <T> & v );

	/** @brief Multiplies the current value by the scalar r.
	 */
	Vector4 <T> operator*=( T r );

	/** @brief Divides the current value by the scalar r.
	 */
	Vector4 <T> operator/=( T r );

	/** @brief Multiplies each component in the current value
	 *  by the corresponding component in the vector v.
	 */
	Vector4 <T> operator*=( const Vector4 <T> & v );

	/** @brief Divides each component in the current value
	 *  by the corresponding component in the vector v.
	 */
	Vector4 <T> operator/=( const Vector4 <T> & v );

	/** @brief Returns whether the current value is equal to the vector v.
	 */
	bool operator==( const Vector4 <T> & v ) const;

	/** @brief Returns whether the current value is not equal to the vector v.
	 */
	bool operator!=( const Vector4 <T> & v ) const;

	/** @brief Accesses the <i>i</i>th component of the current value.
	 */
	template <typename U> T & operator[]( U i );

	/** @brief Returns the <i>i</i>th component of the current value.
	 */
	template <typename U> T operator[]( U i ) const;

	/** @brief Casts to a value of another type.
	 */
	template <typename U> operator Vector4 <U> () const;

	// mutable

	/** @brief Sets the current value's components to (px, py, pz, pw).
	 */
	void set( T px, T py, T pz, T pw );

	/** @brief Normalizes the current value.
	 */
	void normalize();

	/** @brief Sets the magnitude of the current value to the scalar m.
	 */
	void setMagnitude( T m );

	// immutable

	/** @brief Returns the current value normalized.
	 */
	Vector4 <T> normalized() const;

	/** @brief Returns the magnitude of the current value.
	 */
	T magnitude() const;

	/** @brief Returns the squared magnitude of the current value.
	 */
	T magnitudeSquared() const;

	/** @brief Returns the dot product of the current value and the vector v.
	 */
	T dot( const Vector4 <T> & v ) const;

	/** @brief Returns the current value rotated by \htmlonly90&#0176\endhtmlonly.
	 */
	bool isZero() const;

	// swizzling
	Vector4 <T> xxxx() const;	/**< Returns a vector with components of (x, x, x, x).*/
	Vector4 <T> xxxy() const;	/**< Returns a vector with components of (x, x, x, y).*/
	Vector4 <T> xxxz() const;	/**< Returns a vector with components of (x, x, x, z).*/
	Vector4 <T> xxxw() const;	/**< Returns a vector with components of (x, x, x, w).*/
	Vector4 <T> xxyx() const;	/**< Returns a vector with components of (x, x, y, x).*/
	Vector4 <T> xxyy() const;	/**< Returns a vector with components of (x, x, y, y).*/
	Vector4 <T> xxyz() const;	/**< Returns a vector with components of (x, x, y, z).*/
	Vector4 <T> xxyw() const;	/**< Returns a vector with components of (x, x, y, w).*/
	Vector4 <T> xxzx() const;	/**< Returns a vector with components of (x, x, z, x).*/
	Vector4 <T> xxzy() const;	/**< Returns a vector with components of (x, x, z, y).*/
	Vector4 <T> xxzz() const;	/**< Returns a vector with components of (x, x, z, z).*/
	Vector4 <T> xxzw() const;	/**< Returns a vector with components of (x, x, z, w).*/
	Vector4 <T> xxwx() const;	/**< Returns a vector with components of (x, x, w, x).*/
	Vector4 <T> xxwy() const;	/**< Returns a vector with components of (x, x, w, y).*/
	Vector4 <T> xxwz() const;	/**< Returns a vector with components of (x, x, w, z).*/
	Vector4 <T> xxww() const;	/**< Returns a vector with components of (x, x, w, w).*/
	Vector4 <T> xyxx() const;	/**< Returns a vector with components of (x, y, x, x).*/
	Vector4 <T> xyxy() const;	/**< Returns a vector with components of (x, y, x, y).*/
	Vector4 <T> xyxz() const;	/**< Returns a vector with components of (x, y, x, z).*/
	Vector4 <T> xyxw() const;	/**< Returns a vector with components of (x, y, x, w).*/
	Vector4 <T> xyyx() const;	/**< Returns a vector with components of (x, y, y, x).*/
	Vector4 <T> xyyy() const;	/**< Returns a vector with components of (x, y, y, y).*/
	Vector4 <T> xyyz() const;	/**< Returns a vector with components of (x, y, y, z).*/
	Vector4 <T> xyyw() const;	/**< Returns a vector with components of (x, y, y, w).*/
	Vector4 <T> xyzx() const;	/**< Returns a vector with components of (x, y, z, x).*/
	Vector4 <T> xyzy() const;	/**< Returns a vector with components of (x, y, z, y).*/
	Vector4 <T> xyzz() const;	/**< Returns a vector with components of (x, y, z, z).*/
	Vector4 <T> xyzw() const;	/**< Returns a vector with components of (x, y, z, w).*/
	Vector4 <T> xywx() const;	/**< Returns a vector with components of (x, y, w, x).*/
	Vector4 <T> xywy() const;	/**< Returns a vector with components of (x, y, w, y).*/
	Vector4 <T> xywz() const;	/**< Returns a vector with components of (x, y, w, z).*/
	Vector4 <T> xyww() const;	/**< Returns a vector with components of (x, y, w, w).*/
	Vector4 <T> xzxx() const;	/**< Returns a vector with components of (x, z, x, x).*/
	Vector4 <T> xzxy() const;	/**< Returns a vector with components of (x, z, x, y).*/
	Vector4 <T> xzxz() const;	/**< Returns a vector with components of (x, z, x, z).*/
	Vector4 <T> xzxw() const;	/**< Returns a vector with components of (x, z, x, w).*/
	Vector4 <T> xzyx() const;	/**< Returns a vector with components of (x, z, y, x).*/
	Vector4 <T> xzyy() const;	/**< Returns a vector with components of (x, z, y, y).*/
	Vector4 <T> xzyz() const;	/**< Returns a vector with components of (x, z, y, z).*/
	Vector4 <T> xzyw() const;	/**< Returns a vector with components of (x, z, y, w).*/
	Vector4 <T> xzzx() const;	/**< Returns a vector with components of (x, z, z, x).*/
	Vector4 <T> xzzy() const;	/**< Returns a vector with components of (x, z, z, y).*/
	Vector4 <T> xzzz() const;	/**< Returns a vector with components of (x, z, z, z).*/
	Vector4 <T> xzzw() const;	/**< Returns a vector with components of (x, z, z, w).*/
	Vector4 <T> xzwx() const;	/**< Returns a vector with components of (x, z, w, x).*/
	Vector4 <T> xzwy() const;	/**< Returns a vector with components of (x, z, w, y).*/
	Vector4 <T> xzwz() const;	/**< Returns a vector with components of (x, z, w, z).*/
	Vector4 <T> xzww() const;	/**< Returns a vector with components of (x, z, w, w).*/
	Vector4 <T> xwxx() const;	/**< Returns a vector with components of (x, w, x, x).*/
	Vector4 <T> xwxy() const;	/**< Returns a vector with components of (x, w, x, y).*/
	Vector4 <T> xwxz() const;	/**< Returns a vector with components of (x, w, x, z).*/
	Vector4 <T> xwxw() const;	/**< Returns a vector with components of (x, w, x, w).*/
	Vector4 <T> xwyx() const;	/**< Returns a vector with components of (x, w, y, x).*/
	Vector4 <T> xwyy() const;	/**< Returns a vector with components of (x, w, y, y).*/
	Vector4 <T> xwyz() const;	/**< Returns a vector with components of (x, w, y, z).*/
	Vector4 <T> xwyw() const;	/**< Returns a vector with components of (x, w, y, w).*/
	Vector4 <T> xwzx() const;	/**< Returns a vector with components of (x, w, z, x).*/
	Vector4 <T> xwzy() const;	/**< Returns a vector with components of (x, w, z, y).*/
	Vector4 <T> xwzz() const;	/**< Returns a vector with components of (x, w, z, z).*/
	Vector4 <T> xwzw() const;	/**< Returns a vector with components of (x, w, z, w).*/
	Vector4 <T> xwwx() const;	/**< Returns a vector with components of (x, w, w, x).*/
	Vector4 <T> xwwy() const;	/**< Returns a vector with components of (x, w, w, y).*/
	Vector4 <T> xwwz() const;	/**< Returns a vector with components of (x, w, w, z).*/
	Vector4 <T> xwww() const;	/**< Returns a vector with components of (x, w, w, w).*/
	Vector4 <T> yxxx() const;	/**< Returns a vector with components of (y, x, x, x).*/
	Vector4 <T> yxxy() const;	/**< Returns a vector with components of (y, x, x, y).*/
	Vector4 <T> yxxz() const;	/**< Returns a vector with components of (y, x, x, z).*/
	Vector4 <T> yxxw() const;	/**< Returns a vector with components of (y, x, x, w).*/
	Vector4 <T> yxyx() const;	/**< Returns a vector with components of (y, x, y, x).*/
	Vector4 <T> yxyy() const;	/**< Returns a vector with components of (y, x, y, y).*/
	Vector4 <T> yxyz() const;	/**< Returns a vector with components of (y, x, y, z).*/
	Vector4 <T> yxyw() const;	/**< Returns a vector with components of (y, x, y, w).*/
	Vector4 <T> yxzx() const;	/**< Returns a vector with components of (y, x, z, x).*/
	Vector4 <T> yxzy() const;	/**< Returns a vector with components of (y, x, z, y).*/
	Vector4 <T> yxzz() const;	/**< Returns a vector with components of (y, x, z, z).*/
	Vector4 <T> yxzw() const;	/**< Returns a vector with components of (y, x, z, w).*/
	Vector4 <T> yxwx() const;	/**< Returns a vector with components of (y, x, w, x).*/
	Vector4 <T> yxwy() const;	/**< Returns a vector with components of (y, x, w, y).*/
	Vector4 <T> yxwz() const;	/**< Returns a vector with components of (y, x, w, z).*/
	Vector4 <T> yxww() const;	/**< Returns a vector with components of (y, x, w, w).*/
	Vector4 <T> yyxx() const;	/**< Returns a vector with components of (y, y, x, x).*/
	Vector4 <T> yyxy() const;	/**< Returns a vector with components of (y, y, x, y).*/
	Vector4 <T> yyxz() const;	/**< Returns a vector with components of (y, y, x, z).*/
	Vector4 <T> yyxw() const;	/**< Returns a vector with components of (y, y, x, w).*/
	Vector4 <T> yyyx() const;	/**< Returns a vector with components of (y, y, y, x).*/
	Vector4 <T> yyyy() const;	/**< Returns a vector with components of (y, y, y, y).*/
	Vector4 <T> yyyz() const;	/**< Returns a vector with components of (y, y, y, z).*/
	Vector4 <T> yyyw() const;	/**< Returns a vector with components of (y, y, y, w).*/
	Vector4 <T> yyzx() const;	/**< Returns a vector with components of (y, y, z, x).*/
	Vector4 <T> yyzy() const;	/**< Returns a vector with components of (y, y, z, y).*/
	Vector4 <T> yyzz() const;	/**< Returns a vector with components of (y, y, z, z).*/
	Vector4 <T> yyzw() const;	/**< Returns a vector with components of (y, y, z, w).*/
	Vector4 <T> yywx() const;	/**< Returns a vector with components of (y, y, w, x).*/
	Vector4 <T> yywy() const;	/**< Returns a vector with components of (y, y, w, y).*/
	Vector4 <T> yywz() const;	/**< Returns a vector with components of (y, y, w, z).*/
	Vector4 <T> yyww() const;	/**< Returns a vector with components of (y, y, w, w).*/
	Vector4 <T> yzxx() const;	/**< Returns a vector with components of (y, z, x, x).*/
	Vector4 <T> yzxy() const;	/**< Returns a vector with components of (y, z, x, y).*/
	Vector4 <T> yzxz() const;	/**< Returns a vector with components of (y, z, x, z).*/
	Vector4 <T> yzxw() const;	/**< Returns a vector with components of (y, z, x, w).*/
	Vector4 <T> yzyx() const;	/**< Returns a vector with components of (y, z, y, x).*/
	Vector4 <T> yzyy() const;	/**< Returns a vector with components of (y, z, y, y).*/
	Vector4 <T> yzyz() const;	/**< Returns a vector with components of (y, z, y, z).*/
	Vector4 <T> yzyw() const;	/**< Returns a vector with components of (y, z, y, w).*/
	Vector4 <T> yzzx() const;	/**< Returns a vector with components of (y, z, z, x).*/
	Vector4 <T> yzzy() const;	/**< Returns a vector with components of (y, z, z, y).*/
	Vector4 <T> yzzz() const;	/**< Returns a vector with components of (y, z, z, z).*/
	Vector4 <T> yzzw() const;	/**< Returns a vector with components of (y, z, z, w).*/
	Vector4 <T> yzwx() const;	/**< Returns a vector with components of (y, z, w, x).*/
	Vector4 <T> yzwy() const;	/**< Returns a vector with components of (y, z, w, y).*/
	Vector4 <T> yzwz() const;	/**< Returns a vector with components of (y, z, w, z).*/
	Vector4 <T> yzww() const;	/**< Returns a vector with components of (y, z, w, w).*/
	Vector4 <T> ywxx() const;	/**< Returns a vector with components of (y, w, x, x).*/
	Vector4 <T> ywxy() const;	/**< Returns a vector with components of (y, w, x, y).*/
	Vector4 <T> ywxz() const;	/**< Returns a vector with components of (y, w, x, z).*/
	Vector4 <T> ywxw() const;	/**< Returns a vector with components of (y, w, x, w).*/
	Vector4 <T> ywyx() const;	/**< Returns a vector with components of (y, w, y, x).*/
	Vector4 <T> ywyy() const;	/**< Returns a vector with components of (y, w, y, y).*/
	Vector4 <T> ywyz() const;	/**< Returns a vector with components of (y, w, y, z).*/
	Vector4 <T> ywyw() const;	/**< Returns a vector with components of (y, w, y, w).*/
	Vector4 <T> ywzx() const;	/**< Returns a vector with components of (y, w, z, x).*/
	Vector4 <T> ywzy() const;	/**< Returns a vector with components of (y, w, z, y).*/
	Vector4 <T> ywzz() const;	/**< Returns a vector with components of (y, w, z, z).*/
	Vector4 <T> ywzw() const;	/**< Returns a vector with components of (y, w, z, w).*/
	Vector4 <T> ywwx() const;	/**< Returns a vector with components of (y, w, w, x).*/
	Vector4 <T> ywwy() const;	/**< Returns a vector with components of (y, w, w, y).*/
	Vector4 <T> ywwz() const;	/**< Returns a vector with components of (y, w, w, z).*/
	Vector4 <T> ywww() const;	/**< Returns a vector with components of (y, w, w, w).*/
	Vector4 <T> zxxx() const;	/**< Returns a vector with components of (z, x, x, x).*/
	Vector4 <T> zxxy() const;	/**< Returns a vector with components of (z, x, x, y).*/
	Vector4 <T> zxxz() const;	/**< Returns a vector with components of (z, x, x, z).*/
	Vector4 <T> zxxw() const;	/**< Returns a vector with components of (z, x, x, w).*/
	Vector4 <T> zxyx() const;	/**< Returns a vector with components of (z, x, y, x).*/
	Vector4 <T> zxyy() const;	/**< Returns a vector with components of (z, x, y, y).*/
	Vector4 <T> zxyz() const;	/**< Returns a vector with components of (z, x, y, z).*/
	Vector4 <T> zxyw() const;	/**< Returns a vector with components of (z, x, y, w).*/
	Vector4 <T> zxzx() const;	/**< Returns a vector with components of (z, x, z, x).*/
	Vector4 <T> zxzy() const;	/**< Returns a vector with components of (z, x, z, y).*/
	Vector4 <T> zxzz() const;	/**< Returns a vector with components of (z, x, z, z).*/
	Vector4 <T> zxzw() const;	/**< Returns a vector with components of (z, x, z, w).*/
	Vector4 <T> zxwx() const;	/**< Returns a vector with components of (z, x, w, x).*/
	Vector4 <T> zxwy() const;	/**< Returns a vector with components of (z, x, w, y).*/
	Vector4 <T> zxwz() const;	/**< Returns a vector with components of (z, x, w, z).*/
	Vector4 <T> zxww() const;	/**< Returns a vector with components of (z, x, w, w).*/
	Vector4 <T> zyxx() const;	/**< Returns a vector with components of (z, y, x, x).*/
	Vector4 <T> zyxy() const;	/**< Returns a vector with components of (z, y, x, y).*/
	Vector4 <T> zyxz() const;	/**< Returns a vector with components of (z, y, x, z).*/
	Vector4 <T> zyxw() const;	/**< Returns a vector with components of (z, y, x, w).*/
	Vector4 <T> zyyx() const;	/**< Returns a vector with components of (z, y, y, x).*/
	Vector4 <T> zyyy() const;	/**< Returns a vector with components of (z, y, y, y).*/
	Vector4 <T> zyyz() const;	/**< Returns a vector with components of (z, y, y, z).*/
	Vector4 <T> zyyw() const;	/**< Returns a vector with components of (z, y, y, w).*/
	Vector4 <T> zyzx() const;	/**< Returns a vector with components of (z, y, z, x).*/
	Vector4 <T> zyzy() const;	/**< Returns a vector with components of (z, y, z, y).*/
	Vector4 <T> zyzz() const;	/**< Returns a vector with components of (z, y, z, z).*/
	Vector4 <T> zyzw() const;	/**< Returns a vector with components of (z, y, z, w).*/
	Vector4 <T> zywx() const;	/**< Returns a vector with components of (z, y, w, x).*/
	Vector4 <T> zywy() const;	/**< Returns a vector with components of (z, y, w, y).*/
	Vector4 <T> zywz() const;	/**< Returns a vector with components of (z, y, w, z).*/
	Vector4 <T> zyww() const;	/**< Returns a vector with components of (z, y, w, w).*/
	Vector4 <T> zzxx() const;	/**< Returns a vector with components of (z, z, x, x).*/
	Vector4 <T> zzxy() const;	/**< Returns a vector with components of (z, z, x, y).*/
	Vector4 <T> zzxz() const;	/**< Returns a vector with components of (z, z, x, z).*/
	Vector4 <T> zzxw() const;	/**< Returns a vector with components of (z, z, x, w).*/
	Vector4 <T> zzyx() const;	/**< Returns a vector with components of (z, z, y, x).*/
	Vector4 <T> zzyy() const;	/**< Returns a vector with components of (z, z, y, y).*/
	Vector4 <T> zzyz() const;	/**< Returns a vector with components of (z, z, y, z).*/
	Vector4 <T> zzyw() const;	/**< Returns a vector with components of (z, z, y, w).*/
	Vector4 <T> zzzx() const;	/**< Returns a vector with components of (z, z, z, x).*/
	Vector4 <T> zzzy() const;	/**< Returns a vector with components of (z, z, z, y).*/
	Vector4 <T> zzzz() const;	/**< Returns a vector with components of (z, z, z, z).*/
	Vector4 <T> zzzw() const;	/**< Returns a vector with components of (z, z, z, w).*/
	Vector4 <T> zzwx() const;	/**< Returns a vector with components of (z, z, w, x).*/
	Vector4 <T> zzwy() const;	/**< Returns a vector with components of (z, z, w, y).*/
	Vector4 <T> zzwz() const;	/**< Returns a vector with components of (z, z, w, z).*/
	Vector4 <T> zzww() const;	/**< Returns a vector with components of (z, z, w, w).*/
	Vector4 <T> zwxx() const;	/**< Returns a vector with components of (z, w, x, x).*/
	Vector4 <T> zwxy() const;	/**< Returns a vector with components of (z, w, x, y).*/
	Vector4 <T> zwxz() const;	/**< Returns a vector with components of (z, w, x, z).*/
	Vector4 <T> zwxw() const;	/**< Returns a vector with components of (z, w, x, w).*/
	Vector4 <T> zwyx() const;	/**< Returns a vector with components of (z, w, y, x).*/
	Vector4 <T> zwyy() const;	/**< Returns a vector with components of (z, w, y, y).*/
	Vector4 <T> zwyz() const;	/**< Returns a vector with components of (z, w, y, z).*/
	Vector4 <T> zwyw() const;	/**< Returns a vector with components of (z, w, y, w).*/
	Vector4 <T> zwzx() const;	/**< Returns a vector with components of (z, w, z, x).*/
	Vector4 <T> zwzy() const;	/**< Returns a vector with components of (z, w, z, y).*/
	Vector4 <T> zwzz() const;	/**< Returns a vector with components of (z, w, z, z).*/
	Vector4 <T> zwzw() const;	/**< Returns a vector with components of (z, w, z, w).*/
	Vector4 <T> zwwx() const;	/**< Returns a vector with components of (z, w, w, x).*/
	Vector4 <T> zwwy() const;	/**< Returns a vector with components of (z, w, w, y).*/
	Vector4 <T> zwwz() const;	/**< Returns a vector with components of (z, w, w, z).*/
	Vector4 <T> zwww() const;	/**< Returns a vector with components of (z, w, w, w).*/
	Vector4 <T> wxxx() const;	/**< Returns a vector with components of (w, x, x, x).*/
	Vector4 <T> wxxy() const;	/**< Returns a vector with components of (w, x, x, y).*/
	Vector4 <T> wxxz() const;	/**< Returns a vector with components of (w, x, x, z).*/
	Vector4 <T> wxxw() const;	/**< Returns a vector with components of (w, x, x, w).*/
	Vector4 <T> wxyx() const;	/**< Returns a vector with components of (w, x, y, x).*/
	Vector4 <T> wxyy() const;	/**< Returns a vector with components of (w, x, y, y).*/
	Vector4 <T> wxyz() const;	/**< Returns a vector with components of (w, x, y, z).*/
	Vector4 <T> wxyw() const;	/**< Returns a vector with components of (w, x, y, w).*/
	Vector4 <T> wxzx() const;	/**< Returns a vector with components of (w, x, z, x).*/
	Vector4 <T> wxzy() const;	/**< Returns a vector with components of (w, x, z, y).*/
	Vector4 <T> wxzz() const;	/**< Returns a vector with components of (w, x, z, z).*/
	Vector4 <T> wxzw() const;	/**< Returns a vector with components of (w, x, z, w).*/
	Vector4 <T> wxwx() const;	/**< Returns a vector with components of (w, x, w, x).*/
	Vector4 <T> wxwy() const;	/**< Returns a vector with components of (w, x, w, y).*/
	Vector4 <T> wxwz() const;	/**< Returns a vector with components of (w, x, w, z).*/
	Vector4 <T> wxww() const;	/**< Returns a vector with components of (w, x, w, w).*/
	Vector4 <T> wyxx() const;	/**< Returns a vector with components of (w, y, x, x).*/
	Vector4 <T> wyxy() const;	/**< Returns a vector with components of (w, y, x, y).*/
	Vector4 <T> wyxz() const;	/**< Returns a vector with components of (w, y, x, z).*/
	Vector4 <T> wyxw() const;	/**< Returns a vector with components of (w, y, x, w).*/
	Vector4 <T> wyyx() const;	/**< Returns a vector with components of (w, y, y, x).*/
	Vector4 <T> wyyy() const;	/**< Returns a vector with components of (w, y, y, y).*/
	Vector4 <T> wyyz() const;	/**< Returns a vector with components of (w, y, y, z).*/
	Vector4 <T> wyyw() const;	/**< Returns a vector with components of (w, y, y, w).*/
	Vector4 <T> wyzx() const;	/**< Returns a vector with components of (w, y, z, x).*/
	Vector4 <T> wyzy() const;	/**< Returns a vector with components of (w, y, z, y).*/
	Vector4 <T> wyzz() const;	/**< Returns a vector with components of (w, y, z, z).*/
	Vector4 <T> wyzw() const;	/**< Returns a vector with components of (w, y, z, w).*/
	Vector4 <T> wywx() const;	/**< Returns a vector with components of (w, y, w, x).*/
	Vector4 <T> wywy() const;	/**< Returns a vector with components of (w, y, w, y).*/
	Vector4 <T> wywz() const;	/**< Returns a vector with components of (w, y, w, z).*/
	Vector4 <T> wyww() const;	/**< Returns a vector with components of (w, y, w, w).*/
	Vector4 <T> wzxx() const;	/**< Returns a vector with components of (w, z, x, x).*/
	Vector4 <T> wzxy() const;	/**< Returns a vector with components of (w, z, x, y).*/
	Vector4 <T> wzxz() const;	/**< Returns a vector with components of (w, z, x, z).*/
	Vector4 <T> wzxw() const;	/**< Returns a vector with components of (w, z, x, w).*/
	Vector4 <T> wzyx() const;	/**< Returns a vector with components of (w, z, y, x).*/
	Vector4 <T> wzyy() const;	/**< Returns a vector with components of (w, z, y, y).*/
	Vector4 <T> wzyz() const;	/**< Returns a vector with components of (w, z, y, z).*/
	Vector4 <T> wzyw() const;	/**< Returns a vector with components of (w, z, y, w).*/
	Vector4 <T> wzzx() const;	/**< Returns a vector with components of (w, z, z, x).*/
	Vector4 <T> wzzy() const;	/**< Returns a vector with components of (w, z, z, y).*/
	Vector4 <T> wzzz() const;	/**< Returns a vector with components of (w, z, z, z).*/
	Vector4 <T> wzzw() const;	/**< Returns a vector with components of (w, z, z, w).*/
	Vector4 <T> wzwx() const;	/**< Returns a vector with components of (w, z, w, x).*/
	Vector4 <T> wzwy() const;	/**< Returns a vector with components of (w, z, w, y).*/
	Vector4 <T> wzwz() const;	/**< Returns a vector with components of (w, z, w, z).*/
	Vector4 <T> wzww() const;	/**< Returns a vector with components of (w, z, w, w).*/
	Vector4 <T> wwxx() const;	/**< Returns a vector with components of (w, w, x, x).*/
	Vector4 <T> wwxy() const;	/**< Returns a vector with components of (w, w, x, y).*/
	Vector4 <T> wwxz() const;	/**< Returns a vector with components of (w, w, x, z).*/
	Vector4 <T> wwxw() const;	/**< Returns a vector with components of (w, w, x, w).*/
	Vector4 <T> wwyx() const;	/**< Returns a vector with components of (w, w, y, x).*/
	Vector4 <T> wwyy() const;	/**< Returns a vector with components of (w, w, y, y).*/
	Vector4 <T> wwyz() const;	/**< Returns a vector with components of (w, w, y, z).*/
	Vector4 <T> wwyw() const;	/**< Returns a vector with components of (w, w, y, w).*/
	Vector4 <T> wwzx() const;	/**< Returns a vector with components of (w, w, z, x).*/
	Vector4 <T> wwzy() const;	/**< Returns a vector with components of (w, w, z, y).*/
	Vector4 <T> wwzz() const;	/**< Returns a vector with components of (w, w, z, z).*/
	Vector4 <T> wwzw() const;	/**< Returns a vector with components of (w, w, z, w).*/
	Vector4 <T> wwwx() const;	/**< Returns a vector with components of (w, w, w, x).*/
	Vector4 <T> wwwy() const;	/**< Returns a vector with components of (w, w, w, y).*/
	Vector4 <T> wwwz() const;	/**< Returns a vector with components of (w, w, w, z).*/
	Vector4 <T> wwww() const;	/**< Returns a vector with components of (w, w, w, w).*/
	Vector3 <T> xxx() const;	/**< Returns a vector with components of (x, x, x).*/
	Vector3 <T> xxy() const;	/**< Returns a vector with components of (x, x, y).*/
	Vector3 <T> xxz() const;	/**< Returns a vector with components of (x, x, z).*/
	Vector3 <T> xxw() const;	/**< Returns a vector with components of (x, x, w).*/
	Vector3 <T> xyx() const;	/**< Returns a vector with components of (x, y, x).*/
	Vector3 <T> xyy() const;	/**< Returns a vector with components of (x, y, y).*/
	Vector3 <T> xyz() const;	/**< Returns a vector with components of (x, y, z).*/
	Vector3 <T> xyw() const;	/**< Returns a vector with components of (x, y, w).*/
	Vector3 <T> xzx() const;	/**< Returns a vector with components of (x, z, x).*/
	Vector3 <T> xzy() const;	/**< Returns a vector with components of (x, z, y).*/
	Vector3 <T> xzz() const;	/**< Returns a vector with components of (x, z, z).*/
	Vector3 <T> xzw() const;	/**< Returns a vector with components of (x, z, w).*/
	Vector3 <T> xwx() const;	/**< Returns a vector with components of (x, w, x).*/
	Vector3 <T> xwy() const;	/**< Returns a vector with components of (x, w, y).*/
	Vector3 <T> xwz() const;	/**< Returns a vector with components of (x, w, z).*/
	Vector3 <T> xww() const;	/**< Returns a vector with components of (x, w, w).*/
	Vector3 <T> yxx() const;	/**< Returns a vector with components of (y, x, x).*/
	Vector3 <T> yxy() const;	/**< Returns a vector with components of (y, x, y).*/
	Vector3 <T> yxz() const;	/**< Returns a vector with components of (y, x, z).*/
	Vector3 <T> yxw() const;	/**< Returns a vector with components of (y, x, w).*/
	Vector3 <T> yyx() const;	/**< Returns a vector with components of (y, y, x).*/
	Vector3 <T> yyy() const;	/**< Returns a vector with components of (y, y, y).*/
	Vector3 <T> yyz() const;	/**< Returns a vector with components of (y, y, z).*/
	Vector3 <T> yyw() const;	/**< Returns a vector with components of (y, y, w).*/
	Vector3 <T> yzx() const;	/**< Returns a vector with components of (y, z, x).*/
	Vector3 <T> yzy() const;	/**< Returns a vector with components of (y, z, y).*/
	Vector3 <T> yzz() const;	/**< Returns a vector with components of (y, z, z).*/
	Vector3 <T> yzw() const;	/**< Returns a vector with components of (y, z, w).*/
	Vector3 <T> ywx() const;	/**< Returns a vector with components of (y, w, x).*/
	Vector3 <T> ywy() const;	/**< Returns a vector with components of (y, w, y).*/
	Vector3 <T> ywz() const;	/**< Returns a vector with components of (y, w, z).*/
	Vector3 <T> yww() const;	/**< Returns a vector with components of (y, w, w).*/
	Vector3 <T> zxx() const;	/**< Returns a vector with components of (z, x, x).*/
	Vector3 <T> zxy() const;	/**< Returns a vector with components of (z, x, y).*/
	Vector3 <T> zxz() const;	/**< Returns a vector with components of (z, x, z).*/
	Vector3 <T> zxw() const;	/**< Returns a vector with components of (z, x, w).*/
	Vector3 <T> zyx() const;	/**< Returns a vector with components of (z, y, x).*/
	Vector3 <T> zyy() const;	/**< Returns a vector with components of (z, y, y).*/
	Vector3 <T> zyz() const;	/**< Returns a vector with components of (z, y, z).*/
	Vector3 <T> zyw() const;	/**< Returns a vector with components of (z, y, w).*/
	Vector3 <T> zzx() const;	/**< Returns a vector with components of (z, z, x).*/
	Vector3 <T> zzy() const;	/**< Returns a vector with components of (z, z, y).*/
	Vector3 <T> zzz() const;	/**< Returns a vector with components of (z, z, z).*/
	Vector3 <T> zzw() const;	/**< Returns a vector with components of (z, z, w).*/
	Vector3 <T> zwx() const;	/**< Returns a vector with components of (z, w, x).*/
	Vector3 <T> zwy() const;	/**< Returns a vector with components of (z, w, y).*/
	Vector3 <T> zwz() const;	/**< Returns a vector with components of (z, w, z).*/
	Vector3 <T> zww() const;	/**< Returns a vector with components of (z, w, w).*/
	Vector3 <T> wxx() const;	/**< Returns a vector with components of (w, x, x).*/
	Vector3 <T> wxy() const;	/**< Returns a vector with components of (w, x, y).*/
	Vector3 <T> wxz() const;	/**< Returns a vector with components of (w, x, z).*/
	Vector3 <T> wxw() const;	/**< Returns a vector with components of (w, x, w).*/
	Vector3 <T> wyx() const;	/**< Returns a vector with components of (w, y, x).*/
	Vector3 <T> wyy() const;	/**< Returns a vector with components of (w, y, y).*/
	Vector3 <T> wyz() const;	/**< Returns a vector with components of (w, y, z).*/
	Vector3 <T> wyw() const;	/**< Returns a vector with components of (w, y, w).*/
	Vector3 <T> wzx() const;	/**< Returns a vector with components of (w, z, x).*/
	Vector3 <T> wzy() const;	/**< Returns a vector with components of (w, z, y).*/
	Vector3 <T> wzz() const;	/**< Returns a vector with components of (w, z, z).*/
	Vector3 <T> wzw() const;	/**< Returns a vector with components of (w, z, w).*/
	Vector3 <T> wwx() const;	/**< Returns a vector with components of (w, w, x).*/
	Vector3 <T> wwy() const;	/**< Returns a vector with components of (w, w, y).*/
	Vector3 <T> wwz() const;	/**< Returns a vector with components of (w, w, z).*/
	Vector3 <T> www() const;	/**< Returns a vector with components of (w, w, w).*/
	Vector2 <T> xx() const;		/**< Returns a vector with components of (x, x).*/
	Vector2 <T> xy() const;		/**< Returns a vector with components of (x, y).*/
	Vector2 <T> xz() const;		/**< Returns a vector with components of (x, z).*/
	Vector2 <T> xw() const;		/**< Returns a vector with components of (x, w).*/
	Vector2 <T> yx() const;		/**< Returns a vector with components of (y, x).*/
	Vector2 <T> yy() const;		/**< Returns a vector with components of (y, y).*/
	Vector2 <T> yz() const;		/**< Returns a vector with components of (y, z).*/
	Vector2 <T> yw() const;		/**< Returns a vector with components of (y, w).*/
	Vector2 <T> zx() const;		/**< Returns a vector with components of (z, x).*/
	Vector2 <T> zy() const;		/**< Returns a vector with components of (z, y).*/
	Vector2 <T> zz() const;		/**< Returns a vector with components of (z, z).*/
	Vector2 <T> zw() const;		/**< Returns a vector with components of (z, w).*/
	Vector2 <T> wx() const;		/**< Returns a vector with components of (w, x).*/
	Vector2 <T> wy() const;		/**< Returns a vector with components of (w, y).*/
	Vector2 <T> wz() const;		/**< Returns a vector with components of (w, z).*/
	Vector2 <T> ww() const;		/**< Returns a vector with components of (w, w).*/
};

/** @brief Returns the product of the scalar r and the vector v.
 */
template <typename T> Vector4 <T> operator*( T r, const Vector4 <T> & v );

/** @brief Returns a vector containing the max of each corresponding component in vectors v1 and v2.
 */
template <typename T> Vector4 <T> vecMax( const Vector4 <T> & v1, const Vector4 <T> & v2 );

/** @brief Returns a vector containing the min of each corresponding component in vectors v1 and v2.
 */
template <typename T> Vector4 <T> vecMin( const Vector4 <T> & v1, const Vector4 <T> & v2 );

/** @brief Vector4 of type float.
 */
typedef Vector4 <float> Vector4f;

/** @brief Vector4 of type double.
 */
typedef Vector4 <double> Vector4d;

/** @brief Vector4 of type int.
 */
typedef Vector4 <int> Vector4i;

#endif