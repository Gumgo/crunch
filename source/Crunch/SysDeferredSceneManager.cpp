#include "SysDeferredSceneManager.h"
#include "SysRendererDeferredLighting.h"
#include "Context.h"

SceneObject::SceneObject() {
	enabled = true;
}

SceneLight::SceneLight() {
	enabled = true;
}

SysDeferredSceneManager::SysDeferredSceneManager() {
	camera = NULL;
	shadowMapPassKey = Renderer::makeKey( SysRendererDeferredLighting::SHADOW_MAP_PASS, (int)0 );
	shadowMapPassCall = Renderer::Call( this, &SysDeferredSceneManager::drawShadowMaps );
}

void SysDeferredSceneManager::setCamera( const SysCamera3D * cam ) {
	camera = cam;
}

size_t SysDeferredSceneManager::addObject( const SceneObject & sceneObject ) {
	return sceneObjects.add( sceneObject );
}

void SysDeferredSceneManager::removeObject( size_t index ) {
	sceneObjects.remove( index );
}

void SysDeferredSceneManager::updateObjectAabb( size_t index, const SysAabb & aabb ) {
	sceneObjects.get( index ).aabb = aabb;
}

void SysDeferredSceneManager::setObjectEnabled( size_t index, bool e ) {
	sceneObjects.get( index ).enabled = e;
}

size_t SysDeferredSceneManager::addLight( const SceneLight & sceneLight ) {
	return sceneLights.add( sceneLight );
}

void SysDeferredSceneManager::removeLight( size_t index ) {
	sceneLights.remove( index );
}

void SysDeferredSceneManager::lightUpdated( size_t index ) {
	// todo: something when lights are updated
}

void SysDeferredSceneManager::setLightEnabled( size_t index, bool e ) {
	sceneLights.get( index ).enabled = e;
}

void SysDeferredSceneManager::registerDrawCalls() {
	if (camera == NULL)
		return;

	// submit shadow pass
	Context::get().renderer.submit( shadowMapPassCall, shadowMapPassKey );

	// TEMPORARY: "naive" scene traversal

	Indexer <SceneObject>::Iterator oIt = sceneObjects.getIterator();
	while (oIt.next()) {
		SceneObject & o = oIt.get();
		if (o.enabled && !o.drawRegistrator.empty() && camera->getFrustum().contains( o.aabb ))
			oIt.get().drawRegistrator( camera );
	}

	Indexer <SceneLight>::Iterator lIt = sceneLights.getIterator();
	while (lIt.next()) {
		SceneLight & l = lIt.get();
		if (l.enabled) {
			l.light->setCamera( camera );
			Context::get().renderer.submit(
				Renderer::Call( l.light, &SysDeferredLight::draw ),
				Renderer::makeKey( SysRendererDeferredLighting::LIGHTING_PASS, (int)0 ) );
		}
	}
}

void SysDeferredSceneManager::drawShadowMaps() {
	Indexer <SceneLight>::Iterator lIt = sceneLights.getIterator();
	while (lIt.next()) {
		SceneLight & l = lIt.get();
		if (!l.enabled || !l.light->areShadowsEnabled())
			continue;

		struct ShadowReceiverIterator {
			SysDeferredSceneManager * mgr;
			Indexer <SceneObject>::Iterator aabbIt;

			const SysAabb * getNextAabb() {
				while (true) {
					bool end = !aabbIt.next();
					if (end) {
						aabbIt = mgr->sceneObjects.getIterator();
						return NULL;
					} else if (aabbIt.get().enabled)
						return &aabbIt.get().aabb;
				}
			}

			ShadowReceiverIterator( SysDeferredSceneManager * m )
				: aabbIt( m->sceneObjects.getIterator() ) {
				mgr = m;
			}
		} shadowReceiverIterator( this );

		lIt.get().light->computeShadowMapRange(
			fastdelegate::FastDelegate0 <const SysAabb*>( &shadowReceiverIterator, &ShadowReceiverIterator::getNextAabb ) );

		struct ShadowCasterIterator {
			SysDeferredSceneManager * mgr;
			Indexer <SceneObject>::Iterator aabbIt;
			Indexer <SceneObject>::Iterator objectIt;

			const SysAabb * getNextAabb() {
				while (true) {
					bool end = !aabbIt.next();
					if (end) {
						aabbIt = mgr->sceneObjects.getIterator();
						return NULL;
					} else if (aabbIt.get().enabled && !aabbIt.get().shadowMapRenderer.empty())
						return &aabbIt.get().aabb;
				}
			}

			SysDeferredLight::ShadowMapRenderer getNextObject() {
				while (true) {
					bool end = !objectIt.next();
					if (end) {
						objectIt = mgr->sceneObjects.getIterator();
						return NULL;
					} else if (objectIt.get().enabled && !objectIt.get().shadowMapRenderer.empty())
						return objectIt.get().shadowMapRenderer;
				}
			}

			ShadowCasterIterator( SysDeferredSceneManager * m )
				: aabbIt( m->sceneObjects.getIterator() )
				, objectIt( m->sceneObjects.getIterator() ) {
				mgr = m;
			}
		} shadowCasterIterator( this );

		lIt.get().light->drawShadowMap(
			fastdelegate::FastDelegate0 <const SysAabb*>( &shadowCasterIterator, &ShadowCasterIterator::getNextAabb ),
			fastdelegate::FastDelegate0 <SysDeferredLight::ShadowMapRenderer>( &shadowCasterIterator, &ShadowCasterIterator::getNextObject ) );
	}
}