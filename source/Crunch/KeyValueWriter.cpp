#include "KeyValueWriter.h"

KeyValueWriter::Value::Value() {
}

KeyValueWriter::Value::Value( Value && other ) {
	type = other.type;
	key.swap( other.key );
	switch (type) {
	case VAL_INT:
		intValue = other.intValue;
		break;
	case VAL_UINT:
		uintValue = other.uintValue;
		break;
	case VAL_FLOAT:
		floatValue = other.floatValue;
		break;
	case VAL_BOOL:
		boolValue = other.boolValue;
		break;
	case VAL_STRING:
		stringValue.swap( other.stringValue );
		break;
	case VAL_BINARY:
		binaryValue.swap( other.binaryValue );
		break;
	case VAL_ARRAY:
		arrayValue.swap( other.arrayValue );
		arrayMap.swap( other.arrayMap );
	}
}

KeyValueWriter::Value & KeyValueWriter::Value::operator=( Value && other ) {
	type = other.type;
	key.swap( other.key );
	switch (type) {
		case VAL_INT:
			intValue = other.intValue;
			break;
		case VAL_UINT:
			uintValue = other.uintValue;
			break;
		case VAL_FLOAT:
			floatValue = other.floatValue;
			break;
		case VAL_BOOL:
			boolValue = other.boolValue;
			break;
		case VAL_STRING:
			stringValue.swap( other.stringValue );
			break;
		case VAL_BINARY:
			binaryValue.swap( other.binaryValue );
			break;
		case VAL_ARRAY:
			arrayValue.swap( other.arrayValue );
			arrayMap.swap( other.arrayMap );
	}

	return *this;
}

void KeyValueWriter::checkKey( const std::string & key ) {
	bool valid = true;
	if (key.empty())
		valid = false;
	else {
		char ch = key.front();
		if ((ch < 'A' || ch > 'Z') &&
			(ch < 'a' || ch > 'z') &&
			(ch != '_'))
			valid = false;
		else {
			for (size_t i = 1; i < key.length(); ++i) {
				ch = key[i];
				if ((ch < 'A' || ch > 'Z') &&
					(ch < 'a' || ch > 'z') &&
					(ch < '0' || ch > '9') &&
					(ch != '_')) {
					valid = false;
					break;
				}
			}
		}
	}

	if (!valid)
		throw std::runtime_error( "Invalid key specified: " + key );
}

std::string KeyValueWriter::getEscapedString( const std::string & str ) {
	std::string ret;
	ret.reserve( (str.length() * 5) / 4 );

	for (size_t i = 0; i < str.length(); ++i) {
		char ch = str[i];
		if (ch >= 32 && ch <= 127) // covers symbols, numbers, and letters
			ret += ch;
		else if (ch == '\n')
			ret += "\\n";
		else if (ch == '\t')
			ret += "\\t";
		else if (ch == '\"')
			ret += "\\\"";
		else if (ch == '\\')
			ret += "\\\\";
		else {
			byte b = (byte)ch;
			static const char hex[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
			ret += "\\x";
			ret += hex[b / 16];
			ret += hex[b % 16];
		}
	}

	return ret;
}

KeyValueWriter::KeyValueWriter() {
}

KeyValueWriter::Iterator KeyValueWriter::getIterator() {
	return Iterator( root );
}

void KeyValueWriter::writeToFile( const std::string & fname ) const {
	std::ofstream out( fname );
	if (!out.is_open())
		throw std::runtime_error( "Failed to open file" );

	struct ValueWriteState {
		const Value * value;
		size_t position;
		size_t tab;
		ValueWriteState( const Value * v, size_t t )
			: value( v )
			, position( 0 )
			, tab( t ) {
		}

		void writeTabs( std::ofstream & out ) {
			for (size_t i = 0; i < tab; ++i)
				out << '\t';
		}
	};

	std::stack <ValueWriteState> arrayStack;
	// start recursively writing at root
	arrayStack.push( ValueWriteState( &root, 0 ) );

	while (true) {
		if (arrayStack.empty())
			break;
		ValueWriteState vws = arrayStack.top();
		arrayStack.pop();

		if (vws.position != 0) {
			// if not 0, that means a nested array just ended, so write closing brace
			vws.writeTabs( out );
			out << "}\n";
		}

		while (vws.position < vws.value->arrayValue.size()) {
			const Value & v = vws.value->arrayValue[vws.position];
			++vws.position;

			vws.writeTabs( out );
			if (v.key.empty())
				out << '*';
			else
				out << v.key << " = ";
			switch (v.type) {
			case VAL_INT:
				out << v.intValue;
				break;
			case VAL_UINT:
				out << v.uintValue;
				break;
			case VAL_BOOL:
				out << (v.boolValue ? "true" : "false");
				break;
			case VAL_FLOAT:
				out << boost::lexical_cast <std::string>( v.floatValue ); // use lexical cast to ensure precision is preserved
				break;
			case VAL_STRING:
				out << '\"' << getEscapedString( v.stringValue ) << '\"';
				break;
			case VAL_BINARY:
				out << '\'';
				for (size_t i = 0; i < v.binaryValue.size(); ++i) {
					byte b = v.binaryValue[i];
					static const char hex[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
					out << hex[b / 16] << hex[b % 16];
				}
				out << '\'';
				break;
			case VAL_ARRAY:
				out << '{';
				arrayStack.push( vws );
				arrayStack.push( ValueWriteState( &v, vws.tab + 1 ) );
				break;
			}
			out << '\n';

			if (v.type == VAL_ARRAY)
				break;
		}
	}

	if (out.fail())
		throw std::runtime_error( "Failed to write to file" );
}

KeyValueWriter::Iterator::Iterator( KeyValueWriter::Value & src )
	: source( &src ) {
}

KeyValueWriter::Iterator::Iterator( const KeyValueWriter::Iterator & it )
	: source( it.source ) {
}

KeyValueWriter::Iterator & KeyValueWriter::Iterator::operator=( const KeyValueWriter::Iterator & it ) {
	source = it.source;
	return *this;
}

void KeyValueWriter::Iterator::addKeyValue( const std::string & key ) {
	KeyValueWriter::checkKey( key );
	std::string upper = convertToUpper( key );
	std::map <std::string, size_t>::iterator it = source->arrayMap.find( upper );
	if (it != source->arrayMap.end())
		throw std::runtime_error( "Duplicate key specified: " + key );

	source->arrayMap.insert( std::make_pair( upper, source->arrayValue.size() ) );
	source->arrayValue.push_back( KeyValueWriter::Value() );
}

void KeyValueWriter::Iterator::setInt( const std::string & key, int value ) {
	addKeyValue( key );
	source->arrayValue.back().key = key;
	source->arrayValue.back().type = KeyValueWriter::VAL_INT;
	source->arrayValue.back().intValue = value;
}

void KeyValueWriter::Iterator::setInt( int value ) {
	source->arrayValue.push_back( KeyValueWriter::Value() );
	source->arrayValue.back().type = KeyValueWriter::VAL_INT;
	source->arrayValue.back().intValue = value;
}

void KeyValueWriter::Iterator::setUint( const std::string & key, uint value ) {
	addKeyValue( key );
	source->arrayValue.back().key = key;
	source->arrayValue.back().type = KeyValueWriter::VAL_UINT;
	source->arrayValue.back().uintValue = value;
}

void KeyValueWriter::Iterator::setUint( uint value ) {
	source->arrayValue.push_back( KeyValueWriter::Value() );
	source->arrayValue.back().type = KeyValueWriter::VAL_UINT;
	source->arrayValue.back().uintValue = value;
}

void KeyValueWriter::Iterator::setFloat( const std::string & key, float value ) {
	addKeyValue( key );
	source->arrayValue.back().key = key;
	source->arrayValue.back().type = KeyValueWriter::VAL_FLOAT;
	source->arrayValue.back().floatValue = value;
}

void KeyValueWriter::Iterator::setFloat( float value ) {
	source->arrayValue.push_back( KeyValueWriter::Value() );
	source->arrayValue.back().type = KeyValueWriter::VAL_FLOAT;
	source->arrayValue.back().floatValue = value;
}

void KeyValueWriter::Iterator::setBool( const std::string & key, bool value ) {
	addKeyValue( key );
	source->arrayValue.back().key = key;
	source->arrayValue.back().type = KeyValueWriter::VAL_BOOL;
	source->arrayValue.back().boolValue = value;
}

void KeyValueWriter::Iterator::setBool( bool value ) {
	source->arrayValue.push_back( KeyValueWriter::Value() );
	source->arrayValue.back().type = KeyValueWriter::VAL_BOOL;
	source->arrayValue.back().boolValue = value;
}

void KeyValueWriter::Iterator::setString( const std::string & key, const std::string & value ) {
	addKeyValue( key );
	source->arrayValue.back().key = key;
	source->arrayValue.back().type = KeyValueWriter::VAL_STRING;
	source->arrayValue.back().stringValue = value;
}

void KeyValueWriter::Iterator::setString( const std::string & value ) {
	source->arrayValue.push_back( KeyValueWriter::Value() );
	source->arrayValue.back().type = KeyValueWriter::VAL_STRING;
	source->arrayValue.back().stringValue = value;
}

void KeyValueWriter::Iterator::setBinaryData( const std::string & key, const byte * value, size_t length ) {
	addKeyValue( key );
	source->arrayValue.back().key = key;
	source->arrayValue.back().type = KeyValueWriter::VAL_BINARY;
	source->arrayValue.back().binaryValue.resize( length );
	std::copy( value, value + length, source->arrayValue.back().binaryValue.begin() );
}

void KeyValueWriter::Iterator::setBinaryData( const byte * value, size_t length ) {
	source->arrayValue.push_back( KeyValueWriter::Value() );
	source->arrayValue.back().type = KeyValueWriter::VAL_BINARY;
	source->arrayValue.back().binaryValue.resize( length );
	std::copy( value, value + length, source->arrayValue.back().binaryValue.begin() );
}

KeyValueWriter::Iterator KeyValueWriter::Iterator::setArray( const std::string & key ) {
	addKeyValue( key );
	source->arrayValue.back().key = key;
	source->arrayValue.back().type = KeyValueWriter::VAL_ARRAY;
	return KeyValueWriter::Iterator( source->arrayValue.back() );
}

KeyValueWriter::Iterator KeyValueWriter::Iterator::setArray() {
	source->arrayValue.push_back( KeyValueWriter::Value() );
	source->arrayValue.back().type = KeyValueWriter::VAL_ARRAY;
	return KeyValueWriter::Iterator( source->arrayValue.back() );
}