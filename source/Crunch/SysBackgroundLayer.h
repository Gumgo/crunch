#ifndef SYSBACKGROUNDLAYER_DEFINED
#define SYSBACKGROUNDLAYER_DEFINED

#include "Common.h"
#include "Actor.h"
#include "Renderer.h"
#include "TextureResource.h"
#include "SysCamera2D.h"

DEFINE_ACTOR( SysBackgroundLayer, 0, 0 )
public:
	void onCreate();
	void onDraw();

	struct BackgroundLayerData {
		int renderPriority;			// rendering priority of this background layer
		float parallaxMultiplier;	// parallax multiplier for this background layer
	};

	void init( const BackgroundLayerData & backgroundLayerData );
	void setCamera( const SysCamera2D * cam );
	void setBackground( TextureResourceReference & bg );

private:
	TextureResourceReference background;
	const SysCamera2D * camera;

	float parallaxMultiplier;

	void draw();
	Renderer::Call drawCall;
	Renderer::Key drawKey;
};

#endif