#include "Context.h"

Context * Context::context = NULL;

Context::ContextInitializer::ContextInitializer( Context * context ) {
	Context::context = context;
}

#pragma warning( push )
#pragma warning( disable : 4355 )
Context::Context( const Settings & s )
	: contextInitializer( this )
	, server( s.server )
	, resourceManager()
	, arguments( s.args )
	, taskQueue( s.taskQueueThreads ) {
#pragma warning( pop )
	quitSignaled = false;

	// call engine registration function
	registerCoreComponents();
	// call custom registration function
	if (!s.registrationFunction.empty())
		stateInitializer = s.registrationFunction();
	else
		Log::warning() << "No registration function set";

	try {
		KeyValueReader resources( s.resourcePathFile );
		resourceManager.registerResources( resources.getIterator() );
	} catch (const std::exception & e) {
		Log::error() << e.what();
		throw std::runtime_error( "Failed to read game resources" );
	}
}

Context & Context::get() {
	return *context;
}

bool Context::isServer() const {
	return server;
}

std::string Context::getArgument( size_t i ) const {
	if (i >= arguments.size())
		return "";
	else
		return arguments[i];
}

void Context::createInitialState() {
	if (!stateInitializer.stateCreator.empty())
		stateInitializer.stateCreator( *this );
}

void Context::signalQuit() {
	quitSignaled = true;
}

bool Context::getQuitSignal() const {
	return quitSignaled;
}

Context::StateInitializer::StateInitializer() {
}