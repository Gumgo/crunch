#include "SysDeferredTerrain.h"
#include "SysRendererDeferredLighting.h"
#include "Context.h"

SysDeferredTerrain::SysDeferredTerrain() {
	fillGBufferProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___terrain_fill_gbuffer", true );
	shadeProgram = Context::get().resourceManager.acquire <ProgramResource>( "sys___terrain_shade", true );

	GpuBufferReference noBuffer; // placeholder vertex buffer

	fillGBufferAuto.setProgram( fillGBufferProgram->getProgram() );
	fillGBufferAttribs.z = fillGBufferAuto.setAttribute( "z", noBuffer, 3, GL_FLOAT, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, z ) ) );
	fillGBufferAttribs.normalM0 = fillGBufferAuto.setAttribute( "normalM0", noBuffer, 2, GL_SHORT, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, normalM0 ) ) );
	fillGBufferAttribs.normalM1 = fillGBufferAuto.setAttribute( "normalM1", noBuffer, 2, GL_SHORT, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, normalM1 ) ) );
	fillGBufferAttribs.normalM2 = fillGBufferAuto.setAttribute( "normalM2", noBuffer, 2, GL_SHORT, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, normalM2 ) ) );
	fillGBufferAttribs.xyPatchEnabled = fillGBufferAuto.setAttribute( "xyPatchEnabled", noBuffer, 3, GL_UNSIGNED_BYTE, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, xyPatchEnabled ) ) );
	fillGBufferAttribs.materials1 = fillGBufferAuto.setAttribute( "materials1", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) ) );
	fillGBufferAttribs.materials2 = fillGBufferAuto.setAttribute( "materials2", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 4 ) );
	fillGBufferAttribs.materials3 = fillGBufferAuto.setAttribute( "materials3", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 8 ) );
	fillGBufferAttribs.materials4 = fillGBufferAuto.setAttribute( "materials4", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 12 ) );
	fillGBufferAttribs.materials5 = fillGBufferAuto.setAttribute( "materials5", noBuffer, 2, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 16 ) );
	fillGBufferAuto.setUniform( "modelViewMatrix", GL_FLOAT_MAT3x4, &uniforms.modelViewMatrix );
	fillGBufferAuto.setUniform( "mvpMatrix", GL_FLOAT_MAT4, &uniforms.mvpMatrix );
	fillGBufferAuto.setUniform( "scale", GL_FLOAT, &uniforms.scale );
	fillGBufferAuto.setUniform( "offset", GL_FLOAT_VEC2, &uniforms.offset );
	fillGBufferAuto.setUniform( "chunkScale", GL_FLOAT, &uniforms.chunkScale );
	fillGBufferAuto.setUniform( "center", GL_FLOAT_VEC2, &uniforms.center );
	fillGBufferAuto.setUniform( "morphDistances1", GL_FLOAT_VEC2, &uniforms.morphDistances1 );
	fillGBufferAuto.setUniform( "morphDistances2", GL_FLOAT_VEC2, &uniforms.morphDistances2 );
	fillGBufferAuto.setUniform( "materialsEnabled", GL_BOOL, &uniforms.materialsEnabled, 0, SysTerrain::MAX_MATERIALS );
	fillGBufferAuto.setUniform( "uvScales", GL_FLOAT, &uniforms.uvScales, 0, SysTerrain::MAX_MATERIALS );
	fillGBufferAuto.setUniform( "specularPowers", GL_FLOAT, &uniforms.specularPowers, 0, SysTerrain::MAX_MATERIALS );
	fillGBufferAuto.setUniform( "normalMaps", GL_SAMPLER_2D_ARRAY, &uniforms.normalMaps );
	uniforms.normalMaps = 0;

	shadeAuto.setProgram( shadeProgram->getProgram() );
	shadeAttribs.z = shadeAuto.setAttribute( "z", noBuffer, 3, GL_FLOAT, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, z ) ) );
	shadeAttribs.xyPatchEnabled = shadeAuto.setAttribute( "xyPatchEnabled", noBuffer, 3, GL_UNSIGNED_BYTE, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, xyPatchEnabled ) ) );
	shadeAttribs.materials1 = shadeAuto.setAttribute( "materials1", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) ) );
	shadeAttribs.materials2 = shadeAuto.setAttribute( "materials2", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 4 ) );
	shadeAttribs.materials3 = shadeAuto.setAttribute( "materials3", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 8 ) );
	shadeAttribs.materials4 = shadeAuto.setAttribute( "materials4", noBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 12 ) );
	shadeAttribs.materials5 = shadeAuto.setAttribute( "materials5", noBuffer, 2, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 16 ) );
	shadeAuto.setUniform( "mvpMatrix", GL_FLOAT_MAT4, &uniforms.mvpMatrix );
	shadeAuto.setUniform( "scale", GL_FLOAT, &uniforms.scale );
	shadeAuto.setUniform( "offset", GL_FLOAT_VEC2, &uniforms.offset );
	shadeAuto.setUniform( "chunkScale", GL_FLOAT, &uniforms.chunkScale );
	shadeAuto.setUniform( "center", GL_FLOAT_VEC2, &uniforms.center );
	shadeAuto.setUniform( "morphDistances1", GL_FLOAT_VEC2, &uniforms.morphDistances1 );
	shadeAuto.setUniform( "morphDistances2", GL_FLOAT_VEC2, &uniforms.morphDistances2 );
	shadeAuto.setUniform( "diffuseSpecMap", GL_SAMPLER_2D, &uniforms.diffuseSpecMap );
	shadeAuto.setUniform( "screenSize", GL_FLOAT_VEC2, &uniforms.screenSize );
	shadeAuto.setUniform( "materialsEnabled", GL_BOOL, &uniforms.materialsEnabled, 0, SysTerrain::MAX_MATERIALS );
	shadeAuto.setUniform( "uvScales", GL_FLOAT, &uniforms.uvScales, 0, SysTerrain::MAX_MATERIALS );
	shadeAuto.setUniform( "diffuseColors", GL_FLOAT_VEC3, &uniforms.diffuseColors, 0, SysTerrain::MAX_MATERIALS );
	shadeAuto.setUniform( "specularColors", GL_FLOAT_VEC3, &uniforms.specularColors, 0, SysTerrain::MAX_MATERIALS );
	shadeAuto.setUniform( "diffuseColorMaps", GL_SAMPLER_2D_ARRAY, &uniforms.diffuseColorMaps );
	uniforms.diffuseSpecMap = 1;
	uniforms.diffuseColorMaps = 2;

	gBufferBinder.bind( this, &SysDeferredTerrain::setGBufferProgram );
	shadeBinder.bind( this, &SysDeferredTerrain::setShadeProgram );

	camera = NULL;
	terrain = NULL;

	std::fill( uniforms.uvScales, uniforms.uvScales + SysTerrain::MAX_MATERIALS, 1.0f );
	std::fill( uniforms.specularPowers, uniforms.specularPowers + SysTerrain::MAX_MATERIALS, 1.0f );
	std::fill( uniforms.diffuseColors, uniforms.diffuseColors + SysTerrain::MAX_MATERIALS, Vector3f( 1.0f, 1.0f, 1.0f ) );
	std::fill( uniforms.specularColors, uniforms.specularColors + SysTerrain::MAX_MATERIALS, Vector3f( 1.0f, 1.0f, 1.0f ) );
}

SysDeferredTerrain::~SysDeferredTerrain() {
}

void SysDeferredTerrain::setCamera( const SysCamera3D * cam ) {
	camera = cam;
}

void SysDeferredTerrain::setTerrain( SysTerrain * t ) {
	terrain = t;
	uniforms.scale = t->getSettings().scale;
}

void SysDeferredTerrain::setMaterialTextures( GpuTextureReference & diffMaps, GpuTextureReference & normMaps ) {
	diffuseColorMaps = diffMaps;
	normalMaps = normMaps;
}

void SysDeferredTerrain::setMaterialProperties( size_t m, uint scalePow2, const Vector3f & diffuseColor, const Vector3f & specularColor, float specularPower ) {
	if (m > SysTerrain::MAX_MATERIALS)
		return;

	uniforms.uvScales[m] = 1.0f / (float)(1 << scalePow2);
	uniforms.diffuseColors[m] = diffuseColor;
	uniforms.specularColors[m] = specularColor;
	uniforms.specularPowers[m] = specularPower;
}

void SysDeferredTerrain::setGBufferProgram( size_t vertexBufferOffset, const SysTerrain::Chunk & chunk, const bool * materialsEnabled ) {
	// vertex buffer is currently bound
	GpuBufferReference vertexBuffer = Context::get().gpuState.getBoundBuffer( GL_ARRAY_BUFFER );

	fillGBufferAuto.alterAttribute( fillGBufferAttribs.z, vertexBuffer, 3, GL_FLOAT, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, z ) + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.normalM0, vertexBuffer, 2, GL_SHORT, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, normalM0 ) + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.normalM1, vertexBuffer, 2, GL_SHORT, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, normalM1 ) + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.normalM2, vertexBuffer, 2, GL_SHORT, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, normalM2 ) + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.xyPatchEnabled, vertexBuffer, 3, GL_UNSIGNED_BYTE, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, xyPatchEnabled ) + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.materials1, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.materials2, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 4 + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.materials3, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 8 + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.materials4, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 12 + vertexBufferOffset ) );
	fillGBufferAuto.alterAttribute( fillGBufferAttribs.materials5, vertexBuffer, 2, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 16 + vertexBufferOffset ) );

	normalMaps->bind( (size_t)uniforms.normalMaps );

	for (size_t i = 0; i < SysTerrain::MAX_MATERIALS; ++i)
		uniforms.materialsEnabled[i] = materialsEnabled[i] ? 1 : 0;

	// find the center
	Vector2i centerChunk = terrain->getCenter( 0 ).chunk.coords;
	uniforms.center = terrain->getCenter( 0 ).offset * terrain->getSettings().scale;
	// find this chunk's coords
	Vector2i thisChunk = terrain->convertLod( chunk.lod, chunk.coords, 0 );
	// this is the offset from the center
	Vector2i relativeCoords = thisChunk - centerChunk;
	// we will offset this chunk from the center and offset the whole terrain by center
	float sizeScale = (float)terrain->getSettings().chunkSize * terrain->getSettings().scale;
	uniforms.offset = (Vector2f)relativeCoords * sizeScale;
	uniforms.chunkScale = (float)(1 << chunk.lod);
	Vector2f terrainOffset = (Vector2f)centerChunk * sizeScale;

	// set morph distances
	uniforms.morphDistances1 = terrain->getMorphDistances( chunk.lod ) * terrain->getSettings().scale;
	uniforms.morphDistances2 = terrain->getMorphDistances( std::min( chunk.lod+1, terrain->getSettings().maxLod ) ) * terrain->getSettings().scale;

	Matrix44af modelMatrix;
	modelMatrix.translate( terrainOffset.x, terrainOffset.y, 0.0f );
	uniforms.modelViewMatrix = camera->getModelViewMatrix( modelMatrix );
	uniforms.mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

	fillGBufferAuto.bind();
}

void SysDeferredTerrain::setShadeProgram( size_t vertexBufferOffset, const SysTerrain::Chunk & chunk, const bool * materialsEnabled ) {
	// vertex buffer is currently bound
	GpuBufferReference vertexBuffer = Context::get().gpuState.getBoundBuffer( GL_ARRAY_BUFFER );

	shadeAuto.alterAttribute( shadeAttribs.z, vertexBuffer, 3, GL_FLOAT, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, z ) + vertexBufferOffset ) );
	shadeAuto.alterAttribute( shadeAttribs.xyPatchEnabled, vertexBuffer, 3, GL_UNSIGNED_BYTE, false, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, xyPatchEnabled ) + vertexBufferOffset ) );
	shadeAuto.alterAttribute( shadeAttribs.materials1, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + vertexBufferOffset ) );
	shadeAuto.alterAttribute( shadeAttribs.materials2, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 4 + vertexBufferOffset ) );
	shadeAuto.alterAttribute( shadeAttribs.materials3, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 8 + vertexBufferOffset ) );
	shadeAuto.alterAttribute( shadeAttribs.materials4, vertexBuffer, 4, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 12 + vertexBufferOffset ) );
	shadeAuto.alterAttribute( shadeAttribs.materials5, vertexBuffer, 2, GL_UNSIGNED_BYTE, true, false, sizeof( SysTerrain::ChunkVertex ),
		bufferOffset( offsetof( SysTerrain::ChunkVertex, materials ) + 16 + vertexBufferOffset ) );

	diffuseColorMaps->bind( (size_t)uniforms.diffuseColorMaps );

	for (size_t i = 0; i < SysTerrain::MAX_MATERIALS; ++i)
		uniforms.materialsEnabled[i] = materialsEnabled[i] ? 1 : 0;

	// find the center
	Vector2i centerChunk = terrain->getCenter( 0 ).chunk.coords;
	uniforms.center = terrain->getCenter( 0 ).offset * terrain->getSettings().scale;
	// find this chunk's coords
	Vector2i thisChunk = terrain->convertLod( chunk.lod, chunk.coords, 0 );
	// this is the offset from the center
	Vector2i relativeCoords = thisChunk - centerChunk;
	// we will offset this chunk from the center and offset the whole terrain by center
	float sizeScale = (float)terrain->getSettings().chunkSize * terrain->getSettings().scale;
	uniforms.offset = (Vector2f)relativeCoords * sizeScale;
	uniforms.chunkScale = (float)(1 << chunk.lod);
	Vector2f terrainOffset = (Vector2f)centerChunk * sizeScale;

	// set morph distances
	uniforms.morphDistances1 = terrain->getMorphDistances( chunk.lod ) * terrain->getSettings().scale;
	uniforms.morphDistances2 = terrain->getMorphDistances( std::min( chunk.lod+1, terrain->getSettings().maxLod ) ) * terrain->getSettings().scale;

	Matrix44af modelMatrix;
	modelMatrix.translate( terrainOffset.x, terrainOffset.y, 0.0f );
	uniforms.modelViewMatrix = camera->getModelViewMatrix( modelMatrix );
	uniforms.mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );
	uniforms.screenSize = (Vector2f)Context::get().gpuState.getCurrentViewportState().getViewportSize();

	shadeAuto.bind();
}

void SysDeferredTerrain::fillGBuffer() {
	terrain->prepareToDraw( camera );
	terrain->draw( gBufferBinder );
}

void SysDeferredTerrain::shade() {
	terrain->draw( shadeBinder );
}

void SysDeferredTerrain::registerDrawCalls( const SysCamera3D * cam ) {
	setCamera( cam );
	Context::get().renderer.submit(
		Renderer::Call( this, &SysDeferredTerrain::fillGBuffer ),
		Renderer::makeKey( SysRendererDeferredLighting::G_BUFFER_PASS, (int)1000 ) );
	Context::get().renderer.submit(
		Renderer::Call( this, &SysDeferredTerrain::shade ),
		Renderer::makeKey( SysRendererDeferredLighting::SHADING_PASS, (int)1000 ) );
}