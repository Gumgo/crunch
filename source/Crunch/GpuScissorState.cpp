#include "GpuScissorState.h"
#include "GpuState.h"

GpuScissorState::GpuScissorState()
	: GpuObject( NULL ) {
}

void GpuScissorState::bind() {
	getGpuState()->bind( *this );
}

void GpuScissorState::setScissorTestEnabled( bool e ) {
	scissorTestEnabled = e;
}

void GpuScissorState::setScissorBox( const Vector2i & position, const Vector2i & size ) {
	scissorBoxPosition = position;
	scissorBoxSize = size;
}

bool GpuScissorState::isScissorTestEnabled() const {
	return scissorTestEnabled;
}

Vector2i GpuScissorState::getScissorBoxPosition() const {
	return scissorBoxPosition;
}

Vector2i GpuScissorState::getScissorBoxSize() const {
	return scissorBoxSize;
}

GpuScissorState::GpuScissorState( GpuState * state )
	: GpuObject( state ) {
	scissorTestEnabled = false;
}

void GpuScissorState::apply( GpuScissorState & currentState ) const {
	setStateEnabled( scissorTestEnabled, currentState.scissorTestEnabled, GL_SCISSOR_TEST );

	if (scissorTestEnabled) {
		if (scissorBoxPosition != currentState.scissorBoxPosition ||
			scissorBoxSize != currentState.scissorBoxSize) {
			glScissor( scissorBoxPosition.x, scissorBoxPosition.y, scissorBoxSize.x, scissorBoxSize.y );
			currentState.scissorBoxPosition = scissorBoxPosition;
			currentState.scissorBoxSize = scissorBoxSize;
		}
	}
}

void GpuScissorState::getCurrentState() {
	scissorTestEnabled = glIsEnabled( GL_SCISSOR_TEST ) != 0;
	GLint scissorBoxResult[4];
	glGetIntegerv( GL_SCISSOR_BOX, scissorBoxResult );
	scissorBoxPosition.set( scissorBoxResult[0], scissorBoxResult[1] );
	scissorBoxSize.set( scissorBoxResult[2], scissorBoxResult[3] );
}