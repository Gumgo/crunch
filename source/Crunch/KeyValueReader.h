/** @file KeyValueReader.h
 *  @brief Provides functionality to read key/value pairs.
 */

#ifndef KEYVALUEREADER_DEFINED
#define KEYVALUEREADER_DEFINED

#include "Common.h"
#include <fstream>
#include <map>
#include <vector>
#include <stack>

/** @brief Provides functionality to read key/value pairs.
 *
 *  KeyValueReader will parse a file for a list of
 *  key/value pairs. Keys are not case sensitive.
 *  Each pair is specified in the following format:
 *
 *  key = value
 *
 *  Anonymous (unnamed) values may be specified by placing
 *  a * before the value; that is:
 *
 *  *value
 *
 *  The range of possible values includes integers, unsigned
 *  integers, booleans, floats, strings, binary data, and
 *  arrays. Strings must be surrounded by double quotes. The
 *  following are valid escape sequences for strings:
 *  - \\n (newline)
 *  - \\t (tab)
 *  - \\" (quote)
 *  - \\\\ (backslash)
 *  - \\x00 through \\xFF (ASCII character)
 *
 *  Binary data must be surrounded by single quotes and
 *  consists of hex data representing any number of bytes.
 *  Hex data consists of the characters '0' through '9' and
 *  'A' through 'F', where case does not matter and two
 *  characters represent a single byte. Zero-length binary
 *  data is allowed.
 *
 *  Arrays consist of a key, followed by the equals sign,
 *  followed by a sequence of key/value pairs surrounded
 *  in braces ({}). Arrays may be nested.
 *
 *  The # symbol is used to indicate that the rest of the
 *  line is a comment and should be ignored.
 *
 *  All numeric types can be interpreted as floats. Any
 *  numeric type without a decimal or exponent can be
 *  interpreted as an integer. Any numeric type that can
 *  be interpreted as an integer and is greater than or
 *  equal to 0 can be interpreted as an unsigned integer.
 *  No numeric values are explicitly stored as unsigned
 *  integers, however.
 */
class KeyValueReader {
public:
	class Iterator;
	friend class Iterator;

	/** @brief Indicates the value type of a key/value pair.
	 */
	enum ValueType {
		VAL_INT,	/**< A value of type int.*/
		VAL_UINT,	/**< A value of type unsigned int.*/
		VAL_FLOAT,	/**< A value of type float.*/
		VAL_BOOL,	/**< A value of type bool.*/
		VAL_STRING,	/**< A value of type string.*/
		VAL_BINARY,	/**< A value consisting of binary data.*/
		VAL_ARRAY	/**< A value consisting of an array of values.*/
	};

private:
	// declare private before public because the Iterator must access these

	// the value associated with a key
	struct Value {
		ValueType type;		// the type of value
		std::string key;	// the key associated with this value

		// a union is used for primitive types
		union {
			int intValue;			// the int value, if an int
			float floatValue;		// the float value, if a float
			bool boolValue;			// the bool value, if a bool
		};
		std::string stringValue;		// the string value, if a string
		std::vector <byte> binaryValue;	// the binary data, if binary

		std::vector <Value> arrayValue;				// the array of values, if an array
		std::map <std::string, size_t> arrayMap;	// maps keys to indices in the array of values, if an array

		Value();
		Value( Value && other );				// move constructor for efficient resizing of parent vector
		Value & operator=( Value && other );	// move assignment operator for efficient resizing of parent vector
	};

	// keeps track of the current read location in the file
	struct FilePos {
		int line;						// used to keep track of line number for reporting errors
		std::ifstream & in;				// the file input stream
		std::string currentLine;		// the current line in the file
		size_t linePos;					// the current position in the current line in the file
		FilePos( std::ifstream & i );	// the constructor
	};

	// used to indicate the type of token read.
	enum TokenType {
		TT_NONE,		// no token
		TT_OPERATOR,	// either *, =, {, or }
		TT_KEY,			// a named key containing letters, digits, and underscores
		TT_INT,			// an integer
		TT_FLOAT,		// a float
		TT_BOOLEAN,		// true of false
		TT_STRING,		// a string
		TT_BINARY		// binary data
	};

	// the root array of the file
	Value root;

	// returns the next token in the file
	std::pair <TokenType, std::string> getNextToken( FilePos & fp, std::vector <byte> & binaryBuffer );

public:
	/** @brief The constructor loads, parses, and closes the file.
	 */
	KeyValueReader( const std::string & fname );

	/** @brief Returns an iterator used to access the root level key/value pairs.
	 */
	Iterator getIterator() const;

	/** @brief Used to read the key/value pairs of the loaded key/value file.
	 */
	class Iterator {
	public:
		Iterator( const Iterator & it );				/**< Copy constructor.*/
		Iterator & operator=( const Iterator & it );	/**< Assignment operator.*/

		size_t getCount() const;				/**< Returns the number of key/value pairs.*/
		bool isAnonymous( size_t i ) const;		/**< Returns whether the <i>i</i>th value is anonymous.*/
		std::string getKey( size_t i ) const;	/**< Returns the key of the <i>i</i>th key/value pair, or the empty string if it is an anonymous value.*/

		KeyValueReader::ValueType getType( size_t i ) const;							/**< Returns the type of the <i>i</i>th value.*/
		KeyValueReader::ValueType getType( const std::string & key ) const;				/**< Returns the type of the value with the given key.*/
		bool isType( const std::string & key, KeyValueReader::ValueType type ) const;	/**< Returns whether the type if the value with the given key is of the given type.*/

		bool contains( const std::string & key ) const;									/**< Returns whether the given key exists.*/
		bool contains( const std::string & key, KeyValueReader::ValueType type ) const;	/**< Returns whether the given key with the given value type exists.*/

		int getInt( const std::string & key ) const;	/**< Returns the integer value with the given key.*/
		int getInt( size_t i ) const;					/**< Returns the integer value at index i.*/

		uint getUint( const std::string & key ) const;	/**< Returns the unsigned integer value with the given key.*/
		uint getUint( size_t i ) const;					/**< Returns the unsigned integer value at index i.*/

		float getFloat( const std::string & key ) const;	/**< Returns the float value with the given key.*/
		float getFloat( size_t i ) const;					/**< Returns the float value at index i.*/

		bool getBool( const std::string & key ) const;	/**< Returns the boolean value with the given key.*/
		bool getBool( size_t i ) const;					/**< Returns the boolean value at index i.*/

		std::string getString( const std::string & key ) const;	/**< Returns the string value with the given key.*/
		std::string getString( size_t i ) const;				/**< Returns the string value at index i.*/

		size_t getBinaryDataLength( const std::string & key ) const;	/**< Returns the length of the binary data with the given key.*/
		size_t getBinaryDataLength( size_t i ) const;					/**< Returns the length of the binary data at index i.*/
		const byte * getBinaryData( const std::string & key ) const;	/**< Returns a pointer to the binary data with the given key.*/
		const byte * getBinaryData( size_t i ) const;					/**< Returns a pointer to the binary data at index i.*/

		Iterator getArray( const std::string & key ) const;	/**< Returns an iterator to the array with the given key.*/
		Iterator getArray( size_t i ) const;				/**< Returns an iterator to the array at index i.*/

	private:
		friend class KeyValueReader;

		// a reference to the current value being read
		const KeyValueReader::Value * source;

		// private constructor for creation by KeyValueReader or Iterator only
		Iterator( const KeyValueReader::Value & src );

		// attempts to return the ith value
		const KeyValueReader::Value & getValue( size_t i ) const;

		// attempts to return the value with the given key
		const KeyValueReader::Value & getValue( const std::string & key ) const;
	};
};

#endif