#include "Common.h"

std::string convertToUpper( const std::string & str ) {
	std::string conv;
	for (size_t i = 0; i < str.length(); ++i)
		conv.push_back( toupper( str[i] ) );
	return conv;
}

std::string convertToLower( const std::string & str ) {
	std::string conv;
	for (size_t i = 0; i < str.length(); ++i)
		conv.push_back( tolower( str[i] ) );
	return conv;
}