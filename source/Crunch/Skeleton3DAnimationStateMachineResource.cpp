#include "Skeleton3DAnimationStateMachineResource.h"
#include "Log.h"

Skeleton3DAnimationStateMachineResource::Skeleton3DAnimationStateMachineResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m )
	, variableNameMap( 1 )
	, stateNameMap( 1 )
	, eventNameMap( 1 ) {
}

bool Skeleton3DAnimationStateMachineResource::loadDataAsync() {
	try {
		// open and load the file
		KeyValueReader in( getPath() );

		KeyValueReader::Iterator it = in.getIterator();

		// read variables
		KeyValueReader::Iterator variablesIt = it.getArray( "variables" );
		variableNameMap.resize( variablesIt.getCount() * 2 + 1 );
		variableTypes.reserve( variablesIt.getCount() );
		variableNameIndices.reserve( variablesIt.getCount() );
		for (size_t i = 0; i < variablesIt.getCount(); ++i) {
			std::string variableName = variablesIt.getKey( i );
			KeyValueReader::Iterator variableIt = variablesIt.getArray( i );
			std::string typeString = variableIt.getString( "type" );
			if (variableNameMap.contains( variableName ))
				throw std::runtime_error( "Duplicate variable " + variableName + " specified" );
			VariableType type;
			if (typeString == "FLOAT")
				type = VT_FLOAT;
			else if (typeString == "UINT")
				type = VT_UINT;
			else
				throw std::runtime_error( "Unknown variable type " + typeString + " specified" );

			variableNameIndices.push_back( variableNameMap.put( variableName, variableTypes.size() ) );
			variableTypes.push_back( type );
		}

		// reserve space for commands
		KeyValueReader::Iterator statesIt = it.getArray( "states" );
		KeyValueReader::Iterator eventsIt = it.getArray( "events" );
		size_t totalCommands = 0;
		// each command can point to multiple nodes
		for (size_t i = 0; i < statesIt.getCount(); ++i) {
			KeyValueReader::Iterator commandsIt = statesIt.getArray( i ).getArray( "commands" );
			for (size_t c = 0; c < commandsIt.getCount(); ++c) {
				KeyValueReader::Iterator commandIt = commandsIt.getArray( c );
				Command::Type commandType = getCommandType( commandIt );
				if (commandType != Command::T_STATE_SET_ACTIVE && commandType != Command::T_EVENT_TRIGGER) {
					if (commandIt.contains( "nodeName" ))
						++totalCommands;
					else
						totalCommands += commandIt.getArray( "nodeNames" ).getCount();
				}
			}
		}
		for (size_t i = 0; i < eventsIt.getCount(); ++i) {
			KeyValueReader::Iterator commandsIt = eventsIt.getArray( i ).getArray( "commands" );
			for (size_t c = 0; c < commandsIt.getCount(); ++c) {
				KeyValueReader::Iterator commandIt = commandsIt.getArray( c );
				Command::Type commandType = getCommandType( commandIt );
				if (commandType != Command::T_STATE_SET_ACTIVE && commandType != Command::T_EVENT_TRIGGER) {
					if (commandIt.contains( "nodeName" ))
						++totalCommands;
					else
						totalCommands += commandIt.getArray( "nodeNames" ).getCount();
				}
			}
		}
		commands.reserve( totalCommands );

		stateNameMap.resize( statesIt.getCount() * 2 + 1 );
		states.reserve( statesIt.getCount() );
		stateNameIndices.reserve( statesIt.getCount() );
		eventNameMap.resize( eventsIt.getCount() * 2 + 1 );
		events.reserve( eventsIt.getCount() );
		eventNameIndices.reserve( eventsIt.getCount() );

		// read state and event names first
		for (size_t i = 0; i < statesIt.getCount(); ++i) {
			KeyValueReader::Iterator stateIt = statesIt.getArray( i );
			std::string stateName = statesIt.getKey( i );
			if (stateNameMap.contains( stateName ))
				throw std::runtime_error( "Duplicate state " + stateName + " specified" );
			stateNameIndices.push_back( stateNameMap.put( stateName, i ) );
		}

		for (size_t i = 0; i < eventsIt.getCount(); ++i) {
			KeyValueReader::Iterator eventIt = eventsIt.getArray( i );
			std::string eventName = eventsIt.getKey( i );
			if (eventNameMap.contains( eventName ))
				throw std::runtime_error( "Duplicate event " + eventName + " specified" );
			eventNameIndices.push_back( eventNameMap.put( eventName, i ) );
		}

		// read states
		for (size_t i = 0; i < statesIt.getCount(); ++i) {
			KeyValueReader::Iterator stateIt = statesIt.getArray( i );
			std::string stateName = statesIt.getKey( i );

			size_t firstCommand = commands.size();

			// read commands
			KeyValueReader::Iterator commandsIt = stateIt.getArray( "commands" );
			for (size_t c = 0; c < commandsIt.getCount(); ++c) {
				KeyValueReader::Iterator commandIt = commandsIt.getArray( c );
				addCommand( commandIt );
			}

			states.push_back( State( firstCommand, commands.size() - firstCommand ) );
		}

		// read events
		for (size_t i = 0; i < eventsIt.getCount(); ++i) {
			KeyValueReader::Iterator eventIt = eventsIt.getArray( i );
			std::string eventName = eventsIt.getKey( i );

			size_t firstCommand = commands.size();

			// read commands
			KeyValueReader::Iterator commandsIt = eventIt.getArray( "commands" );
			for (size_t c = 0; c < commandsIt.getCount(); ++c) {
				KeyValueReader::Iterator commandIt = commandsIt.getArray( c );
				addCommand( commandIt );
			}

			events.push_back( Event( firstCommand, commands.size() - firstCommand ) );
		}

		return true;
	} catch (const std::exception & e) {
		Log::error() << e.what();
		return false;
	}
}

void Skeleton3DAnimationStateMachineResource::addCommand( const KeyValueReader::Iterator & it ) {
	// get command type
	Command::Type commandType = getCommandType( it );

	// process the command
	// if the command has "nodeName", add just one
	// if it had "nodeNames", add a command for each name
	switch (commandType) {
	case Command::T_ANIMATION_PLAY:
		{
			AnimationPlayCommand cmd;
			cmd.init(
				it.getBool( "loop" ),
				getFloatValue( it, "speed" ) );

			KeyValueReader::Iterator nodeNamesIt = it.getArray( "nodeNames" );
			for (size_t n = 0; n < nodeNamesIt.getCount(); ++n)
				commands.push_back( Command( nodeNamesIt.getString( n ), cmd ) );
		}
		break;
	case Command::T_ANIMATION_SET_TIME:
		{
			AnimationSetTimeCommand cmd;
			cmd.init(
				getFloatValue( it, "time" ) );

			KeyValueReader::Iterator nodeNamesIt = it.getArray( "nodeNames" );
			for (size_t n = 0; n < nodeNamesIt.getCount(); ++n)
				commands.push_back( Command( nodeNamesIt.getString( n ), cmd ) );
		}
		break;
	case Command::T_SELECT_SET_INDEX:
		{
			SelectSetIndexCommand cmd;
			cmd.init(
				getUintValue( it, "index" ) );

			KeyValueReader::Iterator nodeNamesIt = it.getArray( "nodeNames" );
			for (size_t n = 0; n < nodeNamesIt.getCount(); ++n)
				commands.push_back( Command( nodeNamesIt.getString( n ), cmd ) );
		}
		break;
	case Command::T_BLEND_TRANSITION_WEIGHT:
		{
			BlendTransitionWeightCommand cmd;
			cmd.init(
				getFloatValue( it, "weight" ),
				getFloatValue( it, "speed" ) );

			KeyValueReader::Iterator nodeNamesIt = it.getArray( "nodeNames" );
			for (size_t n = 0; n < nodeNamesIt.getCount(); ++n)
				commands.push_back( Command( nodeNamesIt.getString( n ), cmd ) );
		}
		break;
	case Command::T_BLEND_SET_WEIGHT:
		{
			BlendSetWeightCommand cmd;
			cmd.init(
				getFloatValue( it, "weight" ) );

			KeyValueReader::Iterator nodeNamesIt = it.getArray( "nodeNames" );
			for (size_t n = 0; n < nodeNamesIt.getCount(); ++n)
				commands.push_back( Command( nodeNamesIt.getString( n ), cmd ) );
		}
		break;
	case Command::T_SEQUENCE_TRANSITION_WEIGHT:
		{
			SequenceTransitionWeightCommand cmd;
			cmd.init(
				it.getUint( "index" ),
				getFloatValue( it, "weight" ),
				getFloatValue( it, "speed" ) );

			KeyValueReader::Iterator nodeNamesIt = it.getArray( "nodeNames" );
			for (size_t n = 0; n < nodeNamesIt.getCount(); ++n)
				commands.push_back( Command( nodeNamesIt.getString( n ), cmd ) );
		}
		break;
	case Command::T_SEQUENCE_SET_WEIGHT:
		{
			SequenceSetWeightCommand cmd;
			cmd.init(
				it.getUint( "index" ),
				getFloatValue( it, "weight" ) );

			KeyValueReader::Iterator nodeNamesIt = it.getArray( "nodeNames" );
			for (size_t n = 0; n < nodeNamesIt.getCount(); ++n)
				commands.push_back( Command( nodeNamesIt.getString( n ), cmd ) );
		}
		break;
	case Command::T_STATE_SET_ACTIVE:
		{
			bool active = it.getBool( "active" );

			KeyValueReader::Iterator stateNamesIt = it.getArray( "stateNames" );
			for (size_t n = 0; n < stateNamesIt.getCount(); ++n) {
				std::string stateName = stateNamesIt.getString( n );
				size_t stateIndex;
				if (!stateNameMap.get( stateName, stateIndex ))
					throw std::runtime_error( "Invalid state " + stateName );
				StateSetActiveCommand cmd;
				cmd.init( stateIndex, active );
				commands.push_back( Command( cmd ) );
			}
		}
		break;
	case Command::T_EVENT_TRIGGER:
		{
			KeyValueReader::Iterator eventNamesIt = it.getArray( "eventNames" );
			for (size_t n = 0; n < eventNamesIt.getCount(); ++n) {
				std::string eventName = eventNamesIt.getString( n );
				size_t eventIndex;
				if (!eventNameMap.get( eventName, eventIndex ))
					throw std::runtime_error( "Invalid event " + eventName );
				EventTriggerCommand cmd;
				cmd.init( eventIndex );
				commands.push_back( Command( cmd ) );
			}
		}
		break;
	default:
		assert( false );
		// we can't return an empty command, so just throw
		throw std::runtime_error( "Invalid command" );
	} 
}

Skeleton3DAnimationStateMachineResource::Command::Type Skeleton3DAnimationStateMachineResource::getCommandType( const KeyValueReader::Iterator & it ) {
	std::string commandTypeString = it.getString( "type" );
	static const char * COMMAND_TYPE_STRINGS[Command::TYPE_COUNT] = { 
		"ANIMATION_PLAY",
		"ANIMATION_SET_TIME",
		"SELECT_SET_INDEX",
		"BLEND_TRANSITION_WEIGHT",
		"BLEND_SET_WEIGHT",
		"SEQUENCE_TRANSITION_WEIGHT",
		"SEQUENCE_SET_WEIGHT",
		"STATE_SET_ACTIVE",
		"EVENT_TRIGGER"
	};
	size_t s;
	for (s = 0; s < arraySize( COMMAND_TYPE_STRINGS ); ++s) {
		if (commandTypeString == COMMAND_TYPE_STRINGS[s])
			break;
	}
	if (s == arraySize( COMMAND_TYPE_STRINGS ))
		throw std::runtime_error( "Invalid command type " + commandTypeString );
	return (Command::Type)s;
}

Skeleton3DAnimationStateMachineResource::FloatValue Skeleton3DAnimationStateMachineResource::getFloatValue( const KeyValueReader::Iterator & it, const std::string & name ) const {
	FloatValue v;
	if (it.contains( name, KeyValueReader::VAL_STRING )) {
		std::string variableName = it.getString( name );
		size_t variableIndex;
		if (!variableNameMap.get( variableName, variableIndex ))
			throw std::runtime_error( "Variable " + variableName + " used but not declared" );
		if (variableTypes[variableIndex] != VT_FLOAT)
			throw std::runtime_error( "Variable " + variableName + " is not a float" );
		v.setVariableIndex( variableIndex );
	} else
		v.setConstantValue( it.getFloat( name ) );
	return v;
}

Skeleton3DAnimationStateMachineResource::UintValue Skeleton3DAnimationStateMachineResource::getUintValue( const KeyValueReader::Iterator & it, const std::string & name ) const {
	UintValue v;
	if (it.contains( name, KeyValueReader::VAL_STRING )) {
		std::string variableName = it.getString( name );
		size_t variableIndex;
		if (!variableNameMap.get( variableName, variableIndex ))
			throw std::runtime_error( "Variable " + variableName + " used but not declared" );
		if (variableTypes[variableIndex] != VT_UINT)
			throw std::runtime_error( "Variable " + variableName + " is not an unsigned int" );
		v.setVariableIndex( variableIndex );
	} else
		v.setConstantValue( it.getUint( name ) );
	return v;
}

bool Skeleton3DAnimationStateMachineResource::loadDataSync() {
	return true;
}

void Skeleton3DAnimationStateMachineResource::freeData() {
}

size_t Skeleton3DAnimationStateMachineResource::getVariableCount() const {
	return variableTypes.size();
}

Skeleton3DAnimationStateMachineResource::VariableType Skeleton3DAnimationStateMachineResource::getVariableType( size_t i ) const {
	return variableTypes[i];
}

bool Skeleton3DAnimationStateMachineResource::getVariableIndex( const std::string & name, size_t & index ) const {
	return variableNameMap.get( name, index );
}

std::string Skeleton3DAnimationStateMachineResource::getVariableName( size_t index ) const {
	return variableNameMap.getKey( variableNameIndices[index] );
}

bool Skeleton3DAnimationStateMachineResource::AnimationPlayCommand::getLoop() const {
	return loop;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::AnimationPlayCommand::getSpeed() const {
	return speed;
}

void Skeleton3DAnimationStateMachineResource::AnimationPlayCommand::init( bool l, const FloatValue & s ) {
	loop = l;
	speed = s;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::AnimationSetTimeCommand::getTime() const {
	return time;
}

void Skeleton3DAnimationStateMachineResource::AnimationSetTimeCommand::init( const FloatValue & t ) {
	time = t;
}

const Skeleton3DAnimationStateMachineResource::UintValue & Skeleton3DAnimationStateMachineResource::SelectSetIndexCommand::getIndex() const {
	return index;
}

void Skeleton3DAnimationStateMachineResource::SelectSetIndexCommand::init( const UintValue & i ) {
	index = i;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::BlendTransitionWeightCommand::getWeight() const {
	return weight;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::BlendTransitionWeightCommand::getSpeed() const {
	return speed;
}

void Skeleton3DAnimationStateMachineResource::BlendTransitionWeightCommand::init( const FloatValue & w, const FloatValue & s ) {
	weight = w;
	speed = s;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::BlendSetWeightCommand::getWeight() const {
	return weight;
}

void Skeleton3DAnimationStateMachineResource::BlendSetWeightCommand::init( const FloatValue & w ) {
	weight = w;
}

uint Skeleton3DAnimationStateMachineResource::SequenceTransitionWeightCommand::getIndex() const {
	return index;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::SequenceTransitionWeightCommand::getWeight() const {
	return weight;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::SequenceTransitionWeightCommand::getSpeed() const {
	return speed;
}

void Skeleton3DAnimationStateMachineResource::SequenceTransitionWeightCommand::init( uint i, const FloatValue & w, const FloatValue & s ) {
	index = i;
	weight = w;
	speed = s;
}

uint Skeleton3DAnimationStateMachineResource::SequenceSetWeightCommand::getIndex() const {
	return index;
}

const Skeleton3DAnimationStateMachineResource::FloatValue & Skeleton3DAnimationStateMachineResource::SequenceSetWeightCommand::getWeight() const {
	return weight;
}

void Skeleton3DAnimationStateMachineResource::SequenceSetWeightCommand::init( uint i, const FloatValue & w ) {
	index = i;
	weight = w;
}

size_t Skeleton3DAnimationStateMachineResource::StateSetActiveCommand::getStateIndex() const {
	return stateIndex;
}

bool Skeleton3DAnimationStateMachineResource::StateSetActiveCommand::getActive() const {
	return active;
}

void Skeleton3DAnimationStateMachineResource::StateSetActiveCommand::init( size_t i, bool a ) {
	stateIndex = i;
	active = a;
}

size_t Skeleton3DAnimationStateMachineResource::EventTriggerCommand::getEventIndex() const {
	return eventIndex;
}

void Skeleton3DAnimationStateMachineResource::EventTriggerCommand::init( size_t i ) {
	eventIndex = i;
}

const std::string & Skeleton3DAnimationStateMachineResource::Command::getNodeName() const {
	return nodeName;
}

Skeleton3DAnimationStateMachineResource::Command::Type Skeleton3DAnimationStateMachineResource::Command::getType() const {
	return type;
}

const Skeleton3DAnimationStateMachineResource::AnimationPlayCommand & Skeleton3DAnimationStateMachineResource::Command::getAnimationPlayCommand() const {
	if (type != T_ANIMATION_PLAY)
		throw std::runtime_error( "Command is not an animation play command" );
	return animationPlayCommand;
}

const Skeleton3DAnimationStateMachineResource::AnimationSetTimeCommand & Skeleton3DAnimationStateMachineResource::Command::getAnimationSetTimeCommand() const {
	if (type != T_ANIMATION_SET_TIME)
		throw std::runtime_error( "Command is not an animation set time command" );
	return animationSetTimeCommand;
}

const Skeleton3DAnimationStateMachineResource::SelectSetIndexCommand & Skeleton3DAnimationStateMachineResource::Command::getSelectSetIndexCommand() const {
	if (type != T_SELECT_SET_INDEX)
		throw std::runtime_error( "Command is not a select set index command" );
	return selectSetIndexCommand;
}

const Skeleton3DAnimationStateMachineResource::BlendTransitionWeightCommand & Skeleton3DAnimationStateMachineResource::Command::getBlendTransitionWeightCommand() const {
	if (type != T_BLEND_TRANSITION_WEIGHT)
		throw std::runtime_error( "Command is not a blend transition weight command" );
	return blendTransitionWeightCommand;
}

const Skeleton3DAnimationStateMachineResource::BlendSetWeightCommand & Skeleton3DAnimationStateMachineResource::Command::getBlendSetWeightCommand() const {
	if (type != T_BLEND_SET_WEIGHT)
		throw std::runtime_error( "Command is not a blend set weight command" );
	return blendSetWeightCommand;
}

const Skeleton3DAnimationStateMachineResource::SequenceTransitionWeightCommand & Skeleton3DAnimationStateMachineResource::Command::getSequenceTransitionWeightCommand() const {
	if (type != T_SEQUENCE_TRANSITION_WEIGHT)
		throw std::runtime_error( "Command is not a sequence transition weight command" );
	return sequenceTransitionWeightCommand;
}

const Skeleton3DAnimationStateMachineResource::SequenceSetWeightCommand & Skeleton3DAnimationStateMachineResource::Command::getSequenceSetWeightCommand() const {
	if (type != T_SEQUENCE_SET_WEIGHT)
		throw std::runtime_error( "Command is not a sequence set weight command" );
	return sequenceSetWeightCommand;
}

const Skeleton3DAnimationStateMachineResource::StateSetActiveCommand & Skeleton3DAnimationStateMachineResource::Command::getStateSetActiveCommand() const {
	if (type != T_STATE_SET_ACTIVE)
		throw std::runtime_error( "Command is not a state set active command" );
	return stateSetActiveCommand;
}

const Skeleton3DAnimationStateMachineResource::EventTriggerCommand & Skeleton3DAnimationStateMachineResource::Command::getEventTriggerCommand() const {
	if (type != T_EVENT_TRIGGER)
		throw std::runtime_error( "Command is not an event trigger command" );
	return eventTriggerCommand;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const std::string & nName, const AnimationPlayCommand & c )
	: nodeName( nName ) {
	type = T_ANIMATION_PLAY;
	animationPlayCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const std::string & nName, const AnimationSetTimeCommand & c )
	: nodeName( nName ) {
	type = T_ANIMATION_SET_TIME;
	animationSetTimeCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const std::string & nName, const SelectSetIndexCommand & c )
	: nodeName( nName ) {
	type = T_SELECT_SET_INDEX;
	selectSetIndexCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const std::string & nName, const BlendTransitionWeightCommand & c )
	: nodeName( nName ) {
	type = T_BLEND_TRANSITION_WEIGHT;
	blendTransitionWeightCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const std::string & nName, const BlendSetWeightCommand & c )
	: nodeName( nName ) {
	type = T_BLEND_SET_WEIGHT;
	blendSetWeightCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const std::string & nName, const SequenceTransitionWeightCommand & c )
	: nodeName( nName ) {
	type = T_SEQUENCE_TRANSITION_WEIGHT;
	sequenceTransitionWeightCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const std::string & nName, const SequenceSetWeightCommand & c )
	: nodeName( nName ) {
	type = T_SEQUENCE_SET_WEIGHT;
	sequenceSetWeightCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const StateSetActiveCommand & c )
	: nodeName( "" ) {
	type = T_STATE_SET_ACTIVE;
	stateSetActiveCommand = c;
}

Skeleton3DAnimationStateMachineResource::Command::Command( const EventTriggerCommand & c )
	: nodeName( "" ) {
	type = T_EVENT_TRIGGER;
	eventTriggerCommand = c;
}

size_t Skeleton3DAnimationStateMachineResource::getCommandCount() const {
	return commands.size();
}

const Skeleton3DAnimationStateMachineResource::Command & Skeleton3DAnimationStateMachineResource::getCommand( size_t i ) const {
	return commands[i];
}

size_t Skeleton3DAnimationStateMachineResource::State::getFirstCommandIndex() const {
	return firstCommandIndex;
}

size_t Skeleton3DAnimationStateMachineResource::State::getCommandCount() const {
	return commandCount;
}

Skeleton3DAnimationStateMachineResource::State::State( size_t first, size_t count ) {
	firstCommandIndex = first;
	commandCount = count;
}

size_t Skeleton3DAnimationStateMachineResource::getStateCount() const {
	return states.size();
}

const Skeleton3DAnimationStateMachineResource::State & Skeleton3DAnimationStateMachineResource::getState( size_t i ) const {
	return states[i];
}

bool Skeleton3DAnimationStateMachineResource::getStateIndex( const std::string & name, size_t & index ) const {
	return stateNameMap.get( name, index );
}

std::string Skeleton3DAnimationStateMachineResource::getStateName( size_t index ) const {
	return stateNameMap.getKey( stateNameIndices[index] );
}

size_t Skeleton3DAnimationStateMachineResource::Event::getFirstCommandIndex() const {
	return firstCommandIndex;
}

size_t Skeleton3DAnimationStateMachineResource::Event::getCommandCount() const {
	return commandCount;
}

Skeleton3DAnimationStateMachineResource::Event::Event( size_t first, size_t count ) {
	firstCommandIndex = first;
	commandCount = count;
}

size_t Skeleton3DAnimationStateMachineResource::getEventCount() const {
	return events.size();
}

const Skeleton3DAnimationStateMachineResource::Event & Skeleton3DAnimationStateMachineResource::getEvent( size_t i ) const {
	return events[i];
}

bool Skeleton3DAnimationStateMachineResource::getEventIndex( const std::string & name, size_t & index ) const {
	return eventNameMap.get( name, index );
}

std::string Skeleton3DAnimationStateMachineResource::getEventName( size_t index ) const {
	return eventNameMap.getKey( eventNameIndices[index] );
}