#include "GpuDrawBufferState.h"
#include "GpuState.h"

GpuDrawBufferState::GpuDrawBufferState()
	: GpuObject( NULL ) {
}

void GpuDrawBufferState::bind() {
	getGpuState()->bind( *this );
}

bool GpuDrawBufferState::setDrawBuffers( size_t count, const GLenum * buffers ) {
	if (count > getGpuState()->getMaxDrawBuffers())
		return false;
	drawBufferCount = count;
	std::copy( buffers, buffers + count, drawBuffers );
	return true;
}

size_t GpuDrawBufferState::getDrawBufferCount() const {
	return drawBufferCount;
}

const GLenum * GpuDrawBufferState::getDrawBuffers() const {
	return drawBuffers;
}

GpuDrawBufferState::GpuDrawBufferState( GpuState * state )
	: GpuObject( state ) {
}

void GpuDrawBufferState::apply( GpuDrawBufferState & currentState ) const {
	if (drawBufferCount == currentState.drawBufferCount) {
		bool allEqual = true;
		for (size_t i = 0; i < drawBufferCount; ++i) {
			if (drawBuffers[i] != currentState.drawBuffers[i]) {
				allEqual = false;
				break;
			}
		}
		if (allEqual)
			return;
	}

	glDrawBuffers( (uint)drawBufferCount, drawBuffers );
	currentState.drawBufferCount = drawBufferCount;
	std::copy( drawBuffers, drawBuffers + drawBufferCount, currentState.drawBuffers );
}

void GpuDrawBufferState::getCurrentState() {
	for (size_t i = 0; i < getGpuState()->getMaxDrawBuffers(); ++i)
		glGetIntegerv( GL_DRAW_BUFFER0 + i, (GLint*)(drawBuffers + i) );
	// compute the number of draw buffers by starting at the end and finding the first one that isn't GL_NONE
	drawBufferCount = getGpuState()->getMaxDrawBuffers();
	for (size_t i = 0; i < getGpuState()->getMaxDrawBuffers(); ++i) {
		size_t t = getGpuState()->getMaxDrawBuffers() - i - 1;
		if (drawBuffers[t] == GL_NONE)
			--drawBufferCount;
		else
			break;
	}
}