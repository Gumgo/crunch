/** @file Actor.h
 *  @brief Contains the base object for an actor.
 */

#ifndef ACTOR_DEFINED
#define ACTOR_DEFINED

#include "Common.h"
#include <vector>

/** @brief The base class actors are derived from.
 */
class Actor : private boost::noncopyable {
public:
	Actor( int updatePrior, int renderPrior, size_t idx, size_t gsIdx );	/**< The constructor.*/
	virtual ~Actor();														/**< The virtual destructor.*/

	int getUpdatePriority() const;		/**< Returns the update priority.*/
	int getRenderPriority() const;		/**< Returns the render priority.*/
	size_t getIndex() const;			/**< Returns the index of this actor.*/
	size_t getGameStateIndex() const;	/**< Returns the index of this actor.*/
	bool isActive() const;				/**< Returns whether the actor is active.*/
	bool isVisible() const;				/**< Returns whether the actor is visible.*/
	void activate();					/**< Activates the actor.*/
	void deactivate();					/**< Deactivates the actor.*/
	void hide();						/**< Hides the actor.*/
	void show();						/**< Shows the actor.*/
	void destroy();						/**< Destroys the actor.*/

	virtual void onCreate();		/**< The onCreate event.*/
	virtual void onUpdate();		/**< The onUpdate event.*/
	virtual void onDestroy();		/**< The onDestroy event.*/
	virtual void onDraw();			/**< The onDraw event.*/
	virtual void onActivate();		/**< The onActivate event.*/
	virtual void onDeactivate();	/**< The onDeactivate event.*/

private:
	friend class GameState;
	int updatePriority;		// priority in the update list
	int renderPriority;		// priority in the render list
	size_t index;			// index of this actor
	size_t gameStateIndex;	// index of the game state
};

/** @brief Defines an actor with the given name, update priority, and render priority.
 */
#define DEFINE_ACTOR( name, updatePriority, renderPriority )			\
class name : public Actor {												\
public:																	\
	static int getDefaultUpdatePriority() {								\
		return (updatePriority);										\
	}																	\
																		\
	static int getDefaultRenderPriority() {								\
		return (renderPriority);										\
	}																	\
																		\
protected:																\
	friend class GameState;												\
	name( int updatePrior, int renderPrior, size_t idx, size_t gsIdx )	\
		: Actor( updatePrior, renderPrior, idx, gsIdx ) {				\
	}																	\
																		\
private:

/** @brief Defines an actor derived from parent with the given name, update priority, and render priority.
 */
#define DEFINE_DERIVED_ACTOR( name, parent, updatePriority, renderPriority )	\
class name : public parent {													\
public:																			\
	static int getDefaultUpdatePriority() {										\
		return (updatePriority);												\
	}																			\
																				\
	static int getDefaultRenderPriority() {										\
		return (renderPriority);												\
	}																			\
																				\
protected:																		\
	friend class GameState;														\
	name( int updatePrior, int renderPrior, size_t idx, size_t gsIdx )			\
		: parent( updatePrior, renderPrior, idx, gsIdx ) {						\
	}																			\
																				\
private:

#endif