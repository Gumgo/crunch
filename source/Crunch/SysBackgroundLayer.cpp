#include "SysBackgroundLayer.h"
#include "Context.h"
#include "SysRenderer2D.h"

void SysBackgroundLayer::onCreate() {
	camera = NULL;
	drawCall.bind( this, &SysBackgroundLayer::draw );
}

void SysBackgroundLayer::onDraw() {
	if (camera != NULL && background)
		Context::get().renderer.submit( drawCall, drawKey );
}

void SysBackgroundLayer::draw() {
	// find the corners of the view in world space
	float left = camera->getLeft();
	float right = camera->getRight();
	float bottom = camera->getBottom();
	float top = camera->getTop();
	Vector2f tl = camera->getCameraMatrix().transformPoint( Vector2f( left, top ) );
	Vector2f tr = camera->getCameraMatrix().transformPoint( Vector2f( right, top ) );
	Vector2f bl = camera->getCameraMatrix().transformPoint( Vector2f( left, bottom) );
	Vector2f br = camera->getCameraMatrix().transformPoint( Vector2f( right, bottom ) );
	// find a bounding rectangle
	Vector2f cornerMin = vecMin( vecMin( tl, tr ), vecMin( bl, br ) );
	Vector2f cornerMax = vecMax( vecMax( tl, tr ), vecMax( bl, br ) );

	// get the camera's offset from the translation component of the camera matrix
	Vector2f cameraOffset( camera->getCameraMatrix()[2], camera->getCameraMatrix()[5] );
	Vector2f pixelOffset = cameraOffset * parallaxMultiplier;
	Vector2f pixelMin = cornerMin - pixelOffset;
	Vector2f pixelMax = cornerMax - pixelOffset;

	Matrix33af modelMatrix;
	modelMatrix.translate( cornerMin );
	Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

	GpuBlendState blendState = Context::get().gpuState.createBlendState();
	blendState.setBlendFunction( GpuBlendState::BM_ALPHA );
	blendState.bind();

	SysRenderer2D::get().drawSprite( *background, mvpMatrix, Vector2f(), pixelMin, pixelMax );
}

void SysBackgroundLayer::init( const BackgroundLayerData & backgroundLayerData ) {
	parallaxMultiplier = backgroundLayerData.parallaxMultiplier;
	drawKey = Renderer::makeKey( SysRenderer2D::PASS, backgroundLayerData.renderPriority );
}

void SysBackgroundLayer::setCamera( const SysCamera2D * cam ) {
	camera = cam;
}

void SysBackgroundLayer::setBackground( TextureResourceReference & bg ) {
	background = bg;
}