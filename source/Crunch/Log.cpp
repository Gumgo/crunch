#include "Log.h"

std::vector <std::ostream*> * Log::outputs = NULL;
boost::mutex Log::logMutex;

LogMessage::LogMessage( const std::string & header ) {
	Log::logMutex.lock();
	*this << header;
}

LogMessage::~LogMessage() {
	*this << '\n';
	Log::logMutex.unlock();
}

void Log::createStreamList() {
	boost::unique_lock <boost::mutex> lock( logMutex );
	if (outputs == NULL)
		outputs = new std::vector <std::ostream*>();
}

void Log::addStream( std::ostream & stream ) {
	boost::unique_lock <boost::mutex> lock( logMutex );
	if (outputs != NULL)
		outputs->push_back( &stream );
}

void Log::removeStream( std::ostream & stream ) {
	boost::unique_lock <boost::mutex> lock( logMutex );
	if (outputs != NULL) {
		for (size_t i = 0; i < outputs->size(); ++i) {
			if ((*outputs)[i] == &stream) {
				outputs->erase( outputs->begin() + i );
				return;
			}
		}
	}
}

LogMessage Log::info() {
	return LogMessage( "" );
}

LogMessage Log::error() {
	return LogMessage( "ERROR: " );
}

LogMessage Log::warning() {
	return LogMessage( "WARNING: " );
}

LogMessage Log::debug() {
	return LogMessage( "DEBUG: " );
}

void Log::freeStreamList() {
	boost::unique_lock <boost::mutex> lock( logMutex );
	for (size_t i = 0; i < outputs->size(); ++i)
		(*outputs)[i]->flush();
	safeDelete( outputs );
}