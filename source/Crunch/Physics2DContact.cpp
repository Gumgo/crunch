#include "Physics2DContact.h"
#include "Physics2DShape.h"
#include "Physics2DBody.h"

namespace Physics2D {

// circle
Manifold circleCircleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CircleShape*)s0, tf0, (CircleShape*)s1, tf1 );
	return m;
}

Manifold circleEllipseManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CircleShape*)s0, tf0, (EllipseShape*)s1, tf1 );
	return m;
}

Manifold circleCapsuleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s1, tf0, (CircleShape*)s0, tf1 );
	return m;
}

Manifold circleConvexPolygonManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CircleShape*)s0, tf0, (ConvexPolygonShape*)s1, tf1 );
	return m;
}

Manifold circleEdgeManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CircleShape*)s0, tf0, (EdgeShape*)s1, tf1 );
	return m;
}

// ellipse
Manifold ellipseCircleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CircleShape*)s1, tf0, (EllipseShape*)s0, tf1 );
	return m;
}

Manifold ellipseEllipseManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (EllipseShape*)s0, tf0, (EllipseShape*)s1, tf1 );
	return m;
}

Manifold ellipseCapsuleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s1, tf0, (EllipseShape*)s0, tf1 );
	return m;
}

Manifold ellipseConvexPolygonManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (ConvexPolygonShape*)s1, tf0, (EllipseShape*)s0, tf1 );
	return m;
}

Manifold ellipseEdgeManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (EdgeShape*)s1, tf0, (EllipseShape*)s0, tf1 );
	return m;
}

// capsule
Manifold capsuleCircleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s0, tf0, (CircleShape*)s1, tf1 );
	return m;
}

Manifold capsuleEllipseManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s0, tf0, (EllipseShape*)s1, tf1 );
	return m;
}

Manifold capsuleCapsuleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s0, tf0, (CapsuleShape*)s1, tf1 );
	return m;
}

Manifold capsuleConvexPolygonManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s0, tf0, (ConvexPolygonShape*)s1, tf1 );
	return m;
}

Manifold capsuleEdgeManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s0, tf0, (EdgeShape*)s1, tf1 );
	return m;
}

// convex polygon
Manifold convexPolygonCircleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CircleShape*)s1, tf0, (ConvexPolygonShape*)s0, tf1 );
	return m;
}

Manifold convexPolygonEllipseManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (ConvexPolygonShape*)s0, tf0, (EllipseShape*)s1, tf1 );
	return m;
}

Manifold convexPolygonCapsuleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s1, tf0, (ConvexPolygonShape*)s0, tf1 );
	return m;
}

Manifold convexPolygonConvexPolygonManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (ConvexPolygonShape*)s0, tf0, (ConvexPolygonShape*)s1, tf1 );
	return m;
}

Manifold convexPolygonEdgeManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (ConvexPolygonShape*)s0, tf0, (EdgeShape*)s1, tf1 );
	return m;
}

// edge
Manifold edgeCircleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CircleShape*)s1, tf0, (EdgeShape*)s0, tf1 );
	return m;
}

Manifold edgeEllipseManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (EdgeShape*)s0, tf0, (EllipseShape*)s1, tf1 );
	return m;
}

Manifold edgeCapsuleManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (CapsuleShape*)s1, tf0, (EdgeShape*)s0, tf1 );
	return m;
}

Manifold edgeConvexPolygonManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (ConvexPolygonShape*)s1, tf0, (EdgeShape*)s0, tf1 );
	return m;
}

Manifold edgeEdgeManifold( const Shape * s0, const Matrix33ar & tf0, const Shape * s1, const Matrix33ar & tf1 ) {
	Manifold m;
	getContactManifold( &m, (EdgeShape*)s0, tf0, (EdgeShape*)s1, tf1 );
	return m;
}

typedef fastdelegate::FastDelegate4 <const Shape*, const Matrix33ar &, const Shape*, const Matrix33ar &, Manifold> ManifoldFunction;
static ManifoldFunction manifoldFunctions[Shape::TYPE_COUNT][Shape::TYPE_COUNT] = {
	{ &circleCircleManifold, &circleEllipseManifold, &circleCapsuleManifold, &circleConvexPolygonManifold, &circleEdgeManifold },
	{ &ellipseCircleManifold, &ellipseEllipseManifold, &ellipseCapsuleManifold, &ellipseConvexPolygonManifold, &ellipseEdgeManifold },
	{ &capsuleCircleManifold, &capsuleEllipseManifold, &capsuleCapsuleManifold, &capsuleConvexPolygonManifold, &capsuleEdgeManifold },
	{ &convexPolygonCircleManifold, &convexPolygonEllipseManifold, &convexPolygonCapsuleManifold, &convexPolygonConvexPolygonManifold, &convexPolygonEdgeManifold },
	{ &edgeCircleManifold, &edgeEllipseManifold, &edgeCapsuleManifold, &edgeConvexPolygonManifold, &edgeEdgeManifold }
};

Contact::Contact( Shape * s0, Shape * s1 ) {
	shapes[0] = s0;
	shapes[1] = s1;
}

void Contact::computeManifold() {
	Matrix33ar tf0 = shapes[0]->getParent()->getMatrixTransform() * shapes[0]->getTransform();
	Matrix33ar tf1 = shapes[1]->getParent()->getMatrixTransform() * shapes[1]->getTransform();
	manifold = manifoldFunctions[shapes[0]->getType()][shapes[1]->getType()]( shapes[0], tf0, shapes[1], tf1 );
}

const Manifold & Contact::getManifold() const {
	return manifold;
}

Contact::SimulationData & Contact::getSimulationData() {
	return simulationData;
}

const Contact::SimulationData & Contact::getSimulationData() const {
	return simulationData;
}

Shape * Contact::getShape( size_t idx ) {
	return shapes[idx];
}

const Shape * Contact::getShape( size_t idx ) const {
	return shapes[idx];
}

size_t Contact::getShapeIndex( const Shape * s ) const {
	if (shapes[0] == s)
		return 0;
	else if (shapes[1] == s)
		return 1;
	else
		return NO_INDEX;
}

}