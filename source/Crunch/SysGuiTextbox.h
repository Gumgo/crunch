#ifndef SYSGUITEXTBOX_DEFINED
#define SYSGUITEXTBOX_DEFINED

#include "Common.h"
#include "Actor.h"
#include "SysGui.h"

#include "TextUtil.h"
#include <boost/function.hpp>

DEFINE_DERIVED_ACTOR( SysGuiTextbox, SysGui, 0, 0 )

public:
	enum TextMode {
		TM_REAL,
		TM_INT,
		TM_UINT,
		TM_STRING
	};

private:
	Vector2i position;
	int width;
	std::string text;
	bool selected;

	std::string selectedText;

	TextMode mode;

	std::string displayedText;
	TextAlignHoriz horiz;
	void recomputeDisplayedText();

	boost::function <void ( const std::string & )> onChangeText;
	boost::function <void ( const std::string &, const std::string & )> onChangeText2;

	bool pointInside( const Vector2f & point ) const;

public:
	void onCreate();

	void move( const Vector2i & pos );
	void setWidth( int w );
	void setText( const std::string & t, bool callback );
	std::string getText() const;
	void setTextMode( TextMode tm );
	void select();
	void deselect( bool callback );

	void setOnChangeText( const boost::function <void ( const std::string & )> & callback );
	void setOnChangeText( const boost::function <void ( const std::string &, const std::string & )> & callback );

	void receiveEvent( const InputEvent & ev );
	void onDismissParent();
	void draw();
};

#endif