/** @file Skeleton2DResource.h
 *  @brief Contains the resource holding 2D skeleton data.
 */

#ifndef SKELETON2DRESOURCE_DEFINED
#define SKELETON2DRESOURCE_DEFINED

#include "Common.h"
#include "Resource.h"
#include "DataReader.h"
#include <vector>
#include <map>

/** @brief Skeleton 2D resource.
 */
DEFINE_RESOURCE_CLASS( Skeleton2DResource, "skeleton2D", "skeleton2Ds" )
public:
	/** @brief The constructor.
	 */
	Skeleton2DResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m );

protected:
	bool loadDataAsync();	/**< Attempts to load the asynchronous component of the resource.*/
	bool loadDataSync();	/**< Attempts to load the synchronous component of the resource.*/
	void freeData();		/**< Frees the resource data.*/

public:
	/** @brief Holds data for a part in a skeleton.
	 */
	struct PartData {
		Vector2f offset;				/**< The offset from the parent.*/
		float rotation;					/**< The rotation from the parent.*/
		bool visible;					/**< Whether this part is visible.*/
		Vector2i imageMin;				/**< The minimum coordinates of this part on the image.*/
		Vector2i imageMax;				/**< The maximum coordinates of this part on the image.*/
		Vector2i origin;				/**< The origin of this part on the image.*/
		std::vector <size_t> children;	/**< The child part IDs.*/
	};

	size_t getPartCount() const;											/**< Returns the number of parts.*/
	const PartData & getPartData( size_t i ) const;							/**< Returns part data.*/
	bool getPartIndex( const std::string & name, size_t & index ) const;	/**< Returns the index of the part with the given name.*/
	size_t getVisiblePartCount() const;										/**< Returns the number of visible parts.*/
	size_t getPartDrawingOrder( size_t i ) const;	/**< Returns the index of the part to draw at the given position in the drawing order.*/

private:
	std::vector <PartData> parts;				// parts of the skeleton
	std::map <std::string, size_t> partNames;	// maps part names to part IDs
	std::vector <size_t> partDrawingOrder;		// order in which to draw the parts
};

/** @brief More convenient name.
 */
typedef ReferenceCountedPointer <Skeleton2DResource> Skeleton2DResourceReference;

/** @brief More convenient name.
 */
typedef ReferenceCountedConstPointer <Skeleton2DResource> Skeleton2DResourceConstReference;

#endif