/** @file HashFunctions.h
 *  @brief Contains some hash functions that can be used with HashMap and HashSet.
 */

#ifndef HASHFUNCTIONS_DEFINED
#define HASHFUNCTIONS_DEFINED

#include "Common.h"

/** @brief A randomly generated seed for MurmurHash 2.0.
 */
#if _WIN64 || __amd64__
static const size_t MURMUR_HASH_2_SEED = 0x7BD525F3D12931C9;
#else
static const size_t MURMUR_HASH_2_SEED = 0xD12931C9;
#endif

/** @brief MurmurHash 2.0
 *
 *  Credit goes to Austin Appleby; go to http://sites.google.com/site/murmurhash/ for details.
 */
size_t murmurHash2( const void * key, size_t len, size_t seed );

/** @brief A function object to hash objects of type T.
 */
template <typename T> struct Hash {
	/** @brief The hash function.
	 */
	size_t operator()( const T & t ) const;
};

template <typename T> size_t Hash <T>::operator()( const T & t ) const {
	return murmurHash2( &t, sizeof( t ), MURMUR_HASH_2_SEED );
}

/** @brief Specialized hash for strings.
 */
template <> struct Hash <std::string> {
	/** @brief The hash function.
	 */
	size_t operator()( const std::string & t ) const;
};

#endif