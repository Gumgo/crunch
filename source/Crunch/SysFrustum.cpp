#include "SysFrustum.h"

void SysFrustum::setFrustum( const Matrix44f & viewProjectionMatrix ) {
	// we will solve for the frustum planes directly from the view-projection matrix
	// let v be our point in world space and let c be the transformed in clip space
	// let M be our matrix
	// then c = Mv
	// clip space is defined as:
	// -cw <= cx <= cw (left/right plane)
	// -cw <= cy <= cw (bottom/top plane)
	// -cw <= cz <= cw (near/far)
	// let M1, M2, M3, M4 be the rows of M. Then:
	// cx = M1 . v
	// cy = M2 . v
	// cz = M3 . v
	// cw = M4 . v
	// using this, we solve the inequalities
	// -cw <= cx ----> 0 <= M4 . v + M1 . v ----> 0 <= (M4+M1).v (left)
	//  cx <= cw ----> 0 <= M4 . v - M1 . v ----> 0 <= (M4-M1).v (right)
	// -cw <= cy ----> 0 <= M4 . v + M2 . v ----> 0 <= (M4+M2).v (bottom)
	//  cy <= cw ----> 0 <= M4 . v - M2 . v ----> 0 <= (M4-M2).v (top)
	// -cw <= cz ----> 0 <= M4 . v + M3 . v ----> 0 <= (M4+M3).v (near)
	//  cz <= cw ----> 0 <= M4 . v - M3 . v ----> 0 <= (M4-M3).v (far)
	// therefore, we have our plane equations
	// since all our vertices will have w = 1, that becomes a constant, so our plane equation is
	// 0 <= (M4 + Mn)xyz . vxyz + (M4 + Mn)w
	// note that the plane normals are "facing" inwards,
	// so side() will return -1 if a point is OUTSIDE the frustum

	Vector4f m1( viewProjectionMatrix[ 0], viewProjectionMatrix[ 1], viewProjectionMatrix[ 2], viewProjectionMatrix[ 3] );
	Vector4f m2( viewProjectionMatrix[ 4], viewProjectionMatrix[ 5], viewProjectionMatrix[ 6], viewProjectionMatrix[ 7] );
	Vector4f m3( viewProjectionMatrix[ 8], viewProjectionMatrix[ 9], viewProjectionMatrix[10], viewProjectionMatrix[11] );
	Vector4f m4( viewProjectionMatrix[12], viewProjectionMatrix[13], viewProjectionMatrix[14], viewProjectionMatrix[15] );
	planes[  FP_LEFT].set( m4.xyz() + m1.xyz(), m4.w + m1.w );
	planes[ FP_RIGHT].set( m4.xyz() - m1.xyz(), m4.w - m1.w );
	planes[FP_BOTTOM].set( m4.xyz() + m2.xyz(), m4.w + m2.w );
	planes[   FP_TOP].set( m4.xyz() - m2.xyz(), m4.w - m2.w );
	planes[  FP_NEAR].set( m4.xyz() + m3.xyz(), m4.w + m3.w );
	planes[   FP_FAR].set( m4.xyz() - m3.xyz(), m4.w - m3.w );
	for (size_t i = 0; i < FRUSTUM_PLANE_COUNT; ++i)
		planes[i].normalize();
}

Planef * SysFrustum::getPlanes() {
	return planes;
}

const Planef * SysFrustum::getPlanes() const {
	return planes;
}

bool SysFrustum::contains( const SysAabb & aabb ) const {
	const Vector3f * bounds[2] = { &aabb.minBound, &aabb.maxBound };
	for (size_t i = 0; i < SysFrustum::FRUSTUM_PLANE_COUNT; ++i) {
		// frustum planes are facing "inward"
		// we need to compare the point most in the direction of the plane's normal
		// for each component of the normal: if it is positive, take from max extent, otherwise take from min extent
		// if normal.{x,y,z} > 0, 1 will be returned, which is the array index for "max"
		// otherwise 0 is returned, which is "min"
		Vector3f p(
			bounds[planes[i].normal.x > 0.0f]->x,
			bounds[planes[i].normal.y > 0.0f]->y,
			bounds[planes[i].normal.z > 0.0f]->z );
		if (planes[i].side( p ) < 0.0f)
			// outside
			return false;
	}
	return true;
}