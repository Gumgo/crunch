#include "SysGuiCheckbox.h"
#include "Context.h"
#include "SysRenderer2D.h"

bool SysGuiCheckbox::pointInside( const Vector2f & point ) const {
	return (point.x >= (float)position.x && point.y >= (float)position.y &&
			point.x < (float)(position.x + 16) && point.y < (float)(position.y + 16));
}

void SysGuiCheckbox::onCreate() {
	SysGui::onCreate();
	checked = false;
}

void SysGuiCheckbox::move( const Vector2i & pos ) {
	position = pos;
}

void SysGuiCheckbox::setText( const std::string & t ) {
	text = t;
}

void SysGuiCheckbox::setChecked( bool c, bool callback ) {
	checked = c;
	if (callback && onCheck)
		onCheck( checked );
}

bool SysGuiCheckbox::isChecked() const {
	return checked;
}

void SysGuiCheckbox::setOnCheck( const boost::function <void ( bool )> & callback ) {
	onCheck = callback;
}

void SysGuiCheckbox::receiveEvent( const InputEvent & ev ) {
	if (ev.type == IET_MOUSE_BUTTON && ev.mouseButtonEvent.button == MBC_LEFT && ev.mouseButtonEvent.type == MBET_PRESSED) {
		Vector2i mouseWindow = Context::get().inputManager.mousePosition();
		Vector2f mouseWorld = camera->screenToWorld( Vector2f(
			(float)mouseWindow.x / (float)Context::get().window.getWidth(),
			(float)mouseWindow.y / (float)Context::get().window.getHeight() ) );
		if (pointInside( mouseWorld ))
			setChecked( !checked, true );
	}
}

void SysGuiCheckbox::onDismissParent() {
}

void SysGuiCheckbox::draw() {
	if (camera != NULL) {
		Matrix33af modelMatrix;
		modelMatrix.translate( (float)position.x, (float)position.y );
		Matrix44f mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

		GpuBlendState blendState = Context::get().gpuState.createBlendState();
		blendState.setBlendFunction( GpuBlendState::BM_NONE );
		blendState.bind();

		SysRenderer2D::get().drawGradientVert( mvpMatrix,
			Vector2f( 0.0f, 0.0f ), Vector2f( 16.0f, 16.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ),
			Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );

		if (checked) {
			SysRenderer2D::get().drawGradientVert( mvpMatrix,
				Vector2f( 1.0f, 1.0f ), Vector2f( 14.0f, 14.0f ),
				Vector4f( 0.75f, 0.75f, 0.75f, 1.0f ),
				Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ) );
			SysRenderer2D::get().drawGradientVert( mvpMatrix,
				Vector2f( 5.0f, 5.0f ), Vector2f( 6.0f, 6.0f ),
				Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ),
				Vector4f( 0.0f, 0.0f, 0.0f, 1.0f ) );
		} else
			SysRenderer2D::get().drawGradientVert( mvpMatrix,
				Vector2f( 1.0f, 1.0f ), Vector2f( 14.0f, 14.0f ),
				Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ),
				Vector4f( 0.75f, 0.75f, 0.75f, 1.0f ) );

		modelMatrix.translate( 20.0f, 8.0f );
		mvpMatrix = camera->getModelViewProjectionMatrix( modelMatrix );

 		SysRenderer2D::get().drawText( text, *font, mvpMatrix, TAH_LEFT, TAV_MIDDLE, Vector4f( 1.0f, 1.0f, 1.0f, 1.0f ) );
	}
}