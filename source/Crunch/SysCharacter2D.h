#ifndef SYSCHARACTER2D_DEFINED
#define SYSCHARACTER2D_DEFINED

#include "Common.h"
#include "Skeleton2DResource.h"
#include "Skeleton2DAnimationResource.h"
#include "TextureResource.h"
#include "BinarySearch.h"
#include <vector>
#include <stack>

class SysCharacter2D {
public:
	SysCharacter2D();
	void setSkeleton( Skeleton2DResourceReference & skel );
	void setTexture( TextureResourceReference & tex );
	void setColor( const Vector4f & c );

	void startAnimation();
	void applyAnimation( Skeleton2DAnimationResource & anim, uint frame, bool clamp = false );
	void blendAnimation( Skeleton2DAnimationResource & anim, uint frame, float blend, bool clamp = false );
	void endAnimation();

	void draw( const Matrix44f & mvpMatrix );

private:
	std::vector <Vector2f> pixelMins;
	std::vector <Vector2f> pixelMaxes;
	std::vector <Vector2f> origins;
	std::vector <Matrix33af> transforms;
	std::vector <Matrix33af> drawnTransforms;
	std::vector <std::pair <Vector2f, float>> animationTransforms;

	Skeleton2DResourceReference skeleton;
	TextureResourceReference texture;
	Vector4f color;
};

#endif