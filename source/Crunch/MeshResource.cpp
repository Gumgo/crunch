#include "MeshResource.h"
#include "Context.h"
#include "Log.h"

MeshResource::MeshResource( ResourceManager * rm, const std::string & n, const std::string & p, uint m )
	: Resource( rm, n, p, m ) {
}

bool MeshResource::loadDataAsync() {
	try {
		KeyValueReader in( getPath() );

		KeyValueReader::Iterator rootIt = in.getIterator();

		KeyValueReader::Iterator boundsIt = rootIt.getArray( "bounds" );

		KeyValueReader::Iterator minIt = boundsIt.getArray( "min" );
		minBound.set( minIt.getFloat( "x" ), minIt.getFloat( "y" ), minIt.getFloat( "z" ) );

		KeyValueReader::Iterator maxIt = boundsIt.getArray( "max" );
		maxBound.set( maxIt.getFloat( "x" ), maxIt.getFloat( "y" ), maxIt.getFloat( "z" ) );

		KeyValueReader::Iterator buffersIt = rootIt.getArray( "buffers" );

		KeyValueReader::Iterator vertexBufferIt = buffersIt.getArray( "vertexBuffer" );

		KeyValueReader::Iterator attributesIt = vertexBufferIt.getArray( "attributes" );
		for (size_t i = 0; i < attributesIt.getCount(); ++i) {
			KeyValueReader::Iterator attributeIt = attributesIt.getArray( i );
			attributes.push_back( AttributeData() );
			attributes.back().name = attributeIt.getString( "name" );
			std::string typeString = attributeIt.getString( "type" );
			GLenum type;
			if (typeString == "GL_FLOAT")
				type = GL_FLOAT; // most common first
			else if (typeString == "GL_BYTE")
				type = GL_BYTE;
			else if (typeString == "GL_UNSIGNED_BYTE")
				type = GL_UNSIGNED_BYTE;
			else if (typeString == "GL_SHORT")
				type = GL_SHORT;
			else if (typeString == "GL_UNSIGNED_SHORT")
				type = GL_UNSIGNED_SHORT;
			else if (typeString == "GL_INT")
				type = GL_INT;
			else if (typeString == "GL_UNSIGNED_INT")
				type = GL_UNSIGNED_INT;
			else if (typeString == "GL_HALF_FLOAT")
				type = GL_FLOAT;
			else if (typeString == "GL_DOUBLE")
				type = GL_DOUBLE;
			else if (typeString == "GL_INT_2_10_10_10_REV")
				type = GL_INT_2_10_10_10_REV;
			else if (typeString == "GL_UNSIGNED_INT_2_10_10_10_REV")
				type = GL_UNSIGNED_INT_2_10_10_10_REV;
			else
				throw std::runtime_error( "Invalid attribute type " + typeString );
			attributes.back().type = type;
			if (attributeIt.contains( "normalized" ))
				attributes.back().normalized = attributeIt.getBool( "normalized" );
			else
				attributes.back().normalized = false;
			if (attributeIt.contains( "integer" ))
				attributes.back().integer = attributeIt.getBool( "integer" );
			else
				attributes.back().integer = false;
			if (attributes.back().normalized && attributes.back().integer)
				throw std::runtime_error( "Cannot have normalized integer attribute" );
			attributes.back().components = attributeIt.getUint( "components" );
			attributes.back().stride = attributeIt.getUint( "stride" );
			attributes.back().offset = attributeIt.getUint( "offset" );
		}

		size_t vertexDataSize = vertexBufferIt.getBinaryDataLength( "data" );
		const byte * vertexDataPtr = vertexBufferIt.getBinaryData( "data" );
		vertexData.resize( vertexDataSize );
		if (vertexDataSize > 0)
			std::copy( vertexDataPtr, vertexDataPtr + vertexDataSize, vertexData.begin() );

		KeyValueReader::Iterator indexBufferIt = buffersIt.getArray( "indexBuffer" );

		indexSize = indexBufferIt.getUint( "indexSize" );
		if (indexSize != 1 && indexSize != 2 && indexSize != 4)
				throw std::runtime_error( "Invalid index size" );

		size_t indexDataSize = indexBufferIt.getBinaryDataLength( "data" );
		if (indexDataSize % indexSize != 0)
			throw std::runtime_error( "Invalid index data" );
		indexCount = indexDataSize / indexSize;
		const byte * indexDataPtr = indexBufferIt.getBinaryData( "data" );
		indexData.resize( indexDataSize );
		if (indexDataSize > 0)
			std::copy( indexDataPtr, indexDataPtr + indexDataSize, indexData.begin() );

		// measure number of vertices
		vertexCount = 0;
		switch (indexSize) {
		case 1:
			for (size_t i = 0; i < indexCount; ++i)
				vertexCount = std::max( vertexCount, (size_t)indexData[i] );
			break;
		case 2:
			for (size_t i = 0; i < indexCount; ++i)
				vertexCount = std::max( vertexCount, (size_t)((ushort*)&indexData.front())[i] );
			break;
		case 4:
			for (size_t i = 0; i < indexCount; ++i)
				vertexCount = std::max( vertexCount, (size_t)((uint*)&indexData.front())[i] );
			break;
		}
		if (indexCount > 0)
			++vertexCount;
	} catch (const std::exception & e) {
		Log::error() << e.what();
		vertexData.clear();
		vertexData.shrink_to_fit();
		indexData.clear();
		indexData.shrink_to_fit();
		return false;
	}

	return true;
}

bool MeshResource::loadDataSync() {
	try {
		GpuBuffer::BufferDescription vDesc;
		vDesc.type = GL_ARRAY_BUFFER;
		vDesc.size = vertexData.size();
		vDesc.data = vertexData.empty() ? NULL : &vertexData[0];
		vDesc.usage = GL_STATIC_DRAW;
		vertexBuffer = Context::get().gpuState.createBuffer( vDesc );

		GpuBuffer::BufferDescription iDesc;
		iDesc.type = GL_ELEMENT_ARRAY_BUFFER;
		iDesc.size = indexData.size();
		iDesc.data = indexData.empty() ? NULL : &indexData[0];
		iDesc.usage = GL_STATIC_DRAW;
		indexBuffer = Context::get().gpuState.createBuffer( iDesc );

		// release CPU memory
		vertexData.clear();
		vertexData.shrink_to_fit();
		indexData.clear();
		indexData.shrink_to_fit();
	} catch (const std::exception & e) {
		Log::error() << e.what();
		vertexData.clear();
		vertexData.shrink_to_fit();
		indexData.clear();
		indexData.shrink_to_fit();
		vertexBuffer = GpuBufferReference();
		indexBuffer = GpuBufferReference();
		return false;
	}

	return true;
}

void MeshResource::freeData() {
}

const Vector3f & MeshResource::getMinBound() const {
	return minBound;
}

const Vector3f & MeshResource::getMaxBound() const {
	return maxBound;
}

size_t MeshResource::getAttributeCount() const {
	return attributes.size();
}

const MeshResource::AttributeData & MeshResource::getAttribute( size_t i ) const {
	return attributes[i];
}

size_t MeshResource::getIndexSize() const {
	return indexSize;
}

GpuBufferReference MeshResource::getVertexBuffer() {
	return vertexBuffer;
}

GpuBufferConstReference MeshResource::getVertexBuffer() const {
	return vertexBuffer;
}

GpuBufferReference MeshResource::getIndexBuffer() {
	return indexBuffer;
}

GpuBufferConstReference MeshResource::getIndexBuffer() const {
	return indexBuffer;
}

size_t MeshResource::getVertexCount() const {
	return vertexCount;
}

size_t MeshResource::getIndexCount() const {
	return indexCount;
}