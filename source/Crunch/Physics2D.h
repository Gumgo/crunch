/** @file Physics2D.h
 *  @brief Contains physics definitions.
 */

#ifndef PHYSICS2D_DEFINED
#define PHYSICS2D_DEFINED

#include "Common.h"

/** @namespace Physics2D
 *  @brief The namespace for objects related to 2D physics simulation.
 */
namespace Physics2D {

class Shape;

typedef float TimeUnit;					/**< The time unit type.*/
typedef float Real;						/**< The real type.*/
typedef Vector2 <Real> Vector2r;		/**< A vector of real types.*/
typedef Matrix33a <Real> Matrix33ar;	/**< A matrix of real types.*/

/** @brief A time step for the simulation.
 */
struct TimeStep {
	TimeUnit dt;	/**< The time delta.*/
	TimeUnit invDt;	/**< The inverse of the time delta.*/
	TimeStep( TimeUnit pDt );	/**< The constructor.*/
};

/** @brief Holds data about the mass of an object.
 */
struct MassData {
	Vector2r center;	/**< The center of gravity.*/
	Real mass;			/**< The mass.*/
	Real invMass;		/**< The inverse of the mass.*/
	Real inertia;		/**< The moment of inertia.*/
	Real invInertia;	/**< The inverse of the moment of inertia.*/

	MassData();			/**< The default constructor.*/
	MassData( const Vector2r & c, Real m, Real i );	/**< The constructor.*/
};

/** @brief An axis-aligned bounding box.
 */
struct Aabb {
	Vector2r minBound;	/**< The minimum bound.*/
	Vector2r maxBound;	/**< The maximum bound.*/

	Aabb();				/**< The default constructor.*/
	Aabb( const Vector2r & minBnd, const Vector2r & maxBnd );	/**< The constructor.*/

	void expand( Real amount );	/**< Expands this AABB by the given amount.*/

	static Aabb from2Points( const Vector2r & p0, const Vector2r & p1 );	/**< Constructs an AABB from 2 points.*/
	static bool overlap( const Aabb & aabb0, const Aabb & aabb1 );			/**< Returns whether the two AABBs overlap.*/
};

/** @brief Represents a point of contact.
 */
struct ContactPoint {
	Vector2r point;		/**< The point of contact.*/
	Real depth;			/**< The contact depth.*/
};

/** @brief Represents a contact manifold.
 *
 *  The contact manifold is relative to one of the untransformed shapes involved.
 *  This lack of transformation includes both the parent body's transformation as
 *  well as the shape's own transformation.
 */
struct Manifold {
	const Shape * local;			/**< Which shape this manifold is local to.*/
	uint contactPointCount;			/**< The number of contact points.*/
	ContactPoint contactPoints[2];	/**< The contact points.*/
	Vector2r normal;				/**< The vector of minimum translation.*/
};

static const size_t GROUPS = sizeof( uint ) * 8;

/** @brief Used to check whether specified group requirements are met.
 */
class GroupFilter {
public:
	/** @brief Initializes the group filter with the given fields.
	 */
	GroupFilter( uint oneOf, uint allOf, uint noneOf );

	uint getOneOfGroups() const;	/**< Returns the set of groups which inputs must satisfy at least one of.*/
	uint getAllOfGroups() const;	/**< Returns the set of groups which inputs must satisfy all of.*/
	uint getNoneOfGroups() const;	/**< Returns the set of groups which inputs must satisfy none of.*/

	void setOneOfGroups( uint g );	/**< Sets the set of groups which inputs must satisfy at least one of.*/
	void setAllOfGroups( uint g );	/**< Sets the set of groups which inputs must satisfy all of.*/
	void setNoneOfGroups( uint g );	/**< Sets the set of groups which inputs must satisfy none of.*/

	void addOneOfGroup( uint idx );		/**< Adds a group to the set of groups which inputs must satisfy at least one of.*/
	void addAllOfGroup( uint idx );		/**< Adds a group to the set of groups which inputs must satisfy all of.*/
	void addNoneOfGroup( uint idx );	/**< Adds a group to the set of groups which inputs must satisfy none of.*/

	void removeOneOfGroup( uint idx );	/**< Removes a group from the set of groups which inputs must satisfy at least one of.*/
	void removeAllOfGroup( uint idx );	/**< Removes a group from the set of groups which inputs must satisfy all of.*/
	void removeNoneOfGroup( uint idx );	/**< Removes a group from the set of groups which inputs must satisfy none of.*/

	/** @brief Returns whether the given input passes the filter.
	 */
	bool test( uint input ) const;

private:
	uint oneOfGroups;	// inputs must satisfy at least one of these groups
	uint allOfGroups;	// inputs must satisfy all of these groups
	uint noneOfGroups;	// inputs must satisfy none of these groups
};

/** @brief Used to check whether specified group pairings are met.
 */
class GroupFilter2D {
public:
	/** @brief Initializes all pairs to be either true or false.
	 */
	GroupFilter2D( bool initialValue );

	void enablePair( uint a, uint b );	/**< Enables a pair of groups.*/
	void disablePair( uint a, uint b );	/**< Disables a pair of groups.*/

	bool test( uint a, uint b ) const;	/**< Returns whether the given input passes the filter.*/

private:
	// bit array of valid group pairs
	byte groupPairArray[GROUPS*GROUPS/8];
};

}

#endif