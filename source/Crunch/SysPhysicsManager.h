#if 0

#ifndef SYSPHYSICSMANAGER_DEFINED
#define SYSPHYSICSMANAGER_DEFINED

#include "Common.h"
#include "Actor.h"
#include "PhysicsManager.h"
#ifdef _DEBUG
#include "SysCamera2D.h"
#endif

DEFINE_ACTOR( SysPhysicsManager, 0, std::numeric_limits <int>::max() )

#ifdef _DEBUG
	const SysCamera2D * camera;
#endif

public:
	PhysicsManager physicsManager;
	void onCreate();
	void onUpdate();
	void onDraw();

#ifdef _DEBUG
	void setCamera( const SysCamera2D * cam );
#endif
};

#endif

#endif