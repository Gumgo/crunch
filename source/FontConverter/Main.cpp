#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

#pragma comment( lib, "freetype.lib" )

struct GlyphEntry {
	unsigned char ascii, width;
	unsigned short x, y;
};

template <class T, class S> void write( const T & obj, S & out ) {
	out.write( (char*)&obj, sizeof( T ) );
}

int createFont( const std::string & fontfile, size_t fontSize, const std::string & outfile )
{
	// add all the characters
	std::string chars(
		"abcdefghijklmnopqrstuvwxyz"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		" 1234567890"
		"~!@#$%^&*()-_=+;:'\",./?[]|\\<>`{}\xFF" );
//	for (unsigned int i = 0; i < 256; i++)
//		chars += (unsigned char)i;

	// set the margin side
	// setup the image dimensions (NOTE: may change later to make more efficient)
	const size_t margin = 3;
	size_t imageHeight = 0, imageWidth = 256;

	// initialize the freetype library
	FT_Library library;
	if (FT_Init_FreeType( &library ) != 0) {
		std::cout << "Failed to initialize FreeType2 library.\n";
		return 1;
	}

	// load the font face
	FT_Face face;
	if (FT_New_Face( library, fontfile.c_str(), 0, &face ) != 0) {
		std::cout << "Failed to load font file.\n";
		return 1;
	}

	if (!(face->face_flags & FT_FACE_FLAG_SCALABLE) ||
		!(face->face_flags & FT_FACE_FLAG_HORIZONTAL)) {
		std::cout << "Error setting font size.\n";
		return 1;
	}

	// set the size
	FT_Set_Pixel_Sizes( face, fontSize, 0 );

	int max_descent = 0, max_ascent = 0;
	size_t spaceOnLine = imageWidth - margin, lines = 1;

	// for each character
	for (size_t i = 0; i < chars.size(); i++) {
		// get its index
		size_t char_index = FT_Get_Char_Index( face, (unsigned int)chars[i] );
		if (chars[i] == '\xFF')
			char_index = 0;

		// load and render the glyph
		FT_Load_Glyph( face, char_index, FT_LOAD_DEFAULT );
		FT_Render_Glyph( face->glyph, FT_RENDER_MODE_NORMAL );

		// compute the advance of the character
		size_t advance = (face->glyph->metrics.horiAdvance >> 6) + margin;
		// if no more space on the line, go to next line
		if (advance > spaceOnLine) {
			spaceOnLine = imageWidth - margin;
			lines++;
		}
		spaceOnLine -= advance;

		// compute the character's height properties
		max_ascent = std::max( face->glyph->bitmap_top, max_ascent );
		max_descent = std::max( face->glyph->bitmap.rows - face->glyph->bitmap_top, max_descent );
	}

	// last, factor in the text cursor
	size_t cursorAdvance = 1 + margin;
	// if no more space on the line, go to next line
	if (cursorAdvance > spaceOnLine) {
		spaceOnLine = imageWidth - margin;
		lines++;
	}

	// compute the required image height
	size_t neededImageHeight = (max_ascent + max_descent + margin) * lines + margin;
	imageHeight = 16;
	while (imageHeight < neededImageHeight)
		imageHeight *= 2;

	// allocate the image
	unsigned char * image = new unsigned char[imageHeight * imageWidth];
	for (size_t i = 0; i < imageHeight * imageWidth; i++)
		image[i] = 0;

	std::vector <GlyphEntry> entries( chars.size() );
	size_t x = margin, y = margin + max_ascent;

	// write the characters to the image
	for (size_t i = 0; i < chars.size(); i++) {
		size_t char_index = FT_Get_Char_Index( face, (unsigned int)chars[i] );
		if (chars[i] == '\xFF')
			char_index = 0;

		FT_Load_Glyph( face, char_index, FT_LOAD_DEFAULT );
		FT_Render_Glyph( face->glyph, FT_RENDER_MODE_NORMAL );

		size_t advance = (face->glyph->metrics.horiAdvance >> 6) + margin;
		if (advance > imageWidth - x) {
			x = margin;
			y += (max_ascent + max_descent + margin);
		}

		entries[i].ascii = chars[i];
		entries[i].width = advance - 3;
		entries[i].x = x;
		entries[i].y = y - max_ascent;

		for (int row = 0; row < face->glyph->bitmap.rows; row++) {
			for (int pixel = 0; pixel < face->glyph->bitmap.width; pixel++) {
				image[(x + face->glyph->bitmap_left + pixel) +
					  (y - face->glyph->bitmap_top + row) * imageWidth] =
					  face->glyph->bitmap.buffer[ pixel + row * face->glyph->bitmap.pitch ];
			}
		}

		x += advance;
	}

	unsigned char cursorWidth;
	unsigned short cursorX;
	unsigned short cursorY;
	// last, write the cursor
	if (cursorAdvance > imageWidth - x) {
		x = margin;
		y += (max_ascent + max_descent + margin);
	}

	cursorWidth = cursorAdvance - 3;
	cursorX = x;
	cursorY = y - max_ascent;

	// fill one vertical line of pixels
	for (int row = 0; row < max_ascent+max_descent; row++)
		image[x + (y - max_ascent + row) * imageWidth] = 255;

	// write the data to file
	std::ofstream out( outfile.c_str(), std::ios::binary );
	out.put( 'F' );
	out.put( '0' );
	write( imageWidth, out );
	write( imageHeight, out );
	write( max_ascent + max_descent, out );
	write( chars.size(), out );

	for (size_t i = 0; i < chars.size(); i++)
		write( entries[i], out );
	// write the cursor
	write( cursorWidth, out );
	write( cursorX, out );
	write( cursorY, out );
	for (size_t i = 0; i < imageWidth * imageHeight; i++)
		out.put( image[i] ); 

	delete[] image;

	FT_Done_FreeType( library );
	std::cout << "Wrote " << outfile << ", " << imageWidth << " by " << imageHeight << " pixels.\n";
	return 0;
}

int main( int argc, char ** argv ) {
	for (int i = 1; i < argc; ++i) {
		size_t size;
		std::string out;
		std::cout << "Converting " << argv[i] << "\n";
		std::cout << "Please enter font size: \n";
		std::cin >> size;
		while (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore( std::numeric_limits <int>::max(), '\n' );
			std::cout << "Font size invalid; please enter valid font size: \n";
			std::cin >> size;
		}
		std::cout << "Please enter output filename: \n";
		std::cin >> out;

		if (createFont( argv[i], size, out ) == 0)
			std::cout << "Font conversion successful.\n";
		else
			std::cout << "Font conversion failed.\n";
	}
}